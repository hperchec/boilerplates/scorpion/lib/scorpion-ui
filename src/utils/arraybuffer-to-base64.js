/**
 * @vuepress
 * ---
 * title: arrayBufferToBase64
 * headline: arrayBufferToBase64
 * sidebarTitle: arrayBufferToBase64
 * ---
 */

// base64-arraybuffer-es6
import { encode } from 'base64-arraybuffer-es6'

/**
 * arrayBufferToBase64
 * @param {ArrayBuffer} buffer - The ArrayBuffer
 * @param {number} [byteOffset] - See `base64-arraybuffer-es6` package documentation
 * @param {number} [lngth] - See `base64-arraybuffer-es6` package documentation
 * @returns {string} Returns the base64 encoded string
 * @description
 * Convert an ArrayBuffer instance to base64 encoded string
 *
 * ::: tip See also
 * [base64-arraybuffer-es6](https://www.npmjs.com/package/base64-arraybuffer-es6) package documentation
 * :::
 * @example
 * // buffer is an ArrayBuffer containing string: 'foo bar'
 * arrayBufferToBase64(buffer) // => ''
 */
export const arrayBufferToBase64 = function (buffer, byteOffset, lngth) {
  return encode(buffer, byteOffset, lngth)
}

export default arrayBufferToBase64
