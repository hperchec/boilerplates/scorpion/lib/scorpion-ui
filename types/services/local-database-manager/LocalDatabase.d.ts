export default LocalDatabase;
declare class LocalDatabase {
    static readonly get supportedDrivers(): string[];
    constructor(localDatabaseOptions: LocalDatabaseOptions);
    private _name;
    _driver: any;
    _driverOptions: any;
    _initCallback: any;
    _encryptionKey: any;
    _methodsMap: any;
    _instance: any;
    set name(value: string);
    get name(): string;
    set driverOptions(value: string);
    get driverOptions(): string;
    set initCallback(value: Function);
    get initCallback(): Function;
    readonly get driver(): any;
    set encryptionKey(value: Uint8Array);
    get encryptionKey(): Uint8Array;
    setDriver(name: string, driverOptions?: object): void;
    mapDriverMethods(methods: object): void;
    getInstance(): any;
    init(callback?: Function): any;
    useEncryption(...args: any[]): any;
    useCompression(...args: any[]): any;
    open(): Promise<any>;
    close(): any;
    unmount(): LocalDatabase;
    initDexie(): void;
    toJSON(): object;
}
import LocalDatabaseOptions from './LocalDatabaseOptions';
//# sourceMappingURL=LocalDatabase.d.ts.map