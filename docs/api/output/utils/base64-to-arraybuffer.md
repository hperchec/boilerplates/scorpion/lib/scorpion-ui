---
title: base64ToArrayBuffer
headline: base64ToArrayBuffer
sidebarTitle: base64ToArrayBuffer
---

# base64ToArrayBuffer

<a name="base64toarraybuffer">
</a>

## base64ToArrayBuffer ⇒ <code>ArrayBuffer</code>

Convert an base64 encoded string to ArrayBuffer instance::: tip See also[base64-arraybuffer-es6](https://www.npmjs.com/package/base64-arraybuffer-es6) package documentation:::

**Returns**: <code>ArrayBuffer</code> - Returns the ArrayBuffer

| Param | Type | Description |
| --- | --- | --- |
| str | <code>string</code> | The base64 encoded string |
| [options] | <code>object</code> | See `base64-arraybuffer-es6` package documentation |

**Example**

```js
base64ToArrayBuffer('Zm9vIGJhcg==') // => ArrayBuffer containing the string: 'foo bar'
```

