---
title: getAllKeys
headline: getAllKeys
sidebarTitle: getAllKeys
---

# getAllKeys

<a name="getallkeys">
</a>

## getAllKeys ⇒ <code>Array.&lt;string&gt;</code>

Get all keys from an object (see: https://stackoverflow.com/a/70629468)

**Returns**: <code>Array.&lt;string&gt;</code> - - An array of keys

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| obj | <code>\*</code> |  | The target object |
| [options] | <code>object</code> |  | The options object |
| [options.excludeKeys] | <code>Array.&lt;(string\|RegExp)&gt;</code> |  | The keys to exclude by default |
| [options.includeSelf] | <code>boolean</code> | <code>true</code> | Include own keys |
| [options.includePrototypeChain] | <code>boolean</code> | <code>true</code> | Include inherited keys |
| [options.includeTop] | <code>boolean</code> | <code>true</code> | Include top level keys |
| [options.includeEnumerables] | <code>boolean</code> | <code>true</code> | Include enumerable keys |
| [options.includeNonenumerables] | <code>boolean</code> | <code>true</code> | Include non-enumerable keys |
| [options.includeStrings] | <code>boolean</code> | <code>true</code> | Include string keys |
| [options.includeSymbols] | <code>boolean</code> | <code>true</code> | Include symbol keys |

**Example**

```js
getAllKeys({ foo: 'bar' }) // => [ 'foo' ]
```

