/**
 * @vuepress
 * ---
 * title: union
 * headline: union
 * sidebarTitle: union
 * ---
 */

// Lodash.union
import lodashUnion from 'lodash.union'

/**
 * union
 * @param {...Array} args - Same as lodash.union
 * @returns {Array} The union of arrays
 * @description
 * Find max by in array (lodash.union).
 *
 * ::: tip See also
 * [lodash.union](https://www.npmjs.com/package/lodash.union) package documentation
 * :::
 * @example
 * const arrayA = [ 'John', 'Jane', 1, 2 ]
 * const arrayB = [ 1, 2, 3, 4 ]
 * union(arrayA, arrayB) // Return -> [ 'John', 'Jane', 1, 2, 3, 4 ]
 */
export const union = function (...args) {
  return lodashUnion(...args)
}

export default union
