/**
 * @vuepress
 * ---
 * title: Router - AppMiddleware
 * headline: Router - AppMiddleware
 * sidebarTitle: .AppMiddleware
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import Core from '@/Core'

// @ts-ignore
const log = (...args) => Core.service('logger').consoleLog(...args) // eslint-disable-line dot-notation

/**
 * @name AppMiddleware
 * @static
 * @type {Function}
 * @returns {void}
 * @description
 * > {@link ../../../ context}.{@link ../../ router}.{@link ../ middlewares}.{@link ./ AppMiddleware}
 *
 * See also [vue-router beforeEnter guard documentation](https://v3.router.vuejs.org/guide/advanced/navigation-guards.html#global-before-guards)
 *
 * App 'root' middleware.
 *
 * Fill route `meta.from` with the `from` route object
 */
export default (to, from, next) => {
  // Log navigation
  log(`[AppMiddleware] Navigate from "${from.fullPath}" to "${to.fullPath}"`, { type: 'router', prod: false })
  // Fill meta: from
  to.meta.from = from
  // Next
  next()
}
