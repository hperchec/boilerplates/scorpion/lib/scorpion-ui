export namespace vue {
    export { components };
    export { directives };
    export { ensureComponentConstructor };
    export { getComponentDescriptor };
    export { filters };
    export { mixins };
    export const plugins: any;
    export { root };
}
export default vue;
import components from './components';
import directives from './directives';
import ensureComponentConstructor from './ensure-component-constructor';
import getComponentDescriptor from './get-component-descriptor';
import filters from './filters';
import mixins from './mixins';
import root from './root';
//# sourceMappingURL=index.d.ts.map