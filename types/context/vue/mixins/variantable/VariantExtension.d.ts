export default VariantExtension;
/**
 * @classdesc
 * VariantExtension class for variantable mixin
 */
declare class VariantExtension {
    /**
     * Creates new instance
     * @param {string} parentName - The parent variant name
     * @param {object} overrides - An object to override parent variant
     */
    constructor(parentName: string, overrides: object);
    parentName: string;
    overrides: any;
    /**
     * Returns an object that is the result of parent object merged with this.overrides
     * @param {object} parent - The parent variant object
     * @param {string} [format = 'object'] - The format
     * @returns {object} The merged variant object
     */
    extendParent(parent: object, format?: string): object;
}
//# sourceMappingURL=VariantExtension.d.ts.map