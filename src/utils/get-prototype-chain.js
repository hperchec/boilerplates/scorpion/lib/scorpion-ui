/**
 * @vuepress
 * ---
 * title: getPrototypeChain
 * headline: getPrototypeChain
 * sidebarTitle: getPrototypeChain
 * ---
 */

// Utils
import isNull from './is-null'

/**
 * getPrototypeChain
 * @param {*} value - The value
 * @returns {object[]} Returns the prototype chain
 * @example
 * getPrototypeChain({ foo: 'bar' }) // => [ Object ]
 * @description
 * Returns an array containing the prototype chain of `value`
 */
export const getPrototypeChain = function (value) {
  const chain = []
  let t = value
  while (t) {
    t = Object.getPrototypeOf(t) // Get prototype
    if (!isNull(t)) chain.push(t) // push if not not null
  }
  return chain
}

export default getPrototypeChain
