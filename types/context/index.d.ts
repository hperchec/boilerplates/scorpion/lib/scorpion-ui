export namespace context {
    export let models: {};
    export const services: Record<"i18n" | "router" | "api-manager" | "event-bus" | "error-manager" | "local-storage-manager" | "logger" | "modal-manager" | "toast-manager" | "websocket-manager", any>;
    export { support };
    export { vue };
}
export default context;
import support from './support';
import vue from './vue';
//# sourceMappingURL=index.d.ts.map