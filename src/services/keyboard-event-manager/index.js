/**
 * @vuepress
 * ---
 * title: "Service (keyboard-event-manager)"
 * headline: "Service (keyboard-event-manager)"
 * sidebarTitle: keyboard-event-manager
 * initialOpenGroupIndex: -1 # optional, defaults to 0, defines the index of initially opened subgroup
 * prev: ../
 * next: ./options
 * ---
 */

import Service from '@/Service'

import options from './options'
import KeyboardEventManager from './KeyboardEventManager'

/* eslint-disable jsdoc/require-param, jsdoc/require-returns */

/**
 * @scorpion_ui_service {KeyboardEventManager} keyboard-event-manager
 * @description
 * The service "keyboard-event-manager" is designed to make authentication managemnent easier.
 */
export default new Service({
  // Service
  service: {
    // Identifier
    identifier: 'keyboard-event-manager',
    // Register service
    register: function (_Core) {
      // ...
    },
    /**
     * @alias create
     * @description
     * Returns an instance of `KeyboardEventManager` created with options.
     */
    create: (serviceOptions, { KeyboardEventManager: _KeyboardEventManager }, _Core) => {
      // Return new KeyboardEventManager
      return new _KeyboardEventManager(serviceOptions)
    },
    /**
     * @alias plugin
     * @description
     * The following properties will be set to Vue prototype:
     *
     * ```js
     * Vue.prototype.$keyboardEventManager // => Core.service('keyboard-event-manager')
     * ```
     */
    vuePlugin: ({ KeyboardEventManager: _KeyboardEventManager }, _Core) => _KeyboardEventManager,
    // Vue plugin options
    pluginOptions: {},
    // rootOption
    rootOption: ({ KeyboardEventManager: _KeyboardEventManager }, _Core) => _KeyboardEventManager.rootOption
  },
  // On initialized
  onInitialized: (Core) => {
    // ...
  },
  // Options
  options,
  /**
   * @alias expose
   */
  expose: {
    /**
     * @see {@link ./KeyboardEventManager KeyboardEventManager}
     */
    KeyboardEventManager
  }
})
