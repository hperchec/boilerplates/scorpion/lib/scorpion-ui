---
title: clone
headline: clone
sidebarTitle: clone
---

# clone

<a name="clone">
</a>

## clone ⇒ <code>\*</code>

Creates a shallow clone (lodash.clone).::: tip See also[lodash.clone](https://lodash.com/docs/4.17.15#clone) package documentation:::

**Returns**: <code>\*</code> - Returns the cloned object

| Param | Type | Description |
| --- | --- | --- |
| value | <code>\*</code> | The value to clone |

**Example**

```js
clone({ 'a': 1, 'b': 2 }) // creates a shallow clone of the object : { 'a': 1, 'b': 2 }
```

