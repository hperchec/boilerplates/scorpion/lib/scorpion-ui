---
title: set
headline: set
sidebarTitle: set
---

# set

<a name="set">
</a>

## set ⇒ <code>void</code>

Set object property (lodash.set).

| Param | Type | Description |
| --- | --- | --- |
| object | <code>object</code> | The object to modify |
| path | <code>Array</code> \| <code>string</code> | The path of the property to set |
| value | <code>\*</code> | The value to set |

**Example**

```js
const object = { 'a': [{ 'b': { 'c': 3 } }] }set(object, 'a[0].b.c', 4) // => { 'a': [{ 'b': { 'c': 4 } }] }
```

