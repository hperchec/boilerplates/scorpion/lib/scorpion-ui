---
title: PrivateRoute class
headline: PrivateRoute class
sidebarTitle: .PrivateRoute
sidebarDepth: 0
prev: false
next: false
---

# PrivateRoute class

<a name="privateroute">
</a>

## PrivateRoute ⇐ <code>module:services/router/Route~Route</code>

Class that represents a *private* route

- `route.meta.requiresAuth` => `true`

**Extends**: <code>module:services/router/Route~Route</code>  
<a name="newprivateroutenew">
</a>

### new PrivateRoute(...args)

Create a PrivateRouteSee also [Route](../Route)

| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | Same arguments as parent class |

