/**
 * @vuepress
 * ---
 * title: "Support/model"
 * headline: "Support/model"
 * sidebarTitle: model
 * prev: ../
 * next: ./Model
 * ---
 */

import Model from './Model'

/**
 * @typicalname Core.context.support.model
 */
export default {
  /**
   * @type {typeof import('./Model').default}
   */
  Model
}
