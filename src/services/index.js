/**
 * @vuepress
 * ---
 * title: Built-in services
 * headline: Built-in services
 * sidebarTitle: Built-in services
 * initialOpenGroupIndex: -1 # optional, defaults to 0, defines the index of initially opened subgroup
 * prev: ../context/
 * next: ./api-manager/
 * ---
 */

import apiManager from './api-manager'
import authManager from './auth-manager'
import deviceManager from './device-manager'
import errorManager from './error-manager'
import eventBus from './event-bus'
import i18n from './i18n'
import keyboardEventManager from './keyboard-event-manager'
import localDatabaseManager from './local-database-manager'
import localStorageManager from './local-storage-manager'
import logger from './logger'
import modalManager from './modal-manager'
import notificationManager from './notification-manager'
import router from './router'
import store from './store'
import toastManager from './toast-manager'
import webSocketManager from './websocket-manager'

/* eslint-disable quote-props */

/**
 * @alias services
 * @typicalname builtInServices
 * @description
 * ScorpionUI built-in services.
 */
export default [
  /**
   * @alias services['api-manager']
   * @see {@link ./api-manager api-manager}
   */
  apiManager,
  /**
   * @alias services['auth-manager']
   * @see {@link ./auth-manager auth-manager}
   */
  authManager,
  /**
   * @alias services['device-manager']
   * @see {@link ./device-manager device-manager}
   */
  deviceManager,
  /**
   * @alias services['error-manager']
   * @see {@link ./error-manager error-manager}
   */
  errorManager,
  /**
   * @alias services['event-bus']
   * @see {@link ./event-bus event-bus}
   */
  eventBus,
  /**
   * @alias services['i18n']
   * @see {@link ./i18n i18n}
   */
  i18n,
  /**
   * @alias services['keyboard-event-manager']
   * @see {@link ./keyboard-event-manager keyboard-event-manager}
   */
  keyboardEventManager,
  /**
   * @alias services['local-database-manager']
   * @see {@link ./local-database-manager local-database-manager}
   */
  localDatabaseManager,
  /**
   * @alias services['local-storage-manager']
   * @see {@link ./local-storage-manager local-storage-manager}
   */
  localStorageManager,
  /**
   * @alias services['logger']
   * @see {@link ./logger logger}
   */
  logger,
  /**
   * @alias services['modal-manager']
   * @see {@link ./modal-manager modal-manager}
   */
  modalManager,
  /**
   * @alias services['notification-manager']
   * @see {@link ./notification-manager notification-manager}
   */
  notificationManager,
  /**
   * @alias services['router']
   * @see {@link ./router router}
   */
  router,
  /**
   * @alias services['store']
   * @see {@link ./store store}
   */
  store,
  /**
   * @alias services['toast-manager']
   * @see {@link ./toast-manager toast-manager}
   */
  toastManager,
  /**
   * @alias services['websocket-manager']
   * @see {@link ./websocket-manager websocket-manager}
   */
  webSocketManager
]
