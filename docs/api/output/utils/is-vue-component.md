---
title: isVueComponent
headline: isVueComponent
sidebarTitle: isVueComponent
---

# isVueComponent

<a name="isvuecomponent">
</a>

## isVueComponent ⇒ <code>boolean</code>

Returns true if value is a Vue component (VueComponent constructor name or "__isVueComponentOptions" property)

**Returns**: <code>boolean</code> - Returns boolean

| Param | Type | Description |
| --- | --- | --- |
| value | <code>\*</code> | The value to check |

**Example**

```js
isVueComponent({ a: 'b' }) // falseisVueComponent({ template: '<div>Hello World!</div>' }) // falseisVueComponent(Vue.extend({ template: '<div>Hello World!</div>' })) // true
```

