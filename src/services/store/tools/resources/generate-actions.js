import Core from '@/Core'
import { isDef, isUndef } from '@/utils'

/**
 * Generate default resource module actions
 * @param {*} model - The model representing the resource
 * @param {object} [options={}] - The function options
 * @param {object} [options.index] - The index method options
 * @param {object} [options.create] - The create method options
 * @param {object} [options.update] - The update method options
 * @param {object} [options.retrieve] - The retrieve method options
 * @param {object} [options.delete] - The delete method options
 * @returns {object} Returns actions object
 */
export default function generateActions (model, options = {}) {
  // Get module name
  const fullNamespace = model.storeOptions.fullNamespace
  // Utils
  const log = (msg) => Core.service('logger').consoleLog(`"${fullNamespace}" module: ${msg}`, { type: 'store' }) // eslint-disable-line dot-notation

  // Check if model is provided
  if (isUndef(model)) {
    throw new Error('Generate resource module actions: no model provided')
  }
  // Define actions
  const actions = {}

  if (options.index) {
    /**
     * Index method
     * @param {object} context - vuex context
     * @param {object} payload - Payload
     * @returns {Promise<boolean|Error>} Error or return true
     */
    actions.index = async function ({ commit }, payload) {
      // Build request options
      const requestOptions = {
        description: 'Index',
        verb: options.index.method || 'GET',
        path: isDef(options.index.path)
          ? typeof options.index.path === 'function'
            ? options.index.path(payload)
            : options.index.path
          : undefined,
        loading: {
          mutation: `${fullNamespace}/SET_LOADING_STATUS`,
          name: 'index'
        },
        ...(options.index.requestOptions || {})
      }

      const request = await this.makeRequest(requestOptions)
      let result

      result = await request.onError(options.index.onError || ((error, _payload) => {
        // Just returns error
        result = error
      }), payload)

      result ??= await request.onSuccess(options.index.onSuccess || (async (response, _payload) => {
        // Set resource data
        model.insert(response.data, options.index.processData)
        // Return true
        return true
      }), payload)

      return result
    }
  }

  if (options.create) {
    /**
     * Crud: Create method
     * @param {object} context - vuex context
     * @param {object} payload - Payload
     * @param {object} payload.data - Request data (See Server API documentation)
     * @returns {Promise<boolean|Error>} Error or return true
     */
    actions.create = async function ({ commit }, payload) {
      // Build request options
      const requestOptions = {
        description: 'Create',
        verb: options.create.method || 'POST',
        path: isDef(options.create.path)
          ? typeof options.create.path === 'function'
            ? options.create.path(payload)
            : options.create.path
          : undefined,
        loading: {
          mutation: `${fullNamespace}/SET_LOADING_STATUS`,
          name: 'create'
        },
        data: payload.data,
        ...(options.create.requestOptions || {})
      }

      const request = await this.makeRequest(requestOptions)
      let result

      result = await request.onError(options.create.onError || ((error, _payload) => {
        // Just returns error
        return error
      }), payload)

      result ??= await request.onSuccess(options.create.onSuccess || ((response, _payload) => {
        // Log and return true
        log('Resource successfuly created ✔')
        // Set resource data
        return model.insert(response.data, options.create.processData)[0]
      }), payload)

      return result
    }
  }

  if (options.retrieve) {
    /**
     * cRud: Retrieve method
     * @param {object} context - vuex context
     * @param {object} payload - Payload
     * @param {number} payload.id - Resource id
     * @param {object} payload.data - Request data (See Server API documentation)
     * @returns {Promise<boolean|Error>} Error or return true
     */
    actions.retrieve = async function ({ commit }, payload) {
      // Build request options
      const requestOptions = {
        description: 'Retrieve',
        verb: options.retrieve.method || 'GET',
        path: isDef(options.retrieve.path)
          ? typeof options.retrieve.path === 'function'
            ? options.retrieve.path(payload)
            : options.retrieve.path
          : undefined,
        loading: {
          mutation: `${fullNamespace}/SET_LOADING_STATUS`,
          name: 'retrieve'
        },
        data: payload.data,
        ...(options.retrieve.requestOptions || {})
      }

      const request = await this.makeRequest(requestOptions)
      let result

      result = await request.onError(options.retrieve.onError || ((error, _payload) => {
        // Just returns error
        return error
      }), payload)

      result ??= await request.onSuccess(options.retrieve.onSuccess || (async (response, _payload) => {
        // Log and return true
        log('Resource successfuly retrieved ✔')
        // Set resource data
        return model.upsert(response.data, options.retrieve.processData)
      }), payload)

      return result
    }
  }

  if (options.update) {
    /**
     * crUd: Update method
     * @param {object} context - vuex context
     * @param {object} payload - Method payload
     * @param {number} payload.id - Resource id
     * @param {object} payload.data - Request data (See Server API documentation)
     * @returns {Promise<boolean|Error>} Error or return true
     */
    actions.update = async function ({ commit }, payload) {
      // Build request options
      const requestOptions = {
        description: 'Update',
        verb: options.update.method || 'PATCH',
        path: isDef(options.update.path)
          ? typeof options.update.path === 'function'
            ? options.update.path(payload)
            : options.update.path
          : undefined,
        loading: {
          mutation: `${fullNamespace}/SET_LOADING_STATUS`,
          name: 'update'
        },
        data: payload.data,
        ...(options.update.requestOptions || {})
      }

      const request = await this.makeRequest(requestOptions)
      let result

      result = await request.onError(options.update.onError || ((error, _payload) => {
        // Just returns error
        return error
      }), payload)

      result ??= await request.onSuccess(options.update.onSuccess || (async (response, _payload) => {
        // Log and return
        log('Resource successfuly updated ✔')
        // Set resource data
        return model.update(response.data, options.update.processData)
      }), payload)

      return result
    }
  }

  if (options.delete) {
    /**
     * cruD: Delete method
     * @param {object} context - vuex context
     * @param {object} payload - Payload
     * @param {number} payload.id - Resource id
     * @param {object} payload.data - Request data (See Server API documentation)
     * @returns {Promise<boolean|Error>} Error or return true
     */
    actions.delete = async function ({ commit }, payload) {
      // Build request options
      const requestOptions = {
        description: 'Delete',
        verb: options.delete.method || 'DELETE',
        path: isDef(options.delete.path)
          ? typeof options.delete.path === 'function'
            ? options.delete.path(payload)
            : options.delete.path
          : undefined,
        loading: {
          mutation: `${fullNamespace}/SET_LOADING_STATUS`,
          name: 'delete'
        },
        data: payload.data,
        ...(options.delete.requestOptions || {})
      }
      const request = await this.makeRequest(requestOptions)
      let result

      result = await request.onError(options.delete.onError || ((error, _payload) => {
        // Just returns error
        return error
      }), payload)

      result ??= await request.onSuccess(options.delete.onSuccess || (async (response, _payload) => {
        // Set resource data
        model.delete(_payload.id)
        // Log and return true
        log('Resource successfuly deleted ✔')
        return true
      }), payload)

      return result
    }
  }

  return actions
}
