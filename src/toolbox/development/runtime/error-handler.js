/**
 * errorHandler
 * @param {Error} error - The error instance
 * @returns {?boolean} Must return boolean or undefined
 * @description
 * The global runtime error handler (called by webpack-dev-server `client.overlay.runtimeErrors` defined method)
 */
export const errorHandler = (error) => { // eslint-disable-line handle-callback-err
  // console.log('Default error handler returns true', error)
  return true
}

export default errorHandler
