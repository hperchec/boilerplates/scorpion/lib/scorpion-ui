/**
 * @vuepress
 * ---
 * title: base64ToUint8Array
 * headline: base64ToUint8Array
 * sidebarTitle: base64ToUint8Array
 * ---
 */

import base64ToArrayBuffer from './base64-to-arraybuffer'

/**
 * base64ToUint8Array
 * @param {string} str - The base64 encoded string
 * @param {object} [toArrayBufferOptions] - See base64ToArrayBuffer util
 * @param {number} [offset] - See Uint8Array constructor second param
 * @param {number} [length] - See Uint8Array constructor thrid param
 * @returns {Uint8Array} Returns the Uint8Array
 * @description
 * Convert an base64 encoded string to Uint8Array instance
 *
 * ::: tip See also
 * [Uint8Array](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Uint8Array/Uint8Array) constructor documentation
 * :::
 * @example
 * base64ToUint8Array('Zm9vIGJhcg==') // => Uint8Array containing the string: 'foo bar'
 */
export const base64ToUint8Array = function (str, toArrayBufferOptions, offset, length) {
  return new Uint8Array(base64ToArrayBuffer(str, toArrayBufferOptions), offset, length)
}

export default base64ToUint8Array
