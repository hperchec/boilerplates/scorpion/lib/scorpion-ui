---
title: PromiseQueue class
headline: PromiseQueue class
sidebarTitle: PromiseQueue
sidebarDepth: 0
---

# PromiseQueue class

<a name="promisequeue">
</a>

## PromiseQueue

PromiseQueue class.

<a name="newpromisequeuenew">
</a>

### new PromiseQueue([maxPendingPromises], [maxQueuedPromises], [options])

Create a new PromiseQueue

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [maxPendingPromises] | <code>number</code> | <code>Infinity</code> | Max number of concurrently executed promises |
| [maxQueuedPromises] | <code>number</code> | <code>Infinity</code> | Max number of queued promises |
| [options] | <code>object</code> |  | The options |

