---
title: "Service (api-manager): Request class"
headline: "Service (api-manager): Request class"
sidebarTitle: .Request
sidebarDepth: 0
prev: false
next: false
---

# Service (api-manager): Request class

<a name="request">
</a>

## Request

Service **api-manager**: Request class

**Schema**:

- [Request](#request)
  - [new Request(manager, generator, method, url, options, beforeCb, successCb, errorCb)](#newrequestnew)
  - [.send([options])](#request-send) ⇒ <code>Promise</code>
  - _properties_
    - [.manager](#request-manager) : <code>module:services/api-manager/RequestManager~RequestManager</code>
    - [.generator](#request-generator) : <code>function</code>
    - [.method](#request-method) : <code>string</code>
    - [.url](#request-url) : <code>string</code> \| <code>URL</code>
    - [.options](#request-options) : <code>object</code>

<a name="newrequestnew">
</a>

### new Request(manager, generator, method, url, options, beforeCb, successCb, errorCb)

Create a new Request

| Param | Type | Description |
| --- | --- | --- |
| manager | <code>module:services/api-manager/RequestManager~RequestManager</code> | The request manager instance |
| generator | <code>function</code> | The original request generator (function that returns Promise) |
| method | <code>string</code> | The http verb |
| url | <code>string</code> \| <code>URL</code> | The url |
| options | <code>object</code> | The request options |
| beforeCb | <code>function</code> | The before request callback |
| successCb | <code>function</code> | The success callback |
| errorCb | <code>function</code> | The error callback |

<a name="request-send">
</a>

### request.send([options]) ⇒ <code>Promise</code>

Send the request

**Returns**: <code>Promise</code> - - The request promise

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [options] | <code>object</code> | <code>{}</code> | Options object |
| [options.allowConcurrent] | <code>boolean</code> | <code>false</code> | Push to promise queue even if key exists |

<a name="request-manager">
</a>

### request.manager : <code>module:services/api-manager/RequestManager~RequestManager</code>

The request manager

**Category**: properties  
**Read only**: true  
<a name="request-generator">
</a>

### request.generator : <code>function</code>

The request function

**Category**: properties  
**Read only**: true  
<a name="request-method">
</a>

### request.method : <code>string</code>

The request method

**Category**: properties  
**Read only**: true  
<a name="request-url">
</a>

### request.url : <code>string</code> \| <code>URL</code>

The request url

**Category**: properties  
**Read only**: true  
<a name="request-options">
</a>

### request.options : <code>object</code>

The request options

**Category**: properties  
**Read only**: true  
