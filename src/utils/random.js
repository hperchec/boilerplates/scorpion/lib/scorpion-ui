/**
 * @vuepress
 * ---
 * title: random
 * headline: random
 * sidebarTitle: random
 * ---
 */

// Lodash.sample
import samplesize from 'lodash.samplesize'
import lodashRandom from 'lodash.random'

/**
 * @alias random.number
 * @param {number} [lower] - The lower value
 * @param {number} [upper] - The upper value
 * @param {boolean} [floating] - If uses float numbers
 * @returns {number} Returns the random number
 * @description
 * Generate random number based on lodash.random function.
 *
 * ::: tip See also
 * [lodash.random](https://www.npmjs.com/package/lodash.random) package documentation
 * :::
 * @example
 * number(0, 5) // Return -> 1 (example)
 */
export const number = function (lower, upper, floating) {
  return lodashRandom(lower, upper, floating)
}

/**
 * @alias random.alphaNumeric
 * @param {number} length - The length of returned random string
 * @param {Array|string} [charset] - An array or string of characters
 * @returns {string} Returns the created random string
 * @description
 * Generate random alphanumeric string based on lodash.samplesize function.
 *
 * ::: tip See also
 * [lodash.samplesize](https://www.npmjs.com/package/lodash.samplesize) package documentation
 * :::
 * @example
 * alphaNumeric(5) // Return -> 'Gy85f' (example)
 * alphaNumeric(5, 'ABCDE') // Return -> 'AADBE' (example)
 */
export const alphaNumeric = function (length, charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789') {
  return samplesize(charset, length).join('')
}

/**
 * @alias random
 * @type {object}
 */
export default {
  number,
  alphaNumeric
}
