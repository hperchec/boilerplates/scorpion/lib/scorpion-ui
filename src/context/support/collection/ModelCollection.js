/**
 * @vuepress
 * ---
 * title: "Collection class"
 * headline: "Collection class"
 * sidebarTitle: "Collection class"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: ./
 * next: false
 * ---
 */

import Collection from './Collection'

/**
 * @typicalname Core.context.support.collection.Collection
 * @classdesc
 * > **Ref.** : {@link ../../Core Core}.{@link ../ support}.{@link ./ collection}.{@link ./Collection Collection}
 */
class ModelCollection extends Collection {
  /**
   * Private
   */
  #model

  /**
   * Create a Collection
   * @param {Function} model - The model class
   * @param {Array} [data=[]] - (Optional) An array of any types (Default: [])
   * @param {object} [options] - (Optional) An options object
   */
  constructor (model, data = [], options = {}) {
    super(data, options)
    this.#model = model
  }

  /**
   * Get the collection model
   * @returns {Function} Returns the model class
   */
  getModel () {
    return this.#model
  }

  /**
   * findBy
   * @category methods
   * @param {*} key - The key
   * @returns {*} Returns the found collection item
   * @description
   * Find an item by key in the collection
   */
  findBy (key) {
    // @ts-ignore
    return this.#model.findByPK(key, this.items)
  }

  /**
   * @category methods
   * @param {Function|number|string} key - Same parameter as "find" method
   * @returns {boolean} Returns true if collection contains the item
   * @description
   * Same as "find" but returns boolean
   */
  contains (key) {
    return Boolean(this.find(key))
  }

  /**
   * Replace item in collection
   * @param {*} to - The replacement item
   * @returns {Promise<ModelCollection>} Returns the collection
   */
  async replaceItem (to) {
    // @ts-ignore
    const key = this.#model.getPrimaryKey(to).value
    // @ts-ignore
    return super.replaceItem(key, to)
  }

  /**
   * Update or insert item in collection
   * @param {*} to - The replacement item
   * @returns {Promise<ModelCollection>} Returns the collection
   */
  async upsertItem (to) {
    // @ts-ignore
    const key = this.#model.getPrimaryKey(to).value
    // @ts-ignore
    return super.upsertItem(key, to)
  }

  get toData () {
    const tmp = {}
    for (const item of this.items) {
      // @ts-ignore
      const key = this.#model.getPrimaryKey(item).value
      tmp[key] = item
    }
    return tmp
  }

  /**
   * Static methods
   */

  // ...
}

export default ModelCollection
