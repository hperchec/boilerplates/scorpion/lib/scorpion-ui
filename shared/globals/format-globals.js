/**
 * @param {object} source - Globals object
 * @returns {object}
 * @description
 * Parse globals from json file and returns an object like:
 *
 * ```js
 * {
 *   flat: {
 *     // Each property is "flatten", even nested
 *     __APP_NAME__: ...,
 *     __VERSION_CURRENT__: ...,
 *     ...
 *   },
 *   nested: {
 *     __GLOBALS__: {
 *       // Nested globals...
 *       APP_NAME: ...,
 *       VERSION: { CURRENT: ... },
 *       ...
 *     }
 *   }
 * }
 * ```
 */
function formatGlobals (source) {
  const _flat = {}
  const parse = function (obj, prefix = '') {
    const _tmp = {}
    for (const prop in obj) {
      const value = obj[prop]
      const key = prop.replace(/\s+/g, '_').toUpperCase()
      const flatKey = ((prefix.length > 0) ? (prefix + '_') : '') + key
      if (typeof value === 'object') {
        _tmp[key] = parse(value, key)
      } else {
        _flat[`__${flatKey}__`] = _tmp[key] = (typeof value === 'number') ? `${value}` : `'${value}'`
      }
    }
    return _tmp
  }
  const _nested = parse(source)
  return {
    flat: _flat,
    nested: {
      __GLOBALS__: _nested
    }
  }
}

module.exports = formatGlobals
