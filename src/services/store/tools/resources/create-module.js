import Core from '@/Core'
import { isUndef, deepMerge as merge } from '@/utils'

/**
 * Merge state
 * @param {object} generated - The generated state from generateState method
 * @param {object} raw - The raw state from user
 * @returns {object} The merged state
 */
export function mergeState (generated, raw) {
  const { $data: rawData, loadingStatus: rawLoadingStatus, ...rawState } = raw // eslint-disable-line no-unused-vars
  return {
    $data: generated.$data,
    loadingStatus: merge(generated.loadingStatus, (rawLoadingStatus || {})),
    ...rawState
  }
}

/**
 * Merge getters
 * @param {object} generated - The generated getters from generateGetters method
 * @param {object} raw - The raw getters from user
 * @returns {object} The merged getters
 */
export function mergeGetters (generated, raw) {
  return {
    ...raw,
    ...generated
  }
}

/**
 * Merge actions
 * @param {object} generated - The generated actions from generateGetters method
 * @param {object} raw - The raw actions from user
 * @returns {object} The merged actions
 */
export function mergeActions (generated, raw) {
  return {
    ...raw,
    ...generated
  }
}

/**
 * Merge mutations
 * @param {object} generated - The generated mutations from generateGetters method
 * @param {object} raw - The raw mutations from user
 * @returns {object} The merged mutations
 */
export function mergeMutations (generated, raw) {
  return {
    ...raw,
    ...generated
  }
}

/**
 * Create a resource module object
 * @param {*} model - The model representing the resource
 * @param {object} options - The function options
 * @param {object} rawModule - The raw module to merge
 * @returns {object} Returns the created module
 */
export default function createModule (model, options, rawModule) {
  // Check if model is provided
  if (isUndef(model)) throw new Error('Create resource module: no model provided')
  // Check options
  if (isUndef(options)) throw new Error('Create resource module: no options provided')
  const {
    generateState,
    generateGetters,
    generateActions,
    generateMutations
  } = Core.context.services['store'].tools.resources // eslint-disable-line dot-notation
  // Process options
  const stateOptions = {
    loadingStatus: {}
  }
  if (options.actions) {
    for (const name in options.actions) {
      stateOptions.loadingStatus[name] = true // True just to define loading => false by default (see generateState tool)
    }
  }
  const gettersOptions = options.getters
  const actionsOptions = options.actions
  const mutationsOptions = options.mutations
  // Return module object
  return {
    namespaced: true, // Force namespaced prop
    // Generate 'state' property
    state: mergeState(generateState(model, stateOptions), (rawModule.state || {})),
    // Generate 'getters' property
    getters: mergeGetters(generateGetters(model, gettersOptions), (rawModule.getters || {})),
    // Generate 'actions' property
    actions: mergeActions(generateActions(model, actionsOptions), (rawModule.actions || {})),
    // Generate 'mutations' property
    mutations: mergeMutations(generateMutations(model, mutationsOptions), (rawModule.mutations || {}))
  }
}
