/**
 * @vuepress
 * ---
 * title: "Support/collection"
 * headline: "Support/collection"
 * sidebarTitle: collection
 * prev: ../
 * next: ./Collection
 * ---
 */

import Collection from './Collection'
import ModelCollection from './ModelCollection'

/**
 * @name collection
 * @typicalname Core.context.support.collection
 * @type {object}
 * @description
 * > **Ref.** : {@link ../../Core Core}.{@link ../ support}.{@link ./ collection}
 */
export default {
  /**
   * @alias collection.Collection
   * @type {object}
   * @see {@link ./Collection Collection}
   */
  Collection,
  /**
   * @alias collection.ModelCollection
   * @type {object}
   * @see {@link ./ModelCollection ModelCollection}
   */
  ModelCollection
}
