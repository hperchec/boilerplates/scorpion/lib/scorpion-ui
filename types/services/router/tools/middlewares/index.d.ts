declare namespace _default {
    export { publicMiddleware };
    export { privateMiddleware };
}
export default _default;
import publicMiddleware from './public-middleware';
import privateMiddleware from './private-middleware';
//# sourceMappingURL=index.d.ts.map