/**
 * @vuepress
 * ---
 * title: xor
 * headline: xor
 * sidebarTitle: xor
 * ---
 */

// Lodash.xor
import lodashXor from 'lodash.xor'

/**
 * xor
 * @param {...*} args - Same arguments as lodash.xor
 * @description Sort array of objects (lodash.xor). See https://lodash.com/docs/4.17.15#xor
 * @returns {Array} Returns the array difference
 */
export const xor = function (...args) {
  return lodashXor(...args)
}

export default xor
