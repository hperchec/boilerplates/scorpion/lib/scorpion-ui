---
title: Router - routes
headline: Router - routes
sidebarTitle: .routes
prev: false
next: false
---

# Router - routes

<a name="routes">
</a>

## .routes : <code>object</code>

**Schema**:

- [.routes](#routes) : <code>object</code>
  - [.private](#routes-private) : <code>object</code>
  - [.public](#routes-public) : <code>object</code>

<a name="routes-private">
</a>

### routes.private : <code>object</code>

Private routes

**See**: [private](./private)

<a name="routes-public">
</a>

### routes.public : <code>object</code>

Public routes

**See**: [public](./public)

