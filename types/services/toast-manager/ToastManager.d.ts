export default ToastManager;
declare const ToastManager_base: any;
declare class ToastManager extends ToastManager_base {
    [x: string]: any;
    static reactive: boolean;
    static rootOption: string;
    static aliases: any[];
    static install(_Vue: import("vue/types/umd"), options?: object): void;
    constructor(options: {
        toasters: Toaster[];
    });
    readonly get toasters(): Toaster[];
    add(toaster: Toaster): Toaster;
    toaster(name: string): Toaster;
    toJSON(): object;
}
import Toaster from './Toaster';
//# sourceMappingURL=ToastManager.d.ts.map