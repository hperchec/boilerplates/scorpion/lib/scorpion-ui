/**
 * @vuepress
 * ---
 * title: "Service (local-storage-manager)"
 * headline: "Service (local-storage-manager)"
 * sidebarTitle: local-storage-manager
 * initialOpenGroupIndex: -1 # optional, defaults to 0, defines the index of initially opened subgroup
 * prev: ../
 * next: ./options
 * ---
 */

import Service from '@/Service'

import options from './options'
import LocalStorageManager from './LocalStorageManager'

/* eslint-disable jsdoc/require-param, jsdoc/require-returns */

/**
 * @scorpion_ui_service {LocalStorageManager} local-storage-manager
 * @description
 * The service "local-storage-manager" is for localStorage management.
 */
export default new Service({
  // Service
  service: {
    // Identifier
    identifier: 'local-storage-manager',
    // Register service
    register: function (_Core) {
      // ...
    },
    /**
     * @alias create
     * @description
     * Returns an instance of `LocalStorageManager` created with options.
     */
    create: (serviceOptions, { LocalStorageManager: _LocalStorageManager }, _Core) => {
      // First initialize LocalStorageManager
      _LocalStorageManager.init()
      // Return new LocalStorageManager
      return new _LocalStorageManager(serviceOptions)
    },
    /**
     * @alias plugin
     * @description
     * The following properties will be set to Vue prototype:
     *
     * ```js
     * // Each references to Core.service('local-storage-manager')
     * Vue.prototype.$localStorageManager
     * Vue.prototype.$LS
     * ```
     */
    vuePlugin: ({ LocalStorageManager: _LocalStorageManager }, _Core) => _LocalStorageManager,
    // Vue plugin options
    pluginOptions: {},
    // rootOption
    rootOption: ({ LocalStorageManager: _LocalStorageManager }, _Core) => _LocalStorageManager.rootOption
  },
  // On initialized
  onInitialized: (Core) => {
    // ...
  },
  // Options
  options,
  /**
   * @alias expose
   */
  expose: {
    /**
     * @see {@link ./LocalStorageManager LocalStorageManager}
     */
    LocalStorageManager
  }
})
