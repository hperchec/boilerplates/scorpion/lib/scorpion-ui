---
title: capitalize
headline: capitalize
sidebarTitle: capitalize
---

# capitalize

<a name="capitalize">
</a>

## capitalize ⇒ <code>string</code>

Capitalize a string *([Defined as Vue filter](../context/vue/filters#filters-capitalize))*.::: tip See also[lodash.capitalize](https://www.npmjs.com/package/lodash.capitalize) package documentation:::

**Returns**: <code>string</code> - Returns the "capitalized" string

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| str | <code>string</code> |  | The string to capitalize |
| [limit] | <code>number</code> | <code>-1</code> | The limit for substring to parse (-1: no limit, else int from 1 to X) |

**Example**

```js
capitalize('foo Bar') // => 'Foo bar'capitalize('ping the IP Address', 1) // => 'Ping the IP address'capitalize('ping the IP Address', 9) // => 'Ping the ip Address'
```

