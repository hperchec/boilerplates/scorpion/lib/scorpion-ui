/**
 * @vuepress
 * ---
 * title: Options
 * headline: Options
 * sidebarTitle: Options
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: ./prototype/
 * next: false
 * ---
 */

import Core from '@/Core'
import { isDef } from '@/utils'

/**
 * Vue root instance created hook function
 * @ignore
 */
const createdHook = function () {
  // Init user settings
  this.initUserSettings()
  // Detect device
  const deviceType = this._app.device.deviceType || 'browser'
  this._app.log(`Detected device: ${deviceType}`, { type: 'system' })
  // Created ✔
  this._app.log('Root Vue instance created ✔', { type: 'system' })
}

// Set custom property 'priority' to created hook function to call it before all others defined in mixins
createdHook.priority = 0

/**
 * Log XSS warning message in console
 * @returns {void}
 * @ignore
 */
const consoleLogXSSWarnMessage = () => {
  const xssWarnMessages = [
    // Line 1
    [
      '%c⚠ WARNING: Don\'t paste anything in here. It could give hackers access to your account!',
      // CSS for %c1
      'color: red; font-size: 3rem;'
    ],
    // Line 2
    [
      '%cThis tool is intented to be used by developers, not users!',
      // CSS for %c1
      'color: brown; font-size: 1.8rem;'
    ]
  ]
  for (const msg of xssWarnMessages) {
    console.log(...msg)
  }
}

/**
 * @alias options
 * @typicalname Core.context.vue.root.options
 * @description
 * Vue root instance options
 */
export default {
  /**
   * Custom private option
   * @ignore
   */
  _isAppRootInstance: true,
  /**
   * @type {object[]}
   * @description Vue root instance mixins options
   * @default []
   */
  mixins: [],
  /**
   * @type {object}
   * @description Vue root instance components options
   */
  components: {},
  /**
   * @type {Function}
   * @description
   * Vue root instance data options
   */
  data () {
    return {
      /**
       * @alias options.data.shared
       * @type {object}
       * @description
       * Root instance data: the GlobalMixin will inject reference to this object to all child Vue instance.
       *
       * ::: tip INFO
       * See also [GlobalMixin](../../mixins/GlobalMixin) documentation.
       * :::
       */
      shared: {
        /**
         * @alias options.data.shared.BASE_URL
         * @type {string}
         * @description Get app base URL
         * @default process.env.BASE_URL
         */
        // @ts-ignore
        BASE_URL: process.env.BASE_URL,
        /**
         * @alias options.data.shared.GLOBALS
         * @type {object}
         * @description Access app globals
         * @default Core.config.globals
         */
        GLOBALS: Core.config.globals
      },
      /**
       * @alias options.data.userSettings
       * @type {object}
       * @description
       * User settings object
       */
      userSettings: {
        // Empty by default
      }
    }
  },
  /**
   * @type {object}
   * @description
   * Vue root instance methods
   */
  methods: {
    /**
     * Initialize user settings
     * @returns {void}
     */
    initUserSettings () {
      const defaultUserSettings = this.userSettings
      // Set user settings in local storage if it doesn't exist
      if (!this.$localStorageManager.hasItem('user-settings')) {
        this.$localStorageManager.items['user-settings'] = {}
      }
      for (const key in defaultUserSettings) {
        // If key is not already set in local-storage, set with default value
        if (!isDef(this.$localStorageManager.items['user-settings'][key])) {
          this.$localStorageManager.items['user-settings'][key] = defaultUserSettings[key]
        }
      }
      // Assign userSettings
      this.userSettings = this.$localStorageManager.items['user-settings']
    }
  },
  render (createElement) {
    return createElement(Core.context.vue.components.App, {}, [])
  },
  /**
   * @type {Function}
   * @description
   * Vue root instance beforeCreate hook.
   *
   * All `Core.context.vue.root.prototype.app` object properties will be assigned to `this._app` as read-only.
   *
   * See also: how to customize root instance hooks documentation
   */
  beforeCreate () {
    // Set name
    this.$options.name = Core.config.vue.root.name
    // Set el if autoMount option
    if (Core.config.vue.root.autoMount) {
      this.$options.el = Core.config.vue.root.targetElement
    }
    // Set template
    // this.$options.template = Core.config.vue.root.template // see render function
    // Set App component
    this.$options.components.App = Core.context.vue.components.App
    // 'proxify' function: used to pass 'this' (root Vue instance) reference to each function
    const proxify = (target) => {
      const self = this // root vue instance
      const tmp = new Proxy({}, {
        // Overwrite Object type getter to 'observe'
        get: (obj, prop) => {
          if (prop in obj) {
            // If the module object property is a function, pass the 'this' reference
            if (typeof obj[prop] === 'function') {
              return (...args) => { return obj[prop].call(self, ...args) }
            } else {
              return obj[prop]
            }
          }
          // Else return undefined
          return undefined
        },
        // Overwrite Object type setter
        set: function (obj, prop, value) {
          if (typeof value === 'object') {
            obj[prop] = proxify(value)
          } else {
            obj[prop] = value
          }
          return true
        }
      })
      // Loop on received object properties to start recursivity
      for (const property in target) {
        tmp[property] = target[property]
      }
      return tmp
    }
    // Build '$app' object
    const _app = proxify(Core.context.vue.root.prototype.app)
    // Assign property
    Object.defineProperty(this, '_app', {
      get () {
        return _app
      }
    })
  },
  /**
   * @type {Function}
   * @description
   * Vue root instance created hook
   *
   * See also: how to customize root instance hooks documentation.
   * Custom merge strategy is applied for root instance created hook: other created hooks
   * from mixins will be ADDED AFTER this function
   */
  created: createdHook,
  /**
   * @type {Function}
   * @description
   * Vue root instance mounted hook
   *
   * See also: how to customize root instance hooks documentation
   */
  mounted () {
    this._app.log('Vue root instance mounted:', { type: 'system', prod: false })
    this.$debug.info()
    // Log XSS warning message in console
    consoleLogXSSWarnMessage()
  }
}
