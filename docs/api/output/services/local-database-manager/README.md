---
title: Service (local-database-manager)
headline: Service (local-database-manager)
sidebarTitle: local-database-manager
initialOpenGroupIndex: -1
prev: ../
next: ./options
---

# Service (local-database-manager)

<a name="service"></a>

The service "local-database-manager" is designed to make indexedDB managemnent easier.It references any connection needed and provides methods and tools.

## Create

Returns an instance of `LocalDatabaseManager` created with options.

**Returns**: <code>module:services/local-database-manager/LocalDatabaseManager~LocalDatabaseManager</code> - Once services are created:

```js
Core.service('local-database-manager')
```

## Vue

### Plugin

On registering, service Vue plugin will be set in `Core.context.vue.plugins['local-database-manager']`.

Plugin will be automatically used by Vue.

The following properties will be set to Vue prototype:```js// Each references to Core.service('local-database-manager')Vue.prototype.$localDatabaseManagerVue.prototype.$localDB```

### Root instance

<a name="app"></a>

#### Prototype $app property

On registering, the service will inject properties to \`Core.context.vue.root.prototype.app\`.

> Properties will be shared by all components under \`vm.$app\` where \`vm\` is the component instance.

<a name="app-usersettings"></a>

##### User settings

Service will define the following user settings keys:

##### Others

## Expose (service context)

The service will expose the following properties under `Core.context.services['local-database-manager']`:
<a name="expose-databases">
</a>

### .databases : <code>object</code>

> Exposed as `Core.context.services['local-database-manager'].databases`

Default is an empty object

<a name="expose-newlocaldatabase">
</a>

### .newLocalDatabase

> Exposed as `Core.context.services['local-database-manager'].newLocalDatabase`

**See**: [newLocalDatabase](./new-local-database)

<a name="expose-localdatabase">
</a>

### .LocalDatabase

> Exposed as `Core.context.services['local-database-manager'].LocalDatabase`

**See**: [LocalDatabase](./LocalDatabase)

<a name="expose-localdatabasemanager">
</a>

### .LocalDatabaseManager

> Exposed as `Core.context.services['local-database-manager'].LocalDatabaseManager`

**See**: [LocalDatabaseManager](./LocalDatabaseManager)

<a name="expose-localdatabaseoptions">
</a>

### .LocalDatabaseOptions

> Exposed as `Core.context.services['local-database-manager'].LocalDatabaseOptions`

**See**: [LocalDatabaseOptions](./LocalDatabaseOptions)

