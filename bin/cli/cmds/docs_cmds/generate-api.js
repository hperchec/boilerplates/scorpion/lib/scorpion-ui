const fs = require('fs')
const path = require('path')
const juisy = require('@hperchec/juisy')

const {
  rootDir,
  $style,
  log,
  error,
  abort,
  step,
  substep
} = juisy.utils

const generateAPIDoc = require('../../lib/docs/generate-api-doc')

// Exports command object
module.exports = {
  /**
   * Command syntax
   */
  command: 'generate:api',
  /**
   * Aliases
   */
  aliases: [],
  /**
   * Command description
   */
  describe: 'Generate docs from source code with vuepress-jsdoc',
  /**
   * Builder
   * @param {Object} yargs
   * @return {Object}
   */
  builder: function (yargs) {
    yargs.option('c', {
      alias: 'config',
      type: 'string',
      describe: 'Path to custom config file relative to root folder (default: "./docs.config.js")',
      requiresArg: true
    })
    yargs.option('d', {
      alias: 'dev',
      type: 'boolean',
      describe: 'Enables the "watch mode" of vuepress-jsdoc.',
      default: false
    })
    return yargs
  },
  /**
   * Handler
   * @param {Object} argv - The argv
   * @return {void}
   */
  handler: async function (argv) {
    let options = {}
    let configPath = 'docs.config.js' // default relative to root folder

    /**
     * Process config option
     */
    step('Configuration')
    if (argv.config) {
      configPath = path.resolve(rootDir, argv.config)
      // Check if file exist
      if (fs.existsSync(configPath)) {
        options = require(configPath)
      } else {
        // Else log error and exit
        error(`Can't find custom config file: "${configPath}"`)
        abort(1)
      }
    } else {
      // Else -> no custom config. Check if default 'docs.config.js' exists
      if (fs.existsSync(configPath)) {
        options = require(configPath)
      } else {
        // Just log info
        substep($style.yellow('No config file "docs.config.js" found in root folder. Applying default config...\n'))
      }
    }
    substep($style.green('✔ Successfuly configured'), { last: true })
    log() // blank line

    /**
     * If everything is okay, call generateAPIDoc function
     */
    await generateAPIDoc(options, argv.dev)
  }
}
