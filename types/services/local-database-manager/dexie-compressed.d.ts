export function applyCompressionMiddleware(database: Dexie, options: {
    tables?: object;
    defaultEngine?: {
        compress?: Function;
        decompress?: Function;
    };
}): Dexie;
export default applyCompressionMiddleware;
import Dexie from 'dexie';
//# sourceMappingURL=dexie-compressed.d.ts.map