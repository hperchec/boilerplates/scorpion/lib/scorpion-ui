---
title: isUndef
headline: isUndef
sidebarTitle: isUndef
---

# isUndef

<a name="isundef">
</a>

## isUndef ⇒ <code>boolean</code>

Return true if variable is undefined (=== undefined)

**Returns**: <code>boolean</code> - Returns true if value is undefined

| Param | Type | Description |
| --- | --- | --- |
| value | <code>\*</code> | The variable to check |

**Example**

```js
isUndef(1) // => falseisUndef(undefined) // => true
```

