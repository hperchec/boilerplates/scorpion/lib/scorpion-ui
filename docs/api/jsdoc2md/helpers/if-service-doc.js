const minimatch = require('minimatch')
const ddata = require('dmd/helpers/ddata')

// block helpers
exports.ifServiceDoc = ifServiceDoc
exports.ifServiceVueDoc = ifServiceVueDoc
exports.exposeContext = exposeContext
// helpers
exports.hasPluginDoc = hasPluginDoc
exports.hasRootInstanceDoc = hasRootInstanceDoc
exports.hasSharedDoc = hasSharedDoc
exports.hasAppDoc = hasAppDoc
exports.hasUserSettingsDoc = hasUserSettingsDoc

/**
 * Block helper: conditional if service doc
 * @param {object} options - Handlebars options
 * @returns {string} Returns options.fn or options.inverse
 */
function ifServiceDoc (options) {
  if (minimatch(this.options.files[0], '**/src/services/*/index.js')) {
    const serviceRootDoclet = this.find((value, index) => value.id === 'service')
    const serviceIdentifier = serviceRootDoclet.customTags.find((value) => value.tag === 'service_identifier').value
    this.options.serviceDoc = {
      identifier: serviceIdentifier
    }
    return options.fn(this)
  } else {
    return options.inverse(this)
  }
}

/**
 * Block helper: conditional if service has "Vue" specific docs
 * @param {object} options - Handlebars options
 * @returns {string} Returns options.fn or options.inverse
 */
function ifServiceVueDoc (options) {
  if (hasPluginDoc(this) || // has "plugin" doc
    hasRootInstanceDoc(this) // or has any "root instance" doc (like "shared", "$app", etc...)
  ) {
    return options.fn(this)
  } else {
    return options.inverse(this)
  }
}

/**
 * Block helper: modify expose member context to process it as static global
 * @param {object} options - Handlebars options
 * @returns {string} Returns options.fn or options.inverse
 */
function exposeContext (options) {
  const serviceIdentifier = options.data.root.options.serviceDoc.identifier
  const propertyString = this.name.match(/^[a-zA-Z_$][a-zA-Z0-9_$]*$/) ? `.${this.name}` : `['${this.name}']`
  this.kind = 'global'
  this.memberof = undefined
  this.description = `> Exposed as \`Core.context.services['${serviceIdentifier}']${propertyString}\`` +
    (this.description ? `\n\n${this.description}` : '')
  return options.fn(this)
}

/**
 * Helper: returns true if service has "plugin" documented
 * @param {Array|object} identifiers - The identifiers from handlebars data
 * @returns {boolean} Returns true if service has "plugin" documented
 */
function hasPluginDoc (identifiers) {
  const _identifiers = !Array.isArray(identifiers)
    ? ddata._identifiers(identifiers)
    : identifiers
  return Boolean(_identifiers.find((value, index) => value.id === 'plugin'))
}

/**
 * Helper: returns true if service has any root instance property documented
 * @param {Array|object} identifiers - The identifiers from handlebars data
 * @returns {boolean} Returns true if service has any root instance property documented
 */
function hasRootInstanceDoc (identifiers) {
  return hasSharedDoc(identifiers) ||
    hasAppDoc(identifiers)
}

/**
 * Helper: returns true if service has "shared" documented
 * @param {Array|object} identifiers - The identifiers from handlebars data
 * @returns {boolean} Returns true if service has "shared" documented
 */
function hasSharedDoc (identifiers) {
  const _identifiers = !Array.isArray(identifiers)
    ? ddata._identifiers(identifiers)
    : identifiers
  return Boolean(_identifiers.find((value, index) => value.id.startsWith('shared.')))
}

/**
 * Helper: returns true if service has "$app" documented
 * @param {Array|object} identifiers - The identifiers from handlebars data
 * @returns {boolean} Returns true if service has "$app" documented
 */
function hasAppDoc (identifiers) {
  if (hasUserSettingsDoc(identifiers)) {
    return true
  } else {
    const _identifiers = !Array.isArray(identifiers)
      ? ddata._identifiers(identifiers)
      : identifiers
    return Boolean(_identifiers.find((value, index) => value.id.startsWith('$app.')))
  }
}

/**
 * Helper: returns true if service has "$app.userSettings" documented
 * @param {Array|object} identifiers - The identifiers from handlebars data
 * @returns {boolean} Returns true if service has "$app.userSettings" documented
 */
function hasUserSettingsDoc (identifiers) {
  const _identifiers = !Array.isArray(identifiers)
    ? ddata._identifiers(identifiers)
    : identifiers
  return Boolean(_identifiers.find((value, index) => value.id === '$app.userSettings'))
}
