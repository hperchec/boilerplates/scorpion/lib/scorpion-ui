---
title: "Service (notification-manager): NotificationManager class"
headline: "Service (notification-manager): NotificationManager class"
sidebarTitle: .NotificationManager
sidebarDepth: 0
prev: false
next: false
---

# Service (notification-manager): NotificationManager class

<a name="notificationmanager">
</a>

## NotificationManager ⇐ <code>VuePluginable</code>

Service **notification-manager**: NotificationManager class

**Extends**: <code>VuePluginable</code>  
**Schema**:

- [NotificationManager](#notificationmanager) ⇐ <code>VuePluginable</code>
  - [new NotificationManager(options)](#newnotificationmanagernew)
  - _instance_
    - [.reactive](#notificationmanager-reactive)
    - [.toJSON()](#notificationmanager-tojson) ⇒ <code>object</code>
    - _methods_
      - [.new(title, [options])](#notificationmanager-new) ⇒ <code>Notification</code>
      - [.getPermission()](#notificationmanager-getpermission) ⇒ <code>string</code>
      - [.askNotificationPermission()](#notificationmanager-asknotificationpermission) ⇒ <code>Promise.&lt;string&gt;</code>
  - _static_
    - [.install(_Vue, [options])](#notificationmanager-install)

<a name="newnotificationmanagernew">
</a>

### new NotificationManager(options)

Create a new NotificationManager

| Param | Type | Description |
| --- | --- | --- |
| options | <code>object</code> | Constructor options |

<a name="notificationmanager-reactive">
</a>

### notificationManager.reactive

Overrides static VuePluginable properties

<a name="notificationmanager-tojson">
</a>

### notificationManager.toJSON() ⇒ <code>object</code>

Overrides the default toJSON object method for JSON.stringify() calls

**Returns**: <code>object</code> - - The instance data to be serialized

<a name="notificationmanager-new">
</a>

### notificationManager.new(title, [options]) ⇒ <code>Notification</code>

Creates a new NotificationSee https://developer.mozilla.org/fr/docs/Web/API/Notification/Notification

**Returns**: <code>Notification</code> - - The Notification instance

**Category**: methods  
| Param | Type | Default | Description |
| --- | --- | --- | --- |
| title | <code>string</code> |  | Same as Notification constructor |
| [options] | <code>object</code> | <code>{}</code> | Same as Notification constructor plus the following |
| [options.audio] | <code>Audio</code> |  | An Audio instance for notification sound |
| [options.onClick] | <code>function</code> |  | A click event handler |
| [options.onClose] | <code>function</code> |  | A close event handler |
| [options.onError] | <code>function</code> |  | An error handler |
| [options.onShow] | <code>function</code> |  | A callback to call when notification shows |

<a name="notificationmanager-getpermission">
</a>

### notificationManager.getPermission() ⇒ <code>string</code>

Get the value for notifications permission. Can be 'denied', 'granted' or 'default'See https://developer.mozilla.org/fr/docs/Web/API/Notification/permission_static

**Returns**: <code>string</code> - - The value of Notification.permission

**Category**: methods  
<a name="notificationmanager-asknotificationpermission">
</a>

### notificationManager.askNotificationPermission() ⇒ <code>Promise.&lt;string&gt;</code>

Ask for permission for notifications

**Returns**: <code>Promise.&lt;string&gt;</code> - - The value of Notification.permission depending on what the user answers

**Category**: methods  
<a name="notificationmanager-install">
</a>

### NotificationManager.install(_Vue, [options])

Install method for Vue

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| _Vue | <code>Vue</code> |  | Vue |
| [options] | <code>object</code> | <code>{}</code> | The options of the plugin |

