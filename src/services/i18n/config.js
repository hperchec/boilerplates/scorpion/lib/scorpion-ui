/**
 * @vuepress
 * ---
 * title: "Service (i18n) configuration"
 * headline: "Service (i18n) configuration"
 * sidebarTitle: Configuration
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * ⚠ THIS FILE IS ONLY USED TO GENERATE SERVICE CONFIGURATION DOCUMENTATION
 */

/**
 * @name Configuration
 * @typicalname Core.config.services['i18n']
 * @description
 * Service configuration
 *
 * > Refers to `Core.config.services['i18n']`
 *
 * ```js
 * 'i18n': {
 *   // The available locales.
 *   availableLocales: String[],
 *   // Default locale. VueI18n `locale` option.
 *   defaultLocale: String,
 *   // Fallback locale. VueI18n `fallbackLocale` option.
 *   fallbackLocale: String
 * }
 * ```
 */
const config = { // eslint-disable-line no-unused-vars
  /**
   * @alias Configuration.availableLocales
   * @type {string[]}
   * @description
   * An array of available locales.
   */
  availableLocales: [],
  /**
   * @alias Configuration.defaultLocale
   * @type {string}
   * @description
   * The default locale.
   */
  defaultLocale: 'en',
  /**
   * @alias Configuration.fallbackLocale
   * @type {string}
   * @description
   * The fallback locale.
   */
  fallbackLocale: 'en'
}
