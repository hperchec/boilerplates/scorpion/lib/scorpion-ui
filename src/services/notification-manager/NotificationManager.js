/**
 * @vuepress
 * ---
 * title: "Service (notification-manager): NotificationManager class"
 * headline: "Service (notification-manager): NotificationManager class"
 * sidebarTitle: .NotificationManager
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import Core from '@/Core'
import { getAllKeys } from '@/utils'
import toolbox from '@/toolbox'

const { VuePluginable } = toolbox.vue

/**
 * @extends VuePluginable
 * @classdesc
 * Service **notification-manager**: NotificationManager class
 */
class NotificationManager extends VuePluginable {
  /**
   * Create a new NotificationManager
   * @param {object} options - Constructor options
   */
  constructor (options) { // eslint-disable-line no-useless-constructor
    // Call VuePluginable constructor
    super()
    // Set debugging private property
    if (this.__v_descriptor__) {
      this.__v_descriptor__.namespace = 'service:notification-manager'
      this.__v_descriptor__.description = 'Access the NotificationManager instance'
      // @ts-ignore
      this.__v_descriptor__.externalLink = __getDocUrl__('/api/services/notification-manager/NotificationManager')
    }
  }

  /**
   * Overrides static VuePluginable properties
   */
  static reactive = true
  static rootOption = 'notificationManager'
  static aliases = []

  /**
   * Accessors & mutators
   */

  // ...

  /**
   * Methods
   */

  /**
   * @category methods
   * @param {string} title - Same as Notification constructor
   * @param {object} [options = {}] - Same as Notification constructor plus the following
   * @param {Audio} [options.audio] - An Audio instance for notification sound
   * @param {Function} [options.onClick] - A click event handler
   * @param {Function} [options.onClose] - A close event handler
   * @param {Function} [options.onError] - An error handler
   * @param {Function} [options.onShow] - A callback to call when notification shows
   * @returns {Notification} - The Notification instance
   * @description
   * Creates a new Notification
   *
   * See https://developer.mozilla.org/fr/docs/Web/API/Notification/Notification
   */
  new (title, options = {}) {
    options = {
      // Default options for Notification
      // @ts-ignore
      lang: Core.service('i18n').locale,
      badge: '', // img url as string
      body: '', // body (message content) as string
      tag: '', // notification tag
      icon: '', // icon notification url as string
      data: {}, // our custom data to pass to notification
      vibrate: [ 200, 200, 200, 200, 200 ], // vibrate 200ms, not vibrate 200ms, etc...
      renotify: false,
      requireInteraction: false,
      // actions: [], // @todo ? Not implemented in firefox
      silent: false,
      ...options
    }
    // @ts-ignore
    const notification = new Notification(title, options)
    // Set click handler
    notification.onclick = (evt) => {
      if (options.onClick) options.onClick(evt)
    }
    // Set close handler
    notification.onclose = (evt) => {
      if (options.onClose) options.onClose(evt)
    }
    // Set error handler
    // @todo: non-blocking error
    // defining onerror callback will throw unexpected error in Private navigation windows
    notification.onerror = (evt) => {
      console.error('NotificationManager: error caught on new Notification: ', evt)
      if (options.onError) options.onError(evt)
    }
    // Set show handler
    notification.onshow = (evt) => {
      if (options.onShow) options.onShow(evt)
    }
    // Play set sound
    if (options.audio && options.audio instanceof Audio) {
      options.audio.play()
    }
    return notification
  }

  /**
   * @category methods
   * @returns {string} - The value of Notification.permission
   * @description
   * Get the value for notifications permission. Can be 'denied', 'granted' or 'default'
   *
   * See https://developer.mozilla.org/fr/docs/Web/API/Notification/permission_static
   */
  getPermission () {
    return Notification.permission
  }

  /**
   * @category methods
   * @returns {Promise<string>} - The value of Notification.permission depending on what the user answers
   * @description
   * Ask for permission for notifications
   */
  askNotificationPermission () {
    // Check whether browser supports the promise version of requestPermission()
    // Safari only supports the old callback-based version
    const checkNotificationPromise = () => {
      try {
        Notification.requestPermission().then()
      } catch (e) {
        return false
      }
      return true
    }

    return new Promise((resolve, reject) => {
      // Function to actually ask the permissions
      // (code from mdn examples) See also: https://github.com/mdn/dom-examples/blob/main/to-do-notifications/scripts/todo.js#L260
      const handlePermission = (permission) => {
        // Whatever the user answers, we make sure Chrome stores the information
        if (!Reflect.has(Notification, 'permission')) {
          // @ts-ignore
          Notification.permission = permission
        }
        resolve(this.getPermission())
      }
      // Check if the browser supports notifications
      if (!Reflect.has(window, 'Notification')) {
        reject(new Error('This browser does not support notifications.'))
      } else {
        if (checkNotificationPromise()) {
          Notification.requestPermission().then(handlePermission)
        } else {
          Notification.requestPermission(handlePermission)
        }
      }
    })
  }

  /**
   * Overrides the default toJSON object method for JSON.stringify() calls
   * @returns {object} - The instance data to be serialized
   */
  toJSON () {
    const keys = getAllKeys(this, {
      excludeKeys: [
        /^_/, // private properties that starts with '_'
        'toJSON',
        'toString',
        'constructor'
      ]
    })
    const jsonObj = keys.reduce((obj, key) => {
      obj[key] = this[key]
      return obj
    }, {})
    return jsonObj
  }

  /**
   * Private properties
   */

  // ...

  /**
   * Private methods
   */

  // ...

  /**
   * Static
   */

  /**
   * Install method for Vue
   * @param {Vue} _Vue - Vue
   * @param {object} [options = {}] - The options of the plugin
   */
  static install (_Vue, options = {}) {
    const Vue = _Vue
    super.install(Vue)
  }
}

export default NotificationManager
