/**
 * @vuepress
 * ---
 * title: getTimestamp
 * headline: getTimestamp
 * sidebarTitle: getTimestamp
 * ---
 */

/**
 * getTimestamp
 * @param {Date} date - The date to parse
 * @returns {number} Returns timestamp
 * @description
 * Parses date and returns timestamp
 * @example
 * const date = new Date()
 * getTimestamp(date) // => return time
 */
export const getTimestamp = function (date) {
  return date.getTime()
}

export default getTimestamp
