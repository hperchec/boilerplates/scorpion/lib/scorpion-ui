---
title: "Service (toast-manager): Toaster class"
headline: "Service (toast-manager): Toaster class"
sidebarTitle: .Toaster
sidebarDepth: 0
prev: false
next: false
---

# Service (toast-manager): Toaster class

<a name="toaster">
</a>

## Toaster

Service **toast-manager**: Toaster class

**Schema**:

- [Toaster](#toaster)
  - [new Toaster(options)](#newtoasternew)
  - _instance_
    - [.toJSON()](#toaster-tojson) ⇒ <code>object</code>
    - _async methods_
      - [.beforePublish(...args)](#toaster-beforepublish) ⇒ <code>Promise.&lt;void&gt;</code>
      - [.afterPublish(...args)](#toaster-afterpublish) ⇒ <code>Promise.&lt;void&gt;</code>
      - [.beforeDelete(...args)](#toaster-beforedelete) ⇒ <code>Promise.&lt;void&gt;</code>
      - [.afterDelete(...args)](#toaster-afterdelete) ⇒ <code>Promise.&lt;void&gt;</code>
      - [.delete(id)](#toaster-delete) ⇒ <code>Promise.&lt;boolean&gt;</code>
    - _methods_
      - [.setToasts(value)](#toaster-settoasts) ⇒ <code>void</code>
      - [.setBeforePublish(value)](#toaster-setbeforepublish) ⇒ <code>void</code>
      - [.setAfterPublish(value)](#toaster-setafterpublish) ⇒ <code>void</code>
      - [.setBeforeDelete(value)](#toaster-setbeforedelete) ⇒ <code>void</code>
      - [.setAfterDelete(value)](#toaster-setafterdelete) ⇒ <code>void</code>
      - [.publish(toastOptions)](#toaster-publish) ⇒ <code>Promise.&lt;module:services/toast-manager/Toast~Toast&gt;</code>
    - _properties_
      - [.name](#toaster-name) : <code>string</code>
      - [.toasts](#toaster-toasts) : <code>module:context/support/collection/Collection~Collection</code>
  - _static_
    - _async methods_
      - [.beforePublish(toast, toaster, toastOptions)](#toaster-beforepublish) ⇒ <code>Promise.&lt;void&gt;</code>
      - [.afterPublish(toast, toaster, toastOptions)](#toaster-afterpublish) ⇒ <code>Promise.&lt;void&gt;</code>
      - [.beforeDelete(toast, toaster)](#toaster-beforedelete) ⇒ <code>Promise.&lt;void&gt;</code>
      - [.afterDelete(toast, toaster)](#toaster-afterdelete) ⇒ <code>Promise.&lt;void&gt;</code>
    - _properties_
      - [.defaultOptions](#toaster-defaultoptions) : <code>object</code>

<a name="newtoasternew">
</a>

### new Toaster(options)

Create a new Toaster

| Param | Type | Description |
| --- | --- | --- |
| options | <code>object</code> | A toaster option object |
| options.name | <code>string</code> | Name of Toaster |
| [options.toasts] | <code>module:context/support/collection/Collection~Collection</code> | A Collection of Toast object |
| [options.beforePublish] | <code>function</code> | A function to call before publish toast |
| [options.afterPublish] | <code>function</code> | A function to call after publish toast |
| [options.beforeDelete] | <code>function</code> | A function to call before delete toast |
| [options.afterDelete] | <code>function</code> | A function to call after delete toast |

<a name="toaster-tojson">
</a>

### toaster.toJSON() ⇒ <code>object</code>

Overrides the default toJSON object method for JSON.stringify() calls

**Returns**: <code>object</code> - - The instance data to be serialized

<a name="toaster-beforepublish">
</a>

### toaster.beforePublish(...args) ⇒ <code>Promise.&lt;void&gt;</code>

beforePublish method- if customized hook defined, call it with same arguments- else, call static class method beforePublish with same argumentsSee Toaster `beforePublish` static method.

**Category**: async methods  
| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | The beforePublish function arguments |

<a name="toaster-afterpublish">
</a>

### toaster.afterPublish(...args) ⇒ <code>Promise.&lt;void&gt;</code>

afterPublish method- if customized hook defined, call it with same arguments- else, call static class method afterPublish with same argumentsSee Toaster `afterPublish` static method.

**Category**: async methods  
| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | The afterPublish function arguments |

<a name="toaster-beforedelete">
</a>

### toaster.beforeDelete(...args) ⇒ <code>Promise.&lt;void&gt;</code>

beforeDelete method- if customized hook defined, call it with same arguments- else, call static class method beforeDelete with same argumentsSee Toaster `beforeDelete` static method.

**Category**: async methods  
| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | The beforeDelete function arguments |

<a name="toaster-afterdelete">
</a>

### toaster.afterDelete(...args) ⇒ <code>Promise.&lt;void&gt;</code>

afterDelete method- if customized hook defined, call it with same arguments- else, call static class method afterDelete with same argumentsSee Toaster `afterDelete` static method.

**Category**: async methods  
| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | The afterDelete function arguments |

<a name="toaster-delete">
</a>

### toaster.delete(id) ⇒ <code>Promise.&lt;boolean&gt;</code>

Delete method

**Returns**: <code>Promise.&lt;boolean&gt;</code> - Returns true after deleting

**Category**: async methods  
| Param | Type | Description |
| --- | --- | --- |
| id | <code>string</code> | The toast id |

<a name="toaster-settoasts">
</a>

### toaster.setToasts(value) ⇒ <code>void</code>

Set toasts

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| value | <code>module:context/support/collection/Collection~Collection</code> | The toast collection |

<a name="toaster-setbeforepublish">
</a>

### toaster.setBeforePublish(value) ⇒ <code>void</code>

Set beforePublish

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| value | <code>function</code> | The function to call before publish |

<a name="toaster-setafterpublish">
</a>

### toaster.setAfterPublish(value) ⇒ <code>void</code>

Set afterPublish

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| value | <code>function</code> | The function to call after publish |

<a name="toaster-setbeforedelete">
</a>

### toaster.setBeforeDelete(value) ⇒ <code>void</code>

Set beforeDelete

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| value | <code>function</code> | The function to call before publish |

<a name="toaster-setafterdelete">
</a>

### toaster.setAfterDelete(value) ⇒ <code>void</code>

Set afterDelete

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| value | <code>function</code> | The function to call after publish |

<a name="toaster-publish">
</a>

### toaster.publish(toastOptions) ⇒ <code>Promise.&lt;module:services/toast-manager/Toast~Toast&gt;</code>

Publish a toast

**Returns**: <code>Promise.&lt;module:services/toast-manager/Toast~Toast&gt;</code> - Returns the published Toast

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| toastOptions | <code>object</code> | The options of the toast that will be created via new Toast() |

<a name="toaster-name">
</a>

### toaster.name : <code>string</code>

Toaster name

**Category**: properties  
<a name="toaster-toasts">
</a>

### toaster.toasts : <code>module:context/support/collection/Collection~Collection</code>

Toaster toasts

**Category**: properties  
**Read only**: true  
<a name="toaster-beforepublish">
</a>

### Toaster.beforePublish(toast, toaster, toastOptions) ⇒ <code>Promise.&lt;void&gt;</code>

Default beforePublish function (do nothing by default)

**Category**: async methods  
| Param | Type | Description |
| --- | --- | --- |
| toast | <code>module:services/toast-manager/Toast~Toast</code> | The toast |
| toaster | <code>module:services/toast-manager/Toaster~Toaster</code> | The toaster |
| toastOptions | <code>object</code> | The original toast options |

<a name="toaster-afterpublish">
</a>

### Toaster.afterPublish(toast, toaster, toastOptions) ⇒ <code>Promise.&lt;void&gt;</code>

Default afterPublish function (do nothing by default)

**Category**: async methods  
| Param | Type | Description |
| --- | --- | --- |
| toast | <code>module:services/toast-manager/Toast~Toast</code> | The toast |
| toaster | <code>module:services/toast-manager/Toaster~Toaster</code> | The toaster |
| toastOptions | <code>object</code> | The original toast options |

<a name="toaster-beforedelete">
</a>

### Toaster.beforeDelete(toast, toaster) ⇒ <code>Promise.&lt;void&gt;</code>

Default afterPublish function (do nothing by default)

**Category**: async methods  
| Param | Type | Description |
| --- | --- | --- |
| toast | <code>module:services/toast-manager/Toast~Toast</code> | The toast |
| toaster | <code>module:services/toast-manager/Toaster~Toaster</code> | The toaster |

<a name="toaster-afterdelete">
</a>

### Toaster.afterDelete(toast, toaster) ⇒ <code>Promise.&lt;void&gt;</code>

Default afterPublish function (do nothing by default)

**Category**: async methods  
| Param | Type | Description |
| --- | --- | --- |
| toast | <code>module:services/toast-manager/Toast~Toast</code> | The toast |
| toaster | <code>module:services/toast-manager/Toaster~Toaster</code> | The toaster |

<a name="toaster-defaultoptions">
</a>

### Toaster.defaultOptions : <code>object</code>

Default options

**Category**: properties  
**Read only**: true  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The toaster name (default: `undefined`) |
| toasts | <code>module:context/support/collection/Collection~Collection</code> | A collection of toast objects (default: `undefined`) |
| beforePublish | <code>function</code> | The beforePublish hook function (default: `Toaster.beforePublish`) |
| afterPublish | <code>function</code> | The afterPublish hook function (default: `Toaster.afterPublish`) |
| beforeDelete | <code>function</code> | The beforeDelete hook function (default: `Toaster.beforeDelete`) |
| afterDelete | <code>function</code> | The afterDelete hook function (default: `Toaster.afterDelete`) |

