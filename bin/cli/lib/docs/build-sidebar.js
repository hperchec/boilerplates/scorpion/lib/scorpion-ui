const path = require('path')
const fs = require('fs')
const { parseFrontmatter } = require('@vuepress/shared-utils')

/**
 * @type {Map<string, object>}
 */
const cache = new Map()

/**
 * get content with cache
 * @param {string} fileUrl - fileUrl
 * @returns {object} Returns the cached file object
 */
function getContent (fileUrl) {
  if (cache.has(fileUrl)) {
    return cache.get(fileUrl)
  }
  // Read file content
  const content = fs.readFileSync(fileUrl, { encoding: 'utf-8' })
  // Get frontmatter
  const frontmatter = parseFrontmatter(content).data
  // Object
  const cacheObj = {
    fileUrl: fileUrl,
    content: {
      frontmatter: frontmatter,
      raw: content
    }
  }
  cache.set(fileUrl, cacheObj)
  return cacheObj
}

/**
 * Generate custom sidebar based on vuepress-jsdoc auto-generated sidebar
 * @param {object} generatedSidebar - The vuepress-jsdoc auto-generated sidebar
 * @param {string} outputPath - The output dir path
 * @param {string} pathPrefix - The path prefix
 * @returns {object} Returns the sidebar object
 */
module.exports = function buildSidebar (generatedSidebar, outputPath, pathPrefix) {
  /**
   * Build sidebar item
   * @param {object} item - item object from generated sidebarTree
   * @param {string} item.title - item title
   * @param {string} item._name - item name
   * @param {string[]} item._children - Array of children
   * @param {string} item._parent - Name of parent item
   * @returns {object} Returns a sidebar item
   */
  function buildItem (item) {
    // Get file path
    let filePath = path.posix.join(outputPath, `${item._parent}/${item._name}.md`)
    // Item path
    const itemPath = path.posix.join(pathPrefix, item._parent + (item._name === '__index__' ? '/' : `/${item._name}`))
    // Check if file exists (if not a directory)
    if (fs.existsSync(filePath)) {
      const basename = path.basename(filePath)
      const file = getContent(filePath)
      item.title = file.content.frontmatter.sidebarTitle || item.title
      item.initialOpenGroupIndex = file.content.frontmatter.initialOpenGroupIndex || 0 // default
      // Set path
      item.path = itemPath
      // Rename if basename is like "index.md" (case insensitive), add an underscore ('_') at begin
      if (/^(index\.md)$/i.test(basename)) {
        item.path = item.path.split('/')
        item.path[item.path.length - 1] = `_${item._name}`
        item.path = item.path.join('/')
      }
    } else {
      // Else -> directory
      filePath = path.posix.join(outputPath, `${item._parent}/${item._name}`)
    }
    // Children
    if (item._children) {
      const tmp = [] // init empty array
      // Loop on children
      for (const i of item._children) {
        if (!(item.children instanceof Array)) item.children = []
        // If index is found, get title in frontmatter
        if (i.includes(`${item._name}/__index__`)) {
          const file = getContent(path.posix.join(filePath, '__index__.md'))
          const titleFromIndex = file.content.frontmatter.sidebarTitle || item.title
          item.title = titleFromIndex !== '__index__'
            ? titleFromIndex
            : item.title
          item.path = itemPath + '/'
          item.initialOpenGroupIndex = file.content.frontmatter.initialOpenGroupIndex || 0 // default
        } else { // Else, build each child item
          // Split and remove first array item that represents the parent
          const childPath = i.split('/').slice(1).join('/')
          const childItem = {
            title: childPath.split('/')[0],
            _name: childPath.split('/')[0],
            _children: [],
            _parent: path.posix.join(item._parent, item._name)
          }
          // Check if a children already exists
          let childIndex = tmp.findIndex((c) => c._name === childItem._name)
          const childExists = childIndex !== -1
          if (!childExists) { // If does not exist
            // Push children
            tmp.push(childItem)
            // Retrieve index of newly created child
            childIndex = tmp.findIndex((c) => c._name === childItem._name)
          }
          // Push new string item in _children
          if (childPath.split('/').length > 1) { // Prevent empty array
            tmp[childIndex]._children.push(childPath)
          }
        }
      }
      // Loop again
      for (const i of tmp) {
        // recursive call
        item.children.push(buildItem(i))
      }
    }

    // Remove temporary properties
    delete item._name
    delete item._children
    delete item._parent

    return item
  }

  // Remlove and parse first item that is an auto-generated object like:
  // {
  //   title: 'API',
  //   children: [ [ '', 'Mainpage' ], 'Core', '__index__' ]
  // }
  const firstAutoGenItem = generatedSidebar.shift()
  // Get root level children that are not directories or '__index__' file
  const rootLevelChildren = firstAutoGenItem.children.filter((value, index) => {
    return typeof value === 'string' && value !== '__index__'
  }).map((val) => ({ title: val }))

  // For each 'first level' item of the vuepress-jsdoc generated sidebar
  const sidebar = generatedSidebar.concat(rootLevelChildren).map(item => {
    // First, loop on children and format path string
    if (item.children) {
      // Children are string that contains the path like:
      // ex: '<first_level_folder>\nested_1\nested_2\...'
      item.children = item.children.map((filePath) => {
        // Read path
        filePath = filePath.replace(/\\/g, '/') // Prevent 'windows' path format
        // Return filePath: '<first_level_folder>/nested_1/nested_2/...'
        return filePath
      })
    }

    return buildItem({
      title: item.title,
      _name: item.title,
      _children: item.children,
      _parent: ''
    })
  })

  return sidebar
}
