/**
 * DeviceManagerMixin
 */
export default {
  computed: {
    /**
     * @returns {boolean} True if mobile, false otherwise
     */
    IS_MOBILE: function () {
      return this.$app.device.isMobile
    }
  }
}
