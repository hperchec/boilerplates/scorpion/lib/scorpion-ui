import Core from '@/Core'

/**
 * **BaseModal** component
 */
export default {
  name: 'BaseModal',
  components: {
    Modal: (resolve) => resolve(Core.context.vue.components.modals.Modal)
  },
  props: {},
  data () {
    return {
      modalInstance: null
    }
  },
  created () {
    // Set the modal instance
    // We can call this component inside another modal component that extends BaseModal prototype
    this.modalInstance = this.$parent.modal
      ? this.$parent.modal
      : this.$parent.modalInstance
  },
  methods: {
    hide () {
      this.modalInstance.hide()
      if (this.closed) {
        this.closed()
      }
    }
  }
}
