/**
 * @vuepress
 * ---
 * title: "Service (error-manager)"
 * headline: "Service (error-manager)"
 * sidebarTitle: error-manager
 * initialOpenGroupIndex: -1 # optional, defaults to 0, defines the index of initially opened subgroup
 * prev: ../
 * next: ./options
 * ---
 */

import AppError from '@/context/support/errors/AppError'
import Service from '@/Service'

import options from './options'
import ErrorManager from './ErrorManager'

/* eslint-disable jsdoc/require-param, jsdoc/require-returns */

/**
 * @scorpion_ui_service {ErrorManager} error-manager
 * @description
 * The service "error-manager" is designed to make error managemnent easier.
 *
 * It references any error handlers you want to the `window`, `Vue` and `webpack-dev-server` runtime scopes.
 */
export default new Service({
  // Service
  service: {
    // Identifier
    identifier: 'error-manager',
    /**
     * @alias register
     * @description
     * On registering, the service will inject properties to Vue root instance prototype `$app`:
     *
     * - `$app.throwSystemError`
     * - `$app.clearSystemError`
     */
    register: function (_Core) {
      // Define vue root instance prototype in '$app' namespace
      const $app = _Core.context.vue.root.prototype.app
      /**
       * @alias $app.throwSystemError
       * @param {AppError} appError - The error instance
       * @returns {void}
       * @description
       * Throw a system error. If the AppError is already caught by ErrorManager, dont process
       * @example
       * // In any component
       * this.$app.throwSystemError(new AppError())
       */
      $app.throwSystemError = function (appError) {
        const root = this // 'this' is the Vue root instance
        // @ts-ignore
        if (!(appError instanceof AppError || appError instanceof _Core.context.support.errors.AppError)) {
          throw new Error('vm.$app.throwSystemError expects instance of AppError as first parameter')
        } else {
          // Set the system error in the store
          root.$store.dispatch('System/throwSystemError', appError)
          // If the AppError is already caught, dont process
          // @ts-ignore
          if (appError.isCaught()) {
            // ...
          } else {
            // This will run the error handlers
            root.$errorManager.throw(appError)
          }
        }
      }
      /**
       * @alias $app.clearSystemError
       * @returns {void}
       * @description
       * Clear the current system error.
       * @example
       * // In any component
       * this.$app.clearSystemError()
       */
      $app.clearSystemError = function () {
        const root = this // 'this' is the Vue root instance
        root.$store.dispatch('System/clearSystemError')
      }
    },
    /**
     * @alias create
     * @description
     * Returns an instance of `ErrorManager` created with options.
     */
    create: (serviceOptions, { ErrorManager: _ErrorManager }, _Core) => {
      // Return new ErrorManager
      return new _ErrorManager(serviceOptions)
    },
    /**
     * @alias plugin
     * @description
     * The following properties will be set to Vue prototype:
     *
     * ```js
     * Vue.prototype.$errorManager // => Core.service('error-manager')
     * ```
     */
    vuePlugin: ({ ErrorManager: _ErrorManager }, _Core) => _ErrorManager,
    // Vue plugin options
    pluginOptions: {},
    // rootOption
    rootOption: ({ ErrorManager: _ErrorManager }, _Core) => _ErrorManager.rootOption
  },
  /**
   * @alias onInitialized
   * @description
   * Once Core is initialized, defines the AppError `isCaught` instance method. Example:
   *
   * ```js
   * (new AppError()).isCaught()
   * ```
   *
   * It also defines the static method "caught" takes the error instance as first argument. Example:
   *
   * ```js
   * AppError.caught(new AppError())
   * ```
   */
  onInitialized: (Core) => {
    /**
     * AppError.prototype.isCaught
     */
    Object.defineProperty(Core.context.support.errors.AppError.prototype, 'isCaught', {
      value: function () {
        return this._isCaught
      }
    })
    /**
     * AppError.prototype._isCaught (private property)
     * @ignore
     */
    Object.defineProperty(Core.context.support.errors.AppError.prototype, '_isCaught', {
      value: false,
      writable: true
    })
    /**
     * Do something when custom error is caught by ErrorManager
     * @param {*} error - The error instance
     * @returns {void}
     */
    Core.context.support.errors.AppError.caught = function (error) { // eslint-disable-line handle-callback-err
      // Do nothing...
      // console.log('Default AppError caught static method :', error)
    }
  },
  // Options
  options,
  /**
   * @alias expose
   */
  expose: {
    /**
     * @see {@link ./ErrorManager ErrorManager}
     */
    ErrorManager
  }
})
