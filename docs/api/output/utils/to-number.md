---
title: toNumber
headline: toNumber
sidebarTitle: toNumber
---

# toNumber

<a name="tonumber">
</a>

## toNumber ⇒ <code>number</code>

Converts value to Number. Throws error if returned value is NaN

**Returns**: <code>number</code> - Returns the number

| Param | Type | Description |
| --- | --- | --- |
| value | <code>\*</code> | The value |

**Example**

```js
toNumber("2") // => 2toNumber(false) // => 0toNumber("john doe") // => error
```

