---
title: xor
headline: xor
sidebarTitle: xor
---

# xor

<a name="xor">
</a>

## xor ⇒ <code>Array</code>

Sort array of objects (lodash.xor). See https://lodash.com/docs/4.17.15#xor

**Returns**: <code>Array</code> - Returns the array difference

| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | Same arguments as lodash.xor |

