/**
 * @vuepress
 * ---
 * title: "Service (modal-manager) configuration"
 * headline: "Service (modal-manager) configuration"
 * sidebarTitle: Configuration
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * ⚠ THIS FILE IS ONLY USED TO GENERATE SERVICE CONFIGURATION DOCUMENTATION
 */

/**
 * @name Configuration
 * @typicalname Core.config.services['modal-manager']
 * @description
 * Service configuration
 *
 * > Refers to `Core.config.services['modal-manager']`
 *
 * ```js
 * 'modal-manager': {
 *   // The base z-index for ModalOverlay components
 *   baseZIndex: Number
 * }
 * ```
 */
const config = { // eslint-disable-line no-unused-vars
  /**
   * @alias Configuration.baseZIndex
   * @type {number}
   * @description
   * The base z-index for ModalOverlay components
   */
  baseZIndex: 500
}
