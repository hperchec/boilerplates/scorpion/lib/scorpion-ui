/**
 * @vuepress
 * ---
 * title: "Service (api-manager): RequestManager class"
 * headline: "Service (api-manager): RequestManager class"
 * sidebarTitle: .RequestManager
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import Core from '@/Core'
import { Hook, PromiseQueue } from '@/toolbox'
import API from './API'
import Request from './Request'

import { getAllKeys, typeCheck } from '@/utils'

/**
 * @classdesc
 * Service **api-manager**: RequestManager class
 */
class RequestManager {
  /**
   * Create a new RequestManager
   * @param {API} apiInstance - The API instance that creates this RequestManager
   */
  constructor (apiInstance) {
    if (!typeCheck(Core.context.services['api-manager'].API, apiInstance)) {
      throw new TypeError('RequestManager class : constructor failed. API instance attempted, received : ' + typeof apiInstance)
    }
    this.#apiInstance = apiInstance
  }

  /**
   * Accessors & mutators
   */

  // ...

  /**
   * Methods
   */

  /**
   * @category methods
   * @returns {API} - The API instance
   * @description
   * Get the related API instance
   */
  getAPIInstance () {
    return this.#apiInstance
  }

  /**
   * @category methods
   * @param {Function} generator - The request function (must return a Promise)
   * @param {string} method - The http verb
   * @param {string|URL} url - The URL to request
   * @param {object} [options = {}] - The request option
   * @param {Function} [beforeCb] - The function to exec before request
   * @param {Function} [successCb] - The success callback
   * @param {Function} [errorCb] - The error callback
   * @returns {Request} The request instance
   * @description
   * Create a new request
   */
  createRequest (generator, method, url, options, beforeCb, successCb, errorCb) {
    if (!generator || !typeCheck(Function, generator)) {
      throw new TypeError('RequestManager: createRequest method requires function that returns Promise as first parameter')
    }
    if (!typeCheck(String, method)) {
      throw new TypeError('RequestManager: createRequest method requires string as second parameter "method"')
    }
    if (!typeCheck([ String, URL ], url)) {
      throw new TypeError('RequestManager: createRequest method requires string or URL instance as third parameter "url"')
    }
    if (!beforeCb) { beforeCb = () => {} }
    if (!successCb) { successCb = (res) => res }
    if (!errorCb) { errorCb = (err) => err }
    // Check if abort signal is provided
    if (!(options?.signal)) {
      // const controller = new AbortController()
      // options.signal = controller.signal
    }
    const request = new Request(
      this,
      generator,
      // Method
      method,
      // URL,
      url,
      // Request options
      options,
      // Before callback
      async (requestInstance) => {
        // Use 'chain' hook execution to simulate middleware and chain the returned value
        const beforeHookResult = await this.#hooks.BEFORE_REQUEST.chain(requestInstance)
        return beforeCb(...beforeHookResult)
      },
      // Success callback
      async (res, request) => {
        const hookResult = await this.#hooks.REQUEST_SUCCESS.chain(res, request)
        return await successCb(...hookResult)
      },
      // Error callback
      async (requestError, request) => {
        let hookResult
        try {
          // Use 'chain' hook execution to simulate middleware and chain the returned value
          hookResult = await this.#hooks.REQUEST_ERROR.chain(requestError, request)
        } catch (error) {
          return await errorCb(error, request)
        }
        return await errorCb(...(hookResult || [ requestError, request ]))
      }
    )
    return request
  }

  get (key) {
    return this.#requests[key]
  }

  isWaitingFor (key) {
    return Boolean(this.#requests[key])
  }

  /**
   * @category methods
   * @param {string} key - The request key
   * @param {Promise} promise - The request promise
   * @returns {Promise} - The added promise
   * @description
   * Add the request to corresponding queue
   */
  push (key, promise) {
    // Get current promise queue
    const queue = this.get(key)
    if (queue) {
      queue.add(() => promise)
    } else {
      // @ts-ignore
      this.#requests[key] = new PromiseQueue().add(() => promise)
        .finally(() => {
          this.remove(key)
        })
    }
    return promise
  }

  remove (key) {
    if (this.isWaitingFor(key)) {
      delete this.#requests[key]
    }
  }

  /**
   * onBeforeRequest
   * @category public methods
   * @param {Function|Function[]} func - The function(s) to append to the hook queue
   * @returns {void}
   * @description
   * **BEFORE_REQUEST** hook append method.
   * The function to append, takes one arguments:
   *
   * ```ts
   * function (request: Request) => Array|void
   * ```
   *
   * - **request**: `{Request}` the request instance
   * @example
   * requestManager.onBeforeRequest((request) => {
   *   console.log(`Before request!`, request)
   * })
   */
  onBeforeRequest (func) {
    this.#hooks.BEFORE_REQUEST.append(func)
  }

  /**
   * onRequestSuccess
   * @category public methods
   * @param {Function|Function[]} func - The function(s) to append to the hook queue
   * @returns {void}
   * @description
   * **REQUEST_SUCCESS** hook append method.
   * The function to append, takes twp arguments:
   *
   * ```ts
   * function (response: Object, request: Request) => void
   * ```
   *
   * - **response**: `{Object}` the response object
   * - **request**: `{Request}` the request instance
   * @example
   * requestManager.onRequestSuccess((response, request) => {
   *   console.log(`Request success!`, response)
   * })
   */
  onRequestSuccess (func) {
    this.#hooks.REQUEST_SUCCESS.append(func)
  }

  /**
   * onRequestError
   * @category public methods
   * @param {Function|Function[]} func - The function(s) to append to the hook queue
   * @returns {void}
   * @description
   * **REQUEST_ERROR** hook append method.
   * The function to append, takes two arguments:
   *
   * ```ts
   * function (error: Object, request: Request) => void
   * ```
   *
   * - **error**: `{Object}` the error object
   * - **request**: `{Request}` the request instance
   * @example
   * requestManager.onRequestError((error, request) => {
   *   console.log(`Request error!`, error)
   * })
   */
  onRequestError (func) {
    this.#hooks.REQUEST_ERROR.append(func)
  }

  /**
   * Overrides the default toJSON object method for JSON.stringify() calls
   * @returns {object} - The instance data to be serialized
   */
  toJSON () {
    const keys = getAllKeys(this, {
      excludeKeys: [
        /^_/, // private properties that starts with '_'
        'toJSON',
        'toString',
        'constructor'
      ]
    })
    const jsonObj = keys.reduce((obj, key) => {
      obj[key] = this[key]
      return obj
    }, {})
    return jsonObj
  }

  /**
   * Static
   */

  // ...

  /**
   * Private properties
   */

  /**
   * Requests
   * @category private properties
   * @type {object}
   */
  #requests = {}

  /**
   * The API instance
   * @category private properties
   * @type {API}
   */
  #apiInstance = undefined // will be defined at new instance

  /**
   * Hooks
   * @category private properties
   * @type {object}
   */
  #hooks = {
    BEFORE_REQUEST: new Hook('BEFORE_REQUEST', [
      (request) => {
        return [ request ]
      }
    ]),
    REQUEST_SUCCESS: new Hook('REQUEST_SUCCESS', [
      (response, request) => {
        return [ response, request ]
      }
    ]),
    REQUEST_ERROR: new Hook('REQUEST_ERROR', [
      (error, request) => {
        return [ error, request ]
      }
    ])
  }
}

export default RequestManager
