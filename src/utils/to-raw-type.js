/**
 * @vuepress
 * ---
 * title: toRawType
 * headline: toRawType
 * sidebarTitle: toRawType
 * ---
 */

/**
 * toRawType
 * @param {*} value - The variable to check
 * @returns {string} The raw type string
 * @description
 * Get the raw type string of a value, e.g., [object Object].
 * @example
 * toRawType(new Date()) // => 'Date'
 * toRawType({ a: 1, b: 2 }) // => 'Object'
 * toRawType('Hello') // => 'String'
 */
export const toRawType = function (value) {
  return Object.prototype.toString.call(value).slice(8, -1)
}

export default toRawType
