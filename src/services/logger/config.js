/**
 * @vuepress
 * ---
 * title: "Service (logger) configuration"
 * headline: "Service (logger) configuration"
 * sidebarTitle: Configuration
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * ⚠ THIS FILE IS ONLY USED TO GENERATE SERVICE CONFIGURATION DOCUMENTATION
 */

/**
 * @name Configuration
 * @typicalname Core.config.services['logger']
 * @description
 * Service configuration
 *
 * > Refers to `Core.config.services['logger']`
 *
 * ```js
 * 'logger': {
 *   // Enable/disable logger
 *   log: Boolean,
 *   // Logger types
 *   types: Object
 * }
 * ```
 */
const config = { // eslint-disable-line no-unused-vars
  /**
   * @alias Configuration.log
   * @type {boolean}
   * @description
   * Enable/Disable logger.
   */
  log: true,
  /**
   * @alias Configuration.types
   * @type {object}
   * @description
   * Define types for Logger (see Logger constructor options).
   */
  types: {}
}
