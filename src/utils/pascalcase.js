/**
 * @vuepress
 * ---
 * title: pascalcase
 * headline: pascalcase
 * sidebarTitle: pascalcase
 * ---
 */

// Utils
import camelcase from './camelcase'
import capitalize from './capitalize'

/**
 * pascalcase
 * @param {string} str - The string to pascalcase
 * @returns {string} Returns the "pascalcased" string
 * @description
 * Convert a string to pascalcase *({@link ../context/vue/filters#filters-pascalcase Defined as Vue filter})*.
 * @example
 * pascalcase('foo bar') // => 'FooBar'
 */
export const pascalcase = function (str) {
  return capitalize(camelcase(str), 1)
}

export default pascalcase
