export default LocalStorageManager;
declare const LocalStorageManager_base: any;
declare class LocalStorageManager extends LocalStorageManager_base {
    [x: string]: any;
    static "__#14@#IS_INITIALIZED": boolean;
    static originalSetItemMethod: any;
    static originalRemoveItemMethod: any;
    static rootOption: string;
    static aliases: string[];
    static checkCompat(): void;
    static init(): void;
    static isKeyDefined(key: string): boolean;
    static install(_Vue: import("vue/types/umd"), options?: object): void;
    constructor(options?: {
        schema?: object;
        setItemEventName?: string;
        removeItemEventName?: string;
        onSetItem?: Function | Function[];
        onRemoveItem?: Function | Function[];
    });
    setItemEventName: string;
    removeItemEventName: string;
    castMethods: object;
    readonly get items(): any;
    readonly get schema(): any;
    getTypeFromLSValue(lsValue: string): string;
    onSetItem(func: Function | Function[]): void;
    onRemoveItem(func: Function | Function[]): void;
    hasItem(key: string): boolean;
    toJSON(): object;
    #private;
}
//# sourceMappingURL=LocalStorageManager.d.ts.map