import options from './options'

/**
 * SET_LOADING_STATUS
 * @description Mutate state.loadingStatus
 * @param {object} state - vuex store state
 * @param {object} payload - Mutation payload
 * @param {string} payload.name - Loading name
 * @param {boolean} payload.status - Loading status
 * @returns {void}
 */
function SET_LOADING_STATUS (state, { name, status }) {
  state.loadingStatus[name] = status
}

/**
 * Store loading status plugin
 */
export default {
  /**
   * Function to create plugin
   * @param {object} pluginOptions - The create plugin options
   * @returns {Function} Returns store loading status plugin
   */
  createPlugin: function (pluginOptions) {
    // Process options
    // ...

    // Plugin
    return (store) => {
      // Deep parse modules to inject loading mutation
      const deepParse = function (module) {
        // If we find state.loadingStatus, so we inject mutation SET_LOADING_STATUS in module
        if (module.state.loadingStatus) {
          // We have to touch directly the private _rawModule property
          module._rawModule.mutations[pluginOptions.mutationName] = SET_LOADING_STATUS
        }
        module.forEachChild((childObj, childName) => {
          deepParse(childObj)
        })
      }
      const root = store._modules.root
      // Parse
      deepParse(root)

      // ===================================================
      // TO DO
      // // Add setLoading action
      // store._modules.root._rawModule.actions.setLoading = function (ctx, payload) {
      //   const { namespace, loadingName, status } = payload
      //   console.log(`Module: "${namespace}", on veut set le loading "${loadingName}" : `, status)
      // }
      // console.log('coucouuuu :', store._modules.root._rawModule.actions.setLoading)

      // const unsubscribe = store.subscribeAction((action, state) => {
      //   console.log('action.type : ', action.type)
      //   console.log('action.payload : ', action.payload)
      // })

      // // you may call unsubscribe to stop the subscription
      // unsubscribe()
      // ===================================================

      // The mutations added before are not yet detected by the store, so we need to trigger registration.
      // The workaround is to trigger the internal resetStore function
      // but vuex Store class does not provide method to manually reset the store
      // So, we have to use registerModule & unregisterModule methods with A TEMPORARY FAKE STORE MODULE
      // See: https://github.com/vuejs/vuex/blob/7b024d8bc7ed8e273b8bd853b1e23ec78ab1b045/src/store-util.js#L18
      store.registerModule('__temporary__', {})
      store.unregisterModule('__temporary__')
    }
  },
  /**
   * createPlugin options
   */
  options
}
