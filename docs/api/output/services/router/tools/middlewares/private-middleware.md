---
title: private-middleware
headline: private-middleware
---

# private-middleware

<a name="privatemiddleware">
</a>

## privateMiddleware ⇒ <code>function</code>

Creates a private middleware: only private routes

**Returns**: <code>function</code> - - The encapsulated function

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| middlewareFunc | <code>function</code> |  | The middleware function |
| [metaKey] | <code>string</code> | <code>&quot;&#x27;requiresAuth&#x27;&quot;</code> | The meta key |

