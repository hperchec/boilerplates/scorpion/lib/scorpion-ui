import Core from '@/Core'
import DataManager from './DataManager'
import options from './options'

import Request from '@/services/api-manager/Request' // type

/**
 * Store resources plugin
 */
export default {
  /**
   * Function to create plugin
   * @param {object} pluginOptions - The create plugin options
   * @returns {Function} Returns store resources plugin
   */
  createPlugin: function (pluginOptions) {
    // Process options
    // ...

    const storeServiceContext = Core.context.services['store'] // eslint-disable-line dot-notation
    const { getModule, resources: { getResourceModule, getResourceDataManager } } = storeServiceContext.tools

    // Plugin
    return (store) => {
      // Map created service
      const logger = Core.service('logger')
      const apiManager = Core.service('api-manager')
      const errorManager = Core.service('error-manager')

      // Loop on resources modules
      getModule(store, pluginOptions.rootModule).forEachChild((childObj, childName) => {
        // Get model
        const model = childObj.context.getters.$model
        if (model) {
          // Extend module object: add "dataManager" property
          childObj.dataManager = new DataManager(childObj.state.$data, model, {
            storeModuleNamespace: [ pluginOptions.rootModule, model.storeOptions.namespace ].join('/')
          })
        }
      })

      /**
       * Get specific resource module
       * @param {string} moduleName - The resource module name (ex: "Users")
       * @returns {import("vuex").Module} - Returns the instance of DataManager
       */
      store.getResourceModule = function (moduleName) {
        return getResourceModule(store, moduleName)
      }

      /**
       * Get specific resource module data manager
       * @param {string} moduleName - The resource module name (ex: "Users")
       * @returns {DataManager} - Returns the instance of DataManager
       */
      store.getResourceDataManager = function (moduleName) {
        return getResourceDataManager(store, moduleName)
      }

      /**
       * Add custom method to make request
       * @param {object} options - The request options (all property that is not listed below will be passed as API.request() options argument)
       * @param {string} options.path - The path
       * @param {string} options.verb - The HTTP method
       * @param {string} options.description - The request description
       * @param {object} options.loading - The loading options
       * @param {object} options.data - The data to pass to request
       * @param {?string} [options.API] - The api to use. Default is plugin option "defaultAPI" value.
       * @returns {Promise<Error|object>} - Returns the response or Error if occurs
       */
      store.makeRequest = function (options) {
        const {
          path: pathOption,
          verb: verbOption,
          description: descriptionOption, // eslint-disable-line no-unused-vars
          loading: loadingOption, // eslint-disable-line no-unused-vars
          API: apiOption,
          ...apiRequestOptions
        } = options
        const api = apiManager.use(apiOption || pluginOptions.defaultAPI)
        let response
        const responseObject = {
          async onError (callbackFn, payload) {},
          async onSuccess (callbackFn, payload) {},
          getResponse () {
            return response
          }
        }
        return new Promise((resolve, reject) => {
          // Request server
          api.request(
            verbOption,
            pathOption,
            // @ts-ignore
            apiRequestOptions,
            (request) => {
              return store.makeRequest.defaultBeforeRequest(request, options /* the makeRequest method options argument */)
            },
            async (_response, request) => {
              await store.makeRequest.defaultSuccessCallback(_response, request, options /* the makeRequest method options argument */)
              response = _response
              responseObject.onSuccess = async (callbackFn, payload) => {
                return await callbackFn(_response, payload)
              }
              resolve(responseObject)
            },
            async (_error, request) => {
              await store.makeRequest.defaultErrorCallback(_error, request, options /* the makeRequest method options argument */)
              response = _error
              responseObject.onError = async (callbackFn, payload) => {
                return await callbackFn(_error, payload)
              }
              resolve(responseObject)
              // Throw "silently"
              errorManager.throw(_error)
            }
          )
        })
      }

      /**
       * The default hook to exec before api.request() method
       * @param {Request} request - The request instance
       * @param {object} makeRequestOptions - The makeRequest method options
       * @returns {void}
       */
      store.makeRequest.defaultBeforeRequest = (request, makeRequestOptions) => {
        // Log request sending
        logger.consoleLog(`${makeRequestOptions.description} request - Request [${request.method}] "${request.url}" ...`, { type: 'system' })
        if (makeRequestOptions.loading) {
          // Set loading to true
          store.commit(makeRequestOptions.loading.mutation, { name: makeRequestOptions.loading.name, status: true }, { root: true })
        }
      }

      /**
       * The default success callback to pass to api.request() method
       * @param {object} response - The response object
       * @param {Request} request - The request instance
       * @param {object} makeRequestOptions - The makeRequest method options
       * @returns {object} - Returns the response object
       */
      store.makeRequest.defaultSuccessCallback = (response, request, makeRequestOptions) => {
        if (makeRequestOptions.loading) {
          // Set loading to false
          store.commit(makeRequestOptions.loading.mutation, { name: makeRequestOptions.loading.name, status: false }, { root: true })
        }
        return response
      }

      /**
       * The default error callback to pass to api.request() method
       * @param {object} error - The error object
       * @param {Request} request - The request instance
       * @param {object} makeRequestOptions - The makeRequest method options
       * @returns {Error} - Returns the error object
       */
      store.makeRequest.defaultErrorCallback = (error, request, makeRequestOptions) => {
        // Log error message
        logger.consoleLog(`${makeRequestOptions.description} request error - Server response [${request.method}] "${request.url}" ...`, { type: 'error' })
        if (makeRequestOptions.loading) {
          // Set loading to false
          store.commit(makeRequestOptions.loading.mutation, { name: makeRequestOptions.loading.name, status: false }, { root: true })
        }
        return error
      }
    }
  },
  /**
   * createPlugin options
   */
  options
}
