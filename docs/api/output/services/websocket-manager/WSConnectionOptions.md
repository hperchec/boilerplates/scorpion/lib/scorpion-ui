---
title: "Service (websocket-manager): WSConnectionOptions class"
headline: "Service (websocket-manager): WSConnectionOptions class"
sidebarTitle: .WSConnectionOptions
sidebarDepth: 0
prev: false
next: false
---

# Service (websocket-manager): WSConnectionOptions class

<a name="wsconnectionoptions">
</a>

## WSConnectionOptions

Service **websocket-manager**: WSConnectionOptions class

**Schema**:

- [WSConnectionOptions](#wsconnectionoptions)
  - [new WSConnectionOptions(name, driver, options)](#newwsconnectionoptionsnew)
  - [.toJSON()](#wsconnectionoptions-tojson) ⇒ <code>object</code>
  - _properties_
    - [.name](#wsconnectionoptions-name) : <code>string</code>
    - [.driver](#wsconnectionoptions-driver) : <code>string</code>
    - [.options](#wsconnectionoptions-options) : <code>object</code>

<a name="newwsconnectionoptionsnew">
</a>

### new WSConnectionOptions(name, driver, options)

Create new WSConnectionOptions

| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The WSConnection name |
| driver | <code>string</code> | The library to use in WSConnection.supportedDrivers |
| options | <code>object</code> | An object that the driver needs to configure itself |

<a name="wsconnectionoptions-tojson">
</a>

### wsConnectionOptions.toJSON() ⇒ <code>object</code>

Overrides the default toJSON object method for JSON.stringify() calls

**Returns**: <code>object</code> - - The instance data to be serialized

<a name="wsconnectionoptions-name">
</a>

### wsConnectionOptions.name : <code>string</code>

WSConnection name

**Category**: properties  
<a name="wsconnectionoptions-driver">
</a>

### wsConnectionOptions.driver : <code>string</code>

WSConnection driver name

**Category**: properties  
<a name="wsconnectionoptions-options">
</a>

### wsConnectionOptions.options : <code>object</code>

Driver options

**Category**: properties  
