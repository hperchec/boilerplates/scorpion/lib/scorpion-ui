## 1.1.0-rc.14 (2024-11-04)

* docs: update docs ([3bb65ba](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/3bb65ba))
* docs: update types ([4351c6a](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/4351c6a))
* refactor: move mixins to default template ([48f1abd](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/48f1abd))
* build: add export definition for extract-css rollup plugin ([6819a95](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/6819a95))



## 1.1.0-rc.13 (2024-09-10)

* build: add commonjs build ([0e21c1b](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/0e21c1b))



## 1.1.0-rc.12 (2024-08-16)

* fix(Modal): fix Modal bug ([82e69e2](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/82e69e2))



## 1.1.0-rc.11 (2024-08-01)

* refactor(toast-manager): refact Toast components ([dd01cec](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/dd01cec))



## 1.1.0-rc.10 (2024-07-08)

* fix(store): add condition to check if model is set in resources-plugin ([d44a48b](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/d44a48b))



## 1.1.0-rc.9 (2024-06-27)

* docs: fix doc generation errors ([6ba0ed1](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/6ba0ed1))
* docs: remove comments from generated types ([48f20bc](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/48f20bc))
* docs: update comments ([ba0df8d](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/ba0df8d))
* docs: update readme template ([06a77c7](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/06a77c7))
* docs: update types ([281fc0e](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/281fc0e))
* build: enable lintDirtyModulesOnly option for StylelintPlugin ([6574fcf](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/6574fcf))
* build: eslint configuration ([7c83cb4](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/7c83cb4))
* build: exclude some deps from build + minify for .min bundle ([e79a99e](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/e79a99e))
* build: migrate to juisy v1.2.4 ([5cbda25](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/5cbda25))
* build: update vue-docgen-template for glob dependency ([823cbc7](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/823cbc7))
* build(cli): update commit message in release command ([efa3169](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/efa3169))
* fix: bug with config resolution in development mode ([64c0960](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/64c0960))
* refactor: set same node_modules glob as default StylelintPlugin option ([be312ca](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/be312ca))



# [1.1.0-rc.8](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-rc.7...v1.1.0-rc.8) (2024-06-11)


### Features

* add reactive-refs vue plugin ([352ead1](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/352ead14fa8cc0ab21c8d45a7a74ad067662f1c9))



# [1.1.0-rc.7](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-rc.6...v1.1.0-rc.7) (2024-05-30)


### Features

* add BASE_URL component shared computed ([cbccf4e](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/cbccf4e046c36e1afd21b4c2acbfbf36f04f5826))



# [1.1.0-rc.6](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-rc.5...v1.1.0-rc.6) (2024-05-30)


### Features

* add "vue" logger type ([518866a](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/518866acf1737ad6fab9ce4c9eedd126d807f444))
* add debugging magic property support ([49931cb](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/49931cbf1c6e8238d7e04bdbb48bf441cd5a4a28))
* **utils:** add deepBind util ([9d34883](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/9d348832fd00a8aed60f4e77a7ebf671bf4d4de9))



# [1.1.0-rc.5](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-rc.4...v1.1.0-rc.5) (2024-05-29)


### Bug Fixes

* bad reference ([8edef02](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/8edef02e5a06bfc61f7a27c69052b60c88b2ea2a))



# [1.1.0-rc.4](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-rc.3...v1.1.0-rc.4) (2024-05-29)


### Bug Fixes

* move stylelint config ([ad90fca](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/ad90fcad73523d256667cc26a2288de256282c3d))



# [1.1.0-rc.3](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-rc.2...v1.1.0-rc.3) (2024-05-28)


### Bug Fixes

* vue.config.js postcssOptions & pretty pwa options ([c72e899](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/c72e899ddb625aa36347ef94dbf801f6c6c779ef))



# [1.1.0-rc.2](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-rc.1...v1.1.0-rc.2) (2024-05-20)


### Bug Fixes

* add missing dep "promise-queue" ([0720fbe](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/0720fbe24ac9eb2b61a457f543cf63f437afaced))
* toJSON model bug ([8434697](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/8434697b81a9757b20c8757903ae4fc088acf1e2))


### Features

* prevent multiple same request sending + add new allowConcurrent option ([39ba76e](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/39ba76e19c767b0b6a8d75b298c9f16aad01b168))



# [1.1.0-rc.1](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-rc.0...v1.1.0-rc.1) (2024-04-12)


### Features

* **router:** add defaultOnComplete & defaultOnAbort options ([b3007d8](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/b3007d87d231ef3a75306d0be861be981c7652ef))



# [1.1.0-rc.0](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.52...v1.1.0-rc.0) (2024-03-30)


### Bug Fixes

* **Hook:** bug with chain method ([3199eec](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/3199eec058a14420866c34645ee85dd2e8427d0b))
* **modal-manager:** add missing css to ModalContainer component ([8516bb4](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/8516bb49afae1a4cfae39303643ec41c1c2625d8))
* **ModalContainer:** v-for error ([70174dd](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/70174ddc5269538f27ca800831cdd89d303560f0))
* **root:** bad assign in initUserSettings ([134f772](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/134f7726d80f1c6e017bdfc43f18ed483341715d))


### Features

* add modal-manager service ([d37fda6](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/d37fda6fac3a9e7e286d2efb725d5dbaf034da3f))
* finish error management ([c1f651d](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/c1f651d612afa0a35dc49cdb5737205c4d76e24b))
* **logger:** add json-pretty component ([670a5d0](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/670a5d0c3e27f09f755f2bfb79d4f088faf92753))
* **modal-manager:** add $app.ui.showModal and $app.ui.hideModal methods ([181a907](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/181a907f5024ad164eeca7d93c29690d66f76d5c))
* **modal-manager:** use <transition-group> component wrapper and add useTransition option ([2df5fc3](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/2df5fc30d2b27f964401d115e398c5e9a2927d20))
* **services:** add device-manager service ([05c3d77](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/05c3d7715991c1047745940c5207497fe97acb91))
* **services:** add error-manager service ([2db05eb](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/2db05eb4756e64f979704f83c3c63eed11b8ae70))
* **services:** add keyboard-event-manager service ([0b70828](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/0b70828a8f6f7907b15fea66eb2996e89fab5a25))
* **services:** add notification-manager service ([9251ae6](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/9251ae67b04fca53cfaf5b66ee1db857f4adab32))
* **support:** add app errors support ([d01dc74](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/d01dc748f5b8dbb6d304cb1c1fc2e83bb8079a24))
* **toolbox:** add developement runtime errorHandler ([ecc6319](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/ecc63195ac3e875a4bcdeb25483f4ec4b0fac850))
* **utils:** add getAllKeys util ([8b31bbf](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/8b31bbfabab227b5873e6c1e4f4d582dd3b191f2))



# [1.1.0-beta.52](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.51...v1.1.0-beta.52) (2024-03-15)


### Features

* **store:** add support for requestOptions in resources tool generateActions ([d5cc2ae](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/d5cc2ae832bf950def669196f5c7363582c7cb1f))



# [1.1.0-beta.51](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.50...v1.1.0-beta.51) (2024-03-15)


### Features

* **API:** support for beforeRequest hook ([c8b617d](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/c8b617d2cb503ae830751069f716a0ed8e3425e7))



# [1.1.0-beta.50](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.49...v1.1.0-beta.50) (2024-03-14)


### Features

* add auth-manager service ([70c9f02](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/70c9f023b76f62f52984f6596e4832b63dbb36fb))
* **AuthManager:** add authentication manager service ([5a38d1e](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/5a38d1e44ed351f4a6b70f3d7b2ed609893ea226))
* **Model:** add Model store mutation hooks support ([6a5de9f](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/6a5de9f9c7eb42f3a42511b9f979985c0b34b5f6))
* **utils:** add compose function util ([a0c3d7b](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/a0c3d7b21c2eab3141e750b91c261b51f1236fbf))



# [1.1.0-beta.49](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.48...v1.1.0-beta.49) (2024-03-13)


### Features

* **config:** add vue root instance auto-mount configuration ([5a6ef19](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/5a6ef19e7d6597cc6ac03ab6d95a88db1a95762d))
* **Core:** add AFTER_USE_VUE_PLUGIN hook ([4c1b681](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/4c1b6814ed6b530cce3df9f913cd69b890dfeb17))
* **Core:** return root instance in runApp method ([0a64b28](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/0a64b285e3891785caa9e418c2a6dd43a9879da1))
* **router:** make router init hookable ([b590e95](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/b590e9562919bf9e4bf9bf62ce6cbfd0dbb05fb4))



# [1.1.0-beta.48](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.47...v1.1.0-beta.48) (2024-03-12)


### Features

* **API:** add hooks and success and error callback to request() method ([edc129d](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/edc129de80be3722a540101f57071cbaad1b0b0f))
* **Hook:** add compose method to chain execution of queued function ([10c8b8f](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/10c8b8f14de7a10933d432519c1f020ca18774b2))



# [1.1.0-beta.47](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.46...v1.1.0-beta.47) (2024-03-04)


### Features

* **utils:** add random.number util (use lodash.random) ([6e0d782](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/6e0d7822693155ded38a6081fb060817a117eb56))



# [1.1.0-beta.46](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.45...v1.1.0-beta.46) (2024-03-02)


### Features

* **LocalDatabase:** add encryptionKey property ([609be0b](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/609be0b9f5d9d78227c0fa98a033eaa4737a7c14))
* **utils:** add base64-to-uint8array util ([696ab8d](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/696ab8d1487f6f81037be7ae3dbb46d26f462f0a))
* **utils:** add xorBy util ([138daa1](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/138daa15bdc8781a52e00f637250d7ab05aeae11))



# [1.1.0-beta.45](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.44...v1.1.0-beta.45) (2024-02-15)


### Features

* **shared:** add global __APP_VERSION__ to DefinePlugin options in create-config method ([152b871](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/152b871bb9985188ef0013d1a5a5009cd49a6c27))



# [1.1.0-beta.44](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.42...v1.1.0-beta.44) (2024-02-14)


### Features

* add Core INITIALIZED hook and service onInitialized method ([7630c32](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/7630c32f56a0c4537277fd5be3b330e9ebc098ba))
* add Core INITIALIZED hook and service onInitialized method ([477b2dd](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/477b2dd374189156b9f1093742e773bcad82588a))
* **Core:** add getRunningApp static method ([b284f25](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/b284f251ccc78145130a9e8a6bf30c8fbce7b4f4))
* **Core:** add static hasService method ([5a9fc83](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/5a9fc83add52edd79d9ceb435a565e5ff94c5081))
* **Core:** pass serviceOptions and serviceContext to BEFORE_CREATE_SERVICE callback ([b6f2555](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/b6f255563159b1e9c0e95cc13dc226769eb1535b))
* **Logger:** add consoleLogTest method for debugging ([c0d7483](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/c0d7483fe9f2e368e61b7666f85b66ebfac57603))
* **service:** add local-database-manager service ([c7b0f32](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/c7b0f32e665e817f8c55b013f42f021166fa6e02))
* **utils:** add new utils ([0f48499](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/0f484991dd673eb7cd406ae35535b16dccd63666))



# [1.1.0-beta.42](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.41...v1.1.0-beta.42) (2024-02-09)


### Bug Fixes

* **api-manager:** driver options assignment ([bccce0f](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/bccce0ffd7d1c2e3073a4f05795d8e055388ea6d))
* support for webpack 5, vue-cli 5 and vue-loader 15 ([f4a83a8](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/f4a83a8ce42b2dc710da159697cd7cd7cecc9cb9))


### Features

* add websocket-manager service ([b6857f7](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/b6857f7a3c8d3ef00ec97053388875d8c24ec509))
* **store:** pass all options in resources-plugin makeRequest method ([ea6ba47](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/ea6ba4758d6c716f41fddfa8944bacb53cccc764))



# [1.1.0-beta.41](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.40...v1.1.0-beta.41) (2024-01-30)



# [1.1.0-beta.40](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.39...v1.1.0-beta.40) (2024-01-10)


### Features

* **Route:** add "redirectToNotFound" static method ([9811753](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/9811753b3015b027dda4a661b7d2a45c070b3618))



# [1.1.0-beta.39](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.38...v1.1.0-beta.39) (2024-01-09)


### Features

* **EventBus:** add 'strictMode' option ([71ae2b9](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/71ae2b99497c8d77e16f02f9097b7b30d9995118))



# [1.1.0-beta.38](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.37...v1.1.0-beta.38) (2023-12-06)


### Features

* **utils:** add lodash xor util for array differences ([3c86f1a](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/3c86f1af4c205e4f342088e88c7f7fba7eb720aa))



# [1.1.0-beta.37](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.36...v1.1.0-beta.37) (2023-12-01)


### Bug Fixes

* **DataManager:** support for composite keys in delete method ([edaf571](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/edaf571a08e8a78aa59f6b02d383688af7848fda))
* **Model:** support for composite keys ([fa61730](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/fa6173009bb9c0a4c8ce468b749e04ada01499fe))


### Features

* **utils:** add fileToDataUrl and blobFromCanvas utils ([98045a8](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/98045a8e553c514943a31e097c474492d475d3f1))



# [1.1.0-beta.36](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.35...v1.1.0-beta.36) (2023-11-09)


### Bug Fixes

* bug with composite primary keys ([313bd65](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/313bd65d39435daa1915a8ede72f54a76656a1ea))


### Features

* **store:** support for return value in request onError & onSuccess method ([cbf657e](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/cbf657e720297d7b4498a880118a1cbb9a34802a))



# [1.1.0-beta.35](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.34...v1.1.0-beta.35) (2023-10-31)


### Features

* **router:** Add Route getComponentConstructorName method + static buildRouteComponentName ([4824e91](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/4824e912b33f27ddf883f5a02c6b341cf6034ebb))
* **router:** add RouteComponentMixin syncQueryParamsWith method ([7ada54c](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/7ada54cb8c1a5767e2c5250fcae18cba323d3a70))
* **utils:** add pascalcase util ([3d11176](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/3d111761f9c1e832f0c8377ecb88ffe01b68c7b0))



# [1.1.0-beta.34](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.33...v1.1.0-beta.34) (2023-10-26)


### Bug Fixes

* **Model:** define instance attributes as enumerable ([d86f5d6](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/d86f5d6d7d9e7cca2590fab099ff3b1265c16523))


### Features

* **utils:** add groupBy util from lodash.groupby ([c6968b0](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/c6968b02ccbe726e42d6f0e491accb326859efb5))



# [1.1.0-beta.33](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.32...v1.1.0-beta.33) (2023-10-20)


### Features

* **Router:** RouteComponent auto emit "query-param-changed" event ([ca2c10e](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/ca2c10e38912776c70213289aac4a92f38bf9579))



# [1.1.0-beta.32](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.31...v1.1.0-beta.32) (2023-10-19)


### Bug Fixes

* **router:** Route default cast for Boolean ([433f3ed](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/433f3ed808bab6a9daf734180cf6e12c4092eeaa))



# [1.1.0-beta.31](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.30...v1.1.0-beta.31) (2023-09-30)


### Bug Fixes

* **Model:** support for composite primary keys ([7680911](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/76809115a64b8c6b3fbbeaee027e206733fb72a3))


### Features

* **context:** add context.vue.ensureComponentConstructor ([494801a](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/494801aaabd97081c33274fe62a8b6d2dabf6eef))



# [1.1.0-beta.30](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.29...v1.1.0-beta.30) (2023-09-27)


### Features

* add $app.utils.vue.ensureComponentConstructor ([1bf0b54](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/1bf0b54641354d84619276aa504059a7f324cc90))
* **utils:** add isVueComponent util ([b156d61](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/b156d6133d8e0f3c63311bdee4a3a4ccbefb209f))



# [1.1.0-beta.29](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.28...v1.1.0-beta.29) (2023-09-26)


### Bug Fixes

* Route class: cast query param only if defined ([edb948b](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/edb948ba7d71a40720ce9bf03efd0a3535cd8d28))



# [1.1.0-beta.28](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.27...v1.1.0-beta.28) (2023-09-26)


### Features

* **utils:** add get-timezones utill ([d146a1c](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/d146a1c051ed5efbaf4d15f904a8531622e07e3e))



# [1.1.0-beta.27](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.26...v1.1.0-beta.27) (2023-09-25)


### Features

* **router:** support for Route query params & component prop ([2fd9e07](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/2fd9e07650e65c0c04c5b34141761c8c884cd8d2))



# [1.1.0-beta.26](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.25...v1.1.0-beta.26) (2023-09-24)



# [1.1.0-beta.25](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.24...v1.1.0-beta.25) (2023-09-21)


### Features

* **store:** generate-actions tools -> pass data to retrieve and delete request ([e881b8e](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/e881b8e60f873fae8121792c7b9b3f3de35bb2e9))



# [1.1.0-beta.24](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.23...v1.1.0-beta.24) (2023-09-20)


### Features

* **router:** add create public and private middleware tool ([e770c9c](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/e770c9cda2e804735295a95f1ed4ea93aac4933d))



# [1.1.0-beta.23](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.22...v1.1.0-beta.23) (2023-09-14)



# [1.1.0-beta.22](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.21...v1.1.0-beta.22) (2023-09-12)


### Features

* **store:** generate-actions tool => pass payload as second argument to onError and onSuccess callbacks ([73d48cb](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/73d48cb8b6f22da0559018b1a84d43d287f0c389))



# [1.1.0-beta.21](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.20...v1.1.0-beta.21) (2023-09-11)


### Features

* **store:** generateActions tool => add onSuccess and onError option support ([7e12031](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/7e120313719f4b40edbff2569b6e5637b80592bd))



# [1.1.0-beta.20](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.19...v1.1.0-beta.20) (2023-08-30)



# [1.1.0-beta.19](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.18...v1.1.0-beta.19) (2023-08-29)


### Features

* **DataManager:** support for model composite primary key ([7c27412](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/7c2741252480d364c5215d233fdcd3b487bf3cbd))



# [1.1.0-beta.18](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.17...v1.1.0-beta.18) (2023-08-29)



# [1.1.0-beta.17](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.16...v1.1.0-beta.17) (2023-08-29)


### Features

* add "$app.ui" namespace and move toast-manager shortcuts into ([dbaa778](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/dbaa77834b21dac9d677d74659e7d83a0187d86e))
* **Model:** support for composite primary key ([88bc1fe](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/88bc1fed1732e139a681c3fcc21416eda887eb6b))



# [1.1.0-beta.16](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.15...v1.1.0-beta.16) (2023-08-28)


### Bug Fixes

* replace delete by Vue.delete() call to trigger reactivity ([f724c18](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/f724c18d3fe6f5e63952c38810836ed38cc3b8da))



# [1.1.0-beta.15](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.14...v1.1.0-beta.15) (2023-08-28)


### Bug Fixes

* **Model:** don't cast null fields ([f58e79f](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/f58e79fc6722fcb23d0c9d7a2c5660ab76b55eb7))



# [1.1.0-beta.14](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.13...v1.1.0-beta.14) (2023-08-26)


### Features

* cordova shared lib ([c449cc0](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/c449cc01fce798d23b6375d90b27efe89a280d1d))



# [1.1.0-beta.13](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.12...v1.1.0-beta.13) (2023-08-23)



# [1.1.0-beta.12](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.11...v1.1.0-beta.12) (2023-08-23)


### Features

* **Model:** add insert, update, upsert & delete static methods ([64db969](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/64db969f8bd864e0c2a6d5072b8288fe04dad9ef))
* **utils:** add parallelAsync util to run some promises in parallel ([077a297](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/077a2974e32347425296aa2fa48de48453d9ce58))



# [1.1.0-beta.11](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.10...v1.1.0-beta.11) (2023-08-18)


### Bug Fixes

* docs error for DataManager ([26ae50f](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/26ae50faf4c093af1d003ddf94bdfccb8a35a1c4))
* example -> api-manager config ([d6d29ec](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/d6d29ec723a5b428cd9695341f942b4e0fad6c11))


### Features

* **utils:** add get-timestamp & set-date-to-midnight utils ([8246dac](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/8246dacaf83dbf9b79d5b1b6d8002da1122bb5ff))



# [1.1.0-beta.10](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.9...v1.1.0-beta.10) (2023-08-11)


### Bug Fixes

* bad 'api-manager' service configuration ([8a88311](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/8a883111e1277a6d70eb4e6b521e67b33690414e))
* store system plugin translate method ([1349d2f](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/1349d2f3da325f86014334e8044b1fa1a089c6ee))


### Features

* add model relation ([dc84fb5](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/dc84fb54ff625de3af5d23b38864683dd680c9a6))
* **Collection:** add filter & findMany methods ([956c3fc](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/956c3fc47878ca3e152eb39d2b3692846023cc2c))
* **Collection:** add lock option ([3b905ab](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/3b905ab5cb0a4e15c37b5cff631f050045d3c1ad))



# [1.1.0-beta.9](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.8...v1.1.0-beta.9) (2023-08-08)



# [1.1.0-beta.8](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.7...v1.1.0-beta.8) (2023-08-07)


### Bug Fixes

* not valid call of "typeCheck" util ([ca1813d](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/ca1813dd7cd990e98c93a1f843b7973674b1baa7))


### Features

* add collections methods ([f25a60a](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/f25a60ab628b3ad9d008151abde4505500be1732))



# [1.1.0-beta.7](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.6...v1.1.0-beta.7) (2023-08-05)


### Features

* **Collection:** create ModelCollection specific for models ([a2a6270](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/a2a6270e63f260cc914ce8b5468e0f16fa144b62))



# [1.1.0-beta.6](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.5...v1.1.0-beta.6) (2023-08-04)


### Features

* **Model:** add default cast support + primaryKey ([1bec509](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/1bec50917a8966a96119600ed8f80442e3fa45b7))



# [1.1.0-beta.5](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.4...v1.1.0-beta.5) (2023-08-04)


### Features

* lockable collection + upsert method ([e3df649](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/e3df649a379bf91764ef4acf1f35aee7066af406))



# [1.1.0-beta.4](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.3...v1.1.0-beta.4) (2023-06-01)



# [1.1.0-beta.3](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.2...v1.1.0-beta.3) (2023-06-01)


### Bug Fixes

* add missing rootOption statis property to ToastManager class ([70e850c](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/70e850c658635970955ead379b16fe2f1d654dcf))


### Features

* create formatGlobals shared ([98db985](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/98db9856d780e4453041f5d0cfcfb55c83263e7c))



# [1.1.0-beta.2](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.1...v1.1.0-beta.2) (2023-05-25)


### Bug Fixes

* **build:** resolve util for aliases ([98cae4c](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/98cae4cc1e36195661a7873e91b522cbdc939b0b))


### Features

* **context:** add setOptions method to root ([0eedd1a](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/0eedd1ab3d94117e39001dee4de200bca502dc83))
* **utils:** add orderby (lodash.orderby) util ([3cdd367](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/3cdd367ca756f30c0ff0c065ead73e830b6e2302))



# [1.1.0-beta.1](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.1.0-beta.0...v1.1.0-beta.1) (2023-05-23)


### Bug Fixes

* set absolute path for vue-loader-extension in createConfig shared util ([1f3a38c](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/1f3a38cf9b5c46fa6cc558fadf86adfdd2b16882))


### Features

* **toolbox:** add template utils ([2aed382](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/2aed3827cb6240c83918dabe97d3578274f28534))



# [1.1.0-beta.0](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.0.2-beta.7...v1.1.0-beta.0) (2023-05-22)


### Bug Fixes

* force vue plugin name on registering ([01cfc87](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/commit/01cfc8770bd2e4f1186d3819e3071ca6b5030344))



## [1.0.2-beta.7](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.0.2-beta.6...v1.0.2-beta.7) (2023-04-12)



## [1.0.2-beta.6](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.0.2-beta.5...v1.0.2-beta.6) (2023-04-11)



## [1.0.2-beta.5](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.0.2-beta.4...v1.0.2-beta.5) (2023-04-11)



## [1.0.2-beta.4](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.0.2-beta.3...v1.0.2-beta.4) (2023-04-11)



## [1.0.2-beta.3](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.0.2-beta.2...v1.0.2-beta.3) (2023-03-20)



## [1.0.2-beta.2](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.0.2-beta.1...v1.0.2-beta.2) (2023-03-15)



## [1.0.2-beta.1](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.0.2-beta.0...v1.0.2-beta.1) (2023-03-14)



## [1.0.2-beta.0](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.0.1...v1.0.2-beta.0) (2023-03-09)



## [1.0.1](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v1.0.0...v1.0.1) (2022-10-07)



# [1.0.0](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui/compare/v0.0.3...v1.0.0) (2022-10-06)



