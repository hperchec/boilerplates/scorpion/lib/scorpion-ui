/**
 * @vuepress
 * ---
 * title: safe
 * headline: safe
 * sidebarTitle: safe
 * ---
 */

/**
 * safe
 * @param {Function} func - The function to execute
 * @returns {*} Returns the value returned by `func`, else Error
 * @description
 * The sync version of {@link ./safe-async safeAsync}.
 * Encapsulate in a try/catch block a function to execute
 * @example
 * safe(function () {
 *   // Some actions that can throw errors/exceptions
 * })
 */
export const safe = function (func) {
  try {
    const result = func()
    return result
  } catch (err) {
    return err
  }
}

export default safe
