/**
 * @vuepress
 * ---
 * title: "Service (local-database-manager): LocalDatabase class"
 * headline: "Service (local-database-manager): LocalDatabase class"
 * sidebarTitle: .LocalDatabase
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import Dexie from 'dexie'
import { applyEncryptionMiddleware } from 'dexie-encrypted'
import { applyCompressionMiddleware } from './dexie-compressed'
import Core from '@/Core'
import { camelcase, getAllKeys, typeCheck } from '@/utils'
import LocalDatabaseOptions from './LocalDatabaseOptions' // jsDoc type

// @ts-ignore
const log = (...args) => Core.service('logger').consoleLog(...args)

/**
 * @classdesc
 * Service **local-database-manager**: LocalDatabase class
 */
class LocalDatabase {
  /**
   * @private
   */
  _name
  _driver
  _driverOptions
  _initCallback
  _encryptionKey = null
  _methodsMap
  _instance = null

  /**
   * Create a new LocalDatabase
   * @param {LocalDatabaseOptions} localDatabaseOptions - An LocalDatabaseOptions object
   */
  constructor (localDatabaseOptions) {
    if (typeCheck(Core.context.services['local-database-manager'].LocalDatabaseOptions, localDatabaseOptions)) {
      this.name = localDatabaseOptions.name
      this.setDriver(localDatabaseOptions.driver)
      this.driverOptions = localDatabaseOptions.driverOptions
      this.initCallback = localDatabaseOptions.init
    } else {
      throw new TypeError('LocalDatabase class : constructor failed. LocalDatabaseOptions object attempted, received : ' + typeof localDatabaseOptions)
    }
  }

  /**
   * Accessors & mutators
   */

  /**
   * LocalDatabase name
   * @category properties
   * @type {string}
   */
  get name () {
    return this._name
  }

  set name (value) {
    // Check type
    if (typeCheck(String, value)) {
      // Assign
      this._name = value
    } else {
      // Else -> throw error
      throw new Error('LocalDatabase class: \'name\' property must be String. ' + typeof value + ' received...')
    }
  }

  /**
   * LocalDatabase driver options
   * @category properties
   * @type {string}
   */
  get driverOptions () {
    return this._driverOptions
  }

  set driverOptions (value) {
    // Check type
    if (typeCheck(Object, value)) {
      // Assign
      this._driverOptions = value
    } else {
      // Else -> throw error
      throw new Error('LocalDatabase class: \'driverOptions\' property must be Object. ' + typeof value + ' received...')
    }
  }

  /**
   * Get the LocalDatabase driver
   * @category properties
   * @readonly
   * @type {object}
   */
  get driver () {
    return this._driver
  }

  /**
   * LocalDatabase init method
   * @category properties
   * @type {Function}
   */
  get initCallback () {
    return this._initCallback
  }

  set initCallback (value) {
    // Check type
    if (typeCheck(Function, value)) {
      // Assign
      this._initCallback = value
    } else {
      // Else -> throw error
      throw new Error('LocalDatabase class: \'initCallback\' property must be Function. ' + typeof value + ' received...')
    }
  }

  /**
   * LocalDatabase encryption key
   * @category properties
   * @type {Uint8Array}
   */
  get encryptionKey () {
    return this._encryptionKey
  }

  set encryptionKey (value) {
    // Check type
    if (typeCheck(Uint8Array, value)) {
      // Assign
      this._encryptionKey = value
    } else {
      // Else -> throw error
      throw new Error('LocalDatabase class: \'encryptionKey\' property must be Uint8Array. ' + typeof value + ' received...')
    }
  }

  /**
   * Methods
   */

  /**
   * setDriver
   * @category methods
   * @param {string} name - The driver name
   * @param {object} [driverOptions] - The driver options/configuration
   * @returns {void}
   * @description
   * Set the driver for the LocalDatabase
   */
  setDriver (name, driverOptions) {
    if (LocalDatabase.supportedDrivers.indexOf(name) > -1) {
      this[camelcase(`init_${name}`)](driverOptions || this.driverOptions)
    }
  }

  /**
   * mapDriverMethods
   * @category methods
   * @param {object} methods - A methods object
   * @returns {void}
   * @description
   * Map driver methods for requests
   *
   * ```js
   * {
   *   init: methods.init, // this.init([<initCallback>]),
   *   useEncryption: methods.useEncryption, // this.useEncryption(...),
   *   useCompression: methods.useCompression, // this.useCompression(...),
   *   open: methods.open, // this.open(),
   *   close: methods.close, // this.close()
   * }
   * ```
   */
  mapDriverMethods (methods) {
    const genError = (val) => { throw new Error('LocalDatabase class : unmapped or unsupported driver method "' + val + '"') }
    this._methodsMap = {
      init: methods.init || function () { genError('init') },
      useEncryption: methods.useEncryption || function () { genError('useEncryption') },
      useCompression: methods.useCompression || function () { genError('useCompression') },
      open: methods.open || function () { genError('open') },
      close: methods.close || function () { genError('close') }
    }
  }

  /**
   * getInstance
   * @category methods
   * @returns {*} The instance reference
   * @description
   * getInstance method: return the instance reference
   */
  getInstance () {
    return this._instance
  }

  /**
   * init
   * @category methods
   * @param {Function} [callback] - A callback that takes the LocalDatabase as first argument
   * @returns {*} The driver returned value
   * @description
   * Main init method: example init database versions
   */
  init (callback) {
    // Call the corresponding method mapped to the driver equivalent
    return this._methodsMap.init(callback)
  }

  /**
   * useEncryption
   * @category methods
   * @param {...*} args - The driver method arguments
   * @returns {*} The driver returned value
   * @description
   * Encrypt local database
   */
  useEncryption (...args) {
    // Call the corresponding method mapped to the driver equivalent
    return this._methodsMap.useEncryption(...args)
  }

  /**
   * useCompression
   * @category methods
   * @param {...*} args - The driver method arguments
   * @returns {*} The driver returned value
   * @description
   * Compress data in local database
   */
  useCompression (...args) {
    // Call the corresponding method mapped to the driver equivalent
    return this._methodsMap.useCompression(...args)
  }

  /**
   * open
   * @category methods
   * @returns {Promise<*>} The driver returned promise
   * @description
   * Main open method: open a local database
   */
  open () {
    // Call the corresponding method mapped to the driver equivalent
    return this._methodsMap.open() // Must return a promise
  }

  /**
   * close
   * @category methods
   * @returns {*} The driver returned value
   * @description
   * Main close method: close a local database
   */
  close () {
    // Call the corresponding method mapped to the driver equivalent
    return this._methodsMap.close()
  }

  /**
   * unmount
   * @category methods
   * @returns {LocalDatabase} Returns this instance
   * @description
   * Unmount method: close a local database and delete reference. Database must be `init()` again after that
   */
  unmount () {
    this.close()
    delete this._instance
    return this
  }

  /**
   * initDexie
   * @category methods
   * @returns {void}
   * @description
   * Init Dexie as driver
   */
  initDexie () {
    // Assign driver
    this._driver = Dexie // Dexie class
    // Map
    this.mapDriverMethods({
      /**
       * init method
       * @ignore
       * @param {Function} [initCb] - A callback that takes the LocalDatabase instance itself as first argument
       * @returns {LocalDatabase} - Returns instance
       */
      init: (initCb) => {
        log(`Initializing local database: ${this.name}`, { type: 'local-database' })
        const database = this._instance = new this._driver(this.name)
        // First, process arg
        if (initCb) {
          initCb(this)
        }
        // Then, call instance level initCallback
        this.initCallback(this)
        // Check if version() has been called
        if (!database.verno) {
          throw new Error(`LocalDatabase class : version is not defined for local database: ${this.name}`)
        }
        return this
      },
      /**
       * useEncryption method
       * @ignore
       * @param {object} [config] - The config object
       * @param {(db: any) => Promise<any>} [onKeyChange] - The onKeyChange callback
       * @returns {LocalDatabase} - Returns instance
       * @description
       * See also: [dexie-encrypted](https://www.npmjs.com/package/dexie-encrypted) package documentation
       */
      useEncryption: (config, onKeyChange) => {
        log(`Encrypt local database: ${this.name} 🔐`, { type: 'local-database' })
        const database = this._instance
        applyEncryptionMiddleware(database, this.encryptionKey, config, onKeyChange)
        return this
      },
      /**
       * useCompression method
       * @ignore
       * @param {object} [options = {}] - The options
       * @returns {LocalDatabase} - Returns instance
       */
      useCompression: (options = {}) => {
        log(`Using compression for local database: ${this.name}`, { type: 'local-database' })
        const database = this._instance
        applyCompressionMiddleware(database, options)
        return this
      },
      /**
       * open method
       * @ignore
       * @returns {Promise<Dexie>} - A promise resolved with Dexie instance
       */
      open: () => {
        log(`Opening local database: ${this.name} ▶`, { type: 'local-database' })
        const database = this._instance
        return database.open()
      },
      /**
       * close method
       * @ignore
       * @returns {LocalDatabase} - Returns instance
       */
      close: () => {
        log(`Closing local database: ${this.name} 🚫`, { type: 'local-database' })
        const database = this._instance
        database.close()
        return this
      }
    })
  }

  /**
   * Overrides the default toJSON object method for JSON.stringify() calls
   * @returns {object} - The instance data to be serialized
   */
  toJSON () {
    const keys = getAllKeys(this, {
      excludeKeys: [
        /^_/, // private properties that starts with '_'
        'toJSON',
        'toString',
        'constructor'
      ]
    })
    const jsonObj = keys.reduce((obj, key) => {
      obj[key] = this[key]
      return obj
    }, {})
    return jsonObj
  }

  /**
   * Static
   */

  /**
   * supportedDrivers
   * @category properties
   * @readonly
   * @type {string[]}
   * @description
   * Only 'dexie' is supported as a driver for the moment
   */
  static get supportedDrivers () {
    return [ 'dexie' ] // (No more supported 'drivers' for the moment...)
  }
}

export default LocalDatabase
