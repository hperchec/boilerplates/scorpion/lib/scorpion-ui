/**
 * @vuepress
 * ---
 * title: appendPrototypeChain
 * headline: appendPrototypeChain
 * sidebarTitle: appendPrototypeChain
 * ---
 */

/**
 * Function inspired from https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Object/setPrototypeOf#ajouter_une_cha%C3%AEne_de_prototypes_%C3%A0_un_objet
 * @param {*} oChain - The target object
 * @param {*} oProto - The prototype to add to chain
 * @returns {void}
 */
export const appendPrototypeChain = function (oChain, oProto) {
  if (arguments.length < 2) {
    throw new TypeError('appendPrototypeChain - not enough arguments')
  }

  //   const newOProto = typeof oProto === 'function' ? oProto.prototype : oProto
  const newOProto = oProto
  //   const oLast = oChain instanceof Object ? oChain : new oChain.constructor(oChain)
  const oLast = oChain
  let o2nd = oLast
  const oReturn = o2nd

  for (
    let o1st = Object.getPrototypeOf(o2nd);
    o1st !== Object.prototype && o1st !== Function.prototype;
    o1st = Object.getPrototypeOf(o2nd)
  ) {
    o2nd = o1st
  }

  Object.setPrototypeOf(o2nd, newOProto)
  return oReturn
}

export default appendPrototypeChain
