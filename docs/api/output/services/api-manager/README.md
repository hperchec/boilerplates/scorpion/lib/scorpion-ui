---
title: Service (api-manager)
headline: Service (api-manager)
sidebarTitle: api-manager
initialOpenGroupIndex: -1
prev: ../
next: ./options
---

# Service (api-manager)

<a name="service"></a>

The service "api-manager" is designed to make API managemnent easier.It references any API needed and provides methods and tools to make requests.

## Register

On registering, the service will add to support:- `Core.context.support.errors.RequestError`: see [RequestError](./support/errors/RequestError)

## Create

Returns an instance of `APImanager` created with options.

**Returns**: <code>module:services/api-manager/APIManager~APIManager</code> - Once services are created:

```js
Core.service('api-manager')
```

## Vue

### Plugin

On registering, service Vue plugin will be set in `Core.context.vue.plugins['api-manager']`.

Plugin will be automatically used by Vue.

The following properties will be set to Vue prototype:```jsVue.prototype.$APIManager // => Core.service('api-manager')```

### Root instance

<a name="app"></a>

#### Prototype $app property

On registering, the service will inject properties to \`Core.context.vue.root.prototype.app\`.

> Properties will be shared by all components under \`vm.$app\` where \`vm\` is the component instance.

<a name="app-usersettings"></a>

##### User settings

Service will define the following user settings keys:

##### Others

## Expose (service context)

The service will expose the following properties under `Core.context.services['api-manager']`:
<a name="expose-api">
</a>

### .API

> Exposed as `Core.context.services['api-manager'].API`

**See**: [API](./API)

<a name="expose-apimanager">
</a>

### .APIManager

> Exposed as `Core.context.services['api-manager'].APIManager`

**See**: [APIManager](./APIManager)

<a name="expose-apioptions">
</a>

### .APIOptions

> Exposed as `Core.context.services['api-manager'].APIOptions`

**See**: [APIOptions](./APIOptions)

