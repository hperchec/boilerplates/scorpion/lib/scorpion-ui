/**
 * @vuepress
 * ---
 * title: "Service (keyboard-event-manager): KeyboardEventManager class"
 * headline: "Service (keyboard-event-manager): KeyboardEventManager class"
 * sidebarTitle: .KeyboardEventManager
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import hotkeys from 'hotkeys-js'

import { getAllKeys, isArray, typeCheck } from '@/utils'
import toolbox from '@/toolbox'

const { VuePluginable } = toolbox.vue

/**
 * @extends VuePluginable
 * @classdesc
 * Service **keyboard-event-manager**: KeyboardEventManager class
 */
class KeyboardEventManager extends VuePluginable {
  /**
   * Create a new instance
   */
  constructor () {
    const initVMData = {
      currentPrompt: {
        active: false,
        expectedKeys: [],
        resolvedKeys: []
      }
    }
    // Call VuePluginable constructor
    super(initVMData)
    // Set debugging private property
    if (this.__v_descriptor__) {
      this.__v_descriptor__.namespace = 'service:keyboard-event-manager'
      this.__v_descriptor__.description = 'Access the KeyboardEventManager instance'
      // @ts-ignore
      this.__v_descriptor__.externalLink = __getDocUrl__('/api/services/keyboard-event-manager/KeyboardEventManager')
    }
  }

  /**
   * Private properties
   */

  /**
   * scopes
   * @category private properties
   * @type {Set}
   * @description
   * Store the defined scopes
   */
  #scopes = new Set()

  /**
   * Overrides static VuePluginable properties
   */
  static reactive = true
  static rootOption = 'keyboardEventManager'
  static aliases = [
    'keyboardEvents'
  ]

  /**
   * Accessors & mutators
   */

  /**
   * scopes
   * @category properties
   * @type {Set}
   * @description
   * Returns defined scopes.
   */
  get scopes () {
    return this.#scopes
  }

  /**
   * Methods
   */

  /**
   * @category methods
   * @param {string} scope - The scope as string like **hotkeys** scope argument
   * @param {string} keys - Like **hotkeys** first argument. Will be prefixed by 'ctrl+'
   * @param {Function} onKeydown - The keydown listener method as **hotkeys** accepts
   * @param {Function} onKeyup - The keyup listener method as **hotkeys** accepts
   * @description
   * Create a listener to handle KeyboardEvent "ctrl"
   *
   * See [hotkeys-js](https://www.npmjs.com/package/hotkeys-js) package documentation.
   */
  ctrlKey (scope, keys, onKeydown, onKeyup) {
    const fullKeys = `ctrl+${keys}`
    // @ts-ignore
    hotkeys(fullKeys, { keyup: true }, this.#wrapHotKeysListener(scope, onKeydown, onKeyup))
  }

  /**
   * @category methods
   * @param {string} scope - The scope as string like **hotkeys** scope argument
   * @param {string} keys - Like **hotkeys** first argument. Will be prefixed by 'alt+'
   * @param {Function} onKeydown - The keydown listener method as **hotkeys** accepts
   * @param {Function} onKeyup - The keyup listener method as **hotkeys** accepts
   * @description
   * Create a listener to handle KeyboardEvent "alt"
   *
   * See [hotkeys-js](https://www.npmjs.com/package/hotkeys-js) package documentation.
   */
  altKey (scope, keys, onKeydown, onKeyup) {
    const fullKeys = `alt+${keys}`
    // @ts-ignore
    hotkeys(fullKeys, { keyup: true }, this.#wrapHotKeysListener(scope, onKeydown, onKeyup))
  }

  /**
   * @category methods
   * @param {string} scope - The scope as string like **hotkeys** scope argument
   * @param {string} keys - Like **hotkeys** first argument. Will be prefixed by 'ctrl+alt+'
   * @param {Function} onKeydown - The keydown listener method as **hotkeys** accepts
   * @param {Function} onKeyup - The keyup listener method as **hotkeys** accepts
   * @description
   * Create a listener to handle KeyboardEvent "ctrl+alt"
   *
   * See [hotkeys-js](https://www.npmjs.com/package/hotkeys-js) package documentation.
   */
  ctrlAltKey (scope, keys, onKeydown, onKeyup) {
    const fullKeys = `ctrl+alt+${keys}`
    // @ts-ignore
    hotkeys(fullKeys, { keyup: true }, this.#wrapHotKeysListener(scope, onKeydown, onKeyup))
  }

  /**
   * @category methods
   * @param {string[]|Array[]} keys - The keys as array of string or array with hook callback like : `[ key, (resolvedString: String[], expectedKey: String, keyIndex: Number, event: KeyboardEvent) => ?boolean ]`.
   * The hook callback is a function that takes fours arguments: the resolved string, the expected key, the current index and the event itself.
   * It can return false to interrupt
   * @param {Function} [resolvedCb] - The resolved callback. Takes the resolved string as unique argument: `(resolvedString: String[]) => void`
   * @param {Function} [badCombinationCb] - The callback in case of not expected key. Takes five arguments: the resolved string, the expected key, the current index, the received key that does not match the expected one and the event itself.
   * `(resolvedString: String[], expectedKey: String, keyIndex: Number, eventKey: KeyboardEvent.key, event: KeyboardEvent) => void`
   * @returns {object} - Returns an object with the abort function
   * @example
   * keyboardEventManager.attemptPrompt(['f', 'o', 'o'], (resolvedKeys) => { console.log('Resolved: ', resolvedKeys.join()) })
   * // Pass hooks
   * keyboardEventManager.attemptPrompt(
   *   [
   *     [ 'f', (resolvedKeys, expectedKey, keyIndex, event) => {
   *       console.log('Oh, a "F"!')
   *     } ],
   *     'o',
   *     [ 'o', (resolvedKeys, expectedKey, keyIndex, event) => {
   *       console.log('The last "O"!')
   *     } ]
   *   ],
   *   // successCb
   *   (resolvedKeys) => {
   *     console.log('That\'s all guys!')
   *   },
   *   // bad combination callback
   *   (resolvedKeys, expectedKey, keyIndex, eventKey, event) => {
   *     console.log(`The key "${eventKey}" is not valid!`)
   *   }
   * )
   */
  attemptPrompt (keys, resolvedCb, badCombinationCb) {
    const self = this
    const hooks = []
    const expectedKeys = []
    let i = 0
    for (const key of keys) {
      if (isArray(key)) {
        expectedKeys[i] = key[0]
        hooks[i] = key[1] || (() => {})
      } else {
        expectedKeys[i] = key
        hooks[i] = () => {}
      }
      i++
    }
    if (!resolvedCb) resolvedCb = () => {}
    if (!badCombinationCb) badCombinationCb = () => {}

    let expectedKey
    let expectedIndex

    const compareKeys = (value, expected) => {
      let match = true
      for (const i in value) {
        if (value[i] !== expected[i]) {
          match = false
          expectedKey = expected[i]
          expectedIndex = i
          break
        }
      }
      return match
    }

    const prompt = self.#setCurrentPrompt(expectedKeys)

    self._vm.$on('keydown', function ({ data, abort }) {
      const newValue = ([ ...prompt.resolvedKeys, data.key ])
      if (compareKeys(newValue, expectedKeys)) {
        prompt.resolvedKeys.push(data.key)
        const keyIndex = prompt.resolvedKeys.length - 1
        // Call hook
        hooks[keyIndex](prompt.resolvedKeys, data.key, keyIndex, data.event)
        // If all keys match
        if (prompt.resolvedKeys.length === expectedKeys.length) {
          // Abort signal for high-level listener
          abort()
          // ✔ Resolved
          resolvedCb(prompt.resolvedKeys)
        }
      } else {
        // Abort signal for high-level listener
        abort()
        // Call bad combination callback
        badCombinationCb(prompt.resolvedKeys, expectedKey, expectedIndex, data.key, data.event)
        // Reset resolved keys
        prompt.resolvedKeys = []
      }
    })

    const controller = new AbortController()

    const abort = () => {
      controller.abort()
      self._vm.$off('keydown')
      prompt.active = false
    }

    const listener = function (event) {
      self._vm.$emit('keydown', {
        data: { key: event.key, event },
        // Pass an "abort" callback
        abort
      })
    }

    document.addEventListener('keydown', listener, { signal: controller.signal })

    return {
      abort
    }
  }

  /**
   * Overrides the default toJSON object method for JSON.stringify() calls
   * @returns {object} - The instance data to be serialized
   */
  toJSON () {
    const keys = getAllKeys(this, {
      excludeKeys: [
        /^_/, // private properties that starts with '_'
        'toJSON',
        'toString',
        'constructor'
      ]
    })
    const jsonObj = keys.reduce((obj, key) => {
      obj[key] = this[key]
      return obj
    }, {})
    return jsonObj
  }

  /**
   * Private methods
   */

  /**
   * @category methods
   * @param {string} scope - The scope name
   * @returns {void}
   * @description
   * Add a scope to private scopes set
   */
  #addScope (scope) {
    if (typeCheck(String, scope)) {
      this.#scopes.add(scope)
    } else {
      throw new Error('KeyboardEventManager can\'t add scope. Attempt string and received: ' + typeof scope)
    }
  }

  /**
   * @category methods
   * @param {string} scope - The scope
   * @param {Function} onKeydown - The keydown listener function
   * @param {Function} onKeyup - The keyup listener function
   * @param {object} [options] - The options object
   * @param {boolean} [options.prevent = true] - If prevent default event behavior
   * @returns {Function} - The wrapped listener fucntion
   * @description
   * Wrap a hotkeys-js like listener
   */
  #wrapHotKeysListener (scope, onKeydown, onKeyup, options = { prevent: true }) {
    const self = this
    return function (event, handler) {
      if (options.prevent) event.preventDefault()
      hotkeys.setScope(scope)
      self.#addScope(scope)
      ;(event.type === 'keydown' ? onKeydown : onKeyup)(event, handler)
      hotkeys.setScope('all')
      return options.prevent ? false : undefined
    }
  }

  /**
   * @category methods
   * @param {string[]} expectedKeys - The expected keys
   * @returns {object} Return the current prompt reference
   * @description
   * Set the current prompt
   */
  #setCurrentPrompt (expectedKeys) {
    this._vm.currentPrompt.expectedKeys = expectedKeys
    this._vm.currentPrompt.resolvedKeys = [] // always reset relsolved keys
    this._vm.currentPrompt.active = true
    return this._vm.currentPrompt
  }

  /**
   * Static
   */

  /**
   * Install method for Vue
   * @param {Vue} _Vue - Vue
   * @param {object} [options = {}] - The options of the plugin
   */
  static install (_Vue, options = {}) {
    super.install(_Vue)
  }
}

export default KeyboardEventManager
