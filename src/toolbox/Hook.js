/**
 * @vuepress
 * ---
 * title: Hook class
 * headline: Hook class
 * sidebarTitle: Hook
 * sidebarDepth: 0 # To disable auto sidebar links
 * ---
 */

import { compose, isArray, isDef, typeCheck } from '@/utils'

/**
 * Hook class
 * @typicalname hook
 * @classdesc Hook class.
 */
class Hook {
  /**
   * Private
   */
  #name
  #queue

  /**
   * Create a new Hook
   * @param {string} name - The hook identifier
   * @param {Function[]} [queue = []] - The base queue
   */
  constructor (name, queue = []) {
    // Required options
    this.setName(name)
    // Optional
    this.setQueue(queue) // Empty queue by default
  }

  /**
   * Accessors & mutators
   */

  /**
   * name
   * @type {string}
   * @readonly
   * @default undefined
   */
  get name () {
    return this.#name
  }

  /**
   * queue
   * @type {Function[]}
   * @readonly
   * @default []
   */
  get queue () {
    return this.#queue
  }

  /**
   * Methods
   */

  /**
   * Set name property
   * @param {string} value - The name
   * @returns {void}
   */
  setName (value) {
    if (!typeCheck(String, value)) {
      throw new Error('Hook class: "name" property must be String.')
    }
    this.#name = value
  }

  /**
   * Set queue property
   * @param {Function[]} value - The queue (array of function)
   * @returns {void}
   */
  setQueue (value) {
    if (!typeCheck(Array, value)) {
      throw new Error('Hook class: "queue" property must be Array.')
    }
    // Loop on each elements
    for (const func of value) {
      if (!typeCheck(Function, func)) {
        throw new Error('Hook class: "queue" property must be an Array of Function. An item is not a function.')
      }
    }
    this.#queue = value
  }

  /**
   * Add a function to exec to the queue
   * @param {Function|Function[]} func - The function to append to the hook queue. Can be an array of functions.
   * @returns {void}
   */
  append (func) {
    const pushToQueue = (f) => {
      if (!typeCheck(Function, f)) {
        throw new Error('Hook class: the item to append to the queue must be Function type.')
      }
      // Push new function to queue
      this.#queue.push(f)
    }
    if (isDef(func)) {
      if (isArray(func)) {
        // @ts-ignore
        for (const _func of func) {
          pushToQueue(_func)
        }
      } else {
        pushToQueue(func)
      }
    }
  }

  /**
   * exec
   * @param {...*} args - The arguments to pass to each queue function
   * @returns {any[]|Promise<any[]>} The array of result. If at least one result is a promise, so the entire result is a Promise
   * @description
   * Execute the queue asynchronously (in the order of execution)
   */
  exec (...args) {
    const executed = []
    for (const func of this.queue) {
      executed.push(func(...args))
    }
    return executed.some((result) => result instanceof Promise)
      ? Promise.all(executed).then((values) => values)
      : executed
  }

  /**
   * compose
   * @param {...*} args - The arguments to pass to each queue function
   * @returns {Array|Promise<Array>} Each function must return its own arguments as array
   * @description
   * Execute the queue in a "compose" style (from the last to the first): each returned value is passed to the previous queued function.
   * If a queued function returns undefined, the last value of payload is passed.
   * If a returned value is a promise, so the entire result will be Promise
   */
  compose (...args) {
    return compose(this.queue, args)
  }

  /**
   * chain
   * @param {...*} args - The arguments to pass to each queue function
   * @returns {Array|Promise<Array>} Each function must return its own arguments as array
   * @description
   * Execute the queue in a "chain" style (in the order of execution): wait for each returned value and then pass it to the next queued function.
   * If a queued function returns undefined, the last value of payload is passed.
   * If a returned value is a promise, so the entire result will be Promise
   */
  chain (...args) {
    // First reverse queue
    this.queue.reverse()
    let chainedHook
    try {
      chainedHook = this.compose(...args)
    } catch (err) {
      // Reverse again to retrieve original order before throwing error
      this.queue.reverse()
      throw err
    }
    // Reverse again to retrieve original order
    this.queue.reverse()
    return chainedHook
  }
}

export default Hook
