---
title: Service (api-manager) options
headline: Service (api-manager) options
sidebarTitle: Options
sidebarDepth: 0
prev: false
next: false
---

# Service (api-manager) options

<a name="options">
</a>

## options

API manager service options

