/**
 * @vuepress
 * ---
 * title: "Service (toast-manager): ToastManager class"
 * headline: "Service (toast-manager): ToastManager class"
 * sidebarTitle: .ToastManager
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import Core from '@/Core'
import toolbox from '@/toolbox'
import { getAllKeys, typeCheck } from '@/utils'
import Toaster from './Toaster' // jsDoc type

const { VuePluginable } = toolbox.vue

/**
 * @extends VuePluginable
 * @classdesc
 * Service **toast-manager**: ToastManager class
 */
class ToastManager extends VuePluginable {
  /**
   * Create a new Toaster Manager
   * @param {object} options - An array of Toaster
   * @param {Toaster[]} options.toasters - An array of Toaster
   */
  constructor (options) {
    const tmpToasters = []
    // Loop on received options
    options.toasters.forEach((toaster) => {
      // Check if item is a instance of Toaster
      if (typeCheck(Core.context.services['toast-manager'].Toaster, toaster)) {
        tmpToasters.push(toaster)
      } else {
        throw new Error('ToastManager can\'t load Toaster. One of them is not an Toaster instance')
      }
    })
    const initVMData = {
      toasters: tmpToasters
    }
    // Call VuePluginable constructor
    super(initVMData)
    // Set debugging private property
    if (this.__v_descriptor__) {
      this.__v_descriptor__.namespace = 'service:toast-manager'
      this.__v_descriptor__.description = 'Access the ToastManager instance'
      // @ts-ignore
      this.__v_descriptor__.externalLink = __getDocUrl__('/api/services/toast-manager/ToastManager')
    }
  }

  /**
   * Overrides static VuePluginable properties
   */
  static reactive = true
  static rootOption = 'toastManager'
  static aliases = []

  /**
   * Accessors & mutators
   */

  /**
   * Registered Toasters
   * @category properties
   * @readonly
   * @type {Toaster[]}
   */
  get toasters () {
    return this._vm.toasters
  }

  /**
   * Methods
   */

  /**
   * add
   * @category methods
   * @param {Toaster} toaster - The Toaster to add to manager
   * @returns {Toaster} Returns the Toaster
   * @description
   * Add a Toaster to manager anytime
   */
  add (toaster) {
    // Check if item is a instance of Toaster
    if (typeCheck(Core.context.services['toast-manager'].Toaster, toaster)) {
      // Check in registered Toasters
      const toasterExists = this.toaster(toaster.name)
      if (!toasterExists) {
        this._vm.toasters.push(toaster)
      } else {
        console.log('WARNING: ToastManager can\'t add Toaster: "' + toaster.name + '" is already set...')
      }
    } else {
      throw new Error('ToastManager can\'t load Toaster. One of them is not an Toaster instance')
    }
    return this.toaster(toaster.name)
  }

  /**
   * toaster
   * @category methods
   * @param {string} name - The Toaster name to target
   * @returns {Toaster} Returns the Toaster
   * @description
   * Use a specific Toaster that was loaded by the manager
   */
  toaster (name) {
    // Check arg type
    if (typeof name !== 'string') {
      throw new TypeError('ToastManager service : "toaster" method attempts String type for "name" parameter. Received : ' + typeof name)
    }
    // Find by name
    const toaster = this.toasters.find(t => t.name === name)
    // If found
    if (toaster) {
      return toaster
    } else {
      // Not found -> throw Error
      throw new Error('ToastManager service : "toaster" method can\'t find Toaster by name "' + name + '"')
    }
  }

  /**
   * Overrides the default toJSON object method for JSON.stringify() calls
   * @returns {object} - The instance data to be serialized
   */
  toJSON () {
    const keys = getAllKeys(this, {
      excludeKeys: [
        /^_/, // private properties that starts with '_'
        'toJSON',
        'toString',
        'constructor'
      ]
    })
    const jsonObj = keys.reduce((obj, key) => {
      obj[key] = this[key]
      return obj
    }, {})
    return jsonObj
  }

  /**
   * Static
   */

  /**
   * Install method for Vue
   * @param {Vue} _Vue - Vue
   * @param {object} [options={}] - The options of the plugin
   */
  static install (_Vue, options = {}) {
    const Vue = _Vue
    super.install(Vue, options)
  }
}

export default ToastManager
