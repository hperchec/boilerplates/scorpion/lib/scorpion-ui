export function publicMiddleware(middlewareFunc: Function, metaKey?: string): Function;
export default publicMiddleware;
//# sourceMappingURL=public-middleware.d.ts.map