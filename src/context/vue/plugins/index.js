/**
 * @vuepress
 * ---
 * title: Plugins
 * headline: Plugins
 * sidebarTitle: Plugins
 * initialOpenGroupIndex: -1 # optional, defaults to 0, defines the index of initially opened subgroup
 * prev: ../mixins/
 * next: ./core-plugin/
 * ---
 */

import Core from '@/Core'

// Core plugin
import CorePlugin from './core-plugin'
// vue-reactive-refs
import ReactiveRefs from './reactive-refs'

/**
 * @alias plugins
 * @typicalname Core.context.vue.plugins
 * @description
 * Contains all Vue plugins that will be automatically registered.
 *
 * Each plugin is an object that has the following properties:
 *
 * - `plugin` object that will be used by Vue
 * - `options` object to pass to the plugin
 *
 * ::: tip See also
 * Core {@link ../../../Core#core-registervueplugin registerVuePlugin} method documentation
 * :::
 */
export default new Proxy({
  /**
   * @alias plugins.CorePlugin
   */
  CorePlugin: CorePlugin,
  /**
   * @alias plugins.ReactiveRefs
   */
  ReactiveRefs: ReactiveRefs
}, {
  set (obj, prop, value, receiver) {
    if (receiver === Core) {
      obj[prop] = value
      return true
    } else {
      throw new Error('Use Core.registerVuePlugin() method to register a Vue plugin.')
    }
  }
})
