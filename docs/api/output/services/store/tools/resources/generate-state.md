---
title: generate-state
headline: generate-state
---

# generate-state

## Members

<dl>
<dt><a href="#$data">$data</a> : <code>object</code></dt>
<dd><p>Resource collection</p>
</dd>
<dt><a href="#loadingStatus">loadingStatus</a> : <code>object</code></dt>
<dd><p>Save the loading status</p>
</dd>
</dl>

<a name="data">
</a>

## $data : <code>object</code>

Resource collection

<a name="loadingstatus">
</a>

## loadingStatus : <code>object</code>

Save the loading status

