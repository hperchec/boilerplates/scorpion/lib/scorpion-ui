// import lz from 'lz-string'
import Dexie from 'dexie'

/**
 * Apply compression middleware
 * @param {Dexie} database - The Dexie instance
 * @param {object} options - The options object
 * @param {object} [options.tables] - The table to compress
 * @param {object} [options.defaultEngine] - The default engine option object
 * @param {Function} [options.defaultEngine.compress] - The default engine compress method
 * @param {Function} [options.defaultEngine.decompress] - The default engine decompress method
 * @returns {Dexie} - The Dexie instance
 */
export function applyCompressionMiddleware (database, options) {
  // const defaultEngine = {
  //   compress: options.defaultEngine.compress || ((...args) => lz.compress(...args)),
  //   decompress: options.defaultEngine.decompress || ((...args) => lz.decompress(...args))
  // }

  // return database.use({
  //   stack: 'dbcore',
  //   name: 'compression',
  //   level: 1,
  //   create (downlevelDatabase) {
  //     return {
  //       ...downlevelDatabase,
  //       table (tn) {
  //         const tableName = tn
  //         const table = downlevelDatabase.table(tableName)
  //         console.log('table name ? ', tableName)
  //         if (tableName in options.tables === false) {
  //           return table
  //         }

  //         console.log('table name ? ', tableName)

  //         // const compressionSetting = options[tableName]

  //         function compress (data) {
  //           // return encryptEntity(
  //           //   table,
  //           //   data,
  //           //   encryptionSetting,
  //           //   encryptionKey,
  //           //   performEncryption,
  //           //   nonceOverride
  //           // )
  //           return data
  //         }

  //         function decompress (data) {
  //           // return decryptEntity(
  //           //   data,
  //           //   encryptionSetting,
  //           //   encryptionKey,
  //           //   performDecryption
  //           // )
  //           return data
  //         }

  //         return {
  //           ...table,
  //           openCursor (req) {
  //             return table.openCursor(req).then(cursor => {
  //               if (!cursor) {
  //                 return cursor
  //               }
  //               return Object.create(cursor, {
  //                 continue: {
  //                   get () {
  //                     return cursor.continue
  //                   }
  //                 },
  //                 continuePrimaryKey: {
  //                   get () {
  //                     return cursor.continuePrimaryKey
  //                   }
  //                 },
  //                 key: {
  //                   get () {
  //                     return cursor.key
  //                   }
  //                 },
  //                 value: {
  //                   get () {
  //                     return decompress(cursor.value)
  //                   }
  //                 }
  //               })
  //             })
  //           },
  //           get (req) {
  //             return table.get(req).then(decompress)
  //           },
  //           getMany (req) {
  //             return table.getMany(req).then(items => {
  //               return items.map(decompress)
  //             })
  //           },
  //           query (req) {
  //             return table.query(req).then(res => {
  //               return {
  //                 ...res,
  //                 result: res.result.map(decompress)
  //               }
  //             })
  //           },
  //           mutate (req) {
  //             if (req.type === 'add' || req.type === 'put') {
  //               return table.mutate({
  //                 ...req,
  //                 values: req.values.map(compress)
  //               })
  //             }
  //             return table.mutate(req)
  //           }
  //         }
  //       }
  //     }
  //   }
  // })

  return database.use({
    stack: 'dbcore', // The only stack supported so far.
    name: 'MyMiddleware', // Optional name of your middleware
    create (downlevelDatabase) {
      // Return your own implementation of DBCore:
      return {
        // Copy default implementation.
        ...downlevelDatabase,
        // Override table method
        table (tableName) {
          // Call default table method
          const downlevelTable = downlevelDatabase.table(tableName)
          // Derive your own table from it:
          return {
            // Copy default table implementation:
            ...downlevelTable,
            // Override the mutate method:
            mutate: req => {
              // Copy the request object
              const myRequest = { ...req }
              // Do things before mutate, then
              // call downlevel mutate:
              return downlevelTable.mutate(myRequest).then(res => {
                // Do things after mutate
                const myResponse = { ...res }
                // Then return your response:
                return myResponse
              })
            }
          }
        }
      }
    }
  })
}

export default applyCompressionMiddleware
