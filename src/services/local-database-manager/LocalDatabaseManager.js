/**
 * @vuepress
 * ---
 * title: "Service (local-database-manager): LocalDatabaseManager class"
 * headline: "Service (local-database-manager): LocalDatabaseManager class"
 * sidebarTitle: .LocalDatabaseManager
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import Core from '@/Core'
import { getAllKeys, typeCheck } from '@/utils'
import toolbox from '@/toolbox'
import LocalDatabase from './LocalDatabase' // jsDoc type

const { VuePluginable } = toolbox.vue

/**
 * @extends VuePluginable
 * @classdesc
 * Service **local-database-manager**: LocalDatabaseManager class
 */
class LocalDatabaseManager extends VuePluginable {
  /**
   * Create a new local database manager
   * @param {object} options - Constructor options
   * @param {LocalDatabase[]} options.databases - An array of LocalDatabase
   */
  constructor (options) {
    const tmpDatabases = []
    // Loop on received options
    options.databases.forEach((database) => {
      // Check if item is a instance of LocalDatabase
      const _LocalDatabase = Core.context.services['local-database-manager'].LocalDatabase
      if (typeCheck(_LocalDatabase, database)) {
        tmpDatabases.push(database)
      } else {
        throw new Error('LocalDatabaseManager can\'t load LocalDatabase. One of them is not an LocalDatabase instance')
      }
    })
    const initVMData = {
      databases: tmpDatabases
    }
    // Call VuePluginable constructor
    super(initVMData)
    // Set debugging private property
    if (this.__v_descriptor__) {
      this.__v_descriptor__.namespace = 'service:local-database-manager'
      this.__v_descriptor__.description = 'Access the LocalDatabaseManager instance'
      // @ts-ignore
      this.__v_descriptor__.externalLink = __getDocUrl__('/api/services/local-database-manager/LocalDatabaseManager')
    }
  }

  /**
   * Overrides static VuePluginable properties
   */
  static reactive = true
  static rootOption = 'localDatabaseManager'
  static aliases = [ 'localDB' ]

  /**
   * Accessors & mutators
   */

  /**
   * Registered databases
   * @category properties
   * @readonly
   * @type {LocalDatabase[]}
   */
  get databases () {
    return this._vm.databases
  }

  /**
   * Methods
   */

  /**
   * add
   * @category methods
   * @param {LocalDatabase} database - The database to add to manager
   * @returns {LocalDatabase} Return this.use(<database.name>)
   * @description
   * Add a database to manager anytime
   */
  add (database) {
    // Check if item is a instance of LocalDatabase
    const _LocalDatabase = Core.context.services['local-database-manager'].LocalDatabase
    if (typeCheck(_LocalDatabase, database)) {
      // Check in registered databases
      const databaseExists = this.databases.find((c) => { return database.name === c.name })
      if (!databaseExists) {
        this._vm.databases.push(database)
      } else {
        console.log('LocalDatabaseManager can\'t add database: "' + database.name + '" is already set...')
      }
    } else {
      throw new Error('LocalDatabaseManager can\'t load database. One of them is not an LocalDatabase instance')
    }
    return this.use(database.name)
  }

  /**
   * use
   * @category methods
   * @param {string} databaseName - The database name to target
   * @returns {LocalDatabase} Return the database
   * @description
   * Use a specific database that was loaded by the manager
   */
  use (databaseName) {
    // Check arg type
    if (!typeCheck(String, databaseName)) {
      throw new TypeError('LocalDatabaseManager service : "use" method attempts String type for "databaseName" parameter. Received : ' + typeof databaseName)
    }
    // Find by name
    const database = this.databases.find(c => c.name === databaseName)
    // If found
    if (database) {
      return database
    } else {
      // Not found -> throw Error
      throw new Error('LocalDatabaseManager service : "use" method can\'t find database by name "' + databaseName + '"')
    }
  }

  /**
   * Overrides the default toJSON object method for JSON.stringify() calls
   * @returns {object} - The instance data to be serialized
   */
  toJSON () {
    const keys = getAllKeys(this, {
      excludeKeys: [
        /^_/, // private properties that starts with '_'
        'toJSON',
        'toString',
        'constructor'
      ]
    })
    const jsonObj = keys.reduce((obj, key) => {
      obj[key] = this[key]
      return obj
    }, {})
    return jsonObj
  }

  /**
   * Static
   */

  /**
   * Install method for Vue
   * @param {Vue} _Vue - Vue
   * @param {object} [options = {}] - The options of the plugin
   */
  static install (_Vue, options = {}) {
    const Vue = _Vue
    super.install(Vue)
  }
}

export default LocalDatabaseManager
