module.exports = [
  {
    title: 'Default configuration',
    children: [],
    path: '/api/config/',
    initialOpenGroupIndex: 0
  },
  {
    title: 'context',
    children: [
      {
        title: 'support',
        children: [
          {
            title: 'collection',
            children: [
              {
                title: 'Collection class',
                initialOpenGroupIndex: 0,
                path: '/api/context/support/collection/Collection'
              },
              {
                title: 'Collection class',
                initialOpenGroupIndex: 0,
                path: '/api/context/support/collection/ModelCollection'
              }
            ],
            path: '/api/context/support/collection/',
            initialOpenGroupIndex: 0
          },
          {
            title: 'errors',
            children: [
              {
                title: 'AppError class',
                initialOpenGroupIndex: 0,
                path: '/api/context/support/errors/AppError'
              }
            ],
            path: '/api/context/support/errors/',
            initialOpenGroupIndex: 0
          },
          {
            title: 'model',
            children: [
              {
                title: 'Model class',
                initialOpenGroupIndex: 0,
                path: '/api/context/support/model/Model'
              }
            ],
            path: '/api/context/support/model/',
            initialOpenGroupIndex: 0
          }
        ]
      },
      {
        title: 'Vue',
        children: [
          {
            title: 'Components',
            children: [
              {
                title: 'App',
                initialOpenGroupIndex: 0,
                path: '/api/context/vue/components/App'
              },
              {
                title: 'Commons',
                children: [
                  {
                    title: 'Page',
                    initialOpenGroupIndex: 0,
                    path: '/api/context/vue/components/commons/Page'
                  },
                  {
                    title: 'ScorpionLogo',
                    initialOpenGroupIndex: 0,
                    path: '/api/context/vue/components/commons/ScorpionLogo'
                  }
                ],
                path: '/api/context/vue/components/commons/',
                initialOpenGroupIndex: 0
              }
            ],
            path: '/api/context/vue/components/',
            initialOpenGroupIndex: -1
          },
          {
            title: 'directives',
            children: [],
            path: '/api/context/vue/directives/',
            initialOpenGroupIndex: 0
          },
          {
            title: 'ensureComponentConstructor',
            initialOpenGroupIndex: 0,
            path: '/api/context/vue/ensure-component-constructor'
          },
          {
            title: 'Filters',
            children: [],
            path: '/api/context/vue/filters/',
            initialOpenGroupIndex: 0
          },
          {
            title: 'Mixins',
            children: [
              {
                title: 'global',
                children: [
                  {
                    title: 'GlobalMixin',
                    initialOpenGroupIndex: 0,
                    path: '/api/context/vue/mixins/global/GlobalMixin'
                  }
                ],
                path: '/api/context/vue/mixins/global/',
                initialOpenGroupIndex: 0
              }
            ],
            path: '/api/context/vue/mixins/',
            initialOpenGroupIndex: -1
          },
          {
            title: 'Plugins',
            children: [
              {
                title: 'core-plugin',
                children: [],
                path: '/api/context/vue/plugins/core-plugin/',
                initialOpenGroupIndex: 0
              },
              {
                title: 'reactive-refs',
                children: [],
                path: '/api/context/vue/plugins/reactive-refs/',
                initialOpenGroupIndex: 0
              }
            ],
            path: '/api/context/vue/plugins/',
            initialOpenGroupIndex: -1
          },
          {
            title: 'Root instance',
            children: [
              {
                title: 'Options',
                initialOpenGroupIndex: 0,
                path: '/api/context/vue/root/options'
              },
              {
                title: 'Prototype',
                children: [
                  {
                    title: 'app',
                    children: [
                      {
                        title: 'ui',
                        children: [],
                        path: '/api/context/vue/root/prototype/app/ui/',
                        initialOpenGroupIndex: 0
                      },
                      {
                        title: 'app.utils',
                        children: [],
                        path: '/api/context/vue/root/prototype/app/utils/',
                        initialOpenGroupIndex: -1
                      }
                    ],
                    path: '/api/context/vue/root/prototype/app/',
                    initialOpenGroupIndex: -1
                  }
                ],
                path: '/api/context/vue/root/prototype/',
                initialOpenGroupIndex: -1
              }
            ],
            path: '/api/context/vue/root/',
            initialOpenGroupIndex: -1
          }
        ],
        path: '/api/context/vue/',
        initialOpenGroupIndex: -1
      }
    ]
  },
  {
    title: 'Built-in services',
    children: [
      {
        title: 'api-manager',
        children: [
          {
            title: '.API',
            initialOpenGroupIndex: 0,
            path: '/api/services/api-manager/API'
          },
          {
            title: '.APIManager',
            initialOpenGroupIndex: 0,
            path: '/api/services/api-manager/APIManager'
          },
          {
            title: '.APIOptions',
            initialOpenGroupIndex: 0,
            path: '/api/services/api-manager/APIOptions'
          },
          {
            title: 'Configuration',
            initialOpenGroupIndex: 0,
            path: '/api/services/api-manager/config'
          },
          {
            title: 'Options',
            initialOpenGroupIndex: 0,
            path: '/api/services/api-manager/options'
          },
          {
            title: '.Request',
            initialOpenGroupIndex: 0,
            path: '/api/services/api-manager/Request'
          },
          {
            title: '.RequestManager',
            initialOpenGroupIndex: 0,
            path: '/api/services/api-manager/RequestManager'
          },
          {
            title: 'support',
            children: [
              {
                title: 'errors',
                children: [
                  {
                    title: 'RequestError',
                    initialOpenGroupIndex: 0,
                    path: '/api/services/api-manager/support/errors/RequestError'
                  }
                ],
                path: '/api/services/api-manager/support/errors/',
                initialOpenGroupIndex: 0
              }
            ],
            path: '/api/services/api-manager/support/',
            initialOpenGroupIndex: 0
          }
        ],
        path: '/api/services/api-manager/',
        initialOpenGroupIndex: -1
      },
      {
        title: 'auth-manager',
        children: [
          {
            title: '.AuthManager',
            initialOpenGroupIndex: 0,
            path: '/api/services/auth-manager/AuthManager'
          },
          {
            title: 'Options',
            initialOpenGroupIndex: 0,
            path: '/api/services/auth-manager/options'
          }
        ],
        path: '/api/services/auth-manager/',
        initialOpenGroupIndex: -1
      },
      {
        title: 'device-manager',
        children: [
          {
            title: '.Device',
            initialOpenGroupIndex: 0,
            path: '/api/services/device-manager/Device'
          },
          {
            title: '.DeviceManager',
            initialOpenGroupIndex: 0,
            path: '/api/services/device-manager/DeviceManager'
          },
          {
            title: 'mixins',
            children: [
              {
                title: 'global',
                children: [
                  {
                    title: 'DeviceManagerMixin',
                    initialOpenGroupIndex: 0,
                    path: '/api/services/device-manager/mixins/global/DeviceManagerMixin'
                  }
                ],
                path: '/api/services/device-manager/mixins/global/',
                initialOpenGroupIndex: 0
              }
            ],
            path: '/api/services/device-manager/mixins/',
            initialOpenGroupIndex: 0
          },
          {
            title: 'Options',
            initialOpenGroupIndex: 0,
            path: '/api/services/device-manager/options'
          }
        ],
        path: '/api/services/device-manager/',
        initialOpenGroupIndex: -1
      },
      {
        title: 'error-manager',
        children: [
          {
            title: 'Configuration',
            initialOpenGroupIndex: 0,
            path: '/api/services/error-manager/config'
          },
          {
            title: '.ErrorManager',
            initialOpenGroupIndex: 0,
            path: '/api/services/error-manager/ErrorManager'
          },
          {
            title: 'Options',
            initialOpenGroupIndex: 0,
            path: '/api/services/error-manager/options'
          }
        ],
        path: '/api/services/error-manager/',
        initialOpenGroupIndex: -1
      },
      {
        title: 'event-bus',
        children: [
          {
            title: 'Configuration',
            initialOpenGroupIndex: 0,
            path: '/api/services/event-bus/config'
          },
          {
            title: '.EventBus',
            initialOpenGroupIndex: 0,
            path: '/api/services/event-bus/EventBus'
          },
          {
            title: '.options',
            initialOpenGroupIndex: 0,
            path: '/api/services/event-bus/options'
          }
        ],
        path: '/api/services/event-bus/',
        initialOpenGroupIndex: -1
      },
      {
        title: 'i18n',
        children: [
          {
            title: 'Configuration',
            initialOpenGroupIndex: 0,
            path: '/api/services/i18n/config'
          },
          {
            title: '.messages',
            children: [
              {
                title: 'en',
                children: [],
                path: '/api/services/i18n/messages/en/',
                initialOpenGroupIndex: 0
              },
              {
                title: 'fr',
                children: [],
                path: '/api/services/i18n/messages/fr/',
                initialOpenGroupIndex: 0
              }
            ],
            path: '/api/services/i18n/messages/',
            initialOpenGroupIndex: 0
          },
          {
            title: '.options',
            initialOpenGroupIndex: 0,
            path: '/api/services/i18n/options'
          }
        ],
        path: '/api/services/i18n/',
        initialOpenGroupIndex: -1
      },
      {
        title: 'keyboard-event-manager',
        children: [
          {
            title: '.KeyboardEventManager',
            initialOpenGroupIndex: 0,
            path: '/api/services/keyboard-event-manager/KeyboardEventManager'
          },
          {
            title: 'Options',
            initialOpenGroupIndex: 0,
            path: '/api/services/keyboard-event-manager/options'
          }
        ],
        path: '/api/services/keyboard-event-manager/',
        initialOpenGroupIndex: -1
      },
      {
        title: 'local-database-manager',
        children: [
          {
            title: 'dexie-compressed',
            initialOpenGroupIndex: 0,
            path: '/api/services/local-database-manager/dexie-compressed'
          },
          {
            title: '.LocalDatabaseManager',
            initialOpenGroupIndex: 0,
            path: '/api/services/local-database-manager/LocalDatabaseManager'
          },
          {
            title: '.LocalDatabaseOptions',
            initialOpenGroupIndex: 0,
            path: '/api/services/local-database-manager/LocalDatabaseOptions'
          },
          {
            title: 'new-local-database',
            initialOpenGroupIndex: 0,
            path: '/api/services/local-database-manager/new-local-database'
          },
          {
            title: 'Options',
            initialOpenGroupIndex: 0,
            path: '/api/services/local-database-manager/options'
          }
        ],
        path: '/api/services/local-database-manager/',
        initialOpenGroupIndex: -1
      },
      {
        title: 'local-storage-manager',
        children: [
          {
            title: 'Configuration',
            initialOpenGroupIndex: 0,
            path: '/api/services/local-storage-manager/config'
          },
          {
            title: '.LocalStorageManager',
            initialOpenGroupIndex: 0,
            path: '/api/services/local-storage-manager/LocalStorageManager'
          },
          {
            title: '.options',
            initialOpenGroupIndex: 0,
            path: '/api/services/local-storage-manager/options'
          }
        ],
        path: '/api/services/local-storage-manager/',
        initialOpenGroupIndex: -1
      },
      {
        title: 'logger',
        children: [
          {
            title: 'components',
            children: [
              {
                title: 'commons',
                children: [
                  {
                    title: 'logger',
                    children: [
                      {
                        title: 'JsonPretty',
                        initialOpenGroupIndex: 0,
                        path: '/api/services/logger/components/commons/logger/JsonPretty'
                      }
                    ]
                  }
                ],
                path: '/api/services/logger/components/commons/',
                initialOpenGroupIndex: 0
              }
            ],
            path: '/api/services/logger/components/',
            initialOpenGroupIndex: 0
          },
          {
            title: 'Configuration',
            initialOpenGroupIndex: 0,
            path: '/api/services/logger/config'
          },
          {
            title: '.Logger',
            initialOpenGroupIndex: 0,
            path: '/api/services/logger/Logger'
          },
          {
            title: '.options',
            initialOpenGroupIndex: 0,
            path: '/api/services/logger/options'
          }
        ],
        path: '/api/services/logger/',
        initialOpenGroupIndex: -1
      },
      {
        title: 'modal-manager',
        children: [
          {
            title: 'components',
            children: [
              {
                title: 'modals',
                children: [
                  {
                    title: 'BaseModal',
                    initialOpenGroupIndex: 0,
                    path: '/api/services/modal-manager/components/modals/BaseModal'
                  },
                  {
                    title: 'Modal',
                    initialOpenGroupIndex: 0,
                    path: '/api/services/modal-manager/components/modals/Modal'
                  },
                  {
                    title: 'ModalContainer',
                    initialOpenGroupIndex: 0,
                    path: '/api/services/modal-manager/components/modals/ModalContainer'
                  },
                  {
                    title: 'ModalOverlay',
                    initialOpenGroupIndex: 0,
                    path: '/api/services/modal-manager/components/modals/ModalOverlay'
                  }
                ],
                path: '/api/services/modal-manager/components/modals/',
                initialOpenGroupIndex: 0
              }
            ],
            path: '/api/services/modal-manager/components/',
            initialOpenGroupIndex: 0
          },
          {
            title: 'Configuration',
            initialOpenGroupIndex: 0,
            path: '/api/services/modal-manager/config'
          },
          {
            title: '.Modal',
            initialOpenGroupIndex: 0,
            path: '/api/services/modal-manager/Modal'
          },
          {
            title: '.ModalCollection',
            initialOpenGroupIndex: 0,
            path: '/api/services/modal-manager/ModalCollection'
          },
          {
            title: '.ModalManager',
            initialOpenGroupIndex: 0,
            path: '/api/services/modal-manager/ModalManager'
          },
          {
            title: 'Options',
            initialOpenGroupIndex: 0,
            path: '/api/services/modal-manager/options'
          }
        ],
        path: '/api/services/modal-manager/',
        initialOpenGroupIndex: -1
      },
      {
        title: 'notification-manager',
        children: [
          {
            title: 'Configuration',
            initialOpenGroupIndex: 0,
            path: '/api/services/notification-manager/config'
          },
          {
            title: '.NotificationManager',
            initialOpenGroupIndex: 0,
            path: '/api/services/notification-manager/NotificationManager'
          },
          {
            title: 'Options',
            initialOpenGroupIndex: 0,
            path: '/api/services/notification-manager/options'
          }
        ],
        path: '/api/services/notification-manager/',
        initialOpenGroupIndex: -1
      },
      {
        title: 'router',
        children: [
          {
            title: 'components',
            children: [
              {
                title: 'private',
                children: [
                  {
                    title: 'layouts',
                    children: [
                      {
                        title: 'Layout',
                        initialOpenGroupIndex: 0,
                        path: '/api/services/router/components/private/layouts/Layout'
                      }
                    ],
                    path: '/api/services/router/components/private/layouts/',
                    initialOpenGroupIndex: 0
                  },
                  {
                    title: 'views',
                    children: [],
                    path: '/api/services/router/components/private/views/',
                    initialOpenGroupIndex: 0
                  }
                ],
                path: '/api/services/router/components/private/',
                initialOpenGroupIndex: 0
              },
              {
                title: 'public',
                children: [
                  {
                    title: 'layouts',
                    children: [
                      {
                        title: 'Layout',
                        initialOpenGroupIndex: 0,
                        path: '/api/services/router/components/public/layouts/Layout'
                      }
                    ],
                    path: '/api/services/router/components/public/layouts/',
                    initialOpenGroupIndex: 0
                  },
                  {
                    title: 'views',
                    children: [
                      {
                        title: 'home',
                        children: [
                          {
                            title: 'ExampleToastContent',
                            initialOpenGroupIndex: 0,
                            path: '/api/services/router/components/public/views/home/ExampleToastContent'
                          },
                          {
                            title: 'Index',
                            initialOpenGroupIndex: 0,
                            path: '/api/services/router/components/public/views/home/_Index'
                          }
                        ],
                        path: '/api/services/router/components/public/views/home/',
                        initialOpenGroupIndex: 0
                      },
                      {
                        title: 'not-found',
                        children: [
                          {
                            title: 'Index',
                            initialOpenGroupIndex: 0,
                            path: '/api/services/router/components/public/views/not-found/_Index'
                          }
                        ],
                        path: '/api/services/router/components/public/views/not-found/',
                        initialOpenGroupIndex: 0
                      }
                    ],
                    path: '/api/services/router/components/public/views/',
                    initialOpenGroupIndex: 0
                  }
                ],
                path: '/api/services/router/components/public/',
                initialOpenGroupIndex: 0
              }
            ],
            path: '/api/services/router/components/',
            initialOpenGroupIndex: 0
          },
          {
            title: 'Configuration',
            initialOpenGroupIndex: 0,
            path: '/api/services/router/config'
          },
          {
            title: 'lib',
            children: [
              {
                title: 'extend-vue-router',
                initialOpenGroupIndex: 0,
                path: '/api/services/router/lib/extend-vue-router'
              }
            ]
          },
          {
            title: '.middlewares',
            children: [
              {
                title: '.AppMiddleware',
                initialOpenGroupIndex: 0,
                path: '/api/services/router/middlewares/AppMiddleware'
              }
            ],
            path: '/api/services/router/middlewares/',
            initialOpenGroupIndex: 0
          },
          {
            title: 'mixins',
            children: [
              {
                title: 'RouteComponentMixin',
                initialOpenGroupIndex: 0,
                path: '/api/services/router/mixins/RouteComponentMixin'
              }
            ],
            path: '/api/services/router/mixins/',
            initialOpenGroupIndex: 0
          },
          {
            title: 'Options',
            initialOpenGroupIndex: 0,
            path: '/api/services/router/options'
          },
          {
            title: '.PrivateRoute',
            initialOpenGroupIndex: 0,
            path: '/api/services/router/PrivateRoute'
          },
          {
            title: '.PublicRoute',
            initialOpenGroupIndex: 0,
            path: '/api/services/router/PublicRoute'
          },
          {
            title: '.Route',
            initialOpenGroupIndex: 0,
            path: '/api/services/router/Route'
          },
          {
            title: '.routes',
            children: [
              {
                title: '.private',
                children: [],
                path: '/api/services/router/routes/private/',
                initialOpenGroupIndex: 0
              },
              {
                title: '.public',
                children: [
                  {
                    title: '.Home',
                    initialOpenGroupIndex: 0,
                    path: '/api/services/router/routes/public/Home'
                  },
                  {
                    title: '.NotFound',
                    initialOpenGroupIndex: 0,
                    path: '/api/services/router/routes/public/NotFound'
                  },
                  {
                    title: '._CatchAll',
                    initialOpenGroupIndex: 0,
                    path: '/api/services/router/routes/public/_CatchAll'
                  }
                ],
                path: '/api/services/router/routes/public/',
                initialOpenGroupIndex: 0
              }
            ],
            path: '/api/services/router/routes/',
            initialOpenGroupIndex: 0
          },
          {
            title: 'tools',
            children: [
              {
                title: 'middlewares',
                children: [
                  {
                    title: 'private-middleware',
                    initialOpenGroupIndex: 0,
                    path: '/api/services/router/tools/middlewares/private-middleware'
                  },
                  {
                    title: 'public-middleware',
                    initialOpenGroupIndex: 0,
                    path: '/api/services/router/tools/middlewares/public-middleware'
                  }
                ],
                path: '/api/services/router/tools/middlewares/',
                initialOpenGroupIndex: 0
              }
            ],
            path: '/api/services/router/tools/',
            initialOpenGroupIndex: 0
          }
        ],
        path: '/api/services/router/',
        initialOpenGroupIndex: -1
      },
      {
        title: 'store',
        children: [
          {
            title: 'modules',
            children: [
              {
                title: 'resources',
                children: [],
                path: '/api/services/store/modules/resources/',
                initialOpenGroupIndex: 0
              },
              {
                title: 'system',
                children: [],
                path: '/api/services/store/modules/system/',
                initialOpenGroupIndex: 0
              }
            ],
            path: '/api/services/store/modules/',
            initialOpenGroupIndex: 0
          },
          {
            title: '.options',
            initialOpenGroupIndex: 0,
            path: '/api/services/store/options'
          },
          {
            title: 'plugins',
            children: [
              {
                title: 'loading-status-plugin',
                children: [
                  {
                    title: 'options',
                    initialOpenGroupIndex: 0,
                    path: '/api/services/store/plugins/loading-status-plugin/options'
                  }
                ],
                path: '/api/services/store/plugins/loading-status-plugin/',
                initialOpenGroupIndex: 0
              },
              {
                title: 'resources-plugin',
                children: [
                  {
                    title: 'DataManager class',
                    initialOpenGroupIndex: 0,
                    path: '/api/services/store/plugins/resources-plugin/DataManager'
                  },
                  {
                    title: 'options',
                    initialOpenGroupIndex: 0,
                    path: '/api/services/store/plugins/resources-plugin/options'
                  }
                ],
                path: '/api/services/store/plugins/resources-plugin/',
                initialOpenGroupIndex: 0
              },
              {
                title: 'system-plugin',
                children: [
                  {
                    title: 'options',
                    initialOpenGroupIndex: 0,
                    path: '/api/services/store/plugins/system-plugin/options'
                  }
                ],
                path: '/api/services/store/plugins/system-plugin/',
                initialOpenGroupIndex: 0
              }
            ],
            path: '/api/services/store/plugins/',
            initialOpenGroupIndex: 0
          },
          {
            title: 'tools',
            children: [
              {
                title: 'get-module-context',
                initialOpenGroupIndex: 0,
                path: '/api/services/store/tools/get-module-context'
              },
              {
                title: 'get-module',
                initialOpenGroupIndex: 0,
                path: '/api/services/store/tools/get-module'
              },
              {
                title: 'resources',
                children: [
                  {
                    title: 'create-module',
                    initialOpenGroupIndex: 0,
                    path: '/api/services/store/tools/resources/create-module'
                  },
                  {
                    title: 'generate-actions',
                    initialOpenGroupIndex: 0,
                    path: '/api/services/store/tools/resources/generate-actions'
                  },
                  {
                    title: 'generate-getters',
                    initialOpenGroupIndex: 0,
                    path: '/api/services/store/tools/resources/generate-getters'
                  },
                  {
                    title: 'generate-mutations',
                    initialOpenGroupIndex: 0,
                    path: '/api/services/store/tools/resources/generate-mutations'
                  },
                  {
                    title: 'generate-state',
                    initialOpenGroupIndex: 0,
                    path: '/api/services/store/tools/resources/generate-state'
                  },
                  {
                    title: 'get-resource-data-manager',
                    initialOpenGroupIndex: 0,
                    path: '/api/services/store/tools/resources/get-resource-data-manager'
                  },
                  {
                    title: 'get-resource-module',
                    initialOpenGroupIndex: 0,
                    path: '/api/services/store/tools/resources/get-resource-module'
                  }
                ],
                path: '/api/services/store/tools/resources/',
                initialOpenGroupIndex: 0
              }
            ],
            path: '/api/services/store/tools/',
            initialOpenGroupIndex: 0
          }
        ],
        path: '/api/services/store/',
        initialOpenGroupIndex: -1
      },
      {
        title: 'toast-manager',
        children: [
          {
            title: 'components',
            children: [
              {
                title: 'BaseToast',
                initialOpenGroupIndex: 0,
                path: '/api/services/toast-manager/components/BaseToast'
              },
              {
                title: 'BaseToaster',
                initialOpenGroupIndex: 0,
                path: '/api/services/toast-manager/components/BaseToaster'
              },
              {
                title: 'ExampleToaster',
                initialOpenGroupIndex: 0,
                path: '/api/services/toast-manager/components/ExampleToaster'
              },
              {
                title: 'Toast',
                initialOpenGroupIndex: 0,
                path: '/api/services/toast-manager/components/Toast'
              }
            ],
            path: '/api/services/toast-manager/components/',
            initialOpenGroupIndex: 0
          },
          {
            title: 'Configuration',
            initialOpenGroupIndex: 0,
            path: '/api/services/toast-manager/config'
          },
          {
            title: 'Options',
            initialOpenGroupIndex: 0,
            path: '/api/services/toast-manager/options'
          },
          {
            title: '.Toast',
            initialOpenGroupIndex: 0,
            path: '/api/services/toast-manager/Toast'
          },
          {
            title: '.Toaster',
            initialOpenGroupIndex: 0,
            path: '/api/services/toast-manager/Toaster'
          },
          {
            title: '.ToastManager',
            initialOpenGroupIndex: 0,
            path: '/api/services/toast-manager/ToastManager'
          }
        ],
        path: '/api/services/toast-manager/',
        initialOpenGroupIndex: -1
      },
      {
        title: 'websocket-manager',
        children: [
          {
            title: 'Configuration',
            initialOpenGroupIndex: 0,
            path: '/api/services/websocket-manager/config'
          },
          {
            title: 'Options',
            initialOpenGroupIndex: 0,
            path: '/api/services/websocket-manager/options'
          },
          {
            title: '.WebSocketManager',
            initialOpenGroupIndex: 0,
            path: '/api/services/websocket-manager/WebSocketManager'
          },
          {
            title: '.WSConnection',
            initialOpenGroupIndex: 0,
            path: '/api/services/websocket-manager/WSConnection'
          },
          {
            title: '.WSConnectionOptions',
            initialOpenGroupIndex: 0,
            path: '/api/services/websocket-manager/WSConnectionOptions'
          }
        ],
        path: '/api/services/websocket-manager/',
        initialOpenGroupIndex: -1
      }
    ],
    path: '/api/services/',
    initialOpenGroupIndex: -1
  },
  {
    title: 'Toolbox',
    children: [
      {
        title: 'css',
        children: [
          {
            title: 'CSSClasses',
            initialOpenGroupIndex: 0,
            path: '/api/toolbox/css/CSSClasses'
          }
        ],
        path: '/api/toolbox/css/',
        initialOpenGroupIndex: 0
      },
      {
        title: 'development',
        children: [
          {
            title: 'runtime',
            children: [
              {
                title: 'error-handler',
                initialOpenGroupIndex: 0,
                path: '/api/toolbox/development/runtime/error-handler'
              }
            ],
            path: '/api/toolbox/development/runtime/',
            initialOpenGroupIndex: 0
          }
        ],
        path: '/api/toolbox/development/',
        initialOpenGroupIndex: 0
      },
      {
        title: 'Hook',
        initialOpenGroupIndex: 0,
        path: '/api/toolbox/Hook'
      },
      {
        title: 'PromiseQueue',
        initialOpenGroupIndex: 0,
        path: '/api/toolbox/PromiseQueue'
      },
      {
        title: 'services',
        children: [
          {
            title: 'make-configurable-option',
            initialOpenGroupIndex: 0,
            path: '/api/toolbox/services/make-configurable-option'
          }
        ],
        path: '/api/toolbox/services/',
        initialOpenGroupIndex: 0
      },
      {
        title: 'template',
        children: [
          {
            title: 'utils',
            children: [],
            path: '/api/toolbox/template/utils/',
            initialOpenGroupIndex: 0
          }
        ],
        path: '/api/toolbox/template/',
        initialOpenGroupIndex: 0
      },
      {
        title: 'vue',
        children: [
          {
            title: '.VuePluginable',
            initialOpenGroupIndex: 0,
            path: '/api/toolbox/vue/VuePluginable'
          }
        ],
        path: '/api/toolbox/vue/',
        initialOpenGroupIndex: 0
      }
    ],
    path: '/api/toolbox/',
    initialOpenGroupIndex: -1
  },
  {
    title: 'Utils',
    children: [
      {
        title: 'appendPrototypeChain',
        initialOpenGroupIndex: 0,
        path: '/api/utils/append-prototype-chain'
      },
      {
        title: 'arrayBufferToBase64',
        initialOpenGroupIndex: 0,
        path: '/api/utils/arraybuffer-to-base64'
      },
      {
        title: 'base64ToArrayBuffer',
        initialOpenGroupIndex: 0,
        path: '/api/utils/base64-to-arraybuffer'
      },
      {
        title: 'base64ToUint8Array',
        initialOpenGroupIndex: 0,
        path: '/api/utils/base64-to-uint8array'
      },
      {
        title: 'blobFromCanvas',
        initialOpenGroupIndex: 0,
        path: '/api/utils/blob-from-canvas'
      },
      {
        title: 'camelcase',
        initialOpenGroupIndex: 0,
        path: '/api/utils/camelcase'
      },
      {
        title: 'capitalize',
        initialOpenGroupIndex: 0,
        path: '/api/utils/capitalize'
      },
      {
        title: 'cloneDeep',
        initialOpenGroupIndex: 0,
        path: '/api/utils/clone-deep'
      },
      {
        title: 'clone',
        initialOpenGroupIndex: 0,
        path: '/api/utils/clone'
      },
      {
        title: 'completeAssign',
        initialOpenGroupIndex: 0,
        path: '/api/utils/complete-assign'
      },
      {
        title: 'compose',
        initialOpenGroupIndex: 0,
        path: '/api/utils/compose'
      },
      {
        title: 'constantcase',
        initialOpenGroupIndex: 0,
        path: '/api/utils/constantcase'
      },
      {
        title: 'createColor',
        initialOpenGroupIndex: 0,
        path: '/api/utils/create-color'
      },
      {
        title: 'dateFormat',
        initialOpenGroupIndex: 0,
        path: '/api/utils/date-format'
      },
      {
        title: 'debounce',
        initialOpenGroupIndex: 0,
        path: '/api/utils/debounce'
      },
      {
        title: 'deepBind',
        initialOpenGroupIndex: 0,
        path: '/api/utils/deep-bind'
      },
      {
        title: 'deepMerge',
        initialOpenGroupIndex: 0,
        path: '/api/utils/deep-merge'
      },
      {
        title: 'fileToDataUrl',
        initialOpenGroupIndex: 0,
        path: '/api/utils/file-to-data-url'
      },
      {
        title: 'findKey',
        initialOpenGroupIndex: 0,
        path: '/api/utils/find-key'
      },
      {
        title: 'find',
        initialOpenGroupIndex: 0,
        path: '/api/utils/find'
      },
      {
        title: 'getAllKeys',
        initialOpenGroupIndex: 0,
        path: '/api/utils/get-all-keys'
      },
      {
        title: 'getConstructorName',
        initialOpenGroupIndex: 0,
        path: '/api/utils/get-constructor-name'
      },
      {
        title: 'getHostHttpUrl',
        initialOpenGroupIndex: 0,
        path: '/api/utils/get-host-http-url'
      },
      {
        title: 'getPrototypeChain',
        initialOpenGroupIndex: 0,
        path: '/api/utils/get-prototype-chain'
      },
      {
        title: 'getPrototypeNames',
        initialOpenGroupIndex: 0,
        path: '/api/utils/get-prototype-names'
      },
      {
        title: 'getTimestamp',
        initialOpenGroupIndex: 0,
        path: '/api/utils/get-timestamp'
      },
      {
        title: 'getTimezones',
        initialOpenGroupIndex: 0,
        path: '/api/utils/get-timezones'
      },
      {
        title: 'get',
        initialOpenGroupIndex: 0,
        path: '/api/utils/get'
      },
      {
        title: 'groupBy',
        initialOpenGroupIndex: 0,
        path: '/api/utils/group-by'
      },
      {
        title: 'has',
        initialOpenGroupIndex: 0,
        path: '/api/utils/has'
      },
      {
        title: 'hash',
        initialOpenGroupIndex: 0,
        path: '/api/utils/hash'
      },
      {
        title: 'hexToUint8Array',
        initialOpenGroupIndex: 0,
        path: '/api/utils/hex-to-uint8array'
      },
      {
        title: 'isArray',
        initialOpenGroupIndex: 0,
        path: '/api/utils/is-array'
      },
      {
        title: 'isBlankString',
        initialOpenGroupIndex: 0,
        path: '/api/utils/is-blank-string'
      },
      {
        title: 'isDef',
        initialOpenGroupIndex: 0,
        path: '/api/utils/is-def'
      },
      {
        title: 'isEmptyString',
        initialOpenGroupIndex: 0,
        path: '/api/utils/is-empty-string'
      },
      {
        title: 'isEqual',
        initialOpenGroupIndex: 0,
        path: '/api/utils/is-equal'
      },
      {
        title: 'isFalse',
        initialOpenGroupIndex: 0,
        path: '/api/utils/is-false'
      },
      {
        title: 'isNull',
        initialOpenGroupIndex: 0,
        path: '/api/utils/is-null'
      },
      {
        title: 'isObject',
        initialOpenGroupIndex: 0,
        path: '/api/utils/is-object'
      },
      {
        title: 'isPlainObject',
        initialOpenGroupIndex: 0,
        path: '/api/utils/is-plain-object'
      },
      {
        title: 'isPrimitive',
        initialOpenGroupIndex: 0,
        path: '/api/utils/is-primitive'
      },
      {
        title: 'isPromise',
        initialOpenGroupIndex: 0,
        path: '/api/utils/is-promise'
      },
      {
        title: 'isRegExp',
        initialOpenGroupIndex: 0,
        path: '/api/utils/is-regexp'
      },
      {
        title: 'isTrue',
        initialOpenGroupIndex: 0,
        path: '/api/utils/is-true'
      },
      {
        title: 'isUndef',
        initialOpenGroupIndex: 0,
        path: '/api/utils/is-undef'
      },
      {
        title: 'isValidArrayIndex',
        initialOpenGroupIndex: 0,
        path: '/api/utils/is-valid-array-index'
      },
      {
        title: 'isValidHttpUrl',
        initialOpenGroupIndex: 0,
        path: '/api/utils/is-valid-http-url'
      },
      {
        title: 'isVueComponent',
        initialOpenGroupIndex: 0,
        path: '/api/utils/is-vue-component'
      },
      {
        title: 'kebabcase',
        initialOpenGroupIndex: 0,
        path: '/api/utils/kebabcase'
      },
      {
        title: 'lowercase',
        initialOpenGroupIndex: 0,
        path: '/api/utils/lowercase'
      },
      {
        title: 'maxBy',
        initialOpenGroupIndex: 0,
        path: '/api/utils/max-by'
      },
      {
        title: 'minBy',
        initialOpenGroupIndex: 0,
        path: '/api/utils/min-by'
      },
      {
        title: 'orderBy',
        initialOpenGroupIndex: 0,
        path: '/api/utils/order-by'
      },
      {
        title: 'parallelAsync',
        initialOpenGroupIndex: 0,
        path: '/api/utils/parallel-async'
      },
      {
        title: 'pascalcase',
        initialOpenGroupIndex: 0,
        path: '/api/utils/pascalcase'
      },
      {
        title: 'random',
        initialOpenGroupIndex: 0,
        path: '/api/utils/random'
      },
      {
        title: 'safeAsync',
        initialOpenGroupIndex: 0,
        path: '/api/utils/safe-async'
      },
      {
        title: 'safe',
        initialOpenGroupIndex: 0,
        path: '/api/utils/safe'
      },
      {
        title: 'setDateToMidnight',
        initialOpenGroupIndex: 0,
        path: '/api/utils/set-date-to-midnight'
      },
      {
        title: 'set',
        initialOpenGroupIndex: 0,
        path: '/api/utils/set'
      },
      {
        title: 'snakecase',
        initialOpenGroupIndex: 0,
        path: '/api/utils/snakecase'
      },
      {
        title: 'sortBy',
        initialOpenGroupIndex: 0,
        path: '/api/utils/sort-by'
      },
      {
        title: 'toNumber',
        initialOpenGroupIndex: 0,
        path: '/api/utils/to-number'
      },
      {
        title: 'toRawType',
        initialOpenGroupIndex: 0,
        path: '/api/utils/to-raw-type'
      },
      {
        title: 'typeCheck',
        initialOpenGroupIndex: 0,
        path: '/api/utils/type-check'
      },
      {
        title: 'union',
        initialOpenGroupIndex: 0,
        path: '/api/utils/union'
      },
      {
        title: 'uppercase',
        initialOpenGroupIndex: 0,
        path: '/api/utils/uppercase'
      },
      {
        title: 'validateEmail',
        initialOpenGroupIndex: 0,
        path: '/api/utils/validate-email'
      },
      {
        title: 'XArray',
        initialOpenGroupIndex: 0,
        path: '/api/utils/x-array'
      },
      {
        title: 'xorBy',
        initialOpenGroupIndex: 0,
        path: '/api/utils/xor-by'
      },
      {
        title: 'xor',
        initialOpenGroupIndex: 0,
        path: '/api/utils/xor'
      }
    ],
    path: '/api/utils/',
    initialOpenGroupIndex: 0
  },
  {
    title: 'Core',
    initialOpenGroupIndex: 0,
    path: '/api/Core'
  },
  {
    title: 'Service',
    initialOpenGroupIndex: 0,
    path: '/api/Service'
  },
  {
    title: 'ServiceDefinition',
    initialOpenGroupIndex: 0,
    path: '/api/ServiceDefinition'
  }
]
