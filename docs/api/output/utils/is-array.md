---
title: isArray
headline: isArray
sidebarTitle: isArray
---

# isArray

<a name="isarray">
</a>

## isArray ⇒ <code>boolean</code>

Check if value is instance of Array

**Returns**: <code>boolean</code> - Returns true if value instance of Array

| Param | Type | Description |
| --- | --- | --- |
| value | <code>\*</code> | The variable to check |

**Example**

```js
isArray(['foo', 'bar']) // => true
```

