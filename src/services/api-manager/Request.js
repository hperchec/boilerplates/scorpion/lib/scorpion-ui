/**
 * @vuepress
 * ---
 * title: "Service (api-manager): Request class"
 * headline: "Service (api-manager): Request class"
 * sidebarTitle: .Request
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import Core from '@/Core'
import RequestManager from './RequestManager'

/**
 * @classdesc
 * Service **api-manager**: Request class
 */
class Request {
  /**
   * Create a new Request
   * @param {RequestManager} manager - The request manager instance
   * @param {Function} generator - The original request generator (function that returns Promise)
   * @param {string} method - The http verb
   * @param {string|URL} url - The url
   * @param {object} options - The request options
   * @param {Function} beforeCb - The before request callback
   * @param {Function} successCb - The success callback
   * @param {Function} errorCb - The error callback
   */
  constructor (manager, generator, method, url, options, beforeCb, successCb, errorCb) {
    this.#manager = manager
    this.#generator = generator
    this.#method = method
    this.#url = url
    this.#options = options
    this.#beforeCb = beforeCb
    this.#successCb = successCb
    this.#errorCb = errorCb
  }

  /**
   * Accessors & mutators
   */

  /**
   * The request manager
   * @category properties
   * @readonly
   * @type {RequestManager}
   */
  get manager () {
    return this.#manager
  }

  /**
   * The request function
   * @category properties
   * @readonly
   * @type {Function}
   */
  get generator () {
    return this.#generator
  }

  /**
   * The request method
   * @category properties
   * @readonly
   * @type {string}
   */
  get method () {
    return this.#method
  }

  /**
   * The request url
   * @category properties
   * @readonly
   * @type {string|URL}
   */
  get url () {
    return this.#url
  }

  /**
   * The request options
   * @category properties
   * @readonly
   * @type {object}
   */
  get options () {
    return this.#options
  }

  /**
   * Methods
   */

  /**
   * Send the request
   * @param {object} [options = {}] - Options object
   * @param {boolean} [options.allowConcurrent = false] - Push to promise queue even if key exists
   * @returns {Promise} - The request promise
   */
  async send (options = {}) {
    const _options = {
      allowConcurrent: false,
      ...options
    }
    // Creates key to identify request
    // example: "GET:http://localhost/api/sites/1"
    const resolvedUrl = new URL(this.url, this.#manager.getAPIInstance().driver.getUri(this.options))
    const requestKey = `${this.method}:${resolvedUrl.toString()}`
    // If request exists and concurrent are not allowed
    if (this.#manager.get(requestKey) && !_options.allowConcurrent) {
      console.warn('RequestManager: prevent sending multiple same request: ', requestKey)
      return this.#manager.get(requestKey)
    } else {
      return this.#manager.push(
        requestKey,
        this.#beforeCb(this)
          .then(() => this.generator(this)
            .then(async (res) => {
              return await this.#successCb(res, this)
            })
            .catch(async (err) => {
              const requestError = new Core.context.support.errors.RequestError(err, this)
              return await this.#errorCb(requestError, this)
            })
          )
      )
    }
  }

  /**
   * Static
   */

  // ...

  /**
   * Private properties
   */

  #manager
  #generator
  #method
  #url
  #options
  #beforeCb
  #successCb
  #errorCb
}

export default Request
