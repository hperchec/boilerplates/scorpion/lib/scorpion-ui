/**
 * Get a store module by namespace
 * @param {import("vuex").Store} store - The store instance
 * @param {string} namespace - The full module namespace
 * @returns {object} The module object
 */
export const getModule = (store, namespace) => {
  // Split namespace by "/"
  const path = namespace.split('/')
  // Store module collection
  // @ts-ignore
  const moduleCollection = store._modules
  // Call the "get" method of ModuleCollection from vuex library
  // See also: https://github.com/vuejs/vuex/blob/ddf196969379d038e5ce6250233b1a181197a2ca/src/module/module-collection.js#L10
  const mod = moduleCollection.get(path)
  if (!mod) {
    throw new Error(`Store getModule tool: "${namespace}" module not found`)
  }
  return mod
}

export default getModule
