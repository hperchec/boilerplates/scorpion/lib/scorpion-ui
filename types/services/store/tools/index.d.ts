declare namespace _default {
    export { getModule };
    export { getModuleContext };
    export { resources };
}
export default _default;
import getModule from './get-module';
import getModuleContext from './get-module-context';
import resources from './resources';
//# sourceMappingURL=index.d.ts.map