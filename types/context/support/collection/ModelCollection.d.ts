export default ModelCollection;
declare class ModelCollection extends Collection {
    constructor(model: Function, data?: any[], options?: object);
    getModel(): Function;
    findBy(key: any): any;
    contains(key: Function | number | string): boolean;
    replaceItem(to: any): Promise<ModelCollection>;
    upsertItem(to: any): Promise<ModelCollection>;
    get toData(): {};
    #private;
}
import Collection from './Collection';
//# sourceMappingURL=ModelCollection.d.ts.map