export function base64ToUint8Array(str: string, toArrayBufferOptions?: object, offset?: number, length?: number): Uint8Array;
export default base64ToUint8Array;
//# sourceMappingURL=base64-to-uint8array.d.ts.map