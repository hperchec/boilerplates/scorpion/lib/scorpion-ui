---
title: Plugins
headline: Plugins
sidebarTitle: Plugins
initialOpenGroupIndex: -1
prev: ../mixins/
next: ./core-plugin/
---

# Plugins

<a name="plugins">
</a>

## plugins : <code>object</code>

Contains all Vue plugins that will be automatically registered.Each plugin is an object that has the following properties:- `plugin` object that will be used Vue- `options` object to pass to the plugin::: tip See alsoCore [registerVuePlugin](../../../Core#core-registervueplugin) method documentation:::

