---
title: Support/collection
headline: Support/collection
sidebarTitle: collection
prev: ../
next: ./Collection
---

# Support/collection

<a name="collection">
</a>

## collection : <code>object</code>

> **Ref.** : [Core](../../Core).[support](../).[collection](./)

**Schema**:

- [collection](#collection) : <code>object</code>
  - [.Collection](#collection-collection) : <code>object</code>
  - [.ModelCollection](#collection-modelcollection) : <code>object</code>

<a name="collection-collection">
</a>

### Core.context.support.collection.Collection : <code>object</code>

**See**: [Collection](./Collection)

<a name="collection-modelcollection">
</a>

### Core.context.support.collection.ModelCollection : <code>object</code>

**See**: [ModelCollection](./ModelCollection)

