export default {
  /**
   * Default root resources module name
   * @type {string}
   * @default Resources
   */
  rootModule: 'Resources',
  /**
   * Default API to use with apiManager
   * @type {string}
   * @default ServerAPI
   */
  defaultAPI: 'ServerAPI'
}
