/**
 * @vuepress
 * ---
 * title: "Root instance prototype"
 * headline: "Root instance prototype"
 * sidebarTitle: "Prototype"
 * initialOpenGroupIndex: -1 # optional, defaults to 0, defines the index of initially opened subgroup
 * prev: ../
 * next: ./app/
 * ---
 */

/**
 * Prototype
 */
import app from './app'

export default {
  app
}
