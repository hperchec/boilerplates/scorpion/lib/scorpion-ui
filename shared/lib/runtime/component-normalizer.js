// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

/**
 * Normalize component options object (SFC).
 * @param {object} component - The component object
 * @returns {object} Returns the component
 */
export default function normalizeComponent (component) {
  const privateProperty = '__isVueComponentOptions'
  if (!Object.prototype.hasOwnProperty.call(component, privateProperty)) {
    Object.defineProperty(component, privateProperty, {
      value: true,
      writable: false,
      enumerable: true // ?
    })
  }
  return component
}
