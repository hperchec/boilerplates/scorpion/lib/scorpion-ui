---
title: Service (modal-manager) options
headline: Service (modal-manager) options
sidebarTitle: Options
sidebarDepth: 0
prev: false
next: false
---

# Service (modal-manager) options

<a name="options">
</a>

## options

Modal manager service optionsConfigurable options:- **baseZIndex**: `Core.config.services['modal-manager'].baseZIndex`

**Schema**:

- [options](#options)
  - [.modals](#options-modals) : <code>Array.&lt;module:services/modal-manager/Modal~Modal&gt;</code>
  - [.useTransition](#options-usetransition) : <code>object</code>
  - [.baseZIndex](#options-basezindex) : <code>number</code>

<a name="options-modals">
</a>

### Core.context.services[&#x27;modal-manager&#x27;].options.modals : <code>Array.&lt;module:services/modal-manager/Modal~Modal&gt;</code>

The modals to pass to ModalManager constructor

<a name="options-usetransition">
</a>

### Core.context.services[&#x27;modal-manager&#x27;].options.useTransition : <code>object</code>

If defined, a props object to pass to Vue built-in `<transition-group>` componentto wrap each ModalOverlay component.

<a name="options-basezindex">
</a>

### Core.context.services[&#x27;modal-manager&#x27;].options.baseZIndex : <code>number</code>

Define the bbase zIndex for modal overlayCan be overwritten via Core global configuration: `Core.config.services['modal-manager'].baseZIndex`

**Default**: <code>500</code>  
