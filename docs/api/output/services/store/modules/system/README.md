---
title: __index__
headline: __index__
---

# __index__

<a name="translate">
</a>

## translate(...args) ⇒ <code>string</code>

| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>any</code> | See i18n method |

