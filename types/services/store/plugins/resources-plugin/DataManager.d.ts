export default DataManager;
declare class DataManager {
    constructor(data: object, model: typeof Model, options?: {
        storeModuleNamespace?: string;
        findBy?: Function;
    });
    data: any;
    model: typeof Model;
    storeModuleNamespace: any;
    findBy(key: any, items: object[]): object;
    compositeKeyMap: {};
    getDataKeyFromItem(item: object): number | string;
    getModelCollection(state?: object): ModelCollection;
    all(state?: object): object[];
    find(key: Function | number | string, state?: object): object;
    filter(callbackFn: Function, state?: object): object[];
    findMany(...args: any[]): object[];
    insert(payload: object[] | object, state?: object): object[] | object | Promise<object[] | object>;
    update(item: object, state?: object): object;
    upsert(payload: object[] | object, state?: object): object[] | object;
    delete(key: Function | number | string, state?: object): object | Promise<object>;
}
import Model from '@/context/support/model/Model';
import ModelCollection from '@/context/support/collection/ModelCollection';
//# sourceMappingURL=DataManager.d.ts.map