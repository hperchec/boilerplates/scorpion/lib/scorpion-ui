/**
 * @vuepress
 * ---
 * title: "Service (logger)"
 * headline: "Service (logger)"
 * sidebarTitle: logger
 * initialOpenGroupIndex: -1 # optional, defaults to 0, defines the index of initially opened subgroup
 * prev: ../
 * next: ./options
 * ---
 */

import Service from '@/Service'

import options from './options'
import components from './components'
import Logger from './Logger'

/* eslint-disable jsdoc/require-param, jsdoc/require-returns */

/**
 * @scorpion_ui_service {Logger} logger
 * @description
 * The service "logger" is for logging messages (only console is supported until now).
 */
export default new Service({
  // Service
  service: {
    // Identifier
    identifier: 'logger',
    /**
     * @alias register
     * @description
     * On registering, the service will inject properties to Vue root instance prototype `$app`:
     *
     * - `$app.log`
     *
     * It will also define Vue components:
     *
     * - `Core.context.vue.components.commons.logger.JsonPretty`: see {@link ./components/commons/ commons components}
     */
    register: function (_Core) {
      // Define vue root instance prototype in '$app' namespace
      const $app = _Core.context.vue.root.prototype.app
      /**
       * log
       * @alias $app.log
       * @param {...*} args - Same parameters as Logger consoleLog() method
       * @returns {void}
       * @description
       * log method: shortcut for Logger consoleLog() method
       * @example
       * // In any component
       * this.$app.log('Hello world!')
       */
      $app.log = function (...args) {
        const root = this // 'this' is the Vue root instance
        root.$logger.consoleLog(...args) // Simple call of 'consoleLog' method...
      }
      // Expose vue components
      _Core.context.vue.components.commons.logger = {
        JsonPretty: components.commons.logger.JsonPretty
      }
    },
    /**
     * @alias create
     * @description
     * Returns an instance of `Logger` created with options.
     */
    create: (serviceOptions, { Logger: _Logger }, _Core) => {
      // Return new Logger
      return new _Logger(serviceOptions)
    },
    /**
     * @alias plugin
     * @description
     * The following properties will be set to Vue prototype:
     *
     * ```js
     * Vue.prototype.$logger // => Core.service('logger')
     * ```
     */
    vuePlugin: ({ Logger: _Logger }, _Core) => _Logger,
    // Vue plugin options
    pluginOptions: {},
    // rootOption
    rootOption: ({ Logger: _Logger }, _Core) => _Logger.rootOption
  },
  // On initialized
  onInitialized: (Core) => {
    // ...
  },
  /**
   * @alias logger.options
   * @type {object}
   * @see {@link ./options options}
   */
  options,
  /**
   * expose
   */
  expose: {
    /**
     * @alias logger.Logger
     * @type {Function}
     * @class
     * @see {@link ./Logger Logger}
     */
    Logger: Logger
  }
})
