/**
 * @vuepress
 * ---
 * title: xorBy
 * headline: xorBy
 * sidebarTitle: xorBy
 * ---
 */

// Lodash.xorby
import lodashXorBy from 'lodash.xorby'

/**
 * xorBy
 * @param {...*} args - Same arguments as lodash.xorBy
 * @description Sort array of objects (lodash.xorBy). See https://lodash.com/docs/4.17.15#xorBy
 * @returns {Array} Returns the array difference
 */
export const xorBy = function (...args) {
  return lodashXorBy(...args)
}

export default xorBy
