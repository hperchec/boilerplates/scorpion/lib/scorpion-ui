/**
 * @vuepress
 * ---
 * title: "Model class"
 * headline: "Model class"
 * sidebarTitle: "Model class"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: ./
 * next: false
 * ---
 */

// import jsonStringifySafe from 'json-stringify-safe'
import Core from '@/Core'
import { Hook } from '@/toolbox'
import Collection from '../collection/Collection' // jsDoc type
import ModelCollection from '../collection/ModelCollection' // jsDoc type

// Utils
import {
  camelcase,
  find as _find,
  getAllKeys,
  isArray,
  isDef,
  isUndef,
  isNull,
  snakecase,
  typeCheck
} from '@/utils'

/**
 * @typicalname Core.context.support.model.Model
 * @classdesc
 * Model class
 *
 * Every resource of your project should have a model representation
 * extending this class to easily manage it.
 */
class Model {
  /**
   * Create a Model
   * @param {object} data - Data object (data is received as { key: value, ... } object where key is in snake_case format)
   * @param {object} [options={}] - Options object
   */
  constructor (data, options = {}) {
    // Get child constructor
    const model = this.constructor
    // First, prevent new instance from this class directly
    if (model === Model) {
      throw new Error('Model class: can\'t be instanciated via new Model(). Please extends this class.')
    }
    // Then check initialized status
    // @ts-ignore
    if (!model.isInitialized) {
      throw new Error(`Model child class "${model.name}" has not been initialized. Call init() method to define model attributes in static initialization block.`)
    }
    // Check data type
    if (isDef(data) && typeof data !== 'object') {
      throw new Error(`${model.name} constructor: 'data' argument must be Object`)
    }
    // Hydrate instance
    this.hydrate(data)
    // If lock option is passed, freeze the instance
    if (options.lock) {
      Object.freeze(this)
    }
  }

  /**
   * Methods
   */

  /**
   * Hydrate an instance with data
   * @param {object} data - The data
   * @returns {void}
   */
  hydrate (data) {
    const model = this.constructor
    // Get and loop on model attributes
    // @ts-ignore
    for (const attrName in model.attributes) {
      // @ts-ignore
      const attrDef = model.attributes[attrName]
      // Get data attribute name
      const dataAttributeName = attrDef.fromDataAttributeName
      // Get data value
      const dataValue = isDef(data[dataAttributeName])
        ? isNull(data[dataAttributeName])
          ? null
          : attrDef.cast(data[dataAttributeName])
        : attrDef.default // Undefined by default
      // ⚠ While inherited properties are non-enumerable,
      // make sure prop is enumerable
      const attrPropertyDescriptor = Object.getOwnPropertyDescriptor(model.prototype, attrName)
      Object.defineProperty(this, attrName, attrPropertyDescriptor)
      // Call setter
      this[attrName] = dataValue
    }
  }

  /**
   * Serialize a model instance following the data schema
   * @returns {object} Returns the serialized data
   */
  serialize () {
    const model = this.constructor
    const serialized = {}
    // @ts-ignore
    for (const attrName in model.attributes) {
      // @ts-ignore
      const dataAttributeName = model.attributes[attrName].fromDataAttributeName
      serialized[dataAttributeName] = this[attrName]
    }
    return serialized
  }

  /**
   * Serialize a model instance folowing the data schema
   * @returns {object} Returns the serialized data
   * @ignore
   */
  toPlainObject () {
    const model = this.constructor
    return Object.keys(model.prototype).reduce((accumulator, key) => {
      accumulator[key] = this[key]
      return accumulator
    }, {})
  }

  /**
   * Overrides the default toJSON object method for JSON.stringify() calls
   * @returns {object} - The instance data to be serialized
   */
  toJSON () {
    const keys = getAllKeys(this, {
      excludeKeys: [
        /^_/, // private properties that starts with '_'
        'toJSON',
        'toString',
        'constructor'
      ],
      includeSymbols: false,
      includeNonenumerables: false // exclude getters (prevent internal loop for model relations)
    })
    return keys.reduce((obj, key) => {
      const value = this[key]
      obj[key] = value
      return obj
    }, {})
  }

  /**
   * Static properties
   */

  /**
   * Set model primary key
   * Default is "id"
   * @category properties
   * @type {string|string[]}
   */
  static primaryKey = 'id'

  /**
   * Default attribute type cast methods
   * @category properties
   * @type {Array}
   */
  static defaultAttrTypeCast = [
    {
      type: String,
      cast: function castString (value) { return String(value) }
    },
    {
      type: Number,
      cast: function castNumber (value) { return Number(value) }
    },
    {
      type: Boolean,
      cast: function castBoolean (value) { return Boolean(value) }
    },
    {
      type: Date,
      cast: function castDate (value) { return new Date(value) }
    }
  ]

  /**
   * Model attributes
   * @category properties
   * @type {object}
   */
  static attributes

  /**
   * Model hooks
   * @category properties
   * @type {object}
   */
  static get hooks () {
    return {}
  }

  /**
   * @category properties
   * @type {object}
   * @description
   * The store options
   */
  static get storeOptions () {
    return {
      /**
       * Store instance
       * @type {Function}
       * @returns {import("vuex").Store} - The store instance
       * @default undefined
       */
      getStoreInstance: () => undefined,
      /**
       * Store module namespace
       * @type {string}
       * @default undefined
       */
      namespace: undefined,
      /**
       * Target store module
       * @type {object}
       * @default undefined
       */
      module: undefined
    }
  }

  /**
   * @ignore
   */
  static get _privateAttrProps () {
    return undefined
  }

  /* eslint-disable jsdoc/require-returns-check */
  /**
   * @category properties
   * @readonly
   * @returns {object} Returns the store module context
   * @description
   * Default is undefined and accessible only once store is configured for the Model.
   * See init() static method.
   */
  static get storeInterface () {
    throw new Error(`Model "${this.name}" does not have store configured`)
  }
  /* eslint-enable jsdoc/require-returns-check */

  /**
   * @category properties
   * @readonly
   * @returns {boolean} Returns false by default
   * @description
   * Default isInitialized is a static getter that returns false until "init" method is called
   * See below
   */
  static get isInitialized () {
    return false
  }

  /**
   * @category properties
   * @readonly
   * @type {string[]}
   * @description
   * Contains the names of attributes that are required.
   * ⚠ Child class must call init method. See below
   */
  static get requiredAttributes () {
    throw new Error('Unable to access requiredAttributes in "Model" parent class.')
  }

  /**
   * @category properties
   * @readonly
   * @type {string[]}
   * @description
   * Contains the names of attributes that are nullable.
   * ⚠ Child class must call init method. See below
   */
  static get nullableAttributes () {
    throw new Error('Unable to access nullableAttributes in "Model" parent class.')
  }

  /**
   * @category properties
   * @readonly
   * @returns {object} Returns the mapped actions
   * @description
   * Returns an object of mapped actions from store module
   */
  static get api () {
    const model = this
    const resourceModule = model.storeOptions.module
    // Get resource raw module action keys
    const actionKeys = Object.keys(model.storeOptions.module._rawModule.actions)
    // Return an object of mapped actions
    return actionKeys.reduce(
      (apiInterface, actionKey) => {
        // A function that just returns the call of module
        // dispatch method for corresponding action
        apiInterface[actionKey] = function (payload) {
          return resourceModule.context.dispatch(actionKey, payload)
        }
        return apiInterface
      },
      // apiInterface is an empty object by default
      {}
    )
  }

  /**
   * Static methods
   */

  /**
   * getDefaultCastForType
   * @category methods
   * @param {*} type - The target type
   * @returns {Function} Returns the cast function
   * @description
   * Static method to get default cast method for standard types
   */
  static getDefaultCastForType (type) {
    const foundDefault = this.defaultAttrTypeCast.find((obj) => obj.type === type)
    return foundDefault
      ? foundDefault.cast
      : (value) => value
  }

  /**
   * Statis "init" method
   * @param {object} options - The init options
   * @param {object} options.attributes - The attribute definitions object
   * @param {object} [options.store] - The store options
   * @param {string} [options.store.namespace] - The store module namespace
   * @param {string} [options.store.rootModule] - The store root module name (Default: "Resources")
   * @param {*} [options.store.storeInstance] - The custom store instance
   * @param {object} [options.store.onBefore] - An object of mutation listener like: `{ 'INSERT': function (data) { ... } }`
   * @param {object} [options.store.onAfter] - An object of mutation listener like: `{ 'INSERT': function (data) { ... } }`
   * @returns {void}
   */
  static init (options) {
    if (this.isInitialized) {
      throw new Error(`Model child class "${this.name}" is already initialized`)
    }
    // Check if model has primaryKey defined
    if (!Object.prototype.hasOwnProperty.call(this, 'primaryKey')) {
      console.warn(`Model "${this.name}" does not have static primaryKey defined. Default is "id".`)
    }
    const attributeDefs = Object.assign({}, options.attributes) // Work with a copy of options.attributes
    // Unlock _privateAttrProps property
    Object.defineProperty(this, '_privateAttrProps', {
      value: {}
    })
    // Then set attributes
    Object.defineProperty(this, 'attributes', {
      value: {}
    })
    Object.defineProperty(this, 'requiredAttributes', {
      value: []
    })
    Object.defineProperty(this, 'nullableAttributes', {
      value: []
    })
    // Loop on attribute definitions
    for (const prop in attributeDefs) {
      this.defineAttribute(prop, attributeDefs[prop])
    }
    // Freeze _privateAttrProps
    Object.freeze(this._privateAttrProps)
    // Freeze attributes
    Object.freeze(this.attributes)
    // Lock the static requiredAttributes array
    Object.freeze(this.requiredAttributes)
    // Lock the static nullableAttributes array
    Object.freeze(this.nullableAttributes)
    // Set hooks property
    Object.defineProperty(this, 'hooks', {
      value: {
        store: {
          BEFORE_INSERT: new Hook('BEFORE_INSERT'),
          AFTER_INSERT: new Hook('AFTER_INSERT'),
          BEFORE_UPDATE: new Hook('BEFORE_UPDATE'),
          AFTER_UPDATE: new Hook('AFTER_UPDATE'),
          BEFORE_DELETE: new Hook('BEFORE_DELETE'),
          AFTER_DELETE: new Hook('AFTER_DELETE')
        }
      }
    })
    // If store module namespace provided
    if (options.store) {
      const storeOptions = options.store
      const { getModule, getModuleContext } = Core.context.services['store'].tools // eslint-disable-line dot-notation
      const rootModule = storeOptions.rootModule || 'Resources'
      const fullNamespace = [ rootModule, storeOptions.namespace ].join('/')
      const onBeforeListeners = storeOptions.onBefore || {}
      const onAfterListeners = storeOptions.onAfter || {}
      // Set store options
      Object.defineProperty(this, 'storeOptions', {
        value: {} // Empty object by default
      })
      const getStoreInstance = function () {
        return storeOptions.storeInstance || Core.service('store')
      }
      // Set store options
      Object.defineProperties(this.storeOptions, {
        getStoreInstance: {
          value: getStoreInstance,
          writable: false
        },
        namespace: {
          value: storeOptions.namespace,
          writable: false
        },
        rootModule: {
          value: rootModule,
          writable: false
        },
        fullNamespace: {
          get () {
            return fullNamespace
          }
        },
        module: {
          get () {
            return getModule(getStoreInstance(), fullNamespace)
          }
        }
      })
      // Set storeInterface getter
      Object.defineProperty(this, 'storeInterface', {
        get: () => {
          return getModuleContext(getStoreInstance(), fullNamespace)
        }
      })
      // Set "before" listeners
      for (const mutationName in onBeforeListeners) {
        this.onBefore(mutationName, onBeforeListeners[mutationName])
      }
      // Set "after" listeners
      for (const mutationName in onAfterListeners) {
        this.onAfter(mutationName, onAfterListeners[mutationName])
      }
    }
    // Finally lock isInitialized
    Object.defineProperty(this, 'isInitialized', {
      value: true
    })
  }

  /**
   * defineAttribute
   * @category methods
   * @param {string} name - The attribute name
   * @param {object} attrDef - The attribute definition
   * @returns {void}
   * @description
   * Set attribute and its accessor and mutator
   */
  static defineAttribute (name, attrDef) {
    const model = this
    const _attrDef = Object.assign({}, attrDef)
    // We create a unique Symbol and save it in the private "_privateAttrProps" property
    // To keep the dynamic privates properties unaccessible from outside
    // See also: https://stackoverflow.com/a/73807256
    const privateProp = model._privateAttrProps[name] = Symbol(name)
    // First, private property to undefined
    Object.defineProperty(model.prototype, privateProp, {
      value: undefined,
      enumerable: false
    })
    // Check if nullable field
    if (_attrDef.nullable) {
      _attrDef.nullable = true // force filling prop
      model.nullableAttributes.push(name)
    } else {
      _attrDef.nullable = false // force filling prop
      // Else, required field
      model.requiredAttributes.push(name)
    }
    // Enfore default prop
    if (isUndef(_attrDef.default)) {
      _attrDef.default = undefined
    }
    // Enfore fromDataAttributeName prop
    if (isUndef(_attrDef.fromDataAttributeName)) {
      _attrDef.fromDataAttributeName = model.toDataPropertyName(name)
    }
    // Set cast
    _attrDef.cast = _attrDef.cast || model.getDefaultCastForType(_attrDef.type)
    // Set attribute
    model.attributes[name] = _attrDef
    // Check if child class has overwritten getter or setter
    const existingAttributeProp = Object.getOwnPropertyDescriptor(model.prototype, name)
    // If not defined -> generate default getter & setter
    // Getter ?
    const getter = existingAttributeProp && existingAttributeProp.get
      ? existingAttributeProp.get
      : model.generateAttributeGetter(name)
    // Setter ?
    const setter = existingAttributeProp && existingAttributeProp.set
      ? existingAttributeProp.set
      : model.generateAttributeSetter(name, attrDef.type, attrDef.nullable)
    // Define getter & setter
    // model.prototype[name] = undefined
    Object.defineProperty(model.prototype, name, {
      // @ts-ignore
      get: getter,
      // @ts-ignore
      set: setter,
      enumerable: true
    })
  }

  /**
   * toModelAttributeName
   * @category methods
   * @param {string} value - The data attribute name
   * @returns {string} Returns the camelcased string
   * @description
   * Function used to transform data attribute name to model attribute name (default converts to camelcase)
   */
  static toModelAttributeName (value) {
    return camelcase(value)
  }

  /**
   * toDataPropertyName
   * @category methods
   * @param {string} value - The model attribute name
   * @returns {string} Returns the snakecased string
   * @description
   * Function used to transform model attribute name to data property name (default converts to snakecase)
   */
  static toDataPropertyName (value) {
    return snakecase(value)
  }

  /**
   * generateAttributeGetter
   * @category methods
   * @param {string} name - The attribute name
   * @returns {Function} Returns generated getter
   * @description
   * Generate default attribute getter
   */
  static generateAttributeGetter (name) {
    const model = this
    return function () {
      return this[model._privateAttrProps[name]]
    }
  }

  /**
   * generateAttributeSetter
   * @category methods
   * @param {string} name - The attribute name
   * @param {string} type - The type
   * @param {boolean} [nullable=false] - Defines if attribute is nullable
   * @returns {Function} Returns generated setter
   * @description
   * Generate default attribute setter
   */
  static generateAttributeSetter (name, type, nullable = false) {
    const model = this
    return function (value) {
      // Check if value is defined
      if (isDef(value)) {
        // Check type
        if (isNull(value)) {
          if (!nullable) {
            throw new Error(`${model.name} class: "${name}" property is not nullable...`)
          }
        } else {
          if (!typeCheck(type, value)) {
            throw new Error(`${model.name} class: "${name}" property must be of type: ${String(type)}. "${typeof value}" received...`)
          }
        }
      }
      const privateAttrProp = model._privateAttrProps[name]
      // Define as non-enumerable
      Object.defineProperty(this, privateAttrProp, {
        value: value,
        enumerable: false
      })
    }
  }

  /**
   * findByPK
   * @category methods
   * @param {*} key - The key
   * @param {Collection|Array} collection - The collection / array
   * @returns {*} Returns the found collection item
   * @description
   * Find by primary key default function.
   *
   * This function will be called to find an instance
   * for example in a model collection
   */
  static findByPK (key, collection) {
    const model = this
    if (isArray(model.primaryKey)) {
      if (!(typeof key === 'object')) {
        throw new Error(`${model.name} class has composite primary key, "findByPK" method expects object as first argument...`)
      }
      // @ts-ignore
      return _find(collection.items || collection, key)
    } else {
      return collection.find(function (value, index) {
        // @ts-ignore
        return value[model.primaryKey] === key
      })
    }
  }

  /**
   * collect
   * @category methods
   * @param {Array} [items=[]] - The items
   * @param {object} [options={}] - The collection options
   * @returns {ModelCollection} Returns a model collection
   * @description
   * Return a collection of instance of the model class
   */
  static collect (items = [], options = {}) {
    const model = this
    return new Core.context.support.collection.ModelCollection(
      model,
      items.map((item) => item instanceof model ? item : new model(item)), // eslint-disable-line new-cap
      options
    )
  }

  /**
   * Get model key
   * @param {*} obj - The obj to parse
   * @returns {Array|undefined} Returns an object with key "name" and "value"
   */
  static getPrimaryKey (obj) {
    const model = this
    if (isArray(model.primaryKey)) {
      // @ts-ignore
      return model.primaryKey.reduce((accumulator, pk) => {
        // Try to get pk value from attribute OR data attribute name
        const attributeName = isDef(obj[pk]) ? pk : model.attributes[pk].fromDataAttributeName
        const value = obj[attributeName]
        accumulator.push(isUndef(value)
          ? undefined
          : {
            name: pk,
            value: value
          }
        )
        return accumulator
      }, [])
    } else {
      // @ts-ignore
      const value = obj[model.primaryKey]
      return isUndef(value)
        ? undefined
        : {
          // @ts-ignore
          name: model.primaryKey,
          value: value
        }
    }
  }

  /**
   * @category methods
   * @param {object[]|object} payload - The data to insert.
   * If instance of model passed, will be serialized (can be Array).
   * @returns {object[]|object} - The serialized data
   * @description
   * Returns serialized data. Can be array
   */
  static serialize (payload) {
    const model = this
    let data
    if (isArray(payload)) {
      data = [] // Data is an array
      for (const item of payload) {
        data.push(item instanceof model ? item.serialize() : item)
      }
    } else {
      data = payload instanceof model ? payload.serialize() : payload
    }
    return data
  }

  /**
   * @category methods
   * @returns {object[]} Returns all model instances
   * @description
   * Returns all instance from store module state $data.
   * Calls the "all" getter.
   */
  static all () {
    const model = this
    const resourceModule = model.storeOptions.module
    return resourceModule.context.getters.all
  }

  /**
   * @category methods
   * @param {...*} args - Same arguments as "find" getter
   * @returns {object} Returns a model instance
   * @description
   * Returns an instance via the "find" getter.
   */
  static find (...args) {
    const model = this
    const resourceModule = model.storeOptions.module
    return resourceModule.context.getters.find(...args)
  }

  /**
   * @category methods
   * @param {...*} args - Same arguments as "filter" getter
   * @returns {object[]} Returns an array of model instances
   * @description
   * Returns the found instances via the "filter" getter.
   */
  static filter (...args) {
    const model = this
    const resourceModule = model.storeOptions.module
    return resourceModule.context.getters.filter(...args)
  }

  /**
   * @category methods
   * @param {*} data - The received data from data
   * @returns {object} Returns the processed data object
   * @description
   * Model static method called when data is received from server
   */
  static processData (data) {
    // Default return data
    return {
      ...data
    }
  }

  /**
   * @category methods
   * @param {string} mutationName - The mutation name ('INSERT', 'UPDATE', 'DELETE')
   * @param {Function|Function[]} func - The function(s) to append to the hook queue
   * @returns {void}
   * @description
   * **BEFORE_*** hook append method.
   * The function to append, takes the same arguments as mutation payload:
   *
   * ```ts
   * function (...args) => void
   * ```
   */
  static onBefore (mutationName, func) {
    this.hooks.store[`BEFORE_${mutationName}`].append(func)
  }

  /**
   * @category methods
   * @param {string} mutationName - The mutation name ('INSERT', 'UPDATE', 'DELETE')
   * @param {Function|Function[]} func - The function(s) to append to the hook queue
   * @returns {void}
   * @description
   * **AFTER_*** hook append method.
   * The function to append, takes the same arguments as mutation payload:
   *
   * ```ts
   * function (...args) => void
   * ```
   */
  static onAfter (mutationName, func) {
    this.hooks.store[`AFTER_${mutationName}`].append(func)
  }

  /**
   * @category methods
   * @param {object[]|object} payload - The data to insert.
   * If instance of model passed, will be serialized (can be Array).
   * @param {Function|boolean} [processData] - A function to format data to pass to "INSERT" mutation.
   * Default is false, so model processData static method will be called.
   * @returns {boolean} - Returns false if an error occurs, true otherwise.
   * @description
   * Insert method
   */
  static insert (payload, processData = false) {
    const model = this
    const resourceModule = model.storeOptions.module
    const _processData = processData || model.processData
    // Save inserted items
    const inserted = []

    // Local private insert function
    const _insert = (item) => {
      const pk = model.getPrimaryKey(item)
      // Process composite primary key values
      const pkValue = isArray(pk)
        ? pk.reduce((accumulator, key) => {
          accumulator[key.name] = key.value
          return accumulator
        }, {})
        // @ts-ignore
        : pk.value
      // Push item to upserted list
      inserted.push(model.find(pkValue))
    }

    // Serialize first
    let data = model.serialize(payload)
    // Process data
    data = isArray(data)
      // @ts-ignore
      ? data.map((item) => _processData(item))
      // @ts-ignore
      : _processData(data)
    // Commit "INSERT" mutation
    resourceModule.context.commit('INSERT', data)
    // Retrieve inserted model instances
    if (isArray(data)) {
      for (const item of data) _insert(item)
    } else {
      _insert(data)
    }
    // @ts-ignore
    return inserted
  }

  /**
   * Update method. Payload is the same as resource module "UPDATE" mutation
   * @param {object} payload - The data to update.
   * If instance of model passed, will be serialized.
   * @param {Function|boolean} [processData] - A function to format data to pass to "UPDATE" mutation.
   * Default is false, so model processData static method will be called.
   * @returns {boolean} - Returns false if an error occurs, true otherwise.
   * @description
   * Update method
   */
  static update (payload, processData = false) {
    const model = this
    const resourceModule = model.storeOptions.module
    const _processData = processData || model.processData

    let data = model.serialize(payload)
    // @ts-ignore
    data = _processData(data)

    resourceModule.context.commit('UPDATE', data)

    const pk = model.getPrimaryKey(data)
    // Process composite primary key values
    const pkValue = isArray(pk)
      ? pk.reduce((accumulator, key) => {
        accumulator[key.name] = key.value
        return accumulator
      }, {})
      // @ts-ignore
      : pk.value
    // Returns updated item
    return model.find(pkValue)
  }

  /**
   * @category methods
   * @param {object[]|object} payload - The data to upsert.
   * If instance of model passed, will be serialized (can be Array).
   * @param {Function|boolean} [processData] - A function to format data to pass to "UPSERT" mutation.
   * Default is false, so model processData static method will be called.
   * @returns {boolean} - Returns false if an error occurs, true otherwise.
   * @description
   * Upsert method
   */
  static upsert (payload, processData = false) {
    const model = this
    const resourceModule = model.storeOptions.module
    const _processData = processData || model.processData
    // Save upserted
    const upserted = []

    // Local private upsert function
    const _upsert = (item) => {
      const pk = model.getPrimaryKey(item)
      // Process composite primary key values
      const pkValue = isArray(pk)
        ? pk.reduce((accumulator, key) => {
          accumulator[key.name] = key.value
          return accumulator
        }, {})
        // @ts-ignore
        : pk.value
      // Push item to upserted list
      upserted.push(model.find(pkValue))
    }

    let data = model.serialize(payload)
    data = isArray(data)
      // @ts-ignore
      ? data.map((item) => _processData(item))
      // @ts-ignore
      : _processData(data)

    resourceModule.context.commit('UPSERT', data)

    if (isArray(data)) {
      for (const item of data) {
        _upsert(item)
      }
    } else {
      const item = data
      _upsert(item)
    }
    // @ts-ignore
    return upserted
  }

  /**
   * Delete an item from data. Payload is the same as resource module "DELETE" mutation
   * @param {Function|number|string} key - Same as find method
   * @returns {boolean} - Returns false if an error occurs, true otherwise.
   * @description
   * Delete method
   */
  static delete (key) {
    const model = this
    const resourceModule = model.storeOptions.module
    return resourceModule.context.commit('DELETE', key)
  }
}

export default Model
