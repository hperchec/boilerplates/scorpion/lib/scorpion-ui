---
title: Support/model
headline: Support/model
sidebarTitle: model
prev: ../
next: ./Model
---

# Support/model

<a name="model">
</a>

## .model

<a name="model-model">
</a>

### Core.context.support.model.Model : <code>Class.&lt;module:context/support/model/Model~Model&gt;</code>

**See**: [Model](./Model)

