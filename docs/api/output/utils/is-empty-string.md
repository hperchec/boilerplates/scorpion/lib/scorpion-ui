---
title: isEmptyString
headline: isEmptyString
sidebarTitle: isEmptyString
---

# isEmptyString

<a name="isemptystring">
</a>

## isEmptyString ⇒ <code>boolean</code>

Check if the string is empty

**Returns**: <code>boolean</code> - Returns true if string is empty

| Param | Type | Description |
| --- | --- | --- |
| str | <code>string</code> | The string |

**Example**

```js
isEmptyString('') // trueisEmptyString('foo bar') // false
```

