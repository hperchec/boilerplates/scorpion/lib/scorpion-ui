/**
 * @vuepress
 * ---
 * title: isPlainObject
 * headline: isPlainObject
 * sidebarTitle: isPlainObject
 * ---
 */

import { isPlainObject as isplainobject } from 'is-plain-object'

/**
 * isPlainObject
 * @param {*} value - The value to check
 * @returns {boolean} Returns true if value is plain object
 * @description
 * is-plain-object package method.
 *
 * ::: tip See also
 * [is-plain-object](https://github.com/jonschlinkert/is-plain-object) package documentation
 * :::
 * @example
 * isPlainObject({ a: 1, b: 2 }) // true
 * isPlainObject(new Date()) // false
 */
export const isPlainObject = function (value) {
  return isplainobject(value)
}

export default isPlainObject
