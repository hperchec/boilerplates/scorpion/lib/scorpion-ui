export function getResourceModule(store: import("vuex").Store<any>, moduleName: string, rootModule?: string | null): object;
export default getResourceModule;
//# sourceMappingURL=get-resource-module.d.ts.map