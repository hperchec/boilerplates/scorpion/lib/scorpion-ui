---
title: orderBy
headline: orderBy
sidebarTitle: orderBy
---

# orderBy

<a name="orderby">
</a>

## orderBy ⇒ <code>Array</code>

Sort array of objects (lodash.orderby).

**Returns**: <code>Array</code> - Returns the ordered array

| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | Same arguments as lodash.orderby |

**Example**

```js
const array = [ { id: 1, name: 'John' }, { id: 2, name: 'Jane' } ]orderBy(array, function (item) { return item.id }) // Return -> [ { id: 1, name: 'John' }, { id: 2, name: 'Jane' } ]orderBy(array, function (item) { return item.name }) // Return -> [ { id: 2, name: 'Jane' }, { id: 1, name: 'John' } ]
```

