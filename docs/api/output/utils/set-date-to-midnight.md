---
title: setDateToMidnight
headline: setDateToMidnight
sidebarTitle: setDateToMidnight
---

# setDateToMidnight

<a name="setdatetomidnight">
</a>

## setDateToMidnight ⇒ <code>Date</code>

Returns timestamp

**Returns**: <code>Date</code> - - Returns the date at midnight

| Param | Type | Description |
| --- | --- | --- |
| value | <code>Date</code> \| <code>string</code> | The date to parse |

**Example**

```js
const date = new Date('2023-01-01')setDateToMidnight(date) // => 2023-01-01 00:00:00.000
```

