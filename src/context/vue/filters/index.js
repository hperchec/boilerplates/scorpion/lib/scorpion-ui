/**
 * @vuepress
 * ---
 * title: Filters
 * headline: Filters
 * sidebarTitle: Filters
 * prev: ../directives/
 * next: ../mixins/
 * ---
 */

// Utils
import {
  camelcase,
  capitalize,
  constantcase,
  dateFormat,
  kebabcase,
  pascalcase,
  lowercase,
  snakecase,
  uppercase
} from '@/utils'

/**
 * @alias filters
 * @typicalname Core.context.vue.filters
 * @type {object}
 * @description
 * Contains all Vue filters that will be automatically registered.
 * Each filter is also available in `vm.$filters` for each Vue instance.
 */
export default {
  /**
   * @description
   * Same as `camelcase` util
   * ::: tip See also
   * {@link ../../utils/camelcase camelcase} util documentation
   * :::
   * @example
   * // Vue filter
   * {{ 'foo bar' | camelcase }}
   * // In Vue component
   * this.$filters.camelcase(...)
   */
  camelcase,
  /**
   * @description
   * Same as `capitalize` util
   * ::: tip See also
   * {@link ../../utils/capitalize capitalize} util documentation
   * :::
   * @example
   * // Vue filter
   * {{ 'foo bar' | capitalize }}
   * // In Vue component
   * this.$filters.capitalize(...)
   */
  capitalize,
  /**
   * @description
   * Same as `constantcase` util
   * ::: tip See also
   * {@link ../../utils/constantcase constantcase} util documentation
   * :::
   * @example
   * // Vue filter
   * {{ 'foo bar' | constantcase }}
   * // In Vue component
   * this.$filters.constantcase(...)
   */
  constantcase,
  /**
   * @description
   * Same as `dateFormat` util
   * ::: tip See also
   * {@link ../../utils/date-format dateFormat} util documentation
   * :::
   * @example
   * // Vue filter
   * {{ new Date() | dateFormat(...) }}
   * // In Vue component
   * this.$filters.dateFormat(...)
   */
  dateFormat,
  /**
   * @description
   * Same as `kebabcase` util
   * ::: tip See also
   * {@link ../../utils/kebabcase kebabcase} util documentation
   * :::
   * @example
   * // Vue filter
   * {{ 'foo bar' | kebabcase }}
   * // In Vue component
   * this.$filters.kebabcase(...)
   */
  kebabcase,
  /**
   * @description
   * Same as `pascalcase` util
   * ::: tip See also
   * {@link ../../utils/pascalcase pascalcase} util documentation
   * :::
   * @example
   * // Vue filter
   * {{ 'foo bar' | pascalcase }}
   * // In Vue component
   * this.$filters.pascalcase(...)
   */
  pascalcase,
  /**
   * @description
   * Same as `lowercase` util
   * ::: tip See also
   * {@link ../../utils/lowercase lowercase} util documentation
   * :::
   * @example
   * // Vue filter
   * {{ 'foo bar' | lowercase }}
   * // In Vue component
   * this.$filters.lowercase(...)
   */
  lowercase,
  /**
   * @description
   * Same as `snakecase` util
   * ::: tip See also
   * {@link ../../utils/snakecase snakecase} util documentation
   * :::
   * @example
   * // Vue filter
   * {{ 'foo bar' | snakecase }}
   * // In Vue component
   * this.$filters.snakecase(...)
   */
  snakecase,
  /**
   * @description
   * Same as `uppercase` util
   * ::: tip See also
   * {@link ../../utils/uppercase uppercase} util documentation
   * :::
   * @example
   * // Vue filter
   * {{ 'foo bar' | uppercase }}
   * // In Vue component
   * this.$filters.uppercase(...)
   */
  uppercase
}
