---
title: hash
headline: hash
sidebarTitle: hash
---

# hash

<a name="hash">
</a>

## hash : <code>object</code>

<a name="hash-bcrypt">
</a>

### hash.bcrypt : <code>object</code>

See [bcryptjs](https://www.npmjs.com/package/bcryptjs) package documentation

