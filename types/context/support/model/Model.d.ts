export default Model;
declare class Model {
    static primaryKey: string | string[];
    static defaultAttrTypeCast: any[];
    static attributes: object;
    static get hooks(): any;
    static get storeOptions(): any;
    static get _privateAttrProps(): any;
    static readonly get storeInterface(): any;
    static readonly get isInitialized(): boolean;
    static readonly get requiredAttributes(): string[];
    static readonly get nullableAttributes(): string[];
    static readonly get api(): any;
    static getDefaultCastForType(type: any): Function;
    static init(options: {
        attributes: object;
        store?: {
            namespace?: string;
            rootModule?: string;
            storeInstance?: any;
            onBefore?: object;
            onAfter?: object;
        };
    }): void;
    static defineAttribute(name: string, attrDef: object): void;
    static toModelAttributeName(value: string): string;
    static toDataPropertyName(value: string): string;
    static generateAttributeGetter(name: string): Function;
    static generateAttributeSetter(name: string, type: string, nullable?: boolean): Function;
    static findByPK(key: any, collection: Collection | any[]): any;
    static collect(items?: any[], options?: object): ModelCollection;
    static getPrimaryKey(obj: any): any[] | undefined;
    static serialize(payload: object[] | object): object[] | object;
    static all(): object[];
    static find(...args: any[]): object;
    static filter(...args: any[]): object[];
    static processData(data: any): object;
    static onBefore(mutationName: string, func: Function | Function[]): void;
    static onAfter(mutationName: string, func: Function | Function[]): void;
    static insert(payload: object[] | object, processData?: Function | boolean): boolean;
    static update(payload: object, processData?: Function | boolean): boolean;
    static upsert(payload: object[] | object, processData?: Function | boolean): boolean;
    static delete(key: Function | number | string): boolean;
    constructor(data: object, options?: object);
    hydrate(data: object): void;
    serialize(): object;
    toPlainObject(): object;
    toJSON(): object;
}
import Collection from '../collection/Collection';
import ModelCollection from '../collection/ModelCollection';
//# sourceMappingURL=Model.d.ts.map