export default WSConnection;
declare class WSConnection {
    static readonly get supportedDrivers(): string[];
    constructor(wsConnectionOptions: WSConnectionOptions);
    private _name;
    _options: any;
    _driver: any;
    _methodsMap: any;
    _connection: any;
    _listeners: {
        'state-change': ((states: any) => void)[];
        connecting: (() => void)[];
        connected: (() => void)[];
        disconnected: (() => void)[];
        failed: (() => void)[];
        unavailable: (() => void)[];
        error: ((event: any) => void)[];
        subscribe: {
            success: (channelName: any, channel: any) => void;
            error: (channelName: any, channel: any, error: any) => void;
        }[];
        unsubscribe: ((channelName: any, channel: any) => void)[];
    };
    set name(value: string);
    get name(): string;
    set options(value: string);
    get options(): string;
    readonly get driver(): any;
    setDriver(name: string, options?: object): void;
    mapDriverMethods(methods: object): void;
    getConnection(): any;
    connect(options?: object): any;
    disconnect(options?: object): any;
    on(eventName: string, eventCb: Function | object): void;
    subscribe(channelName: string, successCb?: Function | null, errorCb?: Function | null): any;
    unsubscribe(channelName: string): any;
    initPusher(): void;
    toJSON(): object;
}
import WSConnectionOptions from './WSConnectionOptions';
//# sourceMappingURL=WSConnection.d.ts.map