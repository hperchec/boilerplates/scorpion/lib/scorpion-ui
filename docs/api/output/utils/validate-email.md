---
title: validateEmail
headline: validateEmail
sidebarTitle: validateEmail
---

# validateEmail

<a name="validateemail">
</a>

## validateEmail ⇒ <code>boolean</code>

Returns true if email is valid

**Returns**: <code>boolean</code> - Returns true if valid email

| Param | Type | Description |
| --- | --- | --- |
| email | <code>string</code> | The email to check |

**Example**

```js
validateEmail('john.doe@example.com')
```

