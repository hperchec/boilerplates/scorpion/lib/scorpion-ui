---
title: Hook class
headline: Hook class
sidebarTitle: Hook
sidebarDepth: 0
---

# Hook class

<a name="hook">
</a>

## Hook

Hook class.

**Schema**:

- [Hook](#hook)
  - [new Hook(name, [queue])](#newhooknew)
  - [.name](#hook-name) : <code>string</code>
  - [.queue](#hook-queue) : <code>Array.&lt;function()&gt;</code>
  - [.setName(value)](#hook-setname) ⇒ <code>void</code>
  - [.setQueue(value)](#hook-setqueue) ⇒ <code>void</code>
  - [.append(func)](#hook-append) ⇒ <code>void</code>
  - [.exec(...args)](#hook-exec) ⇒ <code>Array.&lt;any&gt;</code> \| <code>Promise.&lt;Array.&lt;any&gt;&gt;</code>
  - [.compose(...args)](#hook-compose) ⇒ <code>Array</code> \| <code>Promise.&lt;Array&gt;</code>
  - [.chain(...args)](#hook-chain) ⇒ <code>Array</code> \| <code>Promise.&lt;Array&gt;</code>

<a name="newhooknew">
</a>

### new Hook(name, [queue])

Create a new Hook

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| name | <code>string</code> |  | The hook identifier |
| [queue] | <code>Array.&lt;function()&gt;</code> | <code>[]</code> | The base queue |

<a name="hook-name">
</a>

### hook.name : <code>string</code>

name

**Default**: <code>&quot;undefined&quot;</code>  
**Read only**: true  
<a name="hook-queue">
</a>

### hook.queue : <code>Array.&lt;function()&gt;</code>

queue

**Default**: <code>[]</code>  
**Read only**: true  
<a name="hook-setname">
</a>

### hook.setName(value) ⇒ <code>void</code>

Set name property

| Param | Type | Description |
| --- | --- | --- |
| value | <code>string</code> | The name |

<a name="hook-setqueue">
</a>

### hook.setQueue(value) ⇒ <code>void</code>

Set queue property

| Param | Type | Description |
| --- | --- | --- |
| value | <code>Array.&lt;function()&gt;</code> | The queue (array of function) |

<a name="hook-append">
</a>

### hook.append(func) ⇒ <code>void</code>

Add a function to exec to the queue

| Param | Type | Description |
| --- | --- | --- |
| func | <code>function</code> \| <code>Array.&lt;function()&gt;</code> | The function to append to the hook queue. Can be an array of functions. |

<a name="hook-exec">
</a>

### hook.exec(...args) ⇒ <code>Array.&lt;any&gt;</code> \| <code>Promise.&lt;Array.&lt;any&gt;&gt;</code>

Execute the queue asynchronously (in the order of execution)

**Returns**: <code>Array.&lt;any&gt;</code> \| <code>Promise.&lt;Array.&lt;any&gt;&gt;</code> - The array of result. If at least one result is a promise, so the entire result is a Promise

| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | The arguments to pass to each queue function |

<a name="hook-compose">
</a>

### hook.compose(...args) ⇒ <code>Array</code> \| <code>Promise.&lt;Array&gt;</code>

Execute the queue in a "compose" style (from the last to the first): each returned value is passed to the previous queued function.If a queued function returns undefined, the last value of payload is passed.If a returned value is a promise, so the entire result will be Promise

**Returns**: <code>Array</code> \| <code>Promise.&lt;Array&gt;</code> - Each function must return its own arguments as array

| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | The arguments to pass to each queue function |

<a name="hook-chain">
</a>

### hook.chain(...args) ⇒ <code>Array</code> \| <code>Promise.&lt;Array&gt;</code>

Execute the queue in a "chain" style (in the order of execution): wait for each returned value and then pass it to the next queued function.If a queued function returns undefined, the last value of payload is passed.If a returned value is a promise, so the entire result will be Promise

**Returns**: <code>Array</code> \| <code>Promise.&lt;Array&gt;</code> - Each function must return its own arguments as array

| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | The arguments to pass to each queue function |

