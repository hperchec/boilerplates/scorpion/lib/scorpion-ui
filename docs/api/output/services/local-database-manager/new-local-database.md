---
title: new-local-database
headline: new-local-database
---

# new-local-database

<a name="newlocaldatabase">
</a>

## newLocalDatabase ⇒ <code>module:services/local-database-manager/LocalDatabase~LocalDatabase</code>

Creates a LocalDatabase instance

**Returns**: <code>module:services/local-database-manager/LocalDatabase~LocalDatabase</code> - - The LocalDatabase instance

| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The database name |
| options | <code>object</code> | An object following LocalDatabaseOptions second arg schema |

