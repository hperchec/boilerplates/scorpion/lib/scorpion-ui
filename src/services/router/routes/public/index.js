/**
 * @vuepress
 * ---
 * title: Router - 'public' routes
 * headline: Router - 'public' routes
 * sidebarTitle: .public
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import Home from './Home'
import _CatchAll from './_CatchAll'
import NotFound from './NotFound'

/**
 * @name public
 * @static
 * @type {object}
 */
export default {
  /**
   * @alias public.Home
   * @type {object}
   * @see {@link ./Home Home}
   * @description Home route
   */
  Home,
  /**
   * @alias public._CatchAll
   * @type {object}
   * @see {@link ./_CatchAll _CatchAll}
   * @description "Catch all" route
   */
  _CatchAll,
  /**
   * @alias public.NotFound
   * @type {object}
   * @see {@link ./NotFound NotFound}
   * @description NotFound route
   */
  NotFound
}
