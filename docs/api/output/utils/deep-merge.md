---
title: deepMerge
headline: deepMerge
sidebarTitle: deepMerge
---

# deepMerge

<a name="deepmerge">
</a>

## deepMerge ⇒ <code>object</code> \| <code>Array</code>

Deep merge two Objects/Array::: tip See also[deepmerge](https://github.com/TehShrike/deepmerge) package documentation:::

**Returns**: <code>object</code> \| <code>Array</code> - Returns the result of merge

| Param | Type | Description |
| --- | --- | --- |
| x | <code>object</code> \| <code>Array</code> | The source |
| y | <code>object</code> \| <code>Array</code> | The target |
| [options] | <code>object</code> | Options |

**Example**

```js
const x = { foo: 'bar', quux: 1 }const y = { foo: 'baz', other: [] }deepMerge(x, y) // => { foo: 'baz', quux: 1, other: [] }
```

