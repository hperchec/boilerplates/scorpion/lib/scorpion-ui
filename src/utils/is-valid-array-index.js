/**
 * @vuepress
 * ---
 * title: isValidArrayIndex
 * headline: isValidArrayIndex
 * sidebarTitle: isValidArrayIndex
 * ---
 */

/**
 * isValidArrayIndex
 * @param {*} value - The value to check
 * @returns {boolean} Returns true if `value` is a valid array index
 * @description
 * Check if value is a valid array index.
 * @example
 * isValidArrayIndex({ a: 1, b: 2 }) // false
 * isValidArrayIndex(1) // true
 */
export const isValidArrayIndex = function (value) {
  const n = parseFloat(String(value))
  return n >= 0 && Math.floor(n) === n && isFinite(value)
}

export default isValidArrayIndex
