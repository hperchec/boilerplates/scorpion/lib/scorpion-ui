/**
 * @vuepress
 * ---
 * title: isPrimitive
 * headline: isPrimitive
 * sidebarTitle: isPrimitive
 * ---
 */

/**
 * isPrimitive
 * @param {*} value - The variable to check
 * @returns {boolean} Returns true if value is primitive type
 * @description
 * Check if value is primitive.
 * @example
 * isPrimitive('Hello') // => true
 * isPrimitive(new Date()) // => false
 */
export const isPrimitive = function (value) {
  return (
    typeof value === 'string' ||
    typeof value === 'number' ||
    typeof value === 'symbol' ||
    typeof value === 'boolean'
  )
}

export default isPrimitive
