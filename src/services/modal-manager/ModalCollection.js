/**
 * @vuepress
 * ---
 * title: "Service (modal-manager): ModalCollection class"
 * headline: "Service (modal-manager): ModalCollection class"
 * sidebarTitle: .ModalCollection
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import Core from '@/Core'
import { typeCheck, XArray } from '@/utils'

import Modal from './Modal'

/**
 * @extends XArray
 * @classdesc
 * Service **modal-manager**: ModalCollection class
 */
class ModalCollection extends XArray {
  /**
   * Create a new ModalCollection
   * @param {Modal[]} modals - Array of Modal instances
   */
  constructor (modals = []) {
    super([])
    if (!(modals instanceof Array)) modals = []
    // Process each element
    for (const modal of modals) {
      this.add(modal)
    }
  }

  /**
   * Accessors & mutators
   */

  // ...

  /**
   * Methods
   */

  /**
   * Get a modal by name
   * @param {string} name - The modal name
   * @returns {?Modal} The modal instance
   */
  get (name) {
    return this.find((modal) => modal.name === name)
  }

  /**
   * add
   * @category methods
   * @param {Modal} modal - The Modal to add to manager
   * @returns {ModalCollection} Return the ModalCollection instance itself
   * @description
   * Add an Modal to collection
   */
  add (modal) {
    // Check if item is a instance of Modal
    if (typeCheck(Core.context.services['modal-manager'].Modal, modal)) {
      // Check in registered modals
      const modalExists = this.get(modal.name)
      if (!modalExists) {
        this.push(modal)
      } else {
        console.log('ModalCollection can\'t add Modal: "' + modal.name + '" is already set...')
      }
    } else {
      throw new Error('ModalCollection can\'t load Modal. One of them is not a Modal instance')
    }
    return this
  }

  /**
   * Static
   */

  // ...

  /**
   * Private properties
   */

  // ...
}

export default ModalCollection
