/**
 * @vuepress
 * ---
 * title: validateEmail
 * headline: validateEmail
 * sidebarTitle: validateEmail
 * ---
 */

/**
 * validateEmail
 * @param {string} email - The email to check
 * @returns {boolean} Returns true if valid email
 * @description
 * Returns true if email is valid
 * @example
 * validateEmail('john.doe@example.com')
 */
export const validateEmail = function (email) {
  return /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(email)
}

export default validateEmail
