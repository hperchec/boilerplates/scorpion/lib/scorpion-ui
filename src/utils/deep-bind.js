/**
 * @vuepress
 * ---
 * title: deepBind
 * headline: deepBind
 * sidebarTitle: deepBind
 * ---
 */

// Use 'deep-bind' lib
import _deepBind from 'deep-bind'

/**
 * deepBind
 * @param {object|Array} target - Object or Array with functions as values that will be bound
 * @param {object} thisArg - Object to bind to the functions
 * @returns {object|Array} Object or Array with bound functions
 * @description
 * Bind a context to all functions in an object, including deeply nested functions
 *
 * ::: tip See also
 * [deep-bind](https://www.npmjs.com/package/deep-bind) package documentation
 * :::
 * @example
 * const thisArg = {
 *   foo: 'bar'
 * }
 *
 * const bound = deepBind({
 *   foo: function() {
 *     return this.foo // => "bar"
 *   }
 * }, thisArg)
 */
export const deepBind = function (target, thisArg) {
  return _deepBind(target, thisArg)
}

export default deepBind
