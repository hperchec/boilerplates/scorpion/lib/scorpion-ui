export default AuthManager;
declare const AuthManager_base: any;
declare class AuthManager extends AuthManager_base {
    [x: string]: any;
    static reactive: boolean;
    static rootOption: string;
    static aliases: any[];
    static install(_Vue: Vue, options?: object): void;
    constructor(options: {
        userModelClass: Function;
    });
    readonly get userModelClass(): Function;
    getCurrentUser(): any | null;
    setCurrentUser(user: any): any;
    toJSON(): object;
    #private;
}
//# sourceMappingURL=AuthManager.d.ts.map