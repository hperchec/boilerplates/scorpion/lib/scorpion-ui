/**
 * @vuepress
 * ---
 * title: Home route
 * headline: Home route
 * sidebarTitle: .Home
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import Core from '@/Core'
import { capitalize } from '@/utils'

/**
 * @param  {...any} args - See i18n method
 * @returns {string}
 */
// @ts-ignore
const translate = (...args) => Core.service('i18n').t(...args) // eslint-disable-line dot-notation

/**
 * @name Home
 * @static
 * @type {object}
 * @description
 * - **Name**: Home
 * - **Path**: `/`
 * - **Params**: *none*
 * - **Query**: *none*
 */
export default {
  /**
   * @description Route path
   * @name Home.path
   * @type {string}
   * @default /
   */
  path: '/',
  /**
   * @description Route name
   * @name Home.name
   * @type {string}
   * @default Home
   */
  name: 'Home',
  /**
   * @alias Home.component
   * @returns {object} Returns the view component
   * @description
   * Route component. By default, returns `Core.context.vue.components.public.views.home.Index`
   */
  component: () => Core.context.vue.components.public.views.home.Index,
  /**
   * Options
   */
  options: {
    /**
     * @description Route meta
     * @alias Home.meta
     * @type {object}
     */
    meta: {
      /**
       * @alias Home.meta.pageMeta
       * @type {object}
       * @property {Function} title - Return translated Home view title (translate: `views.Home.Index.title`)
       * @description
       * Route page meta
       */
      pageMeta: {
        title: (vm) => capitalize(translate('views.Home.Index.title'))
      }
    }
  }
}
