/**
 * @vuepress
 * ---
 * title: Route class
 * headline: Route class
 * sidebarTitle: .Route
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import Core from '@/Core'
// utils
import { getAllKeys, pascalcase, isDef, isUndef, isNull } from '@/utils'

// @ts-ignore
const log = (...args) => Core.service('logger').consoleLog(...args) // eslint-disable-line dot-notation

const createMixin = (component, route) => {
  let _comp
  if (component) {
    _comp = Core.Vue.extend(component)
    const { RouteComponentMixin } = Core.context.vue.mixins
    // Apply RouteComponentMixin
    _comp.mixin(RouteComponentMixin)
    // Apply mixin
    _comp.mixin({
      name: route.getComponentConstructorName(),
      // Get model binding prop keys
      props: Object.keys(route.modelBindings).reduce((props, key) => {
        const bindingDef = route.modelBindings[key]
        props[key] = {
          type: bindingDef.model
        }
        return props
      }, {})
    })
  }
  return _comp // Can be undefined
}

/**
 * @classdesc
 * Class that represents a route (view)
 */
class Route {
  /**
   * @private
   */
  _path
  _name
  _component
  _alias
  _redirect
  _children
  _props
  _beforeEnter
  _meta

  /**
   * Create a Route
   * @param {string} path - The route path
   * @param {string} name - The route name
   * @param {Function|null} component - A function that returns the route Vue component (can be null)
   * @param {object} [options = {}] - Object that contains other the vue-router route object properties
   * @param {string} [options.alias = undefined] - The route alias
   * @param {string|object|Function} [options.redirect = undefined] - The route redirect
   * @param {object[]} [options.children = undefined] - The route children
   * @param {object} [options.modelBindings = undefined] - The route model bindings
   * @param {object} [options.query = undefined] - The route query param definitions
   * @param {Function} [options.props = undefined] - The route props
   * @param {Function} [options.beforeEnter = undefined] - The route beforeEnter function
   * @param {object} [options.meta = undefined] - The route meta fields
   */
  constructor (path, name, component, options = {}) {
    // 'Required'
    this.path = path
    this.name = name
    this.component = component
    // Options
    this.alias = options.alias
    this.redirect = options.redirect
    this.children = options.children
    this.modelBindings = options.modelBindings || {}
    this.queryParams = options.query || {}
    this.props = options.props
    this.beforeEnter = options.beforeEnter
    this._meta = this.defaultMeta
    this.setMeta(options.meta)
  }

  /**
   * Properties
   */

  /**
   * defaultMeta
   * @category properties
   * @type {object}
   * @property {boolean} requiresAuth - If route requires authentication (default: `false`)
   * @property {object} layout - The page layout (vue component) (default: `undefined`)
   * @property {boolean} requiresVerifiedEmail - If route requires a verified email (default: `false`)
   * @property {object} breadcrumb - Breadcrumb options. See also:
   * @property {object} pageMeta - Page meta
   * @property {string} pageMeta.title - Page title
   * @description
   * Default meta
   */
  defaultMeta = {
    requiresAuth: false,
    layout: undefined,
    requiresVerifiedEmail: false,
    breadcrumb: {
      schema: [
        '<this>'
      ],
      title: (vm) => this.name
    },
    pageMeta: {
      title: (vm) => this.name
    }
  }

  /**
   * Accessors & mutators
   */

  /**
   * path
   * @category properties
   * @type {string}
   * @description
   * The route path (See also: [vue-router documentation](https://router.vuejs.org/api/#path))
   */
  get path () {
    return this._path
  }

  set path (value) {
    // Check type
    if (typeof value === 'string') {
      // Assign
      this._path = value
    } else {
      // Else -> throw error
      throw new TypeError('Route class: \'path\' property must be String. ' + typeof value + ' received...')
    }
  }

  /**
   * name
   * @category properties
   * @type {string}
   * @description
   * The route name (See also: [vue-router documentation](https://router.vuejs.org/api/#name))
   */
  get name () {
    return this._name
  }

  set name (value) {
    // Check type
    if (typeof value === 'string') {
      // Assign
      this._name = value
    } else {
      // Else -> throw error
      throw new TypeError('Route class: \'name\' property must be String. ' + typeof value + ' received...')
    }
  }

  /**
   * component
   * @category properties
   * @type {object|null}
   * @description
   * The route component (See also: [vue-router documentation](https://router.vuejs.org/guide/essentials/dynamic-matching.html))
   */
  get component () {
    return createMixin(this._component, this)
  }

  set component (value) {
    // Check type
    if (isNull(value) || typeof value === 'function') {
      // Assign
      this._component = value ? value() : null
    } else {
      // Else -> throw error
      throw new TypeError('Route class: \'component\' property must be null or Function. ' + typeof value + ' received...')
    }
  }

  /**
   * alias
   * @category properties
   * @type {string}
   * @description
   * The route alias (See also: [vue-router documentation](https://router.vuejs.org/api/#alias))
   */
  get alias () {
    return this._alias
  }

  set alias (value) {
    // Can be undefined
    if (isDef(value)) {
      // Check type
      if (typeof value === 'string') {
        // Assign
        this._alias = value
      } else {
        // Else -> throw error
        throw new TypeError('Route class: \'alias\' property must be String. ' + typeof value + ' received...')
      }
    }
  }

  /**
   * redirect
   * @category properties
   * @type {string|object|Function}
   * @description
   * The route redirect (See also: [vue-router documentation](https://router.vuejs.org/guide/essentials/redirect-and-alias.html#redirect))
   */
  get redirect () {
    return this._redirect
  }

  set redirect (value) {
    // Can be undefined
    if (isDef(value)) {
      // Check type
      if (typeof value === 'string' || typeof value === 'object' || typeof value === 'function') {
        // Assign
        this._redirect = value
      } else {
        // Else -> throw error
        throw new TypeError('Route class: \'redirect\' property must be String, Object or Function. ' + typeof value + ' received...')
      }
    }
  }

  /**
   * children
   * @category properties
   * @type {object[]}
   * @description
   * The route children (See also: [vue-router documentation](https://router.vuejs.org/api/#children))
   */
  get children () {
    return this._children
  }

  set children (value) {
    // Can be undefined
    if (isDef(value)) {
      // Check type
      if (value instanceof Array) {
        // Assign
        this._children = value
      } else {
        // Else -> throw error
        throw new TypeError('Route class: \'children\' property must be Array. ' + typeof value + ' received...')
      }
    }
  }

  /**
   * modelBindings
   * @category properties
   * @type {object}
   * @description
   * The route model bindings. An object like:
   *
   * ```js
   * {
   *   user: { model: User, param: 'user' }
   * }
   * ```
   */
  get modelBindings () {
    return this._modelBindings
  }

  set modelBindings (value) {
    // Can be undefined
    if (isDef(value)) {
      // Check type
      if (typeof value === 'object') {
        // Assign
        this._modelBindings = value
      } else {
        // Else -> throw error
        throw new TypeError('Route class: \'modelBindings\' property must be Object. ' + typeof value + ' received...')
      }
    }
  }

  /**
   * queryParams
   * @category properties
   * @type {object}
   * @description
   * The route query params. An object like:
   *
   * ```js
   * {
   *   search: { type: String, cast: value => value }
   * }
   * ```
   */
  get queryParams () {
    return this._queryParams
  }

  set queryParams (value) {
    // Can be undefined
    if (isDef(value)) {
      // Check type
      if (typeof value === 'object') {
        for (const key in value) {
          // Check "type" property
          if (isUndef(value[key].type)) {
            // If type not defined, throw error
            throw new TypeError(`Route class: query param "${key}" property must have type defined.`)
          }
        }
        // Assign
        this._queryParams = value
      } else {
        // Else -> throw error
        throw new TypeError('Route class: \'queryParams\' property must be Object. ' + typeof value + ' received...')
      }
    }
  }

  /**
   * props
   * @category properties
   * @type {?Function}
   * @description
   * The route props in function format (See also: [vue-router documentation](https://router.vuejs.org/api/#props)).
   * If model bindings are defined, props are automatically defined. Query params too, under "queryParams" prop.
   * Re-define these props to override.
   */
  get props () {
    const wrappedProps = (this.modelBindings || Object.keys(this.queryParams).length)
      ? (route) => {
        return {
          ...this.#modelBindingsToProps(route),
          ...this.#queryParamsToProps(route),
          ...(this._props ? this._props(route) : {})
        }
      }
      : this._props
    return wrappedProps
  }

  set props (value) {
    // Can be undefined
    if (isDef(value)) {
      // Check type
      if (typeof value === 'function') {
        // Assign
        this._props = value
      } else {
        // Else -> throw error
        throw new TypeError('Route class: \'props\' property must be Function. ' + typeof value + ' received...')
      }
    }
  }

  /**
   * beforeEnter
   * @category properties
   * @type {Function}
   * @description
   * The route beforeEnter function (See also: [vue-router documentation](https://router.vuejs.org/api/#beforeenter))
   */
  get beforeEnter () {
    return this._beforeEnter
  }

  set beforeEnter (value) {
    // Can be undefined
    if (isDef(value)) {
      // Check type
      if (typeof value === 'function') {
        // Assign
        this._beforeEnter = value
      } else {
        // Else -> throw error
        throw new TypeError('Route class: \'beforeEnter\' property must be Function. ' + typeof value + ' received...')
      }
    }
  }

  /**
   * meta
   * @category properties
   * @readonly
   * @type {object}
   * @description
   * The route meta fields (See also: [vue-router documentation](https://router.vuejs.org/api/#meta))
   */
  get meta () {
    return this._meta
  }

  /**
   * Methods
   */

  /**
   * setMeta
   * @category methods
   * @param {object} meta - The meta object to assign
   * @returns {void}
   * @description
   * Set meta fields
   */
  setMeta (meta) {
    if (isDef(meta)) {
      Object.assign(this._meta, meta)
    }
  }

  /**
   * @category methods
   * @returns {string} - The route component name
   * @description
   * Get the route component name
   */
  getComponentConstructorName () {
    // @ts-ignore
    return this.constructor.buildRouteComponentName(this.name)
  }

  /**
   * Overrides the default toJSON object method for JSON.stringify() calls
   * @returns {object} - The instance data to be serialized
   */
  toJSON () {
    const keys = getAllKeys(this, {
      excludeKeys: [
        /^_/, // private properties that starts with '_'
        'toJSON',
        'toString',
        'constructor'
      ]
    })
    const jsonObj = keys.reduce((obj, key) => {
      obj[key] = this[key]
      return obj
    }, {})
    return jsonObj
  }

  /**
   * Static properties
   */

  /**
   * Default query param type cast methods
   * @category properties
   * @type {Array}
   */
  static defaultQueryParamTypeCast = [
    /**
     * @todo cast for Array types
     */
    {
      type: String,
      cast: function (value) { return String(value) }
    },
    {
      type: Number,
      cast: function (value) { return Number(value) }
    },
    {
      type: Boolean,
      cast: function (value) {
        return (value === 'true' || value === '1')
          ? true
          : (value === 'false' || value === '0')
            ? false
            : Boolean(value) // fallback
      }
    }
  ]

  /**
   * Static methods
   */

  /**
   * getDefaultCastForType
   * @category methods
   * @param {*} type - The target type
   * @returns {Function} Returns the cast function
   * @description
   * Static method to get default cast method for standard types
   */
  static getDefaultCastForType (type) {
    const foundDefault = this.defaultQueryParamTypeCast.find((obj) => obj.type === type)
    return foundDefault
      ? foundDefault.cast
      : (value) => value
  }

  /**
   * @category methods
   * @param {string} name - The route name
   * @returns {string} Returns the route component constructor name
   * @description
   * Default returns name + 'RouteComponent', in pascalcase
   */
  static buildRouteComponentName (name) {
    return pascalcase(name + 'RouteComponent')
  }

  /**
   * @category methods
   * @param {?object|string} [route] - The target toute
   * @param {...any} [args] - The rest to pass to router.push method
   * @returns {void}
   * @description
   * Logic to redirect to "not found" route.
   * If no argument provided, it will take the value of `Core.config.services.router.catchAllRedirect`
   */
  static redirectToNotFound (route = undefined, ...args) {
    return Core.service('router').push(
      route || Core.config.services.router.catchAllRedirect,
      ...args
    )
  }

  /**
   * Protected methods
   */

  /**
   * @category protected methods
   * @param {object} route - The route (first arg of vue-router prop function format)
   * @returns {object} A object compatible for vue component prop defs
   * @description
   * Returns an object compatible for vue component prop defs
   */
  #modelBindingsToProps (route) {
    return Object.keys(this.modelBindings).reduce((props, key) => {
      const bindingDef = this.modelBindings[key]
      // Get model primary key name
      const modelPK = bindingDef.model.primaryKey
      // Get primary key cast method
      const castPK = bindingDef.model.attributes[modelPK].cast
      // If param is not defined in binding definition, take same name as prop
      const routeParam = route.params[bindingDef.param || key]
      // Set prop
      const bind = bindingDef.bind || (_pkValue => bindingDef.model.find(_pkValue))
      // Pass the props object itself as second argument for nested routes
      // and the route as third argument
      props[key] = bind(castPK(routeParam), props, route)
      if (isUndef(props[key])) {
        log(`Route "${route.name}": ${bindingDef.model.name} model instance not found for param: ${castPK(routeParam)}`, { type: 'router' })
        Route.redirectToNotFound()
      }
      return props
    }, {})
  }

  /**
   * @category protected methods
   * @param {object} route - The route (first arg of vue-router prop function format)
   * @returns {object} A object compatible for vue component prop defs
   * @description
   * Returns an object compatible for vue component prop defs
   */
  #queryParamsToProps (route) {
    const props = {
      queryParams: {}
    }
    for (const key in this.queryParams) {
      const queryParamDef = this.queryParams[key]
      // Get cast method
      // @ts-ignore
      const cast = queryParamDef.cast || this.constructor.getDefaultCastForType(queryParamDef.type)
      // Get value of query param
      const queryParam = route.query[key]
      // Set prop with casted value
      props.queryParams[key] = isDef(queryParam)
        ? cast(queryParam)
        : undefined
    }
    return props
  }
}

export default Route
