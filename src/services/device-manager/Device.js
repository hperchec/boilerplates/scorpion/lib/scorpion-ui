/**
 * @vuepress
 * ---
 * title: "Service (device-manager): Device class"
 * headline: "Service (device-manager): Device class"
 * sidebarTitle: .Device
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import { getAllKeys } from '@/utils'

/**
 * @classdesc
 * Service **device-manager**: Device class
 */
class Device {
  /**
   * @private
   */

  // ...

  /**
   * Create a new Device
   * @param {object} options - The device options
   */
  constructor (options) { // eslint-disable-line no-useless-constructor
    // ...
  }

  /**
   * Accessors & mutators
   */

  // ...

  /**
   * Methods
   */

  /**
   * Overrides the default toJSON object method for JSON.stringify() calls
   * @returns {object} - The instance data to be serialized
   */
  toJSON () {
    const keys = getAllKeys(this, {
      excludeKeys: [
        /^_/, // private properties that starts with '_'
        'toJSON',
        'toString',
        'constructor'
      ]
    })
    const jsonObj = keys.reduce((obj, key) => {
      obj[key] = this[key]
      return obj
    }, {})
    return jsonObj
  }

  /**
   * Static
   */

  // ...

  /**
   * Private properties
   */

  // ...
}

export default Device
