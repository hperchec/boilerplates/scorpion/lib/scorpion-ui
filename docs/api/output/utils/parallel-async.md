---
title: parallelAsync
headline: parallelAsync
sidebarTitle: parallelAsync
---

# parallelAsync

<a name="parallelasync">
</a>

## parallelAsync ⇒ <code>Promise.&lt;\*&gt;</code>

Resolve some promises in parallel with Promise.all() method::: tip See also[Promise.all()](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Promise/all) documentation:::

**Returns**: <code>Promise.&lt;\*&gt;</code> - The return value of Promise.all() call

| Param | Type | Description |
| --- | --- | --- |
| callbackFns | <code>Array</code> | The array of Promises (same as Promise.all() argument) |

