/**
 * @vuepress
 * ---
 * title: "Service (error-manager) options"
 * headline: "Service (error-manager) options"
 * sidebarTitle: Options
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import toolbox from '@/toolbox'

const { makeConfigurableOption } = toolbox.services

/**
 * @name options
 * @typicalname Core.context.services['error-manager'].options
 * @description
 * Same as ErrorManager constructor options.
 *
 * Configurable options:
 *
 * - `autoRegister`: `Core.config.services['error-manager'].autoRegister`
 */
const options = {
  // ...
}

/**
 * autoRegister
 * @alias options.autoRegister
 * @type {boolean}
 * @description
 * Automatically creates error handlers for each class/constructor under `Core.context.support.errors`.
 *
 * Can be overwritten via Core global configuration: `Core.config.services['error-manager'].autoRegister`
 *
 * Default: true
 */
makeConfigurableOption(options, {
  propertyName: 'autoRegister',
  serviceIdentifier: 'error-manager',
  configPath: 'autoRegister',
  defaultValue: true
})

export default options
