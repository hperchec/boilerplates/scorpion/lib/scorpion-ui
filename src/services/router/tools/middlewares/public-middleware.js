/**
 * Creates a public middleware: only public routes
 * @param {Function} middlewareFunc - The middleware function
 * @param {string} [metaKey = 'requiresAuth'] - The meta key
 * @returns {Function} - The encapsulated function
 */
export const publicMiddleware = (middlewareFunc, metaKey = 'requiresAuth') => {
  // Returns a closure that encapsulates the passed custom middleware function
  return async (to, from, next) => {
    // check if public
    if (to.matched.some(record => !record.meta[metaKey])) {
      await middlewareFunc(to, from, next)
    } else {
      next()
    }
  }
}

export default publicMiddleware
