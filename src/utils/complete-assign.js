/**
 * @vuepress
 * ---
 * title: completeAssign
 * headline: completeAssign
 * sidebarTitle: completeAssign
 * ---
 */

/**
 * completeAssign
 * @param {object} target - The target object
 * @param {...object} sources - The sources
 * @returns {object} Returns mutated target
 * @description
 * This is an assign function that copies full descriptors.
 * See also [MDN documentation](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Object/assign#copier_des_accesseurs)
 * @example
 * completeAssign({}, { 'a': 1, get b () { return 'b' }, set b (value) { ... } }) // Keep property descriptors (accessors/mutators, enumerable, configurable...)
 */
export const completeAssign = function (target, ...sources) {
  sources.forEach((source) => {
    const descriptors = Object.keys(source).reduce((descriptors, key) => {
      descriptors[key] = Object.getOwnPropertyDescriptor(source, key)
      return descriptors
    }, {})

    // By default, Object.assign copies enumerable Symbols, too
    Object.getOwnPropertySymbols(source).forEach((sym) => {
      const descriptor = Object.getOwnPropertyDescriptor(source, sym)
      if (descriptor.enumerable) {
        descriptors[sym] = descriptor
      }
    })
    Object.defineProperties(target, descriptors)
  })
  return target
}

export default completeAssign
