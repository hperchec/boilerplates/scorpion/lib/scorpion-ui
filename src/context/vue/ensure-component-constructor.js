/**
 * @vuepress
 * ---
 * title: "ensureComponentConstructor"
 * headline: "ensureComponentConstructor"
 * sidebarTitle: "ensureComponentConstructor"
 * prev: ../
 * ---
 */

import Core from '@/Core'
import { isVueComponent } from '@/utils'

/**
 * Ensure that target is a Vue component constructor.
 * If VNode passed, wrap it inside a functional component that just returns the VNode as is.
 * @param {import("vue").VNode|import("vue").Component|object} target - The target component
 * @returns {?import("vue").Component} Returns the component constructor. If error, returns undefined.
 */
export function ensureComponentConstructor (target) {
  // If VNode (and possibly instanciated component/instance)
  if (target.constructor && target.constructor.name === 'VNode') {
    // Returns a generic functional component that just returns the VNode
    return Core.Vue.extend({
      functional: true,
      render: () => target
    })
  // Else if it's a component constructor
  } else if (isVueComponent(target)) {
    return target
  } else {
    // Log error and return undefined
    Core.service('logger').consoleLog('[ensureComponentConstructor] Unable to process component', { type: 'error' })
    return undefined
  }
}

export default ensureComponentConstructor
