const path = require('path')
const loaderUtils = require('loader-utils')

const defaultCustomNormalizerPath = path.resolve(__dirname, '../../../lib/runtime/component-normalizer.js')

/**
 * vue-loader extension to apply our custom component normalizer function.
 * See also vue-loader/lib/runtime/componentNormalizer.js
 * @param source
 * @param map
 * @param meta
 */
module.exports = function (source, map, meta) {
  const loaderContext = this
  let codeReplacement
  // Function to resolve path (code from vue-loader)
  const stringifyRequest = (r) => loaderUtils.stringifyRequest(loaderContext, r)
  // First, get options
  const options = loaderUtils.getOptions(loaderContext) || {}
  // Normalizer path ?
  if (options.normalizerPath) {
    codeReplacement = `import normalizer from ${stringifyRequest(`!${options.normalizerPath}`)}`
  } else {
    codeReplacement = `
      import defaultVueLoaderNormalizer from '@vue/vue-loader-v15/lib/runtime/componentNormalizer.js'
      import defaultCustomNormalizer from ${stringifyRequest(`!${defaultCustomNormalizerPath}`)}
      function normalizer (...args) {
        // Result from vue-loader normalizer is an object with 'exports' and 'options' properties
        const result = defaultVueLoaderNormalizer(...args)
        result.exports = defaultCustomNormalizer(result.exports)
        return result
      }
    `
  }
  // Replace the normalizer import by our own custom
  return source.replace(
    /^import normalizer from "(.*)"/gm,
    codeReplacement
  )
}
