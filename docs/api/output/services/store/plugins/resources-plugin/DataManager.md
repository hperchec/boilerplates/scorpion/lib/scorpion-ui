---
title: DataManager class
headline: DataManager class
sidebarTitle: DataManager class
sidebarDepth: 0
prev: ./
next: false
---

# DataManager class

<a name="datamanager">
</a>

## DataManager

Store "resources-plugin" DataManager class

**Schema**:

- [DataManager](#datamanager)
  - [new DataManager(data, model, [options])](#newdatamanagernew)
  - [.model](#datamanager-model) : <code>Class.&lt;Model&gt;</code>
  - [.compositeKeyMap](#datamanager-compositekeymap)
  - [.getDataKeyFromItem(item)](#datamanager-getdatakeyfromitem) ⇒ <code>number</code> \| <code>string</code>
  - [.findBy(key, items)](#datamanager-findby) ⇒ <code>object</code>
  - [.insert(payload, [state])](#datamanager-insert) ⇒ <code>Array.&lt;object&gt;</code> \| <code>object</code> \| <code>Promise.&lt;(Array.&lt;object&gt;\|object)&gt;</code>
  - [.update(item, [state])](#datamanager-update) ⇒ <code>object</code>
  - [.upsert(payload, [state])](#datamanager-upsert) ⇒ <code>Array.&lt;object&gt;</code> \| <code>object</code>
  - [.delete(key, [state])](#datamanager-delete) ⇒ <code>object</code> \| <code>Promise.&lt;object&gt;</code>
  - _methods_
    - [.getModelCollection([state])](#datamanager-getmodelcollection) ⇒ <code>ModelCollection</code>
    - [.all([state])](#datamanager-all) ⇒ <code>Array.&lt;object&gt;</code>
    - [.find(key, [state])](#datamanager-find) ⇒ <code>object</code>
    - [.filter(callbackFn, [state])](#datamanager-filter) ⇒ <code>Array.&lt;object&gt;</code>
    - [.findMany(...args)](#datamanager-findmany) ⇒ <code>Array.&lt;object&gt;</code>

<a name="newdatamanagernew">
</a>

### new DataManager(data, model, [options])

Create a DataManager

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| data | <code>object</code> |  | A data object (state.$data reference) |
| model | <code>Class.&lt;Model&gt;</code> |  | The model class |
| [options] | <code>object</code> | <code>{}</code> | The options |
| [options.storeModuleNamespace] | <code>string</code> |  | The store module namespace (Default is model storeOptions.fullNamespace) |
| [options.findBy] | <code>function</code> |  | The "findBy" function |

<a name="datamanager-model">
</a>

### dataManager.model : <code>Class.&lt;Model&gt;</code>

<a name="datamanager-compositekeymap">
</a>

### dataManager.compositeKeyMap

Methods

<a name="datamanager-getdatakeyfromitem">
</a>

### dataManager.getDataKeyFromItem(item) ⇒ <code>number</code> \| <code>string</code>

Get data object key for model instance

**Returns**: <code>number</code> \| <code>string</code> - Returns the data property key

| Param | Type | Description |
| --- | --- | --- |
| item | <code>object</code> | The model instance object |

<a name="datamanager-findby">
</a>

### dataManager.findBy(key, items) ⇒ <code>object</code>

Default just calls model findByPK method

**Returns**: <code>object</code> - - Returns the found object

| Param | Type | Description |
| --- | --- | --- |
| key | <code>\*</code> | The key to find item |
| items | <code>Array.&lt;object&gt;</code> | The items |

<a name="datamanager-insert">
</a>

### dataManager.insert(payload, [state]) ⇒ <code>Array.&lt;object&gt;</code> \| <code>object</code> \| <code>Promise.&lt;(Array.&lt;object&gt;\|object)&gt;</code>

Insert method

**Returns**: <code>Array.&lt;object&gt;</code> \| <code>object</code> \| <code>Promise.&lt;(Array.&lt;object&gt;\|object)&gt;</code> - - Returns the inserted item(s) or a Promise that resolve it

| Param | Type | Description |
| --- | --- | --- |
| payload | <code>Array.&lt;object&gt;</code> \| <code>object</code> | The model instance. Can be Array |
| [state] | <code>object</code> | The module state |

<a name="datamanager-update">
</a>

### dataManager.update(item, [state]) ⇒ <code>object</code>

Update method

**Returns**: <code>object</code> - Returns the updated object

| Param | Type | Description |
| --- | --- | --- |
| item | <code>object</code> | The model instance |
| [state] | <code>object</code> | The module state |

<a name="datamanager-upsert">
</a>

### dataManager.upsert(payload, [state]) ⇒ <code>Array.&lt;object&gt;</code> \| <code>object</code>

Upsert method

**Returns**: <code>Array.&lt;object&gt;</code> \| <code>object</code> - - Returns the upserted item(s)

| Param | Type | Description |
| --- | --- | --- |
| payload | <code>Array.&lt;object&gt;</code> \| <code>object</code> | Can be Array |
| [state] | <code>object</code> | The module state |

<a name="datamanager-delete">
</a>

### dataManager.delete(key, [state]) ⇒ <code>object</code> \| <code>Promise.&lt;object&gt;</code>

Delete an item from data

**Returns**: <code>object</code> \| <code>Promise.&lt;object&gt;</code> - - The deleted item data

| Param | Type | Description |
| --- | --- | --- |
| key | <code>function</code> \| <code>number</code> \| <code>string</code> | Same as find method |
| [state] | <code>object</code> | The module state |

<a name="datamanager-getmodelcollection">
</a>

### dataManager.getModelCollection([state]) ⇒ <code>ModelCollection</code>

Get model collection from data

**Returns**: <code>ModelCollection</code> - - Returns the model collection

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| [state] | <code>object</code> | The module state |

<a name="datamanager-all">
</a>

### dataManager.all([state]) ⇒ <code>Array.&lt;object&gt;</code>

Find all items in $data

**Returns**: <code>Array.&lt;object&gt;</code> - - Returns an array of all items

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| [state] | <code>object</code> | The module state |

<a name="datamanager-find">
</a>

### dataManager.find(key, [state]) ⇒ <code>object</code>

Find an item by key

**Returns**: <code>object</code> - Returns the found data item

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| key | <code>function</code> \| <code>number</code> \| <code>string</code> | The key to find item. If function provided, execute this function to find item (same as Array.find function) |
| [state] | <code>object</code> | The module state |

<a name="datamanager-filter">
</a>

### dataManager.filter(callbackFn, [state]) ⇒ <code>Array.&lt;object&gt;</code>

Find some items that match the callbackFn function

**Returns**: <code>Array.&lt;object&gt;</code> - - Returns an array of filtered items

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| callbackFn | <code>function</code> | The filter callback |
| [state] | <code>object</code> | The module state |

<a name="datamanager-findmany">
</a>

### dataManager.findMany(...args) ⇒ <code>Array.&lt;object&gt;</code>

Alias of [filter](#datamanager-filter)

**Returns**: <code>Array.&lt;object&gt;</code> - - Returns an array of filtered items

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | Same as filter |

