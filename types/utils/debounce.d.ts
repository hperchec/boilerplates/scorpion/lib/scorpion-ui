export function debounce(func: Function, wait?: number, options?: object): void;
export default debounce;
//# sourceMappingURL=debounce.d.ts.map