/*
 * Root app script
 */

// Init first
import './init'
// Then, set context
import './context.js'
import { Core } from '@hperchec/scorpion-ui'
import config from '#config/config.js' // App config
import devConfig from '#config/config.dev.js' // DEV config
// Service worker
import './registerServiceWorker'

/**
 * Configure Core
 */
Core.configure(config) // base config

if (process.env.NODE_ENV === 'development') {
  // This will overrides previous configuration
  Core.configure(devConfig)
}

/**
 * main function
 */
async function main () {
  // First, setup
  await Core.setup()

  // Once app is created, services are available
  const [ logger ] = [ Core.service('logger') ]

  logger.consoleLog(`App version: ${__APP_VERSION__}`, { type: 'system' })
  logger.consoleLog('Development mode enabled', { type: 'info', prod: false })
  // Log informations about library
  logger.consoleLog.group('Core info: (Click to expand 👆)', { type: 'info', prod: false }, (subgroup) => {
    const infos = Core.info()
    for (const key in infos) console.log(`%c- ${key}:`, 'font-weight: bold;', infos[key])
  })

  // When calling runApp() method, Core will execute:
  // - onBeforeAppBoot callbacks
  // - onAppBoot callbacks
  // See: ./listeners/core/index.js
  await Core.runApp()
}

/**
 * Run!
 */
main()
