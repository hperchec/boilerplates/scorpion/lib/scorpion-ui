declare namespace _default {
    export { loadingStatusPlugin };
    export { resourcesPlugin };
    export { systemPlugin };
}
export default _default;
import loadingStatusPlugin from './loading-status-plugin';
import resourcesPlugin from './resources-plugin';
import systemPlugin from './system-plugin';
//# sourceMappingURL=index.d.ts.map