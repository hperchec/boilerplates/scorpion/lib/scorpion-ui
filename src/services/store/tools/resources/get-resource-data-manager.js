import DataManager from '../../plugins/resources-plugin/DataManager' // jsDoc type
import getResourceModule from './get-resource-module'

/**
 * Get a resource data manager
 * @param {import("vuex").Store} store - The store instance
 * @param {string} moduleName - The resource module name (ex: "Users")
 * @param {?string} [rootModule="Resources"] - The root resource module ( Default: "Resources")
 * @returns {DataManager} The module data manager
 */
export const getResourceDataManager = (store, moduleName, rootModule = 'Resources') => {
  return getResourceModule(store, moduleName, rootModule).dataManager
}

export default getResourceDataManager
