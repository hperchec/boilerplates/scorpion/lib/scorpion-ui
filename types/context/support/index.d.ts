declare namespace _default {
    export { collection };
    export { errors };
    export { model };
}
export default _default;
import collection from './collection';
import errors from './errors';
import model from './model';
//# sourceMappingURL=index.d.ts.map