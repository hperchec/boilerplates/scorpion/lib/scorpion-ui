import loadingStatusPlugin from './loading-status-plugin'
import resourcesPlugin from './resources-plugin'
import systemPlugin from './system-plugin'

export default {
  loadingStatusPlugin,
  resourcesPlugin,
  systemPlugin
}
