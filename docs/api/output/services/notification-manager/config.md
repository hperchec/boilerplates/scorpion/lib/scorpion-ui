---
title: Service (notification-manager) configuration
headline: Service (notification-manager) configuration
sidebarTitle: Configuration
prev: false
next: false
---

# Service (notification-manager) configuration

<a name="configuration">
</a>

## Configuration

Service configuration> Refers to `Core.config.services['notification-manager']````js'notification-manager': {  // Overwrites service option: `autoRegister`  autoRegister: Boolean}```

<a name="configuration-autoregister">
</a>

### Core.config.services[&#x27;notification-manager&#x27;].autoRegister : <code>boolean</code>

Auto register all error classes/constructors under `Core.context.support.errors`

