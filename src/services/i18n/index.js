/**
 * @vuepress
 * ---
 * title: "Service (i18n)"
 * headline: "Service (i18n)"
 * sidebarTitle: i18n
 * initialOpenGroupIndex: -1 # optional, defaults to 0, defines the index of initially opened subgroup
 * prev: ../
 * next: ./options
 * ---
 */

import languages from '@cospired/i18n-iso-languages'
import VueI18n from 'vue-i18n'

import { typeCheck } from '@/utils'
import Service from '@/Service'

import options from './options'
import messages from './messages'

/* eslint-disable jsdoc/require-param, jsdoc/require-returns */

/**
 * @scorpion_ui_service {VueI18n} i18n
 * @description
 * The service "i18n" is for internationalization / translations.
 * It uses the [vue-i18n](https://kazupon.github.io/vue-i18n/) package.
 */
export default new Service({
  // Service
  service: {
    // Identifier
    identifier: 'i18n',
    /**
     * @alias register
     * @description
     * On registering, the service will add mixin to root
     * instance options (`Core.context.vue.root.options.mixins`)
     * to share `LANGS` computed and define `locale` [user setting](/guide/user-settings).
     * At created hook, `locale` user setting is automatically set.
     *
     * Service also injects properties to Vue root instance prototype `$app`:
     *
     * - `$app.setLocale`
     * - `$app.currentLocale`
     */
    register: function (_Core) {
      // Define user setting in vue root instance data
      _Core.context.vue.root.setOptions({
        data () {
          return {
            shared: {
              /**
               * @alias shared.LANGS
               * @type {object}
               * @description
               * The registered languages.
               *
               * Object like:
               *
               * ```js
               * {
               *   en: { lang: 'en', name: 'English' },
               *   fr: { lang: 'fr', name: 'Français' },
               *   ...
               * }
               * ```
               * @example
               * // In any component
               * this.LANGS
               */
              LANGS: languages.langs().reduce((obj, lang) => {
                obj[lang] = {
                  lang: lang,
                  name: languages.getName(lang, lang)
                }
                return obj
              }, {})
            },
            userSettings: {
              /**
               * @alias $app.userSettings.locale
               * @type {string}
               * @description
               * The language (will be set in created hook). Default is `Core.config.services['i18n'].defaultLocale`
               */
              locale: undefined
            }
          }
        },
        created () {
          // Get saved locale from user settings or set default from Core config
          if (!this.userSettings.locale) {
            this._app.setLocale(_Core.config.services['i18n'].defaultLocale) // eslint-disable-line dot-notation
          } else {
            // Else, just set $i18n.locale
            this.$i18n.locale = this.userSettings.locale
          }
        }
      })
      // Inject properties to $app
      const $app = _Core.context.vue.root.prototype.app
      /**
       * @alias $app.setLocale
       * @param {string} locale - Locale
       * @returns {void}
       * @description
       * Set locale setting and its value in Local storage
       * @example
       * // In any component
       * this.$app.setLocale('en')
       */
      $app.setLocale = function (locale) {
        const root = this // 'this' is the Vue root instance
        // Set in user settings
        root.userSettings.locale = locale
        // Set locale
        root.$i18n.locale = locale
      }
      /**
       * @alias $app.currentLocale
       * @returns {string} The current locale
       * @description
       * Get current locale (from $i18n)
       * @example
       * this.$app.currentLocale()
       */
      $app.currentLocale = function () {
        const root = this // 'this' is the Vue root instance
        return root.$i18n.locale
      }
    },
    /**
     * @alias create
     * @description
     * Under the ground, service uses `@cospired/i18n-iso-languages` library.
     * For each `availableLocales` defined in the service **options**, the corresponding
     * language will be loaded (`@cospired/i18n-iso-languages/langs/<language>.json`).
     *
     * Returns an instance of `VueI18n` created with other options.
     */
    create: (serviceOptions, serviceContext, _Core) => {
      // Destructure options
      const { availableLocales, ...VueI18nOptions } = serviceOptions
      // Dynamically register languages defined in global configuration
      for (const language of availableLocales) { // eslint-disable-line dot-notation
        languages.registerLocale(require(`@cospired/i18n-iso-languages/langs/${language}.json`))
      }
      // @ts-ignore
      VueI18n.prototype.__v_descriptor__ = {
        namespace: 'service:i18n',
        description: 'See VueI18n documentation: https://kazupon.github.io/vue-i18n/api/#vue-injected-methods',
        optionType: 'prototype',
        type: VueI18n,
        // @ts-ignore
        externalLink: __getDocUrl__('/api/services/i18n')
      }
      // new VueI18n
      return new VueI18n(VueI18nOptions)
    },
    /**
     * @alias plugin
     * @description
     * ::: tip SEE ALSO
     * [vue-i18n](https://kazupon.github.io/vue-i18n/) documentation
     * :::
     */
    vuePlugin: (serviceContext, _Core) => VueI18n,
    // Vue plugin options
    pluginOptions: {},
    // rootOption
    rootOption: 'i18n'
  },
  /**
   * @alias onInitialized
   * @description
   * Once Core is initialized, overrides the AppError `errorMessage` instance property as Object.
   * `errorMessage` can be now a translated error object like: `{ en: String, fr: String, ... }`.
   *
   * It also defines the static method "getTranslatedError" that takes errorKey as first argument
   * and returns a translated error object. Example:
   *
   * ```js
   * AppError.getTranslatedError('e0000') // => { en: 'Unknown error', fr: 'Erreur inconnue' }
   * ```
   */
  onInitialized: (Core) => {
    /**
     * AppError.prototype.errorMessage
     */
    Object.defineProperty(Core.context.support.errors.AppError.prototype, 'errorMessage', {
      get () {
        return this._errorMessage
      },
      set (value) {
        // Check type
        if (typeCheck([ String, Object ], value)) {
          // Assign
          this._errorMessage = value
        } else {
          // Else -> throw error
          throw new Error('AppError class: \'errorMessage\' property must be String or Object. ' + typeof value + ' received...')
        }
      }
    })
    /**
     * AppError.getTranslatedError static method
     * @param {string} errorKey - The error key in messages
     * @returns {object} - A translated error object
     */
    Core.context.support.errors.AppError.getTranslatedError = function (errorKey) {
      const i18n = Core.service('i18n')
      const translated = {}
      for (const locale of i18n.availableLocales) {
        translated[locale] = i18n.messages[locale].errors[errorKey]
      }
      return translated
    }
  },
  // Options
  options,
  /**
   * @alias expose
   */
  expose: {
    /**
     * @see {@link ./messages messages}
     */
    messages: messages
  }
})
