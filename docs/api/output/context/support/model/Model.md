---
title: Model class
headline: Model class
sidebarTitle: Model class
sidebarDepth: 0
prev: ./
next: false
---

# Model class

## Classes

<dl>
<dt><a href="#Model">Model</a></dt>
<dd><p>Model class</p>
<p>Every resource of your project should have a model representation
extending this class to easily manage it.</p>
</dd>
</dl>

## Members

<dl>
<dt><a href="#namespace">namespace</a> : <code>string</code></dt>
<dd><p>Store module namespace</p>
</dd>
<dt><a href="#module">module</a> : <code>object</code></dt>
<dd><p>Target store module</p>
</dd>
</dl>

## Functions

<dl>
<dt><a href="#getStoreInstance">getStoreInstance()</a> ⇒ <code>module:vuex~Store</code></dt>
<dd><p>Store instance</p>
</dd>
</dl>

<a name="model">
</a>

## Model

Model classEvery resource of your project should have a model representationextending this class to easily manage it.

**Schema**:

- [Model](#model)
  - [new Model(data, [options])](#newmodelnew)
  - _instance_
    - [.hydrate(data)](#model-hydrate) ⇒ <code>void</code>
    - [.serialize()](#model-serialize) ⇒ <code>object</code>
    - [.toJSON()](#model-tojson) ⇒ <code>object</code>
    - _properties_
      - [.primaryKey](#model-primarykey) : <code>string</code> \| <code>Array.&lt;string&gt;</code>
      - [.defaultAttrTypeCast](#model-defaultattrtypecast) : <code>Array</code>
      - [.attributes](#model-attributes) : <code>object</code>
  - _static_
    - [.init(options)](#model-init) ⇒ <code>void</code>
    - [.getPrimaryKey(obj)](#model-getprimarykey) ⇒ <code>Array</code> \| <code>undefined</code>
    - [.update(payload, [processData])](#model-update) ⇒ <code>boolean</code>
    - [.delete(key)](#model-delete) ⇒ <code>boolean</code>
    - _methods_
      - [.getDefaultCastForType(type)](#model-getdefaultcastfortype) ⇒ <code>function</code>
      - [.defineAttribute(name, attrDef)](#model-defineattribute) ⇒ <code>void</code>
      - [.toModelAttributeName(value)](#model-tomodelattributename) ⇒ <code>string</code>
      - [.toDataPropertyName(value)](#model-todatapropertyname) ⇒ <code>string</code>
      - [.generateAttributeGetter(name)](#model-generateattributegetter) ⇒ <code>function</code>
      - [.generateAttributeSetter(name, type, [nullable])](#model-generateattributesetter) ⇒ <code>function</code>
      - [.findByPK(key, collection)](#model-findbypk) ⇒ <code>\*</code>
      - [.collect([items], [options])](#model-collect) ⇒ <code>module:context/support/collection/ModelCollection~ModelCollection</code>
      - [.serialize(payload)](#model-serialize) ⇒ <code>Array.&lt;object&gt;</code> \| <code>object</code>
      - [.all()](#model-all) ⇒ <code>Array.&lt;object&gt;</code>
      - [.find(...args)](#model-find) ⇒ <code>object</code>
      - [.filter(...args)](#model-filter) ⇒ <code>Array.&lt;object&gt;</code>
      - [.processData(data)](#model-processdata) ⇒ <code>object</code>
      - [.onBefore(mutationName, func)](#model-onbefore) ⇒ <code>void</code>
      - [.onAfter(mutationName, func)](#model-onafter) ⇒ <code>void</code>
      - [.insert(payload, [processData])](#model-insert) ⇒ <code>boolean</code>
      - [.upsert(payload, [processData])](#model-upsert) ⇒ <code>boolean</code>
    - _properties_
      - [.hooks](#model-hooks) : <code>object</code>
      - [.storeOptions](#model-storeoptions) : <code>object</code>
      - [.storeInterface](#model-storeinterface) ⇒ <code>object</code>
      - [.isInitialized](#model-isinitialized) ⇒ <code>boolean</code>
      - [.requiredAttributes](#model-requiredattributes) : <code>Array.&lt;string&gt;</code>
      - [.nullableAttributes](#model-nullableattributes) : <code>Array.&lt;string&gt;</code>
      - [.api](#model-api) ⇒ <code>object</code>

<a name="newmodelnew">
</a>

### new Model(data, [options])

Create a Model

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| data | <code>object</code> |  | Data object (data is received as { key: value, ... } object where key is in snake_case format) |
| [options] | <code>object</code> | <code>{}</code> | Options object |

<a name="model-hydrate">
</a>

### core.context.support.model.Model.hydrate(data) ⇒ <code>void</code>

Hydrate an instance with data

| Param | Type | Description |
| --- | --- | --- |
| data | <code>object</code> | The data |

<a name="model-serialize">
</a>

### core.context.support.model.Model.serialize() ⇒ <code>object</code>

Serialize a model instance following the data schema

**Returns**: <code>object</code> - Returns the serialized data

<a name="model-tojson">
</a>

### core.context.support.model.Model.toJSON() ⇒ <code>object</code>

Overrides the default toJSON object method for JSON.stringify() calls

**Returns**: <code>object</code> - - The instance data to be serialized

<a name="model-primarykey">
</a>

### core.context.support.model.Model.primaryKey : <code>string</code> \| <code>Array.&lt;string&gt;</code>

Set model primary keyDefault is "id"

**Category**: properties  
<a name="model-defaultattrtypecast">
</a>

### core.context.support.model.Model.defaultAttrTypeCast : <code>Array</code>

Default attribute type cast methods

**Category**: properties  
<a name="model-attributes">
</a>

### core.context.support.model.Model.attributes : <code>object</code>

Model attributes

**Category**: properties  
<a name="model-init">
</a>

### Model.init(options) ⇒ <code>void</code>

Statis "init" method

| Param | Type | Description |
| --- | --- | --- |
| options | <code>object</code> | The init options |
| options.attributes | <code>object</code> | The attribute definitions object |
| [options.store] | <code>object</code> | The store options |
| [options.store.namespace] | <code>string</code> | The store module namespace |
| [options.store.rootModule] | <code>string</code> | The store root module name (Default: "Resources") |
| [options.store.storeInstance] | <code>\*</code> | The custom store instance |
| [options.store.onBefore] | <code>object</code> | An object of mutation listener like: `{ 'INSERT': function (data) { ... } }` |
| [options.store.onAfter] | <code>object</code> | An object of mutation listener like: `{ 'INSERT': function (data) { ... } }` |

<a name="model-getprimarykey">
</a>

### Model.getPrimaryKey(obj) ⇒ <code>Array</code> \| <code>undefined</code>

Get model key

**Returns**: <code>Array</code> \| <code>undefined</code> - Returns an object with key "name" and "value"

| Param | Type | Description |
| --- | --- | --- |
| obj | <code>\*</code> | The obj to parse |

<a name="model-update">
</a>

### Model.update(payload, [processData]) ⇒ <code>boolean</code>

Update method

**Returns**: <code>boolean</code> - - Returns false if an error occurs, true otherwise.

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| payload | <code>object</code> |  | The data to update. If instance of model passed, will be serialized. |
| [processData] | <code>function</code> \| <code>boolean</code> | <code>false</code> | A function to format data to pass to "UPDATE" mutation. Default is false, so model processData static method will be called. |

<a name="model-delete">
</a>

### Model.delete(key) ⇒ <code>boolean</code>

Delete method

**Returns**: <code>boolean</code> - - Returns false if an error occurs, true otherwise.

| Param | Type | Description |
| --- | --- | --- |
| key | <code>function</code> \| <code>number</code> \| <code>string</code> | Same as find method |

<a name="model-getdefaultcastfortype">
</a>

### Model.getDefaultCastForType(type) ⇒ <code>function</code>

Static method to get default cast method for standard types

**Returns**: <code>function</code> - Returns the cast function

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| type | <code>\*</code> | The target type |

<a name="model-defineattribute">
</a>

### Model.defineAttribute(name, attrDef) ⇒ <code>void</code>

Set attribute and its accessor and mutator

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The attribute name |
| attrDef | <code>object</code> | The attribute definition |

<a name="model-tomodelattributename">
</a>

### Model.toModelAttributeName(value) ⇒ <code>string</code>

Function used to transform data attribute name to model attribute name (default converts to camelcase)

**Returns**: <code>string</code> - Returns the camelcased string

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| value | <code>string</code> | The data attribute name |

<a name="model-todatapropertyname">
</a>

### Model.toDataPropertyName(value) ⇒ <code>string</code>

Function used to transform model attribute name to data property name (default converts to snakecase)

**Returns**: <code>string</code> - Returns the snakecased string

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| value | <code>string</code> | The model attribute name |

<a name="model-generateattributegetter">
</a>

### Model.generateAttributeGetter(name) ⇒ <code>function</code>

Generate default attribute getter

**Returns**: <code>function</code> - Returns generated getter

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The attribute name |

<a name="model-generateattributesetter">
</a>

### Model.generateAttributeSetter(name, type, [nullable]) ⇒ <code>function</code>

Generate default attribute setter

**Returns**: <code>function</code> - Returns generated setter

**Category**: methods  
| Param | Type | Default | Description |
| --- | --- | --- | --- |
| name | <code>string</code> |  | The attribute name |
| type | <code>string</code> |  | The type |
| [nullable] | <code>boolean</code> | <code>false</code> | Defines if attribute is nullable |

<a name="model-findbypk">
</a>

### Model.findByPK(key, collection) ⇒ <code>\*</code>

Find by primary key default function.This function will be called to find an instancefor example in a model collection

**Returns**: <code>\*</code> - Returns the found collection item

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| key | <code>\*</code> | The key |
| collection | <code>module:context/support/collection/Collection~Collection</code> \| <code>Array</code> | The collection / array |

<a name="model-collect">
</a>

### Model.collect([items], [options]) ⇒ <code>module:context/support/collection/ModelCollection~ModelCollection</code>

Return a collection of instance of the model class

**Returns**: <code>module:context/support/collection/ModelCollection~ModelCollection</code> - Returns a model collection

**Category**: methods  
| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [items] | <code>Array</code> | <code>[]</code> | The items |
| [options] | <code>object</code> | <code>{}</code> | The collection options |

<a name="model-serialize">
</a>

### Model.serialize(payload) ⇒ <code>Array.&lt;object&gt;</code> \| <code>object</code>

Returns serialized data. Can be array

**Returns**: <code>Array.&lt;object&gt;</code> \| <code>object</code> - - The serialized data

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| payload | <code>Array.&lt;object&gt;</code> \| <code>object</code> | The data to insert. If instance of model passed, will be serialized (can be Array). |

<a name="model-all">
</a>

### Model.all() ⇒ <code>Array.&lt;object&gt;</code>

Returns all instance from store module state $data.Calls the "all" getter.

**Returns**: <code>Array.&lt;object&gt;</code> - Returns all model instances

**Category**: methods  
<a name="model-find">
</a>

### Model.find(...args) ⇒ <code>object</code>

Returns an instance via the "find" getter.

**Returns**: <code>object</code> - Returns a model instance

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | Same arguments as "find" getter |

<a name="model-filter">
</a>

### Model.filter(...args) ⇒ <code>Array.&lt;object&gt;</code>

Returns the found instances via the "filter" getter.

**Returns**: <code>Array.&lt;object&gt;</code> - Returns an array of model instances

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | Same arguments as "filter" getter |

<a name="model-processdata">
</a>

### Model.processData(data) ⇒ <code>object</code>

Model static method called when data is received from server

**Returns**: <code>object</code> - Returns the processed data object

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| data | <code>\*</code> | The received data from data |

<a name="model-onbefore">
</a>

### Model.onBefore(mutationName, func) ⇒ <code>void</code>

**BEFORE_*** hook append method.The function to append, takes the same arguments as mutation payload:```tsfunction (...args) => void```

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| mutationName | <code>string</code> | The mutation name ('INSERT', 'UPDATE', 'DELETE') |
| func | <code>function</code> \| <code>Array.&lt;function()&gt;</code> | The function(s) to append to the hook queue |

<a name="model-onafter">
</a>

### Model.onAfter(mutationName, func) ⇒ <code>void</code>

**AFTER_*** hook append method.The function to append, takes the same arguments as mutation payload:```tsfunction (...args) => void```

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| mutationName | <code>string</code> | The mutation name ('INSERT', 'UPDATE', 'DELETE') |
| func | <code>function</code> \| <code>Array.&lt;function()&gt;</code> | The function(s) to append to the hook queue |

<a name="model-insert">
</a>

### Model.insert(payload, [processData]) ⇒ <code>boolean</code>

Insert method

**Returns**: <code>boolean</code> - - Returns false if an error occurs, true otherwise.

**Category**: methods  
| Param | Type | Default | Description |
| --- | --- | --- | --- |
| payload | <code>Array.&lt;object&gt;</code> \| <code>object</code> |  | The data to insert. If instance of model passed, will be serialized (can be Array). |
| [processData] | <code>function</code> \| <code>boolean</code> | <code>false</code> | A function to format data to pass to "INSERT" mutation. Default is false, so model processData static method will be called. |

<a name="model-upsert">
</a>

### Model.upsert(payload, [processData]) ⇒ <code>boolean</code>

Upsert method

**Returns**: <code>boolean</code> - - Returns false if an error occurs, true otherwise.

**Category**: methods  
| Param | Type | Default | Description |
| --- | --- | --- | --- |
| payload | <code>Array.&lt;object&gt;</code> \| <code>object</code> |  | The data to upsert. If instance of model passed, will be serialized (can be Array). |
| [processData] | <code>function</code> \| <code>boolean</code> | <code>false</code> | A function to format data to pass to "UPSERT" mutation. Default is false, so model processData static method will be called. |

<a name="model-hooks">
</a>

### Model.hooks : <code>object</code>

Model hooks

**Category**: properties  
<a name="model-storeoptions">
</a>

### Model.storeOptions : <code>object</code>

The store options

**Category**: properties  
<a name="model-storeinterface">
</a>

### Model.storeInterface ⇒ <code>object</code>

Default is undefined and accessible only once store is configured for the Model.See init() static method.

**Returns**: <code>object</code> - Returns the store module context

**Category**: properties  
**Read only**: true  
<a name="model-isinitialized">
</a>

### Model.isInitialized ⇒ <code>boolean</code>

Default isInitialized is a static getter that returns false until "init" method is calledSee below

**Returns**: <code>boolean</code> - Returns false by default

**Category**: properties  
**Read only**: true  
<a name="model-requiredattributes">
</a>

### Model.requiredAttributes : <code>Array.&lt;string&gt;</code>

Contains the names of attributes that are required.⚠ Child class must call init method. See below

**Category**: properties  
**Read only**: true  
<a name="model-nullableattributes">
</a>

### Model.nullableAttributes : <code>Array.&lt;string&gt;</code>

Contains the names of attributes that are nullable.⚠ Child class must call init method. See below

**Category**: properties  
**Read only**: true  
<a name="model-api">
</a>

### Model.api ⇒ <code>object</code>

Returns an object of mapped actions from store module

**Returns**: <code>object</code> - Returns the mapped actions

**Category**: properties  
**Read only**: true  
<a name="namespace">
</a>

## namespace : <code>string</code>

Store module namespace

**Default**: <code>&quot;undefined&quot;</code>  
<a name="module">
</a>

## module : <code>object</code>

Target store module

**Default**: <code>undefined</code>  
<a name="getstoreinstance">
</a>

## getStoreInstance() ⇒ <code>module:vuex~Store</code>

Store instance

**Default**: <code>undefined</code>  
**Returns**: <code>module:vuex~Store</code> - - The store instance

