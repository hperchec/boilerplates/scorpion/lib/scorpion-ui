---
title: app
headline: app
sidebarTitle: app
initialOpenGroupIndex: -1
prev: ../
---

# app

<a name="usersettings">
</a>

## userSettings() ⇒ <code>object</code>

Get user settings (`this.$app.userSettings`)

**Returns**: <code>object</code> - Returns root instance data 'userSettings' value

