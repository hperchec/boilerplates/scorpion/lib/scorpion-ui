/**
 * @vuepress
 * ---
 * title: Router - middlewares
 * headline: Router - middlewares
 * sidebarTitle: .middlewares
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import AppMiddleware from './AppMiddleware'

/**
 * @name middlewares
 * @static
 * @type {object}
 * @description
 * > {@link ../../ context}.{@link ../ router}.{@link ./ middlewares}
 * ::: tip INFO
 * Please check the [routing](../../../../routing) documentation.
 * :::
 */
export default {
  /**
   * @alias middlewares.AppMiddleware
   * @type {Function}
   * @function
   * @see {@link ./AppMiddleware AppMiddleware}
   */
  AppMiddleware
}
