/**
 * @vuepress
 * ---
 * title: VuePluginable class
 * headline: VuePluginable class
 * sidebarTitle: .VuePluginable
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import Core from '@/Core'

/**
 * @classdesc
 * Abstract class that defines an object as a Vue plugin
 */
export class VuePluginable {
  /**
   * @private
   */
  _vm

  /**
   * VuePluginable abstract class cannot be instantiated with 'new'
   * @param {object} [data = {}] The data to send to _initVM method for reactivity (if reactive = true)
   */
  constructor (data = {}) {
    // Needed for abstract class
    if (this.constructor === VuePluginable) {
      throw new TypeError('Abstract class "VuePluginable" cannot be instantiated directly')
    } else {
      this._vm = null // Default to null
      // If reactive
      // @ts-ignore
      if (this.constructor.reactive) {
        // Init vm to use as a 'reactive' plugin
        this._initVM(data) // Init vm with received data
      }
      // Define the descriptor for debugging
      // @todo
      const vDescriptor = {
        namespace: this.constructor.name,
        description: undefined,
        optionType: 'prototype',
        type: this.constructor,
        externalLink: undefined
      }
      Object.defineProperty(this, '__v_descriptor__', {
        get () {
          return vDescriptor
        },
        enumerable: false
      })
    }
  }

  /**
   * Accessors & mutators
   */

  /**
   * vm
   * @category properties
   * @readonly
   * @type {Vue}
   */
  get vm () {
    return this._vm
  }

  /**
   * Methods
   */

  /**
   * setVMProp
   * @category methods
   * @param {string} name - Name of the property
   * @param {*} value - The value
   * @returns {void}
   * @description
   * Set a property to this._vm
   */
  setVMProp (name, value) {
    this._vm.$set(this._vm, name, value)
  }

  /**
   * _initVM
   * @category methods
   * @param {object} data - The constructor data
   * @description
   * Init the view model to use with Vue
   */
  _initVM (data) {
    if (this._vm) {
      return
    }
    // Save the initial state of silent config
    const silent = Core.Vue.config.silent
    Core.Vue.config.silent = true
    // Assign a new Vue instance with data to this._vm
    this._vm = new Core.Vue({ data })
    // Re-apply silent config
    Core.Vue.config.silent = silent
  }

  /**
   * destroyVM
   * @category methods
   * @returns {void}
   * @description
   * Destroy the vm
   */
  destroyVM () {
    this._vm.$destroy()
  }

  /**
   * Static accessors & mutators
   */

  /**
   * reactive
   * @category properties
   * @type {boolean}
   * @default false
   * @description
   * Static property 'reactive': defines if the service is reactive (will create a Vue instance to make data reactive)
   */
  static get reactive () {
    return false
  }

  /**
   * rootOption
   * @category properties
   * @type {string}
   * @default undefined
   * @description
   * Static property 'rootOption': defines if the service must be passed as Vue root instance option
   */
  static get rootOption () {
    return undefined
  }

  /**
   * aliases
   * @category properties
   * @type {string[]}
   * @default []
   * @description
   * Static property 'aliases'
   */
  static get aliases () {
    return []
  }

  /**
   * Static methods
   */

  /**
   * createMixin
   * @category methods
   * @returns {Vue.ComponentOptions<Vue>} Return the created mixin
   * @description
   * Create a mixin to apply to Vue
   */
  static createMixin () {
    // Default mixin to apply
    const mixin = {
      beforeCreate: null,
      beforeDestroy: null
    }
    const rootOption = this.rootOption
    // Check if rootOption is provided
    if (rootOption && typeof rootOption === 'string') {
      // Default VuePluginable 'beforeCreate' mixin hook
      mixin.beforeCreate = function () {
        // @ts-ignore
        const options = this.$options
        let reference
        // Check if [rootOption] is already defined for the (root) instance
        options[rootOption] = options[rootOption] || null
        if (options[rootOption]) {
          // Assign to 'private' _[rootOption]
          reference = options[rootOption]
        // Else, check the root Vue instance
        // @ts-ignore
        } else if (this.$root && this.$root['$' + rootOption]) {
          // @ts-ignore
          reference = this.$root['$' + rootOption]
        // Else, check the parent Vue instance
        } else if (options.parent && options.parent['$' + rootOption]) {
          reference = options.parent['$' + rootOption]
        }
        // Assign reference
        Object.defineProperty(this, ('_' + rootOption), {
          value: reference,
          configurable: true,
          enumerable: false
        })
      }
      // Default VuePluginable 'beforeDestroy' mixin hook
      mixin.beforeDestroy = function () {
        if (!this['_' + rootOption]) { return }
        Object.defineProperty(this, ('_' + rootOption), {
          value: null,
          configurable: true,
          enumerable: false
        })
      }
    }
    // Return the mixin
    return mixin
  }

  /**
   * install
   * @param {Vue.VueConstructor} _Vue - Vue itself
   * @param {object} [options = {}] - The options of the plugin
   * @description
   * Install method for Vue plugin
   */
  static install (_Vue, options = {}) {
    const self = this
    const rootOption = self.rootOption
    const aliases = self.aliases
    // Create a 'reactive' mixin
    _Vue.mixin(self.createMixin())
    if (rootOption) {
      // Concat aliases + rootOption
      for (const key of aliases.concat(rootOption)) {
        // Check if key is already defined
        if (_Vue.prototype.hasOwnProperty(`$${key}`)) { // eslint-disable-line no-prototype-builtins
          throw new Error(`Error when installing "${self.name}": Vue.prototype['$${key}'] is already defined...`)
        }
        // Define the getter on the 'private' property prefixed by '$'
        Object.defineProperty(_Vue.prototype, `$${key}`, {
          get () { return this['_' + rootOption] },
          enumerable: true
        })
      }
    }
  }
}

// Export VuePluginable class
export default VuePluginable
