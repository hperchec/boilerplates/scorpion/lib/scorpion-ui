---
title: Service (error-manager) configuration
headline: Service (error-manager) configuration
sidebarTitle: Configuration
prev: false
next: false
---

# Service (error-manager) configuration

<a name="configuration">
</a>

## Configuration

Service configuration> Refers to `Core.config.services['error-manager']````js'error-manager': {  // Overwrites service option: `autoRegister`  autoRegister: Boolean}```

<a name="configuration-autoregister">
</a>

### Core.config.services[&#x27;error-manager&#x27;].autoRegister : <code>boolean</code>

Auto register all error classes/constructors under `Core.context.support.errors`

