/**
 * @vuepress
 * ---
 * title: "Service (device-manager)"
 * headline: "Service (device-manager)"
 * sidebarTitle: device-manager
 * initialOpenGroupIndex: -1 # optional, defaults to 0, defines the index of initially opened subgroup
 * prev: ../
 * next: ./options
 * ---
 */

import mobileDeviceDetect from 'mobile-device-detect'

import Service from '@/Service'

import mixins from './mixins'
import options from './options'
import Device from './Device'
import DeviceManager from './DeviceManager'

/* eslint-disable jsdoc/require-param, jsdoc/require-returns */

/**
 * @scorpion_ui_service {DeviceManager} device-manager
 * @description
 * The service "device-manager" is designed to make authentication managemnent easier.
 */
export default new Service({
  // Service
  service: {
    // Identifier
    identifier: 'device-manager',
    /**
     * @alias register
     * @description
     * On registering, the service will add to root instance:
     *
     * - the `currentDevice` data property (default: undefined)
     *
     * it will also inject properties to Vue root instance prototype `$app.device`:
     *
     * - `$app.device`
     *
     * and global mixin:
     *
     * - `Core.context.vue.mixins.global.DeviceManagerMixin`: see {@link ./mixins/global/DeviceManagerMixin DeviceManagerMixin}
     */
    register: function (_Core) {
      // Define custom property in vue root instance data
      _Core.context.vue.root.setOptions({
        data () {
          return {
            currentDevice: undefined
          }
        }
      })
      // Expose mixin
      _Core.context.vue.mixins.global.DeviceManagerMixin = mixins.global.DeviceManagerMixin
      // Define vue root instance prototype in '$app' namespace
      const $app = _Core.context.vue.root.prototype.app
      /**
       * @alias $app.device
       * @type {object}
       * @description
       * Get device utils
       * @example
       * See [mobile-device-detect](https://www.npmjs.com/package/mobile-device-detect) package documentation
       */
      $app.device = mobileDeviceDetect
    },
    /**
     * @alias create
     * @description
     * Returns an instance of `DeviceManager` created with options.
     */
    create: (serviceOptions, { DeviceManager: _DeviceManager }, _Core) => {
      // Return new DeviceManager
      return new _DeviceManager(serviceOptions)
    },
    /**
     * @alias plugin
     * @description
     * The following properties will be set to Vue prototype:
     *
     * ```js
     * Vue.prototype.$deviceManager // => Core.service('device-manager')
     * ```
     */
    vuePlugin: ({ DeviceManager: _DeviceManager }, _Core) => _DeviceManager,
    // Vue plugin options
    pluginOptions: {},
    // rootOption
    rootOption: ({ DeviceManager: _DeviceManager }, _Core) => _DeviceManager.rootOption
  },
  // On initialized
  onInitialized: (Core) => {
    // ...
  },
  // Options
  options,
  /**
   * @alias expose
   */
  expose: {
    /**
     * @see {@link ./Device Device}
     */
    Device,
    /**
     * @see {@link ./DeviceManager DeviceManager}
     */
    DeviceManager
  }
})
