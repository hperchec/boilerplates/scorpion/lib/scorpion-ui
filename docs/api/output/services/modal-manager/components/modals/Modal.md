# Modal

**Modal** component

> This component is globally registered by Vue

**Extends**:

- BaseModal: `BaseModal.js`

## Slots

| Name | Description | Bindings |
| - | - | - |
| header | | |
| default | | |
| footer | | |

