/**
 * @vuepress
 * ---
 * title: Components
 * headline: Components
 * sidebarTitle: Components
 * initialOpenGroupIndex: -1 # optional, defaults to 0, defines the index of initially opened subgroup
 * prev: ../
 * next: ./commons/
 * ---
 */

import commons from './commons'
import App from './App.vue'

/**
 * @typicalname Core.context.vue.components
 * @description
 * Register all app components
 */
export const components = {
  /**
   * The main App component
   * @see {@link ./App App}
   */
  App,
  /**
   * The common components (globally registered)
   * @see {@link ./commons commons}
   */
  commons
}

export default components
