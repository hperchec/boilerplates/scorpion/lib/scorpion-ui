---
title: "Service (api-manager): RequestManager class"
headline: "Service (api-manager): RequestManager class"
sidebarTitle: .RequestManager
sidebarDepth: 0
prev: false
next: false
---

# Service (api-manager): RequestManager class

<a name="requestmanager">
</a>

## RequestManager

Service **api-manager**: RequestManager class

**Schema**:

- [RequestManager](#requestmanager)
  - [new RequestManager(apiInstance)](#newrequestmanagernew)
  - [.toJSON()](#requestmanager-tojson) ⇒ <code>object</code>
  - _methods_
    - [.getAPIInstance()](#requestmanager-getapiinstance) ⇒ <code>module:services/api-manager/API~API</code>
    - [.createRequest(generator, method, url, [options], [beforeCb], [successCb], [errorCb])](#requestmanager-createrequest) ⇒ <code>module:services/api-manager/Request~Request</code>
    - [.push(key, promise)](#requestmanager-push) ⇒ <code>Promise</code>
  - _public methods_
    - [.onBeforeRequest(func)](#requestmanager-onbeforerequest) ⇒ <code>void</code>
    - [.onRequestSuccess(func)](#requestmanager-onrequestsuccess) ⇒ <code>void</code>
    - [.onRequestError(func)](#requestmanager-onrequesterror) ⇒ <code>void</code>

<a name="newrequestmanagernew">
</a>

### new RequestManager(apiInstance)

Create a new RequestManager

| Param | Type | Description |
| --- | --- | --- |
| apiInstance | <code>module:services/api-manager/API~API</code> | The API instance that creates this RequestManager |

<a name="requestmanager-tojson">
</a>

### requestManager.toJSON() ⇒ <code>object</code>

Overrides the default toJSON object method for JSON.stringify() calls

**Returns**: <code>object</code> - - The instance data to be serialized

<a name="requestmanager-getapiinstance">
</a>

### requestManager.getAPIInstance() ⇒ <code>module:services/api-manager/API~API</code>

Get the related API instance

**Returns**: <code>module:services/api-manager/API~API</code> - - The API instance

**Category**: methods  
<a name="requestmanager-createrequest">
</a>

### requestManager.createRequest(generator, method, url, [options], [beforeCb], [successCb], [errorCb]) ⇒ <code>module:services/api-manager/Request~Request</code>

Create a new request

**Returns**: <code>module:services/api-manager/Request~Request</code> - The request instance

**Category**: methods  
| Param | Type | Default | Description |
| --- | --- | --- | --- |
| generator | <code>function</code> |  | The request function (must return a Promise) |
| method | <code>string</code> |  | The http verb |
| url | <code>string</code> \| <code>URL</code> |  | The URL to request |
| [options] | <code>object</code> | <code>{}</code> | The request option |
| [beforeCb] | <code>function</code> |  | The function to exec before request |
| [successCb] | <code>function</code> |  | The success callback |
| [errorCb] | <code>function</code> |  | The error callback |

<a name="requestmanager-push">
</a>

### requestManager.push(key, promise) ⇒ <code>Promise</code>

Add the request to corresponding queue

**Returns**: <code>Promise</code> - - The added promise

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| key | <code>string</code> | The request key |
| promise | <code>Promise</code> | The request promise |

<a name="requestmanager-onbeforerequest">
</a>

### requestManager.onBeforeRequest(func) ⇒ <code>void</code>

**BEFORE_REQUEST** hook append method.The function to append, takes one arguments:```tsfunction (request: Request) => Array|void```- **request**: `{Request}` the request instance

**Category**: public methods  
| Param | Type | Description |
| --- | --- | --- |
| func | <code>function</code> \| <code>Array.&lt;function()&gt;</code> | The function(s) to append to the hook queue |

**Example**

```js
requestManager.onBeforeRequest((request) => {  console.log(`Before request!`, request)})
```

<a name="requestmanager-onrequestsuccess">
</a>

### requestManager.onRequestSuccess(func) ⇒ <code>void</code>

**REQUEST_SUCCESS** hook append method.The function to append, takes twp arguments:```tsfunction (response: Object, request: Request) => void```- **response**: `{Object}` the response object- **request**: `{Request}` the request instance

**Category**: public methods  
| Param | Type | Description |
| --- | --- | --- |
| func | <code>function</code> \| <code>Array.&lt;function()&gt;</code> | The function(s) to append to the hook queue |

**Example**

```js
requestManager.onRequestSuccess((response, request) => {  console.log(`Request success!`, response)})
```

<a name="requestmanager-onrequesterror">
</a>

### requestManager.onRequestError(func) ⇒ <code>void</code>

**REQUEST_ERROR** hook append method.The function to append, takes two arguments:```tsfunction (error: Object, request: Request) => void```- **error**: `{Object}` the error object- **request**: `{Request}` the request instance

**Category**: public methods  
| Param | Type | Description |
| --- | --- | --- |
| func | <code>function</code> \| <code>Array.&lt;function()&gt;</code> | The function(s) to append to the hook queue |

**Example**

```js
requestManager.onRequestError((error, request) => {  console.log(`Request error!`, error)})
```

