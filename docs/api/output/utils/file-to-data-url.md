---
title: fileToDataUrl
headline: fileToDataUrl
sidebarTitle: fileToDataUrl
---

# fileToDataUrl

<a name="filetodataurl">
</a>

## fileToDataUrl ⇒ <code>Promise.&lt;(string\|ArrayBuffer)&gt;</code>

Convert File object to data url (see also)

**Returns**: <code>Promise.&lt;(string\|ArrayBuffer)&gt;</code> - Returns data url string

| Param | Type | Description |
| --- | --- | --- |
| file | <code>File</code> | The target file |

**Example**

```js
fileToDataUrl(file)
```

