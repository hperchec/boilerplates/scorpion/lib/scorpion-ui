export default KeyboardEventManager;
declare const KeyboardEventManager_base: any;
declare class KeyboardEventManager extends KeyboardEventManager_base {
    [x: string]: any;
    static reactive: boolean;
    static rootOption: string;
    static aliases: string[];
    static install(_Vue: Vue, options?: object): void;
    get scopes(): Set<any>;
    ctrlKey(scope: string, keys: string, onKeydown: Function, onKeyup: Function): void;
    altKey(scope: string, keys: string, onKeydown: Function, onKeyup: Function): void;
    ctrlAltKey(scope: string, keys: string, onKeydown: Function, onKeyup: Function): void;
    attemptPrompt(keys: string[] | any[][], resolvedCb?: Function, badCombinationCb?: Function): object;
    toJSON(): object;
    #private;
}
//# sourceMappingURL=KeyboardEventManager.d.ts.map