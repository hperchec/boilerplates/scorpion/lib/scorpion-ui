/**
 * @vuepress
 * ---
 * title: typeCheck
 * headline: typeCheck
 * sidebarTitle: typeCheck
 * ---
 */

// Utils
import isPlainObject from './is-plain-object.js'
import getConstructorName from './get-constructor-name'

// RegExp to find primitive type
const simpleCheckRE = /^(String|Number|Boolean|Function|Symbol|BigInt)$/

/**
 * typeCheck
 * @param {*|Array<*>} type - The type to check (as a Vue prop type format). Can be array of types
 * @param {*} value - The value to check
 * @returns {boolean} Returns true if at least one type matches
 * @description
 * Based on Vue.js prop type parser, check type of a variable
 * @example
 * typeCheck(String, 'John Doe') // true
 * typeCheck(MyCustomClass, 1) // false
 * typeCheck([String, Number], '1') // true
 */
export const typeCheck = function (type, value) {
  // Function to check only one type
  const _check = (_type) => {
    let valid
    // Find constructor name
    const expectedType = getConstructorName(_type)
    // If we match primitive type
    if (simpleCheckRE.test(expectedType)) {
      const t = typeof value
      valid = t === expectedType.toLowerCase()
      // for primitive wrapper objects
      if (!valid && t === 'object') {
        valid = value instanceof _type
      }
    // Else if Object
    } else if (expectedType === 'Object') {
      valid = isPlainObject(value)
    // Else if Array
    } else if (expectedType === 'Array') {
      valid = Array.isArray(value)
    // Else -> custom class
    } else {
      try {
        valid = value instanceof _type
      } catch (e) {
        valid = false
      }
    }
    return valid
  }
  // "type" arg can be array of types
  if (type instanceof Array) {
    for (const t of type) {
      if (_check(t)) return true // Break and return at first match
    }
    return false
  } else {
    return _check(type)
  }
}

export default typeCheck
