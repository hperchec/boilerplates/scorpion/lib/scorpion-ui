/**
 * @vuepress
 * ---
 * title: sortBy
 * headline: sortBy
 * sidebarTitle: sortBy
 * ---
 */

// Lodash.sortBy
import lodashSortBy from 'lodash.sortby'

/**
 * sortBy
 * @param {Array} array - The array
 * @param {Function} sortFct - The sort function
 * @returns {Array} Returns the sorted array
 * @description
 * Sort array of objects (lodash.sortby).
 *
 * ::: tip See also
 * [lodash.union](https://www.npmjs.com/package/lodash.union) package documentation
 * :::
 * @example
 * const array = [ { id: 1, name: 'John' }, { id: 2, name: 'Jane' } ]
 * sortBy(array, function (item) { return item.id }) // Return -> [ { id: 1, name: 'John' }, { id: 2, name: 'Jane' } ]
 * sortBy(array, function (item) { return item.name }) // Return -> [ { id: 2, name: 'Jane' }, { id: 1, name: 'John' } ]
 */
export const sortBy = function (array, sortFct) {
  return lodashSortBy(array, sortFct)
}

export default sortBy
