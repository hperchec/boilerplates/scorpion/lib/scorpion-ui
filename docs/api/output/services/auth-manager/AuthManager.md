---
title: "Service (auth-manager): AuthManager class"
headline: "Service (auth-manager): AuthManager class"
sidebarTitle: .AuthManager
sidebarDepth: 0
prev: false
next: false
---

# Service (auth-manager): AuthManager class

<a name="authmanager">
</a>

## AuthManager ⇐ <code>VuePluginable</code>

Service **auth-manager**: AuthManager class

**Extends**: <code>VuePluginable</code>  
**Schema**:

- [AuthManager](#authmanager) ⇐ <code>VuePluginable</code>
  - [new AuthManager(options)](#newauthmanagernew)
  - _instance_
    - [.reactive](#authmanager-reactive)
    - [.getCurrentUser()](#authmanager-getcurrentuser) ⇒ <code>any</code>
    - [.toJSON()](#authmanager-tojson) ⇒ <code>object</code>
    - _methods_
      - [.setCurrentUser(user)](#authmanager-setcurrentuser) ⇒ <code>any</code>
    - _properties_
      - [.userModelClass](#authmanager-usermodelclass) : <code>function</code>
  - _static_
    - [.install(_Vue, [options])](#authmanager-install)

<a name="newauthmanagernew">
</a>

### new AuthManager(options)

Create a new instance

| Param | Type | Description |
| --- | --- | --- |
| options | <code>object</code> | The constructor options |
| options.userModelClass | <code>function</code> | The User model class |

<a name="authmanager-reactive">
</a>

### authManager.reactive

Overrides static VuePluginable properties

<a name="authmanager-getcurrentuser">
</a>

### authManager.getCurrentUser() ⇒ <code>any</code>

Returns the current authenticated user.

**Returns**: <code>any</code> - The current user object

<a name="authmanager-tojson">
</a>

### authManager.toJSON() ⇒ <code>object</code>

Overrides the default toJSON object method for JSON.stringify() calls

**Returns**: <code>object</code> - - The instance data to be serialized

<a name="authmanager-setcurrentuser">
</a>

### authManager.setCurrentUser(user) ⇒ <code>any</code>

Set the current authenticated user

**Returns**: <code>any</code> - Returns the user

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| user | <code>any</code> | The user to set as current authenticated |

<a name="authmanager-usermodelclass">
</a>

### authManager.userModelClass : <code>function</code>

Get User model class

**Category**: properties  
**Read only**: true  
<a name="authmanager-install">
</a>

### AuthManager.install(_Vue, [options])

Install method for Vue

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| _Vue | <code>Vue</code> |  | Vue |
| [options] | <code>object</code> | <code>{}</code> | The options of the plugin |

