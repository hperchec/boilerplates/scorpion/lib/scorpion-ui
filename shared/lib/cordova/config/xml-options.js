const xmlOptions = {
  allowBooleanAttributes: true,
  // attrNodeName: "attr", //default is 'false'
  attributesGroupName: '@_', // tag attributes are grouped under '@_' property
  attributeNamePrefix: '',
  ignoreAttributes: false,
  parseTagValue: true,
  format: true,
  indentBy: '  ',
  supressEmptyNode: false
}

module.exports = xmlOptions
