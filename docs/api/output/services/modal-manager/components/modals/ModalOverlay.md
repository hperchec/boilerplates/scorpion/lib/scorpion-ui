# ModalOverlay

**ModalOverlay** component

> This component is globally registered by Vue

## Props

| Prop name | Description | Type | Values | Default | Origin |
| - | - | - | - | - | - |
| modal | | `object` | | | |

