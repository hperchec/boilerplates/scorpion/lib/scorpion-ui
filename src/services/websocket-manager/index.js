/**
 * @vuepress
 * ---
 * title: "Service (websocket-manager)"
 * headline: "Service (websocket-manager)"
 * sidebarTitle: websocket-manager
 * initialOpenGroupIndex: -1 # optional, defaults to 0, defines the index of initially opened subgroup
 * prev: ../
 * next: ./options
 * ---
 */

import Service from '@/Service'

import options from './options'
import WSConnection from './WSConnection'
import WebSocketManager from './WebSocketManager'
import WSConnectionOptions from './WSConnectionOptions'

/* eslint-disable jsdoc/require-param, jsdoc/require-returns */

/**
 * @scorpion_ui_service {WebSocketManager} websocket-manager
 * @description
 * The service "websocket-manager" is designed to make websocket connections managemnent easier.
 *
 * It references any connection needed and provides methods and tools.
 */
export default new Service({
  // Service
  service: {
    // Identifier
    identifier: 'websocket-manager',
    // Register function
    register: function (_Core) {
      // ...
    },
    /**
     * @alias create
     * @description
     * Returns an instance of `WebSocketManager` created with options.
     */
    create: (serviceOptions, { WebSocketManager: _WebSocketManager }, _Core) => {
      // Return new WebSocketManager
      return new _WebSocketManager(serviceOptions)
    },
    /**
     * @alias plugin
     * @description
     * The following properties will be set to Vue prototype:
     *
     * ```js
     * // Each references to Core.service('websocket-manager')
     * Vue.prototype.$webSocketManager
     * Vue.prototype.$wsManager
     * ```
     */
    vuePlugin: ({ WebSocketManager: _WebSocketManager }, _Core) => _WebSocketManager,
    // Vue plugin options
    pluginOptions: {},
    // rootOption
    rootOption: ({ WebSocketManager: _WebSocketManager }, _Core) => _WebSocketManager.rootOption
  },
  /**
   * @alias onInitialized
   * @description
   * Once Core is initialized, if the service "logger" is registered too, it will define a `Logger` {@link ../logger/Logger#types type} as following:
   *
   * ```js
   * websocket: {
   *   badgeContent: '[WS]',
   *   badgeColor: '#CDC1E6',
   *   badgeBgColor: '#6D24E3',
   *   messageColor: '#6D24E3',
   *   prependMessage: '💬'
   * }
   * ```
   */
  onInitialized: (Core) => {
    // If 'logger' service is used, set 'websocket' type in config
    if (Core.hasServiceRegistered('logger')) {
      Core.onBeforeCreateService('logger', ({ types }) => {
        // Check if 'websocket' type is defined, else set it
        if (!types.websocket) {
          types.websocket = {
            badgeContent: '[WS]',
            badgeColor: '#CDC1E6',
            badgeBgColor: '#6D24E3',
            messageColor: '#6D24E3',
            prependMessage: '💬'
          }
        }
      })
    }
  },
  // Options
  options,
  /**
   * @alias expose
   */
  expose: {
    /**
     * @see {@link ./WSConnection WSConnection}
     */
    WSConnection,
    /**
     * @see {@link ./WebSocketManager WebSocketManager}
     */
    WebSocketManager,
    /**
     * @see {@link ./WSConnectionOptions WSConnectionOptions}
     */
    WSConnectionOptions
  }
})
