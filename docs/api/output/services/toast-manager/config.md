---
title: Service (toast-manager) configuration
headline: Service (toast-manager) configuration
sidebarTitle: Configuration
prev: false
next: false
---

# Service (toast-manager) configuration

<a name="configuration">
</a>

## Configuration

Service configuration> Refers to `Core.config.services['toast-manager']````js'toast-manager': {  // Toaster definitions  toasters: {    // <name> is the toaster name    '<name>': {      beforePublish: Function, // (optional) Toaster constructor `options.beforePublish`      afterPublish: Function, // (optional) Toaster constructor `options.afterPublish`      beforeDelete: Function, // (optional) Toaster constructor `options.beforeDelete`      afterDelete: Function, // (optional) Toaster constructor `options.afterDelete`    },    ...  }}```

<a name="configuration-toasters">
</a>

### Core.config.services[&#x27;toast-manager&#x27;].toasters : <code>object</code>

Toaster definitions. An object following the schema:```js{  [key: String]: {    beforePublish?: Function,    afterPublish?: Function,    beforeDelete?: Function,    afterDelete?: Function  }}```Object keys are the toaster names while value is a Toaster constructor options object.

