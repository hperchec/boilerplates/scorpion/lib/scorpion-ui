---
title: "Service (api-manager): RequestError class"
headline: "Service (api-manager): RequestError class"
sidebarTitle: RequestError
sidebarDepth: 0
prev: false
next: false
---

# Service (api-manager): RequestError class

<a name="requesterror">
</a>

## RequestError

Service **api-manager**: RequestError class

**Schema**:

- [RequestError](#requesterror)
  - [new RequestError(original, request)](#newrequesterrornew)
  - _instance_
    - [.name](#requesterror-name)
    - [.errorCode](#requesterror-errorcode) : <code>string</code>
    - [.hasResponse()](#requesterror-hasresponse) ⇒ <code>boolean</code>
    - [.toTypedError(customClass)](#requesterror-totypederror) ⇒ <code>\*</code>
    - _properties_
      - [.request](#requesterror-request) : <code>module:services/api-manager/Request~Request</code>
      - [.response](#requesterror-response) : <code>object</code>
  - _static_
    - [.caught(error)](#requesterror-caught) ⇒ <code>void</code>

<a name="newrequesterrornew">
</a>

### new RequestError(original, request)

Creates a RequestError instance

| Param | Type | Description |
| --- | --- | --- |
| original | <code>AxiosError</code> \| <code>module:services/api-manager/support/errors/RequestError~RequestError</code> \| <code>Error</code> | The original AxiosError or a RequestError instance |
| request | <code>module:services/api-manager/Request~Request</code> | The request instance |

<a name="requesterror-name">
</a>

### requestError.name

The error name

<a name="requesterror-errorcode">
</a>

### requestError.errorCode : <code>string</code>

The error code

<a name="requesterror-hasresponse">
</a>

### requestError.hasResponse() ⇒ <code>boolean</code>

Check if request error has response

**Returns**: <code>boolean</code> - Returns true if the request has response, otherwise false

<a name="requesterror-totypederror">
</a>

### requestError.toTypedError(customClass) ⇒ <code>\*</code>

Converts to the target custom error class

**Returns**: <code>\*</code> - Returns the instance of custom class

| Param | Type | Description |
| --- | --- | --- |
| customClass | <code>Class.&lt;module:services/api-manager/support/errors/RequestError~RequestError&gt;</code> | The custom error class |

<a name="requesterror-request">
</a>

### requestError.request : <code>module:services/api-manager/Request~Request</code>

Request object

**Category**: properties  
<a name="requesterror-response">
</a>

### requestError.response : <code>object</code>

Response object

**Category**: properties  
**Read only**: true  
<a name="requesterror-caught">
</a>

### RequestError.caught(error) ⇒ <code>void</code>

Do something when custom error is caught by ErrorManager

| Param | Type | Description |
| --- | --- | --- |
| error | <code>\*</code> | The error instance |

