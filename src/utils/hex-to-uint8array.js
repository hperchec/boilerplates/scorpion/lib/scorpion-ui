/**
 * @vuepress
 * ---
 * title: hexToUint8Array
 * headline: hexToUint8Array
 * sidebarTitle: hexToUint8Array
 * ---
 */

// hex-encoding
import hexEncoding from 'hex-encoding'

/**
 * hexToUint8Array
 * @param {string} str - The hexadecimal string
 * @returns {Uint8Array} Returns the decoded string as Uint8Array
 * @description
 * Convert a hexadecimal string to Uint8Array. (Equivalent to php hex2bin function)
 *
 * ::: tip See also
 * [hex-encoding](https://www.npmjs.com/package/hex-encoding) package documentation
 * :::
 * @example
 * hexToUint8Array('666f6f20626172') // => Uint8Array containing string: 'foo bar'
 */
export const hexToUint8Array = function (str) {
  return hexEncoding.decode(str)
}

export default hexToUint8Array
