/**
 * @vuepress
 * ---
 * title: "Service (keyboard-event-manager) options"
 * headline: "Service (keyboard-event-manager) options"
 * sidebarTitle: Options
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * @name options
 * @typicalname Core.context.services['keyboard-event-manager'].options
 * @description
 * Keyboard events manager service options
 */
const options = {
  // ...
}

export default options
