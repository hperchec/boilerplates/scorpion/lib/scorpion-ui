/**
 * @vuepress
 * ---
 * title: "Service (api-manager): RequestError class"
 * headline: "Service (api-manager): RequestError class"
 * sidebarTitle: RequestError
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import { AxiosError } from 'axios'
import AppError from '@/context/support/errors/AppError'
import { typeCheck } from '@/utils'

import Request from '../../Request'

/**
 * @extends AppError
 * @classdesc
 * Service **api-manager**: RequestError class
 */
class RequestError extends AppError {
  /**
   * @private
   */
  _request
  _response = {
    payload: {},
    status: undefined,
    original: undefined
  }

  /**
   * Creates a RequestError instance
   * @param {AxiosError|RequestError|Error} original - The original AxiosError or a RequestError instance
   * @param {Request} request - The request instance
   */
  constructor (original, request) {
    // IMPORTANT: RequestError class will inherits from AppError once Core is initialized, see service index.js
    super(original.message)
    if (original instanceof RequestError) {
      Object.setPrototypeOf(original, this.constructor.prototype)
      // @ts-ignore
      return original
    } else {

    }
    this.original = original
    // Check type
    if (typeCheck(Request, request)) {
      // Assign
      this._request = request
    } else {
      // Else -> throw error
      throw new TypeError('RequestError class: \'request\' property must be instance of Request. ' + typeof request + ' received...')
    }
    this.init()
    if (original instanceof AxiosError) {
      this.response.original = original.response // XMLHttpRequest
      // If response is defined
      if (this.response.original) {
        // Status
        // @ts-ignore
        this.response.status = this.response.original.status
      }
    } else if (original instanceof Error) {
      // ...
    } else {
      console.log('RequestError constructor: unable to process the provided first argument: ', original)
      throw new Error('RequestError constructor requires instance of AxiosError, RequestError or Error as first argument')
    }
  }

  /**
   * Properties
   */

  /**
   * The error name
   */
  name = 'RequestError'

  /**
   * Accessors and mutators
   */

  /**
   * Request object
   * @category properties
   * @type {Request}
   */
  get request () {
    return this._request
  }

  /**
   * Response object
   * @category properties
   * @readonly
   * @type {object}
   */
  get response () {
    return this._response
  }

  /**
   * Methods
   */

  /**
   * Check if request error has response
   * @returns {boolean} Returns true if the request has response, otherwise false
   */
  hasResponse () {
    return Boolean(this.response.original && this.response.original.status)
  }

  /**
   * Converts to the target custom error class
   * @param {typeof RequestError} customClass - The custom error class
   * @returns {*} Returns the instance of custom class
   */
  toTypedError (customClass) {
    return new customClass(this.original, this.request) // eslint-disable-line new-cap
  }

  /**
   * Static methods
   */

  /**
   * Do something when custom error is caught by ErrorManager
   * @param {*} error - The error instance
   * @returns {void}
   */
  static caught (error) { // eslint-disable-line handle-callback-err
    // Do nothing...
  }

  /**
   * Static properties
   */

  /**
   * The error code
   * @type {string}
   */
  static errorCode = 'e0010'
}

export default RequestError
