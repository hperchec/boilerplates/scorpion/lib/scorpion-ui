---
title: NotFound route
headline: NotFound route
sidebarTitle: .NotFound
sidebarDepth: 0
prev: false
next: false
---

# NotFound route

## Functions

<dl>
<dt><a href="#translate">translate(...args)</a> ⇒ <code>string</code></dt>
<dd></dd>
</dl>

<a name="translate">
</a>

## translate(...args) ⇒ <code>string</code>

| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>any</code> | See i18n method |

<a name="notfound">
</a>

## .NotFound : <code>object</code>

- **Name**: NotFound- **Path**: `/404`- **Params**: *none*- **Query**: *none*

**Schema**:

- [.NotFound](#notfound) : <code>object</code>
  - [.path](#notfound-path) : <code>string</code>
  - [.name](#notfound-name) : <code>string</code>
  - [.meta](#notfound-meta) : <code>object</code>
    - [.pageMeta](#notfound-meta-pagemeta) ⇒ <code>string</code>
  - [.component()](#notfound-component) ⇒ <code>object</code>

<a name="notfound-path">
</a>

### NotFound.path : <code>string</code>

Route path

**Default**: <code>&quot;/404&quot;</code>  
<a name="notfound-name">
</a>

### NotFound.name : <code>string</code>

Route name

**Default**: <code>&quot;NotFound&quot;</code>  
<a name="notfound-meta">
</a>

### NotFound.meta : <code>object</code>

Route meta

<a name="notfound-meta-pagemeta">
</a>

#### meta.pageMeta ⇒ <code>string</code>

Return translated NotFound view title (translate: `views.NotFound.Index.title`)

<a name="notfound-component">
</a>

### NotFound.component() ⇒ <code>object</code>

Route component. By default, returns `Core.context.vue.components.public.views.notFound.Index`

**Returns**: <code>object</code> - Returns view component

