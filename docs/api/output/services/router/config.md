---
title: Service (router) configuration
headline: Service (router) configuration
sidebarTitle: Configuration
prev: false
next: false
---

# Service (router) configuration

<a name="configuration">
</a>

## Configuration

Service configuration> Refers to `Core.config.services['router']````js'router': {  // Trigger routing on root instance mounted hook  initOnRootInstanceMounted: true,  // The route to redirect in _CatchAll route before hook.  catchAllRedirect: String|Object}```

**Schema**:

- [Configuration](#configuration)
  - [.initOnRootInstanceMounted](#configuration-initonrootinstancemounted) : <code>boolean</code>
  - [.catchAllRedirect](#configuration-catchallredirect) : <code>string</code> \| <code>object</code>

<a name="configuration-initonrootinstancemounted">
</a>

### Core.config.services[&#x27;router&#x27;].initOnRootInstanceMounted : <code>boolean</code>

Define if it auto triggers routing at root instance mounted hook.

<a name="configuration-catchallredirect">
</a>

### Core.config.services[&#x27;router&#x27;].catchAllRedirect : <code>string</code> \| <code>object</code>

The route to redirect in _CatchAll route before hook.

