/**
 * @vuepress
 * ---
 * title: "Service (api-manager)"
 * headline: "Service (api-manager)"
 * sidebarTitle: api-manager
 * initialOpenGroupIndex: -1 # optional, defaults to 0, defines the index of initially opened subgroup
 * prev: ../
 * next: ./options
 * ---
 */

import Service from '@/Service'

import support from './support'
import options from './options'
import API from './API'
import APIManager from './APIManager'
import APIOptions from './APIOptions'

/* eslint-disable jsdoc/require-param, jsdoc/require-returns */

/**
 * @scorpion_ui_service {APIManager} api-manager
 * @description
 * The service "api-manager" is designed to make API managemnent easier.
 *
 * It references any API needed and provides methods and tools to make requests.
 */
export default new Service({
  // Service
  service: {
    // Identifier
    identifier: 'api-manager',
    /**
     * @alias register
     * @description
     * On registering, the service will add to support:
     *
     * - `Core.context.support.errors.RequestError`: see {@link ./support/errors/RequestError RequestError}
     */
    register: function (_Core) {
      // Expose RequestError
      _Core.context.support.errors.RequestError = support.errors.RequestError
    },
    /**
     * @alias create
     * @description
     * Returns an instance of `APImanager` created with options.
     */
    create: (serviceOptions, { APIManager: _APIManager }, _Core) => {
      // Return new APIManager
      return new _APIManager(serviceOptions)
    },
    /**
     * @alias plugin
     * @description
     * The following properties will be set to Vue prototype:
     *
     * ```js
     * Vue.prototype.$APIManager // => Core.service('api-manager')
     * ```
     */
    vuePlugin: ({ APIManager: _APIManager }, _Core) => _APIManager,
    // Vue plugin options
    pluginOptions: {},
    // rootOption
    rootOption: ({ APIManager: _APIManager }, _Core) => _APIManager.rootOption
  },
  /**
   * @alias onInitialized
   * @description
   * Once Core is initialized, 'error-manager' service created, add handler for RequestError
   */
  onInitialized: (Core) => {
    // On 'error-manager' service created, add handler for RequestError
    Core.onServiceCreated('error-manager', (errorManager) => {
      // Ensure that ORIGINAL RequestError class extends contextualized AppError class
      if (!(support.errors.RequestError.prototype instanceof Core.context.support.errors.AppError)) {
        // equivalent to class RequestError extends AppError
        Object.setPrototypeOf(support.errors.RequestError.prototype, Core.context.support.errors.AppError.prototype)
      }
    })
  },
  // Options
  options,
  /**
   * @alias expose
   */
  expose: {
    /**
     * @see {@link ./API API}
     */
    API,
    /**
     * @see {@link ./APIManager APIManager}
     */
    APIManager,
    /**
     * @see {@link ./APIOptions APIOptions}
     */
    APIOptions
  }
})
