---
title: "Service (api-manager): API class"
headline: "Service (api-manager): API class"
sidebarTitle: .API
sidebarDepth: 0
prev: false
next: false
---

# Service (api-manager): API class

<a name="api">
</a>

## API

Service **api-manager**: API class

**Schema**:

- [API](#api)
  - [new API(apiOptions)](#newapinew)
  - _instance_
    - [.toJSON()](#api-tojson) ⇒ <code>object</code>
    - _async methods_
      - [.request(method, url, [options], [beforeCb], [successCb], [errorCb])](#api-request) ⇒ <code>Promise.&lt;\*&gt;</code>
    - _methods_
      - [.setDriver(name, [options])](#api-setdriver) ⇒ <code>void</code>
      - [.mapDriverMethods(methods)](#api-mapdrivermethods) ⇒ <code>void</code>
      - [.initAxios(options)](#api-initaxios) ⇒ <code>void</code>
    - _properties_
      - [.name](#api-name) : <code>string</code>
      - [.options](#api-options) : <code>string</code>
      - [.driver](#api-driver) : <code>object</code>
    - _public methods_
      - [.onBeforeRequest(func)](#api-onbeforerequest) ⇒ <code>void</code>
      - [.onRequestSuccess(func)](#api-onrequestsuccess) ⇒ <code>void</code>
      - [.onRequestError(func)](#api-onrequesterror) ⇒ <code>void</code>
  - _static_
    - _properties_
      - [.supportedDrivers](#api-supporteddrivers) : <code>Array.&lt;string&gt;</code>

<a name="newapinew">
</a>

### new API(apiOptions)

Create a new API

| Param | Type | Description |
| --- | --- | --- |
| apiOptions | <code>module:services/api-manager/APIOptions~APIOptions</code> | An APIOptions object |

<a name="api-tojson">
</a>

### apI.toJSON() ⇒ <code>object</code>

Overrides the default toJSON object method for JSON.stringify() calls

**Returns**: <code>object</code> - - The instance data to be serialized

<a name="api-request">
</a>

### apI.request(method, url, [options], [beforeCb], [successCb], [errorCb]) ⇒ <code>Promise.&lt;\*&gt;</code>

Main request method

**Returns**: <code>Promise.&lt;\*&gt;</code> - The driver Promise response

**Category**: async methods  
| Param | Type | Default | Description |
| --- | --- | --- | --- |
| method | <code>string</code> |  | The method name (ex: GET, POST...) |
| url | <code>string</code> |  | The URL to request, without baseURL (Ex: /users) |
| [options] | <code>object</code> | <code>{}</code> | The specific options for the driver |
| [options.allowConcurrent] | <code>object</code> | <code>false</code> | Send request even if concurrent exists |
| [beforeCb] | <code>function</code> |  | The function to exec before request |
| [successCb] | <code>function</code> |  | The success callback |
| [errorCb] | <code>function</code> |  | The error callback |

<a name="api-setdriver">
</a>

### apI.setDriver(name, [options]) ⇒ <code>void</code>

Set the driver for the API

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The driver name |
| [options] | <code>object</code> | The driver options/configuration |

<a name="api-mapdrivermethods">
</a>

### apI.mapDriverMethods(methods) ⇒ <code>void</code>

Map driver methods for requestsFor each HTTP verb, assign driver method:```js{  GET: methods.GET, // this.request('GET', <url>, [<options>])  POST: methods.POST, // this.request('POST', <url>, [<options>])  PATCH: methods.PATCH, // this.request('PATCH', <url>, [<options>])  PUT: methods.PUT, // this.request('PUT', <url>, [<options>])  DELETE: methods.DELETE, // this.request('DELETE', <url>, [<options>])  OPTIONS: methods.OPTIONS // this.request('OPTIONS', <url>, [<options>])}```

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| methods | <code>object</code> | A methods object |

<a name="api-initaxios">
</a>

### apI.initAxios(options) ⇒ <code>void</code>

Assign an axios instance to this.driverSee [axios documentation](https://axios-http.com/fr/docs/req_config) to see configuration options

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| options | <code>module:axios~AxiosRequestConfig</code> | A AxiosRequestConfig object |

<a name="api-name">
</a>

### apI.name : <code>string</code>

API name

**Category**: properties  
<a name="api-options">
</a>

### apI.options : <code>string</code>

API options

**Category**: properties  
<a name="api-driver">
</a>

### apI.driver : <code>object</code>

Get the API driver

**Category**: properties  
**Read only**: true  
<a name="api-onbeforerequest">
</a>

### apI.onBeforeRequest(func) ⇒ <code>void</code>

Before request hook, see also RequestManager#onBeforeRequest method

**Category**: public methods  
| Param | Type | Description |
| --- | --- | --- |
| func | <code>function</code> \| <code>Array.&lt;function()&gt;</code> | The function(s) to append to the hook queue |

**Example**

```js
api.onBeforeRequest((request) => {  console.log(`Before request!`, request)})
```

<a name="api-onrequestsuccess">
</a>

### apI.onRequestSuccess(func) ⇒ <code>void</code>

On success request hook, see also RequestManager#onRequestSuccess method

**Category**: public methods  
| Param | Type | Description |
| --- | --- | --- |
| func | <code>function</code> \| <code>Array.&lt;function()&gt;</code> | The function(s) to append to the hook queue |

**Example**

```js
api.onRequestSuccess((response, request) => {  console.log(`Request success!`, response)})
```

<a name="api-onrequesterror">
</a>

### apI.onRequestError(func) ⇒ <code>void</code>

On request error hook, see also RequestManager#onRequestError method

**Category**: public methods  
| Param | Type | Description |
| --- | --- | --- |
| func | <code>function</code> \| <code>Array.&lt;function()&gt;</code> | The function(s) to append to the hook queue |

**Example**

```js
api.onRequestError((error, request) => {  console.log(`Request error!`, error)})
```

<a name="api-supporteddrivers">
</a>

### API.supportedDrivers : <code>Array.&lt;string&gt;</code>

Only 'axios' is supported as a driver for the moment

**Category**: properties  
**Read only**: true  
