---
title: xorBy
headline: xorBy
sidebarTitle: xorBy
---

# xorBy

<a name="xorby">
</a>

## xorBy ⇒ <code>Array</code>

Sort array of objects (lodash.xorBy). See https://lodash.com/docs/4.17.15#xorBy

**Returns**: <code>Array</code> - Returns the array difference

| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | Same arguments as lodash.xorBy |

