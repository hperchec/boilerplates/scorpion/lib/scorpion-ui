/**
 * @vuepress
 * ---
 * title: safeAsync
 * headline: safeAsync
 * sidebarTitle: safeAsync
 * ---
 */

/**
 * safeAsync
 * @param {Function} func - The function to execute
 * @returns {Promise<*>} Returns the value returned by `func`, else Error
 * @description
 * Encapsulate in a try/catch block a async function to execute
 * @example
 * safeAsync(function () {
 *   // Fetch API
 * })
 */
export const safeAsync = async function (func) {
  try {
    const result = await func()
    return result
  } catch (err) {
    return err
  }
}

export default safeAsync
