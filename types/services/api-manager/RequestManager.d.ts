export default RequestManager;
declare class RequestManager {
    constructor(apiInstance: API);
    getAPIInstance(): API;
    createRequest(generator: Function, method: string, url: string | URL, options?: object, beforeCb?: Function, successCb?: Function, errorCb?: Function): Request;
    get(key: any): any;
    isWaitingFor(key: any): boolean;
    push(key: string, promise: Promise<any>): Promise<any>;
    remove(key: any): void;
    onBeforeRequest(func: Function | Function[]): void;
    onRequestSuccess(func: Function | Function[]): void;
    onRequestError(func: Function | Function[]): void;
    toJSON(): object;
    #private;
}
import API from './API';
import Request from './Request';
//# sourceMappingURL=RequestManager.d.ts.map