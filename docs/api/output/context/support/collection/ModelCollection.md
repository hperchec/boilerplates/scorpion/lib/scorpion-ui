---
title: Collection class
headline: Collection class
sidebarTitle: Collection class
sidebarDepth: 0
prev: ./
next: false
---

# Collection class

<a name="modelcollection">
</a>

## ModelCollection ⇐ <code>module:context/support/collection/Collection~Collection</code>

> **Ref.** : [Core](../../Core).[support](../).[collection](./).[Collection](./Collection)

**Extends**: <code>module:context/support/collection/Collection~Collection</code>  
**Schema**:

- [ModelCollection](#modelcollection) ⇐ <code>module:context/support/collection/Collection~Collection</code>
  - [new ModelCollection(model, [data], [options])](#newmodelcollectionnew)
  - [.getModel()](#modelcollection-getmodel) ⇒ <code>function</code>
  - [.replaceItem(to)](#modelcollection-replaceitem) ⇒ <code>Promise.&lt;module:context/support/collection/ModelCollection~ModelCollection&gt;</code>
  - [.upsertItem(to)](#modelcollection-upsertitem) ⇒ <code>Promise.&lt;module:context/support/collection/ModelCollection~ModelCollection&gt;</code>
  - _methods_
    - [.findBy(key)](#modelcollection-findby) ⇒ <code>\*</code>
    - [.contains(key)](#modelcollection-contains) ⇒ <code>boolean</code>

<a name="newmodelcollectionnew">
</a>

### new ModelCollection(model, [data], [options])

Create a Collection

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| model | <code>function</code> |  | The model class |
| [data] | <code>Array</code> | <code>[]</code> | (Optional) An array of any types (Default: []) |
| [options] | <code>object</code> |  | (Optional) An options object |

<a name="modelcollection-getmodel">
</a>

### core.context.support.collection.Collection.getModel() ⇒ <code>function</code>

Get the collection model

**Returns**: <code>function</code> - Returns the model class

<a name="modelcollection-replaceitem">
</a>

### core.context.support.collection.Collection.replaceItem(to) ⇒ <code>Promise.&lt;module:context/support/collection/ModelCollection~ModelCollection&gt;</code>

Replace item in collection

**Returns**: <code>Promise.&lt;module:context/support/collection/ModelCollection~ModelCollection&gt;</code> - Returns the collection

| Param | Type | Description |
| --- | --- | --- |
| to | <code>\*</code> | The replacement item |

<a name="modelcollection-upsertitem">
</a>

### core.context.support.collection.Collection.upsertItem(to) ⇒ <code>Promise.&lt;module:context/support/collection/ModelCollection~ModelCollection&gt;</code>

Update or insert item in collection

**Returns**: <code>Promise.&lt;module:context/support/collection/ModelCollection~ModelCollection&gt;</code> - Returns the collection

| Param | Type | Description |
| --- | --- | --- |
| to | <code>\*</code> | The replacement item |

<a name="modelcollection-findby">
</a>

### core.context.support.collection.Collection.findBy(key) ⇒ <code>\*</code>

Find an item by key in the collection

**Returns**: <code>\*</code> - Returns the found collection item

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| key | <code>\*</code> | The key |

<a name="modelcollection-contains">
</a>

### core.context.support.collection.Collection.contains(key) ⇒ <code>boolean</code>

Same as "find" but returns boolean

**Returns**: <code>boolean</code> - Returns true if collection contains the item

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| key | <code>function</code> \| <code>number</code> \| <code>string</code> | Same parameter as "find" method |

