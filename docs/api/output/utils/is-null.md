---
title: isNull
headline: isNull
sidebarTitle: isNull
---

# isNull

<a name="isnull">
</a>

## isNull ⇒ <code>boolean</code>

Check if value is null

**Returns**: <code>boolean</code> - Returns true if value is null

| Param | Type | Description |
| --- | --- | --- |
| value | <code>\*</code> | The variable to check |

**Example**

```js
isNull(value) // => true is value is null
```

