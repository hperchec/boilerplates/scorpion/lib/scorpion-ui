export class XArray extends Array<any> {
    constructor(...args: any[]);
    insert(value: any, index?: number): void;
}
export default XArray;
//# sourceMappingURL=x-array.d.ts.map