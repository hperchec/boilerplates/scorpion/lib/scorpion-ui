/**
 * @vuepress
 * ---
 * title: isObject
 * headline: isObject
 * sidebarTitle: isObject
 * ---
 */

/**
 * isObject
 * @param {*} obj - The value to check
 * @returns {boolean} Returns true if value is object type
 * @description
 * Quick object check - this is primarily used to tell
 * Objects from primitive values when we know the value
 * is a JSON-compliant type.
 * @example
 * isObject({ a: 1, b: 2 }) // => true
 * isObject(new Date()) // => true
 * isObject('Hello') // => false
 */
export const isObject = function (obj) {
  return obj !== null && typeof obj === 'object'
}

export default isObject
