export default VariantableOptions;
/**
 * @classdesc
 * VariantableOptions class for variantable mixin
 */
declare class VariantableOptions {
    /**
     * Static methods
     */
    /**
     * Returns the variant names as array
     * @param {VariantableOptions} options - Options
     * @returns {string[]} The variant names as array
     */
    static getVariantsFromOptions(options: VariantableOptions): string[];
    /**
     * Get css classes as string
     * @param {VariantableOptions} options - Options
     * @param {string} [element = undefined] - The element name
     * @param {string} [variant = undefined] - The variant. If not specified, returns default classes
     * @returns {string} The classes as string
     */
    static getElementCssClassFromOptions(options: VariantableOptions, element?: string, variant?: string): string;
    /**
     * Merge two instance of VariantableOptions
     * @param {VariantableOptions} a - From value
     * @param {VariantableOptions} b - To value
     * @returns {VariantableOptions} A new instance of VariantableOptions
     */
    static merge(a: VariantableOptions, b: VariantableOptions): VariantableOptions;
    /**
     * Extend an instance of VariantableOptions
     * @param {VariantableOptions} varOptions - The variantable options to extend
     * @param {Function} processor - A processor function
     * @returns {VariantableOptions} A new instance of VariantableOptions
     */
    static extend(varOptions: VariantableOptions, processor: Function): VariantableOptions;
    /**
     * Creates new instance if obj are valid
     * @param {object} obj - Options
     * @param {string|object} obj.fixedClasses - Fixed classes
     * @param {string|object} obj.classes - Default classes
     * @param {object} [obj.variants = {}] - Variants
     * @param {object} [options = {}] - Options
     * @param {string} [options.cssClassesFormat = object] - The format for css classes: can be 'string', 'array' or 'object'
     */
    constructor(obj: {
        fixedClasses: string | object;
        classes: string | object;
        variants?: object;
    }, options?: {
        cssClassesFormat?: string;
    });
    /**
     * Public properties
     */
    /**
     * fixedClasses
     * @category properties
     * @description Fixed classes
     * @type {?object}
     */
    fixedClasses: object | null;
    /**
     * classes
     * @category properties
     * @description Classes
     * @type {?object}
     */
    classes: object | null;
    /**
     * variants
     * @category properties
     * @description Variants
     * @type {?object}
     */
    variants: object | null;
    /**
     * variantExtensions
     * @category properties
     * @description Variant extensions
     * @type {?object}
     */
    variantExtensions: object | null;
    /**
     * Public methods
     */
    /**
     * Process variant extensions and injects it into this.variants
     * @param {string} [format = object] - The optional format default object
     */
    syncVariantExtensions(format?: string): void;
    /**
     * Returns array of variant names
     * @returns {string[]} A list of variants
     */
    getVariants(): string[];
    /**
     * Returns true if variant exists
     * @param {string} variant - The expected variant
     * @returns {boolean} True if variant exists
     */
    hasVariant(variant: string): boolean;
    /**
     * Returns true if has at least one element key set
     * @returns {string[]} The element keys
     */
    getElementKeys(): string[];
    /**
     * Returns true if has at least one element key set
     * @returns {boolean} True if has at least one element key set
     */
    hasNestedElements(): boolean;
    /**
     * Returns css classes
     * @param {string} [element = undefined] - The element name
     * @param {string} [variant = undefined] - The variant. If not specified, returns default classes
     * @returns {CCSClasses} An instance of CSSClasses
     */
    getElementCssClass(element?: string, variant?: string): any;
    #private;
}
//# sourceMappingURL=VariantableOptions.d.ts.map