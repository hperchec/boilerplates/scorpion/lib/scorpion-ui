/**
 * @vuepress
 * ---
 * title: ServiceDefinition class
 * headline: ServiceDefinition class
 * sidebarTitle: ServiceDefinition
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: ./Service
 * next: false # Disable prev link
 * ---
 */

import { isDef, isUndef, typeCheck } from '@/utils'

const log = (...args) => { // eslint-disable-line no-unused-vars
  console.log(`[ServiceDefinition class] - ${args.shift()}`, ...args)
}
const error = (msg) => {
  throw new Error(`[ServiceDefinition class] - ${msg}`)
}

/**
 * @classdesc
 * ServiceDefinition class
 */
export class ServiceDefinition {
  /**
   * Create a ServiceDefinition instance
   * @param {object} def - The service definition object
   * @param {string} def.identifier - The service identifier
   * @param {Function} def.create - The function that returns the service instance / object.
   * @param {Function} [def.register] - The function to call when registering the service.
   * @param {Function} [def.vuePlugin] - Returns the service Vue plugin object
   * @param {*} [def.pluginOptions] - If `vuePlugin` is defined, will be passed as Vue plugin options
   * @param {Function|string} [def.rootOption] - Returns rootOption string. If defined, the service will be passed to Vue root instance options
   */
  constructor (def) {
    if (isUndef(def)) error('Constructor requires object as first argument')
    for (const prop of Object.keys(def)) {
      this[prop] = def[prop]
    }
  }

  /**
   * Accessors & mutators
   */

  /**
   * identifier
   * @category properties
   * @type {string}
   * @description
   * Service identifier
   */
  get identifier () {
    return this.#identifier
  }

  set identifier (value) {
    if (isUndef(value)) error('identifier is undefined')
    typeCheck(String, value)
      ? this.#identifier = value
      : error(`identifier must be string, "${typeof value}" received`)
  }

  /**
   * create
   * @category properties
   * @type {Function}
   * @description
   * Function that creates the service
   */
  get create () {
    return this.#create
  }

  set create (value) {
    if (isUndef(value)) error('create is undefined')
    typeCheck(Function, value)
      ? this.#create = value
      : error(`create must be a function, "${typeof value}" received`)
  }

  /**
   * register
   * @category properties
   * @type {Function}
   * @description
   * Function to call when registering the service
   */
  get register () {
    return this.#register
  }

  set register (value) {
    if (isDef(value)) {
      typeCheck(Function, value)
        ? this.#register = value
        : error(`register must be a function, "${typeof value}" received`)
    }
  }

  /**
   * vuePlugin
   * @category properties
   * @type {Function}
   * @description
   * A function that returns vue plugin
   */
  get vuePlugin () {
    return this.#vuePlugin
  }

  set vuePlugin (value) {
    if (isDef(value)) {
      typeCheck(Function, value)
        ? this.#vuePlugin = value
        : error(`vuePlugin must be a function, "${typeof value}" received`)
    }
  }

  /**
   * pluginOptions
   * @category properties
   * @type {*}
   * @description
   * The Vue plugin options
   */
  get pluginOptions () {
    return this.#pluginOptions
  }

  set pluginOptions (value) {
    this.#pluginOptions = value
  }

  /**
   * rootOption
   * @category properties
   * @type {Function|string}
   * @description
   * The Vue root instance option property name
   */
  get rootOption () {
    return this.#rootOption
  }

  set rootOption (value) {
    if (isDef(value)) {
      (typeCheck(Function, value) || typeCheck(String, value))
        ? this.#rootOption = value
        : error(`rootOption must be a function or string, "${typeof value}" received`)
    }
  }

  /**
   * Methods
   */

  // ...

  /**
   * Static accessors & mutators
   */

  // ...

  /**
   * Static methods
   */

  // ...

  /**
   * Private properties
   */

  /**
   * identifier
   * @type {string}
   * @default undefined
   */
  #identifier = undefined

  /**
   * create
   * @type {Function}
   * @default undefined
   */
  #create = undefined

  /**
   * register
   * @type {Function}
   * @description
   * Default is a function that does nothing...
   */
  #register = function () {}

  /**
   * vuePlugin
   * @type {Function}
   * @default undefined
   */
  #vuePlugin = undefined

  /**
   * pluginOptions
   * @type {object}
   * @default undefined
   */
  #pluginOptions = undefined

  /**
   * rootOption
   * @type {Function|string}
   * @default undefined
   */
  #rootOption = undefined
}

// Export ServiceDefinition class
export default ServiceDefinition
