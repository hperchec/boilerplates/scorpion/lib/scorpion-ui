---
title: "Service (device-manager): Device class"
headline: "Service (device-manager): Device class"
sidebarTitle: .Device
sidebarDepth: 0
prev: false
next: false
---

# Service (device-manager): Device class

<a name="device">
</a>

## Device

Service **device-manager**: Device class

**Schema**:

- [Device](#device)
  - [new Device(options)](#newdevicenew)
  - [.toJSON()](#device-tojson) ⇒ <code>object</code>

<a name="newdevicenew">
</a>

### new Device(options)

Create a new Device

| Param | Type | Description |
| --- | --- | --- |
| options | <code>object</code> | The device options |

<a name="device-tojson">
</a>

### device.toJSON() ⇒ <code>object</code>

Overrides the default toJSON object method for JSON.stringify() calls

**Returns**: <code>object</code> - - The instance data to be serialized

