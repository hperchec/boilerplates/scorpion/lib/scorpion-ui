---
title: Service (websocket-manager)
headline: Service (websocket-manager)
sidebarTitle: websocket-manager
initialOpenGroupIndex: -1
prev: ../
next: ./options
---

# Service (websocket-manager)

<a name="service"></a>

The service "websocket-manager" is designed to make websocket connections managemnent easier.It references any connection needed and provides methods and tools.

## Create

Returns an instance of `WebSocketManager` created with options.

**Returns**: <code>module:services/websocket-manager/WebSocketManager~WebSocketManager</code> - Once services are created:

```js
Core.service('websocket-manager')
```

## Vue

### Plugin

On registering, service Vue plugin will be set in `Core.context.vue.plugins['websocket-manager']`.

Plugin will be automatically used by Vue.

The following properties will be set to Vue prototype:```js// Each references to Core.service('websocket-manager')Vue.prototype.$webSocketManagerVue.prototype.$wsManager```

### Root instance

<a name="app"></a>

#### Prototype $app property

On registering, the service will inject properties to \`Core.context.vue.root.prototype.app\`.

> Properties will be shared by all components under \`vm.$app\` where \`vm\` is the component instance.

<a name="app-usersettings"></a>

##### User settings

Service will define the following user settings keys:

##### Others

## Expose (service context)

The service will expose the following properties under `Core.context.services['websocket-manager']`:
<a name="expose-wsconnection">
</a>

### .WSConnection

> Exposed as `Core.context.services['websocket-manager'].WSConnection`

**See**: [WSConnection](./WSConnection)

<a name="expose-websocketmanager">
</a>

### .WebSocketManager

> Exposed as `Core.context.services['websocket-manager'].WebSocketManager`

**See**: [WebSocketManager](./WebSocketManager)

<a name="expose-wsconnectionoptions">
</a>

### .WSConnectionOptions

> Exposed as `Core.context.services['websocket-manager'].WSConnectionOptions`

**See**: [WSConnectionOptions](./WSConnectionOptions)

