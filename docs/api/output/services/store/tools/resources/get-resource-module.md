---
title: get-resource-module
headline: get-resource-module
---

# get-resource-module

<a name="getresourcemodule">
</a>

## getResourceModule ⇒ <code>object</code>

Get a resource module by name

**Returns**: <code>object</code> - The module object

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| store | <code>module:vuex~Store</code> |  | The store instance |
| moduleName | <code>string</code> |  | The resource module name (ex: "Users") |
| [rootModule] | <code>string</code> | <code>&quot;\&quot;Resources\&quot;&quot;</code> | The root resource module ( Default: "Resources") |

