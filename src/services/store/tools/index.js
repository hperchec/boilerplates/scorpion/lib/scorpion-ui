import getModule from './get-module'
import getModuleContext from './get-module-context'
import resources from './resources'

export default {
  getModule,
  getModuleContext,
  resources
}
