/**
 * @vuepress
 * ---
 * title: "Service (router) options"
 * headline: "Service (router) options"
 * sidebarTitle: Options
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import Core from '@/Core'
import { isUndef, XArray } from '@/utils'
import toolbox from '@/toolbox'

const log = (str, options = {}) => Core.service('logger').consoleLog(str, { type: 'router', ...options }) // eslint-disable-line dot-notation

const { makeConfigurableOption } = toolbox.services

/**
 * Cache for dynamic properties
 * @ignore
 */
const _options = {
  routes: undefined
}

/**
 * @name options
 * @static
 * @description
 * Accepts all VueRouter options plus the following:
 *
 * - `defaultOnComplete`
 * - `defaultOnAbort`
 * - `beforeEach`
 * - `beforeResolve`
 * - `afterEach`
 * - `initOnRootInstanceMounted`
 * - `catchAllRedirect`
 *
 * By default, each **private** and **public** route are auto-injected as `routes` option.
 * Each object in `Core.context.services['router'].routes.private` and
 * `Core.context.services['router'].routes.public` will be passed respectively to `PrivateRoute` and `PublicRoute` constructor.
 * The result will be an array of the defined routes. You can overwrite it by setting this option.
 *
 * Configurable options:
 *
 * - **initOnRootInstanceMounted**: `Core.config.services['router'].initOnRootInstanceMounted`
 * - **catchAllRedirect**: `Core.config.services['router'].catchAllRedirect`
 *
 * ::: tip SEE ALSO
 * [VueRouter construction options documentation](https://v3.router.vuejs.org/api/#router-construction-options)
 * :::
 */
const options = {
  /**
   * routes (VueRouter option)
   * @ignore
   */
  get routes () {
    /* eslint-disable dot-notation */
    // If routes option is not provided
    if (isUndef(_options.routes)) {
      // Get private & public routes
      const privateRoutes = Object.values(Core.context.services['router'].routes.private).map((route) => {
        return new Core.context.services['router'].PrivateRoute(route.path, route.name, route.component, route.options)
      })
      const publicRoutes = Object.values(Core.context.services['router'].routes.public).map((route) => {
        return new Core.context.services['router'].PublicRoute(route.path, route.name, route.component, route.options)
      })
      // Force _CatchAll to be last route
      const catchAllRoute = publicRoutes.find((value) => value.name === '_CatchAll')
      return [
        ...privateRoutes,
        ...(publicRoutes.filter((value) => value.name !== '_CatchAll').concat(catchAllRoute))
      ]
    } else {
      return _options.routes
    }
  },
  set routes (value) {
    _options.routes = value
  },
  /**
   * scrollBehavior (VueRouter option)
   * @ignore
   */
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return {
        x: 0,
        y: 0,
        behavior: 'smooth'
      }
    }
  },
  /**
   * defaultOnComplete
   * @alias options.defaultOnComplete
   * @type {Function}
   * @description
   * The default onComplete callback to pass to router `push` method.
   */
  defaultOnComplete: undefined,
  /**
   * defaultOnAbort
   * @alias options.defaultOnAbort
   * @type {Function}
   * @param {Error} error - The error instance
   * @description
   * The default onAbort callback to pass to router `push` method.
   */
  defaultOnAbort: (error) => {
    // Prevent error of type "NavigationDuplicated"
    if (error.name === 'NavigationDuplicated') {
      log(`NavigationDuplicated warning ⚠: ${error.message}`)
    } else {
      console.error(error)
    }
  },
  /**
   * beforeEach
   * @alias options.beforeEach
   * @type {Function[]}
   * @description
   * Global before hook. Each function will be injected during router service creation
   * via `router.beforeEach()` before returning new router instance.
   *
   * ::: tip SEE ALSO
   * [VueRouter instance beforeEach documentation](https://v3.router.vuejs.org/api/#router-beforeeach)
   * :::
   *
   * By default, it is a empty `XArray` (see utils documentation). Each middleware in `Core.context.services['router'].middlewares`
   * will be injected at service creation.
   * The result will be a concatenated array of the defined middlewares and the `beforeEach` option array content.
   */
  beforeEach: new XArray([]),
  /**
   * beforeResolve
   * @alias options.beforeResolve
   * @type {Function[]}
   * @description
   * Global before resolve hook. Each function will be injected during router service creation
   * via `router.beforeResolve()` before returning new router instance.
   *
   * ::: tip SEE ALSO
   * [VueRouter instance beforeResolve documentation](https://v3.router.vuejs.org/api/#router-beforeresolve)
   * :::
   *
   * By default, it is an empty `XArray`
   */
  beforeResolve: new XArray([]),
  /**
   * afterEach
   * @alias options.afterEach
   * @type {Function[]}
   * @description
   * Global after hook. Each function will be injected during router service creation
   * via `router.afterEach()` before returning new router instance.
   *
   * ::: tip SEE ALSO
   * [VueRouter instance afterEach documentation](https://v3.router.vuejs.org/api/#router-aftereach)
   * :::
   *
   * By default, it is an empty `XArray`
   */
  afterEach: new XArray([])
}

/**
 * initOnRootInstanceMounted
 * @name options.initOnRootInstanceMounted
 * @type {boolean}
 * @default true
 * @description
 * Define if it auto triggers routing at root instance mounted hook.
 *
 * Can be overwritten via Core global configuration: `Core.config.services['router'].initOnRootInstanceMounted`
 */
makeConfigurableOption(options, {
  propertyName: 'initOnRootInstanceMounted',
  serviceIdentifier: 'router',
  configPath: 'initOnRootInstanceMounted',
  defaultValue: true
})

/**
 * catchAllRedirect
 * @name options.catchAllRedirect
 * @type {string|object}
 * @default /404
 * @description
 * The path / route object to redirect in _CatchAll before hook.
 *
 * Can be overwritten via Core global configuration: `Core.config.services['router'].catchAllRedirect`
 */
makeConfigurableOption(options, {
  propertyName: 'catchAllRedirect',
  serviceIdentifier: 'router',
  configPath: 'catchAllRedirect',
  defaultValue: '/404'
})

export default options
