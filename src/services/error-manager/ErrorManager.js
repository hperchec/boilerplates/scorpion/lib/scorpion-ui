/**
 * @vuepress
 * ---
 * title: "Service (error-manager): ErrorManager class"
 * headline: "Service (error-manager): ErrorManager class"
 * sidebarTitle: .ErrorManager
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import Core from '@/Core'
import AppError from '@/context/support/errors/AppError'
import { getAllKeys, isDef, typeCheck } from '@/utils'
import toolbox from '@/toolbox'

const { VuePluginable } = toolbox.vue

/**
 * @extends VuePluginable
 * @classdesc
 * Service **error-manager**: ErrorManager class
 */
class ErrorManager extends VuePluginable {
  /**
   * Create a new ErrorManager
   * @param {object} options - Constructor options
   */
  constructor (options) {
    // Call VuePluginable constructor
    super()
    // Set debugging private property
    if (this.__v_descriptor__) {
      this.__v_descriptor__.namespace = 'service:error-manager'
      this.__v_descriptor__.description = 'Access the ErrorManager instance'
      // @ts-ignore
      this.__v_descriptor__.externalLink = __getDocUrl__('/api/services/error-manager/ErrorManager')
    }
    // Init scope queues
    for (const scope of this.scopes) {
      this.#scopedQueues[scope] = [] // empty array by default
    }
    Object.freeze(this.#scopedQueues)
    this.initGlobalHandler()
    this.initDevHandler()
    this.initVueHandler()
    // Auto register all error classes/constructors under `Core.context.support.errors`
    if (options.autoRegister) {
      Core.onAppBoot(() => {
        for (const property in Core.context.support.errors) {
          const constructor = Core.context.support.errors[property]
          this.catch(constructor)
        }
      })
    }
  }

  /**
   * Overrides static VuePluginable properties
   */
  static reactive = true
  static rootOption = 'errorManager'
  static aliases = []

  /**
   * Accessors & mutators
   */

  /**
   * The scopes
   * @category properties
   * @readonly
   * @type {string[]}
   */
  get scopes () {
    return [
      'global', // for window level attached event listeners
      'vue', // for vue errorHandler
      'dev' // for webpack-dev-server runtime errorHandler
    ]
  }

  /**
   * Methods
   */

  /**
   * @category methods
   * @param {typeof AppError} errorConstructor - The error constructor that extends AppError
   * @returns {boolean} Return true if the constructor has been added to custom errors to catch
   * @description
   * Catch a custom error at global level
   */
  catch (errorConstructor) {
    // If the given class is not extending AppError class
    if (errorConstructor !== Core.context.support.errors.AppError && !(errorConstructor.prototype instanceof Core.context.support.errors.AppError)) {
      throw new Error('ErrorManager error: the given constructor is not extending AppError class')
    }
    if (this.#customErrorsToCatch.has(errorConstructor)) {
      console.warn('ErrorManager: handler already defined for given constructor:' + errorConstructor.name)
      return false
    }
    this.#customErrorsToCatch.add(errorConstructor)
    return true
  }

  /**
   * @param {AppError} error - The AppError to throw "silently". It will not block the current execution loop
   * @param {boolean} [force = false] - If false, error is not thrown again is isCaught returns true. If true, error will be thrown in all cases
   * @returns {AppError} The error instance itself
   * @description
   * Throw an AppError instance "silently"
   */
  throw (error, force = false) {
    // @ts-ignore
    if (!(error instanceof AppError || error instanceof Core.context.support.errors.AppError)) {
      throw new Error('ErrorManager: throw method expects instance of AppError as first parameter')
    } else {
      // @ts-ignore
      if (force || !error.isCaught()) {
        // Throw error with setTimeout to trigger event listeners
        // The trick with setTimeout is that executes callback in another JavaScript event loop
        // So, the user can listen for request specific error
        setTimeout(() => {
          throw error
        })
        // @ts-ignore
        error._isCaught = true
      }
    }
    return error
  }

  /**
   * @category methods
   * @param {string} scope - The scope to target
   * @param {Function} handler - The handler to add
   * @returns {Function} Return the handler itself
   * @description
   * Add an error handler to the given scope
   */
  addHandler (scope, handler) {
    if (!this.scopes.includes(scope)) {
      throw new Error('ErrorManager error: unknown scope "' + scope + '". Must be one of: ' + this.scopes.join(', '))
    }
    if (!typeCheck(Function, handler)) {
      throw new Error('ErrorManager error: handler must be function, ' + typeof handler + ' received')
    }
    return this.#addHandler(scope, handler)
  }

  /**
   * @category methods
   * @param {string} scope - The scope to target
   * @param {Function} handler - The handler to add
   * @returns {boolean} True if listener was defined and removed, otherwise false
   * @description
   * Remove an error handler from the given scope
   */
  removeHandler (scope, handler) {
    if (!this.scopes.includes(scope)) {
      throw new Error('ErrorManager error: unknown scope "' + scope + '". Must be one of: ' + this.scopes.join(', '))
    }
    if (!typeCheck(Function, handler)) {
      throw new Error('ErrorManager error: handler must be function, ' + typeof handler + ' received')
    }
    return this.#removeHandler(scope, handler)
  }

  /**
   * Init the global handler to global (window level) object
   * @returns {void}
   */
  initGlobalHandler () {
    window.addEventListener('error', (event) => {
      // Initial queue payload
      const queuePayload = {
        error: event.error,
        event: event
      }
      // Iterator function to pass to this.#runQueue method
      const iterator = (payload, current, next) => {
        // Received payload can be array or boolean
        // next(false)
        if (payload === false) {
          return false
        } else {
          return current(payload, (_payload) => {
            // next(true) or next()
            if (_payload === true || _payload === undefined) {
              // pass the previous value of payload
              _payload = payload
            }
            return next(_payload)
          })
        }
      }

      const ranQueueResult = this.#runQueue(this.#scopedQueues.global, iterator, queuePayload, (payload) => {})

      // In case of functions returns any defined value (true or false), we stop the chain
      // returning any value other than undefined will stop the event propagation
      if (isDef(ranQueueResult)) {
        event.stopImmediatePropagation()
      }

      return ranQueueResult
    }, true)

    // Add listener for custom errors to catch
    this.addHandler('global', (payload, next) => {
      // Iterator function to pass to this.#runQueue method
      const iterator = (payload, current, next) => {
        // Received payload can be array or boolean
        // next(false)
        if (payload === false) {
          return false
        } else {
          return current(payload, (_payload) => {
            // next(true) or next()
            if (_payload === true || _payload === undefined) {
              // pass the previous value of payload
              _payload = payload
            }
            return next(_payload)
          })
        }
      }

      let isThereAnErrorCaught = false

      const queue = [ ...this.#customErrorsToCatch ].reduce((accumulator, errorConstructor) => {
        accumulator.push((payload, next) => {
          if (payload.error instanceof errorConstructor) {
            // mark as caught
            payload.error._isCaught = isThereAnErrorCaught = true
            // call the 'caught' method
            // @ts-ignore
            errorConstructor.caught(payload.error)
          }
          return next(payload)
        })
        return accumulator
      }, [])

      const ranQueueResult = this.#runQueue(queue, iterator, payload, (payload) => {
        return !isThereAnErrorCaught
      })

      return ranQueueResult
    })
  }

  /**
   * Init the global handler to vue config object
   * @returns {void}
   */
  initVueHandler () {
    // Function from Vue source code
    const defaultErrorHandler = function (err, vm, info) {
      if (process.env.NODE_ENV === 'development') {
        Core.Vue.util.warn(`Error in ${info}: "${err.toString()}"`, vm)
      }
      console.error(err)
    }
    // Global error handler
    Core.Vue.config.errorHandler = (err, vm, info) => {
      let ranQueueResult
      // Next custom handler
      try {
        // Initial queue payload
        const queuePayload = {
          err: err,
          vm: vm,
          info: info
        }
        // Iterator function to pass to this.#runQueue method
        const iterator = (payload, current, next) => {
          // Received payload can be array or boolean
          // next(false)
          if (payload === false) {
            return false
          } else {
            return current(payload, (_payload) => {
              // next(true) or next()
              if (_payload === true || _payload === undefined) {
                // pass the previous value of payload
                _payload = payload
              }
              return next(_payload)
            })
          }
        }
        ranQueueResult = this.#runQueue(this.#scopedQueues.vue, iterator, queuePayload, (payload) => {})
      } catch (e) {
        // if the user intentionally throws the original error in the handler,
        // do not log it twice
        if (e !== err) {
          defaultErrorHandler(err, vm, info)
        }
      }
      // Custom handler must return false to interupt
      if (ranQueueResult !== false) {
        // Call default
        defaultErrorHandler(err, vm, info)
      }
    }
  }

  /**
   * Init the global handler to toolbox.dev.runtime.errorHandler
   * See https://webpack.js.org/configuration/dev-server/#overlay
   * @returns {void}
   */
  initDevHandler () {
    const defaultErrorHandler = toolbox.development.runtime.errorHandler
    toolbox.development.runtime.errorHandler = (error) => {
      defaultErrorHandler(error)
      // Initial queue payload
      const queuePayload = {
        error: error
      }
      // Iterator function to pass to this.#runQueue method
      const iterator = (payload, current, next) => {
        // Received payload can be array or boolean
        // next(false)
        if (payload === false) {
          return false
        } else {
          return current(payload, (_payload) => {
            // next(true) or next()
            if (_payload === true || _payload === undefined) {
              // pass the previous value of payload
              _payload = payload
            }
            return next(_payload)
          })
        }
      }

      const ranQueueResult = this.#runQueue(this.#scopedQueues.dev, iterator, queuePayload, (payload) => {})

      // for webpack-dev-server client overlay runtimeErrors callback, returning true will trigger the default behavior
      if (ranQueueResult === true || ranQueueResult === undefined) {
        return true
      } else {
        return Boolean(ranQueueResult)
      }
    }
  }

  /**
   * Overrides the default toJSON object method for JSON.stringify() calls
   * @returns {object} - The instance data to be serialized
   */
  toJSON () {
    const keys = getAllKeys(this, {
      excludeKeys: [
        /^_/, // private properties that starts with '_'
        'toJSON',
        'toString',
        'constructor'
      ]
    })
    const jsonObj = keys.reduce((obj, key) => {
      obj[key] = this[key]
      return obj
    }, {})
    return jsonObj
  }

  /**
   * Private properties
   */

  /**
   * Save locally the listener queue for each scope
   * @type {object}
   */
  #scopedQueues = {}

  /**
   * Save locally the custom error constructors
   */
  #customErrorsToCatch = new Set()

  /**
   * Private methods
   */

  /**
   * Get a listener from the target scope queue
   * @param {string} scope - The target scope
   * @param {Function} handler - The error handler
   * @returns {object} Returns and object with the found listener and the index in scoped queue
   */
  #getHandler (scope, handler) {
    const listenerIndex = this.#scopedQueues[scope].findIndex((value) => value === handler)
    return {
      index: listenerIndex,
      listener: this.#scopedQueues[scope][listenerIndex]
    }
  }

  /**
   * Add a listener to the target scope queue
   * @param {string} scope - The target scope
   * @param {Function} handler - The error handler
   * @returns {Function} Returns the handler itself
   */
  #addHandler (scope, handler) {
    this.#scopedQueues[scope].push(handler)
    return handler
  }

  /**
   * Remove a listener from the target scope queue
   * @param {string} scope - The target scope
   * @param {Function} handler - The error handler
   * @returns {boolean} Returns true if defined and removed, otherwise false
   */
  #removeHandler (scope, handler) {
    const { index } = this.#getHandler(scope, handler)
    if (index > -1) {
      this.#scopedQueues[scope].splice(index, 1)
      return true
    } else {
      return false
    }
  }

  /**
   * Remove a listener from the target scope queue
   * @param {Function[]} queue - The function queue
   * @param {Function} iteratorFunc - The iterator function
   * @param {object} [iteratorPayload = {}] - The initial iterator payload
   * @param {Function} [finalCallback] - The final callback to execute (takes payload as unique parameter)
   * @returns {boolean} Returns true if defined and removed, otherwise false
   */
  #runQueue (queue, iteratorFunc, iteratorPayload = {}, finalCallback) {
    // save the returned value of iteratorFunc
    let returnedValue
    // walk on queue array
    const step = (i, payload) => {
      // If last, exec final callback
      if (i >= queue.length) {
        if (!isDef(finalCallback)) finalCallback = (_payload, _returnedValue) => _returnedValue
        return finalCallback(payload, returnedValue)
      } else {
        if (queue[i]) {
          const iteratorFuncResult = iteratorFunc(payload, queue[i], (_payload) => {
            const nextResult = step(i + 1, _payload)
            if (isDef(nextResult)) returnedValue = nextResult
            return nextResult
          })
          // in case of iteratorFuncResult is defined but returnedValue is not, set it
          if (!isDef(returnedValue) && isDef(iteratorFuncResult)) {
            returnedValue = iteratorFuncResult
          }
          return isDef(returnedValue) ? returnedValue : iteratorFuncResult
        } else {
          // in case of queued function is undefined
          return step(i + 1, payload)
        }
      }
    }
    return step(0, iteratorPayload)
  }

  /**
   * Static
   */

  /**
   * Install method for Vue
   * @param {Vue} _Vue - Vue
   * @param {object} [options = {}] - The options of the plugin
   */
  static install (_Vue, options = {}) {
    const Vue = _Vue
    super.install(Vue)
  }
}

export default ErrorManager
