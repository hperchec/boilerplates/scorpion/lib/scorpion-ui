---
title: Service (i18n) options
headline: Service (i18n) options
sidebarTitle: .options
sidebarDepth: 0
prev: false
next: false
---

# Service (i18n) options

## Constants

<dl>
<dt><a href="#_options">_options</a></dt>
<dd><p>Cache for dynamic properties</p>
</dd>
</dl>

<a name="options">
</a>

## \_options

Cache for dynamic properties

<a name="options">
</a>

## .options

Accepts all [VueI18n](https://kazupon.github.io/vue-i18n/api/#constructor-options) options plus the following:- `availableLocales`By default, `locale` and `fallbackLocale` options are set to `'en'`.The `messages` option contains messages defined in `Core.context.services['i18n'].messages` by default.You can overwrite it by setting this option.Configurable options:- `availableLocales`: `Core.config.services['i18n'].availableLocales`- `locale`: `Core.config.services['i18n'].defaultLocale`- `fallbackLocale`: `Core.config.services['i18n'].fallbackLocale`::: tip SEE ALSO[VueI18n construction options](https://kazupon.github.io/vue-i18n/api/#constructor-options) documentation:::

