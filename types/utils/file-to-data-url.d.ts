export function fileToDataUrl(file: File): Promise<string | ArrayBuffer>;
export default fileToDataUrl;
//# sourceMappingURL=file-to-data-url.d.ts.map