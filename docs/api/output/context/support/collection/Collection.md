---
title: Collection class
headline: Collection class
sidebarTitle: Collection class
sidebarDepth: 0
prev: ./
next: false
---

# Collection class

<a name="collection">
</a>

## Collection

> **Ref.** : [Core](../../Core).[support](../).[collection](./).[Collection](./Collection)

**Schema**:

- [Collection](#collection)
  - [new Collection([data], [options])](#newcollectionnew)
  - _async methods_
    - [.pushItem(item)](#collection-pushitem) ⇒ <code>Promise.&lt;module:context/support/collection/Collection~Collection&gt;</code>
    - [.add(...args)](#collection-add) ⇒ <code>Promise.&lt;module:context/support/collection/Collection~Collection&gt;</code>
    - [.push(...args)](#collection-push) ⇒ <code>Promise.&lt;module:context/support/collection/Collection~Collection&gt;</code>
    - [.deleteItem(key)](#collection-deleteitem) ⇒ <code>Promise.&lt;module:context/support/collection/Collection~Collection&gt;</code>
    - [.delete(...args)](#collection-delete) ⇒ <code>Promise.&lt;module:context/support/collection/Collection~Collection&gt;</code>
    - [.replaceItem(key, to)](#collection-replaceitem) ⇒ <code>Promise.&lt;module:context/support/collection/Collection~Collection&gt;</code>
    - [.replace(...args)](#collection-replace) ⇒ <code>Promise.&lt;module:context/support/collection/Collection~Collection&gt;</code>
    - [.touch(...args)](#collection-touch) ⇒ <code>Promise.&lt;module:context/support/collection/Collection~Collection&gt;</code>
    - [.update(...args)](#collection-update) ⇒ <code>Promise.&lt;module:context/support/collection/Collection~Collection&gt;</code>
    - [.upsertItem(key, to)](#collection-upsertitem) ⇒ <code>Promise.&lt;module:context/support/collection/Collection~Collection&gt;</code>
    - [.upsert(...args)](#collection-upsert) ⇒ <code>Promise.&lt;module:context/support/collection/Collection~Collection&gt;</code>
  - _methods_
    - [.lock()](#collection-lock) ⇒ <code>module:context/support/collection/Collection~Collection</code>
    - [.freeze()](#collection-freeze) ⇒ <code>module:context/support/collection/Collection~Collection</code>
    - [.unlock()](#collection-unlock) ⇒ <code>module:context/support/collection/Collection~Collection</code>
    - [.unfreeze()](#collection-unfreeze) ⇒ <code>module:context/support/collection/Collection~Collection</code>
    - [.isLocked()](#collection-islocked) ⇒ <code>boolean</code>
    - [.abortIfLocked()](#collection-abortiflocked) ⇒ <code>void</code>
    - [.findBy(key, array)](#collection-findby) ⇒ <code>\*</code>
    - [.setItems(items)](#collection-setitems) ⇒ <code>void</code>
    - [.get()](#collection-get) ⇒ <code>Array</code>
    - [.all()](#collection-all) ⇒ <code>Array</code>
    - [.find(key)](#collection-find) ⇒ <code>\*</code>
    - [.findWithIndex(key)](#collection-findwithindex) ⇒ <code>\*</code>
    - [.filter(callbackFn, [thisArg])](#collection-filter) ⇒ <code>Array</code>
    - [.findMany(...args)](#collection-findmany) ⇒ <code>Promise.&lt;module:context/support/collection/Collection~Collection&gt;</code>
    - [.map(...args)](#collection-map) ⇒ <code>Array</code>
    - [.reduce(...args)](#collection-reduce) ⇒ <code>Array</code> \| <code>object</code>
    - [.maxBy(sortFct)](#collection-maxby) ⇒ <code>\*</code>
    - [.minBy(sortFct)](#collection-minby) ⇒ <code>\*</code>
    - [.orderBy(...args)](#collection-orderby) ⇒ <code>\*</code>
    - [.sortBy(sortFct)](#collection-sortby) ⇒ <code>\*</code>
    - [.onBeforePush(func)](#collection-onbeforepush) ⇒ <code>void</code>
    - [.onAfterPush(func)](#collection-onafterpush) ⇒ <code>void</code>
    - [.onBeforeDelete(func)](#collection-onbeforedelete) ⇒ <code>void</code>
    - [.onAfterDelete(func)](#collection-onafterdelete) ⇒ <code>void</code>
    - [.onBeforeReplace(func)](#collection-onbeforereplace) ⇒ <code>void</code>
    - [.onAfterReplace(func)](#collection-onafterreplace) ⇒ <code>void</code>
  - _private properties_
    - [._items](#collection-items) : <code>Array</code>
    - [._locked](#collection-locked) : <code>boolean</code>
  - _properties_
    - [.items](#collection-items) : <code>Array</code>
    - [.lastIndex](#collection-lastindex) : <code>number</code>

<a name="newcollectionnew">
</a>

### new Collection([data], [options])

Create a Collection

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [data] | <code>Array</code> | <code>[]</code> | An array of any types |
| [options] | <code>object</code> |  | An options object |
| [options.findBy] | <code>function</code> |  | How to find an item in the collection |
| [options.beforePush] | <code>function</code> \| <code>Array.&lt;function()&gt;</code> |  | The 'beforePush' hook function(s). Can be an array of functions. See also `onBeforePush` method. |
| [options.afterPush] | <code>function</code> \| <code>Array.&lt;function()&gt;</code> |  | The 'afterPush' hook function(s). Can be an array of functions. See also `onAfterPush` method. |
| [options.beforeDelete] | <code>function</code> \| <code>Array.&lt;function()&gt;</code> |  | The 'beforeDelete' hook function(s). Can be an array of functions. See also `onBeforeDelete` method. |
| [options.afterDelete] | <code>function</code> \| <code>Array.&lt;function()&gt;</code> |  | The 'afterDelete' hook function(s). Can be an array of functions. See also `onAfterDelete` method. |
| [options.beforeReplace] | <code>function</code> \| <code>Array.&lt;function()&gt;</code> |  | The 'beforeReplace' hook function(s). Can be an array of functions. See also `onBeforeReplace` method. |
| [options.afterReplace] | <code>function</code> \| <code>Array.&lt;function()&gt;</code> |  | The 'afterReplace' hook function(s). Can be an array of functions. See also `onAfterReplace` method. |
| [options.lock] | <code>boolean</code> |  | The collection "lock" option |

<a name="collection-pushitem">
</a>

### core.context.support.collection.Collection.pushItem(item) ⇒ <code>Promise.&lt;module:context/support/collection/Collection~Collection&gt;</code>

Push an item to the collection

**Returns**: <code>Promise.&lt;module:context/support/collection/Collection~Collection&gt;</code> - Returns the collection

**Category**: async methods  
| Param | Type | Description |
| --- | --- | --- |
| item | <code>\*</code> | Item to push |

**Example**

```js
const collection = new Collection()collection.pushItem({ id: 1, name: 'John' }).items // -> output: [ { id: 1, name: 'John' } ]
```

<a name="collection-add">
</a>

### core.context.support.collection.Collection.add(...args) ⇒ <code>Promise.&lt;module:context/support/collection/Collection~Collection&gt;</code>

[Alias of pushItem](#collection-pushitem)

**Returns**: <code>Promise.&lt;module:context/support/collection/Collection~Collection&gt;</code> - Returns the collection

**Category**: async methods  
| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | Same as pushItem |

<a name="collection-push">
</a>

### core.context.support.collection.Collection.push(...args) ⇒ <code>Promise.&lt;module:context/support/collection/Collection~Collection&gt;</code>

[Alias of pushItem](#collection-pushitem)

**Returns**: <code>Promise.&lt;module:context/support/collection/Collection~Collection&gt;</code> - Returns the collection

**Category**: async methods  
| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | Same as pushItem |

<a name="collection-deleteitem">
</a>

### core.context.support.collection.Collection.deleteItem(key) ⇒ <code>Promise.&lt;module:context/support/collection/Collection~Collection&gt;</code>

Delete item from collection

**Returns**: <code>Promise.&lt;module:context/support/collection/Collection~Collection&gt;</code> - Returns the collection

**Category**: async methods  
| Param | Type | Description |
| --- | --- | --- |
| key | <code>function</code> \| <code>number</code> \| <code>string</code> | The key to find item. If function provided, execute this function to find item (same as Array.find function) |

<a name="collection-delete">
</a>

### core.context.support.collection.Collection.delete(...args) ⇒ <code>Promise.&lt;module:context/support/collection/Collection~Collection&gt;</code>

[Alias of deleteItem](#collection-deleteitem)

**Returns**: <code>Promise.&lt;module:context/support/collection/Collection~Collection&gt;</code> - Returns the collection

**Category**: async methods  
| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | Same as deleteItem |

<a name="collection-replaceitem">
</a>

### core.context.support.collection.Collection.replaceItem(key, to) ⇒ <code>Promise.&lt;module:context/support/collection/Collection~Collection&gt;</code>

Replace item in collection

**Returns**: <code>Promise.&lt;module:context/support/collection/Collection~Collection&gt;</code> - Returns the collection

**Category**: async methods  
| Param | Type | Description |
| --- | --- | --- |
| key | <code>function</code> \| <code>number</code> \| <code>string</code> | The key to find item. If function provided, execute this function to find item (same as Array.find function) |
| to | <code>\*</code> | The replacement item |

<a name="collection-replace">
</a>

### core.context.support.collection.Collection.replace(...args) ⇒ <code>Promise.&lt;module:context/support/collection/Collection~Collection&gt;</code>

[Alias of replaceItem](#collection-replaceitem)

**Returns**: <code>Promise.&lt;module:context/support/collection/Collection~Collection&gt;</code> - Returns the collection

**Category**: async methods  
| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | Same as replaceItem |

<a name="collection-touch">
</a>

### core.context.support.collection.Collection.touch(...args) ⇒ <code>Promise.&lt;module:context/support/collection/Collection~Collection&gt;</code>

[Alias of replaceItem](#collection-replaceitem)

**Returns**: <code>Promise.&lt;module:context/support/collection/Collection~Collection&gt;</code> - Returns the collection

**Category**: async methods  
| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | Same as replaceItem |

<a name="collection-update">
</a>

### core.context.support.collection.Collection.update(...args) ⇒ <code>Promise.&lt;module:context/support/collection/Collection~Collection&gt;</code>

Alias of [replaceItem](#collection-replaceitem)

**Returns**: <code>Promise.&lt;module:context/support/collection/Collection~Collection&gt;</code> - Returns the collection

**Category**: async methods  
| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | Same as replaceItem |

<a name="collection-upsertitem">
</a>

### core.context.support.collection.Collection.upsertItem(key, to) ⇒ <code>Promise.&lt;module:context/support/collection/Collection~Collection&gt;</code>

Update or insert item in collection

**Returns**: <code>Promise.&lt;module:context/support/collection/Collection~Collection&gt;</code> - Returns the collection

**Category**: async methods  
| Param | Type | Description |
| --- | --- | --- |
| key | <code>function</code> \| <code>number</code> \| <code>string</code> | The key to find item. If function provided, execute this function to find item (same as Array.find function) |
| to | <code>\*</code> | The replacement item |

<a name="collection-upsert">
</a>

### core.context.support.collection.Collection.upsert(...args) ⇒ <code>Promise.&lt;module:context/support/collection/Collection~Collection&gt;</code>

Alias of [upsertItem](#collection-upsertitem)

**Returns**: <code>Promise.&lt;module:context/support/collection/Collection~Collection&gt;</code> - Returns the collection

**Category**: async methods  
| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | Same as upsertItem |

<a name="collection-lock">
</a>

### core.context.support.collection.Collection.lock() ⇒ <code>module:context/support/collection/Collection~Collection</code>

Lock the collection

**Returns**: <code>module:context/support/collection/Collection~Collection</code> - Returns the collection

**Category**: methods  
<a name="collection-freeze">
</a>

### core.context.support.collection.Collection.freeze() ⇒ <code>module:context/support/collection/Collection~Collection</code>

Alias of [lock](#collection-lock)

**Returns**: <code>module:context/support/collection/Collection~Collection</code> - Returns the collection

**Category**: methods  
<a name="collection-unlock">
</a>

### core.context.support.collection.Collection.unlock() ⇒ <code>module:context/support/collection/Collection~Collection</code>

Unlock the collection

**Returns**: <code>module:context/support/collection/Collection~Collection</code> - Returns the collection

**Category**: methods  
<a name="collection-unfreeze">
</a>

### core.context.support.collection.Collection.unfreeze() ⇒ <code>module:context/support/collection/Collection~Collection</code>

Alias of [unlock](#collection-unlock)

**Returns**: <code>module:context/support/collection/Collection~Collection</code> - Returns the collection

**Category**: methods  
<a name="collection-islocked">
</a>

### core.context.support.collection.Collection.isLocked() ⇒ <code>boolean</code>

If collection is locked.

**Returns**: <code>boolean</code> - Returns true if collection is locked

**Category**: methods  
<a name="collection-abortiflocked">
</a>

### core.context.support.collection.Collection.abortIfLocked() ⇒ <code>void</code>

abortIfLocked

**Category**: methods  
**Throws**:

- Error

<a name="collection-findby">
</a>

### core.context.support.collection.Collection.findBy(key, array) ⇒ <code>\*</code>

Find an item by key in the collection

**Returns**: <code>\*</code> - Returns the found collection item

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| key | <code>\*</code> | The key |
| array | <code>Array</code> | The collection items |

<a name="collection-setitems">
</a>

### core.context.support.collection.Collection.setItems(items) ⇒ <code>void</code>

Set collection items

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| items | <code>Array</code> | Items to set |

<a name="collection-get">
</a>

### core.context.support.collection.Collection.get() ⇒ <code>Array</code>

Get collection items (same as `all()` method)

**Returns**: <code>Array</code> - Return collection items

**Category**: methods  
<a name="collection-all">
</a>

### core.context.support.collection.Collection.all() ⇒ <code>Array</code>

Get collection items (same as `get()` method)

**Returns**: <code>Array</code> - Return collection items

**Category**: methods  
<a name="collection-find">
</a>

### core.context.support.collection.Collection.find(key) ⇒ <code>\*</code>

Find an item by key

**Returns**: <code>\*</code> - Returns the found collection item

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| key | <code>function</code> \| <code>number</code> \| <code>string</code> | The key to find item. If function provided, execute this function to find item (same as Array.find function) |

<a name="collection-findwithindex">
</a>

### core.context.support.collection.Collection.findWithIndex(key) ⇒ <code>\*</code>

Find an item with its index by key

**Returns**: <code>\*</code> - Returns the found collection item

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| key | <code>function</code> \| <code>number</code> \| <code>string</code> | The key to find item. If function provided, execute this function to find item (same as Array.find function) |

<a name="collection-filter">
</a>

### core.context.support.collection.Collection.filter(callbackFn, [thisArg]) ⇒ <code>Array</code>

filter

**Returns**: <code>Array</code> - - The filtered items

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| callbackFn | <code>function</code> | Same argument as Array.prototype.filter() method |
| [thisArg] | <code>\*</code> | Same argument as Array.prototype.filter() method |

<a name="collection-findmany">
</a>

### core.context.support.collection.Collection.findMany(...args) ⇒ <code>Promise.&lt;module:context/support/collection/Collection~Collection&gt;</code>

[Alias of filter](#collection-filter)

**Returns**: <code>Promise.&lt;module:context/support/collection/Collection~Collection&gt;</code> - Returns the collection

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | Same as filter |

<a name="collection-map">
</a>

### core.context.support.collection.Collection.map(...args) ⇒ <code>Array</code>

Map Array "map" method

**Returns**: <code>Array</code> - Returns the result of Array "map" method

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | The Array "map" method parameters |

<a name="collection-reduce">
</a>

### core.context.support.collection.Collection.reduce(...args) ⇒ <code>Array</code> \| <code>object</code>

Map Array "reduce" method

**Returns**: <code>Array</code> \| <code>object</code> - Returns the result of Array "reduce" method

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | The Array "reduce" method parameters |

<a name="collection-maxby">
</a>

### core.context.support.collection.Collection.maxBy(sortFct) ⇒ <code>\*</code>

See "maxBy" util documentation.

**Returns**: <code>\*</code> - Returns the found value in collection

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| sortFct | <code>function</code> | The sort function |

<a name="collection-minby">
</a>

### core.context.support.collection.Collection.minBy(sortFct) ⇒ <code>\*</code>

See "minBy" util documentation.

**Returns**: <code>\*</code> - Returns the found value in collection

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| sortFct | <code>function</code> | The sort function |

<a name="collection-orderby">
</a>

### core.context.support.collection.Collection.orderBy(...args) ⇒ <code>\*</code>

See "orderBy" util documentation.

**Returns**: <code>\*</code> - Returns the found value in collection

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | The second and third optional parameters of "orderBy" lodash method |

<a name="collection-sortby">
</a>

### core.context.support.collection.Collection.sortBy(sortFct) ⇒ <code>\*</code>

See "sortBy" util documentation.

**Returns**: <code>\*</code> - Returns the found value in collection

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| sortFct | <code>function</code> | The sort function |

<a name="collection-onbeforepush">
</a>

### core.context.support.collection.Collection.onBeforePush(func) ⇒ <code>void</code>

**BEFORE_PUSH** hook append method.The function to append, which can be _async_ takes two arguments:```tsfunction (value: any, collection: Collection) => void | Promise<void>```- **value**: `{any}` the value- **collection**: `{Collection}` the collection object

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| func | <code>function</code> \| <code>Array.&lt;function()&gt;</code> | The function to append to the hook queue. Can be an array of functions. |

**Example**

```js
collection.onBeforePush((value, collection) => {  console.log(value)})
```

<a name="collection-onafterpush">
</a>

### core.context.support.collection.Collection.onAfterPush(func) ⇒ <code>void</code>

**AFTER_PUSH** hook append method.The function to append, which can be _async_ takes two arguments:```tsfunction (value: any, collection: Collection) => void | Promise<void>```- **value**: `{any}` the value- **collection**: `{Collection}` the collection object

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| func | <code>function</code> \| <code>Array.&lt;function()&gt;</code> | The function to append to the hook queue. Can be an array of functions. |

**Example**

```js
collection.onAfterPush((value, collection) => {  console.log(value)})
```

<a name="collection-onbeforedelete">
</a>

### core.context.support.collection.Collection.onBeforeDelete(func) ⇒ <code>void</code>

**BEFORE_DELETE** hook append method.The function to append, which can be _async_ takes two arguments:```tsfunction (value: any, collection: Collection) => void | Promise<void>```- **value**: `{any}` the value- **collection**: `{Collection}` the collection object

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| func | <code>function</code> \| <code>Array.&lt;function()&gt;</code> | The function to append to the hook queue. Can be an array of functions. |

**Example**

```js
collection.onBeforeDelete((value, collection) => {  console.log(value)})
```

<a name="collection-onafterdelete">
</a>

### core.context.support.collection.Collection.onAfterDelete(func) ⇒ <code>void</code>

**AFTER_DELETE** hook append method.The function to append, which can be _async_ takes two arguments:```tsfunction (value: any, collection: Collection) => void | Promise<void>```- **value**: `{any}` the value- **collection**: `{Collection}` the collection object

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| func | <code>function</code> \| <code>Array.&lt;function()&gt;</code> | The function to append to the hook queue. Can be an array of functions. |

**Example**

```js
collection.onAfterDelete((value, collection) => {  console.log(value)})
```

<a name="collection-onbeforereplace">
</a>

### core.context.support.collection.Collection.onBeforeReplace(func) ⇒ <code>void</code>

**BEFORE_REPLACE** hook append method.The function to append, which can be _async_ takes three arguments:```tsfunction (from: any, to: any, collection: Collection) => void | Promise<void>```- **from**: `{any}` the value to replace- **to**: `{any}` the replacement value- **collection**: `{Collection}` the collection object

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| func | <code>function</code> \| <code>Array.&lt;function()&gt;</code> | The function to append to the hook queue. Can be an array of functions. |

**Example**

```js
collection.onBeforeReplace((from, to, collection) => {  console.log('From: ', from)  console.log('To: ', to)})
```

<a name="collection-onafterreplace">
</a>

### core.context.support.collection.Collection.onAfterReplace(func) ⇒ <code>void</code>

**AFTER_REPLACE** hook append method.The function to append, which can be _async_ takes three arguments:```tsfunction (from: any, to: any, collection: Collection) => void | Promise<void>```- **from**: `{any}` the value to replace- **to**: `{any}` the replacement value- **collection**: `{Collection}` the collection object

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| func | <code>function</code> \| <code>Array.&lt;function()&gt;</code> | The function to append to the hook queue. Can be an array of functions. |

**Example**

```js
collection.onAfterReplace((from, to, collection) => {  console.log('From: ', from)  console.log('To: ', to)})
```

<a name="collection-items">
</a>

### core.context.support.collection.Collection.\_items : <code>Array</code>

**Category**: private properties  
<a name="collection-locked">
</a>

### core.context.support.collection.Collection.\_locked : <code>boolean</code>

**Category**: private properties  
<a name="collection-items">
</a>

### core.context.support.collection.Collection.items : <code>Array</code>

Collection items accessor

**Category**: properties  
**Read only**: true  
<a name="collection-lastindex">
</a>

### core.context.support.collection.Collection.lastIndex : <code>number</code>

Collection last index accessor

**Category**: properties  
**Read only**: true  
