---
title: Mixins
headline: Mixins
sidebarTitle: Mixins
initialOpenGroupIndex: -1
prev: ../filters/
next: ./global/
---

# Mixins

<a name="mixins">
</a>

## mixins

Contains all app mixins.

