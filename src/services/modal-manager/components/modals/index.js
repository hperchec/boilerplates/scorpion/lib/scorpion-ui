import BaseModal from './BaseModal'
import ModalContainer from './ModalContainer.vue'
import ModalOverlay from './ModalOverlay.vue'
import Modal from './Modal.vue'

export default {
  BaseModal,
  ModalContainer,
  ModalOverlay,
  Modal
}
