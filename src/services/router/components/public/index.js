import layouts from './layouts'
import views from './views'

export default {
  layouts,
  views
}
