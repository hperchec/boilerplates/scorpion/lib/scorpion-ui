/**
 * @vuepress
 * ---
 * title: "Service (logger): Logger class"
 * headline: "Service (logger): Logger class"
 * sidebarTitle: .Logger
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import Core from '@/Core'
import toolbox from '@/toolbox'
import { getAllKeys, isUndef, deepMerge, dateFormat } from '@/utils'

const { VuePluginable } = toolbox.vue

/**
 * @extends VuePluginable
 * @classdesc
 * Service **logger**: Logger class
 */
class Logger extends VuePluginable {
  /**
   * Private
   */
  #log
  #types

  /**
   * Create a new instance
   * @param {object} [options = {}] - The Logger options
   * @param {boolean} [options.log] - Enable/disable logger
   * @param {object} [options.types] - Logger types
   */
  constructor (options = {}) {
    // Call VuePluginable constructor
    super()
    // Set debugging private property
    if (this.__v_descriptor__) {
      this.__v_descriptor__.namespace = 'service:logger'
      this.__v_descriptor__.description = 'Access the Logger instance'
      // @ts-ignore
      this.__v_descriptor__.externalLink = __getDocUrl__('/api/services/logger/Logger')
    }
    // First, merge with defaultOptions
    options = deepMerge(Logger.defaultOptions, options)
    // Log option
    this.#log = options.log
    // Types
    this.#types = {}
    // Default type first
    if (isUndef(options.types.default)) {
      throw new Error('Logger service class: options.types.default is undefined')
    } else {
      this.setType('default', options.types.default)
    }
    // Set other types
    for (const type in options.types) {
      if (type !== 'default') {
        this.setType(type, options.types[type])
      }
    }
    /**
     * @alias Logger#consoleLog.group
     * @param {string} message - Same as consoleLog parameter
     * @param {object} [options] - Same as consoleLog parameter plus the "collapsed" property
     * @param {boolean} [options.collapsed] - Same as consoleLog parameter plus the "collapsed" property
     * @param {Function} [callback] - A callback function that takes a subgroup callback as unique parameter
     * @returns {void}
     */
    // @ts-ignore
    this.consoleLog.group = (message, options = {}, callback) => {
      options = Object.assign({
        type: 'default',
        dev: true,
        prod: true,
        collapsed: true // default collapsed
      }, options)
      if (this.log) {
        let visibility = false
        // Development
        // @ts-ignore
        if (process.env.NODE_ENV === 'development' && options.dev) {
          visibility = true
        }
        // Production
        // @ts-ignore
        if (process.env.NODE_ENV === 'production' && options.prod) {
          visibility = true
        }
        // If log must be displayed
        if (visibility) {
          // Get log type configuration
          // @ts-ignore
          const { badgeContent, badgeColor, badgeBgColor, timeColor, messageColor, prependMessage } = this.types[options.type]
          // Get time
          const atTime = dateFormat(new Date(), 'HH:MM:ss.l')
          // @ts-ignore
          console[options.collapsed ? 'groupCollapsed' : 'group'](...this.consoleLog.buildStyledMessage(badgeContent, badgeColor, badgeBgColor, atTime, timeColor, prependMessage, message, messageColor))
          // function (args, subgroup) {}
          // @ts-ignore
          callback(this.consoleLog.group)
          console.groupEnd()
        }
      }
    }
  }

  /**
   * Overrides static VuePluginable properties
   */
  static rootOption = 'logger'
  static aliases = []

  /**
   * Accessors & mutators
   */

  /**
   * log
   * @category property
   * @type {boolean}
   * @readonly
   */
  get log () {
    return this.#log
  }

  /**
   * types
   * @category property
   * @type {object}
   * @readonly
   */
  get types () {
    return this.#types
  }

  /**
   * Methods
   */

  /**
   * setType
   * @param {string} name - The type name to add/overwrite
   * @param {object} [options = {}] - The type object
   * @param {string} [options.badgeContent] - The badge content (Default: `default` type value)
   * @param {string} [options.badgeColor] - The badge text color (Default: `default` type value)
   * @param {string} [options.badgeBgColor] - The badge background color (Default: `default` type value)
   * @param {string} [options.timeColor] - The displayed time text color (Default: `default` type value)
   * @param {string} [options.messageColor] - The message text color (Default: `default` type value)
   * @param {string} [options.prependMessage] - The string to prepend to message (Default: `default` type value)
   * @returns {void}
   * @example
   * logger.setType('info', {
   *   badgeContent: '[INFO]',
   *   badgeColor: '#FDE5DC',
   *   badgeBgColor: '#F58E69',
   *   timeColor: '#551919',
   *   messageColor: '#A4330B',
   *   prependMessage: '❔'
   * })
   */
  setType (name, options = {}) {
    this.#types[name] = {
      badgeContent: options.badgeContent || this.types.default.badgeContent,
      badgeColor: options.badgeColor || this.types.default.badgeColor,
      badgeBgColor: options.badgeBgColor || this.types.default.badgeBgColor,
      timeColor: options.timeColor || this.types.default.timeColor,
      messageColor: options.messageColor || this.types.default.messageColor,
      prependMessage: options.prependMessage || this.types.default.prependMessage
    }
  }

  /**
   * consoleLog
   * @category methods
   * @description Log a message in the console
   * @param {string} message - The message
   * @param {object} [options] - The options
   * @param {string} [options.type = 'default'] - Log type ('default', 'warning', 'error', ...)
   * @param {boolean} [options.dev = true] - Define if log must be displayed in development mode
   * @param {boolean} [options.prod = true] - Define if log must be displayed in production mode
   * @returns {void}
   * @example
   * logger.consoleLog('Development mode enabled', { prod: false })
   * logger.consoleLog('Production mode enabled', { type: 'info', dev: false })
   * logger.consoleLog('Use "const" or "let" instead of "var"!', { type: 'advice' })
   * logger.consoleLog('Parameter "options" is missing. Set to default...', { type: 'warning' })
   * logger.consoleLog('Server is not responding...', { type: 'error' })
   * logger.consoleLog('App booted!', { type: 'system' })
   * logger.consoleLog('Redirecting...', { type: 'router' })
   * logger.consoleLog('State mutated!', { type: 'store' })
   */
  consoleLog (message, options = {}) {
    options = Object.assign({
      type: 'default',
      dev: true,
      prod: true
    }, options)
    if (this.log) {
      let visibility = false
      // Development
      if (process.env.NODE_ENV === 'development' && options.dev) {
        visibility = true
      }
      // Production
      if (process.env.NODE_ENV === 'production' && options.prod) {
        visibility = true
      }
      // If log must be displayed
      if (visibility) {
        // Get log type configuration
        const { badgeContent, badgeColor, badgeBgColor, timeColor, messageColor, prependMessage } = this.types[options.type]
        // Get time
        const atTime = dateFormat(new Date(), 'HH:MM:ss.l')
        // Console log
        // @ts-ignore
        console.log(...this.consoleLog.buildStyledMessage(badgeContent, badgeColor, badgeBgColor, atTime, timeColor, prependMessage, message, messageColor))
      }
    }
  }

  /**
   * consoleLogTest
   * @category methods
   * @description Log a message in the console with all defined types
   * @param {string} [message = 'Test'] - The message (default: 'Test')
   * @returns {void}
   * @example
   * logger.consoleLogTest()
   * logger.consoleLogTest('Test message')
   */
  consoleLogTest (message = 'test') {
    for (const type in this.types) {
      console.log('Type: ' + type, this.types[type])
      this.consoleLog(message, { type })
    }
  }

  /**
   * Overrides the default toJSON object method for JSON.stringify() calls
   * @returns {object} - The instance data to be serialized
   */
  toJSON () {
    const keys = getAllKeys(this, {
      excludeKeys: [
        /^_/, // private properties that starts with '_'
        'toJSON',
        'toString',
        'constructor'
      ]
    })
    const jsonObj = keys.reduce((obj, key) => {
      obj[key] = this[key]
      return obj
    }, {})
    return jsonObj
  }

  /**
   * Static properties
   */

  /**
   * Default options
   */
  static defaultOptions = {
    /**
     * Enable the logger
     * @type {boolean}
     */
    log: true, // Default true
    /**
     * Log types
     * @type {object}
     */
    types: {
      default: {
        badgeContent: '[LOG]',
        badgeColor: '#FFFF00',
        badgeBgColor: '#F65D6C',
        timeColor: '#551919',
        messageColor: '#551919',
        prependMessage: '▶'
      }
    }
  }

  /**
   * Static methods
   */

  /**
   * Install method for Vue
   * @param {Vue} _Vue - Vue
   * @param {object} [options = {}] - The options of the plugin
   */
  static install (_Vue, options = {}) {
    const Vue = _Vue
    super.install(Vue)
  }
}

// @ts-ignore
Logger.prototype.consoleLog.buildStyledMessage = function (badgeContent, badgeColor, badgeBgColor, atTime, timeColor, prependMessage, message, messageColor) {
  return [
    // Full string
    `%c${Core.config.globals.APP_NAME}%c${badgeContent}%c[${atTime}]%c${prependMessage} ${message}`,
    // CSS for %c1
    `line-height: 22px; padding: 4px; background-color: ${badgeBgColor}; color: white; border-radius: 4px 0 0 4px;`,
    // CSS for %c2
    `line-height: 22px; padding: 4px 4px 4px 0; background-color: ${badgeBgColor}; color: ${badgeColor}; border-radius: 0 4px 4px 0;`,
    // CSS for %c3
    `line-height: 22px; padding: 4px; color: ${timeColor};`,
    // CSS for %c4
    `line-height: 22px; padding: 4px; color: ${messageColor};`
  ]
}

export default Logger
