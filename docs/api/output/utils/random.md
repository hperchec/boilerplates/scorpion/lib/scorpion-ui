---
title: random
headline: random
sidebarTitle: random
---

# random

<a name="random">
</a>

## random : <code>object</code>

**Schema**:

- [random](#random) : <code>object</code>
  - [.number](#random-number) ⇒ <code>number</code>
  - [.alphaNumeric](#random-alphanumeric) ⇒ <code>string</code>

<a name="random-number">
</a>

### random.number ⇒ <code>number</code>

Generate random number based on lodash.random function.::: tip See also[lodash.random](https://www.npmjs.com/package/lodash.random) package documentation:::

**Returns**: <code>number</code> - Returns the random number

| Param | Type | Description |
| --- | --- | --- |
| [lower] | <code>number</code> | The lower value |
| [upper] | <code>number</code> | The upper value |
| [floating] | <code>boolean</code> | If uses float numbers |

**Example**

```js
number(0, 5) // Return -> 1 (example)
```

<a name="random-alphanumeric">
</a>

### random.alphaNumeric ⇒ <code>string</code>

Generate random alphanumeric string based on lodash.samplesize function.::: tip See also[lodash.samplesize](https://www.npmjs.com/package/lodash.samplesize) package documentation:::

**Returns**: <code>string</code> - Returns the created random string

| Param | Type | Description |
| --- | --- | --- |
| length | <code>number</code> | The length of returned random string |
| [charset] | <code>Array</code> \| <code>string</code> | An array or string of characters |

**Example**

```js
alphaNumeric(5) // Return -> 'Gy85f' (example)alphaNumeric(5, 'ABCDE') // Return -> 'AADBE' (example)
```

