---
title: Router - middlewares
headline: Router - middlewares
sidebarTitle: .middlewares
prev: false
next: false
---

# Router - middlewares

<a name="middlewares">
</a>

## .middlewares : <code>object</code>

> [context](../../).[router](../).[middlewares](./)::: tip INFOPlease check the [routing](../../../../routing) documentation.:::

<a name="middlewares-appmiddleware">
</a>

### middlewares.AppMiddleware() : <code>function</code>

**See**: [AppMiddleware](./AppMiddleware)

