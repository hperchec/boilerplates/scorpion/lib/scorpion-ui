/**
 * @vuepress
 * ---
 * title: CatchAll route
 * headline: CatchAll route
 * sidebarTitle: ._CatchAll
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import Core from '@/Core'

// @ts-ignore
const log = (...args) => Core.service('logger').consoleLog(...args) // eslint-disable-line dot-notation

/**
 * @name _CatchAll
 * @static
 * @type {object}
 * @description
 * - **Name**: _CatchAll
 * - **Path**: `*`
 * - **Params**: *none*
 * - **Query**: *none*
 */
export default {
  /**
   * @description Route path
   * @name _CatchAll.path
   * @type {string}
   * @default '*'
   */
  path: '*',
  /**
   * @description Route name
   * @name _CatchAll.name
   * @type {string}
   * @default _CatchAll
   */
  name: '_CatchAll',
  /**
   * @description Route component
   * @name _CatchAll.component
   * @type {null}
   * @default null
   */
  component: null,
  /**
   * Options
   */
  options: {
    /**
     * @alias _CatchAll.options.beforeEnter
     * @type {Function}
     * @returns {void}
     * @description
     * > See also [beforeEnter per-route guard documentation](https://v3.router.vuejs.org/guide/advanced/navigation-guards.html#per-route-guard)
     *
     * Default redirect to route defined in service option (`Core.context.services['router'].options.catchAllRedirect`)
     */
    beforeEnter: (to, from, next) => {
      /* eslint-disable dot-notation */
      const redirectPath = typeof Core.context.services['router'].options.catchAllRedirect === 'string'
        ? Core.context.services['router'].options.catchAllRedirect
        : Core.context.services['router'].options.catchAllRedirect.path
      log(`❌ Not found route "${to.fullPath}". Redirect to "${redirectPath}"`, { type: 'router' })
      next(redirectPath)
    }
  }
}
