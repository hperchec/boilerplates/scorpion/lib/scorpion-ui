---
title: groupBy
headline: groupBy
sidebarTitle: groupBy
---

# groupBy

<a name="groupby">
</a>

## groupBy ⇒ <code>Array</code>

Groupby on array (lodash.groupBy).

**Returns**: <code>Array</code> - Returns the grouped elements

| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | Same arguments as lodash.groupBy |

**Example**

```js
const users = (['eight', 'nine', 'four', 'seven'])groupBy(users, 'length') // => { '4': [ 'nine', 'four' ], '5': [ 'eight', 'seven' ]}
```

