const ddata = require('dmd/helpers/ddata')

exports.hasIdentifier = hasIdentifier

/**
 * Conditional block helper: if identifier is set
 * @param {object} options - Handlebars options
 * @returns {string} Returns options.fn or options.inverse
 */
function hasIdentifier (options) {
  return ddata._identifier(options)
    ? options.fn(this)
    : options.inverse(this)
}
