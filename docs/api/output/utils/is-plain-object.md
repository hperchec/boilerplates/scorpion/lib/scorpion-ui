---
title: isPlainObject
headline: isPlainObject
sidebarTitle: isPlainObject
---

# isPlainObject

<a name="isplainobject">
</a>

## isPlainObject ⇒ <code>boolean</code>

is-plain-object package method.::: tip See also[is-plain-object](https://github.com/jonschlinkert/is-plain-object) package documentation:::

**Returns**: <code>boolean</code> - Returns true if value is plain object

| Param | Type | Description |
| --- | --- | --- |
| value | <code>\*</code> | The value to check |

**Example**

```js
isPlainObject({ a: 1, b: 2 }) // trueisPlainObject(new Date()) // false
```

