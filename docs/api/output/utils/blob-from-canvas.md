---
title: blobFromCanvas
headline: blobFromCanvas
sidebarTitle: blobFromCanvas
---

# blobFromCanvas

<a name="blobfromcanvas">
</a>

## blobFromCanvas ⇒ <code>Promise.&lt;Blob&gt;</code>

Convert HTMLCanvasElement to blob

**Returns**: <code>Promise.&lt;Blob&gt;</code> - Returns blob

| Param | Type | Description |
| --- | --- | --- |
| canvas | <code>HTMLCanvasElement</code> | The target canvas |

**Example**

```js
blobFromCanvas(canvas)
```

