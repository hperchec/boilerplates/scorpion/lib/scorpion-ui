---
title: get-module-context
headline: get-module-context
---

# get-module-context

<a name="getmodulecontext">
</a>

## getModuleContext ⇒ <code>object</code>

Get a store module context by namespace

**Returns**: <code>object</code> - The module context

| Param | Type | Description |
| --- | --- | --- |
| store | <code>module:vuex~Store</code> | The store instance |
| namespace | <code>string</code> | The full module namespace |

