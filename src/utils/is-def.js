/**
 * @vuepress
 * ---
 * title: isDef
 * headline: isDef
 * sidebarTitle: isDef
 * ---
 */

/**
 * isDef
 * @param {*} value - The variable to check
 * @returns {boolean} Returns true if value is defined (!== undefined)
 * @description
 * Check if value is defined (!== undefined)
 * @example
 * isDef(value) // => true if value is defined
 */
export const isDef = function (value) {
  return value !== undefined
}

export default isDef
