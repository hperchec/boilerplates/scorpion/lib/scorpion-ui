---
title: "Service (toast-manager): Toast class"
headline: "Service (toast-manager): Toast class"
sidebarTitle: .Toast
sidebarDepth: 0
prev: false
next: false
---

# Service (toast-manager): Toast class

<a name="toast">
</a>

## Toast

Service **toast-manager**: Toast class

**Schema**:

- [Toast](#toast)
  - [new Toast(options)](#newtoastnew)
  - _instance_
    - [.toJSON()](#toast-tojson) ⇒ <code>object</code>
    - _methods_
      - [.setId()](#toast-setid) ⇒ <code>void</code>
      - [.init()](#toast-init) ⇒ <code>module:services/toast-manager/Toast~Toast</code>
      - [.clearTimeOut()](#toast-cleartimeout) ⇒ <code>void</code>
      - [.delete()](#toast-delete) ⇒ <code>void</code>
    - _properties_
      - [.id](#toast-id) : <code>string</code>
      - [.toaster](#toast-toaster) : <code>module:services/toast-manager/Toaster~Toaster</code>
      - [.content](#toast-content) : <code>string</code> \| <code>object</code>
      - [.contentProps](#toast-contentprops) : <code>object</code>
      - [.asyncHide](#toast-asynchide) : <code>function</code>
      - [.timeOut](#toast-timeout) : <code>number</code>
      - [.timeOutID](#toast-timeoutid) : <code>number</code>
      - [.keepAlive](#toast-keepalive) : <code>boolean</code>
      - [.noClose](#toast-noclose) : <code>boolean</code>
  - _static_
    - [.defaultOptions](#toast-defaultoptions) : <code>object</code>

<a name="newtoastnew">
</a>

### new Toast(options)

Create new Toast

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| options | <code>object</code> |  | The Toast options |
| options.toaster | <code>module:services/toast-manager/Toaster~Toaster</code> |  | The toast toaster |
| options.content | <code>string</code> \| <code>object</code> |  | The toast content (can be string or Vue component object) |
| [options.contentProps] | <code>object</code> | <code>{}</code> | The props to pass to content component |
| [options.asyncHide] | <code>function</code> |  | A function to call to hide the toast asynchronously |
| [options.timeOut] | <code>number</code> | <code>5000</code> | The time (in ms) to wait before hide toast (Default: 5000ms). Useless if `asyncHide` property is defined |
| [options.keepAlive] | <code>boolean</code> | <code>false</code> | If the toast must be closed manually |
| [options.noClose] | <code>boolean</code> | <code>false</code> | If the toast cant be closed manually |
| [options.meta] | <code>object</code> | <code>{}</code> | All other meta data to pass to the toast |

<a name="toast-tojson">
</a>

### toast.toJSON() ⇒ <code>object</code>

Overrides the default toJSON object method for JSON.stringify() calls

**Returns**: <code>object</code> - - The instance data to be serialized

<a name="toast-setid">
</a>

### toast.setId() ⇒ <code>void</code>

Set unique ID

**Category**: methods  
<a name="toast-init">
</a>

### toast.init() ⇒ <code>module:services/toast-manager/Toast~Toast</code>

Init toast

**Returns**: <code>module:services/toast-manager/Toast~Toast</code> - Returns the Toast instance itself

**Category**: methods  
<a name="toast-cleartimeout">
</a>

### toast.clearTimeOut() ⇒ <code>void</code>

Clear time out

**Category**: methods  
<a name="toast-delete">
</a>

### toast.delete() ⇒ <code>void</code>

Delete toast

**Category**: methods  
<a name="toast-id">
</a>

### toast.id : <code>string</code>

Toast unique id

**Category**: properties  
**Read only**: true  
<a name="toast-toaster">
</a>

### toast.toaster : <code>module:services/toast-manager/Toaster~Toaster</code>

Toast toaster

**Category**: properties  
<a name="toast-content">
</a>

### toast.content : <code>string</code> \| <code>object</code>

Toast content

**Category**: properties  
<a name="toast-contentprops">
</a>

### toast.contentProps : <code>object</code>

Toast contentProps

**Category**: properties  
<a name="toast-asynchide">
</a>

### toast.asyncHide : <code>function</code>

Toast asyncHide

**Category**: properties  
<a name="toast-timeout">
</a>

### toast.timeOut : <code>number</code>

Toast time out

**Category**: properties  
<a name="toast-timeoutid">
</a>

### toast.timeOutID : <code>number</code>

Toast time out ID

**Category**: properties  
**Read only**: true  
<a name="toast-keepalive">
</a>

### toast.keepAlive : <code>boolean</code>

Toast keep-alive

**Category**: properties  
<a name="toast-noclose">
</a>

### toast.noClose : <code>boolean</code>

Toast no-close

**Category**: properties  
<a name="toast-defaultoptions">
</a>

### Toast.defaultOptions : <code>object</code>

Default options

**Properties**

| Name | Type | Description |
| --- | --- | --- |
| toaster | <code>module:services/toast-manager/Toaster~Toaster</code> | The toast toaster (default: `undefined`) |
| content | <code>string</code> \| <code>object</code> | The toast content (can be string or Vue component object) (default: `undefined`) |
| contentProps | <code>object</code> | The props to pass to content component if defined (default: `{}`) |
| asyncHide | <code>function</code> | A function to call to hide the toast asynchronously (default: `undefined`) |
| timeOut | <code>number</code> | The time (in ms) to wait before hide toast. Useless if `asyncHide` property is defined (default: `5000`) |
| keepAlive | <code>boolean</code> | If the toast must be closed manually (default: `false`) |
| noClose | <code>boolean</code> | If the toast cant be closed manually (default: `false`) |
| meta | <code>object</code> | All other meta data to pass to the toast (default: `{}`) |

