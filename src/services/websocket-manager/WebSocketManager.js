/**
 * @vuepress
 * ---
 * title: "Service (websocket-manager): WebSocketManager class"
 * headline: "Service (websocket-manager): WebSocketManager class"
 * sidebarTitle: .WebSocketManager
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import Core from '@/Core'
import { getAllKeys, typeCheck } from '@/utils'
import toolbox from '@/toolbox'
import WSConnection from './WSConnection' // jsDoc type

const { VuePluginable } = toolbox.vue

/**
 * @extends VuePluginable
 * @classdesc
 * Service **websocket-manager**: WebSocketManager class
 */
class WebSocketManager extends VuePluginable {
  /**
   * Create a new WebSocket Manager
   * @param {object} options - Constructor options
   * @param {WSConnection[]} options.connections - An array of WSConnection
   */
  constructor (options) {
    const tmpConnections = []
    // Loop on received options
    options.connections.forEach((connection) => {
      // Check if item is a instance of WSConnection
      const _WSConnection = Core.context.services['websocket-manager'].WSConnection
      if (typeCheck(_WSConnection, connection)) {
        tmpConnections.push(connection)
      } else {
        throw new Error('WebSocketManager can\'t load connection. One of them is not an WSConnection instance')
      }
    })
    const initVMData = {
      connections: tmpConnections
    }
    // Call VuePluginable constructor
    super(initVMData)
    // Set debugging private property
    if (this.__v_descriptor__) {
      this.__v_descriptor__.namespace = 'service:websock-manager'
      this.__v_descriptor__.description = 'Access the WebSocketManager instance'
      // @ts-ignore
      this.__v_descriptor__.externalLink = __getDocUrl__('/api/services/websocket-manager/WebSocketManager')
    }
  }

  /**
   * Overrides static VuePluginable properties
   */
  static reactive = true
  static rootOption = 'webSocketManager'
  static aliases = [ 'wsManager' ]

  /**
   * Accessors & mutators
   */

  /**
   * Registered connections
   * @category properties
   * @readonly
   * @type {WSConnection[]}
   */
  get connections () {
    return this._vm.connections
  }

  /**
   * Methods
   */

  /**
   * add
   * @category methods
   * @param {WSConnection} connection - The connection to add to manager
   * @returns {WSConnection} Return this.use(<connection.name>)
   * @description
   * Add a connection to manager anytime
   */
  add (connection) {
    // Check if item is a instance of WSConnection
    const _WSConnection = Core.context.services['websocket-manager'].WSConnection
    if (typeCheck(_WSConnection, connection)) {
      // Check in registered connections
      const connectionExists = this.connections.find((c) => { return connection.name === c.name })
      if (!connectionExists) {
        this._vm.connections.push(connection)
      } else {
        console.log('WebSocketManager can\'t add connection: "' + connection.name + '" is already set...')
      }
    } else {
      throw new Error('WebSocketManager can\'t load connection. One of them is not an WSConnection instance')
    }
    return this.use(connection.name)
  }

  /**
   * use
   * @category methods
   * @param {string} connectionName - The connection name to target
   * @returns {WSConnection} Return the connection
   * @description
   * Use a specific connection that was loaded by the manager
   */
  use (connectionName) {
    // Check arg type
    if (!typeCheck(String, connectionName)) {
      throw new TypeError('WebSocketManager service : "use" method attempts String type for "connectionName" parameter. Received : ' + typeof connectionName)
    }
    // Find by name
    const connection = this.connections.find(c => c.name === connectionName)
    // If found
    if (connection) {
      return connection
    } else {
      // Not found -> throw Error
      throw new Error('WebSocketManager service : "use" method can\'t find connection by name "' + connectionName + '"')
    }
  }

  /**
   * Overrides the default toJSON object method for JSON.stringify() calls
   * @returns {object} - The instance data to be serialized
   */
  toJSON () {
    const keys = getAllKeys(this, {
      excludeKeys: [
        /^_/, // private properties that starts with '_'
        'toJSON',
        'toString',
        'constructor'
      ]
    })
    const jsonObj = keys.reduce((obj, key) => {
      obj[key] = this[key]
      return obj
    }, {})
    return jsonObj
  }

  /**
   * Static
   */

  /**
   * Install method for Vue
   * @param {Vue} _Vue - Vue
   * @param {object} [options = {}] - The options of the plugin
   */
  static install (_Vue, options = {}) {
    const Vue = _Vue
    super.install(Vue)
  }
}

export default WebSocketManager
