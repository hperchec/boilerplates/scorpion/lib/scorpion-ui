export default LocalDatabaseOptions;
declare class LocalDatabaseOptions {
    constructor(name: string, options?: {
        driver?: string;
        driverOptions?: object;
        init?: Function;
    });
    private _name;
    _driver: any;
    _driverOptions: any;
    _init: any;
    set name(value: string);
    get name(): string;
    set driver(value: string);
    get driver(): string;
    set driverOptions(value: any);
    get driverOptions(): any;
    set init(value: Function);
    get init(): Function;
    toJSON(): object;
}
//# sourceMappingURL=LocalDatabaseOptions.d.ts.map