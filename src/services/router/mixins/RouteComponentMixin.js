import { isDef } from '@/utils'

/**
 * RouteComponentMixin
 */
export default {
  name: 'RouteComponent',
  props: {
    /**
     * Default queryParams prop (default empty object: {})
     */
    queryParams: {
      type: Object,
      default: function () {
        return {}
      }
    }
  },
  computed: {
    // query: function () {
    //   return this.$route.query
    // }
  },
  methods: {
    /**
     * Update Page title (document.title)
     * @param {object} to - The 'to' route
     * @param {object} from - The 'from' route
     * @returns {void}
     */
    updatePageTitle: function (to, from) {
      // Default page title
      let pageTitle = this.$t('global.pageTitleSuffix')
      // If route.meta.pageMeta
      if (to.meta.pageMeta) {
        pageTitle = `${to.meta.pageMeta.title(this)} | ${this.$t('global.pageTitleSuffix')}`
      }
      // Update document.title
      document.title = pageTitle
    },
    /**
     * Emit event on query param changes detected
     * @param {object} to - The values after changes
     * @param {object} from - The values before changes
     */
    emitOnQueryParamChange: function (to, from) {
      // Loop on params
      for (const key in this.queryParams) {
        const fromValue = from.query[key]
        const toValue = to.query[key]
        if (fromValue !== toValue) {
          this.$emit('query-param-changed', {
            param: key,
            value: toValue
          })
        }
      }
    },
    /**
     * Method to sync queryParams with another object reference
     * @param {object} ref - The ref to assign
     * @param {object} map - An object that describes how to map reference properties
     * @example
     * // Consider the following route: /our/route/path?param_a=true&param_b=Hello...
     * // "ref" is a reference to component local data object
     * this.syncQueryParamsWith(ref, {
     *   paramA: { query: 'param_a', default: false },
     *   paramB: { query: 'param_b', default: null },
     *   ...
     * })
     */
    syncQueryParamsWith (ref, map) {
      for (const key in map) {
        const queryParam = map[key].query
        const defaultValue = map[key].default
        const valueToAssign = isDef(this.queryParams[queryParam])
          ? this.queryParams[queryParam]
          : defaultValue
        // Set with reactivity
        this.$set(ref, key, valueToAssign)
      }
    }
  },
  beforeRouteEnter (to, from, next) {
    // called before the route that renders this component is confirmed.
    // does NOT have access to `this` component instance,
    // because it has not been created yet when this guard is called!
    next()
  },
  beforeRouteUpdate (to, from, next) {
    // called when the route that renders this component has changed.
    // This component being reused (by using an explicit `key`) in the new route or not doesn't change anything.
    // For example, for a route with dynamic params `/foo/:id`, when we
    // navigate between `/foo/1` and `/foo/2`, the same `Foo` component instance
    // will be reused (unless you provided a `key` to `<router-view>`), and this hook will be called when that happens.
    // has access to `this` component instance.
    next()
  },
  beforeRouteLeave (to, from, next) {
    // called when the route that renders this component is about to
    // be navigated away from.
    // has access to `this` component instance.
    next()
  },
  watch: {
    // Watch this.$route to dynamically update Page title (document.title)
    $route: function (to, from) {
      this.updatePageTitle(to, from)
      this.emitOnQueryParamChange(to, from)
    }
  },
  mounted () {
    // Update also at 'mounted' hook
    this.updatePageTitle(this.$route)
  }
}
