---
title: Service (notification-manager) options
headline: Service (notification-manager) options
sidebarTitle: Options
sidebarDepth: 0
prev: false
next: false
---

# Service (notification-manager) options

<a name="options">
</a>

## options

Same as NotificationManager constructor options.Configurable options:- `autoRegister`: `Core.config.services['notification-manager'].autoRegister`

