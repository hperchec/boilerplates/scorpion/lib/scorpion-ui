/**
 * @vuepress
 * ---
 * title: base64ToArrayBuffer
 * headline: base64ToArrayBuffer
 * sidebarTitle: base64ToArrayBuffer
 * ---
 */

// base64-arraybuffer-es6
import { decode } from 'base64-arraybuffer-es6'

/**
 * base64ToArrayBuffer
 * @param {string} str - The base64 encoded string
 * @param {object} [options] - See `base64-arraybuffer-es6` package documentation
 * @returns {ArrayBuffer} Returns the ArrayBuffer
 * @description
 * Convert an base64 encoded string to ArrayBuffer instance
 *
 * ::: tip See also
 * [base64-arraybuffer-es6](https://www.npmjs.com/package/base64-arraybuffer-es6) package documentation
 * :::
 * @example
 * base64ToArrayBuffer('Zm9vIGJhcg==') // => ArrayBuffer containing the string: 'foo bar'
 */
export const base64ToArrayBuffer = function (str, options) {
  return decode(str, options)
}

export default base64ToArrayBuffer
