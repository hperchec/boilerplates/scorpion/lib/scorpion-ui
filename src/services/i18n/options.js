/**
 * @vuepress
 * ---
 * title: "Service (i18n) options"
 * headline: "Service (i18n) options"
 * sidebarTitle: .options
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import Core from '@/Core'
import { isUndef } from '@/utils'
import toolbox from '@/toolbox'

const { makeConfigurableOption } = toolbox.services

/**
 * Cache for dynamic properties
 */
const _options = {
  messages: undefined
}

/**
 * @name options
 * @static
 * @description
 * Accepts all [VueI18n](https://kazupon.github.io/vue-i18n/api/#constructor-options) options plus the following:
 *
 * - `availableLocales`
 *
 * By default, `locale` and `fallbackLocale` options are set to `'en'`.
 *
 * The `messages` option contains messages defined in `Core.context.services['i18n'].messages` by default.
 * You can overwrite it by setting this option.
 *
 * Configurable options:
 *
 * - `availableLocales`: `Core.config.services['i18n'].availableLocales`
 * - `locale`: `Core.config.services['i18n'].defaultLocale`
 * - `fallbackLocale`: `Core.config.services['i18n'].fallbackLocale`
 *
 * ::: tip SEE ALSO
 * [VueI18n construction options](https://kazupon.github.io/vue-i18n/api/#constructor-options) documentation
 * :::
 */
const options = {
  /**
   * messages
   * @ignore
   */
  get messages () {
    /* eslint-disable dot-notation */
    // If messages option is not provided
    if (isUndef(_options.messages)) {
      return Core.context.services['i18n'].messages
    } else {
      return _options.messages
    }
  },
  set messages (value) {
    _options.messages = value
  }
}

/**
 * availableLocales
 * @alias options.availableLocales
 * @type {string[]}
 * @default ['en', 'fr']
 * @description
 * App supported languages (following the ISO 639-1 nomenclature)
 * See also the [@cospired/i18n-iso-languages package documentation](https://www.npmjs.com/package/@cospired/i18n-iso-languages) and
 * the [ISO 639-1 wikipedia page](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes)
 *
 * Can be overwritten via Core global configuration: `Core.config.services['i18n'].availableLocales`
 *
 * Default: `[ 'en', 'fr' ]`
 */
makeConfigurableOption(options, {
  propertyName: 'availableLocales',
  serviceIdentifier: 'i18n',
  configPath: 'availableLocales',
  defaultValue: [ 'en', 'fr' ]
})

/**
 * locale
 * @ignore
 */
makeConfigurableOption(options, {
  propertyName: 'locale',
  serviceIdentifier: 'i18n',
  configPath: 'defaultLocale',
  defaultValue: 'en'
})

/**
 * fallbackLocale
 * @ignore
 */
makeConfigurableOption(options, {
  propertyName: 'fallbackLocale',
  serviceIdentifier: 'i18n',
  configPath: 'fallbackLocale',
  defaultValue: 'en'
})

export default options
