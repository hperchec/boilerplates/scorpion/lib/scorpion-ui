/**
 * @vuepress
 * ---
 * title: setDateToMidnight
 * headline: setDateToMidnight
 * sidebarTitle: setDateToMidnight
 * ---
 */

/**
 * setDateToMidnight
 * @param {Date|string} value - The date to parse
 * @returns {Date} - Returns the date at midnight
 * @description
 * Returns timestamp
 * @example
 * const date = new Date('2023-01-01')
 * setDateToMidnight(date) // => 2023-01-01 00:00:00.000
 */
export const setDateToMidnight = function (value) {
  const tmp = new Date(value)
  tmp.setHours(0, 0, 0, 0)

  return tmp
}

export default setDateToMidnight
