---
title: __index__
headline: __index__
---

# __index__

<a name="setloadingstatus">
</a>

## SET\_LOADING\_STATUS(state, payload) ⇒ <code>void</code>

Mutate state.loadingStatus

| Param | Type | Description |
| --- | --- | --- |
| state | <code>object</code> | vuex store state |
| payload | <code>object</code> | Mutation payload |
| payload.name | <code>string</code> | Loading name |
| payload.status | <code>boolean</code> | Loading status |

