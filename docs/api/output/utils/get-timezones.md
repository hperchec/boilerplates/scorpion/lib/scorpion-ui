---
title: getTimezones
headline: getTimezones
sidebarTitle: getTimezones
---

# getTimezones

<a name="gettimezones">
</a>

## getTimezones ⇒ <code>Array.&lt;string&gt;</code>

Returns the supported values of Intl "timeZone"See https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Intl/supportedValuesOf

**Returns**: <code>Array.&lt;string&gt;</code> - - The supported values

**Example**

```js
getTimezones() // => [ 'Europe/Paris', ... ]
```

