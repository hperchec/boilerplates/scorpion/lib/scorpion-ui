/**
 * @vuepress
 * ---
 * title: "Service (notification-manager) configuration"
 * headline: "Service (notification-manager) configuration"
 * sidebarTitle: Configuration
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * ⚠ THIS FILE IS ONLY USED TO GENERATE SERVICE CONFIGURATION DOCUMENTATION
 */

/**
 * @name Configuration
 * @typicalname Core.config.services['notification-manager']
 * @description
 * Service configuration
 *
 * > Refers to `Core.config.services['notification-manager']`
 *
 * ```js
 * 'notification-manager': {
 *   // Overwrites service option: `autoRegister`
 *   autoRegister: Boolean
 * }
 * ```
 */
const config = { // eslint-disable-line no-unused-vars
  /**
   * @alias Configuration.autoRegister
   * @type {boolean}
   * @description
   * Auto register all error classes/constructors under `Core.context.support.errors`
   */
  autoRegister: true
}
