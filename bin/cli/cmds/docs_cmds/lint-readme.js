const juisy = require('@hperchec/juisy')
const path = require('path')

const {
  rootDir,
  run,
  abort
} = juisy.utils

// Exports command object
module.exports = {
  /**
   * Command syntax
   */
  command: 'lint:readme',
  /**
   * Aliases
   */
  aliases: [],
  /**
   * Command description
   */
  describe: 'Lint generated README file with markdownlint',
  /**
   * Builder
   * @param {object} yargs - yargs
   * @returns {object} Returns yargs
   */
  builder: function (yargs) {
    yargs.option('c', {
      alias: 'config',
      type: 'string',
      describe: 'Path to custom markdownlint config file (relative to root folder)',
      requiresArg: true
    })
    return yargs
  },
  /**
   * Handler
   * @param {object} argv - The argv
   * @returns {void}
   */
  handler: async function (argv) {
    const targetPath = './README.md' // relative to rootFolder
    const configPath = argv.config
      ? path.resolve(rootDir, argv.config)
      : path.resolve(rootDir, './docs/readme/.markdownlint-cli2.cjs') // default relative to root folder

    /**
     * Call markdownlint command
     */
    try {
      await run(
        'npx',
        [
          'markdownlint-cli2',
          '--config',
          configPath,
          targetPath
        ],
        { stdio: 'inherit' }
      )
    } catch (error) {
      abort(error.exitCode)
    }
  }
}
