declare namespace _default {
    let namespaced: boolean;
    let state: object;
    let getters: object;
    let actions: object;
    let mutations: object;
}
export default _default;
//# sourceMappingURL=index.d.ts.map