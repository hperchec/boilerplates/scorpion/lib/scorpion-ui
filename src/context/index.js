/**
 * @vuepress
 * ---
 * title: Core context
 * headline: Core context
 * sidebarTitle: Context
 * initialOpenGroupIndex: -1 # optional, defaults to 0, defines the index of initially opened subgroup
 * prev: ../config/
 * next: ./support/
 * ---
 */

import Core from '@/Core'
import support from './support'
import vue from './vue'

const services = new Proxy({}, {
  set (obj, prop, value, receiver) {
    if (receiver === Core) {
      obj[prop] = value
      return true
    } else {
      throw new Error('Use Core.registerService() method to register a service.')
    }
  }
})

/**
 * @typicalname Core.context
 * @description
 * > **Ref.** : {@link ../Core Core}.{@link ./ context}
 *
 * This is the Core context. The context object contains the app specificities.
 */
export const context = {
  /**
   * @default {}
   * @description
   * Contains the app data models (e.g. "User").
   */
  models: {},
  /**
   * @readonly
   * @type {Record<(keyof (typeof Core.config.services)), object>}
   * @see {@link ../services services}
   * @description
   * Contains the context services.
   */
  get services () {
    // @ts-ignore
    return services
  },
  /**
   * @see {@link ./support support}
   * @description
   * Contains the shared classes, functions, utilities...
   */
  support,
  /**
   * @see {@link ./vue vue}
   * @description
   * Contains all necessary Vue options, methods, mixins, etc...
   */
  vue
}

export default context
