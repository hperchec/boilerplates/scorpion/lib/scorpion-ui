/**
 * @vuepress
 * ---
 * title: "Service (toast-manager)"
 * headline: "Service (toast-manager)"
 * sidebarTitle: toast-manager
 * initialOpenGroupIndex: -1 # optional, defaults to 0, defines the index of initially opened subgroup
 * prev: ../
 * next: ./options
 * ---
 */

import Service from '@/Service'

import options from './options'
import components from './components'
import Toast from './Toast'
import Toaster from './Toaster'
import ToastManager from './ToastManager'

/* eslint-disable jsdoc/require-param, jsdoc/require-returns */

/**
 * @scorpion_ui_service {ToastManager} toast-manager
 * @description
 * The service "toast-manager" is for UI toast management.
 * It can creates "toasters" and then publish toasts.
 */
export default new Service({
  // Service definition
  service: {
    // Identifier
    identifier: 'toast-manager',
    /**
     * @alias register
     * @description
     * On registering, the service will inject properties to Vue root instance prototype `$app`:
     *
     * - `$app.ui.toast`
     * - `$app.ui.toaster`
     *
     * It will also define Vue components:
     *
     * - `Core.context.vue.components.toasts.BaseToast`
     * - `Core.context.vue.components.toasts.BaseToaster`
     * - `Core.context.vue.components.toasts.ExampleToaster`
     * - `Core.context.vue.components.toasts.Toast`
     */
    register: function (_Core) {
      // Register components
      _Core.context.vue.components.toasts = {
        BaseToast: components.BaseToast,
        BaseToaster: components.BaseToaster,
        ExampleToaster: components.ExampleToaster,
        Toast: components.Toast
      }
      // Define vue root instance prototype in '$app' namespace
      const $app = _Core.context.vue.root.prototype.app
      /**
       * @alias $app.ui.toast
       * @param {string} toasterName - Toaster name defined in ToastManager service
       * @param {object} toast - The Toast options
       * @returns {Toast} Returns the Toast instance
       * @description
       * Publish a toast in toasterName
       * @example
       * // In any component
       * this.$app.ui.toast('Mainframe', {
       *   content: 'Hello world!'
       * }) // => publish toast in  "Mainframe" toaster
       */
      $app.ui.toast = function (toasterName, toast) {
        const root = this // 'this' is the Vue root instance
        const toaster = root.$toastManager.toaster(toasterName)
        return toaster.publish(toast)
      }
      /**
       * @alias $app.ui.toaster
       * @param {...*} args - Same parameters as ToastManager.toaster method
       * @returns {Toaster} Returns the Toaster instance
       * @see {@link ./ToastManager#toastmanager-toaster ToastManager.toaster}
       * @description
       * Alias for ToastManager.toaster method
       * @example
       * // In any component
       * this.$app.ui.toaster(...)
       */
      $app.ui.toaster = function (...args) {
        const root = this // 'this' is the Vue root instance
        return root.$toastManager.toaster(...args)
      }
    },
    /**
     * @alias create
     * @description
     * Returns an instance of `ToastManager` created with options.
     */
    create: function (serviceOptions, { ToastManager: _ToastManager }, _Core) {
      // Return new ToastManager
      return new _ToastManager(serviceOptions)
    },
    /**
     * @alias plugin
     * @description
     * The following properties will be set to Vue prototype:
     *
     * ```js
     * Vue.prototype.$toastManager // => Core.service('toast-manager')
     * ```
     */
    vuePlugin: ({ ToastManager: _ToastManager }, _Core) => _ToastManager,
    // Vue plugin options
    pluginOptions: {},
    // rootOption
    rootOption: ({ ToastManager: _ToastManager }, _Core) => _ToastManager.rootOption
  },
  // On initialized
  onInitialized: (Core) => {
    // ...
  },
  // Options
  options,
  /**
   * @alias expose
   */
  expose: {
    /**
     * @see {@link ./Toast Toast}
     */
    Toast,
    /**
     * @see {@link ./Toaster Toaster}
     */
    Toaster,
    /**
     * @see {@link ./ToastManager ToastManager}
     */
    ToastManager
  }
})
