---
title: "Service (modal-manager): ModalManager class"
headline: "Service (modal-manager): ModalManager class"
sidebarTitle: .ModalManager
sidebarDepth: 0
prev: false
next: false
---

# Service (modal-manager): ModalManager class

<a name="modalmanager">
</a>

## ModalManager ⇐ <code>VuePluginable</code>

Service **modal-manager**: ModalManager class

**Extends**: <code>VuePluginable</code>  
**Schema**:

- [ModalManager](#modalmanager) ⇐ <code>VuePluginable</code>
  - [new ModalManager([options])](#newmodalmanagernew)
  - _instance_
    - [._useTransition](#modalmanager-usetransition)
    - [.reactive](#modalmanager-reactive)
    - [.toJSON()](#modalmanager-tojson) ⇒ <code>object</code>
    - _methods_
      - [.add(modal)](#modalmanager-add) ⇒ <code>module:services/modal-manager/ModalManager~ModalManager</code>
      - [.show(name, [props])](#modalmanager-show) ⇒ <code>module:services/modal-manager/Modal~Modal</code>
      - [.hide(name)](#modalmanager-hide) ⇒ <code>module:services/modal-manager/Modal~Modal</code>
      - [.isShown(name)](#modalmanager-isshown) ⇒ <code>boolean</code>
      - [.isHidden(name)](#modalmanager-ishidden) ⇒ <code>boolean</code>
    - _properties_
      - [.modals](#modalmanager-modals) : <code>object</code>
      - [.useTransition](#modalmanager-usetransition) : <code>object</code>
  - _static_
    - [.install(_Vue, [options])](#modalmanager-install)

<a name="newmodalmanagernew">
</a>

### new ModalManager([options])

Create a new instance

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [options] | <code>object</code> | <code>{}</code> | Constructor options |
| [options.modals] | <code>Array.&lt;module:services/modal-manager/Modal~Modal&gt;</code> |  | An array of Modal instances |
| [options.useTransition] | <code>object</code> |  | An object for Vue built-in `<transition-group>` component |

<a name="modalmanager-usetransition">
</a>

### modalManager.\_useTransition

Private properties

<a name="modalmanager-reactive">
</a>

### modalManager.reactive

Overrides static VuePluginable properties

<a name="modalmanager-tojson">
</a>

### modalManager.toJSON() ⇒ <code>object</code>

Overrides the default toJSON object method for JSON.stringify() calls

**Returns**: <code>object</code> - - The instance data to be serialized

<a name="modalmanager-add">
</a>

### modalManager.add(modal) ⇒ <code>module:services/modal-manager/ModalManager~ModalManager</code>

Add an Modal to manager anytime

**Returns**: <code>module:services/modal-manager/ModalManager~ModalManager</code> - Return the ModalManager instance itself

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| modal | <code>module:services/modal-manager/Modal~Modal</code> | The Modal to add to manager |

<a name="modalmanager-show">
</a>

### modalManager.show(name, [props]) ⇒ <code>module:services/modal-manager/Modal~Modal</code>

Show the modal named by `name`

**Returns**: <code>module:services/modal-manager/Modal~Modal</code> - Return the Modal instance itself

**Category**: methods  
| Param | Type | Default | Description |
| --- | --- | --- | --- |
| name | <code>string</code> |  | The Modal name to show |
| [props] | <code>object</code> | <code>{}</code> | The props to pass to the modal |

<a name="modalmanager-hide">
</a>

### modalManager.hide(name) ⇒ <code>module:services/modal-manager/Modal~Modal</code>

Hide the modal named by `name`

**Returns**: <code>module:services/modal-manager/Modal~Modal</code> - Return the Modal instance itself

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The Modal name to hide |

<a name="modalmanager-isshown">
</a>

### modalManager.isShown(name) ⇒ <code>boolean</code>

Know if a modal is shown

**Returns**: <code>boolean</code> - Return true if modal is shown

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The Modal name |

<a name="modalmanager-ishidden">
</a>

### modalManager.isHidden(name) ⇒ <code>boolean</code>

Know if a modal is hidden

**Returns**: <code>boolean</code> - Return true if modal is hidden

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The Modal name |

<a name="modalmanager-modals">
</a>

### modalManager.modals : <code>object</code>

Registered modals

**Category**: properties  
**Read only**: true  
<a name="modalmanager-usetransition">
</a>

### modalManager.useTransition : <code>object</code>

If it uses transition for ModalOverlay component

**Category**: properties  
**Read only**: true  
<a name="modalmanager-install">
</a>

### ModalManager.install(_Vue, [options])

Install method for Vue

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| _Vue | <code>Class.&lt;module:vue&gt;</code> |  | Vue |
| [options] | <code>object</code> | <code>{}</code> | The options of the plugin |

