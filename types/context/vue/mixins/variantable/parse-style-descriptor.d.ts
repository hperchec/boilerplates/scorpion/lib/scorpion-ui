export function parseStyleDescriptor(value: string | string[] | object, format?: string): object;
export function mergeStyleDescriptors(a: string | string[] | object, b: string | string[] | object, format?: string): string | string[] | object;
export default parseStyleDescriptor;
//# sourceMappingURL=parse-style-descriptor.d.ts.map