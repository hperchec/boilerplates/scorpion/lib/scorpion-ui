/**
 * @vuepress
 * ---
 * title: PrivateRoute class
 * headline: PrivateRoute class
 * sidebarTitle: .PrivateRoute
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import Route from './Route'

// utils
import { isDef } from '@/utils'

/**
 * @extends Route
 * @classdesc
 * Class that represents a *private* route
 *
 * - `route.meta.requiresAuth` => `true`
 */
class PrivateRoute extends Route { // eslint-disable-line dot-notation
  /**
   * @param {...*} args - Same arguments as parent class
   * @description
   * Create a PrivateRoute
   *
   * See also {@link ../Route Route}
   */
  constructor (...args) {
    // @ts-ignore
    super(...args)
    // Meta
    this.setMeta({
      requiresAuth: true
    })
  }

  /**
   * Accessors & mutators
   */

  /**
   * Override Route children accessor and mutator
   * @ignore
   */
  get children () {
    return this._children
  }

  set children (value) {
    // Can be undefined
    if (isDef(value)) {
      // Check type
      if (value instanceof Array) {
        // Assign
        // this._children = value
        this._children = value.map((child) => {
          return child instanceof this.constructor
            ? child
            // @ts-ignore
            : new this.constructor(child.path, child.name, child.component, child.options)
        })
      } else {
        // Else -> throw error
        throw new TypeError('PrivateRoute class: \'children\' property must be Array. ' + typeof value + ' received...')
      }
    }
  }

  /**
   * Methods
   */

  // ...
}

export default PrivateRoute
