import Core from '@/Core'
import { /* deepBind, */ isDef } from '@/utils'

/**
 * @typicalname Core.context.vue.mixins.global.GlobalMixin
 * @description
 * **Global** mixin
 *
 * All app components will inherit from this mixin.
 *
 * Inherited computed properties:
 * - all `this.$root.shared` properties are mapped
 */
export const GlobalMixin = {
  computed: {
    // ...
  },
  methods: {
    // ...
  },
  beforeCreate () {
    const vm = this
    // Inject all $root.shared as computed property
    const sharedData = vm.$root.shared
    if (isDef(sharedData)) {
      for (const prop in sharedData) {
        vm.$options.computed[prop] = function () {
          return sharedData[prop]
        }
      }
    }
    /**
     * Debug
     * @todo Document $debug tools
     */
    // Set debugging private property
    const $debug = {
      __v_descriptor__: {
        namespace: 'GlobalMixin',
        description: 'Access the component instance debug tools',
        optionType: 'nested',
        type: 'object',
        // @ts-ignore
        externalLink: __getDocUrl__('/api/context/vue/mixins/global/GlobalMixin')
      },
      info: function () {
        vm.$logger.consoleLog.group('Vue component info: (Click to expand 👆)', { type: 'vue', prod: false }, (subgroup) => {
          console.log('%cComponent properties', 'font-weight: bold; color: #41B883')
          // @ts-ignore
          console.log('%c⚠ Keys that start with "_" and native Vue properties are excluded', 'font-weight: bold; color: #41B883') // eslint-disable-line
          // Set aliases as temporary fix for auto-injected props by vue plugins like VueRouter or VueI18n
          const aliases = {
            $route: '$router',
            $d: '$i18n',
            $n: '$i18n',
            $t: '$i18n',
            $tc: '$i18n',
            $te: '$i18n'
          }
          const descriptors = Core.context.vue.getComponentDescriptor(vm)
          const data = {}
          const computed = {}
          const methods = {}
          const prototype = {}
          const other = {}
          for (const key in descriptors) {
            let descriptor
            // First, check if it is an alias
            if (key in aliases) {
              descriptor = descriptors[aliases[key]] || descriptors[key]
            } else {
              descriptor = descriptors[key]
            }
            // Then, sort
            if (descriptor.optionType === 'prototype' || descriptor.optionType === 'nested') {
              prototype[key] = descriptor
            } else if (key in (vm.$options.computed || {})) {
              computed[key] = descriptor
            } else if (key in (vm.$options.methods || {})) {
              methods[key] = descriptor
            } else if (key in (vm.$data || {})) {
              data[key] = descriptor
            } else {
              other[key] = descriptor
            }
          }
          const logDescriptor = (key, descriptor) => {
            console.groupCollapsed(
              `%c${key} %c<${descriptor.namespace || '?'}> %c${descriptor.description ? '/* ' + descriptor.description + ' */' : ''}`,
              'font-weight: bold;',
              'font-weight: normal; color: grey;',
              'font-style: italic; color: MediumPurple;'
            )
            console.log(`├─ Namespace    > %c${descriptor.namespace}`, 'color: grey;')
            console.log('├─ Option       >', descriptor.optionType)
            console.log('├─ Type         >', descriptor.type)
            console.log(`├─ Description  > %c${descriptor.description || ''}`, 'font-style: italic; color: MediumPurple;')
            console.log(`└─ See          > %c${descriptor.externalLink}`, 'font-style: italic; color: MediumSeaGreen;')
            console.groupEnd()
          }
          if (Object.keys(data).length) {
            console.log('%cData', 'font-style: bold; color: LightCoral;')
            for (const key in data) { logDescriptor(key, data[key]) }
          }
          if (Object.keys(computed).length) {
            console.log('%cComputed', 'font-style: bold; color: LightCoral;')
            for (const key in computed) { logDescriptor(key, computed[key]) }
          }
          if (Object.keys(methods).length) {
            console.log('%cMethods', 'font-style: bold; color: LightCoral;')
            for (const key in methods) { logDescriptor(key, methods[key]) }
          }
          if (Object.keys(prototype).length) {
            console.log('%cPrototype', 'font-style: bold; color: LightCoral;')
            for (const key in prototype) { logDescriptor(key, prototype[key]) }
          }
          if (Object.keys(other).length) {
            console.log('%cOther', 'font-style: bold; color: LightCoral;')
            for (const key in other) { logDescriptor(key, other[key]) }
          }
        })
      }
    }
    // Assign property
    Object.defineProperty(this, '$debug', {
      value: $debug
    })
  }
}

export default GlobalMixin
