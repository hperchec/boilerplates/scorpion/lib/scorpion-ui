---
title: Service (store) options
headline: Service (store) options
sidebarTitle: .options
sidebarDepth: 0
prev: false
next: false
---

# Service (store) options

<a name="options">
</a>

## .options

Accepts all [Vuex.Store](https://v3.vuex.vuejs.org/api/#vuex-store-constructor-options) constructor optionsBy default, the `modules` option is an empty Object. The modules defined in `Core.context.services['store'].modules` like `System` and `Resources`will be auto-injected at service creation.The `plugins` option is an empty array. At service creation, the `plugins` option will be a concatenated array of pluginsdefined in `Core.context.services['store'].plugins` and the `plugins` option array content.::: tip SEE ALSO[Vuex.Store constructor options documentation](https://v3.vuex.vuejs.org/api/#vuex-store-constructor-options):::

