export function makeConfigurableOption(target: object, options: {
    propertyName: string;
    serviceIdentifier: string;
    configPath: string;
    formatter?: Function;
    defaultValue: any;
}): void;
export default makeConfigurableOption;
//# sourceMappingURL=make-configurable-option.d.ts.map