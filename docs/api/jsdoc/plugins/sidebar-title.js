/**
 * Scorpion UI JSDoc plugin: To override the title of header auto-generated by vuepress
 * See also https://jsdoc.app/about-plugins.html
 */

// Plugin dictionary
exports.defineTags = function (dictionary) {
  // Define @sidebar_title: To override the title of header auto-generated by vuepress
  dictionary.defineTag('sidebar_title', {
    canHaveType: false,
    canHaveName: false,
    // Tag muyst have value -> service identifier
    mustHaveValue: true,
    // On tagged, we set doclet "sidebarTitle" property
    onTagged: function (doclet, tag) {
      doclet.sidebarTitle = tag.value
    }
  })
}
