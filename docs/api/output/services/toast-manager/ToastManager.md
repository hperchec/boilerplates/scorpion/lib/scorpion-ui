---
title: "Service (toast-manager): ToastManager class"
headline: "Service (toast-manager): ToastManager class"
sidebarTitle: .ToastManager
sidebarDepth: 0
prev: false
next: false
---

# Service (toast-manager): ToastManager class

<a name="toastmanager">
</a>

## ToastManager ⇐ <code>VuePluginable</code>

Service **toast-manager**: ToastManager class

**Extends**: <code>VuePluginable</code>  
**Schema**:

- [ToastManager](#toastmanager) ⇐ <code>VuePluginable</code>
  - [new ToastManager(options)](#newtoastmanagernew)
  - _instance_
    - [.reactive](#toastmanager-reactive)
    - [.toJSON()](#toastmanager-tojson) ⇒ <code>object</code>
    - _methods_
      - [.add(toaster)](#toastmanager-add) ⇒ <code>module:services/toast-manager/Toaster~Toaster</code>
      - [.toaster(name)](#toastmanager-toaster) ⇒ <code>module:services/toast-manager/Toaster~Toaster</code>
    - _properties_
      - [.toasters](#toastmanager-toasters) : <code>Array.&lt;module:services/toast-manager/Toaster~Toaster&gt;</code>
  - _static_
    - [.install(_Vue, [options])](#toastmanager-install)

<a name="newtoastmanagernew">
</a>

### new ToastManager(options)

Create a new Toaster Manager

| Param | Type | Description |
| --- | --- | --- |
| options | <code>object</code> | An array of Toaster |
| options.toasters | <code>Array.&lt;module:services/toast-manager/Toaster~Toaster&gt;</code> | An array of Toaster |

<a name="toastmanager-reactive">
</a>

### toastManager.reactive

Overrides static VuePluginable properties

<a name="toastmanager-tojson">
</a>

### toastManager.toJSON() ⇒ <code>object</code>

Overrides the default toJSON object method for JSON.stringify() calls

**Returns**: <code>object</code> - - The instance data to be serialized

<a name="toastmanager-add">
</a>

### toastManager.add(toaster) ⇒ <code>module:services/toast-manager/Toaster~Toaster</code>

Add a Toaster to manager anytime

**Returns**: <code>module:services/toast-manager/Toaster~Toaster</code> - Returns the Toaster

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| toaster | <code>module:services/toast-manager/Toaster~Toaster</code> | The Toaster to add to manager |

<a name="toastmanager-toaster">
</a>

### toastManager.toaster(name) ⇒ <code>module:services/toast-manager/Toaster~Toaster</code>

Use a specific Toaster that was loaded by the manager

**Returns**: <code>module:services/toast-manager/Toaster~Toaster</code> - Returns the Toaster

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The Toaster name to target |

<a name="toastmanager-toasters">
</a>

### toastManager.toasters : <code>Array.&lt;module:services/toast-manager/Toaster~Toaster&gt;</code>

Registered Toasters

**Category**: properties  
**Read only**: true  
<a name="toastmanager-install">
</a>

### ToastManager.install(_Vue, [options])

Install method for Vue

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| _Vue | <code>Vue</code> |  | Vue |
| [options] | <code>object</code> | <code>{}</code> | The options of the plugin |

