/**
 * @vuepress
 * ---
 * title: "Service (websocket-manager) options"
 * headline: "Service (websocket-manager) options"
 * sidebarTitle: Options
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import Core from '@/Core'
import toolbox from '@/toolbox'
import WSConnectionOptions from './WSConnectionOptions' // jsDoc type

const { makeConfigurableOption } = toolbox.services

/**
 * @name options
 * @static
 * @description
 * Accepts the following options:
 *
 * - `connections`
 *
 * Configurable options:
 *
 * - **connections**: `Core.config.services['websocket-manager'].connections`
 */
const options = {
  // ...
}

/**
 * @alias options.connections
 * @type {WSConnectionOptions[]}
 * @default []
 * @description
 * Array of WSConnectionOptions instance to pass to WebSocketManager constructor.
 *
 * > 🔧 Configurable option: `Core.config.services['websocket-manager'].connections`. See service {@link ./config configuration}
 */
makeConfigurableOption(options, {
  propertyName: 'connections',
  serviceIdentifier: 'websocket-manager',
  configPath: 'connections',
  formatter: (connectionDefs) => {
    const tmp = []
    const WSConnection = Core.context.services['websocket-manager'].WSConnection
    const _WSConnectionOptions = Core.context.services['websocket-manager'].WSConnectionOptions
    for (const name in connectionDefs) {
      // Create a new WSConnectionOptions
      const wsConnectionOptions = new _WSConnectionOptions(name, 'pusher', connectionDefs[name])
      // Add custom connections
      tmp.push(new WSConnection(wsConnectionOptions))
    }
    return tmp
  },
  defaultValue: []
})

export default options
