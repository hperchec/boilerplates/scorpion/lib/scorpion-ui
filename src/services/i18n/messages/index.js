/**
 * @vuepress
 * ---
 * title: Internationalization (i18n) - messages
 * headline: Internationalization (i18n) - messages
 * sidebarTitle: .messages
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import en from './en'
import fr from './fr'

/**
 * @name messages
 * @static
 * @type {object}
 * @description
 * ::: tip INFO
 * Please check the [internationalization](../../../../internationalization) documentation.
 * :::
 */
export default {
  en,
  fr
}
