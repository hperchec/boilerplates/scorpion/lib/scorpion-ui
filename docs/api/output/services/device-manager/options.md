---
title: Service (device-manager) options
headline: Service (device-manager) options
sidebarTitle: Options
sidebarDepth: 0
prev: false
next: false
---

# Service (device-manager) options

<a name="options">
</a>

## options

Device manager service options

