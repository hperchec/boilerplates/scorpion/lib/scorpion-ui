/**
 * @vuepress
 * ---
 * title: "AppError class"
 * headline: "AppError class"
 * sidebarTitle: "AppError class"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: ./
 * next: false
 * ---
 */

import { getAllKeys, typeCheck } from '@/utils'

/**
 * @classdesc
 * AppError class
 */
class AppError extends Error {
  /**
   * @private
   */
  _errorCode = 'e0000'
  _errorMessage = 'Unknown error'

  /**
   * Creates a AppError instance
   * @param {?Error|string} [original] - The original Error or error message
   */
  constructor (original) {
    if (!original) {
      original = new Error('An error has occured')
    } else if (typeof original === 'string') {
      original = new Error(original)
    }
    super(original.message)
    this.init()
  }

  /**
   * Properties
   */

  /**
   * The error name
   */
  name = 'AppError'

  /**
   * Accessors and mutators
   */

  /**
   * Error code
   * @category properties
   * @type {string}
   * @default "e0000"
   */
  get errorCode () {
    return this._errorCode
  }

  set errorCode (value) {
    // Check type
    if (typeCheck(String, value)) {
      // Assign
      this._errorCode = value
    } else {
      // Else -> throw error
      throw new Error('AppError class: \'errorCode\' property must be String. ' + typeof value + ' received...')
    }
  }

  /**
   * Error message
   * @category properties
   * @type {string}
   * @default "Unknown error"
   */
  get errorMessage () {
    return this._errorMessage
  }

  set errorMessage (value) {
    // Check type
    if (typeCheck(String, value)) {
      // Assign
      this._errorMessage = value
    } else {
      // Else -> throw error
      throw new Error('AppError class: \'errorMessage\' property must be String. ' + typeof value + ' received...')
    }
  }

  /**
   * Methods
   */

  /**
   * Init the error code and message
   * @returns {void}
   */
  init () {
    // @ts-ignore
    this.errorCode = this.constructor.errorCode
    // @ts-ignore
    this.errorMessage = this.constructor.getTranslatedError(this.errorCode)
  }

  /**
   * Overrides the default toJSON object method for JSON.stringify() calls
   * @returns {object} - The instance data to be serialized
   */
  toJSON () {
    const keys = getAllKeys(this, {
      excludeKeys: [
        /^_/, // private properties that starts with '_'
        'toJSON',
        'toString',
        'constructor'
      ]
    })
    const jsonObj = keys.reduce((obj, key) => {
      obj[key] = this[key]
      return obj
    }, {})
    return jsonObj
  }

  /**
   * Static properties
   */

  /**
   * @category static properties
   * @static
   * @memberof AppError
   * @type {string}
   * @description
   * The error code
   */
  static errorCode = 'e0000'
}

export default AppError
