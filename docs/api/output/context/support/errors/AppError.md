---
title: AppError class
headline: AppError class
sidebarTitle: AppError class
sidebarDepth: 0
prev: ./
next: false
---

# AppError class

<a name="apperror">
</a>

## AppError ⇐ <code>Error</code>

AppError class

**Extends**: <code>Error</code>  
**Schema**:

- [AppError](#apperror) ⇐ <code>Error</code>
  - [new AppError([original])](#newapperrornew)
  - _instance_
    - [.name](#apperror-name)
    - [.init()](#apperror-init) ⇒ <code>void</code>
    - [.toJSON()](#apperror-tojson) ⇒ <code>object</code>
    - _properties_
      - [.errorCode](#apperror-errorcode) : <code>string</code>
      - [.errorMessage](#apperror-errormessage) : <code>string</code>
  - _static_
    - _static properties_
      - [.errorCode](#apperror-errorcode) : <code>string</code>

<a name="newapperrornew">
</a>

### new AppError([original])

Creates a AppError instance

| Param | Type | Description |
| --- | --- | --- |
| [original] | <code>Error</code> \| <code>string</code> | The original Error or error message |

<a name="apperror-name">
</a>

### appError.name

The error name

<a name="apperror-init">
</a>

### appError.init() ⇒ <code>void</code>

Init the error code and message

<a name="apperror-tojson">
</a>

### appError.toJSON() ⇒ <code>object</code>

Overrides the default toJSON object method for JSON.stringify() calls

**Returns**: <code>object</code> - - The instance data to be serialized

<a name="apperror-errorcode">
</a>

### appError.errorCode : <code>string</code>

Error code

**Default**: <code>&quot;\&quot;e0000\&quot;&quot;</code>  
**Category**: properties  
<a name="apperror-errormessage">
</a>

### appError.errorMessage : <code>string</code>

Error message

**Default**: <code>&quot;\&quot;Unknown error\&quot;&quot;</code>  
**Category**: properties  
<a name="apperror-errorcode">
</a>

### AppError.errorCode : <code>string</code>

The error code

**Category**: static properties  
