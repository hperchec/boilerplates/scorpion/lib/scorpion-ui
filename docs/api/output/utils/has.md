---
title: has
headline: has
sidebarTitle: has
---

# has

<a name="has">
</a>

## has ⇒ <code>boolean</code>

Define is object has property (lodash.has).::: tip See also[lodash.has](https://www.npmjs.com/package/lodash.has) package documentation:::

**Returns**: <code>boolean</code> - Returns true if object has property defined

| Param | Type | Description |
| --- | --- | --- |
| object | <code>object</code> | The object to check |
| path | <code>Array</code> \| <code>string</code> | The path to check |

**Example**

```js
const object = { 'a': [{ 'b': { 'c': 3 } }] }has(object, 'a[0].b.c') // => true
```

