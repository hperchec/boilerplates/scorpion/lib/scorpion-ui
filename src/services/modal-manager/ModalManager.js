/**
 * @vuepress
 * ---
 * title: "Service (modal-manager): ModalManager class"
 * headline: "Service (modal-manager): ModalManager class"
 * sidebarTitle: .ModalManager
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import Core from '@/Core'
import { getAllKeys, isUndef, typeCheck } from '@/utils'
import toolbox from '@/toolbox'

import Modal from './Modal'

const { VuePluginable } = toolbox.vue

/**
 * @extends VuePluginable
 * @classdesc
 * Service **modal-manager**: ModalManager class
 */
class ModalManager extends VuePluginable {
  /**
   * Private properties
   */
  _useTransition

  /**
   * Create a new instance
   * @param {object} [options = {}] - Constructor options
   * @param {Modal[]} [options.modals] - An array of Modal instances
   * @param {object} [options.useTransition] - An object for Vue built-in `<transition-group>` component
   */
  constructor (options = {}) {
    // Call VuePluginable constructor
    const initVMData = {
      modals: {}
    }
    super(initVMData)
    const self = this
    // Set debugging private property
    if (self.__v_descriptor__) {
      self.__v_descriptor__.namespace = 'service:modal-manager'
      self.__v_descriptor__.description = 'Access the ModalManager instance'
      // @ts-ignore
      self.__v_descriptor__.externalLink = __getDocUrl__('/api/services/modal-manager/ModalManager')
      // Set debugging info for modals
      Object.defineProperty(self.modals, '__v_descriptor__', {
        get () {
          return {
            namespace: 'service:modal-manager',
            description: 'Access the defined modals',
            optionType: 'prototype',
            type: 'object',
            // @ts-ignore
            externalLink: __getDocUrl__('/api/services/modal-manager/ModalManager')
          }
        },
        enumerable: false
      })
    }
    // Process options.modals
    options.modals = options.modals || []
    for (const modal of options.modals) {
      self.add(modal)
    }
    // Process useTransition option
    self._useTransition = options.useTransition || {}
    // Injects custom scoped slot to root App component
    self.#wrapRootComponentRender()
  }

  /**
   * Overrides static VuePluginable properties
   */
  static reactive = true
  static rootOption = 'modalManager'
  static aliases = [
    // 'modals' // it's a trick at plugin install method! see below
  ]

  /**
   * Accessors & mutators
   */

  /**
   * Registered modals
   * @category properties
   * @readonly
   * @type {object}
   */
  get modals () {
    return this._vm.modals
  }

  /**
   * If it uses transition for ModalOverlay component
   * @category properties
   * @readonly
   * @type {object}
   */
  get useTransition () {
    return this._useTransition
  }

  /**
   * Methods
   */

  /**
   * add
   * @category methods
   * @param {Modal} modal - The Modal to add to manager
   * @returns {ModalManager} Return the ModalManager instance itself
   * @description
   * Add an Modal to manager anytime
   */
  add (modal) {
    // Check if item is a instance of Modal
    if (typeCheck(Core.context.services['modal-manager'].Modal, modal)) {
      // Check in registered modals
      const modalExists = this.modals[modal.name]
      if (!modalExists) {
        modal._manager = this
        this._vm.$set(this._vm.modals, modal.name, {
          show: false,
          modalInstance: modal
        })
      } else {
        console.log('ModalManager can\'t add Modal: "' + modal.name + '" is already set...')
      }
    } else {
      throw new Error('ModalManager can\'t load Modal. One of them is not a Modal instance')
    }
    return this
  }

  /**
   * show
   * @category methods
   * @param {string} name - The Modal name to show
   * @param {object} [props = {}] - The props to pass to the modal
   * @returns {Modal} Return the Modal instance itself
   * @description
   * Show the modal named by `name`
   */
  show (name, props = {}) {
    // Check in registered modals
    const modal = this.modals[name]
    if (modal) {
      if (isUndef(props)) {
        props = {}
      } else {
        // Check if props is an object
        if (!typeCheck(Object, props)) {
          throw new Error('ModalManager: props must be object, "' + typeof props + '" received')
        }
      }
      // Assign props
      for (const key in props) {
        modal.modalInstance.props[key] = props[key]
      }
      // Show modal
      modal.show = true
    } else {
      throw new Error('ModalManager: unable to show unknown modal: ' + name)
    }
    return modal
  }

  /**
   * hide
   * @category methods
   * @param {string} name - The Modal name to hide
   * @returns {Modal} Return the Modal instance itself
   * @description
   * Hide the modal named by `name`
   */
  hide (name) {
    // Check in registered modals
    const modal = this.modals[name]
    if (modal) {
      // Hide modal
      modal.show = false
    } else {
      throw new Error('ModalManager: unable to hide unknown modal: ' + name)
    }
    return modal
  }

  /**
   * isShown
   * @category methods
   * @param {string} name - The Modal name
   * @returns {boolean} Return true if modal is shown
   * @description
   * Know if a modal is shown
   */
  isShown (name) {
    // Check in registered modals
    const modal = this.modals[name]
    if (modal) {
      return modal.show === true
    } else {
      throw new Error('ModalManager: unknown modal: ' + name)
    }
  }

  /**
   * isHidden
   * @category methods
   * @param {string} name - The Modal name
   * @returns {boolean} Return true if modal is hidden
   * @description
   * Know if a modal is hidden
   */
  isHidden (name) {
    // Check in registered modals
    const modal = this.modals[name]
    if (modal) {
      return modal.show === false
    } else {
      throw new Error('ModalManager: unknown modal: ' + name)
    }
  }

  /**
   * Overrides the default toJSON object method for JSON.stringify() calls
   * @returns {object} - The instance data to be serialized
   */
  toJSON () {
    const keys = getAllKeys(this, {
      excludeKeys: [
        /^_/, // private properties that starts with '_'
        'toJSON',
        'toString',
        'constructor'
      ]
    })
    const jsonObj = keys.reduce((obj, key) => {
      obj[key] = this[key]
      return obj
    }, {})
    return jsonObj
  }

  /**
   * Private methods
   */

  /**
   * Wrap the root component render method to injects custom scopedSlot as last children of element
   * @returns {void}
   */
  #wrapRootComponentRender () {
    const self = this
    // Wrap the default root render
    const defaultRootRender = Core.context.vue.root.options.render
    Core.context.vue.root.options.render = function (createElement) {
      const rootElement = defaultRootRender.call(self, (appElement, data, children) => {
        return createElement(appElement, {
          ...(data || {}),
          scopedSlots: {
            ...(data.scopedSlots || {}),
            /**
             * Define custom slot for service at root element level
             * @ignore
             */
            'service:modal-manager@modal_container': function (props) {
              // Default creates a div
              return createElement(
                Core.context.vue.components.modals.ModalContainer,
                {
                  attrs: {
                    id: 'service:modal-manager@modal_container'
                  },
                  props
                }
              )
            }
          }
        }, children)
      })
      return rootElement
    }
    // Overrides the main app component render method
    const defaultAppComponentRender = Core.context.vue.components.App.render
    Core.context.vue.components.App.render = function (createElement) {
      // Call the default component render method
      const appElement = defaultAppComponentRender.call(this, createElement)
      // Append to children array our custom scopedSlot
      appElement.children.push(appElement.context.$scopedSlots['service:modal-manager@modal_container']({
        useTransition: self.useTransition
      })[0])
      // Return the appElement
      return appElement
    }
  }

  /**
   * Static
   */

  /**
   * Install method for Vue
   * @param {typeof import("vue")} _Vue - Vue
   * @param {object} [options = {}] - The options of the plugin
   */
  static install (_Vue, options = {}) {
    super.install(_Vue)
    // Check if key is already defined
    // @ts-ignore
    if (_Vue.prototype.hasOwnProperty('$modals')) { // eslint-disable-line no-prototype-builtins
      throw new Error('Error when installing "ModalManager": Vue.prototype.$modals is already defined...')
    }
    // Define the getter on the 'private' property prefixed by '$'
    // @ts-ignore
    Object.defineProperty(_Vue.prototype, '$modals', {
      get () { return this._modalManager.modals }
    })
  }
}

export default ModalManager
