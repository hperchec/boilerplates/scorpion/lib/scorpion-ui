/**
 * @vuepress
 * ---
 * title: "Service (event-bus)"
 * headline: "Service (event-bus)"
 * sidebarTitle: event-bus
 * initialOpenGroupIndex: -1 # optional, defaults to 0, defines the index of initially opened subgroup
 * prev: ../
 * next: ./options
 * ---
 */

import Service from '@/Service'

import options from './options'
import EventBus from './EventBus'

/* eslint-disable jsdoc/require-param, jsdoc/require-returns */

/**
 * @scorpion_ui_service {EventBus} event-bus
 * @description
 * The service "event-bus" is a app level event bus.
 */
export default new Service({
  // Service
  service: {
    // Identifier
    identifier: 'event-bus',
    /**
     * @alias register
     * @description
     * On registering, the service will inject properties to Vue root instance prototype `$app`:
     *
     * - `$app.emit`
     * - `$app.off`
     * - `$app.on`
     * - `$app.once`
     */
    register: function (_Core) {
      // Define vue root instance prototype in '$app' namespace
      const $app = _Core.context.vue.root.prototype.app
      /**
       * @alias $app.emit
       * @param {...*} args - Same parameters as EventBus.emit method
       * @returns {void}
       * @see {@link ./EventBus#eventbus-emit EventBus.emit}
       * @description
       * Emit event. If `strictMode` is true, will throw an error
       * if no listener is defined for the event key.
       * @example
       * // In any component
       * this.$app.emit('my-custom-event')
       */
      $app.emit = function (...args) {
        const root = this // 'this' is the Vue root instance
        return root.$eventBus.emit(...args)
      }
      /**
       * @alias $app.off
       * @param {...*} args - Same parameters as EventBus.off method
       * @returns {void}
       * @see {@link ./EventBus#eventbus-off EventBus.off}
       * @description
       * Delete event listener
       * @example
       * // In any component
       * this.$app.off('my-custom-event')
       */
      $app.off = function (...args) {
        const root = this // 'this' is the Vue root instance
        return root.$eventBus.off(...args)
      }
      /**
       * @alias $app.on
       * @param {...*} args - Same parameters as EventBus.on method
       * @returns {void}
       * @see {@link ./EventBus#eventbus-on EventBus.on}
       * @description
       * Create event listener
       * @example
       * // In any component
       * this.$app.on('my-custom-event', function () { ... })
       */
      $app.on = function (...args) {
        const root = this // 'this' is the Vue root instance
        return root.$eventBus.on(...args)
      }
      /**
       * @alias $app.once
       * @param {...*} args - Same parameters as EventBus.once method
       * @returns {void}
       * @see {@link ./EventBus#eventbus-once EventBus.once}
       * @description
       * Create a one-time event listener
       * @example
       * // In any component
       * this.$app.once('my-custom-event', function () { ... })
       */
      $app.once = function (...args) {
        const root = this // 'this' is the Vue root instance
        return root.$eventBus.once(...args)
      }
    },
    /**
     * @alias create
     * @description
     * Under the ground, service uses an instance of Vue to manage events.
     *
     * Returns an instance of `EventBus` created with options.
     */
    create: (serviceOptions, { EventBus: _EventBus }, _Core) => {
      // Return new EventBus
      return new _EventBus(serviceOptions)
    },
    /**
     * @alias plugin
     * @description
     * The following properties will be set to Vue prototype:
     *
     * ```js
     * Vue.prototype.$eventBus // => Core.service('event-bus')
     * ```
     */
    vuePlugin: ({ EventBus: _EventBus }, _Core) => _EventBus,
    // Vue plugin options
    pluginOptions: {},
    // rootOption
    rootOption: ({ EventBus: _EventBus }, _Core) => _EventBus.rootOption
  },
  // On initialized
  onInitialized: (Core) => {
    // ...
  },
  // Options
  options,
  /**
   * @alias expose
   */
  expose: {
    /**
     * @see {@link ./EventBus EventBus}
     */
    EventBus
  }
})
