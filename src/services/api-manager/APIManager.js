/**
 * @vuepress
 * ---
 * title: "Service (api-manager): APIManager class"
 * headline: "Service (api-manager): APIManager class"
 * sidebarTitle: .APIManager
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import Core from '@/Core'
import { getAllKeys, typeCheck } from '@/utils'
import toolbox from '@/toolbox'
import API from './API' // jsDoc type

const { VuePluginable } = toolbox.vue

/**
 * @extends VuePluginable
 * @classdesc
 * Service **api-manager**: APIManager class
 */
class APIManager extends VuePluginable {
  /**
   * Create a new API Manager
   * @param {object} options - Constructor options
   * @param {API[]} options.apis - An array of API
   */
  constructor (options) {
    const tmpApis = []
    // Loop on received options
    options.apis.forEach((api) => {
      // Check if item is a instance of API
      const _API = Core.context.services['api-manager'].API
      if (typeCheck(_API, api)) {
        tmpApis.push(api)
      } else {
        throw new Error('APIManager can\'t load API. One of them is not an API instance')
      }
    })
    const initVMData = {
      apis: tmpApis
    }
    // Call VuePluginable constructor
    super(initVMData)
    // Set debugging private property
    if (this.__v_descriptor__) {
      this.__v_descriptor__.namespace = 'service:api-manager'
      this.__v_descriptor__.description = 'Access the APIManager instance'
      // @ts-ignore
      this.__v_descriptor__.externalLink = __getDocUrl__('/api/services/api-manager/APIManager')
    }
  }

  /**
   * Overrides static VuePluginable properties
   */
  static reactive = true
  static rootOption = 'apiManager'
  static aliases = []

  /**
   * Accessors & mutators
   */

  /**
   * Registered APIs
   * @category properties
   * @readonly
   * @type {API[]}
   */
  get apis () {
    return this._vm.apis
  }

  /**
   * Methods
   */

  /**
   * add
   * @category methods
   * @param {API} api - The API to add to manager
   * @returns {API} Return this.use(<api.name>)
   * @description
   * Add an API to manager anytime
   */
  add (api) {
    // Check if item is a instance of API
    const _API = Core.context.services['api-manager'].API
    if (typeCheck(_API, api)) {
      // Check in registered APIs
      const apiExists = this.apis.find((a) => { return api.name === a.name })
      if (!apiExists) {
        this._vm.apis.push(api)
      } else {
        console.log('APIManager can\'t add API: "' + api.name + '" is already set...')
      }
    } else {
      throw new Error('APIManager can\'t load API. One of them is not an API instance')
    }
    return this.use(api.name)
  }

  /**
   * use
   * @category methods
   * @param {string} apiName - The API name to target
   * @returns {API} Return the API
   * @description
   * Use a specific API that was loaded by the manager
   */
  use (apiName) {
    // Check arg type
    if (!typeCheck(String, apiName)) {
      throw new TypeError('APIManager service : "use" method attempts String type for "apiName" parameter. Received : ' + typeof apiName)
    }
    // Find by name
    const api = this.apis.find(e => e.name === apiName)
    // If found
    if (api) {
      return api
    } else {
      // Not found -> throw Error
      throw new Error('APIManager service : "use" method can\'t find API by name "' + apiName + '"')
    }
  }

  /**
   * Methods
   */

  /**
   * Overrides the default toJSON object method for JSON.stringify() calls
   * @returns {object} - The instance data to be serialized
   */
  toJSON () {
    const keys = getAllKeys(this, {
      excludeKeys: [
        /^_/, // private properties that starts with '_'
        'toJSON',
        'toString',
        'constructor'
      ]
    })
    const jsonObj = keys.reduce((obj, key) => {
      obj[key] = this[key]
      return obj
    }, {})
    return jsonObj
  }

  /**
   * Static
   */

  /**
   * Install method for Vue
   * @param {Vue} _Vue - Vue
   * @param {object} [options = {}] - The options of the plugin
   */
  static install (_Vue, options = {}) {
    const Vue = _Vue
    super.install(Vue)
  }
}

export default APIManager
