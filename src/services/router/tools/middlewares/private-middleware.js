import Core from '@/Core'

/**
 * Creates a private middleware: only private routes
 * @param {Function} middlewareFunc - The middleware function
 * @param {string} [metaKey = 'requiresAuth'] - The meta key
 * @returns {Function} - The encapsulated function
 */
export const privateMiddleware = (middlewareFunc, metaKey = 'requiresAuth') => {
  // Returns a closure that encapsulates the passed custom middleware function
  return async (to, from, next) => {
    // check if private
    if (to.matched.some(record => record.meta[metaKey])) {
      const authUser = Core.service('auth-manager').getCurrentUser()
      // Pass payload with authenticated user
      await middlewareFunc(to, from, next, { authUser })
    } else {
      next()
    }
  }
}

export default privateMiddleware
