/**
 * @vuepress
 * ---
 * title: minBy
 * headline: minBy
 * sidebarTitle: minBy
 * ---
 */

// Lodash.minby
import lodashMinBy from 'lodash.minby'

/**
 * minBy
 * @param {Array} array - The array
 * @param {Function} sortFct - The sort function
 * @returns {*} Returns the found value in `array`
 * @description
 * Find min by in array (lodash.minby).
 *
 * ::: tip See also
 * [lodash.minby](https://www.npmjs.com/package/lodash.minby) package documentation
 * :::
 * @example
 * const array = [ { id: 1, name: 'John' }, { id: 2, name: 'Jane' } ]
 * minBy(array, function (item) { return item.id }) // Return -> { id: 1, name: 'John' }
 * minBy(array, function (item) { return item.name }) // Return -> { id: 2, name: 'Jane' }
 */
export const minBy = function (array, sortFct) {
  return lodashMinBy(array, sortFct)
}

export default minBy
