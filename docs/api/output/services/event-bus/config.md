---
title: Service (event-bus) configuration
headline: Service (event-bus) configuration
sidebarTitle: Configuration
prev: false
next: false
---

# Service (event-bus) configuration

<a name="configuration">
</a>

## Configuration

Service configuration> Refers to `Core.config.services['event-bus']````js'event-bus': {  // Enables the strict mode.  strictMode: Boolean}```

<a name="configuration-strictmode">
</a>

### Core.config.services[&#x27;event-bus&#x27;].strictMode : <code>boolean</code>

Define if EventBus throw Error when emitting event which has no listener defined.

