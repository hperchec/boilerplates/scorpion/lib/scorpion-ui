---
title: create-module
headline: create-module
---

# create-module

## Functions

<dl>
<dt><a href="#mergeState">mergeState(generated, raw)</a> ⇒ <code>object</code></dt>
<dd><p>Merge state</p>
</dd>
<dt><a href="#mergeGetters">mergeGetters(generated, raw)</a> ⇒ <code>object</code></dt>
<dd><p>Merge getters</p>
</dd>
<dt><a href="#mergeActions">mergeActions(generated, raw)</a> ⇒ <code>object</code></dt>
<dd><p>Merge actions</p>
</dd>
<dt><a href="#mergeMutations">mergeMutations(generated, raw)</a> ⇒ <code>object</code></dt>
<dd><p>Merge mutations</p>
</dd>
</dl>

<a name="mergestate">
</a>

## mergeState(generated, raw) ⇒ <code>object</code>

Merge state

**Returns**: <code>object</code> - The merged state

| Param | Type | Description |
| --- | --- | --- |
| generated | <code>object</code> | The generated state from generateState method |
| raw | <code>object</code> | The raw state from user |

<a name="mergegetters">
</a>

## mergeGetters(generated, raw) ⇒ <code>object</code>

Merge getters

**Returns**: <code>object</code> - The merged getters

| Param | Type | Description |
| --- | --- | --- |
| generated | <code>object</code> | The generated getters from generateGetters method |
| raw | <code>object</code> | The raw getters from user |

<a name="mergeactions">
</a>

## mergeActions(generated, raw) ⇒ <code>object</code>

Merge actions

**Returns**: <code>object</code> - The merged actions

| Param | Type | Description |
| --- | --- | --- |
| generated | <code>object</code> | The generated actions from generateGetters method |
| raw | <code>object</code> | The raw actions from user |

<a name="mergemutations">
</a>

## mergeMutations(generated, raw) ⇒ <code>object</code>

Merge mutations

**Returns**: <code>object</code> - The merged mutations

| Param | Type | Description |
| --- | --- | --- |
| generated | <code>object</code> | The generated mutations from generateGetters method |
| raw | <code>object</code> | The raw mutations from user |

