/**
 * @vuepress
 * ---
 * title: Vue
 * headline: Vue
 * sidebarTitle: Vue
 * initialOpenGroupIndex: -1 # optional, defaults to 0, defines the index of initially opened subgroup
 * prev: ../
 * next: ./components/
 * ---
 */

import components from './components'
import directives from './directives'
import ensureComponentConstructor from './ensure-component-constructor'
import getComponentDescriptor from './get-component-descriptor'
import filters from './filters'
import mixins from './mixins'
import plugins from './plugins'
import root from './root'

/**
 * @typicalname Core.context.vue
 * @description
 * Contains all relative Vue data (components, mixins, etc...)
 */
export const vue = {
  /**
   * @alias vue.components
   * @see {@link ./components components}
   */
  components,
  /**
   * @alias vue.directives
   * @see {@link ./directives directives}
   */
  directives,
  /**
   * @alias vue.ensureComponentConstructor
   * @see {@link ./ensureComponentConstructor ensureComponentConstructor}
   */
  ensureComponentConstructor,
  /**
   * @alias vue.getComponentDescriptor
   * @see {@link ./getComponentDescriptor getComponentDescriptor}
   */
  getComponentDescriptor,
  /**
   * @alias vue.filters
   * @see {@link ./filters filters}
   */
  filters,
  /**
   * @alias vue.mixins
   * @see {@link ./mixins mixins}
   */
  mixins,
  /**
   * @alias vue.plugins
   * @type {typeof plugins}
   * @see {@link ./plugins plugins}
   */
  get plugins () {
    return plugins
  },
  /**
   * @alias vue.root
   * @type {object}
   * @see {@link ./root root}
   */
  root
}

export default vue
