/**
 * @vuepress
 * ---
 * title: "Collection class"
 * headline: "Collection class"
 * sidebarTitle: "Collection class"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: ./
 * next: false
 * ---
 */

import {
  maxBy,
  minBy,
  orderBy,
  sortBy,
  typeCheck
} from '@/utils'
// Hook class
import toolbox from '@/toolbox'
const { Hook } = toolbox

/**
 * @typicalname Core.context.support.collection.Collection
 * @classdesc
 * > **Ref.** : {@link ../../Core Core}.{@link ../ support}.{@link ./ collection}.{@link ./Collection Collection}
 */
class Collection {
  /**
   * Create a Collection
   * @param {Array} [data = []] - An array of any types
   * @param {object} [options] - An options object
   * @param {Function} [options.findBy] - How to find an item in the collection
   * @param {Function|Function[]} [options.beforePush] - The 'beforePush' hook function(s).
   * Can be an array of functions. See also `onBeforePush` method.
   * @param {Function|Function[]} [options.afterPush] - The 'afterPush' hook function(s).
   * Can be an array of functions. See also `onAfterPush` method.
   * @param {Function|Function[]} [options.beforeDelete] - The 'beforeDelete' hook function(s).
   * Can be an array of functions. See also `onBeforeDelete` method.
   * @param {Function|Function[]} [options.afterDelete] - The 'afterDelete' hook function(s).
   * Can be an array of functions. See also `onAfterDelete` method.
   * @param {Function|Function[]} [options.beforeReplace] - The 'beforeReplace' hook function(s).
   * Can be an array of functions. See also `onBeforeReplace` method.
   * @param {Function|Function[]} [options.afterReplace] - The 'afterReplace' hook function(s).
   * Can be an array of functions. See also `onAfterReplace` method.
   * @param {boolean} [options.lock] - The collection "lock" option
   */
  constructor (data = [], options = {}) {
    // @ts-ignore
    if (options.findBy) this.findBy = options.findBy
    this.onBeforePush(options.beforePush)
    this.onAfterPush(options.afterPush)
    this.onBeforeDelete(options.beforeDelete)
    this.onAfterDelete(options.afterDelete)
    this.onBeforeReplace(options.beforeReplace)
    this.onAfterReplace(options.afterReplace)
    // Set items
    this.setItems(data)
    // Lock ?
    if (options.lock) this.lock()
  }

  /**
   * Public properties
   */

  /**
   * @category properties
   * @readonly
   * @type {Array}
   * @description
   * Collection items accessor
   */
  get items () {
    return this._items
  }

  set items (value) {
    // Return error
    throw new Error('Core class "Collection": "items" property cannot be directly set, use "setItems" collection method instead.')
  }

  /**
   * @category properties
   * @readonly
   * @type {number}
   * @description
   * Collection last index accessor
   */
  get lastIndex () {
    return this.items.length - 1
  }

  /**
   * Public methods
   */

  /**
   * lock
   * @category methods
   * @returns {Collection} Returns the collection
   * @description
   * Lock the collection
   */
  lock () {
    this._locked = true
    return this
  }

  /**
   * freeze
   * @category methods
   * @returns {Collection} Returns the collection
   * @description
   * Alias of {@link Collection#lock lock}
   */
  freeze () {
    return this.lock()
  }

  /**
   * unlock
   * @category methods
   * @returns {Collection} Returns the collection
   * @description
   * Unlock the collection
   */
  unlock () {
    this._locked = false
    return this
  }

  /**
   * unfreeze
   * @category methods
   * @returns {Collection} Returns the collection
   * @description
   * Alias of {@link Collection#unlock unlock}
   */
  unfreeze () {
    return this.unlock()
  }

  /**
   * isLocked
   * @category methods
   * @returns {boolean} Returns true if collection is locked
   * @description
   * If collection is locked.
   */
  isLocked () {
    return this._locked
  }

  /**
   * abortIfLocked
   * @category methods
   * @returns {void}
   * @throws Error
   */
  abortIfLocked () {
    if (this.isLocked()) {
      throw new Error('Core class "Collection": collection is currently locked.')
    }
  }

  /**
   * findBy
   * @category methods
   * @param {*} key - The key
   * @param {Array} array - The collection items
   * @returns {*} Returns the found collection item
   * @description
   * Find an item by key in the collection
   */
  findBy (key, array) {
    // By default, we search in the collection by Array index
    return array[key]
  }

  /**
   * setItems
   * @category methods
   * @param {Array} items - Items to set
   * @returns {void}
   * @description
   * Set collection items
   */
  setItems (items) {
    this.abortIfLocked()
    if (!typeCheck(Array, items)) {
      throw new Error(`"Collection" constructor first paramater expects to be an Array. "${typeof items}" received.`)
    }
    this._items.push(...items)
  }

  /**
   * get
   * @category methods
   * @returns {Array} Return collection items
   * @description
   * Get collection items (same as `all()` method)
   */
  get () {
    return this.items
  }

  /**
   * all
   * @category methods
   * @returns {Array} Return collection items
   * @description
   * Get collection items (same as `get()` method)
   */
  all () {
    return this.items
  }

  /**
   * find
   * @category methods
   * @param {Function|number|string} key - The key to find item. If function provided, execute this function to find item (same as Array.find function)
   * @returns {*} Returns the found collection item
   * @description
   * Find an item by key
   */
  find (key) {
    return typeCheck(Function, key)
      // @ts-ignore
      ? this.items.find(key)
      : this.findBy(key, this.items)
  }

  /**
   * findWithIndex
   * @category methods
   * @param {Function|number|string} key - The key to find item. If function provided, execute this function to find item (same as Array.find function)
   * @returns {*} Returns the found collection item
   * @description
   * Find an item with its index by key
   */
  findWithIndex (key) {
    const found = this.find(key)
    const index = this.items.findIndex(item => {
      return typeCheck(Function, key)
        // @ts-ignore
        ? this.items.findIndex(key)
        : this.findBy(key, [ item ])
    })
    return {
      item: found,
      index: index
    }
  }

  /**
   * filter
   * @category methods
   * @param {Function} callbackFn - Same argument as Array.prototype.filter() method
   * @param {*} [thisArg] - Same argument as Array.prototype.filter() method
   * @returns {Array} - The filtered items
   */
  filter (callbackFn, thisArg) {
    // @ts-ignore
    return this.items.filter(callbackFn, thisArg)
  }

  /**
   * findMany
   * @category methods
   * @param {...*} args - Same as filter
   * @returns {Promise<Collection>} Returns the collection
   * @description
   * [Alias of filter]{@link Collection#filter}
   */
  findMany (...args) {
    // @ts-ignore
    return this.filter(...args)
  }

  /**
   * pushItem
   * @category async methods
   * @param {*} item - Item to push
   * @returns {Promise<Collection>} Returns the collection
   * @description
   * Push an item to the collection
   * @example
   * const collection = new Collection()
   * collection.pushItem({ id: 1, name: 'John' }).items // -> output: [ { id: 1, name: 'John' } ]
   */
  async pushItem (item) {
    this.abortIfLocked()
    // Call 'beforePush' hook
    await this.#hooks.BEFORE_PUSH.exec(item, this)
    // Push item
    this._items.push(item)
    // Call 'afterPush' hook
    await this.#hooks.AFTER_PUSH.exec(item, this)
    // Return collection
    return this
  }

  /**
   * add
   * @category async methods
   * @param {...*} args - Same as pushItem
   * @returns {Promise<Collection>} Returns the collection
   * @description
   * [Alias of pushItem]{@link Collection#pushItem}
   */
  add (...args) {
    // @ts-ignore
    return this.pushItem(...args)
  }

  /**
   * push
   * @category async methods
   * @param {...*} args - Same as pushItem
   * @returns {Promise<Collection>} Returns the collection
   * @description
   * [Alias of pushItem]{@link Collection#pushItem}
   */
  push (...args) {
    // @ts-ignore
    return this.pushItem(...args)
  }

  /**
   * deleteItem
   * @category async methods
   * @param {Function|number|string} key - The key to find item. If function provided, execute this function to find item (same as Array.find function)
   * @returns {Promise<Collection>} Returns the collection
   * @description
   * Delete item from collection
   */
  async deleteItem (key) {
    this.abortIfLocked()
    const { item, index } = this.findWithIndex(key)
    // If found
    if (item && index > -1) {
      // Call beforeDelete hook
      await this.#hooks.BEFORE_DELETE.exec(item, this)
      // Remove item from collection
      this._items.splice(index, 1)
      // Call afterDelete hook
      await this.#hooks.AFTER_DELETE.exec(item, this)
    }
    return this
  }

  /**
   * delete
   * @category async methods
   * @param {...*} args - Same as deleteItem
   * @returns {Promise<Collection>} Returns the collection
   * @description
   * [Alias of deleteItem]{@link Collection#deleteItem}
   */
  delete (...args) {
    // @ts-ignore
    return this.deleteItem(...args)
  }

  /**
   * replaceItem
   * @category async methods
   * @param {Function|number|string} key - The key to find item. If function provided, execute this function to find item (same as Array.find function)
   * @param {*} to - The replacement item
   * @returns {Promise<Collection>} Returns the collection
   * @description
   * Replace item in collection
   */
  async replaceItem (key, to) {
    this.abortIfLocked()
    const { item: from, index } = this.findWithIndex(key)
    // If found
    if (from && index > -1) {
      // Call 'beforeReplace' hook
      await this.#hooks.BEFORE_REPLACE.exec(from, to, this)
      // Replace value
      this._items.splice(index, 1, to)
      // Call 'afterReplace' hook
      await this.#hooks.AFTER_REPLACE.exec(from, to, this)
    }
    return this
  }

  /**
   * replace
   * @category async methods
   * @param {...*} args - Same as replaceItem
   * @returns {Promise<Collection>} Returns the collection
   * @description
   * [Alias of replaceItem]{@link Collection#replaceItem}
   */
  replace (...args) {
    // @ts-ignore
    return this.replaceItem(...args)
  }

  /**
   * touch
   * @category async methods
   * @param {...*} args - Same as replaceItem
   * @returns {Promise<Collection>} Returns the collection
   * @description
   * [Alias of replaceItem]{@link Collection#replaceItem}
   */
  touch (...args) {
    // @ts-ignore
    return this.replaceItem(...args)
  }

  /**
   * update
   * @category async methods
   * @param {...*} args - Same as replaceItem
   * @returns {Promise<Collection>} Returns the collection
   * @description
   * Alias of {@link Collection#replaceItem replaceItem}
   */
  update (...args) {
    // @ts-ignore
    return this.replaceItem(...args)
  }

  /**
   * upsertItem
   * @category async methods
   * @param {Function|number|string} key - The key to find item. If function provided, execute this function to find item (same as Array.find function)
   * @param {*} to - The replacement item
   * @returns {Promise<Collection>} Returns the collection
   * @description
   * Update or insert item in collection
   */
  async upsertItem (key, to) {
    this.abortIfLocked()
    const { item: from, index } = this.findWithIndex(key)
    // If found
    if (from && index > -1) {
      return await this.replaceItem(key, to)
    } else {
      return await this.pushItem(to)
    }
  }

  /**
   * upsert
   * @category async methods
   * @param {...*} args - Same as upsertItem
   * @returns {Promise<Collection>} Returns the collection
   * @description
   * Alias of {@link Collection#upsertItem upsertItem}
   */
  upsert (...args) {
    // @ts-ignore
    return this.upsertItem(...args)
  }

  /**
   * @category methods
   * @param {...*} args - The Array "map" method parameters
   * @returns {Array} Returns the result of Array "map" method
   * @description
   * Map Array "map" method
   */
  map (...args) {
    // @ts-ignore
    return this.items.map(...args)
  }

  /**
   * @category methods
   * @param {...*} args - The Array "reduce" method parameters
   * @returns {Array|object} Returns the result of Array "reduce" method
   * @description
   * Map Array "reduce" method
   */
  reduce (...args) {
    // @ts-ignore
    return this.items.reduce(...args)
  }

  /**
   * @category methods
   * @param {Function} sortFct - The sort function
   * @returns {*} Returns the found value in collection
   * @description
   * See "maxBy" util documentation.
   */
  maxBy (sortFct) {
    return maxBy(this.items, sortFct)
  }

  /**
   * @category methods
   * @param {Function} sortFct - The sort function
   * @returns {*} Returns the found value in collection
   * @description
   * See "minBy" util documentation.
   */
  minBy (sortFct) {
    return minBy(this.items, sortFct)
  }

  /**
   * @category methods
   * @param {...*} args - The second and third optional parameters of "orderBy" lodash method
   * @returns {*} Returns the found value in collection
   * @description
   * See "orderBy" util documentation.
   */
  orderBy (...args) {
    return orderBy(this.items, ...args)
  }

  /**
   * @category methods
   * @param {Function} sortFct - The sort function
   * @returns {*} Returns the found value in collection
   * @description
   * See "sortBy" util documentation.
   */
  sortBy (sortFct) {
    return sortBy(this.items, sortFct)
  }

  /**
   * @category methods
   * @param {Function|Function[]} func - The function to append to the hook queue. Can be an array of functions.
   * @returns {void}
   * @description
   * **BEFORE_PUSH** hook append method.
   *
   * The function to append, which can be _async_ takes two arguments:
   *
   * ```ts
   * function (value: any, collection: Collection) => void | Promise<void>
   * ```
   *
   * - **value**: `{any}` the value
   * - **collection**: `{Collection}` the collection object
   * @example
   * collection.onBeforePush((value, collection) => {
   *   console.log(value)
   * })
   */
  onBeforePush (func) {
    this.#hooks.BEFORE_PUSH.append(func)
  }

  /**
   * @category methods
   * @param {Function|Function[]} func - The function to append to the hook queue. Can be an array of functions.
   * @returns {void}
   * @description
   * **AFTER_PUSH** hook append method.
   *
   * The function to append, which can be _async_ takes two arguments:
   *
   * ```ts
   * function (value: any, collection: Collection) => void | Promise<void>
   * ```
   *
   * - **value**: `{any}` the value
   * - **collection**: `{Collection}` the collection object
   * @example
   * collection.onAfterPush((value, collection) => {
   *   console.log(value)
   * })
   */
  onAfterPush (func) {
    this.#hooks.AFTER_PUSH.append(func)
  }

  /**
   * @category methods
   * @param {Function|Function[]} func - The function to append to the hook queue. Can be an array of functions.
   * @returns {void}
   * @description
   * **BEFORE_DELETE** hook append method.
   *
   * The function to append, which can be _async_ takes two arguments:
   *
   * ```ts
   * function (value: any, collection: Collection) => void | Promise<void>
   * ```
   *
   * - **value**: `{any}` the value
   * - **collection**: `{Collection}` the collection object
   * @example
   * collection.onBeforeDelete((value, collection) => {
   *   console.log(value)
   * })
   */
  onBeforeDelete (func) {
    this.#hooks.BEFORE_DELETE.append(func)
  }

  /**
   * @category methods
   * @param {Function|Function[]} func - The function to append to the hook queue. Can be an array of functions.
   * @returns {void}
   * @description
   * **AFTER_DELETE** hook append method.
   *
   * The function to append, which can be _async_ takes two arguments:
   *
   * ```ts
   * function (value: any, collection: Collection) => void | Promise<void>
   * ```
   *
   * - **value**: `{any}` the value
   * - **collection**: `{Collection}` the collection object
   * @example
   * collection.onAfterDelete((value, collection) => {
   *   console.log(value)
   * })
   */
  onAfterDelete (func) {
    this.#hooks.AFTER_DELETE.append(func)
  }

  /**
   * @category methods
   * @param {Function|Function[]} func - The function to append to the hook queue. Can be an array of functions.
   * @returns {void}
   * @description
   * **BEFORE_REPLACE** hook append method.
   *
   * The function to append, which can be _async_ takes three arguments:
   *
   * ```ts
   * function (from: any, to: any, collection: Collection) => void | Promise<void>
   * ```
   *
   * - **from**: `{any}` the value to replace
   * - **to**: `{any}` the replacement value
   * - **collection**: `{Collection}` the collection object
   * @example
   * collection.onBeforeReplace((from, to, collection) => {
   *   console.log('From: ', from)
   *   console.log('To: ', to)
   * })
   */
  onBeforeReplace (func) {
    this.#hooks.BEFORE_REPLACE.append(func)
  }

  /**
   * @category methods
   * @param {Function|Function[]} func - The function to append to the hook queue. Can be an array of functions.
   * @returns {void}
   * @description
   * **AFTER_REPLACE** hook append method.
   *
   * The function to append, which can be _async_ takes three arguments:
   *
   * ```ts
   * function (from: any, to: any, collection: Collection) => void | Promise<void>
   * ```
   *
   * - **from**: `{any}` the value to replace
   * - **to**: `{any}` the replacement value
   * - **collection**: `{Collection}` the collection object
   * @example
   * collection.onAfterReplace((from, to, collection) => {
   *   console.log('From: ', from)
   *   console.log('To: ', to)
   * })
   */
  onAfterReplace (func) {
    this.#hooks.AFTER_REPLACE.append(func)
  }

  /**
   * Private properties
   */

  /**
   * @category private properties
   * @type {Array}
   */
  _items = []

  /**
   * @category private properties
   * @type {boolean}
   */
  _locked = false

  /**
   * Protected properties
   */

  /**
   * @category protected properties
   * @type {object}
   */
  #hooks = {
    /**
     * @alias Collection['#hooks'].BEFORE_PUSH
     * @type {Hook}
     * @description
     * Called before pushing new item in collection
     */
    BEFORE_PUSH: new Hook('BEFORE_PUSH', []),
    AFTER_PUSH: new Hook('AFTER_PUSH', []),
    BEFORE_DELETE: new Hook('BEFORE_DELETE', []),
    AFTER_DELETE: new Hook('AFTER_DELETE', []),
    BEFORE_REPLACE: new Hook('BEFORE_REPLACE', []),
    AFTER_REPLACE: new Hook('AFTER_REPLACE', [])
  }
}

export default Collection
