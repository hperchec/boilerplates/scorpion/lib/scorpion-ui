export default RequestError;
declare class RequestError extends AppError {
    static caught(error: any): void;
    constructor(original: AxiosError | RequestError | Error, request: Request);
    private _request;
    _response: {
        payload: {};
        status: any;
        original: any;
    };
    original: Error;
    get request(): Request;
    readonly get response(): any;
    hasResponse(): boolean;
    toTypedError(customClass: typeof RequestError): any;
}
import AppError from '@/context/support/errors/AppError';
import Request from '../../Request';
import { AxiosError } from 'axios';
//# sourceMappingURL=RequestError.d.ts.map