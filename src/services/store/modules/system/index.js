import Core from '@/Core'
import AppError from '@/context/support/errors/AppError'

import { isNull, typeCheck } from '@/utils'

const log = (msg) => Core.service('logger').consoleLog(msg, { type: 'store' }) // eslint-disable-line dot-notation
const error = (msg, err) => {
  log(`System module: ${msg}`)
  throw err || new Error(msg)
}

/**
 * @param  {...any} args - See i18n method
 * @returns {string}
 */
// @ts-ignore
const translate = (...args) => Core.service('i18n').t(...args) // eslint-disable-line dot-notation

/**
 * Store.state.System
 */
export default {
  /**
   * @private
   */
  namespaced: true,
  /**
   * state
   * @alias module:store.state
   * @type {object}
   * @readonly
   * @description Vuex Store option. See syntax for accessor.
   */
  state: {
    /**
     * showMainLoading
     * @description Determine if show main loading
     * @type {boolean}
     * @default false
     */
    showMainLoading: false,
    /**
     * mainLoadingMessage
     * @description Main loading message to display
     * @type {string}
     * @default ''
     */
    mainLoadingMessage: '',
    /**
     * systemError
     * @description The current error payload
     * @type {object}
     * @default null
     */
    systemError: null,
    /**
     * desktopNavBar
     * @description Data for Desktop NavBar
     * @type {object}
     */
    desktopNavBar: {
      /**
       * expanded
       * @description Define if desktop navbar is expanded
       * @type {boolean}
       * @default false
       */
      expanded: false
    },
    /**
     * native
     * @description Data for native context (on Android/iOS)
     * @type {object}
     */
    native: {
      /**
       * Back button
       * @description Back button options
       * @type {object}
       */
      backButton: {
        /**
         * clickHandler
         * @type {Function}
         * @default undefined
         * @description A function to override default backbutton click handler.
         * Cordova 'device' plugin adds 'backbutton' event dispatcher at document level.
         */
        clickHandler: undefined
      },
      /**
       * Menu button
       * @description Menu button options
       * @type {object}
       */
      menuButton: {
        /**
         * clickHandler
         * @type {Function}
         * @default undefined
         * @description A function to override default menubutton click handler.
         * Cordova 'device' plugin adds 'menubutton' event dispatcher at document level.
         */
        clickHandler: undefined
      }
    }
  },
  /**
   * getters
   * @type {object}
   * @readonly
   * @description Store module getters
   * @example
   * // Access to the module getters, where <name> is the getter name
   * Store.getters['System/<name>']
   */
  getters: {
    // ...
  },
  /**
   * actions
   * @type {object}
   * @protected
   * @description Store module actions
   * @example
   * // Access to the module action, where <name> is the action name
   * Store.dispatch('System/<name>')
   */
  actions: {
    /**
     * Set the current system error
     * @param {object} context - The vuex context
     * @param {Error} [payload] - (Optional) Payload
     * @returns {void}
     * @example
     * this.$store.dispatch('System/throwSystemError', { message: 'Unknown error appears', error: new Error() })
     */
    throwSystemError: function ({ commit }, payload) {
      // @ts-ignore
      const defaultPayload = new Error(translate('errors.e0000'))
      // Set error payload
      commit('SET_SYSTEM_ERROR', payload || defaultPayload)
    },
    /**
     * Reset system error
     * @param {object} context - The vuex context
     * @returns {void}
     * @example
     * this.$store.dispatch('System/clearSystemError')
     */
    clearSystemError: function ({ commit }) {
      // Reset error payload
      commit('SET_SYSTEM_ERROR', null)
    },
    /**
     * Set device native back button behavior (click handler). Overrides existing handler
     * @param {object} context - The vuex context
     * @param {Function} handler - The handler to set
     * @returns {void}
     */
    setNativeBackButtonHandler: function ({ commit, state }, handler) {
      // Remove event listener with reference
      document.removeEventListener('backbutton', state.native.backButton.clickHandler)
      // Set new handler
      commit('SET_NATIVE_BACK_BUTTON_HANDLER', handler)
      // Add new handler
      document.addEventListener('backbutton', state.native.backButton.clickHandler, false)
    },
    /**
     * Set device native menu button behavior (click handler). Overrides existing handler
     * @param {object} context - The vuex context
     * @param {Function} handler - The handler to set
     * @returns {void}
     */
    setNativeMenuButtonHandler: function ({ commit, state }, handler) {
      // Remove event listener with reference
      document.removeEventListener('menubutton', state.native.menuButton.clickHandler)
      // Set new handler
      commit('SET_NATIVE_MENU_BUTTON_HANDLER', handler)
      // Add new handler
      document.addEventListener('menubutton', state.native.menuButton.clickHandler, false)
    }
  },
  /**
   * mutations
   * @type {object}
   * @protected
   * @description Store module mutations
   * @example
   * // Dispatch a module mutation, where <mutation_name> is the mutation name
   * Store.commit('System/<mutation_name>', [payload])
   */
  mutations: {
    /**
     * SET_SHOW_MAIN_LOADING
     * @description Mutate state.showMainLoading
     * @param {object} state - The module state
     * @param {boolean} value - True or false
     * @returns {void}
     */
    SET_SHOW_MAIN_LOADING (state, value) {
      state.showMainLoading = value
    },
    /**
     * SET_MAIN_LOADING_MESSAGE
     * @description Mutate state.mainLoadingMessage
     * @param {object} state - The module state
     * @param {string} message - The message
     * @returns {void}
     */
    SET_MAIN_LOADING_MESSAGE (state, message) {
      state.mainLoadingMessage = message
    },
    /**
     * SET_SYSTEM_ERROR
     * @description Mutate state.system.systemError
     * @param {object} state - The module state
     * @param {?AppError} err - The error instance
     * @returns {void}
     */
    SET_SYSTEM_ERROR (state, err) {
      // @ts-ignore
      if (!isNull(err) && !(err instanceof AppError || err instanceof Core.context.support.errors.AppError)) {
        error('Error in SET_SYSTEM_ERROR mutation: value must inherits from AppError class.')
      } else {
        state.systemError = err
      }
    },
    /**
     * SET_DESKTOP_NAVBAR_EXPANDED
     * @description Mutate state.system.desktopNavbar.expanded
     * @param {object} state - The module state
     * @param {boolean} value - True or false
     * @returns {void}
     */
    SET_DESKTOP_NAVBAR_EXPANDED (state, value) {
      state.desktopNavBar.expanded = value
    },
    /**
     * SET_NATIVE_BACK_BUTTON_HANDLER
     * @description Mutate state.native.backButton.clickHandler
     * @param {object} state - The module state
     * @param {Function} handler - Event handler function
     * @returns {void}
     */
    SET_NATIVE_BACK_BUTTON_HANDLER (state, handler) {
      if (!typeCheck(Function, handler)) error('Error in SET_NATIVE_BACK_BUTTON_HANDLER mutation: handler must be function.')
      state.native.backButton.clickHandler = handler
    },
    /**
     * SET_NATIVE_MENU_BUTTON_HANDLER
     * @description Mutate state.native.menuButton.clickHandler
     * @param {object} state - The module state
     * @param {Function} handler - Event handler function
     * @returns {void}
     */
    SET_NATIVE_MENU_BUTTON_HANDLER (state, handler) {
      if (!typeCheck(Function, handler)) error('Error in SET_NATIVE_MENU_BUTTON_HANDLER mutation: handler must be function.')
      state.native.menuButton.clickHandler = handler
    }
  }
}
