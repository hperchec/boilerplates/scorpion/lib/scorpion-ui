/**
 * @vuepress
 * ---
 * title: PromiseQueue class
 * headline: PromiseQueue class
 * sidebarTitle: PromiseQueue
 * sidebarDepth: 0 # To disable auto sidebar links
 * ---
 */

import Queue from 'promise-queue'
// import { compose, isArray, isDef, typeCheck } from '@/utils'

/**
 * PromiseQueue class. See https://www.npmjs.com/package/promise-queue
 * @typicalname queue
 * @classdesc PromiseQueue class.
 */
class PromiseQueue extends Queue {
  /**
   * Create a new PromiseQueue
   * @param {number} [maxPendingPromises = Infinity] - Max number of concurrently executed promises
   * @param {number} [maxQueuedPromises = Infinity] - Max number of queued promises
   * @param {object} [options] - The options
   */
  constructor (maxPendingPromises, maxQueuedPromises, options) { // eslint-disable-line no-useless-constructor
    super(maxPendingPromises, maxQueuedPromises, options)
    // new Queue(
    //   options.maxConcurrent || 1,
    //   options.maxQueued || 1
    // )
  }
}

// @ts-ignore
PromiseQueue.configure(Promise)

export default PromiseQueue
