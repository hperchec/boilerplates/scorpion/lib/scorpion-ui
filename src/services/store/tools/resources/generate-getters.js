import { isUndef } from '@/utils'

export const defaultGetterNames = {
  // Only one entity
  find: [ 'get', 'find' ],
  // Many results
  filter: [ 'where', 'filter' ]
}

/**
 * Generate default resource module getters
 * @param {*} model - The model representing the resource
 * @param {object} options - The function options
 * @returns {object} Returns an object of getters
 */
export default function generateGetters (model, options = {}) {
  // Check if model is provided
  if (isUndef(model)) {
    throw new Error('Generate resource module getters: no model provided')
  }
  const getters = {
    $model: (state) => model,
    // Returns all
    all: (state) => {
      const $dm = model.storeOptions.module.dataManager
      return $dm.getModelCollection(state).all()
    }
  }
  // Getters for one result
  for (const name of defaultGetterNames.find) {
    getters[name] = (state) => (key) => {
      const $dm = model.storeOptions.module.dataManager
      return $dm.getModelCollection(state).find(key)
    }
  }
  // Getters for many results
  for (const name of defaultGetterNames.filter) {
    getters[name] = (state) => (callbackFn) => {
      const $dm = model.storeOptions.module.dataManager
      return $dm.getModelCollection(state).filter(callbackFn)
    }
  }
  // Return getters object
  return getters
}
