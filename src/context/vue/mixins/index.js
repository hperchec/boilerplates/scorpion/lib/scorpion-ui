/**
 * @vuepress
 * ---
 * title: Mixins
 * headline: Mixins
 * sidebarTitle: Mixins
 * initialOpenGroupIndex: -1 # optional, defaults to 0, defines the index of initially opened subgroup
 * prev: ../filters/
 * next: ./global/
 * ---
 */

import global from './global'

/**
 * @alias mixins
 * @typicalname Core.context.vue.mixins
 * @description
 * Contains all app mixins.
 */
export default {
  /**
   * @alias mixins.global
   * @see {@link ./global global}
   */
  global
}
