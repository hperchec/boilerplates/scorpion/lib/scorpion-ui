---
title: isBlankString
headline: isBlankString
sidebarTitle: isBlankString
---

# isBlankString

<a name="isblankstring">
</a>

## isBlankString ⇒ <code>boolean</code>

Check if the string is empty or contains only spaces

**Returns**: <code>boolean</code> - Returns true if the string is empty or contains only spaces

| Param | Type | Description |
| --- | --- | --- |
| str | <code>string</code> | The string |

**Example**

```js
isBlankString('') // trueisBlankString('    ') // trueisBlankString('  foo bar  ') // false
```

