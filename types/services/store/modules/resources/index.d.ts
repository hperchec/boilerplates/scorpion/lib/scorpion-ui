declare namespace _default {
    let namespaced: boolean;
    let modules: object;
    let state: object;
    let getters: object;
    let actions: object;
    let mutations: object;
}
export default _default;
//# sourceMappingURL=index.d.ts.map