/**
 * @vuepress
 * ---
 * title: has
 * headline: has
 * sidebarTitle: has
 * ---
 */

// Lodash.has
import lodashHas from 'lodash.has'

/**
 * has
 * @param {object} object - The object to check
 * @param {Array|string} path - The path to check
 * @returns {boolean} Returns true if object has property defined
 * @description
 * Define is object has property (lodash.has).
 *
 * ::: tip See also
 * [lodash.has](https://www.npmjs.com/package/lodash.has) package documentation
 * :::
 * @example
 * const object = { 'a': [{ 'b': { 'c': 3 } }] }
 * has(object, 'a[0].b.c') // => true
 */
export const has = function (object, path) {
  return lodashHas(object, path)
}

export default has
