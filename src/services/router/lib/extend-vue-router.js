import Core from '@/Core'
import { isNull } from '@/utils'

/**
 * Extend VueRouter class
 * @ignore
 * @param {Function} vueRouterClass - The base VueRouter class
 * @returns {Function} - The extended class
 */
export default function (vueRouterClass) {
  // Set debugging private property
  vueRouterClass.prototype.__v_descriptor__ = {
    namespace: 'service:router',
    description: 'See VueRouter documentation: https://v3.router.vuejs.org/api/#component-injections',
    optionType: 'prototype',
    type: vueRouterClass,
    // @ts-ignore
    externalLink: __getDocUrl__('/api/services/router')
  }

  // Save original method
  const originalInitMethod = vueRouterClass.prototype.init

  vueRouterClass.prototype.init = function (vm) {
    // Define event listener for 'routing-triggered'
    vm.$on('routing-triggered', () => {
      originalInitMethod.call(this, vm)
      vm.isRouterInitialized = true
      // IMPORTANT to set manually the current route
      vm._routerRoot._route = vm.$router.currentRoute
    })
  }

  // Save original method
  const originalPushMethod = vueRouterClass.prototype.push

  /**
   * Overriden push method.
   * @param {*} location - The target location
   * @param {?Function} [onComplete] - Default is service options `defaultOnComplete`. Pass null to disable
   * @param {?Function} [onAbort] - Default is service options `defaultOnAbort`. Pass null to disable
   * @returns {void}
   */
  vueRouterClass.prototype.push = function (location, onComplete, onAbort) {
    return originalPushMethod.call(
      this,
      location,
      isNull(onComplete) ? undefined : (onComplete || Core.context.services.router.options.defaultOnComplete),
      isNull(onAbort) ? undefined : (onAbort || Core.context.services.router.options.defaultOnAbort)
    )
  }

  return vueRouterClass
}
