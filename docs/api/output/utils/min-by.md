---
title: minBy
headline: minBy
sidebarTitle: minBy
---

# minBy

<a name="minby">
</a>

## minBy ⇒ <code>\*</code>

Find min by in array (lodash.minby).::: tip See also[lodash.minby](https://www.npmjs.com/package/lodash.minby) package documentation:::

**Returns**: <code>\*</code> - Returns the found value in `array`

| Param | Type | Description |
| --- | --- | --- |
| array | <code>Array</code> | The array |
| sortFct | <code>function</code> | The sort function |

**Example**

```js
const array = [ { id: 1, name: 'John' }, { id: 2, name: 'Jane' } ]minBy(array, function (item) { return item.id }) // Return -> { id: 1, name: 'John' }minBy(array, function (item) { return item.name }) // Return -> { id: 2, name: 'Jane' }
```

