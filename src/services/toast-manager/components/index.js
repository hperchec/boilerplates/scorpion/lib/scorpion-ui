import BaseToast from './BaseToast.vue'
import BaseToaster from './BaseToaster.vue'
import ExampleToaster from './ExampleToaster.vue'
import Toast from './Toast.vue'

export default {
  BaseToast,
  BaseToaster,
  ExampleToaster,
  Toast
}
