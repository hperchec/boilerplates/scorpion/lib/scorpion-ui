/**
 * @vuepress
 * ---
 * title: "Service (websocket-manager) configuration"
 * headline: "Service (websocket-manager) configuration"
 * sidebarTitle: Configuration
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * ⚠ THIS FILE IS ONLY USED TO GENERATE SERVICE CONFIGURATION DOCUMENTATION
 */

/**
 * @name Configuration
 * @typicalname Core.config.services['websocket-manager']
 * @description
 * Service configuration
 *
 * > Refers to `Core.config.services['websocket-manager']`
 *
 * ```js
 * 'websocket-manager': {
 *   // Connections definitions
 *   // Overwrites service option: `connections`
 *   connections: {
 *     // <name> is the connection name and the value is a connection options object
 *     // (see WSConnectionOptions constructor third param)
 *     '<name>': Object,
 *     ...
 *   }
 * }
 * ```
 */
const config = { // eslint-disable-line no-unused-vars
  /**
   * @alias Configuration.connections
   * @type {object}
   * @description
   * Connection definitions. An object following the schema:
   *
   * ```js
   * {
   *   [key: String]: Object
   * }
   * ```
   *
   * Object keys are the connection names while values are Object like WSConnectionOptions constructor third param.
   */
  connections: {}
}
