/**
 * @vuepress
 * ---
 * title: "Service (store)"
 * headline: "Service (store)"
 * sidebarTitle: store
 * initialOpenGroupIndex: -1 # optional, defaults to 0, defines the index of initially opened subgroup
 * prev: ../
 * next: ./options
 * ---
 */

import Vuex from 'vuex'

import Service from '@/Service'

import options from './options'
import modules from './modules'
import plugins from './plugins'
import tools from './tools'

/* eslint-disable jsdoc/require-param, jsdoc/require-returns */

/**
 * @scorpion_ui_service {Vuex.Store} store
 * @description
 * The service "store" is for data state and sharing between components.
 * It uses the [vuex](https://vuex.vuejs.org/) package.
 */
export default new Service({
  // Service
  service: {
    // Identifier
    identifier: 'store',
    // Register function
    register: function (_Core) {
      // ...
    },
    /**
     * @alias create
     * @description
     * Returns an instance of `Vuex.Store` created with Vuex options.
     */
    create: function (serviceOptions, serviceContext, _Core) {
      /* eslint-disable no-unused-vars */
      const {
        logger,
        log,
        translator,
        translate,
        localStorageManager,
        apiManager,
        ...VuexOptions
      } = serviceOptions
      /* eslint-enable no-unused-vars */
      // Create new Store instance
      return new Vuex.Store(VuexOptions)
    },
    /**
     * @alias plugin
     * @description
     * ::: tip SEE ALSO
     * [vuex](https://vuex.vuejs.org/) documentation
     * :::
     */
    vuePlugin: (serviceContext, _Core) => Vuex,
    // Vue plugin options
    pluginOptions: {},
    // rootOption
    rootOption: 'store'
  },
  /**
   * @alias onInitialized
   * @description
   * Once Core is initialized, it will define a `Logger` {@link ../logger/Logger#types type} as following:
   *
   * ```js
   * store: {
   *   badgeContent: '[STORE]',
   *   badgeColor: '#BDC6FB',
   *   badgeBgColor: '#5168F5',
   *   messageColor: '#091D9A',
   *   prependMessage: '☁'
   * }
   * ```
   */
  onInitialized: (Core) => {
    Core.onBeforeCreateService('logger', ({ types }) => {
      // Check if 'store' type is defined, else set it
      if (!types.store) {
        types.store = {
          badgeContent: '[STORE]',
          badgeColor: '#BDC6FB',
          badgeBgColor: '#5168F5',
          messageColor: '#091D9A',
          prependMessage: '☁'
        }
      }
    })
  },
  // Options
  options,
  /**
   * @alias expose
   */
  expose: {
    /**
     * @see {@link ./modules modules}
     */
    modules,
    /**
     * @see {@link ./plugins plugins}
     */
    plugins,
    /**
     * @see {@link ./tools tools}
     */
    tools
  }
})
