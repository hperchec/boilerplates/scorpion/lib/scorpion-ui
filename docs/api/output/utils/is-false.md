---
title: isFalse
headline: isFalse
sidebarTitle: isFalse
---

# isFalse

<a name="isfalse">
</a>

## isFalse ⇒ <code>boolean</code>

Returns true if variable is boolean and false (=== false)

**Returns**: <code>boolean</code> - Returns true if value strictly equals false

| Param | Type | Description |
| --- | --- | --- |
| value | <code>\*</code> | The variable to check |

**Example**

```js
isFalse(false) // => trueisFalse('Hello') // => false
```

