export default LocalDatabaseManager;
declare const LocalDatabaseManager_base: any;
declare class LocalDatabaseManager extends LocalDatabaseManager_base {
    [x: string]: any;
    static reactive: boolean;
    static rootOption: string;
    static aliases: string[];
    static install(_Vue: import("vue/types/umd"), options?: object): void;
    constructor(options: {
        databases: LocalDatabase[];
    });
    readonly get databases(): LocalDatabase[];
    add(database: LocalDatabase): LocalDatabase;
    use(databaseName: string): LocalDatabase;
    toJSON(): object;
}
import LocalDatabase from './LocalDatabase';
//# sourceMappingURL=LocalDatabaseManager.d.ts.map