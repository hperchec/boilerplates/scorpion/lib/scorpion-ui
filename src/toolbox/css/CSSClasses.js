/**
 * @vuepress
 * ---
 * title: CSSClasses class
 * headline: CSSClasses class
 * sidebarTitle: CSSClasses
 * sidebarDepth: 0 # To disable auto sidebar links
 * ---
 */

import stripIndent from 'strip-indent'
import splitLines from 'split-lines'
import classNames from 'classnames'

/**
 * @classdesc
 * CSSClasses wrapper class to manipulate css classes
 */
class CSSClasses {
  /**
   * Private
   */
  #classes

  /**
   * @param {string|string[]|object} classes - The css classes as Vue accepts
   */
  constructor (classes) {
    this.#classes = CSSClasses.parseCssClassValue(classes)
  }

  /**
   * Returns css classes as array
   * @returns {string[]} The classes as array
   */
  toArray () {
    return Object.keys(this.#classes).reduce((arr, _class) => {
      if (this.#classes[_class]) { arr.push(_class) }
      return arr
    }, [])
  }

  /**
   * ToString override to string default method
   * @returns {string} The classes as string
   */
  toString () {
    return this.toArray().join(' ')
  }

  /**
   * Returns classes as object
   * @returns {object} A classes object
   */
  toObject () {
    return Object.assign({}, this.#classes)
  }

  /**
   * CSS class value can be string or any formats accepted by Vue for class attribute
   * @param {string|string[]|object} value - The CSS class value
   * @returns {object} - An classes object format
   */
  static parseCssClassValue (value) {
    if (value instanceof Array) {
      // Check if array contains string only
      if (value.filter((item) => typeof item !== 'string').length) {
        throw new Error('CSSClasses - Error when parsing CSS class value: array can contain strings (class names) only. See also https://v2.vuejs.org/v2/guide/class-and-style.html#ad for Vue accepted formats.')
      }
      // Return classes object
      return this.objectifyClassesArray(value)
    } else if (typeof value === 'object') {
      // If object, check if object follows the schema: { [key: String]: Boolean }
      if (Object.keys(value).filter((key) => typeof key !== 'string').length) {
        throw new Error('CSSClasses - Error when parsing CSS class value: if Object passed, keys must be String. See also https://v2.vuejs.org/v2/guide/class-and-style.html#ad for Vue accepted formats.')
      }
      if (Object.values(value).filter((item) => typeof item !== 'boolean').length) {
        throw new Error('CSSClasses - Error when parsing CSS class value: Object values can be Boolean only. See also https://v2.vuejs.org/v2/guide/class-and-style.html#ad for Vue accepted formats.')
      }
      // return value itself
      return value
    } else if (typeof value === 'string') {
      // else if value is string, try to extract class names and build array of classes
      return this.objectifyClassesArray(this.extractClassesFromStr(value))
    } else {
      throw new Error('CSSClasses - Error when parsing CSS class value: must be String, Object or Array of strings. See also https://v2.vuejs.org/v2/guide/class-and-style.html#ad for Vue accepted formats.')
    }
  }

  /**
   * Takes an array of css classes as unique parameter and check if it is valid
   * @param {string} classesStr - The classes string
   * @returns {string[]} The classes as array
   */
  static extractClassesFromStr (classesStr) {
    // Split lines
    const lines = splitLines(classesStr)
    // Function to extract classes from line
    const extractFromLine = (line) => line.split(' ').filter((val) => val.length /* non empty string */)

    // return this.sanitizeClassesValue(classesStr).split(' ')
    return [].concat(...lines.map(extractFromLine))
  }

  /**
   * Takes an array of css classes an returns as object
   * @param {string[]} classes - The classes array
   * @returns {object} The classes object
   */
  static objectifyClassesArray (classes) {
    return classes.reduce((obj, _class) => {
      obj[_class] = true
      return obj
    }, {})
  }

  /**
   * Sanitize classes value to make it more readable
   * @param {string} str - The classes str to parse
   * @returns {string} Classes as string
   */
  static sanitizeClassesValue (str) {
    // Remove redundant indentation
    str = stripIndent(str)
    // Trim
    str = str.trim()
    return str
  }

  /**
   * MergeCssClasses classes value
   * @param {...*} args - Arguments to pass to `classnames` library
   * @returns {object} The merged classes
   */
  static mergeCssClasses (...args) {
    return new this(classNames(...args)).toObject()
  }
}

export default CSSClasses
