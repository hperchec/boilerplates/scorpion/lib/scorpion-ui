/**
 * @vuepress
 * ---
 * title: "Service (store) options"
 * headline: "Service (store) options"
 * sidebarTitle: .options
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import Core from '@/Core'

import { isUndef } from '@/utils'

/**
 * Cache for dynamic properties
 * @ignore
 */
const _options = {
  modules: undefined,
  plugins: undefined
}

/**
 * @name options
 * @static
 * @description
 * Accepts all [Vuex.Store](https://v3.vuex.vuejs.org/api/#vuex-store-constructor-options) constructor options
 *
 * By default, the `modules` option is an empty Object. The modules defined in `Core.context.services['store'].modules` like `System` and `Resources`
 * will be auto-injected at service creation.
 * The `plugins` option is an empty array. At service creation, the `plugins` option will be a concatenated array of plugins
 * defined in `Core.context.services['store'].plugins` and the `plugins` option array content.
 *
 * ::: tip SEE ALSO
 * [Vuex.Store constructor options documentation](https://v3.vuex.vuejs.org/api/#vuex-store-constructor-options)
 * :::
 */
export default {
  /* eslint-disable dot-notation */
  /**
   * modules
   * @ignore
   */
  get modules () {
    // If modules option is not provided
    if (isUndef(_options.modules)) {
      return Core.context.services['store'].modules
    } else {
      return _options.modules
    }
  },
  set modules (value) {
    _options.modules = value
  },
  /**
   * state
   * @ignore
   */
  state: {},
  /**
   * getters
   * @ignore
   */
  getters: {},
  /**
   * actions
   * @ignore
   */
  actions: {},
  /**
   * mutations
   * @ignore
   */
  mutations: {},
  /**
   * plugins
   * @ignore
   */
  get plugins () {
    // If plugins option is not provided, return context plugins
    return isUndef(_options.plugins)
      ? [
        ...Object.values(Core.context.services['store'].plugins).reduce((array, pluginObj) => {
          array.push(pluginObj.createPlugin(pluginObj.options))
          return array
        }, [])
      ]
      : _options.plugins
  },
  set plugins (value) {
    _options.plugins = value
  }
}
