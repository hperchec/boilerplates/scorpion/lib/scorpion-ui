module.exports = {
  globals: {
    __VERSION__: false,
    __DOC_BASE_URL__: false,
    __getDocUrl__: false
  },
  env: {
    node: true
  },
  ignorePatterns: [
    // Excludes all './types/**' files
    'types'
  ],
  extends: [
    'plugin:vue/essential',
    '@vue/standard',
    'plugin:jsdoc/recommended-typescript'
  ],
  parserOptions: {
    parser: '@babel/eslint-parser',
    // ecmaVersion: 'latest',
    sourceType: 'module'
  },
  processor: 'disable/disable', // For "disable" plugin
  plugins: [
    'disable', // plugin to disable other plugins (https://www.npmjs.com/package/eslint-plugin-disable)
    'jsdoc' // parse docblocks (JSDoc)
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-unused-vars': [ 'error', { args: 'none' } ],
    'array-bracket-spacing': [ 'error', 'always' ],
    'vue/multi-word-component-names': 'off',
    'jsdoc/no-undefined-types': [ 'error', {
      definedTypes: [
        'Vue',
        'VNode',
        'VueComponent'
      ]
    } ],
    'jsdoc/check-tag-names': [ 'error', {
      definedTags: [
        'vuepress',
        'category', // jsdoc2md/dmd featured tag
        'typicalname', // jsdoc2md/dmd featured tag
        'sidebar_title', // custom tag for sidebar title
        'scorpion_ui_service', // custom tag for services
        'todo' // custom tag for project management
      ]
    } ],
    'jsdoc/require-param': [ 'warn', {
      checkDestructured: false
    } ],
    'jsdoc/check-param-names': [ 'warn', {
      checkDestructured: false
    } ],
    'jsdoc/no-defaults': 'off', // unexpected conflict with doc generation (TO DO)
    'jsdoc/no-types': 'off' // unexpected conflict with typescript errors
  },
  settings: {
    'disable/plugins': [
      'vue'
    ],
    'disable/externalProcessor': 'vue/.vue',
    jsdoc: {
      mode: 'typescript',
      tagNamePreference: {
        return: 'returns',
        augments: 'extends'
      }
    }
  },
  overrides: [
    /**
     * Disable jsdoc plugin for .vue files
     */
    {
      files: [
        '*.vue',
        '*rollup-plugin-vue=script.js' // script in .vue files are processed as file that ends with ".vue?rollup-plugin-vue=script.js"
      ],
      settings: {
        'disable/plugins': [
          'jsdoc'
        ]
      }
    },
    /**
     * For services identifiers: always quoted
     * Default is not consistent
     */
    {
      files: [
        './src/config/index.js'
      ],
      rules: {
        'quote-props': [ 'error', 'consistent' ]
      }
    },
    /**
     * Ignore folders for jsdoc rules
     */
    {
      files: [
        './build/**/*'
      ],
      settings: {
        'disable/plugins': [
          'jsdoc'
        ]
      }
    },
    /**
     * Tests environment
     */
    {
      files: [
        '**/__tests__/*.{j,t}s?(x)',
        '**/tests/unit/**/*.spec.{j,t}s?(x)'
      ],
      env: {
        jest: true
      }
    }
  ]
}
