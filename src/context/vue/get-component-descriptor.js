/**
 * @vuepress
 * ---
 * title: "getComponentDescriptor"
 * headline: "getComponentDescriptor"
 * sidebarTitle: "getComponentDescriptor"
 * prev: ../
 * ---
 */

// import Core from '@/Core'
import { getAllKeys } from '@/utils'

/**
 * Get the app component descriptor (WIP)
 * The keys that start with "_" and native Vue properties are excluded)
 * @todo
 * @param {object} target - A vue component instance
 * @returns {object} Returns the component descriptor
 */
export function getComponentDescriptor (target) {
  // Parse root instance first children object
  const keys = getAllKeys(target, {
    excludeKeys: [
      /^_/, // private properties that starts with '_'
      'constructor',
      // Vue native props
      '$data',
      '$props',
      '$set',
      '$delete',
      '$watch',
      '$on',
      '$once',
      '$off',
      '$emit',
      '$forceUpdate',
      '$destroy',
      '$nextTick',
      '$mount',
      '$inspect',
      '$options',
      '$parent',
      '$root',
      '$refs',
      '$vnode',
      '$slots',
      '$scopedSlots',
      '$createElement',
      '$attrs',
      '$listeners',
      '$el',
      '$children',
      '$isServer',
      '$ssrContext'
    ]
  })

  // Sort alphabetically
  keys.sort((a, b) => {
    return a < b
      ? -1
      : a > b
        ? 1
        : 0
  })

  const descriptors = {}

  const unknownDescriptor = {
    namespace: '?',
    description: '',
    optionType: undefined, // can be: 'data', 'computed', 'method', 'prototype' or 'nested' (for thisArg bound nested objects)
    type: undefined,
    externalLink: undefined
  }

  for (const key of keys) {
    const value = target[key]
    try {
      descriptors[key] = value?.__v_descriptor__
        ? value.__v_descriptor__
        : unknownDescriptor
    } catch (err) {
      console.warn('Error when parsing Vue component to get descriptor:', err)
      descriptors[key] = unknownDescriptor
    }
  }

  return descriptors
}

export default getComponentDescriptor
