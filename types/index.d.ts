export const Core: typeof _Core;
export const utils: any;
export const toolbox: any;
export const builtInServices: _Service[];
export const Service: typeof _Service;
export const ServiceDefinition: typeof _ServiceDefinition;
export default ScorpionUI;
import _Core from './Core';
import _Service from './Service';
import _ServiceDefinition from './ServiceDefinition';
declare const ScorpionUI: object;
//# sourceMappingURL=index.d.ts.map