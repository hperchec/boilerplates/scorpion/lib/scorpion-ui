/**
 * @vuepress
 * ---
 * title: Template tools - utils
 * headline: Template tools - utils
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import Core from '@/Core'
// Utils
import {
  deepMerge as merge,
  get
} from '@/utils'

/**
 * @alias utils.mapComponent
 * @description
 * It is a "hack" to define component through function.
 * Use it in template, when components are dependent on each other
 * See also https://v2.vuejs.org/v2/guide/components-dynamic-async.html#Async-Components.
 * @param {string} path - The path from `Core.context`
 * @returns {Function} Returns a function that returns a Promise
 */
export function mapComponent (path) {
  return (resolve, reject) => {
    resolve(get(Core.context, path))
  }
}

/**
 * @alias utils.mergeContext
 * @description
 * Merge Core context objects
 * @example
 * // In template extend:
 * mergeContext(Core.context.vue.components, 'App', App)
 * @param {*} obj - The Core context property target
 * @param {string} key - The key to merge
 * @param {*} value - The value
 * @returns {void}
 */
export function mergeContext (obj, key, value) {
  obj[key] = merge(obj[key], value)
}

/**
 * @name utils
 * @static
 * @type {object}
 */
export default {
  mapComponent,
  mergeContext
}
