/**
 * @vuepress
 * ---
 * title: Toolbox
 * headline: Toolbox
 * sidebarTitle: Toolbox
 * initialOpenGroupIndex: -1 # optional, defaults to 0, defines the index of initially opened subgroup
 * prev: ../services/
 * next: ./css/
 * ---
 */

import css from './css'
import development from './development'
import services from './services'
import template from './template'
import vue from './vue'
import Hook from './Hook'
import PromiseQueue from './PromiseQueue'

export { default as Hook } from './Hook'
export { default as PromiseQueue } from './PromiseQueue'

/**
 * @name toolbox
 * @static
 * @type {object}
 * @description
 * ScorpionUI toolbox.
 *
 * ```js
 * import { toolbox } from '@hperchec/scorpion-ui'
 * ```
 */
export const toolbox = {
  /**
   * @alias toolbox.css
   * @type {object}
   * @see {@link ./css css}
   */
  css,
  /**
   * @alias toolbox.development
   * @type {object}
   * @see {@link ./development development}
   */
  development,
  /**
   * @alias toolbox.services
   * @type {object}
   * @see {@link ./services services}
   */
  services,
  /**
   * @alias toolbox.template
   * @type {object}
   * @see {@link ./template template}
   */
  template,
  /**
   * @alias toolbox.vue
   * @type {object}
   * @see {@link ./vue vue}
   */
  vue,
  /**
   * @alias toolbox.Hook
   * @type {Function}
   * @class
   * @see {@link ./Hook Hook}
   */
  Hook,
  /**
   * @alias toolbox.PromiseQueue
   * @type {Function}
   * @class
   * @see {@link ./PromiseQueue PromiseQueue}
   */
  PromiseQueue
}

export default toolbox
