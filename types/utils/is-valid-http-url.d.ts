export function isValidHttpUrl(str: string): (URL | boolean);
export default isValidHttpUrl;
//# sourceMappingURL=is-valid-http-url.d.ts.map