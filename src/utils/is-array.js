/**
 * @vuepress
 * ---
 * title: isArray
 * headline: isArray
 * sidebarTitle: isArray
 * ---
 */

/**
 * isArray
 * @param {*} value - The variable to check
 * @returns {boolean} Returns true if value instance of Array
 * @description
 * Check if value is instance of Array
 * @example
 * isArray(['foo', 'bar']) // => true
 */
export const isArray = function (value) {
  return value instanceof Array
}

export default isArray
