---
title: get-module
headline: get-module
---

# get-module

<a name="getmodule">
</a>

## getModule ⇒ <code>object</code>

Get a store module by namespace

**Returns**: <code>object</code> - The module object

| Param | Type | Description |
| --- | --- | --- |
| store | <code>module:vuex~Store</code> | The store instance |
| namespace | <code>string</code> | The full module namespace |

