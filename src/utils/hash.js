/**
 * @vuepress
 * ---
 * title: hash
 * headline: hash
 * sidebarTitle: hash
 * ---
 */

// bcrypt
import bcrypt from 'bcryptjs'

/**
 * @alias hash
 * @type {object}
 */
export default {
  /**
   * @alias hash.bcrypt
   * @type {object}
   * @description
   * See [bcryptjs](https://www.npmjs.com/package/bcryptjs) package documentation
   */
  bcrypt
}
