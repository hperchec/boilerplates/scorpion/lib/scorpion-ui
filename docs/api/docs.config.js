const path = require('path')

module.exports = {
  /**
   * See also: https://github.com/ph1p/vuepress-jsdoc
   */
  /**
   * Title
   */
  title: 'API',
  /**
   * Source directory
   * > relative to root folder
   */
  sourceDir: './src',
  /**
   * Source file paths
   * > relative to source directory
   */
  sourceFiles: [
    './config/index.js' // copy config file source for default configuration doc
  ],
  /**
   * Dist directory
   * > relative to root folder
   */
  distDir: './docs/api',
  /**
   * Output directory
   * > relative to distDir folder
   */
  outputDir: 'output',
  /**
   * Exclude files
   */
  exclude: [
    'globals.config.json'
    // 'contextualize.js',
    // '**/context/services/*/config.js'
  ],
  /**
   * JSDoc options
   */
  jsdoc: {
    /**
     * Configuration path
     */
    configPath: path.resolve(__dirname, './jsdoc/jsdoc.config.js')
  },
  /**
   * jsdoc2md options
   */
  jsdoc2md: {
    /**
     * jsdoc2md partial option
     */
    partials: [
      path.resolve(__dirname, './jsdoc2md/partials/main.hbs'),
      path.resolve(__dirname, './jsdoc2md/partials/layouts/layout-default.hbs'),
      path.resolve(__dirname, './jsdoc2md/partials/layouts/layout-service.hbs'),
      path.resolve(__dirname, './jsdoc2md/partials/service-docs/service-docs.hbs'),
      path.resolve(__dirname, './jsdoc2md/partials/service-docs/service-create.hbs'),
      path.resolve(__dirname, './jsdoc2md/partials/service-docs/service-expose.hbs'),
      path.resolve(__dirname, './jsdoc2md/partials/service-docs/service-register.hbs'),
      path.resolve(__dirname, './jsdoc2md/partials/service-docs/service-vue/service-app.hbs'),
      path.resolve(__dirname, './jsdoc2md/partials/service-docs/service-vue/service-plugin.hbs'),
      path.resolve(__dirname, './jsdoc2md/partials/service-docs/service-vue/service-shared.hbs'),
      path.resolve(__dirname, './jsdoc2md/partials/service-docs/service-vue/service-vue.hbs'),
      path.resolve(__dirname, './jsdoc2md/partials/all-docs/docs/header.hbs'),
      path.resolve(__dirname, './jsdoc2md/partials/all-docs/docs/body/customTags.hbs'),
      path.resolve(__dirname, './jsdoc2md/partials/all-docs/docs/body/description.hbs'),
      path.resolve(__dirname, './jsdoc2md/partials/all-docs/docs/body/examples.hbs'),
      path.resolve(__dirname, './jsdoc2md/partials/all-docs/docs/body/returns.hbs'),
      path.resolve(__dirname, './jsdoc2md/partials/all-docs/docs/body/scope.hbs'),
      path.resolve(__dirname, './jsdoc2md/partials/all-docs/docs/body/see.hbs'),
      path.resolve(__dirname, './jsdoc2md/partials/all-docs/docs/body/params/params-table.hbs'),
      path.resolve(__dirname, './jsdoc2md/partials/all-docs/docs/member-index/member-index.hbs'),
      path.resolve(__dirname, './jsdoc2md/partials/all-docs/docs/member-index/member-index-list.hbs'),
      path.resolve(__dirname, './jsdoc2md/partials/all-docs/docs/member-index/member-index-grouped.hbs'),
      path.resolve(__dirname, './jsdoc2md/partials/shared/index-indent.hbs'),
      path.resolve(__dirname, './jsdoc2md/partials/shared/value/link.hbs'),
      path.resolve(__dirname, './jsdoc2md/partials/shared/signature/sig-link.hbs'),
      path.resolve(__dirname, './jsdoc2md/partials/shared/signature/sig-name.hbs')
    ],
    /**
     * jsdoc2md helper option
     */
    helpers: [
      path.resolve(__dirname, './jsdoc2md/helpers/arrayify.js'),
      path.resolve(__dirname, './jsdoc2md/helpers/custom-inline-links.js'),
      path.resolve(__dirname, './jsdoc2md/helpers/has-identifier.js'),
      path.resolve(__dirname, './jsdoc2md/helpers/if-service-doc.js'),
      path.resolve(__dirname, './jsdoc2md/helpers/slugify.js')
    ]
  },
  /**
   * vue-docgen options
   */
  docgen: {
    /**
     * vue-docgen-cli configuration file path
     * > relative to root folder
     */
    configPath: './docs/api/vue-docgen/docgen.config.js'
  },
  /**
   * Report file (will contain vuepress-jsdoc stdout)
   * Path to file, relative to root folder
   */
  reportFile: './docs/api.report.txt',
  /**
   * Vuepress options
   */
  vuepress: {
    /**
     * Base path
     */
    basePath: 'v1.0',
    /**
     * API target path
     */
    targetPath: '/api'
  }
}
