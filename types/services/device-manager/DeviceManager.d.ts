export default DeviceManager;
declare const DeviceManager_base: any;
declare class DeviceManager extends DeviceManager_base {
    [x: string]: any;
    static reactive: boolean;
    static rootOption: string;
    static aliases: any[];
    static install(_Vue: Vue, options?: object): void;
    constructor(options: object);
    toJSON(): object;
}
//# sourceMappingURL=DeviceManager.d.ts.map