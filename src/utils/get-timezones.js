/**
 * @vuepress
 * ---
 * title: getTimezones
 * headline: getTimezones
 * sidebarTitle: getTimezones
 * ---
 */

/**
 * Returns Intl.supportedValuesOf('timeZone')
 * @ignore
 */
// @ts-ignore
const supportedTimezones = Intl.supportedValuesOf('timeZone')

/**
 * getTimezones
 * @returns {string[]} - The supported values
 * @description
 * Returns the supported values of Intl "timeZone"
 * See https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Intl/supportedValuesOf
 * @example
 * getTimezones() // => [ 'Europe/Paris', ... ]
 */
export const getTimezones = () => {
  return supportedTimezones
}

export default getTimezones
