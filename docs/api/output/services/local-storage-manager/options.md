---
title: Service (local-storage-manager) options
headline: Service (local-storage-manager) options
sidebarTitle: .options
sidebarDepth: 0
prev: false
next: false
---

# Service (local-storage-manager) options

<a name="options">
</a>

## .options

Default is an empty object

