export default PrivateRoute;
declare class PrivateRoute extends Route {
    constructor(...args: any[]);
    set children(value: any);
    get children(): any;
}
import Route from './Route';
//# sourceMappingURL=PrivateRoute.d.ts.map