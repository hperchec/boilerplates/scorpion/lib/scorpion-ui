/**
 * @vuepress
 * ---
 * title: isVueComponent
 * headline: isVueComponent
 * sidebarTitle: isVueComponent
 * ---
 */

import getConstructorName from './get-constructor-name'

/**
 * isVueComponent
 * @param {*} value - The value to check
 * @returns {boolean} Returns boolean
 * @description
 * Returns true if value is a Vue component (VueComponent constructor name or "__isVueComponentOptions" property)
 * @example
 * isVueComponent({ a: 'b' }) // false
 * isVueComponent({ template: '<div>Hello World!</div>' }) // false
 * isVueComponent(Vue.extend({ template: '<div>Hello World!</div>' })) // true
 */
export const isVueComponent = function (value) {
  return getConstructorName(value) === 'VueComponent' || // constructor is VueComponent
    value?.constructor?.name === 'VueComponent' ||
    Object.prototype.hasOwnProperty.call(value, '__isVueComponentOptions') // see shared/lib/runtime/component-normalizer
}

export default isVueComponent
