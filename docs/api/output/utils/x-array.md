---
title: XArray
headline: XArray
sidebarTitle: XArray
---

# XArray

<a name="xarray">
</a>

## XArray ⇐ <code>Array</code>

Extended array

**Extends**: <code>Array</code>  
**Schema**:

- [XArray](#xarray) ⇐ <code>Array</code>
  - [new exports.XArray(...args)](#newxarraynew)
  - [.insert(value, [index])](#xarray-insert)

<a name="newxarraynew">
</a>

### new exports.XArray(...args)

Extended constructor: first arg can be Array

| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | Same parameters as Array constructor |

**Example**

```js
new XArray([ 'foo', 'bar' ])
```

<a name="xarray-insert">
</a>

### xArray.insert(value, [index])

Insert method: insert a value in array at `<index>`. If `<index>` not provided, push at the end of the array.

| Param | Type | Description |
| --- | --- | --- |
| value | <code>\*</code> | The value to insert |
| [index] | <code>number</code> | The index |

