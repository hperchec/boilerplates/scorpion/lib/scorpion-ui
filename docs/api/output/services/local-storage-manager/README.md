---
title: Service (local-storage-manager)
headline: Service (local-storage-manager)
sidebarTitle: local-storage-manager
initialOpenGroupIndex: -1
prev: ../
next: ./options
---

# Service (local-storage-manager)

<a name="service"></a>

The service "local-storage-manager" is for localStorage management.

## Create

Returns an instance of `LocalStorageManager` created with options.

**Returns**: <code>module:services/local-storage-manager/LocalStorageManager~LocalStorageManager</code> - Once services are created:

```js
Core.service('local-storage-manager')
```

## Vue

### Plugin

On registering, service Vue plugin will be set in `Core.context.vue.plugins['local-storage-manager']`.

Plugin will be automatically used by Vue.

The following properties will be set to Vue prototype:```js// Each references to Core.service('local-storage-manager')Vue.prototype.$localStorageManagerVue.prototype.$LS```

### Root instance

<a name="app"></a>

#### Prototype $app property

On registering, the service will inject properties to \`Core.context.vue.root.prototype.app\`.

> Properties will be shared by all components under \`vm.$app\` where \`vm\` is the component instance.

<a name="app-usersettings"></a>

##### User settings

Service will define the following user settings keys:

##### Others

## Expose (service context)

The service will expose the following properties under `Core.context.services['local-storage-manager']`:
<a name="expose-localstoragemanager">
</a>

### .LocalStorageManager

> Exposed as `Core.context.services['local-storage-manager'].LocalStorageManager`

**See**: [LocalStorageManager](./LocalStorageManager)

