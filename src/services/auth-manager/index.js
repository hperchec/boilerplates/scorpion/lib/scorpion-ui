/**
 * @vuepress
 * ---
 * title: "Service (auth-manager)"
 * headline: "Service (auth-manager)"
 * sidebarTitle: auth-manager
 * initialOpenGroupIndex: -1 # optional, defaults to 0, defines the index of initially opened subgroup
 * prev: ../
 * next: ./options
 * ---
 */

import Service from '@/Service'

import options from './options'
import AuthManager from './AuthManager'

/* eslint-disable jsdoc/require-param, jsdoc/require-returns */

/**
 * @scorpion_ui_service {AuthManager} auth-manager
 * @description
 * The service "auth-manager" is designed to make authentication managemnent easier.
 */
export default new Service({
  // Service
  service: {
    // Identifier
    identifier: 'auth-manager',
    /**
     * @alias register
     * @description
     * On registering, the service will inject properties to Vue root instance prototype `$app.auth`:
     *
     * - `$app.auth.user`
     */
    register: function (_Core) {
      // Define vue root instance prototype in '$app' namespace
      const $app = _Core.context.vue.root.prototype.app
      // Init 'auth' namespace
      $app.auth = {}
      /**
       * @alias $app.auth.user
       * @returns {?any} - The current user
       * @see {@link ./AuthManager#authmanager-getcurrentuser AuthManager.getCurrentUser}
       * @description
       * Get the current authenticated user
       * @example
       * // In any component
       * this.$app.auth.user()
       */
      $app.auth.user = function () {
        // const root = this // 'this' is the Vue root instance
        return _Core.service('auth-manager').getCurrentUser()
      }
    },
    /**
     * @alias create
     * @description
     * Returns an instance of `Authmanager` created with options.
     */
    create: (serviceOptions, { AuthManager: _AuthManager }, _Core) => {
      // Return new AuthManager
      return new _AuthManager(serviceOptions)
    },
    /**
     * @alias plugin
     * @description
     * The following properties will be set to Vue prototype:
     *
     * ```js
     * Vue.prototype.$authManager // => Core.service('auth-manager')
     * ```
     */
    vuePlugin: ({ AuthManager: _AuthManager }, _Core) => _AuthManager,
    // Vue plugin options
    pluginOptions: {},
    // rootOption
    rootOption: ({ AuthManager: _AuthManager }, _Core) => _AuthManager.rootOption
  },
  // On initialized
  onInitialized: (Core) => {
    // ...
  },
  // Options
  options,
  /**
   * @alias expose
   */
  expose: {
    /**
     * @see {@link ./AuthManager AuthManager}
     */
    AuthManager
  }
})
