/**
 * @vuepress
 * ---
 * title: getConstructorName
 * headline: getConstructorName
 * sidebarTitle: getConstructorName
 * ---
 */

// RegExp to find constructor name
const functionTypeCheckRE = /^\s*function (\w+)/

/**
 * @description
 * Use function string name to check built-in types,
 * because a simple equality check will fail
 * @example
 * getConstructorName(Date) // => 'Date'
 * @param {Function} fn - The constructor
 * @returns {string} Returns the constructor name
 */
export const getConstructorName = function (fn) {
  const match = fn && fn.toString().match(functionTypeCheckRE)
  return match ? match[1] : ''
}

export default getConstructorName
