const { compose, deeplyParseHeaders, slugify } = require('@vuepress/shared-utils')

exports.slugifyAnchorName = slugifyAnchorName
exports.slugifyAnchorIfRelativeUrl = slugifyAnchorIfRelativeUrl

/**
 * See also https://github.com/ulivz/vuepress-plugin-check-md/blob/a8601505b7e1e99f45f517727549019c51dcf377/index.js#L23
 * @param {string} anchorName - The anchor name
 * @param {Object} options - Handlebars options
 * @return {string}
 */
function slugifyAnchorName (anchorName, options) {
  return compose(deeplyParseHeaders, slugify)(anchorName)
}

/**
 * If url is relative, slugify anchor name.
 * Else, do nothing. Returns url.
 * @param {string} url - The URL string
 * @param {*} options - Handlebars options
 * @return {string}
 */
function slugifyAnchorIfRelativeUrl (url, options) {
  let isUrlRelative
  try {
    // Throws error if no base or 'http(s):'. See URL documentation
    const urlInstance = new URL(url) // eslint-disable-line no-unused-vars
    isUrlRelative = false
  } catch (error) {
    isUrlRelative = true
  }
  const slugifyAnchorInLink = function (str) {
    const re = /#(.*)$/g // Match anchor at the end of the string
    const matches = [ ...str.matchAll(re) ]
    // if anchor name is found
    if (matches[0]) {
      // get anchor name
      const anchorName = matches[0][1]
      str = str.replace(`#${anchorName}`, `#${slugifyAnchorName(anchorName)}`)
    }
    return str
  }
  // If url is relative, consider that is an INTERNAL link, so slugify anchor name
  return isUrlRelative ? slugifyAnchorInLink(url) : url
}
