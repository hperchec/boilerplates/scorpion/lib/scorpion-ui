/**
 * @vuepress
 * ---
 * title: "Service (modal-manager): Modal class"
 * headline: "Service (modal-manager): Modal class"
 * sidebarTitle: .Modal
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import Core from '@/Core'
import { getAllKeys, typeCheck } from '@/utils'

/**
 * @classdesc
 * Service **modal-manager**: Modal class
 */
class Modal {
  /**
   * Private
   */
  _manager = undefined // will be set by the manager itself at modal registration
  _name
  _component
  _props = {}
  _vm = null

  // ...

  /**
   * Create a new Modal
   * @param {string} name - The modal unique name
   * @param {import("vue").Component} component - The modal component
   * @param {object} [props = {}] - The props to pass to componeny
   */
  constructor (name, component, props = {}) {
    const modalInstance = this
    modalInstance.name = name
    // @ts-ignore
    modalInstance.component = Core.Vue.extend(component).mixin({
      props: {
        // @ts-ignore
        ...modalInstance.constructor.props // Modal class static props
      },
      mounted () {
        modalInstance._vm = this
        // @ts-ignore
        this.opened()
      },
      destroyed () {
        modalInstance._vm = null
      }
    })
    modalInstance.#mergeProps(props)
  }

  /**
   * Accessors & mutators
   */

  /**
   * name
   * @category properties
   * @type {string}
   * @description
   * The modal name
   */
  get name () {
    return this._name
  }

  set name (value) {
    // Check type
    if (typeCheck(String, value)) {
      // Assign
      this._name = value
    } else {
      // Else -> throw error
      throw new TypeError('Modal class: \'name\' property must be String. ' + typeof value + ' received...')
    }
  }

  /**
   * component
   * @category properties
   * @type {object|null}
   * @description
   * The modal component
   */
  get component () {
    return this._component
  }

  set component (value) {
    // Check type
    if (typeCheck([ Object, Function ], value)) {
      // Assign
      this._component = value
    } else {
      // Else -> throw error
      throw new TypeError('Modal class: \'component\' property must be Object or Function. ' + typeof value + ' received...')
    }
  }

  /**
   * props
   * @category properties
   * @readonly
   * @type {?object}
   * @description
   * The modal props
   */
  get props () {
    return this._props
  }

  /**
   * isShown
   * @category properties
   * @readonly
   * @type {boolean}
   * @description
   * Returns true if modal is shown
   */
  get isShown () {
    return this._manager.isShown(this.name)
  }

  /**
   * isHidden
   * @category properties
   * @readonly
   * @type {boolean}
   * @description
   * Returns true if modal is hidden
   */
  get isHidden () {
    return this._manager.isHidden(this.name)
  }

  /**
   * vm
   * @category properties
   * @readonly
   * @type {object|null}
   * @description
   * The vue model
   */
  get vm () {
    return this._vm
  }

  /**
   * Methods
   */

  /**
   * @category methods
   * @param {object} [props = {}] - The props to pass to the modal
   * @returns {void}
   * @description
   * Show this modal
   */
  show (props = {}) {
    this._manager.show(this.name, props)
  }

  /**
   * @category methods
   * @returns {void}
   * @description
   * Hide this modal
   */
  hide () {
    this._manager.hide(this.name)
  }

  /**
   * Overrides the default toJSON object method for JSON.stringify() calls
   * @returns {object} - The instance data to be serialized
   */
  toJSON () {
    const keys = getAllKeys(this, {
      excludeKeys: [
        /^_/, // private properties that starts with '_'
        'toJSON',
        'toString',
        'constructor'
      ]
    })
    const jsonObj = keys.reduce((obj, key) => {
      const value = this[key]
      // Custom output for props
      if (key === 'props') {
        const props = obj[key] = {}
        for (const propKey in value) {
          props[propKey] = typeof value[propKey] === 'function' ? value[propKey].toString() : value[propKey]
        }
      } else {
        obj[key] = value
      }
      return obj
    }, {})
    return jsonObj
  }

  toString () {
    return JSON.stringify(this.toJSON())
  }

  /**
   * Private methods
   */

  /**
   * @category methods
   * @param {*} props - The props object to merge with current
   * @returns {void}
   * @description
   * Merge the given props object with instance props
   */
  #mergeProps (props) {
    // Check type
    if (typeCheck(Object, props)) {
      // @ts-ignore
      const defaultProps = this.constructor.getDefaultProps()
      const mergedProps = { ...defaultProps, ...props }
      for (const key in mergedProps) {
        // Assign
        this._props[key] = mergedProps[key]
      }
    } else {
      // Else -> throw error
      throw new TypeError('Modal class: \'props\' property must be Object. ' + typeof props + ' received...')
    }
  }

  /**
   * Static properties
   */

  /**
   * @category methods
   * @static
   * @memberof Modal
   * @type {object}
   */
  static props = {
    /**
     * @returns {void}
     * @description
     * Called before modal opens
     */
    beforeOpen: {
      type: Function,
      default: () => { /* void */ }
    },
    /**
     * @returns {void}
     * @description
     * Called before modal closes
     */
    beforeClose: {
      type: Function,
      default: () => { /* void */ }
    },
    /**
     * @returns {void}
     * @description
     * Called when modal is opened
     */
    opened: {
      type: Function,
      default: () => { /* void */ }
    },
    /**
     * @description
     * Called when modal is closed
     */
    closed: {
      type: Function,
      default: () => { /* void */ }
    },
    /**
     * @default 0
     * @description
     * The layer as number. Can be string: "foreground"
     */
    layer: {
      type: [ Number, String ],
      default: 0
    },
    /**
     * @description
     * Called when "confirm" button is clicked
     */
    onConfirm: {
      type: Function,
      default: () => { /* void */ }
    },
    /**
     * @description
     * Called when "cancel" button is clicked
     */
    onCancel: {
      type: Function,
      default: () => { /* void */ }
    }
  }

  /**
   * Static methods
   */

  /**
   * @category methods
   * @returns {object} Returns a default props object
   */
  static getDefaultProps () {
    // @ts-ignore
    return Object.keys(this.props).reduce((props, propKey) => {
      props[propKey] = this.props[propKey].default
      return props
    }, {})
  }

  /**
   * Private properties
   */

  // ...
}

export default Modal
