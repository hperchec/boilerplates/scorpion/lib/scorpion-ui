module.exports = {
  plugins: [
    '@babel/plugin-transform-runtime',
    '@babel/plugin-proposal-class-properties',
    '@babel/plugin-proposal-private-methods'
    // '@babel/plugin-transform-classes'
  ],
  presets: [
    [
      '@vue/cli-plugin-babel/preset',
      {
        useBuiltIns: false
      }
    ]
  ]
}
