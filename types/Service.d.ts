export class Service {
    static resolveServiceDef(def: object): ServiceDefinition;
    constructor(serviceObject: {
        service: object;
        onInitialized?: Function;
        options?: object;
        expose?: object;
    });
    set onInitialized(value: any);
    get onInitialized(): any;
    set options(value: any);
    get options(): any;
    getDefinition(): ServiceDefinition;
    getNamespacedProperties(): string[];
    getNamespace(): object;
    #private;
}
export default Service;
import ServiceDefinition from './ServiceDefinition';
//# sourceMappingURL=Service.d.ts.map