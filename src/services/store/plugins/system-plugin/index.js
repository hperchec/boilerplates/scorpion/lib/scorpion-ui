// import Core from '@/Core'
import options from './options'

/**
 * Store system plugin
 */
export default {
  /**
   * Function to create plugin
   * @param {object} options - The create plugin options
   * @returns {Function} Returns store system plugin
   */
  createPlugin: function (options) {
    // Process options
    // ...

    // Plugin
    return (store) => {
      // Set debugging private prop
      store.__v_descriptor__ = {
        namespace: 'service:store',
        description: 'Access the Vuex store instance',
        optionType: 'prototype',
        type: store.constructor,
        // @ts-ignore
        externalLink: __getDocUrl__('/api/services/store')
      }
      /**
       * Watch mutations
       */
      store.subscribe((mutation, state) => {
        // ...
      })
    }
  },
  /**
   * createPlugin options
   */
  options
}
