/**
 * @vuepress
 * ---
 * title: capitalize
 * headline: capitalize
 * sidebarTitle: capitalize
 * ---
 */

// Lodash.capitalize
import lodashCapitalize from 'lodash.capitalize'

/**
 * capitalize
 * @param {string} str - The string to capitalize
 * @param {number} [limit = -1] - The limit for substring to parse (-1: no limit, else int from 1 to X)
 * @returns {string} Returns the "capitalized" string
 * @description
 * Capitalize a string *({@link ../context/vue/filters#filters-capitalize Defined as Vue filter})*.
 *
 * ::: tip See also
 * [lodash.capitalize](https://www.npmjs.com/package/lodash.capitalize) package documentation
 * :::
 * @example
 * capitalize('foo Bar') // => 'Foo bar'
 * capitalize('ping the IP Address', 1) // => 'Ping the IP address'
 * capitalize('ping the IP Address', 9) // => 'Ping the ip Address'
 */
export const capitalize = function (str, limit = -1) {
  let strToCapitalize = lodashCapitalize(str)
  let remainder = ''
  if (limit > 0) {
    strToCapitalize = lodashCapitalize(str.substring(0, limit))
    remainder = str.substring(limit, str.length)
  }
  return strToCapitalize + remainder
}

export default capitalize
