/**
 * @vuepress
 * ---
 * title: findKey
 * headline: findKey
 * sidebarTitle: findKey
 * ---
 */

// Lodash.findKey
import lodashFindKey from 'lodash.findkey'

/**
 * findKey
 * @param {...*} args - Same args as lodash findKey method
 * @returns {*} - Returns the key of the matched element, else undefined
 * @description
 * Find key of object that matches conditions
 *
 * ::: tip See also
 * [lodash.findkey](https://www.npmjs.com/package/lodash.findkey) package documentation
 * :::
 */
export const findKey = function (...args) {
  return lodashFindKey(...args)
}

export default findKey
