---
title: Service (modal-manager) configuration
headline: Service (modal-manager) configuration
sidebarTitle: Configuration
prev: false
next: false
---

# Service (modal-manager) configuration

<a name="configuration">
</a>

## Configuration

Service configuration> Refers to `Core.config.services['modal-manager']````js'modal-manager': {  // The base z-index for ModalOverlay components  baseZIndex: Number}```

<a name="configuration-basezindex">
</a>

### Core.config.services[&#x27;modal-manager&#x27;].baseZIndex : <code>number</code>

The base z-index for ModalOverlay components

