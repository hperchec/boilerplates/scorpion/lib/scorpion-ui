import errors from './errors.json'
import global from './global.json'
import modals from './modals.json'
import models from './models.json'
import views from './views.json'

export default {
  ...errors,
  ...global,
  ...modals,
  ...models,
  ...views
}
