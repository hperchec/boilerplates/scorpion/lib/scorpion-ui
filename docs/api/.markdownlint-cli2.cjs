const options = require('@github/markdownlint-github').init()

/**
 * Export markdownlint config
 */
module.exports = {
  config: {
    ...options,
    // MD004 - Unordered list style
    'ul-style': {
      'style': 'consistent'
    },
    // MD012 - Multiple consecutive blank lines
    'no-multiple-blanks': {
      'maximum': 2
    },
    // MD013 - Line length
    'line-length': false,
    // MD025 - Multiple top-level headings in the same document
    'single-title': {
      'front_matter_title': '' // accepts front matter title duplicate
    },
    // MD033 - Inline HTML
    'no-inline-html': false, // accepts inline html
    // MD036 - Emphasis used instead of a heading
    'no-emphasis-as-heading': false // disable for dmd template
  },
  customRules: [
    '@github/markdownlint-github'
  ],
  outputFormatters: [
    [ 
      'markdownlint-cli2-formatter-pretty',
      {
        appendLink: true // ensures the error message includes a link to the rule documentation
      }
    ]
  ]
}