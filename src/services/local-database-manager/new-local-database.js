import Core from '@/Core'
import LocalDatabase from './LocalDatabase'

/**
 * Creates a LocalDatabase instance
 * @param {string} name - The database name
 * @param {object} options - An object following LocalDatabaseOptions second arg schema
 * @returns {LocalDatabase} - The LocalDatabase instance
 */
export const newLocalDatabase = (name, options) => {
  return new Core.context.services['local-database-manager'].LocalDatabase(
    new Core.context.services['local-database-manager'].LocalDatabaseOptions(name, options)
  )
}

export default newLocalDatabase
