/**
 * @vuepress
 * ---
 * title: lowercase
 * headline: lowercase
 * sidebarTitle: lowercase
 * ---
 */

// Lodash.lowercase
import lodashLowercase from 'lodash.lowercase'

/**
 * lowercase
 * @param {string} str - The string to lowercase
 * @returns {string} Returns the "lowercased" string
 * @description
 * Convert a string to lowercase *({@link ../context/vue/filters#filters-lowercase Defined as Vue filter})*.
 *
 * ::: tip See also
 * [lodash.lowercase](https://www.npmjs.com/package/lodash.lowercase) package documentation
 * :::
 * @example
 * lowercase('Foo BAR') // => 'foo bar'
 */
export const lowercase = function (str) {
  return lodashLowercase(str)
}

export default lowercase
