export default NotificationManager;
declare const NotificationManager_base: any;
declare class NotificationManager extends NotificationManager_base {
    [x: string]: any;
    static reactive: boolean;
    static rootOption: string;
    static aliases: any[];
    static install(_Vue: import("vue/types/umd"), options?: object): void;
    constructor(options: object);
    "new"(title: string, options?: {
        audio?: new (src?: string) => HTMLAudioElement;
        onClick?: Function;
        onClose?: Function;
        onError?: Function;
        onShow?: Function;
    }): Notification;
    getPermission(): string;
    askNotificationPermission(): Promise<string>;
    toJSON(): object;
}
//# sourceMappingURL=NotificationManager.d.ts.map