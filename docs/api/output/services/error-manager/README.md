---
title: Service (error-manager)
headline: Service (error-manager)
sidebarTitle: error-manager
initialOpenGroupIndex: -1
prev: ../
next: ./options
---

# Service (error-manager)

<a name="service"></a>

The service "error-manager" is designed to make error managemnent easier.It references any error handlers you want to the `window`, `Vue` and `webpack-dev-server` runtime scopes.

## Register

On registering, the service will inject properties to Vue root instance prototype `$app`:- `$app.throwSystemError`- `$app.clearSystemError`

## Create

Returns an instance of `ErrorManager` created with options.

**Returns**: <code>module:services/error-manager/ErrorManager~ErrorManager</code> - Once services are created:

```js
Core.service('error-manager')
```

## Vue

### Plugin

On registering, service Vue plugin will be set in `Core.context.vue.plugins['error-manager']`.

Plugin will be automatically used by Vue.

The following properties will be set to Vue prototype:```jsVue.prototype.$errorManager // => Core.service('error-manager')```

### Root instance

<a name="app"></a>

#### Prototype $app property

On registering, the service will inject properties to \`Core.context.vue.root.prototype.app\`.

> Properties will be shared by all components under \`vm.$app\` where \`vm\` is the component instance.

**Schema**:

- [$app](#app) : <code>object</code>
  - [.userSettings](#app-usersettings) : <code>object</code>
  - [.throwSystemError(appError)](#app-throwsystemerror) ⇒ <code>void</code>
  - [.clearSystemError()](#app-clearsystemerror) ⇒ <code>void</code>

<a name="app-usersettings"></a>

##### User settings

Service will define the following user settings keys:

##### Others

<a name="app-throwsystemerror">
</a>

###### vm.$app.throwSystemError(appError) ⇒ <code>void</code>

Throw a system error. If the AppError is already caught by ErrorManager, dont process

| Param | Type | Description |
| --- | --- | --- |
| appError | <code>AppError</code> | The error instance |

**Example**

```js
// In any componentthis.$app.throwSystemError(new AppError())
```

<a name="app-clearsystemerror">
</a>

###### vm.$app.clearSystemError() ⇒ <code>void</code>

Clear the current system error.

**Example**

```js
// In any componentthis.$app.clearSystemError()
```

## Expose (service context)

The service will expose the following properties under `Core.context.services['error-manager']`:
<a name="expose-errormanager">
</a>

### .ErrorManager

> Exposed as `Core.context.services['error-manager'].ErrorManager`

**See**: [ErrorManager](./ErrorManager)

