/**
 * @vuepress
 * ---
 * title: isEqual
 * headline: isEqual
 * sidebarTitle: isEqual
 * ---
 */

// Lodash.minby
import lodashIsEqual from 'lodash.isequal'

/**
 * isEqual
 * @param {*} value - The value to compare
 * @param {*} other - The other value to compare
 * @returns {boolean} Returns lodash.isequal result
 * @description
 * Compare two values (lodash.isequal).
 *
 * ::: tip See also
 * [lodash.isequal](https://www.npmjs.com/package/lodash.isequal) package documentation
 * :::
 * @example
 * isEqual(1, 2) // Return -> false
 * isEqual([ 'toto', 'tata' ], [ 'tata', 'toto' ]) // Return -> true
 */
export const isEqual = function (value, other) {
  return lodashIsEqual(value, other)
}

export default isEqual
