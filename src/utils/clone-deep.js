/**
 * @vuepress
 * ---
 * title: cloneDeep
 * headline: cloneDeep
 * sidebarTitle: cloneDeep
 * ---
 */

// Lodash.clonedeep
import lodashCloneDeep from 'lodash.clonedeep'

/**
 * cloneDeep
 * @param {*} value - The value to clone
 * @returns {*} Returns the depp cloned object
 * @description
 * This method is like 'clone' except that it recursively clones value.
 *
 * ::: tip See also
 * [lodash.cloneDeep](https://lodash.com/docs/4.17.15#cloneDeep) package documentation
 * :::
 * @example
 * cloneDeep({ 'a': 1, 'b': { foo: 'bar' } }) // creates a deep clone
 */
export const cloneDeep = function (value) {
  return lodashCloneDeep(value)
}

export default cloneDeep
