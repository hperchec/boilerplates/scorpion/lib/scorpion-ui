/**
 * @vuepress
 * ---
 * title: groupBy
 * headline: groupBy
 * sidebarTitle: groupBy
 * ---
 */

// Lodash.groupBy
import lodashGroupBy from 'lodash.groupby'

/**
 * groupBy
 * @param {...*} args - Same arguments as lodash.groupBy
 * @description Groupby on array (lodash.groupBy).
 * @returns {Array} Returns the grouped elements
 * @example
 * const users = (['eight', 'nine', 'four', 'seven'])
 * groupBy(users, 'length') // => { '4': [ 'nine', 'four' ], '5': [ 'eight', 'seven' ]}
 */
export const groupBy = function (...args) {
  return lodashGroupBy(...args)
}

export default groupBy
