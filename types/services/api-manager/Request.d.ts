export default Request;
declare class Request {
    constructor(manager: RequestManager, generator: Function, method: string, url: string | URL, options: object, beforeCb: Function, successCb: Function, errorCb: Function);
    readonly get manager(): RequestManager;
    readonly get generator(): Function;
    readonly get method(): string;
    readonly get url(): string | URL;
    readonly get options(): any;
    send(options?: {
        allowConcurrent?: boolean;
    }): Promise<any>;
    #private;
}
import RequestManager from './RequestManager';
//# sourceMappingURL=Request.d.ts.map