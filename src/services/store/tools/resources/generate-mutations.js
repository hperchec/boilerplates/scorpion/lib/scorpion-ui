// Utils
import { isUndef } from '@/utils'

/**
 * Generate default resource module state
 * @param {*} model - The model representing the resource
 * @param {object} options - The function options
 * @returns {object} Returns mutations object
 */
export default function generateMutations (model, options = {}) {
  // Check if model is provided
  if (isUndef(model)) {
    throw new Error('Generate resource module mutations: no model provided')
  }

  const moduleName = model.storeOptions.namespace

  // Return mutations object
  return {
    /**
     * INSERT
     * @description Mutate state.collection
     * @param {object} state - vuex store state
     * @param {*} payload - The item to push
     * @returns {void}
     */
    INSERT (state, payload) {
      // Get data manager
      const $dm = this.getResourceDataManager(moduleName)
      $dm.insert(payload, state)
    },
    /**
     * UPDATE
     * @description Mutate state.collection
     * @param {object} state - vuex store state
     * @param {object} payload - Mutation payload
     * @returns {void}
     */
    UPDATE (state, payload) {
      // Get data manager
      const $dm = this.getResourceDataManager(moduleName)
      $dm.update(payload, state)
    },
    /**
     * UPSERT
     * @description Mutate state.collection
     * @param {object} state - vuex store state
     * @param {object[]|object} payload - Mutation payload
     * @returns {void}
     */
    UPSERT (state, payload) {
      // Get data manager
      const $dm = this.getResourceDataManager(moduleName)
      $dm.upsert(payload, state)
    },
    /**
     * DELETE
     * @description Mutate state.collection
     * @param {object} state - vuex store state
     * @param {Function|number|string} key - The key to find item to delete in collection
     * @returns {void}
     */
    DELETE (state, key) {
      // Get data manager
      const $dm = this.getResourceDataManager(moduleName)
      $dm.delete(key, state)
    }
  }
}
