---
title: "Service (keyboard-event-manager): KeyboardEventManager class"
headline: "Service (keyboard-event-manager): KeyboardEventManager class"
sidebarTitle: .KeyboardEventManager
sidebarDepth: 0
prev: false
next: false
---

# Service (keyboard-event-manager): KeyboardEventManager class

<a name="keyboardeventmanager">
</a>

## KeyboardEventManager ⇐ <code>VuePluginable</code>

Service **keyboard-event-manager**: KeyboardEventManager class

**Extends**: <code>VuePluginable</code>  
**Schema**:

- [KeyboardEventManager](#keyboardeventmanager) ⇐ <code>VuePluginable</code>
  - [new KeyboardEventManager()](#newkeyboardeventmanagernew)
  - _instance_
    - [.reactive](#keyboardeventmanager-reactive)
    - [.toJSON()](#keyboardeventmanager-tojson) ⇒ <code>object</code>
    - _methods_
      - [.ctrlKey(scope, keys, onKeydown, onKeyup)](#keyboardeventmanager-ctrlkey)
      - [.altKey(scope, keys, onKeydown, onKeyup)](#keyboardeventmanager-altkey)
      - [.ctrlAltKey(scope, keys, onKeydown, onKeyup)](#keyboardeventmanager-ctrlaltkey)
      - [.attemptPrompt(keys, [resolvedCb], [badCombinationCb])](#keyboardeventmanager-attemptprompt) ⇒ <code>object</code>
    - _properties_
      - [.scopes](#keyboardeventmanager-scopes) : <code>Set</code>
  - _static_
    - [.install(_Vue, [options])](#keyboardeventmanager-install)

<a name="newkeyboardeventmanagernew">
</a>

### new KeyboardEventManager()

Create a new instance

<a name="keyboardeventmanager-reactive">
</a>

### keyboardEventManager.reactive

Overrides static VuePluginable properties

<a name="keyboardeventmanager-tojson">
</a>

### keyboardEventManager.toJSON() ⇒ <code>object</code>

Overrides the default toJSON object method for JSON.stringify() calls

**Returns**: <code>object</code> - - The instance data to be serialized

<a name="keyboardeventmanager-ctrlkey">
</a>

### keyboardEventManager.ctrlKey(scope, keys, onKeydown, onKeyup)

Create a listener to handle KeyboardEvent "ctrl"See [hotkeys-js](https://www.npmjs.com/package/hotkeys-js) package documentation.

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| scope | <code>string</code> | The scope as string like **hotkeys** scope argument |
| keys | <code>string</code> | Like **hotkeys** first argument. Will be prefixed by 'ctrl+' |
| onKeydown | <code>function</code> | The keydown listener method as **hotkeys** accepts |
| onKeyup | <code>function</code> | The keyup listener method as **hotkeys** accepts |

<a name="keyboardeventmanager-altkey">
</a>

### keyboardEventManager.altKey(scope, keys, onKeydown, onKeyup)

Create a listener to handle KeyboardEvent "alt"See [hotkeys-js](https://www.npmjs.com/package/hotkeys-js) package documentation.

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| scope | <code>string</code> | The scope as string like **hotkeys** scope argument |
| keys | <code>string</code> | Like **hotkeys** first argument. Will be prefixed by 'alt+' |
| onKeydown | <code>function</code> | The keydown listener method as **hotkeys** accepts |
| onKeyup | <code>function</code> | The keyup listener method as **hotkeys** accepts |

<a name="keyboardeventmanager-ctrlaltkey">
</a>

### keyboardEventManager.ctrlAltKey(scope, keys, onKeydown, onKeyup)

Create a listener to handle KeyboardEvent "ctrl+alt"See [hotkeys-js](https://www.npmjs.com/package/hotkeys-js) package documentation.

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| scope | <code>string</code> | The scope as string like **hotkeys** scope argument |
| keys | <code>string</code> | Like **hotkeys** first argument. Will be prefixed by 'ctrl+alt+' |
| onKeydown | <code>function</code> | The keydown listener method as **hotkeys** accepts |
| onKeyup | <code>function</code> | The keyup listener method as **hotkeys** accepts |

<a name="keyboardeventmanager-attemptprompt">
</a>

### keyboardEventManager.attemptPrompt(keys, [resolvedCb], [badCombinationCb]) ⇒ <code>object</code>

**Returns**: <code>object</code> - - Returns an object with the abort function

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| keys | <code>Array.&lt;string&gt;</code> \| <code>Array.&lt;Array&gt;</code> | The keys as array of string or array with hook callback like : `[ key, (resolvedString: String[], expectedKey: String, keyIndex: Number, event: KeyboardEvent) => ?boolean ]`. The hook callback is a function that takes fours arguments: the resolved string, the expected key, the current index and the event itself. It can return false to interrupt |
| [resolvedCb] | <code>function</code> | The resolved callback. Takes the resolved string as unique argument: `(resolvedString: String[]) => void` |
| [badCombinationCb] | <code>function</code> | The callback in case of not expected key. Takes five arguments: the resolved string, the expected key, the current index, the received key that does not match the expected one and the event itself. `(resolvedString: String[], expectedKey: String, keyIndex: Number, eventKey: KeyboardEvent.key, event: KeyboardEvent) => void` |

**Example**

```js
keyboardEventManager.attemptPrompt(['f', 'o', 'o'], (resolvedKeys) => { console.log('Resolved: ', resolvedKeys.join()) })// Pass hookskeyboardEventManager.attemptPrompt(  [    [ 'f', (resolvedKeys, expectedKey, keyIndex, event) => {      console.log('Oh, a "F"!')    } ],    'o',    [ 'o', (resolvedKeys, expectedKey, keyIndex, event) => {      console.log('The last "O"!')    } ]  ],  // successCb  (resolvedKeys) => {    console.log('That\'s all guys!')  },  // bad combination callback  (resolvedKeys, expectedKey, keyIndex, eventKey, event) => {    console.log(`The key "${eventKey}" is not valid!`)  })
```

<a name="keyboardeventmanager-scopes">
</a>

### keyboardEventManager.scopes : <code>Set</code>

Returns defined scopes.

**Category**: properties  
<a name="keyboardeventmanager-install">
</a>

### KeyboardEventManager.install(_Vue, [options])

Install method for Vue

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| _Vue | <code>Vue</code> |  | Vue |
| [options] | <code>object</code> | <code>{}</code> | The options of the plugin |

