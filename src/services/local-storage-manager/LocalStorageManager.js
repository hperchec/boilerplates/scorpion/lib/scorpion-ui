/**
 * @vuepress
 * ---
 * title: "Service (local-storage-manager): LocalStorageManager class"
 * headline: "Service (local-storage-manager): LocalStorageManager class"
 * sidebarTitle: .LocalStorageManager
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import toolbox, { Hook } from '@/toolbox'
// Utils
import { getAllKeys, isDef, isUndef, toNumber, typeCheck } from '@/utils'

const { VuePluginable } = toolbox.vue

/**
 * @extends VuePluginable
 * @classdesc
 * Service **local-storage-manager**: LocalStorageManager class
 */
class LocalStorageManager extends VuePluginable {
  /**
   * Create a new LocalStorage manager
   * @param {object} [options = {}] - The constructor options
   * @param {object} [options.schema] - The local storage items object schema (`{ [itemKey: string]: string \| number \| object }`)
   * @param {string} [options.setItemEventName] - The event name to dispatch at document level when item is set (default is: 'ls-item-set')
   * @param {string} [options.removeItemEventName] - The event name to dispatch at document level when item is removed (default is: 'ls-item-removed')
   * @param {Function|Function[]} [options.onSetItem] - The callback function(s) to call when item is set (See also onSetItem method)
   * @param {Function|Function[]} [options.onRemoveItem] - The callback function(s) to call when item is removed (See also onRemoveItem method)
   */
  constructor (options = {}) {
    if (!(LocalStorageManager.#IS_INITIALIZED)) {
      throw new Error('LocalStorageManager constructor: can\'t create new instance of LocalStorageManager class before initiliazing. Call LocalStorageManager.init() static method.')
    }
    // Call VuePluginable constructor
    super()
    // Set debugging private property
    if (this.__v_descriptor__) {
      this.__v_descriptor__.namespace = 'service:local-storage-manager'
      this.__v_descriptor__.description = 'Access the LocalStorageManager instance'
      // @ts-ignore
      this.__v_descriptor__.externalLink = __getDocUrl__('/api/services/local-storage-manager/LocalStorageManager')
    }
    // Process schema option
    if (isDef(options.schema)) {
      if (typeCheck(Object, options.schema)) {
        for (const key in options.schema) {
          this.schema[key] = options.schema[key]
        }
      } else {
        throw new Error('LocalStorageManager constructor: options.schema must be an object.')
      }
    }
    // Set event names
    if (isDef(options.setItemEventName)) this.setItemEventName = options.setItemEventName
    if (isDef(options.removeItemEventName)) this.removeItemEventName = options.removeItemEventName
    // Process hooks options
    if (isDef(options.onSetItem)) this.onSetItem(options.onSetItem)
    if (isDef(options.onRemoveItem)) this.onRemoveItem(options.onRemoveItem)
    // Overwrite orignal setItem method to dispatch event at document level
    const newSetItemMethod = (key, value) => {
      const event = new Event(this.setItemEventName)
      // @ts-ignore
      event.value = value // Save value
      // @ts-ignore
      event.key = key // Save key
      document.dispatchEvent(event) // Dispatch event
      LocalStorageManager.originalSetItemMethod.apply(localStorage, [ key, value ]) // Then call original method
    }
    localStorage.constructor.prototype.setItem = newSetItemMethod
    // Overwrite orignal setItem method to dispatch event at document level
    const newRemoveItemMethod = (key) => {
      const event = new Event(this.removeItemEventName)
      // @ts-ignore
      event.key = key // Save key
      document.dispatchEvent(event) // Dispatch event
      LocalStorageManager.originalRemoveItemMethod.apply(localStorage, [ key ]) // Then call original method
    }
    localStorage.constructor.prototype.removeItem = newRemoveItemMethod
    // Set onSetItem handler
    const localStorageSetHandler = (e) => {
      this.#hooks.SET_ITEM.exec(e.key, this.items[e.key], e.value)
    }
    document.addEventListener(this.setItemEventName, localStorageSetHandler, false)
    // Set onRemoveItem handler
    const localStorageRemoveHandler = (e) => {
      this.#hooks.REMOVE_ITEM.exec(e.key)
    }
    document.addEventListener(this.removeItemEventName, localStorageRemoveHandler, false)
    // Loop on values in localStorage
    const items = {}
    for (const [ key, value ] of Object.entries(localStorage)) {
      // Based on defined schema, cast values in target cache
      // (set schema if not set)
      if (isUndef(this.schema[key])) {
        this.schema[key] = this.getTypeFromLSValue(value)
      }
      const castedValue = this.castMethods[this.schema[key]](value)
      items[key] = typeof castedValue === 'object'
        ? this.#proxifyObjectItem(castedValue, key)
        : castedValue
    }
    // Init cache
    this.#initCache(items)
  }

  /**
   * Static private properties
   */

  static #IS_INITIALIZED = false // False by default

  /**
   * Public static properties
   */

  static originalSetItemMethod = undefined

  static originalRemoveItemMethod = undefined

  // Overrides static VuePluginable properties
  // static reactive = true
  static rootOption = 'localStorageManager'
  static aliases = [ 'LS' ]

  /**
   * Private properties
   */

  /**
   * Hooks
   * @category private properties
   * @type {object}
   */
  #hooks = {
    SET_ITEM: new Hook('SET_ITEM', [
      (key, cacheValue, lsValue) => {
        // console.log(`Set item "${key}":`, cacheValue)
      }
    ]),
    REMOVE_ITEM: new Hook('REMOVE_ITEM', [
      (key) => {
        // console.log(`Remove item "${key}"`)
      }
    ])
  }

  /**
   * schema
   * @category private properties
   * @type {Proxy}
   */
  // @ts-ignore
  #schema = new Proxy({}, {
    set: (obj, prop, value) => {
      if (typeof value !== 'string' || !([ 'string', 'number', 'object' ].includes(value))) {
        throw new Error(`LocalStorageManager: schema "${String(prop)}" property must be one of the following values: "string", "number" or "object".`)
      }
      obj[prop] = value
      return true
    }
  })

  /**
   * cache
   * @category private properties
   * @type {Proxy}
   * @description
   * Store local storage items as cache object
   */
  #cache = undefined

  /**
   * Properties
   */

  /**
   * castMethods
   * @category properties
   * @type {object}
   */
  castMethods = {
    string: function (value) {
      return String(value)
    },
    number: function (value) {
      return toNumber(value)
    },
    // objects are stored as string in localStorage
    object: function (value) {
      return JSON.parse(value)
    }
  }

  /**
   * setItemEventName
   * @category properties
   * @type {string}
   */
  setItemEventName = 'ls-item-set'

  /**
   * removeItemEventName
   * @category properties
   * @type {string}
   */
  removeItemEventName = 'ls-item-removed'

  /**
   * Accessors & mutators
   */

  /**
   * Local storage cache
   * @category properties
   * @readonly
   * @type {object}
   */
  get items () {
    // return this._vm.items
    return this.#cache
  }

  /**
   * Local storage schema (key/type)
   * @category properties
   * @readonly
   * @type {object}
   */
  get schema () {
    return this.#schema
  }

  /**
   * Private methods
   */

  /**
   * initCache
   * @param {object} items - The items object
   * @returns {void}
   * @description
   * Creates Proxy from items object
   */
  #initCache (items) {
    this.#cache = new Proxy(items, {
      // Delete in local storage when deleting prop
      deleteProperty: (obj, prop) => {
        delete obj[prop]
        delete this.schema[prop] // delete in schema
        // @ts-ignore
        localStorage.removeItem(prop)
        return true
      },
      // Overwrite Object type setter
      set: (obj, prop, value) => {
        if (typeof value === 'object') {
          // @ts-ignore
          obj[prop] = this.#proxifyObjectItem(value, prop)
        } else {
          obj[prop] = value
        }
        // (set schema if not set)
        if (isUndef(this.schema[prop])) {
          this.schema[prop] = typeof value
        }
        // Set in localStorage
        // @ts-ignore
        this.#setFromCacheValue(prop)
        // return true
        return true
      }
    })
  }

  /**
   * proxifyObjectItem
   * @category private methods
   * @param {object} target - The object to "proxify"
   * @param {string} cacheProp - The target item key (far parent)
   * @returns {Proxy} Returns proxy object
   * @description
   * Transform object to proxy to "observe" changes
   */
  #proxifyObjectItem (target, cacheProp) {
    const getValue = (val) => {
      return typeof val === 'object'
        ? this.#proxifyObjectItem(val, cacheProp)
        : val
    }
    const tmpObj = {}
    for (const key in target) {
      tmpObj[key] = getValue(target[key])
    }
    const proxy = new Proxy(tmpObj, {
      // Update in cache when deleting prop
      deleteProperty: (obj, prop) => {
        delete obj[prop]
        // Sync localStorage value with cache
        this.#setFromCacheValue(cacheProp)
        return true
      },
      // Overwrite Object type setter
      set: (obj, prop, value) => {
        obj[prop] = getValue(value)
        // Sync localStorage value with cache
        this.#setFromCacheValue(cacheProp)
        // Return true
        return true
      }
    })
    // @ts-ignore
    return proxy
  }

  /**
   * setFromCacheValue
   * @category private methods
   * @param {string} key - The item key
   * @returns {void}
   * @description
   * Converts cache value to string and sets in local storage
   */
  #setFromCacheValue (key) {
    const cacheValue = this.items[key]
    let lsValue
    switch (typeof cacheValue) {
      case 'object':
        lsValue = JSON.stringify(cacheValue)
        break
      default:
        lsValue = cacheValue
        break
    }
    localStorage.setItem(key, lsValue)
  }

  /**
   * Methods
   */

  /**
   * getTypeFromLSValue
   * @param {string} lsValue - The local storage item value (string)
   * @returns {string} Returns 'string', 'number' or 'object'
   * @description
   * Auto-detect type of local storage item value
   * @example
   * localStorageManager.getTypeFromLSValue('example') // => 'string'
   * localStorageManager.getTypeFromLSValue('42') // => 'number'
   * localStorageManager.getTypeFromLSValue('{ "prop1": "test1", "prop2": "test2" }') // => 'object'
   */
  getTypeFromLSValue (lsValue) {
    let type
    // First, check object
    try {
      if (this.castMethods.object(lsValue)) type = 'object'
    } catch (err) {
      // Then, number
      try {
        if (this.castMethods.number(lsValue)) type = 'number'
      } catch (err) {
        // Default is String
        type = 'string'
      }
    }
    return type
  }

  /**
   * onSetItem
   * @category public methods
   * @param {Function|Function[]} func - The function(s) to append to the hook queue
   * @returns {void}
   * @description
   * **SET_ITEM** hook append method.
   * The function to append, takes three arguments:
   *
   * ```ts
   * function (key: String, cacheValue: any, lsValue: String) => void
   * ```
   *
   * - **key**: `{String}` the item key
   * - **cacheValue**: `{any}` the cache value
   * - **lsValue**: `{String}` the value in local storage
   * @example
   * localStorageManager.onSetItem((key, cacheValue, lsValue) => {
   *   console.log(`Set item "${key}":`, cacheValue)
   * })
   */
  onSetItem (func) {
    this.#hooks.SET_ITEM.append(func)
  }

  /**
   * onRemoveItem
   * @category public methods
   * @param {Function|Function[]} func - The function(s) to append to the hook queue
   * @returns {void}
   * @description
   * **REMOVE_ITEM** hook append method.
   * The function to append, takes one argument:
   *
   * ```ts
   * function (key: String) => void
   * ```
   *
   * - **key**: `{String}` the item key
   * @example
   * localStorageManager.onRemoveItem((key) => {
   *   console.log(`Remove item "${key}"`)
   * })
   */
  onRemoveItem (func) {
    this.#hooks.REMOVE_ITEM.append(func)
  }

  /**
   * Check if provided item key is defined
   * @param {string} key - The item key
   * @returns {boolean} Returns true if key is set, else false
   */
  hasItem (key) {
    return Object.keys(this.items).includes(key)
  }

  /**
   * Overrides the default toJSON object method for JSON.stringify() calls
   * @returns {object} - The instance data to be serialized
   */
  toJSON () {
    const keys = getAllKeys(this, {
      excludeKeys: [
        /^_/, // private properties that starts with '_'
        'toJSON',
        'toString',
        'constructor'
      ]
    })
    const jsonObj = keys.reduce((obj, key) => {
      obj[key] = this[key]
      return obj
    }, {})
    return jsonObj
  }

  /**
   * Static methods
   */

  /**
   * Check compatibility. Throws error if localStorage variable is undefined
   * @returns {void}
   */
  static checkCompat () {
    // Check if localStorage is defined
    if (isUndef(localStorage)) {
      throw new Error('LocalStorageManager class: localStorage is undefined')
    }
  }

  /**
   * Initialize LocalStorageManager. Check compatibility + event listener to observe changes.
   * @returns {void}
   */
  static init () {
    // Check compatibility
    this.checkCompat()
    // Get original setItem method
    this.originalSetItemMethod = localStorage.setItem
    // Get original removeItem method
    this.originalRemoveItemMethod = localStorage.removeItem
    // Mark as initialized
    this.#IS_INITIALIZED = true
  }

  /**
   * Check if a localStorage key is defined
   * @param {string} key - The key in local storage
   * @returns {boolean} Returns true if key is defined, else false
   */
  static isKeyDefined (key) {
    return Object.keys(localStorage).includes(key)
  }

  /**
   * Install method for Vue
   * @param {Vue} _Vue - Vue
   * @param {object} [options = {}] - The options of the plugin
   */
  static install (_Vue, options = {}) {
    const Vue = _Vue
    super.install(Vue, options)
  }
}

// Return LocalStorageManager class
export default LocalStorageManager
