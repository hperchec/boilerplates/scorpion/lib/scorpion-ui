import makeConfigurableOption from './make-configurable-option'

export default {
  makeConfigurableOption
}
