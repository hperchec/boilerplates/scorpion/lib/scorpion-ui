---
title: Service (modal-manager)
headline: Service (modal-manager)
sidebarTitle: modal-manager
initialOpenGroupIndex: -1
prev: ../
next: ./options
---

# Service (modal-manager)

<a name="service"></a>

The service "modal-manager" is designed to make authentication managemnent easier.

## Register

On registering, the service will set root instance options:at created hook, `ModalContainer` component is automatically add to root component.It injects properties to Vue root instance prototype `$app`:- `$app.ui.showModal`- `$app.ui.hideModal`It will also define Vue components:- `Core.context.vue.components.modals.BaseModal`- `Core.context.vue.components.modals.ModalContainer`- `Core.context.vue.components.modals.ModalOverlay`- `Core.context.vue.components.modals.Modal`

## Create

Returns an instance of `ModalManager` created with options.

**Returns**: <code>module:services/modal-manager/ModalManager~ModalManager</code> - Once services are created:

```js
Core.service('modal-manager')
```

## Vue

### Plugin

On registering, service Vue plugin will be set in `Core.context.vue.plugins['modal-manager']`.

Plugin will be automatically used by Vue.

The following properties will be set to Vue prototype:```jsVue.prototype.$modalManager // => Core.service('modal-manager')Vue.prototype.$modals // => Core.service('modal-manager')```

### Root instance

<a name="app"></a>

#### Prototype $app property

On registering, the service will inject properties to \`Core.context.vue.root.prototype.app\`.

> Properties will be shared by all components under \`vm.$app\` where \`vm\` is the component instance.

<a name="app-usersettings"></a>

##### User settings

Service will define the following user settings keys:

##### Others

## Expose (service context)

The service will expose the following properties under `Core.context.services['modal-manager']`:
<a name="expose-modal">
</a>

### .Modal

> Exposed as `Core.context.services['modal-manager'].Modal`

**See**: [Modal](./Modal)

<a name="expose-modalmanager">
</a>

### .ModalManager

> Exposed as `Core.context.services['modal-manager'].ModalManager`

**See**: [ModalManager](./ModalManager)

