export default EventBus;
declare const EventBus_base: any;
declare class EventBus extends EventBus_base {
    [x: string]: any;
    static reactive: boolean;
    static rootOption: string;
    static aliases: any[];
    static install(_Vue: Vue, options?: object): void;
    constructor(options?: {
        strictMode?: boolean;
    });
    get listeners(): any;
    emit(...args: any[]): void;
    off(...args: any[]): void;
    on(...args: any[]): void;
    once(...args: any[]): void;
    toJSON(): object;
    #private;
}
//# sourceMappingURL=EventBus.d.ts.map