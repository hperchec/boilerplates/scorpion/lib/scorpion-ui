/**
 * @vuepress
 * ---
 * title: isPromise
 * headline: isPromise
 * sidebarTitle: isPromise
 * ---
 */

import isDef from './is-def'

/**
 * isPromise
 * @param {*} value - The value to check
 * @returns {boolean} Returns true if value is a Promise
 * @description
 * Check if value is Promise
 * @example
 * isPromise(new Promise(...)) // => true
 * isPromise(new Date()) // => false
 */
export const isPromise = function (value) {
  return (
    isDef(value) &&
    typeof value.then === 'function' &&
    typeof value.catch === 'function'
  )
}

export default isPromise
