---
title: Template tools - utils
headline: Template tools - utils
sidebarDepth: 0
prev: false
next: false
---

# Template tools - utils

<a name="utils">
</a>

## .utils : <code>object</code>

**Schema**:

- [.utils](#utils) : <code>object</code>
  - [.mapComponent(path)](#utils-mapcomponent) ⇒ <code>function</code>
  - [.mergeContext(obj, key, value)](#utils-mergecontext) ⇒ <code>void</code>

<a name="utils-mapcomponent">
</a>

### utils.mapComponent(path) ⇒ <code>function</code>

It is a "hack" to define component through function.Use it in template, when components are dependent on each otherSee also https://v2.vuejs.org/v2/guide/components-dynamic-async.html#Async-Components.

**Returns**: <code>function</code> - Returns a function that returns a Promise

| Param | Type | Description |
| --- | --- | --- |
| path | <code>string</code> | The path from `Core.context` |

<a name="utils-mergecontext">
</a>

### utils.mergeContext(obj, key, value) ⇒ <code>void</code>

Merge Core context objects

| Param | Type | Description |
| --- | --- | --- |
| obj | <code>\*</code> | The Core context property target |
| key | <code>string</code> | The key to merge |
| value | <code>\*</code> | The value |

**Example**

```js
// In template extend:mergeContext(Core.context.vue.components, 'App', App)
```

