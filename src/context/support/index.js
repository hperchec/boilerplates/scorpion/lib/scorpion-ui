/**
 * @vuepress
 * ---
 * title: Support
 * headline: Support
 * sidebarTitle: Support
 * initialOpenGroupIndex: -1 # optional, defaults to 0, defines the index of initially opened subgroup
 * prev: ../
 * next: ./collection/
 * ---
 */

import collection from './collection'
import errors from './errors'
import model from './model'

/**
 * @alias support
 * @typicalname Core.context.support
 * @description
 * Context shared classes / functions / utilities...
 */
export default {
  /**
   * @alias support.collection
   * @see {@link ./collection collection}
   */
  collection,
  /**
   * @alias support.errors
   * @see {@link ./errors errors}
   */
  errors,
  /**
   * @alias support.model
   * @see {@link ./model model}
   */
  model
}
