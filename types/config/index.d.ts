export namespace config {
    namespace globals {
        let APP_NAME: string;
        namespace VERSION {
            let CURRENT: string;
        }
    }
    namespace vue {
        namespace root {
            let name: string;
            let targetElement: string;
            let autoMount: boolean;
        }
    }
    let services: {
        'api-manager': any;
        'event-bus': {
            strictMode: boolean;
        };
        'error-manager': {
            autoRegister: boolean;
        };
        i18n: {
            availableLocales: string[];
            defaultLocale: string;
            fallbackLocale: string;
        };
        'local-storage-manager': {
            setItemEventName: string;
            deleteItemEventName: string;
        };
        logger: {
            log: boolean;
            types: {
                info: {
                    badgeContent: string;
                    badgeColor: string;
                    badgeBgColor: string;
                    messageColor: string;
                    prependMessage: string;
                };
                advice: {
                    badgeContent: string;
                    badgeColor: string;
                    badgeBgColor: string;
                    messageColor: string;
                    prependMessage: string;
                };
                warning: {
                    badgeContent: string;
                    badgeColor: string;
                    badgeBgColor: string;
                    messageColor: string;
                    prependMessage: string;
                };
                error: {
                    badgeContent: string;
                    badgeColor: string;
                    badgeBgColor: string;
                    messageColor: string;
                    prependMessage: string;
                };
                system: {
                    badgeContent: string;
                    badgeColor: string;
                    badgeBgColor: string;
                    messageColor: string;
                    prependMessage: string;
                };
                vue: {
                    badgeContent: string;
                    badgeColor: string;
                    badgeBgColor: string;
                    messageColor: string;
                    prependMessage: string;
                };
                router: {
                    badgeContent: string;
                    badgeColor: string;
                    badgeBgColor: string;
                    messageColor: string;
                    prependMessage: string;
                };
                store: {
                    badgeContent: string;
                    badgeColor: string;
                    badgeBgColor: string;
                    messageColor: string;
                    prependMessage: string;
                };
                websocket: {
                    badgeContent: string;
                    badgeColor: string;
                    badgeBgColor: string;
                    messageColor: string;
                    prependMessage: string;
                };
                'local-database': {
                    badgeContent: string;
                    badgeColor: string;
                    badgeBgColor: string;
                    messageColor: string;
                    prependMessage: string;
                };
            };
        };
        'modal-manager': {
            baseZIndex: number;
        };
        router: {
            initOnRootInstanceMounted: boolean;
            catchAllRedirect: any;
        };
        'toast-manager': {
            toasters: {
                Example: {};
            };
        };
        'websocket-manager': {
            connections: {};
        };
    };
}
export default config;
//# sourceMappingURL=index.d.ts.map