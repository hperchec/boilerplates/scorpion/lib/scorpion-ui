/**
 * Use docgen.config.js file here (with vuepress-jsdoc).
 * The following vue-docgen configuration options will be ignored:
 * - componentsRoot
 * - components
 * - outDir
 * Because vuepress-jsdoc will call vue-docgen-cli compileTemplates method FOR EACH vue file.
 * The output directory is defined in vuepress-jsdoc configuration.
 * See also https://vue-styleguidist.github.io/docs/docgen-cli.html#config
 */

// Use @hperchec/vue-docgen-template
const { createConfig } = require('@hperchec/vue-docgen-template')

const config = {
  /* config */
}

const templateOptions = {
  /* Template options */
}

module.exports = createConfig(config, templateOptions)
