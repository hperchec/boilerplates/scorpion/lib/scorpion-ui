import GlobalMixin from './GlobalMixin'

/**
 * @typicalname Core.context.vue.mixins.global
 * @description
 * Contains all app global mixins.
 */
export const global = {
  /**
   * @alias global.GlobalMixin
   */
  GlobalMixin
}

export default global
