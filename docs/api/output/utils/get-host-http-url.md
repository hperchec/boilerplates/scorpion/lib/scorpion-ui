---
title: getHostHttpUrl
headline: getHostHttpUrl
sidebarTitle: getHostHttpUrl
---

# getHostHttpUrl

<a name="gethosthttpurl">
</a>

## getHostHttpUrl ⇒ <code>string</code> \| <code>boolean</code>

Returns server url

**Returns**: <code>string</code> \| <code>boolean</code> - Returns the substring of url

| Param | Type | Description |
| --- | --- | --- |
| str | <code>string</code> | The url string |

**Example**

```js
getHostHttpUrl('http://example.com/blog/posts?id=1') // return 'http://example.com/'getHostHttpUrl('example.com/blog/posts/') // fail
```

