export function mergeState(generated: object, raw: object): object;
export function mergeGetters(generated: object, raw: object): object;
export function mergeActions(generated: object, raw: object): object;
export function mergeMutations(generated: object, raw: object): object;
export default function createModule(model: any, options: object, rawModule: object): object;
//# sourceMappingURL=create-module.d.ts.map