import Core from '@/Core'
import { isUndef, isDef, isVueComponent } from '@/utils'

export default {
  /**
   * Vue plugin install method
   * @param {typeof import("vue").default} Vue - The vue prototype
   * @param {object} [options] - (Optional) The plugin options
   * @returns {void}
   */
  install (Vue, options) {
    /**
     * App 'remote'
     */
    // Define '$app' for each Vue instance
    Object.defineProperty(Vue.prototype, '$app', {
      get () {
        // Set debugging private property
        if (!this.$root._app.__v_descriptor__) {
          this.$root._app.__v_descriptor__ = {
            namespace: 'CorePlugin',
            description: 'Access the Vue root instance extensions',
            optionType: 'nested',
            type: 'object',
            // @ts-ignore
            externalLink: __getDocUrl__('/api/context/vue/plugins/core-plugin')
          }
        }
        return this.$root._app
      }
    })

    /**
     * Filters
     *
     * Define each Core filter as a Vue filter.
     * Also, assign filters object to:
     * - Vue.prototype.$filters
     */
    for (const name in Core.context.vue.filters) {
      Vue.filter(name, Core.context.vue.filters[name])
    }
    // Define '$filters' for each Vue instance
    Vue.prototype.$filters = Core.context.vue.filters
    // Set debugging private property
    Vue.prototype.$filters.__v_descriptor__ = {
      namespace: 'CorePlugin',
      description: 'Access the globally defined Vue filters',
      optionType: 'prototype', // not nested because thisArg is not bound
      type: 'object',
      // @ts-ignore
      externalLink: __getDocUrl__('/api/context/vue/plugins/core-plugin')
    }

    /**
     * Mixins utilisation
     *
     * For each mixin in Core.context.vue.mixins.global:
     * define as global mixin
     */
    for (const name in Core.context.vue.mixins.global) {
      Vue.mixin(Core.context.vue.mixins.global[name])
    }

    /**
     * Directives utilisation
     *
     * For each directive in Core.context.vue.directives:
     * apply directive to Vue
     */
    for (const name in Core.context.vue.directives) {
      // Check createDirective()
      const createDirective = Core.context.vue.directives[name].createDirective
      if (isUndef(createDirective)) {
        throw new Error(`Core vue plugin: error during "${name}" directive registration: directive does not have "createDirective" method.`)
      }
      // Check options && options.id
      const directiveOptions = Core.context.vue.directives[name].options
      if (isUndef(directiveOptions)) {
        throw new Error(`Core vue plugin: error during "${name}" directive registration: directive does not have "options" property.`)
      }
      const directiveId = directiveOptions.id
      if (isUndef(directiveId)) {
        throw new Error(`Core vue plugin: error during "${name}" directive registration: directive options does not have "id" property.`)
      }
      Vue.directive(directiveId, createDirective(directiveOptions))
    }

    /**
     * Components registration
     *
     * Define each 'Core.context.vue.components.commons' component as global component
     */

    /**
     * Find Vue component options in nested object (VueComponent constructor).
     * As components are loaded by vue-loader-extension ('@hperchec/scorpion-ui/shared/webpack/loaders/vue-loader-extension')
     * we also check if objects have '__isVueComponentOptions' property defined.
     * @param {object} target - The target object
     * @param {object} [components = {}] - To stack components
     * @returns {object} Returns components object
     */
    function findComponents (target, components = {}) {
      if (typeof target === 'object') {
        for (const key in target) {
          const value = target[key]
          if (isVueComponent(value)) {
            // Check if component has "name" property defined
            if (isUndef(value.name)) {
              console.warn(`Core vue plugin: error during "commons" components registration: component "${key}" doesn't have "name" property defined. Please give a name to the component.`)
            } else {
              // Check if name is not already taken
              if (isDef(components[value.name])) {
                console.warn(`Core vue plugin: error during "commons" components registration: duplicata for component name "${value.name}".`)
              } else {
                components[value.name] = value
              }
            }
          } else {
            const nested = findComponents(value, components)
            for (const _key in nested) {
              components[_key] = nested[_key]
            }
          }
        }
      }
      return components
    }

    const commonComponents = findComponents(Core.context.vue.components.commons)

    for (const key in commonComponents) {
      // Get component options object
      const component = commonComponents[key]
      // Check if component has a name already taken
      // @ts-ignore
      const componentExists = component.name in Vue.options.components
      if (componentExists) {
        console.warn(`Core vue plugin: error during "commons" components registration: component name "${component.name}" already taken. Please provide an available name.`)
      } else {
        // Define global component
        Vue.component(component.name, component)
      }
    }
  }
}
