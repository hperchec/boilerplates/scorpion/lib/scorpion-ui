---
title: CSSClasses class
headline: CSSClasses class
sidebarTitle: CSSClasses
sidebarDepth: 0
---

# CSSClasses class

<a name="cssclasses">
</a>

## CSSClasses

CSSClasses wrapper class to manipulate css classes

**Schema**:

- [CSSClasses](#cssclasses)
  - [new CSSClasses(classes)](#newcssclassesnew)
  - _instance_
    - [.toArray()](#cssclasses-toarray) ⇒ <code>Array.&lt;string&gt;</code>
    - [.toString()](#cssclasses-tostring) ⇒ <code>string</code>
    - [.toObject()](#cssclasses-toobject) ⇒ <code>object</code>
  - _static_
    - [.parseCssClassValue(value)](#cssclasses-parsecssclassvalue) ⇒ <code>object</code>
    - [.extractClassesFromStr(classesStr)](#cssclasses-extractclassesfromstr) ⇒ <code>Array.&lt;string&gt;</code>
    - [.objectifyClassesArray(classes)](#cssclasses-objectifyclassesarray) ⇒ <code>object</code>
    - [.sanitizeClassesValue(str)](#cssclasses-sanitizeclassesvalue) ⇒ <code>string</code>
    - [.mergeCssClasses(...args)](#cssclasses-mergecssclasses) ⇒ <code>object</code>

<a name="newcssclassesnew">
</a>

### new CSSClasses(classes)

| Param | Type | Description |
| --- | --- | --- |
| classes | <code>string</code> \| <code>Array.&lt;string&gt;</code> \| <code>object</code> | The css classes as Vue accepts |

<a name="cssclasses-toarray">
</a>

### cssClasses.toArray() ⇒ <code>Array.&lt;string&gt;</code>

Returns css classes as array

**Returns**: <code>Array.&lt;string&gt;</code> - The classes as array

<a name="cssclasses-tostring">
</a>

### cssClasses.toString() ⇒ <code>string</code>

ToString override to string default method

**Returns**: <code>string</code> - The classes as string

<a name="cssclasses-toobject">
</a>

### cssClasses.toObject() ⇒ <code>object</code>

Returns classes as object

**Returns**: <code>object</code> - A classes object

<a name="cssclasses-parsecssclassvalue">
</a>

### CSSClasses.parseCssClassValue(value) ⇒ <code>object</code>

CSS class value can be string or any formats accepted by Vue for class attribute

**Returns**: <code>object</code> - - An classes object format

| Param | Type | Description |
| --- | --- | --- |
| value | <code>string</code> \| <code>Array.&lt;string&gt;</code> \| <code>object</code> | The CSS class value |

<a name="cssclasses-extractclassesfromstr">
</a>

### CSSClasses.extractClassesFromStr(classesStr) ⇒ <code>Array.&lt;string&gt;</code>

Takes an array of css classes as unique parameter and check if it is valid

**Returns**: <code>Array.&lt;string&gt;</code> - The classes as array

| Param | Type | Description |
| --- | --- | --- |
| classesStr | <code>string</code> | The classes string |

<a name="cssclasses-objectifyclassesarray">
</a>

### CSSClasses.objectifyClassesArray(classes) ⇒ <code>object</code>

Takes an array of css classes an returns as object

**Returns**: <code>object</code> - The classes object

| Param | Type | Description |
| --- | --- | --- |
| classes | <code>Array.&lt;string&gt;</code> | The classes array |

<a name="cssclasses-sanitizeclassesvalue">
</a>

### CSSClasses.sanitizeClassesValue(str) ⇒ <code>string</code>

Sanitize classes value to make it more readable

**Returns**: <code>string</code> - Classes as string

| Param | Type | Description |
| --- | --- | --- |
| str | <code>string</code> | The classes str to parse |

<a name="cssclasses-mergecssclasses">
</a>

### CSSClasses.mergeCssClasses(...args) ⇒ <code>object</code>

MergeCssClasses classes value

**Returns**: <code>object</code> - The merged classes

| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | Arguments to pass to `classnames` library |

