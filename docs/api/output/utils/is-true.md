---
title: isTrue
headline: isTrue
sidebarTitle: isTrue
---

# isTrue

<a name="istrue">
</a>

## isTrue ⇒ <code>boolean</code>

Check if value is boolean and true (=== true)

**Returns**: <code>boolean</code> - Returns true if value strictly equals true

| Param | Type | Description |
| --- | --- | --- |
| value | <code>\*</code> | The variable to check |

**Example**

```js
isTrue(true) // => trueisTrue('Hello') // => false
```

