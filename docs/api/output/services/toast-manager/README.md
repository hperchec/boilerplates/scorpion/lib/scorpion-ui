---
title: Service (toast-manager)
headline: Service (toast-manager)
sidebarTitle: toast-manager
initialOpenGroupIndex: -1
prev: ../
next: ./options
---

# Service (toast-manager)

<a name="service"></a>

The service "toast-manager" is for UI toast management.It can creates "toasters" and then publish toasts.

## Register

On registering, the service will inject properties to Vue root instance prototype `$app`:- `$app.ui.toast`- `$app.ui.toaster`It will also define Vue components:- `Core.context.vue.components.toasts.BaseToast`- `Core.context.vue.components.toasts.BaseToaster`- `Core.context.vue.components.toasts.ExampleToaster`- `Core.context.vue.components.toasts.Toast`

## Create

Returns an instance of `ToastManager` created with options.

**Returns**: <code>module:services/toast-manager/ToastManager~ToastManager</code> - Once services are created:

```js
Core.service('toast-manager')
```

## Vue

### Plugin

On registering, service Vue plugin will be set in `Core.context.vue.plugins['toast-manager']`.

Plugin will be automatically used by Vue.

The following properties will be set to Vue prototype:```jsVue.prototype.$toastManager // => Core.service('toast-manager')```

### Root instance

<a name="app"></a>

#### Prototype $app property

On registering, the service will inject properties to \`Core.context.vue.root.prototype.app\`.

> Properties will be shared by all components under \`vm.$app\` where \`vm\` is the component instance.

<a name="app-usersettings"></a>

##### User settings

Service will define the following user settings keys:

##### Others

## Expose (service context)

The service will expose the following properties under `Core.context.services['toast-manager']`:
<a name="expose-toast">
</a>

### .Toast

> Exposed as `Core.context.services['toast-manager'].Toast`

**See**: [Toast](./Toast)

<a name="expose-toaster">
</a>

### .Toaster

> Exposed as `Core.context.services['toast-manager'].Toaster`

**See**: [Toaster](./Toaster)

<a name="expose-toastmanager">
</a>

### .ToastManager

> Exposed as `Core.context.services['toast-manager'].ToastManager`

**See**: [ToastManager](./ToastManager)

