/**
 * @vuepress
 * ---
 * title: Core class
 * headline: Core class
 * sidebarTitle: Core
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: ./utils/
 * next: ./Service
 * ---
 */

// utils
import {
  deepMerge as merge,
  isUndef,
  isDef,
  typeCheck
} from './utils'
// Global configuration
import globalConfig from './config'
// Context
import context from './context'
// Built-in services
import builtInServices from './services'
import Service from './Service'
// Hook class
import toolbox from './toolbox'
const { Hook } = toolbox

const VUE_LIFECYCLE_HOOKS = [
  'beforeCreate',
  'created',
  'beforeMount',
  'mounted',
  'beforeUpdate',
  'updated',
  'beforeDestroy',
  'destroyed',
  'activated',
  'deactivated',
  'errorCaptured',
  'serverPrefetch',
  'renderTracked',
  'renderTriggered'
]

/**
 * getMergeStrategyWithPriority
 * @ignore
 * @description
 * Takes original Vue merge startegy as first parameter. Returns a merge strategy function
 * that check if the hook functions have 'priority' property and sort them based on it.
 * @param {Function} originalMergeStrat - The original merge strategy function
 * @returns {Function} Returns the merge strategy function
 */
const getMergeStrategyWithPriority = (originalMergeStrat) => {
  return function (toVal, fromVal) {
    // Created hook 'priority' property can be an integer between 0 and X or undefined
    const createdArray = originalMergeStrat(toVal, fromVal).sort(function (a, b) {
      const pA = typeof a.priority !== 'number' ? undefined : a.priority
      const pB = typeof b.priority !== 'number' ? undefined : b.priority
      return (pA === undefined && pB === undefined)
        ? 0
        : (pA === undefined)
          ? 1
          : (pB === undefined)
            ? -1
            : (pA - pB)
    })
    return createdArray
  }
}

const log = (...args) => {
  (isDef(console.info) ? console.info : console.log)(`%c[Core]%c ${args.shift()}`, 'font-weight: bold; color: #F65D6C;', 'color: black;', ...args)
}
const error = (msg) => {
  throw new Error(`[Core] - ${msg}`)
}

/**
 * @classdesc
 * Scorpion UI Core class
 */
class Core {
  /**
   * Static public properties
   */

  /**
   * @category static properties
   * @type {string}
   * @readonly
   * @description
   * Core version
   */
  static get version () {
    // @ts-ignore
    return __VERSION__
  }

  /**
   * @category static properties
   * @type {boolean}
   * @readonly
   * @description
   * Returns true if Core is initialized
   */
  static get isInitialized () {
    return this.#IS_INITIALIZED
  }

  /**
   * @category static properties
   * @type {boolean}
   * @readonly
   * @description
   * Returns true if Core is setup
   */
  static get isSetup () {
    return this.#IS_SETUP
  }

  /**
   * @category static properties
   * @type {typeof Vue}
   * @readonly
   * @default undefined
   * @description
   * Vue class reference. Will be defined once `init()` is called
   */
  static get Vue () {
    return this.#Vue
  }

  /**
   * @category static properties
   * @type {typeof globalConfig}
   * @description
   * Contains default configuration.
   * Will be overwritten once configure() is called.
   */
  static get config () {
    return this.#config
  }

  /**
   * @category static properties
   * @static
   * @memberof Core
   * @type {typeof context}
   * @description
   * The app context. See also {@link ./context context} documentation.
   */
  static context = context

  /**
   * Public static methods
   */

  /**
   * @category static methods
   * @param {typeof Vue} _Vue - Vue
   * @returns {void}
   * @description
   * Initialize Core. Takes `Vue` as first argument. It will:
   *
   * - set `Core.Vue`
   * - register services
   * - call "INITIALIZED" hook
   */
  static init (_Vue) {
    // Throw error if already initialized
    if (this.isInitialized) error('Already initialized...')
    // Assign Vue ref
    this.#Vue = _Vue
    // Apply custom merge strategy for each Vue lifecycle hook
    for (const hookName of VUE_LIFECYCLE_HOOKS) {
      const originalMergeStrategy = this.#Vue.config.optionMergeStrategies[hookName]
      this.#Vue.config.optionMergeStrategies[hookName] = getMergeStrategyWithPriority(originalMergeStrategy)
    }
    // Mark as initialized
    this.#IS_INITIALIZED = true
    // Initialized hook
    this.#hooks.INITIALIZED.exec()
    log('Initialized ✔')
  }

  /**
   * registerService
   * @category static methods
   * @param {Service} service - The service object
   * @returns {void}
   * @description
   * Register a service. Takes a service object as first argument. See services documentation.
   * Core must be _initialized_ before registering service.
   */
  static registerService (service) {
    // Check if instance of Service
    if (!(typeCheck(Service, service))) error('registerService method requires instance of Service as first argument')
    const serviceDef = service.getDefinition()
    // Get service identifier
    const identifier = serviceDef.identifier
    // Check if service is already registered
    if (isDef(this.context.services[identifier])) error(`Service "${identifier}" is already registered`)
    // Exec register method
    serviceDef.register(this) // Takes Core as first argument
    // Contextualize
    // this.context.services[identifier] = service
    Reflect.set(this.context.services, identifier, service, this)
    // If service is "pluginable", contextualize vue plugin
    if (isDef(serviceDef.vuePlugin)) {
      const plugin = serviceDef.vuePlugin(this.context.services[identifier].getNamespace(), this) // Takes Core as second argument
      const pluginName = `service:${identifier}-plugin`
      const pluginOptions = serviceDef.pluginOptions
      // Register Vue plugin
      this.registerVuePlugin(pluginName, plugin, pluginOptions)
    }
    // Init service hooks
    this.#serviceHooks[identifier] = {
      BEFORE_CREATE_SERVICE: new Hook('BEFORE_CREATE_SERVICE', [
        (options, context, Core) => { /* ... */ }
      ]),
      SERVICE_CREATED: new Hook('SERVICE_CREATED', [
        (service) => { /* ... */ }
      ])
    }
    // Define onInitialized callback
    if (service.onInitialized) {
      this.onInitialized(() => service.onInitialized(this))
    }
  }

  /**
   * registerVuePlugin
   * @category static methods
   * @param {string} name - The plugin name
   * @param {object} plugin - The plugin
   * @param {object} [options = {}] - The plugin options
   * @returns {void}
   * @description
   * Register a Vue plugin.
   * Core must be _initialized_ before registering Vue plugin.
   */
  static registerVuePlugin (name, plugin, options = {}) {
    // Check args type
    if (!(typeCheck(String, name))) error('registerVuePlugin method requires String as first argument')
    // Check if plugin is already registered
    if (isDef(this.context.vue.plugins[name])) error(`Vue plugin "${name}" is already registered`)
    // Contextualize
    Reflect.set(this.context.vue.plugins, name, { plugin, options }, this)
    // Init Vue plugin hooks
    this.#vuePluginsHooks[name] = {
      BEFORE_USE_VUE_PLUGIN: new Hook('BEFORE_USE_VUE_PLUGIN', [
        (plugin, options) => { /* ... */ }
      ]),
      AFTER_USE_VUE_PLUGIN: new Hook('AFTER_USE_VUE_PLUGIN', [
        (plugin, options) => { /* ... */ }
      ])
    }
  }

  /**
   * configure
   * @category static methods
   * @param {object} config - The config
   * @returns {void}
   * @description
   * Configure Core. Takes an object that follows {@link ./config global configuration} schema as first argument.
   * The given object will be {@link ./utils/deep-merge deep merged} with Core configuration. The result will be set in `Core.config`.
   * Core must be _initialized_ before configuring.
   */
  static configure (config) {
    // Throw error if not initialized
    if (!this.isInitialized) error('Can\'t configure before initializing...')
    // Resolve config
    const merged = this.resolveConfig(config)
    // Assign as class constant
    this.#config = merged
  }

  /**
   * resolveConfig
   * @category static methods
   * @param {object} [config = {}] - (Optional) The config object
   * @returns {object} Returns the resolved config
   * @description
   * Resolve configuration. Returns the result of `config` parameter object and the `Core.config` object deep merge.
   */
  static resolveConfig (config = {}) {
    // Merge with current config
    const mergedWithCurrent = merge(this.config, config)
    return mergedWithCurrent
  }

  /**
   * setup
   * @category static async methods
   * @returns {Promise<void>}
   * @description
   * Setup Core. It will:
   *
   * - create and use Vue plugins
   * - create services
   *
   * Core must be _initialized_ before setup.
   */
  static setup () {
    // Throw error if not initialized
    if (!this.isInitialized) error('Can\'t configure before initializing...')
    // Throw error if already setup
    if (this.isSetup) error('Already setup...')
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        // Use Vue plugins
        this.#useVuePlugins().then(() => {
          // Create services
          this.#createServices().then(() => {
            // Mark as setup
            this.#IS_SETUP = true
            // @ts-ignore
            resolve(this)
          }).catch((error) => {
            reject(error)
          })
        }).catch((error) => {
          reject(error)
        })
      })
    })
  }

  /**
   * @category static async methods
   * @returns {Promise<Vue>} The running app root Vue instance
   * @description
   * Run the app. It will:
   *
   * - call "BEFORE_APP_BOOT" hook
   * - call "APP_BOOT" hook
   * - create new Vue instance with `Core.context.vue.root.options`
   *
   * All options in `Core.context.vue.root.options` are injected.
   * Pluginable services that have `config.rootOption` defined are auto-injected too.
   * (example: `router` service is passed as `{ router: Core.service('router') }`)
   *
   * New Vue instance will be assigned to `window.<propertyName>` where `propertyName` is the value of `Core.config.vue.root.name`.
   *
   * Core must be _initialized_ and _setup_ before running app.
   */
  static async runApp () {
    // Before boot hook
    await this.#hooks.BEFORE_APP_BOOT.exec()
    // Boot hook
    await this.#hooks.APP_BOOT.exec()
    // Create the new instance of Vue and assign to window.<targetPropName>
    const rootVueInstance = new this.Vue(this.context.vue.root.options)
    // @ts-ignore
    window[this.config.vue.root.name] = rootVueInstance
    return rootVueInstance
  }

  /**
   * getRunningApp
   * @category static methods
   * @returns {Vue} Returns the app root Vue instance
   * @description
   * Get the current running app (created after the `runApp()` method call)
   */
  static getRunningApp () {
    // Returns save reference from window.<targetPropName>
    // @ts-ignore
    return window[this.config.vue.root.name]
  }

  /**
   * @category static methods
   * @returns {object} Return an object of registered services
   * @description
   * Returns an object like: `{ [identifier: String]: Service }`
   */
  static getRegisteredServices () {
    // Throw error if not initialized
    if (!this.isInitialized) error('Can\'t access registered services before initializing...')
    return this.context.services
  }

  /**
   * @category static methods
   * @param {string} identifier - The service unique identifier
   * @returns {boolean} Returns true if service is defined
   * @description
   * Returns boolean based on passed service `identifier`
   */
  static hasServiceRegistered (identifier) {
    return isDef(this.getRegisteredServices()[identifier])
  }

  /**
   * @category static methods
   * @returns {object} Returns an object of "pluginable" services
   * @description
   * Returns registered services that must be treated as Vue plugins
   */
  static getPluginableServices () {
    const registeredServices = this.getRegisteredServices()
    // First loop on services
    return Object.keys(registeredServices).reduce((accumulator, identifier) => {
      // Get service definition
      const serviceDef = registeredServices[identifier].getDefinition()
      // If serviceDef.vuePlugin is defined
      if (isDef(serviceDef.vuePlugin)) {
        accumulator[identifier] = registeredServices[identifier]
      }
      return accumulator
    }, {})
  }

  /**
   * @category static methods
   * @returns {object} Return an object of created services
   * @description
   * Returns an object like: `{ [identifier: String]: any }`
   */
  static getCreatedServices () {
    return this.#services
  }

  /**
   * @overload
   * @param {'api-manager'} identifier
   * @returns {import('./services/api-manager/APIManager').default}
   */
  /**
   * @overload
   * @param {'auth-manager'} identifier
   * @returns {import('./services/auth-manager/AuthManager').default}
   */
  /**
   * @overload
   * @param {'device-manager'} identifier
   * @returns {import('./services/device-manager/DeviceManager').default}
   */
  /**
   * @overload
   * @param {'error-manager'} identifier
   * @returns {import('@/services/error-manager/ErrorManager').default}
   */
  /**
   * @overload
   * @param {'event-bus'} identifier
   * @returns {import('@/services/event-bus/EventBus').default}
   */
  /**
   * @overload
   * @param {'i18n'} identifier
   * @returns {import('vue-i18n').default}
   */
  /**
   * @overload
   * @param {'keyboard-event-manager'} identifier
   * @returns {import('@/services/keyboard-event-manager/KeyboardEventManager').default}
   */
  /**
   * @overload
   * @param {'local-database-manager'} identifier
   * @returns {import('@/services/local-database-manager/LocalDatabaseManager').default}
   */
  /**
   * @overload
   * @param {'local-storage-manager'} identifier
   * @returns {import('@/services/local-storage-manager/LocalStorageManager').default}
   */
  /**
   * @overload
   * @param {'logger'} identifier
   * @returns {import('@/services/logger/Logger').default}
   */
  /**
   * @overload
   * @param {'modal-manager'} identifier
   * @returns {import('@/services/modal-manager/ModalManager').default}
   */
  /**
   * @overload
   * @param {'notification-manager'} identifier
   * @returns {import('@/services/notification-manager/NotificationManager').default}
   */
  /**
   * @overload
   * @param {'router'} identifier
   * @returns {import('vue-router').default}
   */
  /**
   * @overload
   * @param {'store'} identifier
   * @returns {import('vuex/types/index.d.ts').Store}
   */
  /**
   * @overload
   * @param {'toast-manager'} identifier
   * @returns {import('@/services/toast-manager/ToastManager').default}
   */
  /**
   * @overload
   * @param {'websocket-manager'} identifier
   * @returns {import('@/services/websocket-manager/WebSocketManager').default}
   */
  /**
   * @overload
   * @param {string} identifier
   * @returns {any}
   */
  /**
   * @category static methods
   * @param {string} identifier - The service identifier
   * @description
   * Access created service
   */
  static service (identifier) {
    const createdServices = this.getCreatedServices()
    // If service not created
    if (isUndef(createdServices[identifier])) error(`Service "${identifier}" not created.`)
    return createdServices[identifier]
  }

  /**
   * @category static methods
   * @param {Function} func - The function to append to the hook queue
   * @returns {void}
   * @description
   * **INITIALIZED** hook append method.
   *
   * The function to append can be _async_:
   *
   * ```ts
   * function () => void | Promise<void>
   * ```
   * @example
   * Core.onInitialized(() => {
   *   // Services are only registered but NOT created
   *   console.log('Core is initialized!')
   * })
   */
  static onInitialized (func) {
    this.#hooks.INITIALIZED.append(func)
  }

  /**
   * @category static methods
   * @param {string} name - The plugin name
   * @param {Function} func - The function to append to the hook queue
   * @returns {void}
   * @description
   * **BEFORE_USE_VUE_PLUGIN** hook append method.
   *
   * The function to append, which can be _async_ takes two arguments:
   *
   * ```ts
   * function (plugin: Object, options: any) => void | Promise<void>
   * ```
   *
   * - **plugin**: `{object}` the Vue plugin object
   * - **options**: `{any}` the plugin options
   * @example
   * Core.onBeforeUseVuePlugin('<name>', (plugin, options) => {
   *   // Do logic before use Vue plugin...
   * })
   */
  static onBeforeUseVuePlugin (name, func) {
    if (!name) error('onBeforeUseVuePlugin: plugin name is not defined')
    if (!this.context.vue.plugins[name]) error(`onBeforeUseVuePlugin: plugin "${name}" is not defined`)
    this.#vuePluginsHooks[name].BEFORE_USE_VUE_PLUGIN.append(func)
  }

  /**
   * @category static methods
   * @param {string} name - The plugin name
   * @param {Function} func - The function to append to the hook queue
   * @returns {void}
   * @description
   * **AFTER_USE_VUE_PLUGIN** hook append method.
   *
   * The function to append, which can be _async_ takes two arguments:
   *
   * ```ts
   * function (plugin: Object, options: any) => void | Promise<void>
   * ```
   *
   * - **plugin**: `{object}` the Vue plugin object
   * - **options**: `{any}` the plugin options
   * @example
   * Core.onAfterUseVuePlugin('<name>', (plugin, options) => {
   *   // Do logic after use Vue plugin...
   * })
   */
  static onAfterUseVuePlugin (name, func) {
    if (!name) error('onAfterUseVuePlugin: plugin name is not defined')
    if (!this.context.vue.plugins[name]) error(`onAfterUseVuePlugin: plugin "${name}" is not defined`)
    this.#vuePluginsHooks[name].AFTER_USE_VUE_PLUGIN.append(func)
  }

  /**
   * @category static methods
   * @param {string} identifier - The service unique identifier
   * @param {Function} func - The function to append to the hook queue
   * @returns {void}
   * @description
   * **BEFORE_CREATE_SERVICE** hook append method.
   *
   * The function to append takes three arguments:
   *
   * ```ts
   * function (options: Object, context: Object, Core: Function) => void | Promise<void>
   * ```
   *
   * - **options**: `{object}` the service options
   * - **context**: `{object}` the service context (`expose`)
   * - **Core**: `{Function}` the Core
   * @example
   * Core.onBeforeCreateService('<identifier>', (options, context) => {
   *   // Do logic before create service...
   * })
   */
  static onBeforeCreateService (identifier, func) {
    if (!identifier) error('onBeforeCreateService: service identifier is not defined')
    if (!this.context.services[identifier]) error(`onBeforeCreateService: service "${identifier}" is not defined`)
    this.#serviceHooks[identifier].BEFORE_CREATE_SERVICE.append(func)
  }

  /**
   * @category static methods
   * @param {string} identifier - The service unique identifier
   * @param {Function} func - The function to append to the hook queue
   * @returns {void}
   * @description
   * **SERVICE_CREATED** hook append method.
   *
   * The function to append, that can be async, takes one argument:
   *
   * ```ts
   * function () => void | Promise<void>
   * ```
   *
   * - **service**: `{*}` the created service
   * @example
   * Core.onServiceCreated('<identifier>', (service) => {
   *   // Service is created now. Do logic...
   * })
   */
  static onServiceCreated (identifier, func) {
    if (!identifier) error('onServiceCreated: service identifier is not defined')
    if (!this.context.services[identifier]) error(`onServiceCreated: service ${identifier} is not defined`)
    this.#serviceHooks[identifier].SERVICE_CREATED.append(func)
  }

  /**
   * @category static methods
   * @param {Function} func - The function to append to the hook queue
   * @returns {void}
   * @description
   * **BEFORE_APP_BOOT** hook append method.
   *
   * The function to append can be _async_:
   *
   * ```ts
   * function () => void | Promise<void>
   * ```
   * @example
   * Core.onBeforeAppBoot(() => {
   *   // Services are created and accessible in this hook
   *   Core.service('logger').consoleLog('Before app boot!')
   * })
   */
  static onBeforeAppBoot (func) {
    this.#hooks.BEFORE_APP_BOOT.append(func)
  }

  /**
   * @category static methods
   * @param {Function} func - The function to append to the hook queue
   * @returns {void}
   * @description
   * **APP_BOOT** hook append method.
   *
   * The function to append can be _async_:
   *
   * ```ts
   * function () => void | Promise<void>
   * ```
   * @example
   * Core.onAppBoot(() => {
   *   // Services are created and accessible in this hook
   *   Core.service('logger').consoleLog('App boot!')
   * })
   */
  static onAppBoot (func) {
    this.#hooks.APP_BOOT.append(func)
  }

  /**
   * @category static methods
   * @returns {object} - The info object
   * @description
   * Returns Core info
   * @example
   * Core.info()
   */
  static info () {
    return {
      version: this.version,
      defaultConfig: globalConfig,
      isInitialized: this.isInitialized,
      Vue: this.Vue,
      config: this.config,
      globals: this.config.globals,
      registeredServices: this.isInitialized ? this.getRegisteredServices() : {},
      pluginableServices: this.isInitialized ? this.getPluginableServices() : {},
      isSetup: this.isSetup,
      createdServices: this.isSetup ? this.getCreatedServices() : {},
      vuePlugins: this.context.vue.plugins,
      runningApp: this.getRunningApp(),
      context: this.context
    }
  }

  /**
   * Private static properties
   */

  /**
   * IS_INITIALIZED
   * @type {boolean}
   * @default false
   */
  static #IS_INITIALIZED = false

  /**
   * IS_SETUP
   * @type {boolean}
   * @default false
   */
  static #IS_SETUP = false

  /**
   * Config
   * @type {object}
   */
  static #config = globalConfig

  /**
   * Vue
   * @type {typeof Vue}
   * @default undefined
   */
  static #Vue = undefined

  /**
   * Services
   * @type {object}
   * @default {}
   * @description
   * Created services. Will be defined once `#createServices()` is called.
   */
  static #services = {}

  /**
   * Hooks
   * @type {object}
   */
  static #hooks = {
    INITIALIZED: new Hook('INITIALIZED', [
      () => {
        // Register built-in services
        for (const serviceObject of builtInServices) {
          // Assign service
          Core.registerService(serviceObject)
        }
      }
    ]),
    BEFORE_APP_BOOT: new Hook('BEFORE_APP_BOOT', [
      () => {
        // Log in development mode
        // if (process.env.NODE_ENV !== 'production') log('"BEFORE_APP_BOOT" hook triggered')
        // Nothing else by default...
      }
    ]),
    APP_BOOT: new Hook('APP_BOOT', [
      () => {
        // Log in development mode
        // if (process.env.NODE_ENV !== 'production') log('"APP_BOOT" hook triggered')
        // Nothing else by default...
      }
    ])
  }

  /**
   * Service level hooks
   * @type {object}
   */
  static #serviceHooks = {
    /**
     * Empty object by default.
     * At service registration, set property named by service identifier with the following hooks:
     * - BEFORE_CREATE_SERVICE
     * - SERVICE_CREATED
     */
  }

  /**
   * Vue plugins level hooks
   * @type {object}
   */
  static #vuePluginsHooks = {
    // CorePlugin
    CorePlugin: {
      BEFORE_USE_VUE_PLUGIN: new Hook('BEFORE_USE_VUE_PLUGIN', [
        (plugin, options) => { /* ... */ }
      ]),
      AFTER_USE_VUE_PLUGIN: new Hook('AFTER_USE_VUE_PLUGIN', [
        (plugin, options) => { /* ... */ }
      ])
    },
    // ReactiveRefs
    ReactiveRefs: {
      BEFORE_USE_VUE_PLUGIN: new Hook('BEFORE_USE_VUE_PLUGIN', [
        (plugin, options) => { /* ... */ }
      ]),
      AFTER_USE_VUE_PLUGIN: new Hook('AFTER_USE_VUE_PLUGIN', [
        (plugin, options) => { /* ... */ }
      ])
    }
    /**
     * At plugin registration, set property named by plugin name with the following hooks:
     * - BEFORE_USE_VUE_PLUGIN
     * - AFTER_USE_VUE_PLUGIN
     */
  }

  /**
   * Private static methods
   */

  /**
   * @alias Core['#useVuePlugins']
   * @returns {Promise<void>}
   * @description
   * Call Vue.use(plugin, options) for each plugin defined in Core.context.vue.plugins
   */
  static async #useVuePlugins () {
    // Loop on context plugins
    for (const name in this.context.vue.plugins) {
      const plugin = this.context.vue.plugins[name].plugin
      if (isUndef(plugin.name)) plugin.name = name
      const options = this.context.vue.plugins[name].options
      // Call BEFORE_USE_VUE_PLUGIN hook
      await this.#vuePluginsHooks[name].BEFORE_USE_VUE_PLUGIN.exec(plugin, options)
      // Use plugin
      this.Vue.use(plugin, options)
      // Call AFTER_USE_VUE_PLUGIN hook
      await this.#vuePluginsHooks[name].AFTER_USE_VUE_PLUGIN.exec(plugin, options)
    }
    log('Vue plugins successfully used ✔', this.context.vue.plugins)
  }

  /**
   * @alias Core['#createServices']
   * @returns {Promise<void>}
   * @description
   * Create services. Loop on `Core.context.services` object properties. If a service is "pluginable" (has `service.vuePlugin` property set),
   * the corresponding option is automatically added to `Core.context.vue.root.options`.
   */
  static async #createServices () {
    const availableServices = Object.keys(this.context.services)
    const pluginableServices = this.getPluginableServices()
    // Loop on services identifier
    for (const identifier of availableServices) {
      // Service options defined in Core configuration are automatically passed
      const serviceDef = this.context.services[identifier].getDefinition()
      const serviceOptions = this.context.services[identifier].options
      const serviceContext = this.context.services[identifier].getNamespace()
      // Call BEFORE_CREATE_SERVICE hook
      await this.#serviceHooks[identifier].BEFORE_CREATE_SERVICE.exec(serviceOptions, serviceContext, this)
      // Create service (instance)
      this.#services[identifier] = serviceDef.create(serviceOptions, serviceContext, this)
      // If "pluginable" service, process Vue root instance option
      if (Object.keys(pluginableServices).includes(identifier)) {
        let rootOption = serviceDef.rootOption
        if (isDef(rootOption)) {
          rootOption = typeof rootOption === 'function'
            ? rootOption(serviceContext, this) // takes Core as second argument
            : rootOption
          // Assign prop to options
          this.context.vue.root.options[rootOption] = this.service(identifier)
        }
      }
      // Call SERVICE_CREATED hook
      await this.#serviceHooks[identifier].SERVICE_CREATED.exec(this.service(identifier))
    }
    log('Services successfully created ✔', this.#services)
  }
}

export default Core
