export default ModalManager;
declare const ModalManager_base: any;
declare class ModalManager extends ModalManager_base {
    [x: string]: any;
    static reactive: boolean;
    static rootOption: string;
    static aliases: any[];
    static install(_Vue: typeof import("vue"), options?: object): void;
    constructor(options?: {
        modals?: Modal[];
        useTransition?: object;
    });
    _useTransition: any;
    readonly get modals(): any;
    readonly get useTransition(): any;
    add(modal: Modal): ModalManager;
    show(name: string, props?: object): Modal;
    hide(name: string): Modal;
    isShown(name: string): boolean;
    isHidden(name: string): boolean;
    toJSON(): object;
    #private;
}
import Modal from './Modal';
//# sourceMappingURL=ModalManager.d.ts.map