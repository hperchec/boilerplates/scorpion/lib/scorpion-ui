---
title: getPrototypeNames
headline: getPrototypeNames
sidebarTitle: getPrototypeNames
---

# getPrototypeNames

<a name="getprototypenames">
</a>

## getPrototypeNames ⇒ <code>Array.&lt;string&gt;</code>

Similar to getPrototypeChain() except that returns constructor names (strings)

**Returns**: <code>Array.&lt;string&gt;</code> - Returns an array of constructor names

| Param | Type | Description |
| --- | --- | --- |
| value | <code>\*</code> | The value |

**Example**

```js
getPrototypeNames({ foo: 'bar' }) // => [ 'Object' ]
```

