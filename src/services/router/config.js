/**
 * @vuepress
 * ---
 * title: "Service (router) configuration"
 * headline: "Service (router) configuration"
 * sidebarTitle: Configuration
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * ⚠ THIS FILE IS ONLY USED TO GENERATE SERVICE CONFIGURATION DOCUMENTATION
 */

/**
 * @name Configuration
 * @typicalname Core.config.services['router']
 * @description
 * Service configuration
 *
 * > Refers to `Core.config.services['router']`
 *
 * ```js
 * 'router': {
 *   // Trigger routing on root instance mounted hook
 *   initOnRootInstanceMounted: true,
 *   // The route to redirect in _CatchAll route before hook.
 *   catchAllRedirect: String|Object
 * }
 * ```
 */
export default {
  /**
   * @alias Configuration.initOnRootInstanceMounted
   * @type {boolean}
   * @description
   * Define if it auto triggers routing at root instance mounted hook.
   */
  initOnRootInstanceMounted: true,
  /**
   * @alias Configuration.catchAllRedirect
   * @type {string|object}
   * @description
   * The route to redirect in _CatchAll route before hook.
   */
  catchAllRedirect: undefined
}
