// Utils
import { isUndef } from '@/utils'
// import ModelCollection from '@/context/support/collection/ModelCollection' // type for jsDoc

/**
 * Generate default resource module state
 * @param {*} model - The model representing the resource
 * @param {object} options - The function options
 * @returns {object} Returns state object
 */
export default function generateState (model, options = {}) {
  // Check if model is provided
  if (isUndef(model)) {
    throw new Error('Generate resource module state: no model provided')
  }

  const loadingStatus = {}
  if (options.loadingStatus.index) loadingStatus.index = false
  if (options.loadingStatus.create) loadingStatus.create = false
  if (options.loadingStatus.retrieve) loadingStatus.retrieve = false
  if (options.loadingStatus.update) loadingStatus.update = false
  if (options.loadingStatus.delete) loadingStatus.delete = false

  // Return state object
  return {
    /**
     * collection
     * @description Resource collection
     * @type {object}
     */
    // collection: model.collect([], { lock: true }),
    $data: {},
    /**
     * loadingStatus
     * @description Save the loading status
     * @type {object}
     */
    loadingStatus: loadingStatus
  }
}
