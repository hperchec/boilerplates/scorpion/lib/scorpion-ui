/**
 * @vuepress
 * ---
 * title: "Service (local-database-manager): LocalDatabaseOptions class"
 * headline: "Service (local-database-manager): LocalDatabaseOptions class"
 * sidebarTitle: .LocalDatabaseOptions
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import { getAllKeys, isDef, typeCheck } from '@/utils'

/**
 * @classdesc
 * Service **local-database-manager**: LocalDatabaseOptions class
 */
class LocalDatabaseOptions {
  /**
   * @private
   */
  _name
  _driver
  _driverOptions
  _init

  /**
   * Create new LocalDatabaseOptions
   * @param {string} name - The LocalDatabase name
   * @param {object} [options = {}] - An options object
   * @param {string} [options.driver = 'dexie'] - The library to use in LocalDatabase.supportedDrivers
   * @param {object} [options.driverOptions] - An options object that the driver needs to configure itself
   * @param {Function} [options.init] - A function to call when "init" method of LocalDatabase instance is called (takes LocalDatabase instance as unique parameter)
   */
  constructor (name, options = {}) {
    this.name = name
    this.driver = options.driver || 'dexie'
    // Set driverOptions
    if (isDef(options.driverOptions)) this.driverOptions = options.driverOptions
    // Set init method
    if (isDef(options.init)) {
      this.init = options.init
    } else {
      this.init = (db) => db
    }
  }

  /**
   * Accessors & mutators
   */

  /**
   * LocalDatabase name
   * @category properties
   * @type {string}
   */
  get name () {
    return this._name
  }

  set name (value) {
    // Check type
    if (typeCheck(String, value)) {
      // Assign
      this._name = value
    } else {
      // Else -> throw error
      throw new Error('LocalDatabaseOptions class: \'name\' property must be String. ' + typeof value + ' received...')
    }
  }

  /**
   * LocalDatabase driver name
   * @category properties
   * @type {string}
   */
  get driver () {
    return this._driver
  }

  set driver (value) {
    // Check type
    if (typeCheck(String, value)) {
      // Assign
      this._driver = value
    } else {
      // Else -> throw error
      throw new Error('LocalDatabaseOptions class: \'driver\' property must be String. ' + typeof value + ' received...')
    }
  }

  /**
   * Driver options
   * @category properties
   * @type {object}
   */
  get driverOptions () {
    return this._driverOptions
  }

  set driverOptions (value) {
    // Check type
    if (typeCheck(Object, value)) {
      // Assign
      this._driverOptions = value
    } else {
      // Else -> throw error
      throw new Error('LocalDatabaseOptions class: \'driverOptions\' property must be Object. ' + typeof value + ' received...')
    }
  }

  /**
   * Init callback
   * @category properties
   * @type {Function}
   */
  get init () {
    return this._init
  }

  set init (value) {
    // Check type
    if (typeCheck(Function, value)) {
      // Assign
      this._init = value
    } else {
      // Else -> throw error
      throw new Error('LocalDatabaseOptions class: \'init\' property must be Function. ' + typeof value + ' received...')
    }
  }

  /**
   * Methods
   */

  /**
   * Overrides the default toJSON object method for JSON.stringify() calls
   * @returns {object} - The instance data to be serialized
   */
  toJSON () {
    const keys = getAllKeys(this, {
      excludeKeys: [
        /^_/, // private properties that starts with '_'
        'toJSON',
        'toString',
        'constructor'
      ]
    })
    const jsonObj = keys.reduce((obj, key) => {
      obj[key] = this[key]
      return obj
    }, {})
    return jsonObj
  }
}

export default LocalDatabaseOptions
