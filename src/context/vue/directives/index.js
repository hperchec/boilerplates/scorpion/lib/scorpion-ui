/**
 * Core vue directives
 * each directive must have:
 * - `createDirective` method that returns the directive that will be used Vue
 * - `options` object to configure directive. Mandatory properties:
 *   - `options.id`: how to name directive, Vue.directive() first param (see also https://v2.vuejs.org/v2/api/#Vue-directive)
 */

export default {}
