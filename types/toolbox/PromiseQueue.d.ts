export default PromiseQueue;
declare class PromiseQueue {
    constructor(maxPendingPromises?: number, maxQueuedPromises?: number, options?: object);
}
//# sourceMappingURL=PromiseQueue.d.ts.map