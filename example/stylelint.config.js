module.exports = {
  extends: 'stylelint-config-standard',
  rules: {
    'at-rule-empty-line-before': [ 'always', {
      except: [ 'blockless-after-same-name-blockless', 'first-nested', 'inside-block' ],
      ignore: [ 'after-comment' ]
    } ],
    'declaration-empty-line-before': null,
    'at-rule-no-unknown': null
  }
}
