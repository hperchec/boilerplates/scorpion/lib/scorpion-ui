---
title: Default configuration
headline: Default configuration
sidebarTitle: Default configuration
sidebarDepth: 0
prev: ../
next: ../context/
---

# Default configuration

<a name="config">
</a>

## config

> **Ref.** : [Core](../Core).[config](./)This is the default configuration<<< @/v1.0/api/config/index.source.js#snippet

**Schema**:

- [config](#config)
  - [.globals](#config-globals)
  - [.vue](#config-vue)
    - [.root](#config-vue-root)
      - [.name](#config-vue-root-name)
      - [.targetElement](#config-vue-root-targetelement)
      - [.autoMount](#config-vue-root-automount)
  - [.services](#config-services)
    - [.api-manager](#config-services-api-manager) : <code>Class.&lt;module:@/services/ap-manager/config&gt;</code>
    - [.event-bus](#config-services-event-bus)
    - [.error-manager](#config-services-error-manager)
    - [.i18n](#config-services-i18n)
    - [.local-storage-manager](#config-services-local-storage-manager)
    - [.logger](#config-services-logger)
    - [.modal-manager](#config-services-modal-manager)
    - [.router](#config-services-router) : <code>Class.&lt;module:@/services/router/config&gt;</code>
    - [.toast-manager](#config-services-toast-manager)
    - [.websocket-manager](#config-services-websocket-manager)

<a name="config-globals">
</a>

### config.globals

Define app globals

<a name="config-vue">
</a>

### config.vue

Vue specific configuration options

**Schema**:

- [.vue](#config-vue)
  - [.root](#config-vue-root)
    - [.name](#config-vue-root-name)
    - [.targetElement](#config-vue-root-targetelement)
    - [.autoMount](#config-vue-root-automount)

<a name="config-vue-root">
</a>

#### vue.root

Options for Vue root instance

**Schema**:

- [.root](#config-vue-root)
  - [.name](#config-vue-root-name)
  - [.targetElement](#config-vue-root-targetelement)
  - [.autoMount](#config-vue-root-automount)

<a name="config-vue-root-name">
</a>

##### root.name

The name of the component and the window property that will be assigned (`window.<name>`)

<a name="config-vue-root-targetelement">
</a>

##### root.targetElement

The element (selector) on which to mount Vue root instance

<a name="config-vue-root-automount">
</a>

##### root.autoMount

Defines if the Vue root instance is auto mounted

<a name="config-services">
</a>

### config.services

Services configuration

**Schema**:

- [.services](#config-services)
  - [.api-manager](#config-services-api-manager) : <code>Class.&lt;module:@/services/ap-manager/config&gt;</code>
  - [.event-bus](#config-services-event-bus)
  - [.error-manager](#config-services-error-manager)
  - [.i18n](#config-services-i18n)
  - [.local-storage-manager](#config-services-local-storage-manager)
  - [.logger](#config-services-logger)
  - [.modal-manager](#config-services-modal-manager)
  - [.router](#config-services-router) : <code>Class.&lt;module:@/services/router/config&gt;</code>
  - [.toast-manager](#config-services-toast-manager)
  - [.websocket-manager](#config-services-websocket-manager)

<a name="config-services-api-manager">
</a>

#### services.api-manager : <code>Class.&lt;module:@/services/ap-manager/config&gt;</code>

Built-in 'api-manager' service default configuration

<a name="config-services-event-bus">
</a>

#### services.event-bus

Built-in 'event-bus' service default configuration

<a name="config-services-error-manager">
</a>

#### services.error-manager

Built-in 'error-manager' service default configuration

<a name="config-services-i18n">
</a>

#### services.i18n

Built-in 'i18n' service default configuration

<a name="config-services-local-storage-manager">
</a>

#### services.local-storage-manager

Built-in 'local-storage-manager' service default configuration

<a name="config-services-logger">
</a>

#### services.logger

Built-in 'logger' service default configuration

<a name="config-services-modal-manager">
</a>

#### services.modal-manager

Built-in 'modal-manager' service default configuration

<a name="config-services-router">
</a>

#### services.router : <code>Class.&lt;module:@/services/router/config&gt;</code>

Built-in 'router' service default configuration

<a name="config-services-toast-manager">
</a>

#### services.toast-manager

Built-in 'toast-manager' service default configuration

<a name="config-services-websocket-manager">
</a>

#### services.websocket-manager

Built-in 'websocket-manager' service default configuration

