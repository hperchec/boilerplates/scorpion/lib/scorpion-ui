/**
 * @vuepress
 * ---
 * title: "Service (websocket-manager): WSConnectionOptions class"
 * headline: "Service (websocket-manager): WSConnectionOptions class"
 * sidebarTitle: .WSConnectionOptions
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import { getAllKeys, typeCheck } from '@/utils'

/**
 * @classdesc
 * Service **websocket-manager**: WSConnectionOptions class
 */
class WSConnectionOptions {
  /**
   * @private
   */
  _name
  _driver
  _options

  /**
   * Create new WSConnectionOptions
   * @param {string} name - The WSConnection name
   * @param {string} driver - The library to use in WSConnection.supportedDrivers
   * @param {object} options - An object that the driver needs to configure itself
   */
  constructor (name, driver, options) {
    this.name = name
    this.driver = driver
    this.options = options
  }

  /**
   * Accessors & mutators
   */

  /**
   * WSConnection name
   * @category properties
   * @type {string}
   */
  get name () {
    return this._name
  }

  set name (value) {
    // Check type
    if (typeCheck(String, value)) {
      // Assign
      this._name = value
    } else {
      // Else -> throw error
      throw new Error('WSConnectionOptions class: \'name\' property must be String. ' + typeof value + ' received...')
    }
  }

  /**
   * WSConnection driver name
   * @category properties
   * @type {string}
   */
  get driver () {
    return this._driver
  }

  set driver (value) {
    // Check type
    if (typeCheck(String, value)) {
      // Assign
      this._driver = value
    } else {
      // Else -> throw error
      throw new Error('WSConnectionOptions class: \'driver\' property must be String. ' + typeof value + ' received...')
    }
  }

  /**
   * Driver options
   * @category properties
   * @type {object}
   */
  get options () {
    return this._options
  }

  set options (value) {
    // Check type
    if (typeCheck(Object, value)) {
      // Assign
      this._options = value
    } else {
      // Else -> throw error
      throw new Error('WSConnectionOptions class: \'options\' property must be Object. ' + typeof value + ' received...')
    }
  }

  /**
   * Methods
   */

  /**
   * Overrides the default toJSON object method for JSON.stringify() calls
   * @returns {object} - The instance data to be serialized
   */
  toJSON () {
    const keys = getAllKeys(this, {
      excludeKeys: [
        /^_/, // private properties that starts with '_'
        'toJSON',
        'toString',
        'constructor'
      ]
    })
    const jsonObj = keys.reduce((obj, key) => {
      obj[key] = this[key]
      return obj
    }, {})
    return jsonObj
  }
}

export default WSConnectionOptions
