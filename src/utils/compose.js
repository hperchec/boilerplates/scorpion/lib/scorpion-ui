/**
 * @vuepress
 * ---
 * title: compose
 * headline: compose
 * sidebarTitle: compose
 * ---
 */

import isDef from './is-def'

/**
 * compose
 * @param {Function[]} funcs - The array of function to compose
 * @param {Array<*>} [args = []] - The array of arguments to pass
 * @returns {*} The returned value of compose functions
 * @description
 * Compose functions. If at least one returned value is a promise, so the entire result will be promisyfied
 * @example
 * compose([ (str) => [ str.toUpperCase() ], (str) => [ `_${str}` ] ], [ 'foo' ]) // => '_FOO'
 * compose([ (str) => [ str.toUpperCase() ], (str) => new Promise((resolve) => resolve([ `_${str}` ])) ], [ 'foo' ]) // => Promise resolved with '_FOO'
 */
export const compose = function (funcs, args = []) {
  return funcs.reduce(
    (wrap, wrapped) => {
      return function (...args) {
        let nextPayload = wrapped(...args)
        if (!isDef(nextPayload)) {
          nextPayload = args
        }
        if (nextPayload instanceof Promise) {
          return new Promise((resolve, reject) => {
            nextPayload
              .then((payload) => {
                resolve(wrap(...payload) || args)
              })
              .catch((error) => reject(error))
          })
        }
        return wrap(...nextPayload) || args
      }
    },
    // IMPORTANT: Initial value (accumulator): function that returns the received args
    (..._args) => _args
  )(...args)
}

export default compose
