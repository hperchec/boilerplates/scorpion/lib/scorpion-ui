/**
 * @vuepress
 * ---
 * title: createColor
 * headline: createColor
 * sidebarTitle: createColor
 * ---
 */

// color package
import Color from 'color'

/**
 * createColor
 * @type {Function}
 * @returns {Color} An instance of Color
 * @description
 * Create a color
 *
 * ::: tip See also
 * [color](https://www.npmjs.com/package/color) package documentation
 * :::
 * @example
 * const color = createColor('#FFFFFF')
 * const color = createColor('rgb(255, 255, 255)')
 * const color = createColor({r: 255, g: 255, b: 255})
 * const color = createColor.rgb(255, 255, 255)
 * const color = createColor.rgb([255, 255, 255])
 */
export const createColor = Color

export default createColor
