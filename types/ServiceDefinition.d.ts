export class ServiceDefinition {
    constructor(def: {
        identifier: string;
        create: Function;
        register?: Function;
        vuePlugin?: Function;
        pluginOptions?: any;
        rootOption?: Function | string;
    });
    set identifier(value: string);
    get identifier(): string;
    set create(value: Function);
    get create(): Function;
    set register(value: Function);
    get register(): Function;
    set vuePlugin(value: Function);
    get vuePlugin(): Function;
    set pluginOptions(value: any);
    get pluginOptions(): any;
    set rootOption(value: string | Function);
    get rootOption(): string | Function;
    #private;
}
export default ServiceDefinition;
//# sourceMappingURL=ServiceDefinition.d.ts.map