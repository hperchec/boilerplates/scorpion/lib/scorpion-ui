/**
 * @vuepress
 * ---
 * title: "Service (api-manager) options"
 * headline: "Service (api-manager) options"
 * sidebarTitle: Options
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import Core from '@/Core'
import toolbox from '@/toolbox'
import APIOptions from './APIOptions' // jsDoc type

const { makeConfigurableOption } = toolbox.services

/**
 * @name options
 * @typicalname Core.context.services['api-manager'].options
 * @description
 * API manager service options
 */
const options = {
  //
}

/**
 * @alias options.apis
 * @type {APIOptions[]}
 * @default []
 * @description
 * Array of APIOptions instance to pass to APIManager constructor.
 *
 * > 🔧 Configurable option: `Core.config.services['api-manager'].apis`. See service {@link ./config configuration}
 */
makeConfigurableOption(options, {
  propertyName: 'apis',
  serviceIdentifier: 'api-manager',
  configPath: 'apis',
  formatter: (apiDefs) => {
    const tmp = []
    const API = Core.context.services['api-manager'].API
    const _APIOptions = Core.context.services['api-manager'].APIOptions
    for (const name in apiDefs) {
      // Create a new APIOptions
      const apiOptions = new _APIOptions(name, 'axios', apiDefs[name])
      // Add custom apis
      tmp.push(new API(apiOptions))
    }
    return tmp
  },
  defaultValue: []
})

export default options
