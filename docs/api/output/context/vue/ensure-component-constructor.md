---
title: ensureComponentConstructor
headline: ensureComponentConstructor
sidebarTitle: ensureComponentConstructor
prev: ../
---

# ensureComponentConstructor

<a name="ensurecomponentconstructor">
</a>

## ensureComponentConstructor(target) ⇒ <code>module:vue~Component</code>

Ensure that target is a Vue component constructor.If VNode passed, wrap it inside a functional component that just returns the VNode as is.

**Returns**: <code>module:vue~Component</code> - Returns the component constructor. If error, returns undefined.

| Param | Type | Description |
| --- | --- | --- |
| target | <code>module:vue~VNode</code> \| <code>module:vue~Component</code> \| <code>object</code> | The target component |

