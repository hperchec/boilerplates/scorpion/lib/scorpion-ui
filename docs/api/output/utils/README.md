---
title: Utils
headline: Utils
sidebarTitle: Utils
prev: false
next: ./camelcase
---

# Utils

<a name="utils">
</a>

## utils : <code>object</code>

ScorpionUI built-in utils.```jsimport { utils } from '@hperchec/scorpion-ui'```

