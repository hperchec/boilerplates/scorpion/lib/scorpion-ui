---
title: app.utils
headline: app.utils
sidebarTitle: app.utils
initialOpenGroupIndex: -1
prev: ../
---

# app.utils

<a name="vue">
</a>

## vue : <code>object</code>

$app.utils.vue

**Schema**:

- [vue](#vue) : <code>object</code>
  - [.isVueComponent(value)](#vue-isvuecomponent) ⇒ <code>boolean</code>
  - [.ensureComponentConstructor(target)](#vue-ensurecomponentconstructor) ⇒ <code>module:vue~Component</code>

<a name="vue-isvuecomponent">
</a>

### vue.isVueComponent(value) ⇒ <code>boolean</code>

See "isVueComponent" utils documentation

**Returns**: <code>boolean</code> - Returns boolean

| Param | Type | Description |
| --- | --- | --- |
| value | <code>\*</code> | The value to check |

<a name="vue-ensurecomponentconstructor">
</a>

### vue.ensureComponentConstructor(target) ⇒ <code>module:vue~Component</code>

See `Core.context.vue.ensureComponentConstructor`

**Returns**: <code>module:vue~Component</code> - Returns the component constructor. If error, returns undefined.

| Param | Type | Description |
| --- | --- | --- |
| target | <code>module:vue~VNode</code> \| <code>module:vue~Component</code> \| <code>object</code> | The target component |

