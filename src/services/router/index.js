/**
 * @vuepress
 * ---
 * title: "Service (router)"
 * headline: "Service (router)"
 * sidebarTitle: router
 * initialOpenGroupIndex: -1 # optional, defaults to 0, defines the index of initially opened subgroup
 * prev: ../
 * next: ./options
 * ---
 */

import VueRouter from 'vue-router'

import Service from '@/Service'

import extendVueRouter from './lib/extend-vue-router'
import options from './options'
import components from './components'
import mixins from './mixins'
import middlewares from './middlewares'
import routes from './routes'
import Route from './Route'
import PrivateRoute from './PrivateRoute'
import PublicRoute from './PublicRoute'
import tools from './tools'

const ExtendedVueRouter = extendVueRouter(VueRouter)

/* eslint-disable jsdoc/require-param, jsdoc/require-returns */

/**
 * @scorpion_ui_service {VueRouter} router
 * @description
 * The service "router" is for routing.
 * It uses the [vue-router](https://router.vuejs.org/) package.
 */
export default new Service({
  // Service
  service: {
    // Identifier
    identifier: 'router',
    /**
     * @alias register
     * @description
     * On registering, the service will add to root instance:
     *
     * - the `isRouterInitiliazed` data property
     * - the `triggerRouting` method
     *
     * It will also define Vue components:
     *
     * - `Core.context.vue.components.public.views`: see {@link ./components/public/ public components}
     * - `Core.context.vue.components.private.views`: see {@link ./components/private/ private components}
     *
     * and mixin:
     *
     * - `Core.context.vue.mixins.RouteComponentMixin`: see {@link ./mixins/RouteComponentMixin RouteComponentMixin}
     */
    register: function (_Core) {
      // Define custom property in vue root instance data
      _Core.context.vue.root.setOptions({
        data () {
          return {
            isRouterInitialized: false
          }
        },
        methods: {
          triggerRouting () {
            this.$emit('routing-triggered')
          }
        },
        mounted () {
          if (_Core.config.services['router'].initOnRootInstanceMounted) { // eslint-disable-line dot-notation
            this.triggerRouting()
          }
        }
      })
      // Expose vue components
      _Core.context.vue.components.public = {
        layouts: components.public.layouts,
        views: components.public.views
      }
      _Core.context.vue.components.private = {
        layouts: components.public.layouts,
        views: components.private.views
      }
      // Expose mixin
      _Core.context.vue.mixins.RouteComponentMixin = mixins.RouteComponentMixin
    },
    /**
     * @alias create
     * @description
     * Returns an instance of `VueRouter` created with options.
     */
    create: (serviceOptions, serviceContext, _Core) => {
      // Destructure options
      /* eslint-disable no-unused-vars */
      const {
        defaultOnComplete,
        defaultOnAbort,
        beforeEach,
        beforeResolve,
        afterEach,
        catchAllRedirect,
        ...VueRouterOptions
      } = serviceOptions
      /* eslint-enable no-unused-vars */
      // new VueRouter
      // @ts-ignore
      const router = new ExtendedVueRouter(VueRouterOptions)
      // Global before hook
      const middlewares = Object.values(serviceContext.middlewares).concat(beforeEach)
      for (const fct of middlewares) router.beforeEach(fct)
      // Global before resolve hook
      for (const fct of beforeResolve) router.beforeResolve(fct)
      // Global after hook
      for (const fct of afterEach) router.afterEach(fct)
      // Return router
      return router
    },
    /**
     * @alias plugin
     * @description
     * ::: tip SEE ALSO
     * [vue-router](https://router.vuejs.org/) documentation
     * :::
     */
    vuePlugin: (serviceContext, _Core) => ExtendedVueRouter,
    // Vue plugin options
    pluginOptions: {},
    // rootOption: will be assigned to Vue.prototype.$router
    rootOption: 'router'
  },
  /**
   * @alias onInitialized
   * @description
   * Once Core is initialized, it will define a `Logger` {@link ../logger/Logger#types type} as following:
   *
   * ```js
   * router: {
   *   badgeContent: '[ROUTER]',
   *   badgeColor: '#AFD3E4',
   *   badgeBgColor: '#3883A8',
   *   messageColor: '#1C4254',
   *   prependMessage: '🚦'
   * }
   * ```
   */
  onInitialized: (Core) => {
    // Set logger 'router' type in config
    Core.onBeforeCreateService('logger', ({ types }) => {
      // Check if 'router' type is defined, else set it
      if (!types.router) {
        types.router = {
          badgeContent: '[ROUTER]',
          badgeColor: '#AFD3E4',
          badgeBgColor: '#3883A8',
          messageColor: '#1C4254',
          prependMessage: '🚦'
        }
      }
    })
  },
  // Options
  options,
  /**
   * @alias expose
   */
  expose: {
    /**
     * @see {@link ./middlewares middlewares}
     */
    middlewares,
    /**
     * @see {@link ./routes routes}
     */
    routes,
    /**
     * @see {@link ./Route Route}
     */
    Route,
    /**
     * @see {@link ./PrivateRoute PrivateRoute}
     */
    PrivateRoute,
    /**
     * @see {@link ./PublicRoute PublicRoute}
     */
    PublicRoute,
    /**
     * @see {@link ./tools tools}
     */
    tools
  }
})
