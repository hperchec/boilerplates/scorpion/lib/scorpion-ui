---
title: "Service (websocket-manager): WSConnection class"
headline: "Service (websocket-manager): WSConnection class"
sidebarTitle: .WSConnection
sidebarDepth: 0
prev: false
next: false
---

# Service (websocket-manager): WSConnection class

## Classes

<dl>
<dt><a href="#WSConnection">WSConnection</a></dt>
<dd><p>Service <strong>websocket-manager</strong>: WSConnection class</p>
</dd>
</dl>

## Functions

<dl>
<dt><a href="#log">log(...args)</a> ⇒ <code>string</code></dt>
<dd></dd>
</dl>

<a name="wsconnection">
</a>

## WSConnection

Service **websocket-manager**: WSConnection class

**Schema**:

- [WSConnection](#wsconnection)
  - [new WSConnection(wsConnectionOptions)](#newwsconnectionnew)
  - _instance_
    - [.toJSON()](#wsconnection-tojson) ⇒ <code>object</code>
    - _methods_
      - [.setDriver(name, [options])](#wsconnection-setdriver) ⇒ <code>void</code>
      - [.mapDriverMethods(methods)](#wsconnection-mapdrivermethods) ⇒ <code>void</code>
      - [.getConnection()](#wsconnection-getconnection) ⇒ <code>\*</code>
      - [.connect([options])](#wsconnection-connect) ⇒ <code>\*</code>
      - [.disconnect([options])](#wsconnection-disconnect) ⇒ <code>\*</code>
      - [.on(eventName, eventCb)](#wsconnection-on) ⇒ <code>void</code>
      - [.subscribe(channelName, [successCb], [errorCb])](#wsconnection-subscribe) ⇒ <code>\*</code>
      - [.unsubscribe(channelName)](#wsconnection-unsubscribe) ⇒ <code>\*</code>
      - [.initPusher()](#wsconnection-initpusher) ⇒ <code>void</code>
    - _properties_
      - [.name](#wsconnection-name) : <code>string</code>
      - [.options](#wsconnection-options) : <code>string</code>
      - [.driver](#wsconnection-driver) : <code>object</code>
  - _static_
    - _properties_
      - [.supportedDrivers](#wsconnection-supporteddrivers) : <code>Array.&lt;string&gt;</code>

<a name="newwsconnectionnew">
</a>

### new WSConnection(wsConnectionOptions)

Create a new WSConnection

| Param | Type | Description |
| --- | --- | --- |
| wsConnectionOptions | <code>module:services/websocket-manager/WSConnectionOptions~WSConnectionOptions</code> | An WSConnectionOptions object |

<a name="wsconnection-tojson">
</a>

### wsConnection.toJSON() ⇒ <code>object</code>

Overrides the default toJSON object method for JSON.stringify() calls

**Returns**: <code>object</code> - - The instance data to be serialized

<a name="wsconnection-setdriver">
</a>

### wsConnection.setDriver(name, [options]) ⇒ <code>void</code>

Set the driver for the WSConnection

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The driver name |
| [options] | <code>object</code> | The driver options/configuration |

<a name="wsconnection-mapdrivermethods">
</a>

### wsConnection.mapDriverMethods(methods) ⇒ <code>void</code>

Map driver methods for requests```js{  connect: methods.connect, // this.connect([<options>])  disconnect: methods.disconnect, // this.disconnect([<options>])  subscribe: methods.subscribe, // this.subscribe(<channelName>, [<successCb>, <errorCb>])  unsubscribe: methods.unsubscribe // this.unsubscribe(<channelName>)}```

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| methods | <code>object</code> | A methods object |

<a name="wsconnection-getconnection">
</a>

### wsConnection.getConnection() ⇒ <code>\*</code>

getConnection method: return the connection reference

**Returns**: <code>\*</code> - The connection reference

**Category**: methods  
<a name="wsconnection-connect">
</a>

### wsConnection.connect([options]) ⇒ <code>\*</code>

Main connect method: etablish connection with websocket server

**Returns**: <code>\*</code> - The driver returned value

**Category**: methods  
| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [options] | <code>object</code> | <code>{}</code> | Will be merged with instance options |

<a name="wsconnection-disconnect">
</a>

### wsConnection.disconnect([options]) ⇒ <code>\*</code>

Main disconnect method: unetablish connection with websocket server

**Returns**: <code>\*</code> - The driver returned value

**Category**: methods  
| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [options] | <code>object</code> | <code>{}</code> | The specific options for the driver |

<a name="wsconnection-on">
</a>

### wsConnection.on(eventName, eventCb) ⇒ <code>void</code>

Do something when the event is triggered

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| eventName | <code>string</code> | The event name |
| eventCb | <code>function</code> \| <code>object</code> | The event callback |

<a name="wsconnection-subscribe">
</a>

### wsConnection.subscribe(channelName, [successCb], [errorCb]) ⇒ <code>\*</code>

Do something when the channel is subscribed

**Returns**: <code>\*</code> - - The driver method returned value

**Category**: methods  
| Param | Type | Default | Description |
| --- | --- | --- | --- |
| channelName | <code>string</code> |  | The channel name |
| [successCb] | <code>function</code> | <code></code> | The callback to call in case of success |
| [errorCb] | <code>function</code> | <code></code> | The callback to call in case of error |

<a name="wsconnection-unsubscribe">
</a>

### wsConnection.unsubscribe(channelName) ⇒ <code>\*</code>

Do something when the channel is unsubscribed

**Returns**: <code>\*</code> - - The driver method returned value

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| channelName | <code>string</code> | The channel name |

<a name="wsconnection-initpusher">
</a>

### wsConnection.initPusher() ⇒ <code>void</code>

Init pusher as driver

**Category**: methods  
<a name="wsconnection-name">
</a>

### wsConnection.name : <code>string</code>

WSConnection name

**Category**: properties  
<a name="wsconnection-options">
</a>

### wsConnection.options : <code>string</code>

WSConnection options

**Category**: properties  
<a name="wsconnection-driver">
</a>

### wsConnection.driver : <code>object</code>

Get the WSConnection driver

**Category**: properties  
**Read only**: true  
<a name="wsconnection-supporteddrivers">
</a>

### WSConnection.supportedDrivers : <code>Array.&lt;string&gt;</code>

Only 'pusher' is supported as a driver for the moment

**Category**: properties  
**Read only**: true  
<a name="log">
</a>

## log(...args) ⇒ <code>string</code>

| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>any</code> | See i18n method |

