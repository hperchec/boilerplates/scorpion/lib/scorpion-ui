/**
 * @vuepress
 * ---
 * title: "Service (toast-manager) configuration"
 * headline: "Service (toast-manager) configuration"
 * sidebarTitle: Configuration
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * ⚠ THIS FILE IS ONLY USED TO GENERATE SERVICE CONFIGURATION DOCUMENTATION
 */

/**
 * @name Configuration
 * @typicalname Core.config.services['toast-manager']
 * @description
 * Service configuration
 *
 * > Refers to `Core.config.services['toast-manager']`
 *
 * ```js
 * 'toast-manager': {
 *   // Toaster definitions
 *   toasters: {
 *     // <name> is the toaster name
 *     '<name>': {
 *       beforePublish: Function, // (optional) Toaster constructor `options.beforePublish`
 *       afterPublish: Function, // (optional) Toaster constructor `options.afterPublish`
 *       beforeDelete: Function, // (optional) Toaster constructor `options.beforeDelete`
 *       afterDelete: Function, // (optional) Toaster constructor `options.afterDelete`
 *     },
 *     ...
 *   }
 * }
 * ```
 */
const config = { // eslint-disable-line no-unused-vars
  /**
   * @alias Configuration.toasters
   * @type {object}
   * @description
   * Toaster definitions. An object following the schema:
   *
   * ```js
   * {
   *   [key: String]: {
   *     beforePublish?: Function,
   *     afterPublish?: Function,
   *     beforeDelete?: Function,
   *     afterDelete?: Function
   *   }
   * }
   * ```
   *
   * Object keys are the toaster names while value is a Toaster constructor options object.
   */
  toasters: {}
}
