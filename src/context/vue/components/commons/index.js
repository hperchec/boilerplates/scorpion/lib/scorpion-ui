/**
 * @vuepress
 * ---
 * title: Commons
 * headline: Commons
 * sidebarTitle: Commons
 * prev: ../
 * next: ./Page
 * ---
 */

import Page from './Page.vue'
import ScorpionLogo from './ScorpionLogo.vue'

/**
 * @alias commons
 * @typicalname Core.context.vue.components.commons
 * @description
 * Each common component is automatically registered as global component.
 */
export default {
  /**
   * @alias commons.Page
   */
  Page,
  /**
   * @alias commons.ScorpionLogo
   */
  ScorpionLogo
}
