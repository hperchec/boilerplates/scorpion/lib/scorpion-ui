---
title: constantcase
headline: constantcase
sidebarTitle: constantcase
---

# constantcase

<a name="constantcase">
</a>

## constantcase ⇒ <code>string</code>

Convert a string to constantcase *([Defined as Vue filter](../context/vue/filters#filters-constantcase))*.

**Returns**: <code>string</code> - Returns the 'constantcased' string

| Param | Type | Description |
| --- | --- | --- |
| str | <code>string</code> | The string to constantcase |

**Example**

```js
constantcase('Foo Bar') // => 'FOO_BAR'
```

