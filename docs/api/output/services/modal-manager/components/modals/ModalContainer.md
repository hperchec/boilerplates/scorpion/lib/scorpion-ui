# ModalContainer

**ModalContainer** component: the parent of all ModalOverlay instances

> This component is globally registered by Vue

## Props

| Prop name | Description | Type | Values | Default | Origin |
| - | - | - | - | - | - |
| useTransition | A props object to pass to <transition-group> component for ModalOverlay. If defined, must be an object like:<br/>```js<br/>{<br/>  mode: 'in-out',<br/>  enterActiveClass: 'custom-class',<br/>  leaveActiveClass: 'another-custom-class',<br/>  ...<br/>}<br/>```<br/><br/>See also: https://v2.fr.vuejs.org/v2/guide/transitions.html#Classes-de-transition-personnalisees | `object` | | {} | |

