/**
 * @vuepress
 * ---
 * title: "app"
 * headline: "app"
 * sidebarTitle: "app"
 * initialOpenGroupIndex: -1 # optional, defaults to 0, defines the index of initially opened subgroup
 * prev: ../
 * ---
 */

import ui from './ui'
import utils from './utils'

/**
 * '_app' root instance property
 */

/**
 * Get user settings (`this.$app.userSettings`)
 * @returns {object} Returns root instance data 'userSettings' value
 */
export function userSettings () {
  const root = this // 'this' is the Vue root instance
  return root.userSettings
}

export default {
  userSettings,
  ui,
  utils
}
