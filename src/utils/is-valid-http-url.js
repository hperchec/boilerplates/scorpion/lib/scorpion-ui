/**
 * @vuepress
 * ---
 * title: isValidHttpUrl
 * headline: isValidHttpUrl
 * sidebarTitle: isValidHttpUrl
 * ---
 */

/**
 * isValidHttpUrl
 * @param {string} str - The url string
 * @returns {(URL|boolean)} Return new URL object if valid, else false
 * @description
 * Check if `str` is a valid http URL
 * @example
 * isValidHttpUrl('http://example.com/') // success: return new URL('http://example.com/')
 * isValidHttpUrl('example.com') // fail
 */
export const isValidHttpUrl = function (str) {
  let url
  try {
    url = new URL(str)
  } catch (error) {
    return false
  }
  if (url.protocol === 'http:' || url.protocol === 'https:') {
    return url
  } else {
    return false
  }
}

export default isValidHttpUrl
