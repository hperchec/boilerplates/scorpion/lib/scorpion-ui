/**
 * @vuepress
 * ---
 * title: fileToDataUrl
 * headline: fileToDataUrl
 * sidebarTitle: fileToDataUrl
 * ---
 */

/**
 * fileToDataUrl
 * @param {File} file - The target file
 * @returns {Promise<string|ArrayBuffer>} Returns data url string
 * @description
 * Convert File object to data url (see also)
 * @example
 * fileToDataUrl(file)
 */
export const fileToDataUrl = async function (file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader()
    reader.onload = (evt) => {
      resolve(evt.target.result)
    }
    reader.onerror = reject
    reader.readAsDataURL(file)
  })
}

export default fileToDataUrl
