/**
 * @vuepress
 * ---
 * title: "DataManager class"
 * headline: "DataManager class"
 * sidebarTitle: "DataManager class"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: ./
 * next: false
 * ---
 */

import Core from '@/Core'
import Model from '@/context/support/model/Model'
import ModelCollection from '@/context/support/collection/ModelCollection'
import {
  findKey,
  isDef,
  isUndef,
  isArray,
  typeCheck
} from '@/utils'

const setReactiveKey = (data, key, value) => {
  if (isDef(data[key])) {
    data[key] = value
  } else {
    // Use Vue.set() to extend properties of state.$data because state is managed as Vue data
    // See also: https://vuex.vuejs.org/guide/state.html#single-state-tree
    Core.Vue.set(data, key, value)
  }
}

const deleteReactiveKey = (data, key) => {
  // Use Vue.delete() for reverse behavior of Vue.set()
  Core.Vue.delete(data, key)
}

/**
 * @classdesc
 * Store "resources-plugin" DataManager class
 */
class DataManager {
  /**
   * Create a DataManager
   * @param {object} data - A data object (state.$data reference)
   * @param {typeof Model} model - The model class
   * @param {object} [options = {}] - The options
   * @param {string} [options.storeModuleNamespace] - The store module namespace (Default is model storeOptions.fullNamespace)
   * @param {Function} [options.findBy] - The "findBy" function
   */
  constructor (data, model, options = {}) {
    this.data = data // Map state.$data reference
    this.model = model
    // Options
    this.storeModuleNamespace = isDef(options.storeModuleNamespace)
      ? options.storeModuleNamespace
      // @ts-ignore
      : this.model.storeOptions.fullNamespace // Default is model storeOptions.fullNamespace
    if (isDef(options.findBy)) {
      // @ts-ignore
      this.findBy = options.findBy
    }
  }

  /**
   * @type {typeof Model}
   */
  model

  /**
   * Accessors & mutators
   */

  // ...

  /**
   * Methods
   */

  compositeKeyMap = {}

  /**
   * Get data object key for model instance
   * @param {object} item - The model instance object
   * @returns {number|string} Returns the data property key
   */
  getDataKeyFromItem (item) {
    const pk = this.model.getPrimaryKey(item)
    if (isArray(pk)) {
      // composite keys
      const pkValue = pk.reduce((accumulator, key) => {
        // Get attribute name (data format)
        const dataAttributeName = this.model.attributes[key.name] && this.model.attributes[key.name].fromDataAttributeName
        accumulator[dataAttributeName] = key.value
        return accumulator
      }, {})
      if (!findKey(this.compositeKeyMap, pkValue)) {
        const uid = `uid_${Object.keys(this.compositeKeyMap).length + 1}`
        this.compositeKeyMap[uid] = pkValue
      }
      return findKey(this.compositeKeyMap, pkValue)
    } else {
      // @ts-ignore
      return pk.value // return pk value that must be unique
    }
  }

  /**
   * Default just calls model findByPK method
   * @param {*} key - The key to find item
   * @param {object[]} items - The items
   * @returns {object} - Returns the found object
   */
  findBy (key, items) {
    return this.model.findByPK(key, items)
  }

  /**
   * @category methods
   * @returns {ModelCollection} - Returns the model collection
   * @param {object} [state] - The module state
   * @description
   * Get model collection from data
   */
  getModelCollection (state) {
    const $data = state ? state.$data : this.data
    const model = this.model
    const items = Object.values($data)
    return model.collect(items.map((item) => new model(item))) // eslint-disable-line new-cap
  }

  /**
   * @category methods
   * @param {object} [state] - The module state
   * @returns {object[]} - Returns an array of all items
   * @description
   * Find all items in $data
   */
  all (state) {
    const $data = state ? state.$data : this.data
    return Object.values($data)
  }

  /**
   * @category methods
   * @param {Function|number|string} key - The key to find item. If function provided, execute this function to find item (same as Array.find function)
   * @param {object} [state] - The module state
   * @returns {object} Returns the found data item
   * @description
   * Find an item by key
   */
  find (key, state) {
    const $data = state ? state.$data : this.data
    return typeCheck(Function, key)
      // @ts-ignore
      ? Object.values($data).find(key)
      : this.findBy(key, Object.values($data))
  }

  /**
   * @category methods
   * @param {Function} callbackFn - The filter callback
   * @param {object} [state] - The module state
   * @returns {object[]} - Returns an array of filtered items
   * @description
   * Find some items that match the callbackFn function
   */
  filter (callbackFn, state) {
    const $data = state ? state.$data : this.data
    // @ts-ignore
    return Object.values($data).filter(callbackFn)
  }

  /**
   * @category methods
   * @param {...*} args - Same as filter
   * @returns {object[]} - Returns an array of filtered items
   * @description
   * Alias of {@link DataManager#filter filter}
   */
  findMany (...args) {
    // @ts-ignore
    return this.filter(...args)
  }

  /**
   * Insert method
   * @param {object[]|object} payload - The model instance. Can be Array
   * @param {object} [state] - The module state
   * @returns {object[]|object|Promise<object[]|object>} - Returns the inserted item(s) or a Promise that resolve it
   */
  insert (payload, state) {
    const $data = state ? state.$data : this.data
    const inserted = []
    // Insert method
    const insert = (item) => {
      const localDataKey = this.getDataKeyFromItem(item)
      if (isDef($data[localDataKey])) {
        throw new Error(`[${this.storeModuleNamespace}] Unable to push item "${localDataKey}" to resource module data (already defined): `, item)
      } else {
        inserted.push(this.upsert(item, state))
      }
    }
    if (payload instanceof Array) {
      for (const item of payload) {
        insert(item)
      }
      return inserted.some((item) => item instanceof Promise)
        ? Promise.all(inserted).then((values) => values)
        : inserted
    } else {
      const item = payload
      insert(item)
      return inserted[0] // return only first item
    }
  }

  /**
   * Update method
   * @param {object} item - The model instance
   * @param {object} [state] - The module state
   * @returns {object} Returns the updated object
   */
  update (item, state) {
    const $data = state ? state.$data : this.data
    const localDataKey = this.getDataKeyFromItem(item)
    if (isUndef($data[localDataKey])) {
      throw new Error(`[${this.storeModuleNamespace}] Unable to update item "${localDataKey}" in resource module data (does not exist): `, item)
    } else {
      return this.upsert(item, state)
    }
  }

  /**
   * Upsert method
   * @param {object[]|object} payload - Can be Array
   * @param {object} [state] - The module state
   * @returns {object[]|object} - Returns the upserted item(s)
   */
  upsert (payload, state) {
    const $data = state ? state.$data : this.data
    const upserted = []
    // Upsert method
    const upsert = (item) => {
      const localDataKey = this.getDataKeyFromItem(item)
      const mutationType = isDef($data[localDataKey])
        ? 'UPDATE'
        : 'INSERT'
      const beforeHook = this.model.hooks.store[`BEFORE_${mutationType}`]
      const afterHook = this.model.hooks.store[`AFTER_${mutationType}`]
      let toUpsert
      const setDataAndExecAfterHook = (data) => {
        setReactiveKey($data, localDataKey, data)
        const upsertedItem = $data[localDataKey]
        const afterHookResult = afterHook.chain(upsertedItem)
        if (afterHookResult instanceof Promise) {
          return new Promise((resolve, reject) => {
            afterHookResult.then((payload) => {
              resolve(payload[0])
            })
          })
        } else {
          return afterHookResult[0]
        }
      }
      const beforeHookResult = beforeHook.chain(item)
      if (beforeHookResult instanceof Promise) {
        toUpsert = new Promise((resolve, reject) => {
          beforeHookResult.then((payload) => {
            const res = setDataAndExecAfterHook(payload[0])
            if (res instanceof Promise) {
              res.then((value) => resolve(value))
            } else {
              resolve(res)
            }
          }).catch((error) => reject(error))
        })
      } else {
        toUpsert = setDataAndExecAfterHook(beforeHookResult[0])
      }
      upserted.push(toUpsert)
    }
    if (payload instanceof Array) {
      for (const item of payload) {
        upsert(item)
      }
      return upserted.some((item) => item instanceof Promise)
        ? Promise.all(upserted).then((values) => values)
        : upserted
    } else {
      const item = payload
      upsert(item)
      return upserted[0] // return only first item
    }
  }

  /**
   * Delete an item from data
   * @param {Function|number|string} key - Same as find method
   * @param {object} [state] - The module state
   * @returns {object|Promise<object>} - The deleted item data
   */
  delete (key, state) {
    const $data = state ? state.$data : this.data

    const target = typeof key === 'object'
      ? $data[this.getDataKeyFromItem(key)]
      : this.find(key, state)

    if (isUndef(target)) {
      throw new Error(`[${this.storeModuleNamespace}] Unable to delete item with key "${key}" from resource module data (does not exist)`)
    } else {
      const beforeHook = this.model.hooks.store.BEFORE_DELETE
      const afterHook = this.model.hooks.store.AFTER_DELETE
      const deleteDataAndExecAfterHook = (data) => {
        deleteReactiveKey($data, this.getDataKeyFromItem(target))
        const afterHookResult = afterHook.chain(data)
        if (afterHookResult instanceof Promise) {
          return new Promise((resolve, reject) => {
            afterHookResult.then((payload) => {
              resolve(payload[0])
            })
          })
        } else {
          return afterHookResult[0]
        }
      }
      const beforeHookResult = beforeHook.chain(target)
      if (beforeHookResult instanceof Promise) {
        return new Promise((resolve, reject) => {
          beforeHookResult.then((payload) => {
            const res = deleteDataAndExecAfterHook(payload[0])
            if (res instanceof Promise) {
              res.then((value) => resolve(value))
            } else {
              resolve(res)
            }
          }).catch((error) => reject(error))
        })
      } else {
        return deleteDataAndExecAfterHook(beforeHookResult[0])
      }
    }
  }
}

export default DataManager
