export default APIManager;
declare const APIManager_base: any;
declare class APIManager extends APIManager_base {
    [x: string]: any;
    static reactive: boolean;
    static rootOption: string;
    static aliases: any[];
    static install(_Vue: import("vue/types/umd"), options?: object): void;
    constructor(options: {
        apis: API[];
    });
    readonly get apis(): API[];
    add(api: API): API;
    use(apiName: string): API;
    toJSON(): object;
}
import API from './API';
//# sourceMappingURL=APIManager.d.ts.map