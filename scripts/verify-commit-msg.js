const colors = require('colors') // eslint-disable-line
const msgPath = process.env.GIT_PARAMS
const msg = require('fs').readFileSync(msgPath, 'utf-8').trim()

const commitRE =
  /^(revert: )?(wip|release|feat|fix|polish|docs|style|refactor|perf|test|workflow|ci|chore|types|build|cli)(\(.+\))?: .{1,50}/

if (!commitRE.test(msg)) {
  console.log()
  console.error(
    'ERROR: invalid commit message format.\n'.red +
    '  Proper commit message format is required for automated changelog generation.\n' +
    '  See ./COMMIT_CONVENTION.md for more details.\n'
  )
  process.exit(1)
}
