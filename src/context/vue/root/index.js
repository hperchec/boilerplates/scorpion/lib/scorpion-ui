/**
 * @vuepress
 * ---
 * title: Root instance
 * headline: Root instance
 * sidebarTitle: Root instance
 * initialOpenGroupIndex: -1 # optional, defaults to 0, defines the index of initially opened subgroup
 * prev: ../plugins/
 * next: ./prototype/
 * ---
 */

import options from './options'
import prototype from './prototype'

/**
 * @alias root
 * @typicalname Core.context.vue.root
 * @type {object}
 * @description
 * Vue root instance specific data/options...
 */
export default {
  /**
   * @alias root.setOptions
   * @param {object} mixin - Root instance mixin
   * @returns {void}
   * @description
   * Extend Vue root instance options by passing mixin.
   */
  setOptions (mixin) {
    this.options.mixins.push(mixin)
  },
  /**
   * @see {@link ./options options}
   */
  options,
  /**
   * @see {@link ./prototype prototype}
   */
  prototype
}
