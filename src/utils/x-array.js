/**
 * @vuepress
 * ---
 * title: XArray
 * headline: XArray
 * sidebarTitle: XArray
 * ---
 */

import isArray from './is-array'
import isDef from './is-def'

/**
 * Extended array
 */
export class XArray extends Array {
  /**
   * Extended constructor: first arg can be Array
   * @param {...*} args - Same parameters as Array constructor
   * @example
   * new XArray([ 'foo', 'bar' ])
   */
  constructor (...args) {
    if (args.length === 1 && isArray(args[0])) {
      super(...args[0])
    } else {
      super(...args)
    }
  }

  /**
   * Insert method: insert a value in array at `<index>`. If `<index>` not provided, push at the end of the array.
   * @param {*} value - The value to insert
   * @param {number} [index = undefined] - The index
   */
  insert (value, index = undefined) {
    if (isDef(index)) {
      this.splice(index, 0, value)
    } else {
      this.push(value)
    }
  }
}

export default XArray
