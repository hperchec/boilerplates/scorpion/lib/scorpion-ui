import Core from '@/Core'
import { get, has, isDef } from '@/utils'

/**
 * makeConfigurableOption
 * @param {object} target - Target service options object
 * @param {object} options - Options
 * @param {string} options.propertyName - The property name
 * @param {string} options.serviceIdentifier - The service identifier
 * @param {string} options.configPath - The config path to check with has/get utils
 * @param {Function} [options.formatter] - The function to transform value from configuration. Default returns value as is.
 * @param {*} options.defaultValue - The default value
 * @returns {void}
 * @description
 * Make service option configurable: the value of the option is extracted from Core global config 'services' property.
 */
export const makeConfigurableOption = function (target, options) {
  let optionValue = options.defaultValue // Default value
  const descriptor = {
    get: function () {
      const serviceGlobalConfig = Core.config.services[options.serviceIdentifier]
      if (has(serviceGlobalConfig, options.configPath)) {
        const formatter = isDef(options.formatter)
          ? options.formatter
          : (value) => value
        return formatter(get(serviceGlobalConfig, options.configPath))
      } else {
        return optionValue
      }
    },
    set: function (value) {
      const serviceGlobalConfig = Core.config.services[options.serviceIdentifier]
      if (has(serviceGlobalConfig, options.configPath)) {
        throw new Error(`Can't set option of service "${options.serviceIdentifier}". The option is defined in global configuration: Core.config.services['${options.serviceIdentifier}'].${options.configPath}.`)
      } else {
        optionValue = value
      }
    },
    enumerable: true
  }
  Object.defineProperty(target, options.propertyName, descriptor)
}

export default makeConfigurableOption
