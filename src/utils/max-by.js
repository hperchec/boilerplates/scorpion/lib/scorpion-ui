/**
 * @vuepress
 * ---
 * title: maxBy
 * headline: maxBy
 * sidebarTitle: maxBy
 * ---
 */

// Lodash.maxby
import lodashMaxBy from 'lodash.maxby'

/**
 * maxBy
 * @param {Array} array - The array
 * @param {Function} sortFct - The sort function
 * @returns {string} Returns the found value in `array`
 * @description
 * Find max by in array (lodash.maxby).
 *
 * ::: tip See also
 * [lodash.maxby](https://www.npmjs.com/package/lodash.maxby) package documentation
 * :::
 * @example
 * const array = [ { id: 1, name: 'John' }, { id: 2, name: 'Jane' } ]
 * maxBy(array, function (item) { return item.id }) // Return -> { id: 2, name: 'Jane' }
 * maxBy(array, function (item) { return item.name }) // Return -> { id: 1, name: 'John' }
 */
export const maxBy = function (array, sortFct) {
  return lodashMaxBy(array, sortFct)
}

export default maxBy
