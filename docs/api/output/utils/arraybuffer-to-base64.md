---
title: arrayBufferToBase64
headline: arrayBufferToBase64
sidebarTitle: arrayBufferToBase64
---

# arrayBufferToBase64

<a name="arraybuffertobase64">
</a>

## arrayBufferToBase64 ⇒ <code>string</code>

Convert an ArrayBuffer instance to base64 encoded string::: tip See also[base64-arraybuffer-es6](https://www.npmjs.com/package/base64-arraybuffer-es6) package documentation:::

**Returns**: <code>string</code> - Returns the base64 encoded string

| Param | Type | Description |
| --- | --- | --- |
| buffer | <code>ArrayBuffer</code> | The ArrayBuffer |
| [byteOffset] | <code>number</code> | See `base64-arraybuffer-es6` package documentation |
| [lngth] | <code>number</code> | See `base64-arraybuffer-es6` package documentation |

**Example**

```js
// buffer is an ArrayBuffer containing string: 'foo bar'arrayBufferToBase64(buffer) // => ''
```

