/**
 * @vuepress
 * ---
 * title: isUndef
 * headline: isUndef
 * sidebarTitle: isUndef
 * ---
 */

/**
 * isUndef
 * @param {*} value - The variable to check
 * @returns {boolean} Returns true if value is undefined
 * @description
 * Return true if variable is undefined (=== undefined)
 * @example
 * isUndef(1) // => false
 * isUndef(undefined) // => true
 */
export const isUndef = function (value) {
  return value === undefined
}

export default isUndef
