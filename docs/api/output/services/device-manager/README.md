---
title: Service (device-manager)
headline: Service (device-manager)
sidebarTitle: device-manager
initialOpenGroupIndex: -1
prev: ../
next: ./options
---

# Service (device-manager)

<a name="service"></a>

The service "device-manager" is designed to make authentication managemnent easier.

## Register

On registering, the service will add to root instance:- the `currentDevice` data property (default: undefined)it will also inject properties to Vue root instance prototype `$app.device`:- `$app.device`and global mixin:- `Core.context.vue.mixins.global.DeviceManagerMixin`: see [DeviceManagerMixin](./mixins/global/DeviceManagerMixin)

## Create

Returns an instance of `DeviceManager` created with options.

**Returns**: <code>module:services/device-manager/DeviceManager~DeviceManager</code> - Once services are created:

```js
Core.service('device-manager')
```

## Vue

### Plugin

On registering, service Vue plugin will be set in `Core.context.vue.plugins['device-manager']`.

Plugin will be automatically used by Vue.

The following properties will be set to Vue prototype:```jsVue.prototype.$deviceManager // => Core.service('device-manager')```

### Root instance

<a name="app"></a>

#### Prototype $app property

On registering, the service will inject properties to \`Core.context.vue.root.prototype.app\`.

> Properties will be shared by all components under \`vm.$app\` where \`vm\` is the component instance.

**Schema**:

- [$app](#app) : <code>object</code>
  - [.device](#app-device) : <code>object</code>
  - [.userSettings](#app-usersettings) : <code>object</code>

<a name="app-usersettings"></a>

##### User settings

Service will define the following user settings keys:

##### Others

<a name="app-device">
</a>

###### vm.$app.device : <code>object</code>

Get device utils

**Example**

```js
See [mobile-device-detect](https://www.npmjs.com/package/mobile-device-detect) package documentation
```

## Expose (service context)

The service will expose the following properties under `Core.context.services['device-manager']`:
<a name="expose-device">
</a>

### .Device

> Exposed as `Core.context.services['device-manager'].Device`

**See**: [Device](./Device)

<a name="expose-devicemanager">
</a>

### .DeviceManager

> Exposed as `Core.context.services['device-manager'].DeviceManager`

**See**: [DeviceManager](./DeviceManager)

