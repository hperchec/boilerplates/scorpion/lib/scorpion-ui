# 🦂 Scorpion UI default template

> *This file is generated with **@hperchec/readme-generator**. Don't edit it.*

[![Scorpion UI](https://img.shields.io/static/v1?label=scorpion-ui&message=<%= scorpionUiVersion %>&color=362F2D&logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACWklEQVQ4T4WSTUhUURTH753PN2MTRZAtAinKdiGt2ojbCAIrmEghU4i+aMApyU3ja2aYmaaNOBLRQl0UUUESgSESDhTkJiRKTFv0gVEkBco0737MPaf7RuerUu/q8e45v3v+//+hpOoszya2+Ti5TySZcS1u6qWHQ7z6/n/ftPSTzSd3OyUdIxz2EQaEcJyGnGrzHjHfrwcpAwqzybsoSftKM8wgUxOEkS5lFZp8wfjHtSAVwLtEBwoyYgOQawiDTuDwkwhsMIKxwQ0BOGVuLjjcP/TrXrSnYAgoVMrz1tlHTbOwIcAukK/i87p5r262ZRAbRBlmJYe2urOJb+uaWAS8iH1HhvV2sy0FGC5QrqaIBQe1nNO+y+nnf0PKHtgXIhvjutFT9KEkg5NG+lueQIlRtFTUIIG4lqRfWDllAI7frJNOKwcWXHJwyKyOn3crdwP/tbSFKNeHIpTjhMFkO81kFmsBk+ZOyelrz6G+evEgcpEwdZwKfOk+k4jYhez6lWW0IGBPRwV5Ytzqb60B5J6Z+z2KvEEBQe+x6KNqrcwMN6Kic9qLojc62mHfnYGuGoAcu9aC0pnVC/QVODb7TlWWJ2f27HBx+KwlfNE+7NEmX/UPD6ZrAPyp2UoFjK6aJwhXEe+F1I3SJFZPdwvmIUwENFPpPOAb6f9UAxCPI53a6aHiiAxQ74LwgGMX7a7knz8fmlachANDA5P/pCAemk0gCuOU43Y9ogCuEsaSP6kjE6Xi/LnQUf/tgdFqf2r2AO/1buU5R4oyOKnjcusktKmODiOenltrlf8Awt9jILDjUAQAAAAASUVORK5CYII=)](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui)
[![Discord](https://img.shields.io/discord/<%= discordServerID %>?label=Discord&labelColor=5562EA&logo=discord&logoColor=white&color=2F3035)](<%= discordInviteLink %>)

![author](https://img.shields.io/static/v1?label=&message=Author:&color=black)
[![herve-perchec](http://herve-perchec.com/badge.svg)](http://herve-perchec.com/)

[![release](https://img.shields.io/gitlab/v/release/<%= projectPath %>?include_prereleases&logo=gitlab&sort=semver)](<%= projectUrl %>/-/releases)
[![pipeline-status](<%= projectUrl %>/badges/main/pipeline.svg)](<%= projectUrl %>/commits/main)
[![package](https://img.shields.io/npm/v/<%= packageName %>?logo=npm)](<%= packageUrl %>)
[![downloads](https://img.shields.io/npm/dw/<%= packageName %>?logo=npm)](<%= packageUrl %>)
[![issues](https://img.shields.io/gitlab/issues/open/<%= projectPath %>?gitlab_url=https%3A%2F%2Fgitlab.com)](<%= issuesUrl %>)
![license](https://img.shields.io/gitlab/license/<%= projectPath %>?gitlab_url=https%3A%2F%2Fgitlab.com)

**Table of contents**:

<!-- markdownlint-disable-next-line emphasis-style -->
[[_TOC_]]

Scorpion UI library

<!-- markdownlint-disable line-length -->
<svg version="1.1" id="scorpion_logo" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100px" height="100px" viewBox="0 0 1200 1200" enable-background="new 0 0 1200 1200" xml:space="preserve"><g>
    <linearGradient id="scorpion_logo_gradient" gradientUnits="userSpaceOnUse" x1="1703.8506" y1="199.5737" x2="984.5436" y2="1058.8916" gradientTransform="matrix(-0.91 0.4147 0.4147 0.91 1482.0361 -461.2266)">
        <stop offset="0" style="stop-color:#FCDF8A"/>
        <stop offset="0.4916" style="stop-color:#F9A17C"/>
        <stop offset="1" style="stop-color:#F65D6C"/>
    </linearGradient>
    <path fill="url(#scorpion_logo_gradient)" d="M565.418,718.28c-7.933,16.874-29.095,26.496-47.026,21.383L413.74,709.821 c-17.931-5.113-38.287-23.454-45.235-40.757l-22.793-56.764c-6.948-17.303-17.978-17.171-24.511,0.293l-27.329,73.058 c-6.533,17.464-3.922,44.771,5.801,60.681l64.219,105.079c9.723,15.91,31.582,35.209,48.574,42.886l49.161,22.212 c16.992,7.677,37.503,27.708,45.581,44.514l31.418,65.372c8.077,16.806-0.51,29.203-19.083,27.549l-66.472-5.919 c-18.572-1.653-20.975,5.304-5.338,15.462l179.42,116.555c15.637,10.158,13.174,18.422-5.472,18.364l-206.528-0.635 c-18.646-0.058-46.837-8.192-62.647-18.077l-131.726-82.355c-15.811-9.885-27.72-33.193-26.466-51.798l42.454-629.73 c1.254-18.604-2.775-48.219-8.953-65.812l-52.771-150.265c-6.178-17.592,0.232-42.05,14.245-54.351L335.167,17.333 c14.013-12.301,37.814-13.388,52.89-2.417l191.75,139.539c15.076,10.972,32.373,34.375,38.436,52.007l26.642,77.472 c6.063,17.632-1.251,23-16.253,11.928L437.029,154.461c-15.003-11.072-20.661-6.385-12.574,10.417l35.857,74.496 c8.087,16.801,26.869,39.753,41.738,51.004l507.186,383.791c14.869,11.251,24.651,35.525,21.739,53.942l-24.267,153.446 c-2.912,18.417-15.269,45.029-27.458,59.14L844.237,1096.98c-12.189,14.109-20.042,10.547-17.45-7.918l29.746-211.878 c2.592-18.465-4.235-21.216-15.172-6.114l-39.143,54.05c-10.937,15.102-25.927,13.449-33.312-3.672l-28.724-66.598 c-7.385-17.121-9.046-45.743-3.692-63.604l15.49-51.674c5.354-17.861,5.129-47.019-0.499-64.795l-37.175-117.405 c-5.628-17.776-24.524-37.661-41.99-44.188l-73.066-27.305c-17.466-6.527-24.802,1.71-16.301,18.306l27.887,54.442 c8.501,16.595,8.965,43.979,1.032,60.854L565.418,718.28z"/>
</g></svg>
<!-- markdownlint-enable line-length -->

## Development

see contributing file

## 🚀 Get started

Install package:

```sh
npm install <%= packageName %>
```

Use template:

```js
import { Core } from '@hperchec/scorpion-ui'
import template from '@hperchec/scorpion-ui-template-default'

Core.useTemplate(template)
```

## 📙 Documentation

Please check the [documentation](http://scorpion-ui.herve-perchec.com/docs/template)

## 🧱 Dependencies

<details>
<summary>Global</summary>

<%= dependencies %>

</details>

<details>
<summary>Dev</summary>

<%= devDependencies %>

</details>

<details>
<summary>Peer</summary>

<%= peerDependencies %>

</details>

## ✍ Author

<%= author %>

## 📄 License

<%= license %>

----

Made with ❤ by <%= author %>

<!-- markdownlint-disable MD013 -->
