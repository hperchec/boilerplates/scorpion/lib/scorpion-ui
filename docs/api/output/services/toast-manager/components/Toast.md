# Toast

**Toast** component

> This component is globally registered by Vue

**Extends**:

- BaseToast: `BaseToast.vue`

## Props

| Prop name | Description | Type | Values | Default | Origin |
| - | - | - | - | - | - |
| toast | toast | `Toast` | | null | |
| noClose | noClose | `boolean` | | false | |

## Slots

| Name | Description | Bindings |
| - | - | - |
| default | | |

