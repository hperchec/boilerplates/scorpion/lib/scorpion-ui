export function privateMiddleware(middlewareFunc: Function, metaKey?: string): Function;
export default privateMiddleware;
//# sourceMappingURL=private-middleware.d.ts.map