export class VuePluginable {
    static get reactive(): boolean;
    static get rootOption(): string;
    static get aliases(): string[];
    static createMixin(): Vue.ComponentOptions<Vue>;
    static install(_Vue: Vue.VueConstructor, options?: object): void;
    constructor(data?: object);
    private _vm;
    readonly get vm(): import("vue/types/umd");
    setVMProp(name: string, value: any): void;
    _initVM(data: object): void;
    destroyVM(): void;
}
export default VuePluginable;
//# sourceMappingURL=VuePluginable.d.ts.map