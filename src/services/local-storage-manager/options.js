/**
 * @vuepress
 * ---
 * title: "Service (local-storage-manager) options"
 * headline: "Service (local-storage-manager) options"
 * sidebarTitle: .options
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import toolbox from '@/toolbox'

const { makeConfigurableOption } = toolbox.services

/**
 * @name options
 * @static
 * @description
 * Default is an empty object
 */
const options = {
  //
}

/**
 * setItemEventName
 * @alias options.setItemEventName
 * @type {string}
 * @default 'ls-item-set'
 * @description
 * Event name to dispatch at document level when item is set in local storage.
 *
 * Can be overwritten via Core global configuration: `Core.config.services['local-storage-manager'].setItemEventName`
 *
 * Default: `'ls-item-set'`
 */
makeConfigurableOption(options, {
  propertyName: 'setItemEventName',
  serviceIdentifier: 'local-storage-manager',
  configPath: 'setItemEventName',
  defaultValue: 'ls-item-set'
})

/**
 * removeItemEventName
 * @alias options.removeItemEventName
 * @type {string}
 * @default 'ls-item-removed'
 * @description
 * Event name to dispatch at document level when item is removed from local storage.
 *
 * Can be overwritten via Core global configuration: `Core.config.services['local-storage-manager'].removeItemEventName`
 *
 * Default: `'ls-item-removed'`
 */
makeConfigurableOption(options, {
  propertyName: 'removeItemEventName',
  serviceIdentifier: 'local-storage-manager',
  configPath: 'removeItemEventName',
  defaultValue: 'ls-item-removed'
})

/**
 * onSetItem
 * @alias options.onSetItem
 * @type {Function|Function[]}
 * @default undefined
 * @description
 * Callback to execute when item is set in local storage.
 *
 * Can be overwritten via Core global configuration: `Core.config.services['local-storage-manager'].onSetItem`
 *
 * Default: `undefined`
 */
makeConfigurableOption(options, {
  propertyName: 'onSetItem',
  serviceIdentifier: 'local-storage-manager',
  configPath: 'onSetItem'
})

/**
 * onRemoveItem
 * @alias options.onRemoveItem
 * @type {Function|Function[]}
 * @default undefined
 * @description
 * Callback to execute when item is removed from local storage.
 *
 * Can be overwritten via Core global configuration: `Core.config.services['local-storage-manager'].onRemoveItem`
 *
 * Default: `undefined`
 */
makeConfigurableOption(options, {
  propertyName: 'onRemoveItem',
  serviceIdentifier: 'local-storage-manager',
  configPath: 'onRemoveItem'
})

export default options
