export default function generateActions(model: any, options?: {
    index?: object;
    create?: object;
    update?: object;
    retrieve?: object;
    delete?: object;
}): object;
//# sourceMappingURL=generate-actions.d.ts.map