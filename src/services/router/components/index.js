import publicComponents from './public'
import privateComponents from './private'

export default {
  public: publicComponents,
  private: privateComponents
}
