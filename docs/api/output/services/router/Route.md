---
title: Route class
headline: Route class
sidebarTitle: .Route
sidebarDepth: 0
prev: false
next: false
---

# Route class

<a name="route">
</a>

## Route

Class that represents a route (view)

**Schema**:

- [Route](#route)
  - [new Route(path, name, component, [options])](#newroutenew)
  - _instance_
    - [.toJSON()](#route-tojson) ⇒ <code>object</code>
    - _methods_
      - [.setMeta(meta)](#route-setmeta) ⇒ <code>void</code>
      - [.getComponentConstructorName()](#route-getcomponentconstructorname) ⇒ <code>string</code>
    - _properties_
      - [.defaultMeta](#route-defaultmeta) : <code>object</code>
      - [.path](#route-path) : <code>string</code>
      - [.name](#route-name) : <code>string</code>
      - [.component](#route-component) : <code>object</code> \| <code>null</code>
      - [.alias](#route-alias) : <code>string</code>
      - [.redirect](#route-redirect) : <code>string</code> \| <code>object</code> \| <code>function</code>
      - [.children](#route-children) : <code>Array.&lt;object&gt;</code>
      - [.modelBindings](#route-modelbindings) : <code>object</code>
      - [.queryParams](#route-queryparams) : <code>object</code>
      - [.props](#route-props) : <code>function</code>
      - [.beforeEnter](#route-beforeenter) : <code>function</code>
      - [.meta](#route-meta) : <code>object</code>
      - [.defaultQueryParamTypeCast](#route-defaultqueryparamtypecast) : <code>Array</code>
  - _static_
    - _methods_
      - [.getDefaultCastForType(type)](#route-getdefaultcastfortype) ⇒ <code>function</code>
      - [.buildRouteComponentName(name)](#route-buildroutecomponentname) ⇒ <code>string</code>
      - [.redirectToNotFound([route], [...args])](#route-redirecttonotfound) ⇒ <code>void</code>

<a name="newroutenew">
</a>

### new Route(path, name, component, [options])

Create a Route

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| path | <code>string</code> |  | The route path |
| name | <code>string</code> |  | The route name |
| component | <code>function</code> \| <code>null</code> |  | A function that returns the route Vue component (can be null) |
| [options] | <code>object</code> | <code>{}</code> | Object that contains other the vue-router route object properties |
| [options.alias] | <code>string</code> |  | The route alias |
| [options.redirect] | <code>string</code> \| <code>object</code> \| <code>function</code> |  | The route redirect |
| [options.children] | <code>Array.&lt;object&gt;</code> |  | The route children |
| [options.modelBindings] | <code>object</code> |  | The route model bindings |
| [options.query] | <code>object</code> |  | The route query param definitions |
| [options.props] | <code>function</code> |  | The route props |
| [options.beforeEnter] | <code>function</code> |  | The route beforeEnter function |
| [options.meta] | <code>object</code> |  | The route meta fields |

<a name="route-tojson">
</a>

### route.toJSON() ⇒ <code>object</code>

Overrides the default toJSON object method for JSON.stringify() calls

**Returns**: <code>object</code> - - The instance data to be serialized

<a name="route-setmeta">
</a>

### route.setMeta(meta) ⇒ <code>void</code>

Set meta fields

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| meta | <code>object</code> | The meta object to assign |

<a name="route-getcomponentconstructorname">
</a>

### route.getComponentConstructorName() ⇒ <code>string</code>

Get the route component name

**Returns**: <code>string</code> - - The route component name

**Category**: methods  
<a name="route-defaultmeta">
</a>

### route.defaultMeta : <code>object</code>

Default meta

**Category**: properties  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| requiresAuth | <code>boolean</code> | If route requires authentication (default: `false`) |
| layout | <code>object</code> | The page layout (vue component) (default: `undefined`) |
| requiresVerifiedEmail | <code>boolean</code> | If route requires a verified email (default: `false`) |
| breadcrumb | <code>object</code> | Breadcrumb options. See also: |
| pageMeta | <code>object</code> | Page meta |
| pageMeta.title | <code>string</code> | Page title |

<a name="route-path">
</a>

### route.path : <code>string</code>

The route path (See also: [vue-router documentation](https://router.vuejs.org/api/#path))

**Category**: properties  
<a name="route-name">
</a>

### route.name : <code>string</code>

The route name (See also: [vue-router documentation](https://router.vuejs.org/api/#name))

**Category**: properties  
<a name="route-component">
</a>

### route.component : <code>object</code> \| <code>null</code>

The route component (See also: [vue-router documentation](https://router.vuejs.org/guide/essentials/dynamic-matching.html))

**Category**: properties  
<a name="route-alias">
</a>

### route.alias : <code>string</code>

The route alias (See also: [vue-router documentation](https://router.vuejs.org/api/#alias))

**Category**: properties  
<a name="route-redirect">
</a>

### route.redirect : <code>string</code> \| <code>object</code> \| <code>function</code>

The route redirect (See also: [vue-router documentation](https://router.vuejs.org/guide/essentials/redirect-and-alias.html#redirect))

**Category**: properties  
<a name="route-children">
</a>

### route.children : <code>Array.&lt;object&gt;</code>

The route children (See also: [vue-router documentation](https://router.vuejs.org/api/#children))

**Category**: properties  
<a name="route-modelbindings">
</a>

### route.modelBindings : <code>object</code>

The route model bindings. An object like:```js{  user: { model: User, param: 'user' }}```

**Category**: properties  
<a name="route-queryparams">
</a>

### route.queryParams : <code>object</code>

The route query params. An object like:```js{  search: { type: String, cast: value => value }}```

**Category**: properties  
<a name="route-props">
</a>

### route.props : <code>function</code>

The route props in function format (See also: [vue-router documentation](https://router.vuejs.org/api/#props)).If model bindings are defined, props are automatically defined. Query params too, under "queryParams" prop.Re-define these props to override.

**Category**: properties  
<a name="route-beforeenter">
</a>

### route.beforeEnter : <code>function</code>

The route beforeEnter function (See also: [vue-router documentation](https://router.vuejs.org/api/#beforeenter))

**Category**: properties  
<a name="route-meta">
</a>

### route.meta : <code>object</code>

The route meta fields (See also: [vue-router documentation](https://router.vuejs.org/api/#meta))

**Category**: properties  
**Read only**: true  
<a name="route-defaultqueryparamtypecast">
</a>

### route.defaultQueryParamTypeCast : <code>Array</code>

Default query param type cast methods

**Category**: properties  
<a name="route-getdefaultcastfortype">
</a>

### Route.getDefaultCastForType(type) ⇒ <code>function</code>

Static method to get default cast method for standard types

**Returns**: <code>function</code> - Returns the cast function

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| type | <code>\*</code> | The target type |

<a name="route-buildroutecomponentname">
</a>

### Route.buildRouteComponentName(name) ⇒ <code>string</code>

Default returns name + 'RouteComponent', in pascalcase

**Returns**: <code>string</code> - Returns the route component constructor name

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The route name |

<a name="route-redirecttonotfound">
</a>

### Route.redirectToNotFound([route], [...args]) ⇒ <code>void</code>

Logic to redirect to "not found" route.If no argument provided, it will take the value of `Core.config.services.router.catchAllRedirect`

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| [route] | <code>object</code> \| <code>string</code> | The target toute |
| [...args] | <code>any</code> | The rest to pass to router.push method |

