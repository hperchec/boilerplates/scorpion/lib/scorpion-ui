---
title: Service (event-bus) options
headline: Service (event-bus) options
sidebarTitle: .options
sidebarDepth: 0
prev: false
next: false
---

# Service (event-bus) options

<a name="options">
</a>

## .options

Accepts the following options:- `strictMode`Configurable options:- **strictMode**: `Core.config.services['event-bus'].strictMode`

<a name="options-strictmode">
</a>

### options.strictMode : <code>boolean</code>

Define if EventBus throw Error when emitting event which has no listener defined.Can be overwritten via Core global configuration: `Core.config.services['event-bus'].strictMode`

**Default**: <code>true</code>  
