/**
 * @vuepress
 * ---
 * title: "Service (error-manager) configuration"
 * headline: "Service (error-manager) configuration"
 * sidebarTitle: Configuration
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * ⚠ THIS FILE IS ONLY USED TO GENERATE SERVICE CONFIGURATION DOCUMENTATION
 */

/**
 * @name Configuration
 * @typicalname Core.config.services['error-manager']
 * @description
 * Service configuration
 *
 * > Refers to `Core.config.services['error-manager']`
 *
 * ```js
 * 'error-manager': {
 *   // Overwrites service option: `autoRegister`
 *   autoRegister: Boolean
 * }
 * ```
 */
const config = { // eslint-disable-line no-unused-vars
  /**
   * @alias Configuration.autoRegister
   * @type {boolean}
   * @description
   * Auto register all error classes/constructors under `Core.context.support.errors`
   */
  autoRegister: true
}
