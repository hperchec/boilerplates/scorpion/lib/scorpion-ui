/**
 * @vuepress
 * ---
 * title: getHostHttpUrl
 * headline: getHostHttpUrl
 * sidebarTitle: getHostHttpUrl
 * ---
 */

/**
 * getHostHttpUrl
 * @param {string} str - The url string
 * @returns {(string|boolean)} Returns the substring of url
 * @description
 * Returns server url
 * @example
 * getHostHttpUrl('http://example.com/blog/posts?id=1') // return 'http://example.com/'
 * getHostHttpUrl('example.com/blog/posts/') // fail
 */
export const getHostHttpUrl = function (str) {
  let url
  try {
    url = new URL(str)
  } catch (error) {
    return false
  }
  if (url.protocol === 'http:' || url.protocol === 'https:') {
    return url.protocol + '//' + url.host
  } else {
    return false
  }
}

export default getHostHttpUrl
