/**
 * @vuepress
 * ---
 * title: Router - 'private' routes
 * headline: Router - 'private' routes
 * sidebarTitle: .private
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * @name private
 * @static
 * @type {object}
 */
export default {}
