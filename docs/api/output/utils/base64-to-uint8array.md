---
title: base64ToUint8Array
headline: base64ToUint8Array
sidebarTitle: base64ToUint8Array
---

# base64ToUint8Array

<a name="base64touint8array">
</a>

## base64ToUint8Array ⇒ <code>Uint8Array</code>

Convert an base64 encoded string to Uint8Array instance::: tip See also[Uint8Array](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Uint8Array/Uint8Array) constructor documentation:::

**Returns**: <code>Uint8Array</code> - Returns the Uint8Array

| Param | Type | Description |
| --- | --- | --- |
| str | <code>string</code> | The base64 encoded string |
| [toArrayBufferOptions] | <code>object</code> | See base64ToArrayBuffer util |
| [offset] | <code>number</code> | See Uint8Array constructor second param |
| [length] | <code>number</code> | See Uint8Array constructor thrid param |

**Example**

```js
base64ToUint8Array('Zm9vIGJhcg==') // => Uint8Array containing the string: 'foo bar'
```

