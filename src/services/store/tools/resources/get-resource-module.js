import getModule from '../get-module'

/**
 * Get a resource module by name
 * @param {import("vuex").Store} store - The store instance
 * @param {string} moduleName - The resource module name (ex: "Users")
 * @param {?string} [rootModule="Resources"] - The root resource module ( Default: "Resources")
 * @returns {object} The module object
 */
export const getResourceModule = (store, moduleName, rootModule = 'Resources') => {
  const namespace = [ rootModule, moduleName ].join('/')
  return getModule(store, namespace)
}

export default getResourceModule
