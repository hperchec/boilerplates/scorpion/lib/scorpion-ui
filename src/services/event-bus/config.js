/**
 * @vuepress
 * ---
 * title: "Service (event-bus) configuration"
 * headline: "Service (event-bus) configuration"
 * sidebarTitle: Configuration
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * ⚠ THIS FILE IS ONLY USED TO GENERATE SERVICE CONFIGURATION DOCUMENTATION
 */

/**
 * @name Configuration
 * @typicalname Core.config.services['event-bus']
 * @description
 * Service configuration
 *
 * > Refers to `Core.config.services['event-bus']`
 *
 * ```js
 * 'event-bus': {
 *   // Enables the strict mode.
 *   strictMode: Boolean
 * }
 * ```
 */
const config = { // eslint-disable-line no-unused-vars
  /**
   * @alias Configuration.strictMode
   * @type {boolean}
   * @description
   * Define if EventBus throw Error when emitting event which has no listener defined.
   */
  strictMode: true
}
