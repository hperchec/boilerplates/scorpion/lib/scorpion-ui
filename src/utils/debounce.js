/**
 * @vuepress
 * ---
 * title: debounce
 * headline: debounce
 * sidebarTitle: debounce
 * ---
 */

// Lodash.debounce
import lodashDebounce from 'lodash.debounce'

/**
 * debounce
 * @param {Function} func - The callback function
 * @param {number} [wait=0] - Time delay
 * @param {object} [options={}] - Options
 * @returns {void}
 * @description
 * Debounce callback (lodash.debounce).
 *
 * ::: tip See also
 * [lodash.debounce](https://www.npmjs.com/package/lodash.debounce) package documentation
 * :::
 * @example
 * debounce(myFunc, 150) // call 'myfunc()' after 150 ms
 */
export const debounce = function (func, wait = 0, options = {}) {
  return lodashDebounce(func, wait, options)
}

export default debounce
