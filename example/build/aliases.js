const path = require('path')
// Project root path
const rootPath = path.resolve(__dirname, '../')
// Resolve from root path
const resolve = (...args) => { // eslint-disable-line no-unused-vars
  return path.join(rootPath, ...args)
}

/**
 * Aliases for webpack
 */
module.exports = {
  // Uncomment the following alias to test the other versions of scorpion-ui
  // '@hperchec/scorpion-ui$': '@hperchec/scorpion-ui/dist/scorpion-ui.min.js'
  // '@hperchec/scorpion-ui$': '@hperchec/scorpion-ui/dist/scorpion-ui.common.cjs'
}
