import getModule from './get-module'

/**
 * Get a store module context by namespace
 * @param {import("vuex").Store} store - The store instance
 * @param {string} namespace - The full module namespace
 * @returns {object} The module context
 */
export const getModuleContext = (store, namespace) => {
  return getModule(store, namespace).context
}

export default getModuleContext
