/**
 * @vuepress
 * ---
 * title: "Service (local-database-manager) options"
 * headline: "Service (local-database-manager) options"
 * sidebarTitle: Options
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import Core from '@/Core'
import { isUndef } from '@/utils'
import LocalDatabase from './LocalDatabase' // jsDoc type

/**
 * Cache for dynamic properties
 * @ignore
 */
const _options = {
  databases: undefined
}

/**
 * @name options
 * @static
 * @description
 * Accepts the following options:
 *
 * - `databases`
 *
 * By default, each object in `Core.context.services['local-database-manager'].databases` will be passed to `LocalDatabaseOptions` constructor.
 * The result of `databases` option will be an array of the defined routes. You can overwrite it by setting this option.
 */
const options = {
  /**
   * databases
   * @alias options.databases
   * @type {LocalDatabase[]}
   * @description
   * Database definitions:
   *
   * ```js
   * databases: {
   *   // <name> is the database name and the value is a local database options object
   *   // (see LocalDatabaseOptions constructor second param)
   *   '<name>': Object,
   *   ...
   * }
   * ```
   */
  get databases () {
    // If databases option is not provided
    if (isUndef(_options.databases)) {
      const tmp = []
      const databaseDefs = Core.context.services['local-database-manager'].databases
      for (const name in databaseDefs) {
        // Create a new LocalDatabase
        tmp.push(Core.context.services['local-database-manager'].newLocalDatabase(name, databaseDefs[name]))
      }
      return tmp
    } else {
      return _options.databases
    }
  },
  set databases (value) {
    _options.databases = value
  }
}

export default options
