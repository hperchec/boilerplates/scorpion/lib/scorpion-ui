declare namespace _default {
    let name: string;
    namespace components {
        function Modal(resolve: any): any;
    }
    let props: {};
    function data(): {
        modalInstance: any;
    };
    function created(): void;
    namespace methods {
        function hide(): void;
    }
}
export default _default;
//# sourceMappingURL=BaseModal.d.ts.map