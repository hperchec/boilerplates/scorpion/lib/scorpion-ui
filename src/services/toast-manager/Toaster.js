/**
 * @vuepress
 * ---
 * title: "Service (toast-manager): Toaster class"
 * headline: "Service (toast-manager): Toaster class"
 * sidebarTitle: .Toaster
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import Core from '@/Core'
import { getAllKeys, isDef, typeCheck } from '@/utils'
import Toast from './Toast' // jsDoc type
import Collection from '../../context/support/collection/Collection' // jsDoc type

/**
 * @classdesc
 * Service **toast-manager**: Toaster class
 */
class Toaster {
  /**
   * @private
   */
  _name
  _toasts
  _beforePublish
  _afterPublish
  _beforeDelete
  _afterDelete

  /**
   * Create a new Toaster
   * @param {object} options - A toaster option object
   * @param {string} options.name - Name of Toaster
   * @param {Collection} [options.toasts] - A Collection of Toast object
   * @param {Function} [options.beforePublish] - A function to call before publish toast
   * @param {Function} [options.afterPublish] - A function to call after publish toast
   * @param {Function} [options.beforeDelete] - A function to call before delete toast
   * @param {Function} [options.afterDelete] - A function to call after delete toast
   */
  constructor (options) {
    // First merge options
    const opts = Object.assign(Toaster.defaultOptions, options)
    // Assign properties
    this.name = opts.name
    if (isDef(opts.toasts)) {
      this.setToasts(opts.toasts)
    }
    this.setBeforePublish(opts.beforePublish)
    this.setAfterPublish(opts.afterPublish)
    this.setBeforeDelete(opts.beforeDelete)
    this.setAfterDelete(opts.afterDelete)
  }

  /**
   * Accessors & mutators
   */

  /**
   * Toaster name
   * @category properties
   * @type {string}
   */
  get name () {
    return this._name
  }

  set name (value) {
    // Check type
    if (typeCheck(String, value)) {
      // Assign
      this._name = value
    } else {
      // Else -> throw error
      throw new Error('Toaster class: \'name\' property must be String. ' + typeof value + ' received...')
    }
  }

  /**
   * Toaster toasts
   * @category properties
   * @readonly
   * @type {Collection}
   */
  get toasts () {
    // If toasts not defined throw error
    if (!isDef(this._toasts)) {
      throw new Error('Toaster class: \'toasts\' property not defined, use "setToasts" method.')
    }
    return this._toasts
  }

  /**
   * Methods
   */

  /**
   * setToasts
   * @category methods
   * @param {Collection} value - The toast collection
   * @returns {void}
   * @description
   * Set toasts
   */
  setToasts (value) {
    // Check type
    if (typeCheck(Core.context.support.collection.Collection, value)) {
      // Assign
      this._toasts = value
    } else {
      // Else -> throw error
      throw new Error('Toaster class: \'toasts\' property must be Collection. ' + typeof value + ' received...')
    }
  }

  /**
   * setBeforePublish
   * @category methods
   * @param {Function} value - The function to call before publish
   * @returns {void}
   * @description
   * Set beforePublish
   */
  setBeforePublish (value) {
    if (isDef(value)) {
      // Check type
      if (typeCheck(Function, value)) {
        // Assign
        this._beforePublish = value
      } else {
        // Else -> throw error
        throw new Error('Toaster class: \'setBeforePublish\' method parameter 1 expects function. ' + typeof value + ' received...')
      }
    }
  }

  /**
   * setAfterPublish
   * @category methods
   * @param {Function} value - The function to call after publish
   * @returns {void}
   * @description
   * Set afterPublish
   */
  setAfterPublish (value) {
    if (isDef(value)) {
      // Check type
      if (typeCheck(Function, value)) {
        // Assign
        this._afterPublish = value
      } else {
        // Else -> throw error
        throw new Error('Toaster class: \'setAfterPublish\' method parameter 1 expects function. ' + typeof value + ' received...')
      }
    }
  }

  /**
   * setBeforeDelete
   * @category methods
   * @param {Function} value - The function to call before publish
   * @returns {void}
   * @description
   * Set beforeDelete
   */
  setBeforeDelete (value) {
    if (isDef(value)) {
      // Check type
      if (typeCheck(Function, value)) {
        // Assign
        this._beforeDelete = value
      } else {
        // Else -> throw error
        throw new Error('Toaster class: \'setBeforeDelete\' method parameter 1 expects function. ' + typeof value + ' received...')
      }
    }
  }

  /**
   * setAfterDelete
   * @category methods
   * @param {Function} value - The function to call after publish
   * @returns {void}
   * @description
   * Set afterDelete
   */
  setAfterDelete (value) {
    if (isDef(value)) {
      // Check type
      if (typeCheck(Function, value)) {
        // Assign
        this._afterDelete = value
      } else {
        // Else -> throw error
        throw new Error('Toaster class: \'setAfterDelete\' method parameter 1 expects function. ' + typeof value + ' received...')
      }
    }
  }

  /**
   * beforePublish
   * @category async methods
   * @param {...*} args - The beforePublish function arguments
   * @returns {Promise<void>}
   * @description
   * beforePublish method
   *
   * - if customized hook defined, call it with same arguments
   * - else, call static class method beforePublish with same arguments
   *
   * See Toaster `beforePublish` static method.
   */
  async beforePublish (...args) {
    const exec = isDef(this._beforePublish)
      ? this._beforePublish // Customized hook
      : Toaster.beforePublish // Else default class static method
    await exec(...args)
  }

  /**
   * afterPublish
   * @category async methods
   * @param {...*} args - The afterPublish function arguments
   * @returns {Promise<void>}
   * @description
   * afterPublish method
   *
   * - if customized hook defined, call it with same arguments
   * - else, call static class method afterPublish with same arguments
   *
   * See Toaster `afterPublish` static method.
   */
  async afterPublish (...args) {
    const exec = isDef(this._afterPublish)
      ? this._afterPublish // Customized hook
      : Toaster.afterPublish // Else default class static method
    await exec(...args)
  }

  /**
   * beforeDelete
   * @category async methods
   * @param {...*} args - The beforeDelete function arguments
   * @returns {Promise<void>}
   * @description
   * beforeDelete method
   *
   * - if customized hook defined, call it with same arguments
   * - else, call static class method beforeDelete with same arguments
   *
   * See Toaster `beforeDelete` static method.
   */
  async beforeDelete (...args) {
    const exec = isDef(this._beforeDelete)
      ? this._beforeDelete // Customized hook
      : Toaster.beforeDelete // Else default class static method
    await exec(...args)
  }

  /**
   * afterDelete
   * @category async methods
   * @param {...*} args - The afterDelete function arguments
   * @returns {Promise<void>}
   * @description
   * afterDelete method
   *
   * - if customized hook defined, call it with same arguments
   * - else, call static class method afterDelete with same arguments
   *
   * See Toaster `afterDelete` static method.
   */
  async afterDelete (...args) {
    const exec = isDef(this._afterDelete)
      ? this._afterDelete // Customized hook
      : Toaster.afterDelete // Else default class static method
    await exec(...args)
  }

  /**
   * delete
   * @category async methods
   * @param {string} id - The toast id
   * @returns {Promise<boolean>} Returns true after deleting
   * @description
   * Delete method
   */
  async delete (id) {
    const toast = this.toasts.find(id)
    if (isDef(toast)) {
      // Before delete HOOK
      await this.beforeDelete(toast, this)
      // Delete toast from collection
      await this.toasts.delete(toast.id)
      // After delete HOOK
      await this.afterDelete(toast, this)
    }
    return true
  }

  /**
   * publish
   * @category methods
   * @param {object} toastOptions - The options of the toast that will be created via new Toast()
   * @returns {Promise<Toast>} Returns the published Toast
   * @description
   * Publish a toast
   */
  async publish (toastOptions) {
    const toast = new Core.context.services['toast-manager'].Toast({
      toaster: this,
      ...toastOptions
    })
    // Before publish HOOK
    await this.beforePublish(toast, this, toastOptions)
    // Add toast to collection
    await this.toasts.add(toast)
    // Init toast
    this.toasts.find(toast.id).init()
    // After publish HOOK
    await this.afterPublish(toast, this, toastOptions)
    // Return toast
    return toast
  }

  /**
   * Overrides the default toJSON object method for JSON.stringify() calls
   * @returns {object} - The instance data to be serialized
   */
  toJSON () {
    const keys = getAllKeys(this, {
      excludeKeys: [
        /^_/, // private properties that starts with '_'
        'toJSON',
        'toString',
        'constructor'
      ]
    })
    const jsonObj = keys.reduce((obj, key) => {
      obj[key] = this[key]
      return obj
    }, {})
    return jsonObj
  }

  /**
   * Static
   */

  /**
   * beforePublish
   * @category async methods
   * @param {Toast} toast - The toast
   * @param {Toaster} toaster - The toaster
   * @param {object} toastOptions - The original toast options
   * @returns {Promise<void>}
   * @description
   * Default beforePublish function (do nothing by default)
   */
  static async beforePublish (toast, toaster, toastOptions) {
    // Do nothing by default
  }

  /**
   * afterPublish
   * @category async methods
   * @param {Toast} toast - The toast
   * @param {Toaster} toaster - The toaster
   * @param {object} toastOptions - The original toast options
   * @returns {Promise<void>}
   * @description
   * Default afterPublish function (do nothing by default)
   */
  static async afterPublish (toast, toaster, toastOptions) {
    // Do nothing by default
  }

  /**
   * beforeDelete
   * @category async methods
   * @param {Toast} toast - The toast
   * @param {Toaster} toaster - The toaster
   * @returns {Promise<void>}
   * @description
   * Default afterPublish function (do nothing by default)
   */
  static async beforeDelete (toast, toaster) {
    // Do nothing by default
  }

  /**
   * afterDelete
   * @category async methods
   * @param {Toast} toast - The toast
   * @param {Toaster} toaster - The toaster
   * @returns {Promise<void>}
   * @description
   * Default afterPublish function (do nothing by default)
   */
  static async afterDelete (toast, toaster) {
    // Do nothing by default
  }

  /**
   * Default options
   * @category properties
   * @readonly
   * @type {object}
   * @property {string} name - The toaster name (default: `undefined`)
   * @property {Collection} toasts - A collection of toast objects (default: `undefined`)
   * @property {Function} beforePublish - The beforePublish hook function (default: `Toaster.beforePublish`)
   * @property {Function} afterPublish - The afterPublish hook function (default: `Toaster.afterPublish`)
   * @property {Function} beforeDelete - The beforeDelete hook function (default: `Toaster.beforeDelete`)
   * @property {Function} afterDelete - The afterDelete hook function (default: `Toaster.afterDelete`)
   */
  static get defaultOptions () {
    return {
      name: undefined,
      toasts: undefined,
      beforePublish: Toaster.beforePublish,
      afterPublish: Toaster.afterPublish,
      beforeDelete: Toaster.beforeDelete,
      afterDelete: Toaster.afterDelete
    }
  }
}

export default Toaster
