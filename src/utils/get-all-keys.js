/**
 * @vuepress
 * ---
 * title: getAllKeys
 * headline: getAllKeys
 * sidebarTitle: getAllKeys
 * ---
 */

/**
 * getAllKeys
 * @param {*} obj - The target object
 * @param {object} [options] - The options object
 * @param {Array<string|RegExp>} [options.excludeKeys] - The keys to exclude by default
 * @param {boolean} [options.includeSelf = true] - Include own keys
 * @param {boolean} [options.includePrototypeChain = true] - Include inherited keys
 * @param {boolean} [options.includeTop = true] - Include top level keys
 * @param {boolean} [options.includeEnumerables = true] - Include enumerable keys
 * @param {boolean} [options.includeNonenumerables = true] - Include non-enumerable keys
 * @param {boolean} [options.includeStrings = true] - Include string keys
 * @param {boolean} [options.includeSymbols = true] - Include symbol keys
 * @returns {string[]} - An array of keys
 * @description
 * Get all keys from an object (see: https://stackoverflow.com/a/70629468)
 * @example
 * getAllKeys({ foo: 'bar' }) // => [ 'foo' ]
 */
export const getAllKeys = function (obj, options = {}) {
  // Process options
  options = {
    excludeKeys: [],
    includeSelf: true,
    includePrototypeChain: true,
    includeTop: false,
    includeEnumerables: true,
    includeNonenumerables: true,
    includeStrings: true,
    includeSymbols: true,
    ...options
  }
  // Boolean (mini-)functions to determine any given key's eligibility:
  const isExcludedKey = (key) => {
    return Boolean(options.excludeKeys.some((val) => {
      return (val instanceof RegExp && typeof key === 'string')
        ? val.test(key)
        : val === key
    }))
  }
  const isEnumerable = (obj, key) => Object.propertyIsEnumerable.call(obj, key)
  const isString = (key) => typeof key === 'string'
  const isSymbol = (key) => typeof key === 'symbol'
  const includeBasedOnEnumerability = (obj, key) => (options.includeEnumerables && isEnumerable(obj, key)) || (options.includeNonenumerables && !isEnumerable(obj, key))
  const includeBasedOnKeyType = (key) => (options.includeStrings && isString(key)) || (options.includeSymbols && isSymbol(key))
  const include = (obj, key) => !isExcludedKey(key) && includeBasedOnEnumerability(obj, key) && includeBasedOnKeyType(key)
  const notYetRetrieved = (keys, key) => !keys.includes(key)

  // filter function putting all the above together:
  const filterFn = key => notYetRetrieved(keys, key) && include(obj, key)

  // conditional chooses one of two functions to determine whether to exclude the top level or not:
  const stopFn = options.includeTop ? obj => obj === null : obj => Object.getPrototypeOf(obj) === null

  // and now the loop to collect and filter everything:
  let keys = []
  while (!stopFn(obj)) {
    if (options.includeSelf) {
      const ownKeys = Reflect.ownKeys(obj).filter(filterFn)
      keys = keys.concat(ownKeys)
    }
    if (!options.includePrototypeChain) {
      break
    } else {
      options.includeSelf = true
      obj = Object.getPrototypeOf(obj)
    }
  }
  return keys
}

export default getAllKeys
