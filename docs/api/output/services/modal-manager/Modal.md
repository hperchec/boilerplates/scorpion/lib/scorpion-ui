---
title: "Service (modal-manager): Modal class"
headline: "Service (modal-manager): Modal class"
sidebarTitle: .Modal
sidebarDepth: 0
prev: false
next: false
---

# Service (modal-manager): Modal class

## Classes

<dl>
<dt><a href="#Modal">Modal</a></dt>
<dd><p>Service <strong>modal-manager</strong>: Modal class</p>
</dd>
</dl>

## Members

<dl>
<dt><a href="#beforeOpen">beforeOpen</a> ⇒ <code>void</code></dt>
<dd><p>Called before modal opens</p>
</dd>
<dt><a href="#beforeClose">beforeClose</a> ⇒ <code>void</code></dt>
<dd><p>Called before modal closes</p>
</dd>
<dt><a href="#opened">opened</a> ⇒ <code>void</code></dt>
<dd><p>Called when modal is opened</p>
</dd>
<dt><a href="#closed">closed</a></dt>
<dd><p>Called when modal is closed</p>
</dd>
<dt><a href="#layer">layer</a></dt>
<dd><p>The layer as number. Can be string: &quot;foreground&quot;</p>
</dd>
<dt><a href="#onConfirm">onConfirm</a></dt>
<dd><p>Called when &quot;confirm&quot; button is clicked</p>
</dd>
<dt><a href="#onCancel">onCancel</a></dt>
<dd><p>Called when &quot;cancel&quot; button is clicked</p>
</dd>
</dl>

<a name="modal">
</a>

## Modal

Service **modal-manager**: Modal class

**Schema**:

- [Modal](#modal)
  - [new Modal(name, component, [props])](#newmodalnew)
  - _instance_
    - [._manager](#modal-manager)
    - [.toJSON()](#modal-tojson) ⇒ <code>object</code>
    - _methods_
      - [.show([props])](#modal-show) ⇒ <code>void</code>
      - [.hide()](#modal-hide) ⇒ <code>void</code>
    - _properties_
      - [.name](#modal-name) : <code>string</code>
      - [.component](#modal-component) : <code>object</code> \| <code>null</code>
      - [.props](#modal-props) : <code>object</code>
      - [.isShown](#modal-isshown) : <code>boolean</code>
      - [.isHidden](#modal-ishidden) : <code>boolean</code>
      - [.vm](#modal-vm) : <code>object</code> \| <code>null</code>
  - _static_
    - _methods_
      - [.props](#modal-props) : <code>object</code>
      - [.getDefaultProps()](#modal-getdefaultprops) ⇒ <code>object</code>

<a name="newmodalnew">
</a>

### new Modal(name, component, [props])

Create a new Modal

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| name | <code>string</code> |  | The modal unique name |
| component | <code>module:vue~Component</code> |  | The modal component |
| [props] | <code>object</code> | <code>{}</code> | The props to pass to componeny |

<a name="modal-manager">
</a>

### modal.\_manager

Private

<a name="modal-tojson">
</a>

### modal.toJSON() ⇒ <code>object</code>

Overrides the default toJSON object method for JSON.stringify() calls

**Returns**: <code>object</code> - - The instance data to be serialized

<a name="modal-show">
</a>

### modal.show([props]) ⇒ <code>void</code>

Show this modal

**Category**: methods  
| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [props] | <code>object</code> | <code>{}</code> | The props to pass to the modal |

<a name="modal-hide">
</a>

### modal.hide() ⇒ <code>void</code>

Hide this modal

**Category**: methods  
<a name="modal-name">
</a>

### modal.name : <code>string</code>

The modal name

**Category**: properties  
<a name="modal-component">
</a>

### modal.component : <code>object</code> \| <code>null</code>

The modal component

**Category**: properties  
<a name="modal-props">
</a>

### modal.props : <code>object</code>

The modal props

**Category**: properties  
**Read only**: true  
<a name="modal-isshown">
</a>

### modal.isShown : <code>boolean</code>

Returns true if modal is shown

**Category**: properties  
**Read only**: true  
<a name="modal-ishidden">
</a>

### modal.isHidden : <code>boolean</code>

Returns true if modal is hidden

**Category**: properties  
**Read only**: true  
<a name="modal-vm">
</a>

### modal.vm : <code>object</code> \| <code>null</code>

The vue model

**Category**: properties  
**Read only**: true  
<a name="modal-props">
</a>

### Modal.props : <code>object</code>

**Category**: methods  
<a name="modal-getdefaultprops">
</a>

### Modal.getDefaultProps() ⇒ <code>object</code>

**Returns**: <code>object</code> - Returns a default props object

**Category**: methods  
<a name="beforeopen">
</a>

## beforeOpen ⇒ <code>void</code>

Called before modal opens

<a name="beforeclose">
</a>

## beforeClose ⇒ <code>void</code>

Called before modal closes

<a name="opened">
</a>

## opened ⇒ <code>void</code>

Called when modal is opened

<a name="closed">
</a>

## closed

Called when modal is closed

<a name="layer">
</a>

## layer

The layer as number. Can be string: "foreground"

**Default**: <code>0</code>  
<a name="onconfirm">
</a>

## onConfirm

Called when "confirm" button is clicked

<a name="oncancel">
</a>

## onCancel

Called when "cancel" button is clicked

