---
title: findKey
headline: findKey
sidebarTitle: findKey
---

# findKey

<a name="findkey">
</a>

## findKey ⇒ <code>\*</code>

Find key of object that matches conditions::: tip See also[lodash.findkey](https://www.npmjs.com/package/lodash.findkey) package documentation:::

**Returns**: <code>\*</code> - - Returns the key of the matched element, else undefined

| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | Same args as lodash findKey method |

