// Default normalize-component method
import normalizeComponent from 'vue-runtime-helpers/dist/normalize-component.js'
// scorpion-ui component normalizer
import defaultCustomNormalizer from '../../../../../lib/runtime/component-normalizer.js'

// Overwrite normalizer to easily identify object as Vue component options
// => Core will retrieve common components in nested object
export default function (...args) {
  const result = normalizeComponent(...args)
  return defaultCustomNormalizer(result)
}
