/**
 * @vuepress
 * ---
 * title: "Service (api-manager) configuration"
 * headline: "Service (api-manager) configuration"
 * sidebarTitle: Configuration
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * ⚠ THIS FILE IS ONLY USED TO GENERATE SERVICE CONFIGURATION DOCUMENTATION
 */

/**
 * @name Configuration
 * @typicalname Core.config.services['api-manager']
 * @description
 * Service configuration
 *
 * > Refers to `Core.config.services['api-manager']`
 *
 * ```js
 * 'api-manager': {
 *   // API definitions
 *   // Overwrites service option: `apis`
 *   apis: {
 *     // <name> is the api name and the value is an api options object
 *     // (see APIOptions constructor third param)
 *     '<name>': Object,
 *     ...
 *   }
 * }
 * ```
 */
export default {
  /**
   * @alias Configuration.apis
   * @type {object}
   * @description
   * API definitions. An object following the schema:
   *
   * ```js
   * {
   *   [key: String]: Object
   * }
   * ```
   *
   * Object keys are the API names while values are Object like APIoptions constructor third param.
   */
  apis: {}
}
