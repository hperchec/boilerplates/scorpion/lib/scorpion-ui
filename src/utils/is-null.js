/**
 * @vuepress
 * ---
 * title: isNull
 * headline: isNull
 * sidebarTitle: isNull
 * ---
 */

/**
 * isNull
 * @param {*} value - The variable to check
 * @returns {boolean} Returns true if value is null
 * @description
 * Check if value is null
 * @example
 * isNull(value) // => true is value is null
 */
export const isNull = function (value) {
  return value === null
}

export default isNull
