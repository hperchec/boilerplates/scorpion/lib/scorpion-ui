/**
 * @vuepress
 * ---
 * title: isRegExp
 * headline: isRegExp
 * sidebarTitle: isRegExp
 * ---
 */

/**
 * isRegExp
 * @param {*} value - The value to check
 * @returns {boolean} Returns true if value is RegExp
 * @description
 * Check if value is a RegExp
 * @example
 * isRegExp(/^Hello$/) // => true
 * isRegExp('Hello') // => false
 */
export const isRegExp = function (value) {
  return Object.prototype.toString.call(value) === '[object RegExp]'
}

export default isRegExp
