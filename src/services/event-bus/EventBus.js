/**
 * @vuepress
 * ---
 * title: "Service (event-bus): EventBus class"
 * headline: "Service (event-bus): EventBus class"
 * sidebarTitle: .EventBus
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import Core from '@/Core'
import toolbox from '@/toolbox'
import { getAllKeys, isDef } from '@/utils'

const { VuePluginable } = toolbox.vue

// @ts-ignore
const log = (...args) => Core.service('logger').consoleLog(...args) // eslint-disable-line dot-notation

/**
 * @extends VuePluginable
 * @classdesc
 * Service **event-bus**: EventBus class
 */
class EventBus extends VuePluginable {
  /**
   * Create a new instance
   * @param {object} [options = {}] - The constructor options
   * @param {boolean} [options.strictMode] - Strict mode option
   */
  constructor (options = {}) {
    // Call VuePluginable constructor
    const initVMData = {}
    super(initVMData)
    // Set debugging private property
    if (this.__v_descriptor__) {
      this.__v_descriptor__.namespace = 'service:event-bus'
      this.__v_descriptor__.description = 'Access the EventBus instance'
      // @ts-ignore
      this.__v_descriptor__.externalLink = __getDocUrl__('/api/services/event-bus/EventBus')
    }
    // Process options
    if (isDef(options.strictMode)) this.#strictMode = Boolean(options.strictMode)
  }

  /**
   * Private properties
   */

  /**
   * strictMode
   * @category private properties
   * @type {boolean}
   * @description
   * Strict mode
   */
  #strictMode = true

  /**
   * Overrides static VuePluginable properties
   */
  static reactive = true
  static rootOption = 'eventBus'
  static aliases = []

  /**
   * Accessors & mutators
   */

  /**
   * listeners
   * @category properties
   * @type {object}
   * @description
   * Returns defined listeners.
   */
  get listeners () {
    // return this._vm.$listeners does not work
    // See also [Vuejs documentation](https://fr.vuejs.org/v2/api/#vm-listeners) for $listeners method
    // @todo: this references a private Vue property...
    return this._vm._events
  }

  /**
   * Methods
   */

  /**
   * emit
   * @category methods
   * @param {...*} args - same as Vue $emit arguments
   * @returns {void}
   * @description
   * emit method: see [Vuejs documentation](https://fr.vuejs.org/v2/api/#vm-emit) for $emit event method
   */
  emit (...args) {
    const event = args[0]
    const strictModeErrorMessage = 'EventBus: no listener defined for the event "' + String(event) + '"'
    if (!this.listeners[event]) {
      // If strict mode
      if (this.#strictMode) {
        log(strictModeErrorMessage, { type: 'error' })
        throw new Error(strictModeErrorMessage)
      } else {
        log(strictModeErrorMessage, { type: 'warning', prod: false })
      }
    }
    this._vm.$emit(...args)
  }

  /**
   * off
   * @category methods
   * @param {...*} args - same as Vue $off arguments
   * @returns {void}
   * @description
   * off method: see [Vuejs documentation](https://fr.vuejs.org/v2/api/#vm-off) for $off event method
   */
  off (...args) {
    this._vm.$off(...args)
  }

  /**
   * on
   * @category methods
   * @param {...*} args - same as Vue $on arguments
   * @returns {void}
   * @description
   * on method: see [Vuejs documentation](https://fr.vuejs.org/v2/api/#vm-on) for $on event method
   */
  on (...args) {
    this._vm.$on(...args)
  }

  /**
   * once
   * @category methods
   * @param {...*} args - same as Vue $once arguments
   * @returns {void}
   * @description
   * once method: see [Vuejs documentation](https://fr.vuejs.org/v2/api/#vm-once) for $once event method
   */
  once (...args) {
    this._vm.$once(...args)
  }

  /**
   * Overrides the default toJSON object method for JSON.stringify() calls
   * @returns {object} - The instance data to be serialized
   */
  toJSON () {
    const keys = getAllKeys(this, {
      excludeKeys: [
        /^_/, // private properties that starts with '_'
        'toJSON',
        'toString',
        'constructor'
      ]
    })
    const jsonObj = keys.reduce((obj, key) => {
      obj[key] = this[key]
      return obj
    }, {})
    return jsonObj
  }

  /**
   * Static
   */

  /**
   * Install method for Vue
   * @param {Vue} _Vue - Vue
   * @param {object} [options = {}] - The options of the plugin
   */
  static install (_Vue, options = {}) {
    super.install(_Vue)
  }
}

export default EventBus
