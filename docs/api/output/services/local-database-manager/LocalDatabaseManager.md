---
title: "Service (local-database-manager): LocalDatabaseManager class"
headline: "Service (local-database-manager): LocalDatabaseManager class"
sidebarTitle: .LocalDatabaseManager
sidebarDepth: 0
prev: false
next: false
---

# Service (local-database-manager): LocalDatabaseManager class

<a name="localdatabasemanager">
</a>

## LocalDatabaseManager ⇐ <code>VuePluginable</code>

Service **local-database-manager**: LocalDatabaseManager class

**Extends**: <code>VuePluginable</code>  
**Schema**:

- [LocalDatabaseManager](#localdatabasemanager) ⇐ <code>VuePluginable</code>
  - [new LocalDatabaseManager(options)](#newlocaldatabasemanagernew)
  - _instance_
    - [.reactive](#localdatabasemanager-reactive)
    - [.toJSON()](#localdatabasemanager-tojson) ⇒ <code>object</code>
    - _methods_
      - [.add(database)](#localdatabasemanager-add) ⇒ <code>module:services/local-database-manager/LocalDatabase~LocalDatabase</code>
      - [.use(databaseName)](#localdatabasemanager-use) ⇒ <code>module:services/local-database-manager/LocalDatabase~LocalDatabase</code>
    - _properties_
      - [.databases](#localdatabasemanager-databases) : <code>Array.&lt;module:services/local-database-manager/LocalDatabase~LocalDatabase&gt;</code>
  - _static_
    - [.install(_Vue, [options])](#localdatabasemanager-install)

<a name="newlocaldatabasemanagernew">
</a>

### new LocalDatabaseManager(options)

Create a new local database manager

| Param | Type | Description |
| --- | --- | --- |
| options | <code>object</code> | Constructor options |
| options.databases | <code>Array.&lt;module:services/local-database-manager/LocalDatabase~LocalDatabase&gt;</code> | An array of LocalDatabase |

<a name="localdatabasemanager-reactive">
</a>

### localDatabaseManager.reactive

Overrides static VuePluginable properties

<a name="localdatabasemanager-tojson">
</a>

### localDatabaseManager.toJSON() ⇒ <code>object</code>

Overrides the default toJSON object method for JSON.stringify() calls

**Returns**: <code>object</code> - - The instance data to be serialized

<a name="localdatabasemanager-add">
</a>

### localDatabaseManager.add(database) ⇒ <code>module:services/local-database-manager/LocalDatabase~LocalDatabase</code>

Add a database to manager anytime

**Returns**: <code>module:services/local-database-manager/LocalDatabase~LocalDatabase</code> - Return this.use(<database.name>)

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| database | <code>module:services/local-database-manager/LocalDatabase~LocalDatabase</code> | The database to add to manager |

<a name="localdatabasemanager-use">
</a>

### localDatabaseManager.use(databaseName) ⇒ <code>module:services/local-database-manager/LocalDatabase~LocalDatabase</code>

Use a specific database that was loaded by the manager

**Returns**: <code>module:services/local-database-manager/LocalDatabase~LocalDatabase</code> - Return the database

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| databaseName | <code>string</code> | The database name to target |

<a name="localdatabasemanager-databases">
</a>

### localDatabaseManager.databases : <code>Array.&lt;module:services/local-database-manager/LocalDatabase~LocalDatabase&gt;</code>

Registered databases

**Category**: properties  
**Read only**: true  
<a name="localdatabasemanager-install">
</a>

### LocalDatabaseManager.install(_Vue, [options])

Install method for Vue

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| _Vue | <code>Vue</code> |  | Vue |
| [options] | <code>object</code> | <code>{}</code> | The options of the plugin |

