---
title: maxBy
headline: maxBy
sidebarTitle: maxBy
---

# maxBy

<a name="maxby">
</a>

## maxBy ⇒ <code>string</code>

Find max by in array (lodash.maxby).::: tip See also[lodash.maxby](https://www.npmjs.com/package/lodash.maxby) package documentation:::

**Returns**: <code>string</code> - Returns the found value in `array`

| Param | Type | Description |
| --- | --- | --- |
| array | <code>Array</code> | The array |
| sortFct | <code>function</code> | The sort function |

**Example**

```js
const array = [ { id: 1, name: 'John' }, { id: 2, name: 'Jane' } ]maxBy(array, function (item) { return item.id }) // Return -> { id: 2, name: 'Jane' }maxBy(array, function (item) { return item.name }) // Return -> { id: 1, name: 'John' }
```

