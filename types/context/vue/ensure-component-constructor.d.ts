export function ensureComponentConstructor(target: import("vue").VNode | import("vue").Component | object): import("vue").Component | null;
export default ensureComponentConstructor;
//# sourceMappingURL=ensure-component-constructor.d.ts.map