---
title: uppercase
headline: uppercase
sidebarTitle: uppercase
---

# uppercase

<a name="uppercase">
</a>

## uppercase ⇒ <code>string</code>

Convert a string to uppercase *([Defined as Vue filter](../context/vue/filters#filters-uppercase))*.::: tip See also[lodash.uppercase](https://www.npmjs.com/package/lodash.uppercase) package documentation:::

**Returns**: <code>string</code> - The "uppercased" string

| Param | Type | Description |
| --- | --- | --- |
| str | <code>string</code> | The string to uppercase |

**Example**

```js
uppercase('Foo Bar') // => 'FOO BAR'
```

