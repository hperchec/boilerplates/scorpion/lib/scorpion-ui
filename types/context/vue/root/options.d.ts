declare namespace _default {
    export let _isAppRootInstance: boolean;
    export let mixins: object[];
    export let components: object;
    export namespace data { }
    export let methods: object;
    export function render(createElement: any): any;
    export namespace beforeCreate { }
    export { createdHook as created };
    export namespace mounted { }
}
export default _default;
declare function createdHook(): void;
declare namespace createdHook {
    let priority: number;
}
//# sourceMappingURL=options.d.ts.map