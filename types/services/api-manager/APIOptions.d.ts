export default APIOptions;
declare class APIOptions {
    constructor(name: string, driver: string, options: object);
    private _name;
    _driver: any;
    _options: any;
    set name(value: string);
    get name(): string;
    set driver(value: string);
    get driver(): string;
    set options(value: any);
    get options(): any;
    toJSON(): object;
}
//# sourceMappingURL=APIOptions.d.ts.map