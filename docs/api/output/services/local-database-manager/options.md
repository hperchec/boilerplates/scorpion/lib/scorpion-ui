---
title: Service (local-database-manager) options
headline: Service (local-database-manager) options
sidebarTitle: Options
sidebarDepth: 0
prev: false
next: false
---

# Service (local-database-manager) options

<a name="options">
</a>

## .options

Accepts the following options:- `databases`By default, each object in `Core.context.services['local-database-manager'].databases` will be passed to `LocalDatabaseOptions` constructor.The result of `databases` option will be an array of the defined routes. You can overwrite it by setting this option.

<a name="options-databases">
</a>

### options.databases : <code>Array.&lt;module:services/local-database-manager/LocalDatabase~LocalDatabase&gt;</code>

Database definitions:```jsdatabases: {  // <name> is the database name and the value is a local database options object  // (see LocalDatabaseOptions constructor second param)  '<name>': Object,  ...}```

