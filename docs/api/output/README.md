---
title: ScorpionUI API
headline: ScorpionUI API
prev: false
next: ./config/
---

# ScorpionUI API

<a name="module-scorpionui">
</a>

## ScorpionUI

Scorpion UI module```jsimport ScorpionUI from '@hperchec/scorpion-ui'```

**Author**: Hervé Perchec <contact@herve-perchec.com>  
**Schema**:

- [ScorpionUI](#module-scorpionui)
  - [ScorpionUI](#expmodulescorpionui-scorpionui) : <code>object</code> ⏏
    - [.Core](#module-scorpionui-scorpionui-core) : <code>function</code>
    - [.builtInServices](#module-scorpionui-scorpionui-builtinservices) : <code>object</code>
    - [.Service](#module-scorpionui-scorpionui-service) : <code>function</code>
    - [.ServiceDefinition](#module-scorpionui-scorpionui-servicedefinition) : <code>function</code>
    - [.utils](#module-scorpionui-scorpionui-utils) : <code>object</code>
    - [.toolbox](#module-scorpionui-scorpionui-toolbox) : <code>object</code>

<a name="expmodulescorpionui-scorpionui" data-sidebar-title=".exports (default)"></a>

### ScorpionUI : <code>object</code> ⏏

scorpion-ui module

<a name="module-scorpionui-scorpionui-core">
</a>

#### ScorpionUI.Core : <code>function</code>

The Core class

**See**: [./Core](./Core)

<a name="module-scorpionui-scorpionui-builtinservices">
</a>

#### ScorpionUI.builtInServices : <code>object</code>

The built-in services

**See**: [./services](./services)

<a name="module-scorpionui-scorpionui-service">
</a>

#### ScorpionUI.Service : <code>function</code>

The Service class

**See**: [./Service](./Service)

<a name="module-scorpionui-scorpionui-servicedefinition">
</a>

#### ScorpionUI.ServiceDefinition : <code>function</code>

The ServiceDefinition class

**See**: [./ServiceDefinition](./ServiceDefinition)

<a name="module-scorpionui-scorpionui-utils">
</a>

#### ScorpionUI.utils : <code>object</code>

The default Core utils

**See**: [./utils](./utils)

<a name="module-scorpionui-scorpionui-toolbox">
</a>

#### ScorpionUI.toolbox : <code>object</code>

The lib toolbox

**See**: [./toolbox](./toolbox)

