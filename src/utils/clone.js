/**
 * @vuepress
 * ---
 * title: clone
 * headline: clone
 * sidebarTitle: clone
 * ---
 */

// Lodash.clone
import lodashClone from 'lodash.clone'

/**
 * clone
 * @param {*} value - The value to clone
 * @returns {*} Returns the cloned object
 * @description
 * Creates a shallow clone (lodash.clone).
 *
 * ::: tip See also
 * [lodash.clone](https://lodash.com/docs/4.17.15#clone) package documentation
 * :::
 * @example
 * clone({ 'a': 1, 'b': 2 }) // creates a shallow clone of the object : { 'a': 1, 'b': 2 }
 */
export const clone = function (value) {
  return lodashClone(value)
}

export default clone
