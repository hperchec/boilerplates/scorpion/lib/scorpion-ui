#!/usr/bin/env node

'use strict'

const cli = require('./cli')

cli().parse(process.argv.slice(2))
