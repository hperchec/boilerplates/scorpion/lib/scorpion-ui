// Node v18 compat
const crypto = require('crypto')
const cryptoOrigCreateHash = crypto.createHash
crypto.createHash = algorithm => cryptoOrigCreateHash(algorithm === 'md4' ? 'sha256' : algorithm)

const path = require('path')
const deepMerge = require('deepmerge')
const { isPlainObject } = require('is-plain-object')
const StyleLintPlugin = require('stylelint-webpack-plugin')

const formatGlobals = require('../../globals/format-globals')

/**
 * Create vue config tool
 * @param {object} packageJson - The package.json content object
 * @param {object} globals - The globals object
 * @param {string} rootPath - The project root path
 * @param {object} [config = {}] - The custom configuration to merge
 * @param {object} [options = {}] - The options object
 * @param {string} [options.outputDir = 'www'] - Output path (relative to root directory)
 * @param {string} [options.jsOutputPath = 'js'] - JS output subdirectory (relative to output directory)
 * @param {string} [options.cssOutputPath = 'css'] - CSS output subdirectory (relative to output directory)
 * @returns {object} - The created config
 */
module.exports = function createConfig (packageJson, globals, rootPath, config = {}, options = {}) {
  // Get package.json content
  if (!packageJson) throw new Error('ScorpionUI - createConfig tool expects packageJson as first parameter, got undefined...')
  // Get globals
  if (!globals) throw new Error('ScorpionUI - createConfig tool expects globals as first parameter, got undefined...')
  // Get root path
  if (!rootPath) throw new Error('ScorpionUI - createConfig tool expects root path as first parameter, got undefined...')
  // Format globals
  const formattedGlobals = formatGlobals(globals)
  // Output path
  const outputDir = options.outputDir || 'www'
  // JS output subdirectory
  const jsOutputPath = options.jsOutputPath || 'js'
  // CSS output subdirectory
  const cssOutputPath = options.cssOutputPath || 'css'
  // Base configuration
  const baseConfig = {
    outputDir: outputDir,
    publicPath: './',
    devServer: {
      client: {
        overlay: {
          warnings: true,
          errors: true,
          runtimeErrors: (...args) => {
            if (window.ScorpionUI?.toolbox.development.runtime.errorHandler) {
              return window.ScorpionUI.toolbox.development.runtime.errorHandler(...args)
            }
            console.warn('[webpack-dev-server:client.overlay.runtimeErrors] window.ScorpionUI is not defined. Unable to call custom development runtime error handler')
            // Default return true
            return true
          }
        }
      }
    },
    configureWebpack: {
      // Output
      output: {
        filename: `${jsOutputPath}/[name].${packageJson.version}.js`,
        chunkFilename: `${jsOutputPath}/[name].${packageJson.version}.js`
      },
      resolve: {
        alias: {
          vue$: 'vue/dist/vue.esm.js',
          '@': path.resolve(rootPath, 'src'),
          '~': rootPath, // Root directory
          '#components': path.resolve(rootPath, 'src/components'),
          '#config': path.resolve(rootPath, 'src/config'),
          '#i18n': path.resolve(rootPath, 'src/i18n'),
          '#mixins': path.resolve(rootPath, 'src/mixins'),
          '#models': path.resolve(rootPath, 'src/models'),
          '#store': path.resolve(rootPath, 'src/store'),
          '#styles': path.resolve(rootPath, 'src/scss')
        }
      }
    },
    chainWebpack: _config => {
      // Add
      _config
        .plugin('stylelint')
        .use(StyleLintPlugin, [ {
          configFile: path.resolve(rootPath, 'stylelint.config.js'),
          files: [
            'src/**/*.{vue,htm,html,css,sss,less,scss,sass}'
          ],
          lintDirtyModulesOnly: true,
          exclude: [
            '**/node_modules/**',
            outputDir,
            'src/assets/**'
          ]
        } ])
      // Define plugin
      _config
        .plugin('define') // webpack.DefinePlugin
        .tap(args => {
          return [ {
            // Keep default global vars
            ...args[0],
            // add base url
            __BASE_URL__: 'process.env.BASE_URL',
            // add app version
            __APP_VERSION__: `'${packageJson.version}'`,
            // add custom globals
            ...formattedGlobals.nested // TODO: maybe add option to chose between 'flat' and 'nested' or both?
          } ]
        })
      // vue-loader-extension
      _config.module
        .rule('vue')
        .use('vue-loader-extension')
        .loader('@hperchec/scorpion-ui/shared/webpack/loaders/vue-loader-extension')
        .before('vue-loader')
    },
    css: {
      extract: {
        // Options similar to the same options in webpackOptions.output
        // both options are optional
        filename: `${cssOutputPath}/[name].${packageJson.version}.css`,
        chunkFilename: `${cssOutputPath}/[id].${packageJson.version}.css`
      }
    },
    /**
     * See @vue/cli-plugin-pwa: https://cli.vuejs.org/core-plugins/pwa.html#configuration
     */
    pwa: {
      /**
       * This allows you to choose between the two modes supported by the underlying workbox-webpack-plugin.
       * 'GenerateSW' (default), will lead to a new service worker file being created each time you rebuild your web app.
       * 'InjectManifest' allows you to start with an existing service worker file, and creates a copy of that file with a "precache manifest" injected into it.
       */
      workboxPluginMode: 'GenerateSW',
      /**
       * These options are passed on through to the underlying workbox-webpack-plugin.
       * For more information on what values are supported, please see the guide for GenerateSW or for InjectManifest.
       */
      // workboxOptions: {},
      /**
       * Default: "name" field in package.json
       *
       * Used as the value for the apple-mobile-web-app-title meta tag in the generated HTML. Note you will need to edit public/manifest.json to match this.
       */
      // name: '',
      /**
       * Theme color
       *
       * Default: '#4DBA87'
       */
      themeColor: '#4DBA87',
      /**
       * Microsoft tile color
       *
       * Default: '#000000'
       */
      msTileColor: '#000000',
      /**
       * Default: 'no'
       *
       * This defaults to 'no' because iOS before 11.3 does not have proper PWA support.
       */
      appleMobileWebAppCapable: 'no',
      /**
       * Default: 'default'
       */
      appleMobileWebAppStatusBarStyle: 'default',
      /**
       * Default: ''
       *
       * This option is used if you need to add a version to your icons and manifest, against browser’s cache.
       * This will append ?v=<pwa.assetsVersion> to the URLs of the icons and manifest.
       */
      assetsVersion: '',
      /**
       * Default: 'manifest.json'
       *
       * The path of app’s manifest. If the path is an URL, the plugin won't
       * generate a manifest.json in the dist directory during the build.
       */
      manifestPath: 'manifest.json',
      /**
       * Default: {}
       *
       * The object will be used to generate the manifest.json
       *
       * If the following attributes are not defined in the object, the options of pwa or default options will be used instead.
       *     name: pwa.name
       *     short_name: pwa.name
       *     start_url: '.'
       *     display: 'standalone'
       *     theme_color: pwa.themeColor
       */
      manifestOptions: {},
      /**
       * Default: undefined
       *
       * Value for crossorigin attribute in manifest link tag in the generated HTML.
       * You may need to set this if your PWA is behind an authenticated proxy.
       * See cross-origin values for more details.
       */
      manifestCrossorigin: undefined
      /**
       * Defaults:
       *   {
       *     faviconSVG: 'img/icons/favicon.svg',
       *     favicon32: 'img/icons/favicon-32x32.png',
       *     favicon16: 'img/icons/favicon-16x16.png',
       *     appleTouchIcon: 'img/icons/apple-touch-icon-152x152.png',
       *     maskIcon: 'img/icons/safari-pinned-tab.svg',
       *     msTileImage: 'img/icons/msapplication-icon-144x144.png'
       *   }
       */
      // iconPaths: {
      //   // ...
      // }
    }
  }
  // Before merging configs, save original "chainWebpack" option
  const originalChainWebpack = baseConfig.chainWebpack
  // Merge
  const mergedConfig = deepMerge(baseConfig, config, {
    isMergeableObject: isPlainObject // only merge object that are not instances (esle, it throws errors for webpack plugins)
  })
  // Override chainWebpack method if user provides its own
  if (config.chainWebpack) {
    mergedConfig.chainWebpack = _config => {
      // First, call original chainWebpack
      originalChainWebpack(_config)
      // Then, user custom
      config.chainWebpack(_config)
    }
  }
  // Return merged config
  return mergedConfig
}
