/**
 * @vuepress
 * ---
 * title: "Service (auth-manager): AuthManager class"
 * headline: "Service (auth-manager): AuthManager class"
 * sidebarTitle: .AuthManager
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import { getAllKeys, isNull, typeCheck } from '@/utils'
import toolbox from '@/toolbox'

const { VuePluginable } = toolbox.vue

/**
 * @extends VuePluginable
 * @classdesc
 * Service **auth-manager**: AuthManager class
 */
class AuthManager extends VuePluginable {
  /**
   * Create a new instance
   * @param {object} options - The constructor options
   * @param {Function} options.userModelClass - The User model class
   */
  constructor (options) {
    // Call VuePluginable constructor
    const initVMData = {
      currentUser: null
    }
    super(initVMData)
    // Set debugging private property
    if (this.__v_descriptor__) {
      this.__v_descriptor__.namespace = 'service:auth-manager'
      this.__v_descriptor__.description = 'Access the AuthManager instance'
      // @ts-ignore
      this.__v_descriptor__.externalLink = __getDocUrl__('/api/services/auth-manager/AuthManager')
    }
    // Set User model class
    this.#userModelClass = options.userModelClass
  }

  /**
   * Private properties
   */

  /**
   * Save the User model class
   */
  #userModelClass

  /**
   * Overrides static VuePluginable properties
   */
  static reactive = true
  static rootOption = 'authManager'
  static aliases = []

  /**
   * Accessors & mutators
   */

  /**
   * Get User model class
   * @category properties
   * @readonly
   * @type {Function}
   */
  get userModelClass () {
    return this.#userModelClass
  }

  /**
   * Methods
   */

  /**
   * getCurrentUser
   * @returns {?any} The current user object
   * @description
   * Returns the current authenticated user.
   */
  getCurrentUser () {
    return this._vm.currentUser
  }

  /**
   * setCurrentUser
   * @category methods
   * @param {any} user - The user to set as current authenticated
   * @returns {any} Returns the user
   * @description
   * Set the current authenticated user
   */
  setCurrentUser (user) {
    // Check if item is a instance of Toaster
    if (isNull(user) || typeCheck(this.#userModelClass, user)) {
      this._vm.currentUser = user
    } else {
      throw new Error('AuthManager can\'t set current user: must be null or a User model class instance')
    }
    return this._vm.currentUser
  }

  /**
   * Overrides the default toJSON object method for JSON.stringify() calls
   * @returns {object} - The instance data to be serialized
   */
  toJSON () {
    const keys = getAllKeys(this, {
      excludeKeys: [
        /^_/, // private properties that starts with '_'
        'toJSON',
        'toString',
        'constructor'
      ]
    })
    const jsonObj = keys.reduce((obj, key) => {
      obj[key] = this[key]
      return obj
    }, {})
    return jsonObj
  }

  /**
   * Static
   */

  /**
   * Install method for Vue
   * @param {Vue} _Vue - Vue
   * @param {object} [options = {}] - The options of the plugin
   */
  static install (_Vue, options = {}) {
    super.install(_Vue)
  }
}

export default AuthManager
