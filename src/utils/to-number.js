/**
 * @vuepress
 * ---
 * title: toNumber
 * headline: toNumber
 * sidebarTitle: toNumber
 * ---
 */

/**
 * toNumber
 * @param {*} value - The value
 * @returns {number} Returns the number
 * @description
 * Converts value to Number. Throws error if returned value is NaN
 * @example
 * toNumber("2") // => 2
 * toNumber(false) // => 0
 * toNumber("john doe") // => error
 */
export const toNumber = function (value) {
  const result = Number(value)
  if (isNaN(result)) throw new Error(`Can't convert value "${value}" to Number...`)
  return result
}

export default toNumber
