export default Toaster;
declare class Toaster {
    static beforePublish(toast: Toast, toaster: Toaster, toastOptions: object): Promise<void>;
    static afterPublish(toast: Toast, toaster: Toaster, toastOptions: object): Promise<void>;
    static beforeDelete(toast: Toast, toaster: Toaster): Promise<void>;
    static afterDelete(toast: Toast, toaster: Toaster): Promise<void>;
    static readonly get defaultOptions(): any;
    constructor(options: {
        name: string;
        toasts?: Collection;
        beforePublish?: Function;
        afterPublish?: Function;
        beforeDelete?: Function;
        afterDelete?: Function;
    });
    private _name;
    _toasts: any;
    _beforePublish: any;
    _afterPublish: any;
    _beforeDelete: any;
    _afterDelete: any;
    set name(value: string);
    get name(): string;
    readonly get toasts(): Collection;
    setToasts(value: Collection): void;
    setBeforePublish(value: Function): void;
    setAfterPublish(value: Function): void;
    setBeforeDelete(value: Function): void;
    setAfterDelete(value: Function): void;
    beforePublish(...args: any[]): Promise<void>;
    afterPublish(...args: any[]): Promise<void>;
    beforeDelete(...args: any[]): Promise<void>;
    afterDelete(...args: any[]): Promise<void>;
    delete(id: string): Promise<boolean>;
    publish(toastOptions: object): Promise<Toast>;
    toJSON(): object;
}
import Collection from '../../context/support/collection/Collection';
import Toast from './Toast';
//# sourceMappingURL=Toaster.d.ts.map