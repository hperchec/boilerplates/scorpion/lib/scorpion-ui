export default ModalCollection;
declare class ModalCollection extends XArray {
    constructor(modals?: Modal[]);
    get(name: string): Modal | null;
    add(modal: Modal): ModalCollection;
}
import { XArray } from '@/utils';
import Modal from './Modal';
//# sourceMappingURL=ModalCollection.d.ts.map