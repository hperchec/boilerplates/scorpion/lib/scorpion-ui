declare namespace _default {
    let name: string;
    namespace props {
        namespace queryParams {
            export let type: ObjectConstructor;
            function _default(): {};
            export { _default as default };
        }
    }
    let computed: {};
    namespace methods {
        function updatePageTitle(to: any, from: any): void;
        function emitOnQueryParamChange(to: any, from: any): void;
        function syncQueryParamsWith(ref: any, map: any): void;
    }
    function beforeRouteEnter(to: any, from: any, next: any): void;
    function beforeRouteUpdate(to: any, from: any, next: any): void;
    function beforeRouteLeave(to: any, from: any, next: any): void;
    namespace watch {
        function $route(to: any, from: any): void;
    }
    function mounted(): void;
}
export default _default;
//# sourceMappingURL=RouteComponentMixin.d.ts.map