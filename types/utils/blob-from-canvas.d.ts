export function blobFromCanvas(canvas: HTMLCanvasElement): Promise<Blob>;
export default blobFromCanvas;
//# sourceMappingURL=blob-from-canvas.d.ts.map