/**
 * PostCSS config
 * See also: https://github.com/postcss/postcss-load-config
 * @param root0
 */
module.exports = ({ cwd, env, file, options }) => {
  return {
    ...options,
    plugins: {
      autoprefixer: {},
      // use cssnano only for files ending with '.min.css'
      cssnano: (options.to || file?.basename || '').endsWith('.min.css')
        ? {
          preset: 'default'
        }
        : false
    }
  }
}
// env === 'production' ? {} : false
