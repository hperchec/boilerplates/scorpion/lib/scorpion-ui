/**
 * @vuepress
 * ---
 * title: "Service (device-manager): DeviceManager class"
 * headline: "Service (device-manager): DeviceManager class"
 * sidebarTitle: .DeviceManager
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import { getAllKeys } from '@/utils'
import toolbox from '@/toolbox'

const { VuePluginable } = toolbox.vue

/**
 * @extends VuePluginable
 * @classdesc
 * Service **device-manager**: DeviceManager class
 */
class DeviceManager extends VuePluginable {
  /**
   * Create a new instance
   * @param {object} options - The constructor options
   */
  constructor (options) {
    // Call VuePluginable constructor
    const initVMData = {}
    super(initVMData)
    // Set debugging private property
    if (this.__v_descriptor__) {
      this.__v_descriptor__.namespace = 'service:device-manager'
      this.__v_descriptor__.description = 'Access the DeviceManager instance'
      // @ts-ignore
      this.__v_descriptor__.externalLink = __getDocUrl__('/api/services/device-manager/DeviceManager')
    }
  }

  /**
   * Private properties
   */

  // ...

  /**
   * Overrides static VuePluginable properties
   */
  static reactive = true
  static rootOption = 'deviceManager'
  static aliases = []

  /**
   * Accessors & mutators
   */

  // ...

  /**
   * Methods
   */

  /**
   * Overrides the default toJSON object method for JSON.stringify() calls
   * @returns {object} - The instance data to be serialized
   */
  toJSON () {
    const keys = getAllKeys(this, {
      excludeKeys: [
        /^_/, // private properties that starts with '_'
        'toJSON',
        'toString',
        'constructor'
      ]
    })
    const jsonObj = keys.reduce((obj, key) => {
      obj[key] = this[key]
      return obj
    }, {})
    return jsonObj
  }

  /**
   * Static
   */

  /**
   * Install method for Vue
   * @param {Vue} _Vue - Vue
   * @param {object} [options = {}] - The options of the plugin
   */
  static install (_Vue, options = {}) {
    super.install(_Vue)
  }
}

export default DeviceManager
