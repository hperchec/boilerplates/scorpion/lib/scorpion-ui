---
title: Service (websocket-manager) configuration
headline: Service (websocket-manager) configuration
sidebarTitle: Configuration
prev: false
next: false
---

# Service (websocket-manager) configuration

<a name="configuration">
</a>

## Configuration

Service configuration> Refers to `Core.config.services['websocket-manager']````js'websocket-manager': {  // Connections definitions  // Overwrites service option: `connections`  connections: {    // <name> is the connection name and the value is a connection options object    // (see WSConnectionOptions constructor third param)    '<name>': Object,    ...  }}```

<a name="configuration-connections">
</a>

### Core.config.services[&#x27;websocket-manager&#x27;].connections : <code>object</code>

Connection definitions. An object following the schema:```js{  [key: String]: Object}```Object keys are the connection names while values are Object like WSConnectionOptions constructor third param.

