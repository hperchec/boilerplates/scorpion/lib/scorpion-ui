/**
 * @vuepress
 * ---
 * title: blobFromCanvas
 * headline: blobFromCanvas
 * sidebarTitle: blobFromCanvas
 * ---
 */

/**
 * blobFromCanvas
 * @param {HTMLCanvasElement} canvas - The target canvas
 * @returns {Promise<Blob>} Returns blob
 * @description
 * Convert HTMLCanvasElement to blob
 * @example
 * blobFromCanvas(canvas)
 */
export const blobFromCanvas = async function (canvas) {
  return new Promise((resolve, reject) => {
    canvas.toBlob((blob) => {
      resolve(blob)
    })
  })
}

export default blobFromCanvas
