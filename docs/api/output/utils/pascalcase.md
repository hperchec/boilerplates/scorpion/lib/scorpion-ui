---
title: pascalcase
headline: pascalcase
sidebarTitle: pascalcase
---

# pascalcase

<a name="pascalcase">
</a>

## pascalcase ⇒ <code>string</code>

Convert a string to pascalcase *([Defined as Vue filter](../context/vue/filters#filters-pascalcase))*.

**Returns**: <code>string</code> - Returns the "pascalcased" string

| Param | Type | Description |
| --- | --- | --- |
| str | <code>string</code> | The string to pascalcase |

**Example**

```js
pascalcase('foo bar') // => 'FooBar'
```

