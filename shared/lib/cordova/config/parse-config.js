const fs = require('fs')
const { XMLParser } = require('fast-xml-parser')
const xmlOptions = require('./xml-options.js')

module.exports = function parseConfig (configFilePath, options = xmlOptions) {
  const xmlParser = new XMLParser(options)
  return xmlParser.parse(fs.readFileSync(configFilePath, 'utf8'))
}
