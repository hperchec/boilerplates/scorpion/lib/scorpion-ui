/**
 * @vuepress
 * ---
 * title: set
 * headline: set
 * sidebarTitle: set
 * ---
 */

// Lodash.set
import lodashSet from 'lodash.set'

/**
 * set
 * @param {object} object - The object to modify
 * @param {Array|string} path - The path of the property to set
 * @param {*} value - The value to set
 * @returns {void}
 * @description
 * Set object property (lodash.set).
 * @example
 * const object = { 'a': [{ 'b': { 'c': 3 } }] }
 * set(object, 'a[0].b.c', 4) // => { 'a': [{ 'b': { 'c': 4 } }] }
 */
export const set = function (object, path, value) {
  return lodashSet(object, path, value)
}

export default set
