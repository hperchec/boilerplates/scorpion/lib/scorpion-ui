import publicMiddleware from './public-middleware'
import privateMiddleware from './private-middleware'

export default {
  publicMiddleware,
  privateMiddleware
}
