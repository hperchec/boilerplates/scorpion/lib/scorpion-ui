declare namespace _default {
    export { createModule };
    export { generateActions };
    export { generateGetters };
    export { generateMutations };
    export { generateState };
    export { getResourceDataManager };
    export { getResourceModule };
}
export default _default;
import createModule from './create-module';
import generateActions from './generate-actions';
import generateGetters from './generate-getters';
import generateMutations from './generate-mutations';
import generateState from './generate-state';
import getResourceDataManager from './get-resource-data-manager';
import getResourceModule from './get-resource-module';
//# sourceMappingURL=index.d.ts.map