---
title: isValidArrayIndex
headline: isValidArrayIndex
sidebarTitle: isValidArrayIndex
---

# isValidArrayIndex

<a name="isvalidarrayindex">
</a>

## isValidArrayIndex ⇒ <code>boolean</code>

Check if value is a valid array index.

**Returns**: <code>boolean</code> - Returns true if `value` is a valid array index

| Param | Type | Description |
| --- | --- | --- |
| value | <code>\*</code> | The value to check |

**Example**

```js
isValidArrayIndex({ a: 1, b: 2 }) // falseisValidArrayIndex(1) // true
```

