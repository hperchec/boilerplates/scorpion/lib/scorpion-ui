/**
 * @vuepress
 * ---
 * title: "Service (notification-manager)"
 * headline: "Service (notification-manager)"
 * sidebarTitle: notification-manager
 * initialOpenGroupIndex: -1 # optional, defaults to 0, defines the index of initially opened subgroup
 * prev: ../
 * next: ./options
 * ---
 */

import Service from '@/Service'

import options from './options'
import NotificationManager from './NotificationManager'

/* eslint-disable jsdoc/require-param, jsdoc/require-returns */

/**
 * @scorpion_ui_service {NotificationManager} notification-manager
 * @description
 * The service "notification-manager" is designed to make notification managemnent easier.
 */
export default new Service({
  // Service
  service: {
    // Identifier
    identifier: 'notification-manager',
    /**
     * @alias register
     * @description
     * On registering, the service will inject properties to Vue root instance prototype `$app`:
     *
     * - `$app.throwSystemError`
     * - `$app.clearSystemError`
     */
    register: function (_Core) {
      // // Define vue root instance prototype in '$app' namespace
      // const $app = _Core.context.vue.root.prototype.app
      // /**
      //  * @alias $app.throwSystemError
      //  * @param {AppError} appError - The error instance
      //  * @returns {void}
      //  * @description
      //  * Throw a system error. If the AppError is already caught by NotificationManager, dont process
      //  * @example
      //  * // In any component
      //  * this.$app.throwSystemError(new AppError())
      //  */
      // $app.throwSystemError = function (appError) {
      //   const root = this // 'this' is the Vue root instance
      //   // @ts-ignore
      //   if (!(appError instanceof AppError || appError instanceof _Core.context.support.errors.AppError)) {
      //     throw new Error('vm.$app.throwSystemError expects instance of AppError as first parameter')
      //   } else {
      //     // Set the system error in the store
      //     root.$store.dispatch('System/throwSystemError', appError)
      //     // If the AppError is already caught, dont process
      //     // @ts-ignore
      //     if (appError.isCaught()) {
      //       // ...
      //     } else {
      //       // This will run the error handlers
      //       root.$errorManager.throw(appError)
      //     }
      //   }
      // }
      // /**
      //  * @alias $app.clearSystemError
      //  * @returns {void}
      //  * @description
      //  * Clear the current system error.
      //  * @example
      //  * // In any component
      //  * this.$app.clearSystemError()
      //  */
      // $app.clearSystemError = function () {
      //   const root = this // 'this' is the Vue root instance
      //   root.$store.dispatch('System/clearSystemError')
      // }
    },
    /**
     * @alias create
     * @description
     * Returns an instance of `NotificationManager` created with options.
     */
    create: (serviceOptions, { NotificationManager: _NotificationManager }, _Core) => {
      // Return new NotificationManager
      return new _NotificationManager(serviceOptions)
    },
    /**
     * @alias plugin
     * @description
     * The following properties will be set to Vue prototype:
     *
     * ```js
     * Vue.prototype.$notificationManager // => Core.service('notification-manager')
     * ```
     */
    vuePlugin: ({ NotificationManager: _NotificationManager }, _Core) => _NotificationManager,
    // Vue plugin options
    pluginOptions: {},
    // rootOption
    rootOption: ({ NotificationManager: _NotificationManager }, _Core) => _NotificationManager.rootOption
  },
  // Core is initialized
  onInitialized: (Core) => {
    // ...
  },
  // Options
  options,
  /**
   * @alias expose
   */
  expose: {
    /**
     * @see {@link ./NotificationManager NotificationManager}
     */
    NotificationManager
  }
})
