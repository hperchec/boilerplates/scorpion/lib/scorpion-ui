const options = require('@github/markdownlint-github').init()

/**
 * Export markdownlint config
 */
module.exports = {
  config: {
    ...options
    // // MD004 - Unordered list style
    // 'ul-style': {
    //   'style': 'consistent'
    // }
  },
  customRules: [
    '@github/markdownlint-github'
  ],
  outputFormatters: [
    [ 
      'markdownlint-cli2-formatter-pretty',
      {
        appendLink: true // ensures the error message includes a link to the rule documentation
      }
    ]
  ]
}