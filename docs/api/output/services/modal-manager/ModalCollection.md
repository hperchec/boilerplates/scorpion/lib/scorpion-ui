---
title: "Service (modal-manager): ModalCollection class"
headline: "Service (modal-manager): ModalCollection class"
sidebarTitle: .ModalCollection
sidebarDepth: 0
prev: false
next: false
---

# Service (modal-manager): ModalCollection class

<a name="modalcollection">
</a>

## ModalCollection

Service **modal-manager**: ModalCollection class

**Schema**:

- [ModalCollection](#modalcollection)
  - [new ModalCollection(modals)](#newmodalcollectionnew)
  - [.get(name)](#modalcollection-get) ⇒ <code>module:services/modal-manager/Modal~Modal</code>
  - _methods_
    - [.add(modal)](#modalcollection-add) ⇒ <code>module:services/modal-manager/ModalCollection~ModalCollection</code>

<a name="newmodalcollectionnew">
</a>

### new ModalCollection(modals)

Create a new ModalCollection

| Param | Type | Description |
| --- | --- | --- |
| modals | <code>Array.&lt;module:services/modal-manager/Modal~Modal&gt;</code> | Array of Modal instances |

<a name="modalcollection-get">
</a>

### modalCollection.get(name) ⇒ <code>module:services/modal-manager/Modal~Modal</code>

Get a modal by name

**Returns**: <code>module:services/modal-manager/Modal~Modal</code> - The modal instance

| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The modal name |

<a name="modalcollection-add">
</a>

### modalCollection.add(modal) ⇒ <code>module:services/modal-manager/ModalCollection~ModalCollection</code>

Add an Modal to collection

**Returns**: <code>module:services/modal-manager/ModalCollection~ModalCollection</code> - Return the ModalCollection instance itself

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| modal | <code>module:services/modal-manager/Modal~Modal</code> | The Modal to add to manager |

