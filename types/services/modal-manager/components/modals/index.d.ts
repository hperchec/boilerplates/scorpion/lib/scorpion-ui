/// <reference path="../../../../vue-shim.d.ts" />
declare namespace _default {
    export { BaseModal };
    export { ModalContainer };
    export { ModalOverlay };
    export { Modal };
}
export default _default;
import BaseModal from './BaseModal';
import ModalContainer from './ModalContainer.vue';
import ModalOverlay from './ModalOverlay.vue';
import Modal from './Modal.vue';
//# sourceMappingURL=index.d.ts.map