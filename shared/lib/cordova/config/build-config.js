const fs = require('fs')
const { XMLBuilder } = require('fast-xml-parser')
const xmlOptions = require('./xml-options.js')

module.exports = function buildConfig (configFilePath, content, options = xmlOptions) {
  const xmlBuilder = new XMLBuilder(options)
  fs.writeFileSync(configFilePath, xmlBuilder.build(content), 'utf8')
  return true
}
