/**
 * @vuepress
 * ---
 * title: get
 * headline: get
 * sidebarTitle: get
 * ---
 */

// Lodash.get
import lodashGet from 'lodash.get'

/**
 * get
 * @param {object} object - The object to query
 * @param {Array|string} path - The path of the property to get
 * @param {*} [defaultValue] - The value returned for undefined resolved values
 * @returns {*} Returns the target property value
 * @description
 * Get object property (lodash.get).
 *
 * ::: tip See also
 * [lodash.has](https://www.npmjs.com/package/lodash.has) package documentation
 * :::
 * @example
 * const object = { 'a': [{ 'b': { 'c': 3 } }] }
 * get(object, 'a[0].b.c') // => 3
 */
export const get = function (object, path, defaultValue) {
  return lodashGet(object, path, defaultValue)
}

export default get
