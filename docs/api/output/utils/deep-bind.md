---
title: deepBind
headline: deepBind
sidebarTitle: deepBind
---

# deepBind

<a name="deepbind">
</a>

## deepBind ⇒ <code>object</code> \| <code>Array</code>

Bind a context to all functions in an object, including deeply nested functions::: tip See also[deep-bind](https://www.npmjs.com/package/deep-bind) package documentation:::

**Returns**: <code>object</code> \| <code>Array</code> - Object or Array with bound functions

| Param | Type | Description |
| --- | --- | --- |
| target | <code>object</code> \| <code>Array</code> | Object or Array with functions as values that will be bound |
| thisArg | <code>object</code> | Object to bind to the functions |

**Example**

```js
const thisArg = {  foo: 'bar'}const bound = deepBind({  foo: function() {    return this.foo // => "bar"  }}, thisArg)
```

