export default API;
declare class API {
    static readonly get supportedDrivers(): string[];
    constructor(apiOptions: APIOptions);
    private _name;
    _options: any;
    _driver: any;
    _methodsMap: any;
    set name(value: string);
    get name(): string;
    set options(value: string);
    get options(): string;
    readonly get driver(): any;
    setDriver(name: string, options?: object): void;
    mapDriverMethods(methods: object): void;
    request(method: string, url: string, options?: {
        allowConcurrent?: object;
    }, beforeCb?: Function, successCb?: Function, errorCb?: Function): Promise<any>;
    onBeforeRequest(func: Function | Function[]): void;
    onRequestSuccess(func: Function | Function[]): void;
    onRequestError(func: Function | Function[]): void;
    initAxios(options: import("axios").AxiosRequestConfig): void;
    toJSON(): object;
    #private;
}
import APIOptions from './APIOptions';
//# sourceMappingURL=API.d.ts.map