/**
 * @vuepress
 * ---
 * title: isTrue
 * headline: isTrue
 * sidebarTitle: isTrue
 * ---
 */

/**
 * isTrue
 * @param {*} value - The variable to check
 * @returns {boolean} Returns true if value strictly equals true
 * @description
 * Check if value is boolean and true (=== true)
 * @example
 * isTrue(true) // => true
 * isTrue('Hello') // => false
 */
export const isTrue = function (value) {
  return value === true
}

export default isTrue
