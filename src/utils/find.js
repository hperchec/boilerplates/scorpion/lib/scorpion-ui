/**
 * @vuepress
 * ---
 * title: find
 * headline: find
 * sidebarTitle: find
 * ---
 */

// Lodash.find
import lodashFind from 'lodash.find'

/**
 * find
 * @param {...*} args - Same args as lodash find method
 * @returns {boolean} Returns the found object or undefined
 * @description
 * Find an item in array/collection
 *
 * ::: tip See also
 * [lodash.find](https://www.npmjs.com/package/lodash.find) package documentation
 * :::
 */
export const find = function (...args) {
  return lodashFind(...args)
}

export default find
