/**
 * @vuepress
 * ---
 * title: "Service (api-manager): API class"
 * headline: "Service (api-manager): API class"
 * sidebarTitle: .API
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import axios from 'axios'
import Core from '@/Core'
import { camelcase, getAllKeys, typeCheck } from '@/utils'
import APIOptions from './APIOptions' // jsDoc type
import RequestManager from './RequestManager'

/**
 * @classdesc
 * Service **api-manager**: API class
 */
class API {
  /**
   * @private
   */
  _name
  _options
  _driver
  _methodsMap

  /**
   * Create a new API
   * @param {APIOptions} apiOptions - An APIOptions object
   */
  constructor (apiOptions) {
    if (typeCheck(Core.context.services['api-manager'].APIOptions, apiOptions)) {
      this.name = apiOptions.name
      this.options = apiOptions.options
      this.setDriver(apiOptions.driver)
      this.#requestManager = new RequestManager(this)
    } else {
      throw new TypeError('API class : constructor failed. APIOptions object attempted, received : ' + typeof apiOptions)
    }
  }

  /**
   * Accessors & mutators
   */

  /**
   * API name
   * @category properties
   * @type {string}
   */
  get name () {
    return this._name
  }

  set name (value) {
    // Check type
    if (typeCheck(String, value)) {
      // Assign
      this._name = value
    } else {
      // Else -> throw error
      throw new Error('API class: \'name\' property must be String. ' + typeof value + ' received...')
    }
  }

  /**
   * API options
   * @category properties
   * @type {string}
   */
  get options () {
    return this._options
  }

  set options (value) {
    // Check type
    if (typeCheck(Object, value)) {
      // Assign
      this._options = value
    } else {
      // Else -> throw error
      throw new Error('API class: \'options\' property must be Object. ' + typeof value + ' received...')
    }
  }

  /**
   * Get the API driver
   * @category properties
   * @readonly
   * @type {object}
   */
  get driver () {
    return this._driver
  }

  /**
   * Methods
   */

  /**
   * setDriver
   * @category methods
   * @param {string} name - The driver name
   * @param {object} [options] - The driver options/configuration
   * @returns {void}
   * @description
   * Set the driver for the API
   */
  setDriver (name, options) {
    if (API.supportedDrivers.indexOf(name) > -1) {
      this[camelcase(`init_${name}`)](options || this.options)
    }
  }

  /**
   * mapDriverMethods
   * @category methods
   * @param {object} methods - A methods object
   * @returns {void}
   * @description
   * Map driver methods for requests
   *
   * For each HTTP verb, assign driver method:
   *
   * ```js
   * {
   *   GET: methods.GET, // this.request('GET', <url>, [<options>])
   *   POST: methods.POST, // this.request('POST', <url>, [<options>])
   *   PATCH: methods.PATCH, // this.request('PATCH', <url>, [<options>])
   *   PUT: methods.PUT, // this.request('PUT', <url>, [<options>])
   *   DELETE: methods.DELETE, // this.request('DELETE', <url>, [<options>])
   *   OPTIONS: methods.OPTIONS // this.request('OPTIONS', <url>, [<options>])
   * }
   * ```
   */
  mapDriverMethods (methods) {
    const genError = (val) => { throw new Error('API class : unmapped or unsupported driver method "' + val + '"') }
    this._methodsMap = {
      GET: methods.GET || function () { genError('GET') },
      POST: methods.POST || function () { genError('POST') },
      PATCH: methods.PATCH || function () { genError('PATCH') },
      PUT: methods.PUT || function () { genError('PUT') },
      DELETE: methods.DELETE || function () { genError('DELETE') },
      OPTIONS: methods.OPTIONS || function () { genError('OPTIONS') }
    }
  }

  /**
   * request
   * @category async methods
   * @param {string} method - The method name (ex: GET, POST...)
   * @param {string} url - The URL to request, without baseURL (Ex: /users)
   * @param {object} [options = {}] - The specific options for the driver
   * @param {object} [options.allowConcurrent = false] - Send request even if concurrent exists
   * @param {Function} [beforeCb] - The function to exec before request
   * @param {Function} [successCb] - The success callback
   * @param {Function} [errorCb] - The error callback
   * @returns {Promise<*>} The driver Promise response
   * @description
   * Main request method
   */
  request (method, url, options = {}, beforeCb, successCb, errorCb) {
    const { allowConcurrent, ..._options } = options
    // If method exists
    if (Object.keys(this._methodsMap).indexOf(method) > -1) {
      const request = this.#requestManager.createRequest(
        // Request generator
        (requestInstance) => {
          // Dynamically call the corresponding method mapped to the driver equivalent
          return this._methodsMap[method](url, _options) // Must return a promise
        },
        // Method
        method,
        // URL,
        url,
        // Request options
        _options,
        // Before callback
        beforeCb,
        // On request success
        successCb,
        // On request error
        errorCb
      )
      return request.send({ allowConcurrent })
    } else {
      throw new Error('API class : unrecognized method "' + method + '".')
    }
  }

  /**
   * onBeforeRequest
   * @category public methods
   * @param {Function|Function[]} func - The function(s) to append to the hook queue
   * @returns {void}
   * @description
   * Before request hook, see also RequestManager#onBeforeRequest method
   * @example
   * api.onBeforeRequest((request) => {
   *   console.log(`Before request!`, request)
   * })
   */
  onBeforeRequest (func) {
    this.#requestManager.onBeforeRequest(func)
  }

  /**
   * onRequestSuccess
   * @category public methods
   * @param {Function|Function[]} func - The function(s) to append to the hook queue
   * @returns {void}
   * @description
   * On success request hook, see also RequestManager#onRequestSuccess method
   * @example
   * api.onRequestSuccess((response, request) => {
   *   console.log(`Request success!`, response)
   * })
   */
  onRequestSuccess (func) {
    this.#requestManager.onRequestSuccess(func)
  }

  /**
   * onRequestError
   * @category public methods
   * @param {Function|Function[]} func - The function(s) to append to the hook queue
   * @returns {void}
   * @description
   * On request error hook, see also RequestManager#onRequestError method
   * @example
   * api.onRequestError((error, request) => {
   *   console.log(`Request error!`, error)
   * })
   */
  onRequestError (func) {
    this.#requestManager.onRequestError(func)
  }

  /**
   * initAxios
   * @category methods
   * @param {import("axios").AxiosRequestConfig} options - A AxiosRequestConfig object
   * @returns {void}
   * @description
   * Assign an axios instance to this.driver
   *
   * See [axios documentation](https://axios-http.com/fr/docs/req_config) to see configuration options
   */
  initAxios (options) {
    // Assign driver
    this._driver = axios.create(options) // AxiosInstance object
    // Map
    this.mapDriverMethods({
      /**
       * Axios driver GET mapped method
       * @ignore
       * @param {string} url - The URL to request (see request method)
       * @param {object} options - The payload for the driver
       * @returns {Promise} - Must return a Promise
       */
      GET: (url, options) => {
        return this._driver.get(url, options) // axios.get(...) -> Promise
      },
      /**
       * Axios driver POST mapped method
       * @ignore
       * @param {string} url - The URL to request (see request method)
       * @param {object} options - The payload for the driver
       * @returns {Promise} - Must return a Promise
       */
      POST: (url, options) => {
        const { data, ...opts } = options
        return this._driver.post(url, data, opts) // axios.post(...) -> Promise
      },
      /**
       * Axios driver PATCH mapped method
       * @ignore
       * @param {string} url - The URL to request (see request method)
       * @param {object} options - The payload for the driver
       * @returns {Promise} - Must return a Promise
       */
      PATCH: (url, options) => {
        const { data, ...opts } = options
        return this._driver.patch(url, data, opts) // axios.patch(...) -> Promise
      },
      /**
       * Axios driver DELETE mapped method
       * @ignore
       * @param {string} url - The URL to request (see request method)
       * @param {object} options - The payload for the driver
       * @returns {Promise} - Must return a Promise
       */
      DELETE: (url, options) => {
        const { data, ...opts } = options
        // 'data' payload property must be named for delete method
        return this._driver.delete(url, { data: data }, opts) // axios.delete(...) -> Promise
      }
    })
  }

  /**
   * Overrides the default toJSON object method for JSON.stringify() calls
   * @returns {object} - The instance data to be serialized
   */
  toJSON () {
    const keys = getAllKeys(this, {
      excludeKeys: [
        /^_/, // private properties that starts with '_'
        'toJSON',
        'toString',
        'constructor'
      ]
    })
    const jsonObj = keys.reduce((obj, key) => {
      obj[key] = this[key]
      return obj
    }, {})
    return jsonObj
  }

  /**
   * Static
   */

  /**
   * supportedDrivers
   * @category properties
   * @readonly
   * @type {string[]}
   * @description
   * Only 'axios' is supported as a driver for the moment
   */
  static get supportedDrivers () {
    return [ 'axios' ] // (No more supported 'drivers' for the moment...)
  }

  /**
   * Private properties
   */

  /**
   * Request manager
   * @category private properties
   * @type {object}
   */
  #requestManager = undefined
}

export default API
