/**
 * @vuepress
 * ---
 * title: PublicRoute class
 * headline: PublicRoute class
 * sidebarTitle: .PublicRoute
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import Route from './Route'

/**
 * @extends Route
 * @classdesc
 * Class that represents a *public* route
 */
class PublicRoute extends Route { // eslint-disable-line dot-notation
  /**
   * @param {...*} args - Same arguments as parent class
   * @description
   * Create a PrivateRoute
   *
   * See also {@link ../Route Route}
   */
  constructor (...args) { // eslint-disable-line no-useless-constructor
    // @ts-ignore
    super(...args)
  }

  /**
   * Accessors & mutators
   */

  // ...

  /**
   * Methods
   */

  // ...
}

export default PublicRoute
