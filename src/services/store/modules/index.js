import Resources from './resources'
import System from './system'

export default {
  Resources,
  System
}
