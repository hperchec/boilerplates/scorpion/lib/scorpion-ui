---
title: Service (router) options
headline: Service (router) options
sidebarTitle: Options
sidebarDepth: 0
prev: false
next: false
---

# Service (router) options

<a name="options">
</a>

## .options

Accepts all VueRouter options plus the following:- `defaultOnComplete`- `defaultOnAbort`- `beforeEach`- `beforeResolve`- `afterEach`- `initOnRootInstanceMounted`- `catchAllRedirect`By default, each **private** and **public** route are auto-injected as `routes` option.Each object in `Core.context.services['router'].routes.private` and`Core.context.services['router'].routes.public` will be passed respectively to `PrivateRoute` and `PublicRoute` constructor.The result will be an array of the defined routes. You can overwrite it by setting this option.Configurable options:- **initOnRootInstanceMounted**: `Core.config.services['router'].initOnRootInstanceMounted`- **catchAllRedirect**: `Core.config.services['router'].catchAllRedirect`::: tip SEE ALSO[VueRouter construction options documentation](https://v3.router.vuejs.org/api/#router-construction-options):::

**Schema**:

- [.options](#options)
  - [.defaultOnComplete](#options-defaultoncomplete) : <code>function</code>
  - [.beforeEach](#options-beforeeach) : <code>Array.&lt;function()&gt;</code>
  - [.beforeResolve](#options-beforeresolve) : <code>Array.&lt;function()&gt;</code>
  - [.afterEach](#options-aftereach) : <code>Array.&lt;function()&gt;</code>
  - [.initOnRootInstanceMounted](#options-initonrootinstancemounted) : <code>boolean</code>
  - [.catchAllRedirect](#options-catchallredirect) : <code>string</code> \| <code>object</code>
  - [.defaultOnAbort(error)](#options-defaultonabort) : <code>function</code>

<a name="options-defaultoncomplete">
</a>

### options.defaultOnComplete : <code>function</code>

The default onComplete callback to pass to router `push` method.

<a name="options-beforeeach">
</a>

### options.beforeEach : <code>Array.&lt;function()&gt;</code>

Global before hook. Each function will be injected during router service creationvia `router.beforeEach()` before returning new router instance.::: tip SEE ALSO[VueRouter instance beforeEach documentation](https://v3.router.vuejs.org/api/#router-beforeeach):::By default, it is a empty `XArray` (see utils documentation). Each middleware in `Core.context.services['router'].middlewares`will be injected at service creation.The result will be a concatenated array of the defined middlewares and the `beforeEach` option array content.

<a name="options-beforeresolve">
</a>

### options.beforeResolve : <code>Array.&lt;function()&gt;</code>

Global before resolve hook. Each function will be injected during router service creationvia `router.beforeResolve()` before returning new router instance.::: tip SEE ALSO[VueRouter instance beforeResolve documentation](https://v3.router.vuejs.org/api/#router-beforeresolve):::By default, it is an empty `XArray`

<a name="options-aftereach">
</a>

### options.afterEach : <code>Array.&lt;function()&gt;</code>

Global after hook. Each function will be injected during router service creationvia `router.afterEach()` before returning new router instance.::: tip SEE ALSO[VueRouter instance afterEach documentation](https://v3.router.vuejs.org/api/#router-aftereach):::By default, it is an empty `XArray`

<a name="options-initonrootinstancemounted">
</a>

### options.initOnRootInstanceMounted : <code>boolean</code>

Define if it auto triggers routing at root instance mounted hook.Can be overwritten via Core global configuration: `Core.config.services['router'].initOnRootInstanceMounted`

**Default**: <code>true</code>  
<a name="options-catchallredirect">
</a>

### options.catchAllRedirect : <code>string</code> \| <code>object</code>

The path / route object to redirect in _CatchAll before hook.Can be overwritten via Core global configuration: `Core.config.services['router'].catchAllRedirect`

**Default**: <code>&quot;/404&quot;</code>  
<a name="options-defaultonabort">
</a>

### options.defaultOnAbort(error) : <code>function</code>

The default onAbort callback to pass to router `push` method.

| Param | Type | Description |
| --- | --- | --- |
| error | <code>Error</code> | The error instance |

