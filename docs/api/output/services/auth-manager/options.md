---
title: Service (auth-manager) options
headline: Service (auth-manager) options
sidebarTitle: Options
sidebarDepth: 0
prev: false
next: false
---

# Service (auth-manager) options

<a name="options">
</a>

## options

Authentication manager service optionsAccepts the following options:- `userModelClass`

<a name="options-usermodelclass">
</a>

### Core.context.services[&#x27;auth-manager&#x27;].options.userModelClass : <code>function</code>

User model class. Default is undefined

