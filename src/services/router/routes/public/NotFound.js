/**
 * @vuepress
 * ---
 * title: NotFound route
 * headline: NotFound route
 * sidebarTitle: .NotFound
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import Core from '@/Core'
import { capitalize } from '@/utils'

/**
 * @param  {...any} args - See i18n method
 * @returns {string}
 */
// @ts-ignore
const translate = (...args) => Core.service('i18n').t(...args) // eslint-disable-line dot-notation

/**
 * @name NotFound
 * @static
 * @type {object}
 * @description
 * - **Name**: NotFound
 * - **Path**: `/404`
 * - **Params**: *none*
 * - **Query**: *none*
 */
export default {
  /**
   * @description Route path
   * @name NotFound.path
   * @type {string}
   * @default /404
   */
  path: '/404',
  /**
   * @description Route name
   * @name NotFound.name
   * @type {string}
   * @default NotFound
   */
  name: 'NotFound',
  /**
   * @alias NotFound.component
   * @returns {object} Returns view component
   * @description
   * Route component. By default, returns `Core.context.vue.components.public.views.notFound.Index`
   */
  component: () => Core.context.vue.components.public.views.notFound.Index,
  /**
   * Options
   */
  options: {
    /**
     * @description Route meta
     * @alias NotFound.meta
     * @type {object}
     */
    meta: {
      /**
       * @alias NotFound.meta.pageMeta
       * @type {object}
       * @returns {string}
       * @description
       * Return translated NotFound view title (translate: `views.NotFound.Index.title`)
       */
      pageMeta: {
        title: (vm) => capitalize(translate('views.NotFound.Index.title'))
      }
    }
  }
}
