---
title: "Service (device-manager): DeviceManager class"
headline: "Service (device-manager): DeviceManager class"
sidebarTitle: .DeviceManager
sidebarDepth: 0
prev: false
next: false
---

# Service (device-manager): DeviceManager class

<a name="devicemanager">
</a>

## DeviceManager ⇐ <code>VuePluginable</code>

Service **device-manager**: DeviceManager class

**Extends**: <code>VuePluginable</code>  
**Schema**:

- [DeviceManager](#devicemanager) ⇐ <code>VuePluginable</code>
  - [new DeviceManager(options)](#newdevicemanagernew)
  - _instance_
    - [.reactive](#devicemanager-reactive)
    - [.toJSON()](#devicemanager-tojson) ⇒ <code>object</code>
  - _static_
    - [.install(_Vue, [options])](#devicemanager-install)

<a name="newdevicemanagernew">
</a>

### new DeviceManager(options)

Create a new instance

| Param | Type | Description |
| --- | --- | --- |
| options | <code>object</code> | The constructor options |

<a name="devicemanager-reactive">
</a>

### deviceManager.reactive

Overrides static VuePluginable properties

<a name="devicemanager-tojson">
</a>

### deviceManager.toJSON() ⇒ <code>object</code>

Overrides the default toJSON object method for JSON.stringify() calls

**Returns**: <code>object</code> - - The instance data to be serialized

<a name="devicemanager-install">
</a>

### DeviceManager.install(_Vue, [options])

Install method for Vue

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| _Vue | <code>Vue</code> |  | Vue |
| [options] | <code>object</code> | <code>{}</code> | The options of the plugin |

