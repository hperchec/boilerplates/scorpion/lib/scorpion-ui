---
title: Service (notification-manager)
headline: Service (notification-manager)
sidebarTitle: notification-manager
initialOpenGroupIndex: -1
prev: ../
next: ./options
---

# Service (notification-manager)

<a name="service"></a>

The service "notification-manager" is designed to make notification managemnent easier.

## Register

On registering, the service will inject properties to Vue root instance prototype `$app`:- `$app.throwSystemError`- `$app.clearSystemError`

## Create

Returns an instance of `NotificationManager` created with options.

**Returns**: <code>module:services/notification-manager/NotificationManager~NotificationManager</code> - Once services are created:

```js
Core.service('notification-manager')
```

## Vue

### Plugin

On registering, service Vue plugin will be set in `Core.context.vue.plugins['notification-manager']`.

Plugin will be automatically used by Vue.

The following properties will be set to Vue prototype:```jsVue.prototype.$notificationManager // => Core.service('notification-manager')```

### Root instance

<a name="app"></a>

#### Prototype $app property

On registering, the service will inject properties to \`Core.context.vue.root.prototype.app\`.

> Properties will be shared by all components under \`vm.$app\` where \`vm\` is the component instance.

<a name="app-usersettings"></a>

##### User settings

Service will define the following user settings keys:

##### Others

## Expose (service context)

The service will expose the following properties under `Core.context.services['notification-manager']`:
<a name="expose-notificationmanager">
</a>

### .NotificationManager

> Exposed as `Core.context.services['notification-manager'].NotificationManager`

**See**: [NotificationManager](./NotificationManager)

