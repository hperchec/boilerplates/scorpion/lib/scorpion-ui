---
title: make-configurable-option
headline: make-configurable-option
---

# make-configurable-option

<a name="makeconfigurableoption">
</a>

## makeConfigurableOption ⇒ <code>void</code>

Make service option configurable: the value of the option is extracted from Core global config 'services' property.

| Param | Type | Description |
| --- | --- | --- |
| target | <code>object</code> | Target service options object |
| options | <code>object</code> | Options |
| options.propertyName | <code>string</code> | The property name |
| options.serviceIdentifier | <code>string</code> | The service identifier |
| options.configPath | <code>string</code> | The config path to check with has/get utils |
| [options.formatter] | <code>function</code> | The function to transform value from configuration. Default returns value as is. |
| options.defaultValue | <code>\*</code> | The default value |

