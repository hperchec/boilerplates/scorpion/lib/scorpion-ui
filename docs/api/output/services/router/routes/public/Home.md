---
title: Home route
headline: Home route
sidebarTitle: .Home
sidebarDepth: 0
prev: false
next: false
---

# Home route

## Functions

<dl>
<dt><a href="#translate">translate(...args)</a> ⇒ <code>string</code></dt>
<dd></dd>
</dl>

<a name="translate">
</a>

## translate(...args) ⇒ <code>string</code>

| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>any</code> | See i18n method |

<a name="home">
</a>

## .Home : <code>object</code>

- **Name**: Home- **Path**: `/`- **Params**: *none*- **Query**: *none*

**Schema**:

- [.Home](#home) : <code>object</code>
  - [.path](#home-path) : <code>string</code>
  - [.name](#home-name) : <code>string</code>
  - [.meta](#home-meta) : <code>object</code>
    - [.pageMeta](#home-meta-pagemeta) : <code>object</code>
  - [.component()](#home-component) ⇒ <code>object</code>

<a name="home-path">
</a>

### Home.path : <code>string</code>

Route path

**Default**: <code>&quot;/&quot;</code>  
<a name="home-name">
</a>

### Home.name : <code>string</code>

Route name

**Default**: <code>&quot;Home&quot;</code>  
<a name="home-meta">
</a>

### Home.meta : <code>object</code>

Route meta

<a name="home-meta-pagemeta">
</a>

#### meta.pageMeta : <code>object</code>

Route page meta

**Properties**

| Name | Type | Description |
| --- | --- | --- |
| title | <code>function</code> | Return translated Home view title (translate: `views.Home.Index.title`) |

<a name="home-component">
</a>

### Home.component() ⇒ <code>object</code>

Route component. By default, returns `Core.context.vue.components.public.views.home.Index`

**Returns**: <code>object</code> - Returns the view component

