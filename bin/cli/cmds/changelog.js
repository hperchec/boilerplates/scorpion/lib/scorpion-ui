const juisy = require('@hperchec/juisy')

// Utils
const {
  $style,
  log,
  step,
  substep,
  run,
  error,
  wait
} = juisy.utils

// Exports command object
module.exports = {
  /**
   * Command syntax
   */
  command: 'changelog',
  /**
   * Aliases
   */
  aliases: [],
  /**
   * Command description
   */
  describe: 'Generate changelog file with conventional-changelog-cli',
  /**
   * Builder
   * @param {Object} yargs
   * @return {Object}
   */
  builder: function (yargs) {
    return yargs.option('i', {
      alias: 'infile',
      type: 'string',
      describe: 'Same as conventional-changelog option',
      default: 'CHANGELOG.md',
      requiresArg: true
    })
  },
  /**
   * Handler
   * @param {Object} argv - The argv
   * @return {void}
   */
  handler: async function (argv) {
    /**
     * Generate changelog file
     */
    step('Generating changelog')
    await wait('Generating', async () => {
      try {
        await run(
          'npx',
          [
            'conventional-changelog',
            '-p', 'angular',
            '-i', argv.infile,
            '-s' // same file to output
          ],
          { stdio: 'pipe' }
        )
      } catch (e) {
        error('Unable to generate changelog', e)
      }
    })
    substep($style.green('✔ Success'), { last: true })
    log() // Blank line
  }
}
