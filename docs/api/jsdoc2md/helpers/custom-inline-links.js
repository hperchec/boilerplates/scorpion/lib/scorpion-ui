const ddata = require('dmd/helpers/ddata')
const { slugifyAnchorIfRelativeUrl } = require('./slugify')

exports.customInlineLinks = customInlineLinks

/**
 * replaces {@link} tags with markdown links in the supplied input text
 * @param {string} text - The URL string
 * @param {Object} options - Handlebars options
 * @return {string}
 */
function customInlineLinks (text, options) {
  if (text) {
    var links = ddata.parseLink(text)
    links.forEach(function (link) {
      var linked = ddata._link(link.url, options)
      if (link.caption === link.url) link.caption = linked.name
      if (linked.url) link.url = linked.url
      text = text.replace(link.original, '[' + link.caption + '](' + slugifyAnchorIfRelativeUrl(link.url) + ')')
    })
  }
  return text
}
