/**
 * @vuepress
 * ---
 * title: "Service (event-bus) options"
 * headline: "Service (event-bus) options"
 * sidebarTitle: .options
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import toolbox from '@/toolbox'

const { makeConfigurableOption } = toolbox.services

/**
 * @name options
 * @static
 * @description
 * Accepts the following options:
 *
 * - `strictMode`
 *
 * Configurable options:
 *
 * - **strictMode**: `Core.config.services['event-bus'].strictMode`
 */
const options = {
  // ...
}

/**
 * strictMode
 * @name options.strictMode
 * @type {boolean}
 * @default true
 * @description
 * Define if EventBus throw Error when emitting event which has no listener defined.
 *
 * Can be overwritten via Core global configuration: `Core.config.services['event-bus'].strictMode`
 */
makeConfigurableOption(options, {
  propertyName: 'strictMode',
  serviceIdentifier: 'event-bus',
  configPath: 'strictMode',
  defaultValue: true
})

export default options
