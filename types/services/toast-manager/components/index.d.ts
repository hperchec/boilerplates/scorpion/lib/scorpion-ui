/// <reference path="../../../vue-shim.d.ts" />
declare namespace _default {
    export { BaseToast };
    export { BaseToaster };
    export { ExampleToaster };
    export { Toast };
}
export default _default;
import BaseToast from './BaseToast.vue';
import BaseToaster from './BaseToaster.vue';
import ExampleToaster from './ExampleToaster.vue';
import Toast from './Toast.vue';
//# sourceMappingURL=index.d.ts.map