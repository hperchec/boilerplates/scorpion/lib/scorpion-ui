---
title: getTimestamp
headline: getTimestamp
sidebarTitle: getTimestamp
---

# getTimestamp

<a name="gettimestamp">
</a>

## getTimestamp ⇒ <code>number</code>

Parses date and returns timestamp

**Returns**: <code>number</code> - Returns timestamp

| Param | Type | Description |
| --- | --- | --- |
| date | <code>Date</code> | The date to parse |

**Example**

```js
const date = new Date()getTimestamp(date) // => return time
```

