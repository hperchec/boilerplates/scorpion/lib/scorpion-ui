/**
 * @vuepress
 * ---
 * title: Service class
 * headline: Service class
 * sidebarTitle: Service
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: ./Core
 * next: ./ServiceDefinition
 * ---
 */

import ServiceDefinition from './ServiceDefinition'

import { isDef, typeCheck } from '@/utils'

const log = (...args) => { // eslint-disable-line no-unused-vars
  console.log(`[Service class] - ${args.shift()}`, ...args)
}
const error = (msg) => {
  throw new Error(`[Service class] - ${msg}`)
}

/**
 * @classdesc
 * Service class
 *
 * ```js
 * import { Service } from '@hperchec/scorpion-ui'
 * ```
 */
export class Service {
  /**
   * Service class
   * @param {object} serviceObject - The service object
   * @param {object} serviceObject.service - The service definition
   * @param {Function} [serviceObject.onInitialized] - A callback to add to Core "INITIALIZED" hook
   * @param {object} [serviceObject.options] - The service options object
   * @param {object} [serviceObject.expose] - The object to expose in service context
   */
  constructor (serviceObject) {
    // Process service definition
    const serviceDef = new ServiceDefinition(serviceObject.service)
    // Get 'expose' object
    const expose = serviceObject.expose || {}
    // Process service definition
    this.#definition = serviceDef
    // Initialized hook
    this.onInitialized = serviceObject.onInitialized
    // Options
    this.options = serviceObject.options
    // Get property names to exclude in 'expose' object
    const excludePropertyNames = Object.getOwnPropertyNames(Service.prototype)
    // Loop on 'expose' properties
    for (const prop of Object.keys(expose)) {
      if (excludePropertyNames.includes(prop)) error(`expose object can't have "${prop}" property`)
      this[prop] = expose[prop]
    }
  }

  /**
   * Accessors & mutators
   */

  /**
   * onInitialized
   * @category properties
   * @type {object}
   * @description
   * Service onInitialized
   */
  get onInitialized () {
    return this.#onInitialized
  }

  set onInitialized (value) {
    if (isDef(value)) {
      typeCheck(Function, value)
        ? this.#onInitialized = value
        : error(`onInitialized must be function, "${typeof value}" received`)
    }
  }

  /**
   * options
   * @category properties
   * @type {object}
   * @description
   * Service options
   */
  get options () {
    return this.#options
  }

  set options (value) {
    if (isDef(value)) {
      typeCheck(Object, value)
        ? this.#options = value
        : error(`options must be object, "${typeof value}" received`)
    }
  }

  /**
   * Methods
   */

  /**
   * @category methods
   * @returns {ServiceDefinition} Returns the service definition
   * @description
   * Returns the service definition
   */
  getDefinition () {
    return this.#definition
  }

  /**
   * @category methods
   * @returns {string[]} Returns the namespaced keys (from 'expose')
   * @description
   * Returns the namespaced keys (from 'expose')
   */
  getNamespacedProperties () {
    // Get property names to exclude
    const excludePropertyNames = Object.getOwnPropertyNames(Service.prototype)
    return Object.keys(this).filter((prop) => !excludePropertyNames.includes(prop))
  }

  /**
   * @category methods
   * @returns {object} Returns the exposed object
   * @description
   * Returns the exposed object
   */
  getNamespace () {
    return this.getNamespacedProperties().reduce((accumulator, value) => {
      accumulator[value] = this[value]
      return accumulator
    }, {})
  }

  /**
   * Static accessors & mutators
   */

  // ...

  /**
   * Static methods
   */

  /**
   * resolveServiceDef
   * @category static methods
   * @param {object} def - The service definition object to pass to ServiceDefinition constructor
   * @returns {ServiceDefinition} Returns the service definition
   * @description
   * Resolve a service definition
   */
  static resolveServiceDef (def) {
    return new ServiceDefinition(def)
  }

  /**
   * Private properties
   */

  /**
   * definition
   * @type {ServiceDefinition}
   * @default undefined
   */
  #definition = undefined

  /**
   * onInitialized
   * @type {Function}
   * @default undefined
   */
  #onInitialized = undefined

  /**
   * options
   * @type {object}
   * @default {}
   */
  #options = {}
}

// Export Service class
export default Service
