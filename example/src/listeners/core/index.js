import { Core } from '@hperchec/scorpion-ui'

/**
 * Do logic before use Vue plugin...
 */
// Core.onBeforeUseVuePlugin('<name>', (plugin, options) => {
//   // ...
// })

/**
 * Do logic after use Vue plugin...
 */
// Core.onAfterUseVuePlugin('<name>', (plugin, options) => {
//   // ...
// })

/**
 * Do logic before create service...
 */
// Core.onBeforeCreateService('<identifier>', (options, context, Core) => {
//   // ...
// })

/**
 * Service is created now. Do logic...
 */
// Core.onServiceCreated('<identifier>', (service) => {
//   // ...
// })

/**
 * Do something before app boot
 *
 * Services are created and accessible in this hook
 */
Core.onBeforeAppBoot(() => {
  // ...
})

/**
 * Do something when app boot
 *
 * Services are created and accessible in this hook.
 * ⚠ Vue root instance is not yet created and not assigned to window property
 */
Core.onAppBoot(async () => {
  // const [ authManager ] = [
  //   Core.service('auth-manager')
  // ]
  // // Check if user is authenticated
  // const checkAuthResponse = await authManager.check()
  // // Continue if no errors
  // if (!(checkAuthResponse instanceof Error)) {
  //   // If user is authenticated
  //   if (authManager.getCurrentUser()) {
  //     // ...
  //   }
  // }
})
