/**
 * @vuepress
 * ---
 * title: "Service (local-database-manager)"
 * headline: "Service (local-database-manager)"
 * sidebarTitle: local-database-manager
 * initialOpenGroupIndex: -1 # optional, defaults to 0, defines the index of initially opened subgroup
 * prev: ../
 * next: ./options
 * ---
 */

import Service from '@/Service'

import options from './options'
import LocalDatabase from './LocalDatabase'
import LocalDatabaseManager from './LocalDatabaseManager'
import LocalDatabaseOptions from './LocalDatabaseOptions'
import newLocalDatabase from './new-local-database'

/* eslint-disable jsdoc/require-param, jsdoc/require-returns */

/**
 * @scorpion_ui_service {LocalDatabaseManager} local-database-manager
 * @description
 * The service "local-database-manager" is designed to make indexedDB managemnent easier.
 *
 * It references any connection needed and provides methods and tools.
 */
export default new Service({
  // Service
  service: {
    // Identifier
    identifier: 'local-database-manager',
    // Register service
    register: function (_Core) {
      // ...
    },
    /**
     * @alias create
     * @description
     * Returns an instance of `LocalDatabaseManager` created with options.
     */
    create: (serviceOptions, { LocalDatabaseManager: _LocalDatabaseManager }, _Core) => {
      // Return new LocalDatabaseManager
      return new _LocalDatabaseManager(serviceOptions)
    },
    /**
     * @alias plugin
     * @description
     * The following properties will be set to Vue prototype:
     *
     * ```js
     * // Each references to Core.service('local-database-manager')
     * Vue.prototype.$localDatabaseManager
     * Vue.prototype.$localDB
     * ```
     */
    vuePlugin: ({ LocalDatabaseManager: _LocalDatabaseManager }, _Core) => _LocalDatabaseManager,
    // Vue plugin options
    pluginOptions: {},
    // rootOption
    rootOption: ({ LocalDatabaseManager: _LocalDatabaseManager }, _Core) => _LocalDatabaseManager.rootOption
  },
  /**
   * @alias onInitialized
   * @description
   * Once Core is initialized, it will define a `Logger` {@link ../logger/Logger#types type} as following:
   *
   * ```js
   * 'local-database': {
   *   badgeContent: '[INDEXEDDB]',
   *   badgeColor: '#D1C1E6',
   *   badgeBgColor: '#584576',
   *   messageColor: '#584576',
   *   prependMessage: '⛃'
   * }
   * ```
   */
  onInitialized: (Core) => {
    Core.onBeforeCreateService('logger', ({ types }) => {
      // Check if 'local-database' type is defined, else set it
      if (!types['local-database']) {
        types['local-database'] = {
          badgeContent: '[INDEXEDDB]',
          badgeColor: '#D1C1E6',
          badgeBgColor: '#584576',
          messageColor: '#584576',
          prependMessage: '⛃'
        }
      }
    })
  },
  // Options
  options,
  /**
   * @alias expose
   */
  expose: {
    /**
     * @type {object}
     * @description
     * Default is an empty object
     */
    databases: {},
    /**
     * @see {@link ./new-local-database newLocalDatabase}
     */
    newLocalDatabase,
    /**
     * @see {@link ./LocalDatabase LocalDatabase}
     */
    LocalDatabase,
    /**
     * @see {@link ./LocalDatabaseManager LocalDatabaseManager}
     */
    LocalDatabaseManager,
    /**
     * @see {@link ./LocalDatabaseOptions LocalDatabaseOptions}
     */
    LocalDatabaseOptions
  }
})
