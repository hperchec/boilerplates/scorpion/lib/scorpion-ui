---
title: "Service (websocket-manager): WebSocketManager class"
headline: "Service (websocket-manager): WebSocketManager class"
sidebarTitle: .WebSocketManager
sidebarDepth: 0
prev: false
next: false
---

# Service (websocket-manager): WebSocketManager class

<a name="websocketmanager">
</a>

## WebSocketManager ⇐ <code>VuePluginable</code>

Service **websocket-manager**: WebSocketManager class

**Extends**: <code>VuePluginable</code>  
**Schema**:

- [WebSocketManager](#websocketmanager) ⇐ <code>VuePluginable</code>
  - [new WebSocketManager(options)](#newwebsocketmanagernew)
  - _instance_
    - [.reactive](#websocketmanager-reactive)
    - [.toJSON()](#websocketmanager-tojson) ⇒ <code>object</code>
    - _methods_
      - [.add(connection)](#websocketmanager-add) ⇒ <code>module:services/websocket-manager/WSConnection~WSConnection</code>
      - [.use(connectionName)](#websocketmanager-use) ⇒ <code>module:services/websocket-manager/WSConnection~WSConnection</code>
    - _properties_
      - [.connections](#websocketmanager-connections) : <code>Array.&lt;module:services/websocket-manager/WSConnection~WSConnection&gt;</code>
  - _static_
    - [.install(_Vue, [options])](#websocketmanager-install)

<a name="newwebsocketmanagernew">
</a>

### new WebSocketManager(options)

Create a new WebSocket Manager

| Param | Type | Description |
| --- | --- | --- |
| options | <code>object</code> | Constructor options |
| options.connections | <code>Array.&lt;module:services/websocket-manager/WSConnection~WSConnection&gt;</code> | An array of WSConnection |

<a name="websocketmanager-reactive">
</a>

### webSocketManager.reactive

Overrides static VuePluginable properties

<a name="websocketmanager-tojson">
</a>

### webSocketManager.toJSON() ⇒ <code>object</code>

Overrides the default toJSON object method for JSON.stringify() calls

**Returns**: <code>object</code> - - The instance data to be serialized

<a name="websocketmanager-add">
</a>

### webSocketManager.add(connection) ⇒ <code>module:services/websocket-manager/WSConnection~WSConnection</code>

Add a connection to manager anytime

**Returns**: <code>module:services/websocket-manager/WSConnection~WSConnection</code> - Return this.use(<connection.name>)

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| connection | <code>module:services/websocket-manager/WSConnection~WSConnection</code> | The connection to add to manager |

<a name="websocketmanager-use">
</a>

### webSocketManager.use(connectionName) ⇒ <code>module:services/websocket-manager/WSConnection~WSConnection</code>

Use a specific connection that was loaded by the manager

**Returns**: <code>module:services/websocket-manager/WSConnection~WSConnection</code> - Return the connection

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| connectionName | <code>string</code> | The connection name to target |

<a name="websocketmanager-connections">
</a>

### webSocketManager.connections : <code>Array.&lt;module:services/websocket-manager/WSConnection~WSConnection&gt;</code>

Registered connections

**Category**: properties  
**Read only**: true  
<a name="websocketmanager-install">
</a>

### WebSocketManager.install(_Vue, [options])

Install method for Vue

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| _Vue | <code>Vue</code> |  | Vue |
| [options] | <code>object</code> | <code>{}</code> | The options of the plugin |

