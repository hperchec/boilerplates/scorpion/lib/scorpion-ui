---
title: get
headline: get
sidebarTitle: get
---

# get

<a name="get">
</a>

## get ⇒ <code>\*</code>

Get object property (lodash.get).::: tip See also[lodash.has](https://www.npmjs.com/package/lodash.has) package documentation:::

**Returns**: <code>\*</code> - Returns the target property value

| Param | Type | Description |
| --- | --- | --- |
| object | <code>object</code> | The object to query |
| path | <code>Array</code> \| <code>string</code> | The path of the property to get |
| [defaultValue] | <code>\*</code> | The value returned for undefined resolved values |

**Example**

```js
const object = { 'a': [{ 'b': { 'c': 3 } }] }get(object, 'a[0].b.c') // => 3
```

