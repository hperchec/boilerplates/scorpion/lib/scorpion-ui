/**
 * @vuepress
 * ---
 * title: deepMerge
 * headline: deepMerge
 * sidebarTitle: deepMerge
 * ---
 */

// Use 'deepmerge' lib
import merge from 'deepmerge'

/**
 * deepMerge
 * @param {object|Array} x - The source
 * @param {object|Array} y - The target
 * @param {object} [options = undefined] - Options
 * @returns {object|Array} Returns the result of merge
 * @description
 * Deep merge two Objects/Array
 *
 * ::: tip See also
 * [deepmerge](https://github.com/TehShrike/deepmerge) package documentation
 * :::
 * @example
 * const x = { foo: 'bar', quux: 1 }
 * const y = { foo: 'baz', other: [] }
 * deepMerge(x, y) // => { foo: 'baz', quux: 1, other: [] }
 */
export const deepMerge = function (x, y, options) {
  return merge(x, y, options)
}

export default deepMerge
