/**
 * @vuepress
 * ---
 * title: "Service (auth-manager) options"
 * headline: "Service (auth-manager) options"
 * sidebarTitle: Options
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * @name options
 * @typicalname Core.context.services['auth-manager'].options
 * @description
 * Authentication manager service options
 *
 * Accepts the following options:
 *
 * - `userModelClass`
 */
const options = {
  /**
   * userModelClass
   * @alias options.userModelClass
   * @type {Function}
   * @description
   * User model class. Default is undefined
   */
  userModelClass: undefined
}

export default options
