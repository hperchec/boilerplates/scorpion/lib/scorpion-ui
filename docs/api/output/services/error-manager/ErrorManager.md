---
title: "Service (error-manager): ErrorManager class"
headline: "Service (error-manager): ErrorManager class"
sidebarTitle: .ErrorManager
sidebarDepth: 0
prev: false
next: false
---

# Service (error-manager): ErrorManager class

<a name="errormanager">
</a>

## ErrorManager ⇐ <code>VuePluginable</code>

Service **error-manager**: ErrorManager class

**Extends**: <code>VuePluginable</code>  
**Schema**:

- [ErrorManager](#errormanager) ⇐ <code>VuePluginable</code>
  - [new ErrorManager(options)](#newerrormanagernew)
  - _instance_
    - [.reactive](#errormanager-reactive)
    - [.throw(error, [force])](#errormanager-throw) ⇒ <code>AppError</code>
    - [.initGlobalHandler()](#errormanager-initglobalhandler) ⇒ <code>void</code>
    - [.initVueHandler()](#errormanager-initvuehandler) ⇒ <code>void</code>
    - [.initDevHandler()](#errormanager-initdevhandler) ⇒ <code>void</code>
    - [.toJSON()](#errormanager-tojson) ⇒ <code>object</code>
    - _methods_
      - [.catch(errorConstructor)](#errormanager-catch) ⇒ <code>boolean</code>
      - [.addHandler(scope, handler)](#errormanager-addhandler) ⇒ <code>function</code>
      - [.removeHandler(scope, handler)](#errormanager-removehandler) ⇒ <code>boolean</code>
    - _properties_
      - [.scopes](#errormanager-scopes) : <code>Array.&lt;string&gt;</code>
  - _static_
    - [.install(_Vue, [options])](#errormanager-install)

<a name="newerrormanagernew">
</a>

### new ErrorManager(options)

Create a new ErrorManager

| Param | Type | Description |
| --- | --- | --- |
| options | <code>object</code> | Constructor options |

<a name="errormanager-reactive">
</a>

### errorManager.reactive

Overrides static VuePluginable properties

<a name="errormanager-throw">
</a>

### errorManager.throw(error, [force]) ⇒ <code>AppError</code>

Throw an AppError instance "silently"

**Returns**: <code>AppError</code> - The error instance itself

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| error | <code>AppError</code> |  | The AppError to throw "silently". It will not block the current execution loop |
| [force] | <code>boolean</code> | <code>false</code> | If false, error is not thrown again is isCaught returns true. If true, error will be thrown in all cases |

<a name="errormanager-initglobalhandler">
</a>

### errorManager.initGlobalHandler() ⇒ <code>void</code>

Init the global handler to global (window level) object

<a name="errormanager-initvuehandler">
</a>

### errorManager.initVueHandler() ⇒ <code>void</code>

Init the global handler to vue config object

<a name="errormanager-initdevhandler">
</a>

### errorManager.initDevHandler() ⇒ <code>void</code>

Init the global handler to toolbox.dev.runtime.errorHandlerSee https://webpack.js.org/configuration/dev-server/#overlay

<a name="errormanager-tojson">
</a>

### errorManager.toJSON() ⇒ <code>object</code>

Overrides the default toJSON object method for JSON.stringify() calls

**Returns**: <code>object</code> - - The instance data to be serialized

<a name="errormanager-catch">
</a>

### errorManager.catch(errorConstructor) ⇒ <code>boolean</code>

Catch a custom error at global level

**Returns**: <code>boolean</code> - Return true if the constructor has been added to custom errors to catch

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| errorConstructor | <code>Class.&lt;AppError&gt;</code> | The error constructor that extends AppError |

<a name="errormanager-addhandler">
</a>

### errorManager.addHandler(scope, handler) ⇒ <code>function</code>

Add an error handler to the given scope

**Returns**: <code>function</code> - Return the handler itself

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| scope | <code>string</code> | The scope to target |
| handler | <code>function</code> | The handler to add |

<a name="errormanager-removehandler">
</a>

### errorManager.removeHandler(scope, handler) ⇒ <code>boolean</code>

Remove an error handler from the given scope

**Returns**: <code>boolean</code> - True if listener was defined and removed, otherwise false

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| scope | <code>string</code> | The scope to target |
| handler | <code>function</code> | The handler to add |

<a name="errormanager-scopes">
</a>

### errorManager.scopes : <code>Array.&lt;string&gt;</code>

The scopes

**Category**: properties  
**Read only**: true  
<a name="errormanager-install">
</a>

### ErrorManager.install(_Vue, [options])

Install method for Vue

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| _Vue | <code>Vue</code> |  | Vue |
| [options] | <code>object</code> | <code>{}</code> | The options of the plugin |

