/**
 * @vuepress
 * ---
 * title: uppercase
 * headline: uppercase
 * sidebarTitle: uppercase
 * ---
 */

// Lodash.uppercase
import lodashUppercase from 'lodash.uppercase'

/**
 * uppercase
 * @param {string} str - The string to uppercase
 * @returns {string} The "uppercased" string
 * @description
 * Convert a string to uppercase *({@link ../context/vue/filters#filters-uppercase Defined as Vue filter})*.
 *
 * ::: tip See also
 * [lodash.uppercase](https://www.npmjs.com/package/lodash.uppercase) package documentation
 * :::
 * @example
 * uppercase('Foo Bar') // => 'FOO BAR'
 */
export const uppercase = function (str) {
  return lodashUppercase(str)
}

export default uppercase
