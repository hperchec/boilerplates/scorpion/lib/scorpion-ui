export default Toast;
declare class Toast {
    static get defaultOptions(): any;
    constructor(options: {
        toaster: Toaster;
        content: string | object;
        contentProps?: object;
        asyncHide?: Function;
        timeOut?: number;
        keepAlive?: boolean;
        noClose?: boolean;
        meta?: object;
    });
    private _id;
    _toaster: any;
    _content: any;
    _asyncHide: any;
    _timeOut: any;
    _timeOutID: any;
    _keepAlive: any;
    _noClose: any;
    set toaster(value: Toaster);
    get toaster(): Toaster;
    set content(value: any);
    get content(): any;
    set contentProps(value: any);
    get contentProps(): any;
    set asyncHide(value: Function);
    get asyncHide(): Function;
    set timeOut(value: number);
    get timeOut(): number;
    set keepAlive(value: boolean);
    get keepAlive(): boolean;
    set noClose(value: boolean);
    get noClose(): boolean;
    meta: any;
    readonly get id(): string;
    _contentProps: any;
    readonly get timeOutID(): number;
    setId(): void;
    init(): Toast;
    clearTimeOut(): void;
    delete(): void;
    toJSON(): object;
}
import Toaster from './Toaster';
//# sourceMappingURL=Toast.d.ts.map