/**
 * @vuepress
 * ---
 * title: "Service (logger) options"
 * headline: "Service (logger) options"
 * sidebarTitle: .options
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import toolbox from '@/toolbox'

const { makeConfigurableOption } = toolbox.services

/**
 * @name options
 * @static
 * @description
 * Same as Logger constructor options.
 *
 * Configurable options:
 *
 * - `log`: `Core.config.services['logger'].log`
 * - `types`: `Core.config.services['logger'].types`
 */
const options = {
  // ...
}

/**
 * log
 * @alias options.log
 * @type {boolean}
 * @description
 * Enable/Disable logger.
 *
 * Can be overwritten via Core global configuration: `Core.config.services['logger'].log`
 *
 * Default: true
 */
makeConfigurableOption(options, {
  propertyName: 'log',
  serviceIdentifier: 'logger',
  configPath: 'log',
  defaultValue: true
})

/**
 * types
 * @alias options.types
 * @type {object}
 * @description
 * Define types for Logger.
 *
 * Can be overwritten via Core global configuration: `Core.config.services['logger'].types`
 *
 * Default: `{}`
 */
makeConfigurableOption(options, {
  propertyName: 'types',
  serviceIdentifier: 'logger',
  configPath: 'types',
  defaultValue: {}
})

export default options
