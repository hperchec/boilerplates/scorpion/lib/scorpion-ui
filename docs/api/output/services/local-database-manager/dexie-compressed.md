---
title: dexie-compressed
headline: dexie-compressed
---

# dexie-compressed

<a name="applycompressionmiddleware">
</a>

## applyCompressionMiddleware(database, options) ⇒ <code>Dexie</code>

Apply compression middleware

**Returns**: <code>Dexie</code> - - The Dexie instance

| Param | Type | Description |
| --- | --- | --- |
| database | <code>Dexie</code> | The Dexie instance |
| options | <code>object</code> | The options object |
| [options.tables] | <code>object</code> | The table to compress |
| [options.defaultEngine] | <code>object</code> | The default engine option object |
| [options.defaultEngine.compress] | <code>function</code> | The default engine compress method |
| [options.defaultEngine.decompress] | <code>function</code> | The default engine decompress method |

