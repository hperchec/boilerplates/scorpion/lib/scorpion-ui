---
title: Components
headline: Components
sidebarTitle: Components
initialOpenGroupIndex: -1
prev: ../
next: ./commons/
---

# Components

<a name="components">
</a>

## components

Register all app components

**Schema**:

- [components](#components)
  - [.App](#components-app)
  - [.commons](#components-commons)

<a name="components-app">
</a>

### Core.context.vue.components.App

The main App component

**See**: [App](./App)

<a name="components-commons">
</a>

### Core.context.vue.components.commons

The common components (globally registered)

**See**: [commons](./commons)

