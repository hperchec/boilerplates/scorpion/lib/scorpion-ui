/**
 * Scorpion UI JSDoc plugin: specific behavior for services documentation
 * See also https://jsdoc.app/about-plugins.html
 */

const minimatch = require('minimatch')
const logger = require('jsdoc/util/logger')
const { Doclet, combine: docletCombine } = require('jsdoc/doclet')

// Cache for service file names
const serviceFiles = []

// Necessary top level doclets
const defaultTopLevelDoclets = {
  // Register
  register: new Doclet(`
    /**
     * @name register
     * @function
     * @returns {void}
     */
  `),
  // data.shared
  shared: new Doclet(`
    /**
     * @name shared
     * @typicalname vm
     * @type {object}
     */
  `),
  // $app
  $app: new Doclet(`
    /**
     * @name $app
     * @typicalname vm.$app
     * @type {object}
     */
  `),
  // user settings
  userSettings: new Doclet(`
    /**
     * @name userSettings
     * @typicalname vm.$app.userSettings
     * @memberof $app
     * @type {object}
     */
  `),
  // create
  create: new Doclet(`
    /**
     * @name create
     * @function
     * @returns {*}
     */
  `),
  // plugin
  plugin: new Doclet(`
    /**
     * @name plugin
     */
  `)
}

// Plugin dictionary
exports.defineTags = function (dictionary) {
  // Define @scorpion_ui_service tag
  // The tag must be always used as follow:
  // ex: @scorpion_ui_service {VueI18n} i18n
  dictionary.defineTag('scorpion_ui_service', {
    // Service can have type -> returned by created() method and under Core.service('<identifier>')
    canHaveType: true,
    canHaveName: true,
    // Tag muyst have value -> service identifier
    mustHaveValue: true,
    // On tagged, we consider the doclet is the root service doclet.
    // The type is the type returned by service "create" method, while the name is the service identifier
    onTagged: function (doclet, tag) {
      const identifier = tag.value.name
      doclet.longname = 'Service'
      doclet.name = 'service'
      doclet.kind = 'member'
      doclet.scope = 'global'
      doclet.description = `**Identifier**: \`${identifier}\`\n\n${doclet.description}`
      doclet.type = tag.value.type
      doclet.addTag('service_identifier', identifier)
    }
  })
}

// Plugin handlers
exports.handlers = {
  // When file begin, save filename if service file
  fileBegin (event) {
    if (minimatch(event.filename, '**/src/services/*/index.js')) {
      serviceFiles.push(event.filename)
    }
  },
  // When parsing is complete, if service file, check for
  // @scorpion_ui_service tag and injects necessary top level doclets
  parseComplete (event) {
    const filename = event.sourcefiles[0]
    // If service file
    if (serviceFiles.includes(filename)) {
      const serviceRootDoclet = event.doclets.find((value, index) => value.name === 'service')
      if (!serviceRootDoclet) {
        logger.error(`Error when parsing "${filename}". Tag "@scorpion_ui_service" not found.`)
        throw new Error(`Error when parsing "${filename}". Tag "@scorpion_ui_service" not found.`)
      }
      // Get service identifier
      const serviceIdentifier = serviceRootDoclet.tags.find((value) => value.title === 'service_identifier').value
      // Save temporary found index
      let foundIndex

      // Get user defined register doc block
      const registerDoclet = event.doclets.find((value, index) => {
        foundIndex = index
        return value.name === 'register'
      })
      if (registerDoclet) {
        event.doclets[foundIndex] = docletCombine(registerDoclet, defaultTopLevelDoclets.register)
      }
      foundIndex = undefined

      // Get user defined create doc block
      const createDoclet = event.doclets.find((value, index) => {
        foundIndex = index
        return value.name === 'create'
      })
      const finalCreateDoclet = createDoclet
        ? docletCombine(createDoclet, defaultTopLevelDoclets.create)
        : defaultTopLevelDoclets.create
      // Apply returns type based on service root doclet type
      finalCreateDoclet.returns[0].type = serviceRootDoclet.type
      finalCreateDoclet.returns[0].description = `Once services are created:\n\n\`\`\`js\nCore.service('${serviceIdentifier}')\n\`\`\``
      if (createDoclet) {
        event.doclets[foundIndex] = finalCreateDoclet
      } else {
        event.doclets.push(finalCreateDoclet)
      }
      foundIndex = undefined

      // Get user defined shared doc block
      const sharedDoclet = event.doclets.find((value, index) => {
        foundIndex = index
        return value.name === 'shared'
      })
      if (sharedDoclet) {
        // If set, remove it
        event.doclets.splice(foundIndex, 1)
      }
      // Inject
      event.doclets.push(defaultTopLevelDoclets.shared)
      foundIndex = undefined

      // Get user defined $app doc block
      const appDoclet = event.doclets.find((value, index) => {
        foundIndex = index
        return value.name === '$app'
      })
      if (appDoclet) {
        // If set, remove it
        event.doclets.splice(foundIndex, 1)
      }
      // Inject
      event.doclets.push(defaultTopLevelDoclets.$app)
      foundIndex = undefined

      // Get user defined $app.userSettings doc block
      const appUserSettingsDoclet = event.doclets.find((value, index) => {
        foundIndex = index
        return value.name === '$app.userSettings'
      })
      if (appUserSettingsDoclet) {
        // If set, remove it
        event.doclets.splice(foundIndex, 1)
      }
      // Inject
      event.doclets.push(defaultTopLevelDoclets.userSettings)
      foundIndex = undefined

      // Get user defined plugin doc block
      const pluginDoclet = event.doclets.find((value, index) => {
        foundIndex = index
        return value.name === 'plugin'
      })
      if (pluginDoclet) {
        event.doclets[foundIndex] = docletCombine(pluginDoclet, defaultTopLevelDoclets.plugin)
      }
      foundIndex = undefined
    }
  }
}
