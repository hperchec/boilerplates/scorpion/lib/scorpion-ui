import createModule from './create-module'
import generateActions from './generate-actions'
import generateGetters from './generate-getters'
import generateMutations from './generate-mutations'
import generateState from './generate-state'
import getResourceModule from './get-resource-module'
import getResourceDataManager from './get-resource-data-manager'

export default {
  createModule: createModule,
  generateActions: generateActions,
  generateGetters: generateGetters,
  generateMutations: generateMutations,
  generateState: generateState,
  getResourceDataManager: getResourceDataManager,
  getResourceModule: getResourceModule
}
