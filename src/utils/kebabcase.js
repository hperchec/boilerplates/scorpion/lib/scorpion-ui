/**
 * @vuepress
 * ---
 * title: kebabcase
 * headline: kebabcase
 * sidebarTitle: kebabcase
 * ---
 */

// Lodash.kebabcase
import lodashKebabcase from 'lodash.kebabcase'

/**
 * kebabcase
 * @param {string} str - The string to kebabcase
 * @returns {string} Returns the "kebabcased" string
 * @description
 * Convert a string to kebabcase *({@link ../context/vue/filters#filters-kebabcase Defined as Vue filter})*.
 *
 * ::: tip See also
 * [lodash.kebabcase](https://www.npmjs.com/package/lodash.kebabcase) package documentation
 * :::
 * @example
 * kebabcase('Foo Bar') // => 'foo-bar'
 */
export const kebabcase = function (str) {
  return lodashKebabcase(str)
}

export default kebabcase
