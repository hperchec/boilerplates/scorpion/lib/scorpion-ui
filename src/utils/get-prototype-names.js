/**
 * @vuepress
 * ---
 * title: getPrototypeNames
 * headline: getPrototypeNames
 * sidebarTitle: getPrototypeNames
 * ---
 */

// Utils
import getPrototypeChain from './get-prototype-chain'
import getConstructorName from './get-constructor-name'

/**
 * getPrototypeNames
 * @param {*} value - The value
 * @returns {string[]} Returns an array of constructor names
 * @description
 * Similar to getPrototypeChain() except that returns constructor names (strings)
 * @example
 * getPrototypeNames({ foo: 'bar' }) // => [ 'Object' ]
 */
export const getPrototypeNames = function (value) {
  return getPrototypeChain(value).map((proto) => {
    return getConstructorName(proto.constructor)
  })
}

export default getPrototypeNames
