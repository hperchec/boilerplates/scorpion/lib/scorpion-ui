export default Core;
declare class Core {
    static readonly get version(): string;
    static readonly get isInitialized(): boolean;
    static readonly get isSetup(): boolean;
    static readonly get Vue(): typeof import("vue/types/umd");
    static get config(): {
        globals: {
            APP_NAME: string;
            VERSION: {
                CURRENT: string;
            };
        };
        vue: {
            root: {
                name: string;
                targetElement: string;
                autoMount: boolean;
            };
        };
        services: {
            'api-manager': any;
            'event-bus': {
                strictMode: boolean;
            };
            'error-manager': {
                autoRegister: boolean;
            };
            i18n: {
                availableLocales: string[];
                defaultLocale: string;
                fallbackLocale: string;
            };
            'local-storage-manager': {
                setItemEventName: string;
                deleteItemEventName: string;
            };
            logger: {
                log: boolean;
                types: {
                    info: {
                        badgeContent: string;
                        badgeColor: string;
                        badgeBgColor: string;
                        messageColor: string;
                        prependMessage: string;
                    };
                    advice: {
                        badgeContent: string;
                        badgeColor: string;
                        badgeBgColor: string;
                        messageColor: string;
                        prependMessage: string;
                    };
                    warning: {
                        badgeContent: string;
                        badgeColor: string;
                        badgeBgColor: string;
                        messageColor: string;
                        prependMessage: string;
                    };
                    error: {
                        badgeContent: string;
                        badgeColor: string;
                        badgeBgColor: string;
                        messageColor: string;
                        prependMessage: string;
                    };
                    system: {
                        badgeContent: string;
                        badgeColor: string;
                        badgeBgColor: string;
                        messageColor: string;
                        prependMessage: string;
                    };
                    vue: {
                        badgeContent: string;
                        badgeColor: string;
                        badgeBgColor: string;
                        messageColor: string;
                        prependMessage: string;
                    };
                    router: {
                        badgeContent: string;
                        badgeColor: string;
                        badgeBgColor: string;
                        messageColor: string;
                        prependMessage: string;
                    };
                    store: {
                        badgeContent: string;
                        badgeColor: string;
                        badgeBgColor: string;
                        messageColor: string;
                        prependMessage: string;
                    };
                    websocket: {
                        badgeContent: string;
                        badgeColor: string;
                        badgeBgColor: string;
                        messageColor: string;
                        prependMessage: string;
                    };
                    'local-database': {
                        badgeContent: string;
                        badgeColor: string;
                        badgeBgColor: string;
                        messageColor: string;
                        prependMessage: string;
                    };
                };
            };
            'modal-manager': {
                baseZIndex: number;
            };
            router: {
                initOnRootInstanceMounted: boolean;
                catchAllRedirect: any;
            };
            'toast-manager': {
                toasters: {
                    Example: {};
                };
            };
            'websocket-manager': {
                connections: {};
            };
        };
    };
    static context: {
        models: {};
        readonly services: Record<"i18n" | "router" | "api-manager" | "event-bus" | "error-manager" | "local-storage-manager" | "logger" | "modal-manager" | "toast-manager" | "websocket-manager", any>;
        support: {
            collection: any;
            errors: any;
            model: {
                Model: typeof import("@/context/support/model/Model").default;
            };
        };
        vue: {
            components: any;
            directives: any;
            ensureComponentConstructor: Function;
            getComponentDescriptor: Function;
            filters: any;
            mixins: any;
            readonly plugins: any;
            root: any;
        };
    };
    static setFeatureDetector(definition: {
        name: string;
        supports: Function;
        failed?: Function;
    }): void;
    static init(_Vue: typeof Vue): void;
    static registerService(service: Service): void;
    static registerVuePlugin(name: string, plugin: object, options?: object): void;
    static configure(config: object): void;
    static resolveConfig(config?: object): object;
    static setup(): Promise<void>;
    static runApp(): Promise<Vue>;
    static getRunningApp(): Vue;
    static getRegisteredServices(): object;
    static hasServiceRegistered(identifier: string): boolean;
    static getPluginableServices(): object;
    static getCreatedServices(): object;
    static service(identifier: 'api-manager'): import('./services/api-manager/APIManager').default;
    static service(identifier: 'auth-manager'): import('./services/auth-manager/AuthManager').default;
    static service(identifier: 'device-manager'): import('./services/device-manager/DeviceManager').default;
    static service(identifier: 'error-manager'): import('@/services/error-manager/ErrorManager').default;
    static service(identifier: 'event-bus'): import('@/services/event-bus/EventBus').default;
    static service(identifier: 'i18n'): import('vue-i18n').default;
    static service(identifier: 'keyboard-event-manager'): import('@/services/keyboard-event-manager/KeyboardEventManager').default;
    static service(identifier: 'local-database-manager'): import('@/services/local-database-manager/LocalDatabaseManager').default;
    static service(identifier: 'local-storage-manager'): import('@/services/local-storage-manager/LocalStorageManager').default;
    static service(identifier: 'logger'): import('@/services/logger/Logger').default;
    static service(identifier: 'modal-manager'): import('@/services/modal-manager/ModalManager').default;
    static service(identifier: 'notification-manager'): import('@/services/notification-manager/NotificationManager').default;
    static service(identifier: 'router'): import('vue-router').default;
    static service(identifier: 'store'): typeof import("vuex");
    static service(identifier: 'toast-manager'): import('@/services/toast-manager/ToastManager').default;
    static service(identifier: 'websocket-manager'): import('@/services/websocket-manager/WebSocketManager').default;
    static service(identifier: string): any;
    static onInitialized(func: Function): void;
    static onBeforeUseVuePlugin(name: string, func: Function): void;
    static onAfterUseVuePlugin(name: string, func: Function): void;
    static onBeforeCreateService(identifier: string, func: Function): void;
    static onServiceCreated(identifier: string, func: Function): void;
    static onBeforeAppBoot(func: Function): void;
    static onAppBoot(func: Function): void;
    static info(): object;
    static "__#19@#IS_INITIALIZED": boolean;
    static "__#19@#IS_SETUP": boolean;
    static "__#19@#config": object;
    static "__#19@#Vue": typeof Vue;
    static "__#19@#featureDetectors": object;
    static "__#19@#services": object;
    static "__#19@#hooks": object;
    static "__#19@#serviceHooks": object;
    static "__#19@#vuePluginsHooks": object;
    static "__#19@#useVuePlugins"(): Promise<void>;
    static "__#19@#createServices"(): Promise<void>;
}
import Service from './Service';
//# sourceMappingURL=Core.d.ts.map