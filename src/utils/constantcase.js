/**
 * @vuepress
 * ---
 * title: constantcase
 * headline: constantcase
 * sidebarTitle: constantcase
 * ---
 */

import uppercase from './uppercase'
import snakecase from './snakecase'

/**
 * constantcase
 * @param {string} str - The string to constantcase
 * @returns {string} Returns the 'constantcased' string
 * @description
 * Convert a string to constantcase *({@link ../context/vue/filters#filters-constantcase Defined as Vue filter})*.
 * @example
 * constantcase('Foo Bar') // => 'FOO_BAR'
 */
export const constantcase = function (str) {
  return uppercase(snakecase(str))
}

export default constantcase
