/**
 * @vuepress
 * ---
 * title: snakecase
 * headline: snakecase
 * sidebarTitle: snakecase
 * ---
 */

// Lodash.snakecase
import lodashSnakecase from 'lodash.snakecase'

/**
 * snakecase
 * @param {string} str - The string to snakecase
 * @returns {string} Returns the "snakecased" string
 * @description
 * Convert a string to snakecase *({@link ../context/vue/filters#filters-snakecase Defined as Vue filter})*.
 *
 * ::: tip See also
 * [lodash.snakecase](https://www.npmjs.com/package/lodash.snakecase) package documentation
 * :::
 * @example
 * snakecase('Foo Bar') // => 'foo_bar'
 */
export const snakecase = function (str) {
  return lodashSnakecase(str)
}

export default snakecase
