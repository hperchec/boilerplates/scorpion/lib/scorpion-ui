---
title: Service (error-manager) options
headline: Service (error-manager) options
sidebarTitle: Options
sidebarDepth: 0
prev: false
next: false
---

# Service (error-manager) options

<a name="options">
</a>

## options

Same as ErrorManager constructor options.Configurable options:- `autoRegister`: `Core.config.services['error-manager'].autoRegister`

