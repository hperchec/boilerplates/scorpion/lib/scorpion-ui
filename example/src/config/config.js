/* eslint-disable quote-props */

// Globals
import GLOBALS from '../../globals.config.json'

/**
 * Core global configuration
 */
const config = {
  /**
   * App globals
   */
  globals: GLOBALS,
  /**
   * Vue configuration
   */
  vue: {
    /**
     * Vue root instance configuration
     */
    rootInstance: {
      /**
       * The name of the component and the window property that will be assigned (`window.<name>`).
       * The value will be assigned to Vue root instance `name` option in `beforeCreate` hook.
       */
      name: '__ROOT_VUE_INSTANCE__',
      /**
       * The element (selector) on which to mount Vue root instance.
       * The value will be assigned to Vue root instance `el` option in `beforeCreate` hook.
       */
      targetElement: '#app',
      /**
       * Defines if the Vue root instance is auto mounted
       */
      autoMount: true
    }
  },
  /**
   * Services configuration
   */
  services: {
    /**
     * Built-in 'error-manager' service default configuration
     */
    'error-manager': {
      // Auto register all error classes/constructors under `Core.context.support.errors`
      autoRegister: true
    },
    /**
     * Built-in 'i18n' service configuration
     */
    'i18n': {
      /**
       * Available locales
       */
      availableLocales: [ 'en', 'fr' ],
      /**
       * Default locale
       */
      defaultLocale: 'fr',
      /**
       * Fallback locale
       */
      fallbackLocale: 'fr'
    },
    /**
     * Built-in 'local-database-manager' service default configuration
     */
    'local-database-manager': {
      /**
       * Custom property to add configuration to local databases
       */
      databases: {
        // 'my-database': {
        //   /**
        //    * Stored encrypted DEK (in browser LocalStorage)
        //    */
        //   storedEncryptedDEKKey: 'client_encrypted_data_encryption_key'
        // }
      }
    },
    /**
     * Built-in 'logger' service default configuration
     */
    'logger': {
      // ...
    },
    /**
     * Built-in 'router' service configuration
     */
    'router': {
      /**
       * Auto trigger routing on root instance mounted hook
       */
      initOnRootInstanceMounted: true,
      /**
       * The route to redirect in _CatchAll route before hook.
       */
      catchAllRedirect: '/404'
    },
    /**
     * Built-in 'toast-manager' service configuration
     */
    'toast-manager': {
      toasters: {
        /**
         * Define "Mainframe" toaster
         */
        Mainframe: {}
      }
    },
    /**
     * Built-in 'websocket-manager' service default configuration
     */
    'websocket-manager': {
      connections: {
        // ServerWS: {
        //   appKey: 'my-app-key',
        //   cluster: 'my-cluster',
        //   forceTLS: false,
        //   wsHost: 'localhost',
        //   wsPort: 6001,
        //   // wssPort: 6001,
        //   wsPath: '',
        //   authEndpoint: 'http://localhost:8000/broadcasting/auth',
        //   auth: {
        //     headers: {
        //       // 'X-CSRF-Token': 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
        //       'X-App-ID': 'my-app-id'
        //     }
        //   },
        //   enabledTransports: [ 'ws', 'flash' ]
        // }
      }
    }
  },
  /**
   * System
   */
  system: {
    // ...
  },
  /**
   * Authentication
   */
  auth: {
    /**
     * oAuth access token identifier (in browser LocalStorage)
     * @default 'app_access_token'
     * @type {string}
     */
    accessTokenKey: 'app_access_token',
    /**
     * oAuth refresh token identifier (in browser LocalStorage)
     * @default 'app_refresh_token'
     * @type {string}
     */
    refreshTokenKey: 'app_refresh_token'
  }
}

export default config
