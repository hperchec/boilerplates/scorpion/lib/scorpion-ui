# BaseToast

**BaseToast** component

> This component is globally registered by Vue

## Props

| Prop name | Description | Type | Values | Default | Origin |
| - | - | - | - | - | - |
| toast | toast | `Toast` | | null | |
| noClose | noClose | `boolean` | | false | |

