---
title: Service (api-manager) configuration
headline: Service (api-manager) configuration
sidebarTitle: Configuration
prev: false
next: false
---

# Service (api-manager) configuration

<a name="configuration">
</a>

## Configuration

Service configuration> Refers to `Core.config.services['api-manager']````js'api-manager': {  // API definitions  // Overwrites service option: `apis`  apis: {    // <name> is the api name and the value is an api options object    // (see APIOptions constructor third param)    '<name>': Object,    ...  }}```

<a name="configuration-apis">
</a>

### Core.config.services[&#x27;api-manager&#x27;].apis : <code>object</code>

API definitions. An object following the schema:```js{  [key: String]: Object}```Object keys are the API names while values are Object like APIoptions constructor third param.

