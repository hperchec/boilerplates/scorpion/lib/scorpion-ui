/**
 * @vuepress
 * ---
 * title: "Service (notification-manager) options"
 * headline: "Service (notification-manager) options"
 * sidebarTitle: Options
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import toolbox from '@/toolbox'

const { makeConfigurableOption } = toolbox.services

/**
 * @name options
 * @typicalname Core.context.services['notification-manager'].options
 * @description
 * Same as NotificationManager constructor options.
 *
 * Configurable options:
 *
 * - `autoRegister`: `Core.config.services['notification-manager'].autoRegister`
 */
const options = {
  // ...
}

/**
 * autoRegister
 * @alias options.autoRegister
 * @type {boolean}
 * @description
 * Automatically creates error handlers for each class/constructor under `Core.context.support.errors`.
 *
 * Can be overwritten via Core global configuration: `Core.config.services['notification-manager'].autoRegister`
 *
 * Default: true
 */
makeConfigurableOption(options, {
  propertyName: 'autoRegister',
  serviceIdentifier: 'notification-manager',
  configPath: 'autoRegister',
  defaultValue: true
})

export default options
