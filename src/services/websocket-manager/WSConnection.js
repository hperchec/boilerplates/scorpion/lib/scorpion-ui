/**
 * @vuepress
 * ---
 * title: "Service (websocket-manager): WSConnection class"
 * headline: "Service (websocket-manager): WSConnection class"
 * sidebarTitle: .WSConnection
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import Pusher from 'pusher-js'
import Core from '@/Core'
import { camelcase, getAllKeys, typeCheck, deepMerge } from '@/utils'
import WSConnectionOptions from './WSConnectionOptions' // jsDoc type

/**
 * @param  {...any} args - See i18n method
 * @returns {string}
 */
// @ts-ignore
const log = (...args) => Core.service('logger').consoleLog(...args)

/**
 * @classdesc
 * Service **websocket-manager**: WSConnection class
 */
class WSConnection {
  /**
   * @private
   */
  _name
  _options
  _driver
  _methodsMap
  _connection = null
  _listeners = {
    'state-change': [
      states => {
        log(`${this.name}: state changed: ` + states.current, { type: 'websocket' })
      }
    ],
    connecting: [
      () => {
        log(`${this.name}: connecting...`, { type: 'websocket' })
      }
    ],
    connected: [
      () => {
        log(`${this.name}: connected ✔`, { type: 'websocket' })
      }
    ],
    disconnected: [
      () => {
        log(`${this.name}: disconnected ❌`, { type: 'websocket' })
      }
    ],
    failed: [
      () => {
        log(`${this.name}: failed ❌`, { type: 'websocket' })
      }
    ],
    unavailable: [
      () => {
        log(`${this.name}: unavailable ❌`, { type: 'websocket' })
      }
    ],
    error: [
      event => {
        log(`${this.name}: an error has occured ❗`, { type: 'websocket' })
        console.log(event.error)
      }
    ],
    subscribe: [
      {
        success: (channelName, channel) => {
          log(`${this.name}: subscribed to channel "${channelName}"`, { type: 'websocket' })
        },
        error: (channelName, channel, error) => {
          log(`${this.name}: unable to subscribe to channel "${channelName}"`, { type: 'websocket' })
          console.log(error)
        }
      }
    ],
    unsubscribe: [
      (channelName, channel) => {
        log(`${this.name}: unsubscribe from channel "${channelName}"`, { type: 'websocket' })
      }
    ]
  }

  /**
   * Create a new WSConnection
   * @param {WSConnectionOptions} wsConnectionOptions - An WSConnectionOptions object
   */
  constructor (wsConnectionOptions) {
    if (typeCheck(Core.context.services['websocket-manager'].WSConnectionOptions, wsConnectionOptions)) {
      this.name = wsConnectionOptions.name
      this.options = wsConnectionOptions.options
      this.setDriver(wsConnectionOptions.driver)
    } else {
      throw new TypeError('WSConnection class : constructor failed. WSConnectionOptions object attempted, received : ' + typeof wsConnectionOptions)
    }
  }

  /**
   * Accessors & mutators
   */

  /**
   * WSConnection name
   * @category properties
   * @type {string}
   */
  get name () {
    return this._name
  }

  set name (value) {
    // Check type
    if (typeCheck(String, value)) {
      // Assign
      this._name = value
    } else {
      // Else -> throw error
      throw new Error('WSConnection class: \'name\' property must be String. ' + typeof value + ' received...')
    }
  }

  /**
   * WSConnection options
   * @category properties
   * @type {string}
   */
  get options () {
    return this._options
  }

  set options (value) {
    // Check type
    if (typeCheck(Object, value)) {
      // Assign
      this._options = value
    } else {
      // Else -> throw error
      throw new Error('WSConnection class: \'options\' property must be Object. ' + typeof value + ' received...')
    }
  }

  /**
   * Get the WSConnection driver
   * @category properties
   * @readonly
   * @type {object}
   */
  get driver () {
    return this._driver
  }

  /**
   * Methods
   */

  /**
   * setDriver
   * @category methods
   * @param {string} name - The driver name
   * @param {object} [options] - The driver options/configuration
   * @returns {void}
   * @description
   * Set the driver for the WSConnection
   */
  setDriver (name, options) {
    if (WSConnection.supportedDrivers.indexOf(name) > -1) {
      this[camelcase(`init_${name}`)](options || this.options)
    }
  }

  /**
   * mapDriverMethods
   * @category methods
   * @param {object} methods - A methods object
   * @returns {void}
   * @description
   * Map driver methods for requests
   *
   * ```js
   * {
   *   connect: methods.connect, // this.connect([<options>])
   *   disconnect: methods.disconnect, // this.disconnect([<options>])
   *   subscribe: methods.subscribe, // this.subscribe(<channelName>, [<successCb>, <errorCb>])
   *   unsubscribe: methods.unsubscribe // this.unsubscribe(<channelName>)
   * }
   * ```
   */
  mapDriverMethods (methods) {
    const genError = (val) => { throw new Error('WSConnection class : unmapped or unsupported driver method "' + val + '"') }
    this._methodsMap = {
      connect: methods.connect || function () { genError('connect') },
      disconnect: methods.disconnect || function () { genError('disconnect') },
      subscribe: methods.subscribe || function () { genError('subscribe') },
      unsubscribe: methods.unsubscribe || function () { genError('unsubscribe') }
    }
  }

  /**
   * getConnection
   * @category methods
   * @returns {*} The connection reference
   * @description
   * getConnection method: return the connection reference
   */
  getConnection () {
    return this._connection
  }

  /**
   * connect
   * @category methods
   * @param {object} [options = {}] - Will be merged with instance options
   * @returns {*} The driver returned value
   * @description
   * Main connect method: etablish connection with websocket server
   */
  connect (options = {}) {
    // First, merge options
    options = deepMerge(this.options, options)
    // Call the corresponding method mapped to the driver equivalent
    return this._methodsMap.connect(options) // Must return a promise
  }

  /**
   * disconnect
   * @category methods
   * @param {object} [options = {}] - The specific options for the driver
   * @returns {*} The driver returned value
   * @description
   * Main disconnect method: unetablish connection with websocket server
   */
  disconnect (options = {}) {
    // Call the corresponding method mapped to the driver equivalent
    return this._methodsMap.disconnect(options) // Must return a promise
  }

  /**
   * @category methods
   * @param {string} eventName - The event name
   * @param {Function|object} eventCb - The event callback
   * @returns {void}
   * @description
   * Do something when the event is triggered
   */
  on (eventName, eventCb) {
    return this._listeners[eventName].push(eventCb)
  }

  /**
   * @category methods
   * @param {string} channelName - The channel name
   * @param {?Function} [successCb = null] - The callback to call in case of success
   * @param {?Function} [errorCb = null] - The callback to call in case of error
   * @returns {*} - The driver method returned value
   * @description
   * Do something when the channel is subscribed
   */
  subscribe (channelName, successCb = null, errorCb = null) {
    return this._methodsMap.subscribe(channelName, successCb, errorCb)
  }

  /**
   * @category methods
   * @param {string} channelName - The channel name
   * @returns {*} - The driver method returned value
   * @description
   * Do something when the channel is unsubscribed
   */
  unsubscribe (channelName) {
    return this._methodsMap.unsubscribe(channelName)
  }

  /**
   * initPusher
   * @category methods
   * @returns {void}
   * @description
   * Init pusher as driver
   */
  initPusher () {
    // Assign driver
    this._driver = Pusher // Pusher class
    // Map
    this.mapDriverMethods({
      /**
       * Connect method
       * @ignore
       * @param {object} options - The payload for the driver
       * @param {string} options.appKey - Pusher constructor first argument
       * @returns {Pusher} - Pusher instance
       */
      connect: (options) => {
        const { appKey, ..._options } = options
        log(`${this.name}: etablish connection ⏳`, { type: 'websocket' })
        const pusher = this._connection = new this._driver(appKey, _options)
        // Init default listeners
        this._listeners['state-change'].forEach((eventCb) => pusher.connection.bind('state_change', eventCb))
        this._listeners.connecting.forEach((eventCb) => pusher.connection.bind('connecting', eventCb))
        this._listeners.connected.forEach((eventCb) => pusher.connection.bind('connected', eventCb))
        this._listeners.disconnected.forEach((eventCb) => pusher.connection.bind('disconnected', eventCb))
        this._listeners.failed.forEach((eventCb) => pusher.connection.bind('failed', eventCb))
        this._listeners.unavailable.forEach((eventCb) => pusher.connection.bind('unavailable', eventCb))
        this._listeners.error.forEach((eventCb) => pusher.connection.bind('error', eventCb))
        return pusher
      },
      /**
       * Disconnect method
       * @ignore
       * @param {object} options - The payload for the driver
       * @returns {Pusher} - Pusher instance
       */
      disconnect: (options) => {
        const pusher = this._connection
        pusher.disconnect()
        return pusher
      },
      /**
       * Subscribe method
       * @ignore
       * @param {string} channelName - The channel
       * @param {?Function} [successCb = null] - The callback to call in case of success
       * @param {?Function} [errorCb = null] - The callback to call in case of error
       * @returns {object} - The channel object
       */
      subscribe: (channelName, successCb = null, errorCb = null) => {
        const pusher = this._connection
        const pusherChannel = pusher.subscribe(channelName)
        this._listeners.subscribe.forEach(({ success, error }) => pusherChannel.bind('pusher:subscription_succeeded', () => {
          if (success) success(channelName, pusherChannel)
        }))
        if (successCb) pusherChannel.bind('pusher:subscription_succeeded', successCb)
        this._listeners.subscribe.forEach(({ success, error }) => pusherChannel.bind('pusher:subscription_error', (err) => {
          if (error) error(channelName, pusherChannel, err)
        }))
        if (errorCb) pusherChannel.bind('pusher:subscription_error', errorCb)
        return pusherChannel
      },
      /**
       * Unsubscribe method
       * @ignore
       * @param {string} channelName - The channel
       * @returns {object} - The channel object
       */
      unsubscribe: (channelName) => {
        const pusher = this._connection
        const pusherChannel = pusher.unsubscribe(channelName)
        this._listeners.unsubscribe.forEach((eventCb) => eventCb(channelName, pusherChannel))
        return pusherChannel
      }
    })
  }

  /**
   * Overrides the default toJSON object method for JSON.stringify() calls
   * @returns {object} - The instance data to be serialized
   */
  toJSON () {
    const keys = getAllKeys(this, {
      excludeKeys: [
        /^_/, // private properties that starts with '_'
        'toJSON',
        'toString',
        'constructor'
      ]
    })
    const jsonObj = keys.reduce((obj, key) => {
      obj[key] = this[key]
      return obj
    }, {})
    return jsonObj
  }

  /**
   * Static
   */

  /**
   * supportedDrivers
   * @category properties
   * @readonly
   * @type {string[]}
   * @description
   * Only 'pusher' is supported as a driver for the moment
   */
  static get supportedDrivers () {
    return [ 'pusher' ] // (No more supported 'drivers' for the moment...)
  }
}

export default WSConnection
