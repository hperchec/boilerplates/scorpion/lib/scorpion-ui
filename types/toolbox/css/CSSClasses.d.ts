export default CSSClasses;
declare class CSSClasses {
    static parseCssClassValue(value: string | string[] | object): object;
    static extractClassesFromStr(classesStr: string): string[];
    static objectifyClassesArray(classes: string[]): object;
    static sanitizeClassesValue(str: string): string;
    static mergeCssClasses(...args: any[]): object;
    constructor(classes: string | string[] | object);
    toArray(): string[];
    toString(): string;
    toObject(): object;
    #private;
}
//# sourceMappingURL=CSSClasses.d.ts.map