export { VariantableOptions };
export namespace Variantable {
    namespace style {
        let fixedClasses: {};
        let classes: {};
        let variants: {};
    }
    namespace props {
        namespace variant {
            export let type: StringConstructor;
            let _default: string;
            export { _default as default };
        }
        let vStyle: (object | Function) | null;
    }
    namespace computed {
        function overriddenStyle(): VariantableOptions;
    }
    namespace methods {
        /**
         * getElementCssClass
         * @public
         * @param {string} element - The element (optional, null/undefined is root element)
         * @param {object} [options = {}] - The options
         * @param {string} [options.format = 'object'] - The returned format: can be 'string', 'array' or 'object' (default: 'object')
         * @param {Function} [options.visit] - A visitor function
         * @returns {string|string[]|object} The classes
         * @description
         * Get classes for an element.
         *
         * If the component does not have children elements:
         *
         * ```js
         * style: {
         *   fixedClasses: 'p-1 m-1',
         *   classes: 'text-white',
         *   variants: {
         *     myVariant: 'text-gray'
         *   }
         * }
         * ```
         *
         * Assum we didn't have specify variant, call method without argument:
         *
         * ```js
         * this.getElementCssClass() // => 'p-1 m-1 text-white'
         * ```
         *
         * With children elements:
         *
         * ```js
         * style: {
         *   fixedClasses: { wrapper: 'p-1 m-1' },
         *   classes: { wrapper: 'text-white' },
         *   variants: {
         *     myVariant: { wrapper: 'text-gray' }
         *   }
         * }
         * ```
         *
         * In that case, call method by specifying element name:
         *
         * ```js
         * this.getElementCssClass('wrapper') // => 'p-1 m-1 text-white'
         * this.getElementCssClass() // => error =(
         * ```
         */
        function getElementCssClass(element: string, options?: {
            format?: string;
            visit?: Function;
        }): any;
    }
    function beforeCreate(): void;
}
export default Variantable;
import VariantableOptions from './VariantableOptions';
//# sourceMappingURL=index.d.ts.map