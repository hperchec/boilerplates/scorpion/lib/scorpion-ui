export default WebSocketManager;
declare const WebSocketManager_base: any;
declare class WebSocketManager extends WebSocketManager_base {
    [x: string]: any;
    static reactive: boolean;
    static rootOption: string;
    static aliases: string[];
    static install(_Vue: import("vue/types/umd"), options?: object): void;
    constructor(options: {
        connections: WSConnection[];
    });
    readonly get connections(): WSConnection[];
    add(connection: WSConnection): WSConnection;
    use(connectionName: string): WSConnection;
    toJSON(): object;
}
import WSConnection from './WSConnection';
//# sourceMappingURL=WebSocketManager.d.ts.map