---
title: Service (local-storage-manager) configuration
headline: Service (local-storage-manager) configuration
sidebarTitle: Configuration
prev: false
next: false
---

# Service (local-storage-manager) configuration

<a name="configuration">
</a>

## Configuration

Service configuration> Refers to `Core.config.services['local-storage-manager']````js'local-storage-manager': {  // Name of event that is dispatched when local storage item is set  setItemEventName: String,  // Name of event that is dispatched when local storage item is removed  deleteItemEventName: String,  // Callback to execute when item is set  onSetItem: Function | Function[],  // Callback to execute when item is removed  onRemoveItem: Function | Function[]}```

**Schema**:

- [Configuration](#configuration)
  - [.setItemEventName](#configuration-setitemeventname) : <code>string</code>
  - [.deleteItemEventName](#configuration-deleteitemeventname) : <code>string</code>
  - [.onSetItem](#configuration-onsetitem) : <code>function</code> \| <code>Array.&lt;function()&gt;</code>
  - [.onRemoveItem](#configuration-onremoveitem) : <code>function</code> \| <code>Array.&lt;function()&gt;</code>

<a name="configuration-setitemeventname">
</a>

### Core.config.services[&#x27;local-storage-manager&#x27;].setItemEventName : <code>string</code>

Name of event that is dispatched when local storage item is set

<a name="configuration-deleteitemeventname">
</a>

### Core.config.services[&#x27;local-storage-manager&#x27;].deleteItemEventName : <code>string</code>

Name of event that is dispatched when local storage item is removed

<a name="configuration-onsetitem">
</a>

### Core.config.services[&#x27;local-storage-manager&#x27;].onSetItem : <code>function</code> \| <code>Array.&lt;function()&gt;</code>

Callback to execute when item is set

<a name="configuration-onremoveitem">
</a>

### Core.config.services[&#x27;local-storage-manager&#x27;].onRemoveItem : <code>function</code> \| <code>Array.&lt;function()&gt;</code>

Callback to execute when item is removed

