---
title: Service (keyboard-event-manager) options
headline: Service (keyboard-event-manager) options
sidebarTitle: Options
sidebarDepth: 0
prev: false
next: false
---

# Service (keyboard-event-manager) options

<a name="options">
</a>

## options

Keyboard events manager service options

