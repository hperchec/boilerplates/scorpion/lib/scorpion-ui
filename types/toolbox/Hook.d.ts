export default Hook;
declare class Hook {
    constructor(name: string, queue?: Function[]);
    readonly get name(): string;
    readonly get queue(): Function[];
    setName(value: string): void;
    setQueue(value: Function[]): void;
    append(func: Function | Function[]): void;
    exec(...args: any[]): any[] | Promise<any[]>;
    compose(...args: any[]): any[] | Promise<any[]>;
    chain(...args: any[]): any[] | Promise<any[]>;
    #private;
}
//# sourceMappingURL=Hook.d.ts.map