/**
 * @vuepress
 * ---
 * title: "app.utils"
 * headline: "app.utils"
 * sidebarTitle: "app.utils"
 * initialOpenGroupIndex: -1 # optional, defaults to 0, defines the index of initially opened subgroup
 * prev: ../
 * ---
 */

import Core from '@/Core'
import { isVueComponent as _isVueComponent } from '@/utils'

/**
 * $app.utils.vue
 * @type {object}
 */
const vue = {
  /**
   * $app.utils.vue.isVueComponent
   * @param {*} value - The value to check
   * @returns {boolean} Returns boolean
   * @description
   * See "isVueComponent" utils documentation
   */
  isVueComponent (value) {
    return _isVueComponent(value)
  },
  /**
   * $app.utils.vue.ensureComponentConstructor
   * Ensure that target is a Vue component constructor.
   * If VNode passed, wrap it inside a functional component that just returns the VNode as is.
   * @param {import("vue").VNode|import("vue").Component|object} target - The target component
   * @returns {?import("vue").Component} Returns the component constructor. If error, returns undefined.
   * @description
   * See `Core.context.vue.ensureComponentConstructor`
   */
  ensureComponentConstructor (target) {
    return Core.context.vue.ensureComponentConstructor(target)
  }
}

// Object module
export default {
  vue
}
