/**
 * @vuepress
 * ---
 * title: "Service (local-storage-manager) configuration"
 * headline: "Service (local-storage-manager) configuration"
 * sidebarTitle: Configuration
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * ⚠ THIS FILE IS ONLY USED TO GENERATE SERVICE CONFIGURATION DOCUMENTATION
 */

/**
 * @name Configuration
 * @typicalname Core.config.services['local-storage-manager']
 * @description
 * Service configuration
 *
 * > Refers to `Core.config.services['local-storage-manager']`
 *
 * ```js
 * 'local-storage-manager': {
 *   // Name of event that is dispatched when local storage item is set
 *   setItemEventName: String,
 *   // Name of event that is dispatched when local storage item is removed
 *   deleteItemEventName: String,
 *   // Callback to execute when item is set
 *   onSetItem: Function | Function[],
 *   // Callback to execute when item is removed
 *   onRemoveItem: Function | Function[]
 * }
 * ```
 */
const config = { // eslint-disable-line no-unused-vars
  /**
   * @alias Configuration.setItemEventName
   * @type {string}
   * @description
   * Name of event that is dispatched when local storage item is set
   */
  setItemEventName: undefined,
  /**
   * @alias Configuration.deleteItemEventName
   * @type {string}
   * @description
   * Name of event that is dispatched when local storage item is removed
   */
  deleteItemEventName: 'en',
  /**
   * @alias Configuration.onSetItem
   * @type {Function|Function[]}
   * @description
   * Callback to execute when item is set
   */
  onSetItem: undefined,
  /**
   * @alias Configuration.onRemoveItem
   * @type {Function|Function[]}
   * @description
   * Callback to execute when item is removed
   */
  onRemoveItem: undefined
}
