---
title: find
headline: find
sidebarTitle: find
---

# find

<a name="find">
</a>

## find ⇒ <code>boolean</code>

Find an item in array/collection::: tip See also[lodash.find](https://www.npmjs.com/package/lodash.find) package documentation:::

**Returns**: <code>boolean</code> - Returns the found object or undefined

| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | Same args as lodash find method |

