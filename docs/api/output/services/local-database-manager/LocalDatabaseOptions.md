---
title: "Service (local-database-manager): LocalDatabaseOptions class"
headline: "Service (local-database-manager): LocalDatabaseOptions class"
sidebarTitle: .LocalDatabaseOptions
sidebarDepth: 0
prev: false
next: false
---

# Service (local-database-manager): LocalDatabaseOptions class

<a name="localdatabaseoptions">
</a>

## LocalDatabaseOptions

Service **local-database-manager**: LocalDatabaseOptions class

**Schema**:

- [LocalDatabaseOptions](#localdatabaseoptions)
  - [new LocalDatabaseOptions(name, [options])](#newlocaldatabaseoptionsnew)
  - [.toJSON()](#localdatabaseoptions-tojson) ⇒ <code>object</code>
  - _properties_
    - [.name](#localdatabaseoptions-name) : <code>string</code>
    - [.driver](#localdatabaseoptions-driver) : <code>string</code>
    - [.driverOptions](#localdatabaseoptions-driveroptions) : <code>object</code>
    - [.init](#localdatabaseoptions-init) : <code>function</code>

<a name="newlocaldatabaseoptionsnew">
</a>

### new LocalDatabaseOptions(name, [options])

Create new LocalDatabaseOptions

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| name | <code>string</code> |  | The LocalDatabase name |
| [options] | <code>object</code> | <code>{}</code> | An options object |
| [options.driver] | <code>string</code> | <code>&quot;&#x27;dexie&#x27;&quot;</code> | The library to use in LocalDatabase.supportedDrivers |
| [options.driverOptions] | <code>object</code> |  | An options object that the driver needs to configure itself |
| [options.init] | <code>function</code> |  | A function to call when "init" method of LocalDatabase instance is called (takes LocalDatabase instance as unique parameter) |

<a name="localdatabaseoptions-tojson">
</a>

### localDatabaseOptions.toJSON() ⇒ <code>object</code>

Overrides the default toJSON object method for JSON.stringify() calls

**Returns**: <code>object</code> - - The instance data to be serialized

<a name="localdatabaseoptions-name">
</a>

### localDatabaseOptions.name : <code>string</code>

LocalDatabase name

**Category**: properties  
<a name="localdatabaseoptions-driver">
</a>

### localDatabaseOptions.driver : <code>string</code>

LocalDatabase driver name

**Category**: properties  
<a name="localdatabaseoptions-driveroptions">
</a>

### localDatabaseOptions.driverOptions : <code>object</code>

Driver options

**Category**: properties  
<a name="localdatabaseoptions-init">
</a>

### localDatabaseOptions.init : <code>function</code>

Init callback

**Category**: properties  
