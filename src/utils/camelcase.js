/**
 * @vuepress
 * ---
 * title: camelcase
 * headline: camelcase
 * sidebarTitle: camelcase
 * ---
 */

// Lodash.camelcase
import lodashCamelcase from 'lodash.camelcase'

/**
 * camelcase
 * @param {string} str - The string to camelcase
 * @returns {string} Returns the "camelcased" string
 * @description
 * Convert a string to camelcase *({@link ../context/vue/filters#filters-camelcase Defined as Vue filter})*.
 *
 * ::: tip See also
 * [lodash.camelcase](https://www.npmjs.com/package/lodash.camelcase) package documentation
 * :::
 * @example
 * camelcase('Foo Bar') // => 'fooBar'
 */
export const camelcase = function (str) {
  return lodashCamelcase(str)
}

export default camelcase
