---
title: appendPrototypeChain
headline: appendPrototypeChain
sidebarTitle: appendPrototypeChain
---

# appendPrototypeChain

<a name="appendprototypechain">
</a>

## appendPrototypeChain ⇒ <code>void</code>

Function inspired from https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Object/setPrototypeOf#ajouter_une_cha%C3%AEne_de_prototypes_%C3%A0_un_objet

| Param | Type | Description |
| --- | --- | --- |
| oChain | <code>\*</code> | The target object |
| oProto | <code>\*</code> | The prototype to add to chain |

