export default ErrorManager;
declare const ErrorManager_base: any;
declare class ErrorManager extends ErrorManager_base {
    [x: string]: any;
    static reactive: boolean;
    static rootOption: string;
    static aliases: any[];
    static install(_Vue: import("vue/types/umd"), options?: object): void;
    constructor(options: object);
    readonly get scopes(): string[];
    catch(errorConstructor: typeof AppError): boolean;
    throw(error: AppError, force?: boolean): AppError;
    addHandler(scope: string, handler: Function): Function;
    removeHandler(scope: string, handler: Function): boolean;
    initGlobalHandler(): void;
    initVueHandler(): void;
    initDevHandler(): void;
    toJSON(): object;
    #private;
}
import AppError from '@/context/support/errors/AppError';
//# sourceMappingURL=ErrorManager.d.ts.map