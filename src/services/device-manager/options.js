/**
 * @vuepress
 * ---
 * title: "Service (device-manager) options"
 * headline: "Service (device-manager) options"
 * sidebarTitle: Options
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * @name options
 * @typicalname Core.context.services['device-manager'].options
 * @description
 * Device manager service options
 */
const options = {
  // ...
}

export default options
