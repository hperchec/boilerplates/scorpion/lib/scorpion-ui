/**
 * @classdesc
 * Service **api-manager**: RequestError class
 */
export class RequestError extends AppError {
    /**
     * Static methods
     */
    /**
     * Do something when custom error is caught by ErrorManager
     * @param {*} error - The error instance
     * @returns {void}
     */
    static caught(error: any): void;
    /**
     * Creates a RequestError instance
     * @param {AxiosError|RequestError|Error} original - The original AxiosError or a RequestError instance
     * @param {API} apiInstance - The instance of API
     * @param {string} method - The method name (ex: GET, POST...)
     * @param {string} url - The URL to request
     * @param {object} requestOptions - The API request method options
     */
    constructor(original: AxiosError | RequestError | Error, apiInstance: API, method: string, url: string, requestOptions: object);
    /**
     * @private
     */
    private _apiInstance;
    _method: any;
    _url: any;
    _requestOptions: any;
    _request: any;
    _response: {
        payload: {};
        status: any;
        original: any;
    };
    original: Error;
    set apiInstance(value: API);
    /**
     * Accessors and mutators
     */
    /**
     * API instance
     * @category properties
     * @type {API}
     */
    get apiInstance(): API;
    set method(value: string);
    /**
     * Request method
     * @category properties
     * @type {string}
     */
    get method(): string;
    set url(value: string);
    /**
     * Request url
     * @category properties
     * @type {string}
     */
    get url(): string;
    set requestOptions(value: string);
    /**
     * Request options
     * @category properties
     * @type {string}
     */
    get requestOptions(): string;
    errorMessage: any;
    set request(value: XMLHttpRequest);
    /**
     * Request object
     * @category properties
     * @type {XMLHttpRequest}
     */
    get request(): XMLHttpRequest;
    /**
     * Response object
     * @category properties
     * @readonly
     * @type {object}
     */
    readonly get response(): any;
    /**
     * Methods
     */
    /**
     * Check if request error has response
     * @returns {boolean} Returns true if the request has response, otherwise false
     */
    hasResponse(): boolean;
    /**
     * Converts to the target custom error class
     * @param {typeof RequestError} customClass - The custom error class
     * @returns {*} Returns the instance of custom class
     */
    toTypedError(customClass: typeof RequestError): any;
}
export default RequestError;
import AppError from '@/context/support/errors/AppError';
import API from './API';
import { AxiosError } from 'axios';
//# sourceMappingURL=RequestError.d.ts.map