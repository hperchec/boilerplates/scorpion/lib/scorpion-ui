---
title: PublicRoute class
headline: PublicRoute class
sidebarTitle: .PublicRoute
sidebarDepth: 0
prev: false
next: false
---

# PublicRoute class

<a name="publicroute">
</a>

## PublicRoute ⇐ <code>module:services/router/Route~Route</code>

Class that represents a *public* route

**Extends**: <code>module:services/router/Route~Route</code>  
<a name="newpublicroutenew">
</a>

### new PublicRoute(...args)

Create a PrivateRouteSee also [Route](../Route)

| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | Same arguments as parent class |

