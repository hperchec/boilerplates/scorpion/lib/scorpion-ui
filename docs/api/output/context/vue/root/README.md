---
title: Root instance
headline: Root instance
sidebarTitle: Root instance
initialOpenGroupIndex: -1
prev: ../plugins/
next: ./prototype/
---

# Root instance

<a name="root">
</a>

## root : <code>object</code>

Vue root instance specific data/options...

**Schema**:

- [root](#root) : <code>object</code>
  - [.options](#root-options)
  - [.prototype](#root-prototype)
  - [.setOptions(mixin)](#root-setoptions) ⇒ <code>void</code>

<a name="root-options">
</a>

### Core.context.vue.root.options

**See**: [options](./options)

<a name="root-prototype">
</a>

### Core.context.vue.root.prototype

**See**: [prototype](./prototype)

<a name="root-setoptions">
</a>

### Core.context.vue.root.setOptions(mixin) ⇒ <code>void</code>

Extend Vue root instance options by passing mixin.

| Param | Type | Description |
| --- | --- | --- |
| mixin | <code>object</code> | Root instance mixin |

