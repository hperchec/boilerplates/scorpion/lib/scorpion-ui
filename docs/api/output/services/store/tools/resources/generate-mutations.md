---
title: generate-mutations
headline: generate-mutations
---

# generate-mutations

## Functions

<dl>
<dt><a href="#INSERT">INSERT(state, payload)</a> ⇒ <code>void</code></dt>
<dd><p>Mutate state.collection</p>
</dd>
<dt><a href="#UPDATE">UPDATE(state, payload)</a> ⇒ <code>void</code></dt>
<dd><p>Mutate state.collection</p>
</dd>
<dt><a href="#UPSERT">UPSERT(state, payload)</a> ⇒ <code>void</code></dt>
<dd><p>Mutate state.collection</p>
</dd>
<dt><a href="#DELETE">DELETE(state, key)</a> ⇒ <code>void</code></dt>
<dd><p>Mutate state.collection</p>
</dd>
</dl>

<a name="insert">
</a>

## INSERT(state, payload) ⇒ <code>void</code>

Mutate state.collection

| Param | Type | Description |
| --- | --- | --- |
| state | <code>object</code> | vuex store state |
| payload | <code>\*</code> | The item to push |

<a name="update">
</a>

## UPDATE(state, payload) ⇒ <code>void</code>

Mutate state.collection

| Param | Type | Description |
| --- | --- | --- |
| state | <code>object</code> | vuex store state |
| payload | <code>object</code> | Mutation payload |

<a name="upsert">
</a>

## UPSERT(state, payload) ⇒ <code>void</code>

Mutate state.collection

| Param | Type | Description |
| --- | --- | --- |
| state | <code>object</code> | vuex store state |
| payload | <code>Array.&lt;object&gt;</code> \| <code>object</code> | Mutation payload |

<a name="delete">
</a>

## DELETE(state, key) ⇒ <code>void</code>

Mutate state.collection

| Param | Type | Description |
| --- | --- | --- |
| state | <code>object</code> | vuex store state |
| key | <code>function</code> \| <code>number</code> \| <code>string</code> | The key to find item to delete in collection |

