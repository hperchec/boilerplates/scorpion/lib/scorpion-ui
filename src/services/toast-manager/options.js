/**
 * @vuepress
 * ---
 * title: "Service (toast-manager) options"
 * headline: "Service (toast-manager) options"
 * sidebarTitle: Options
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import Core from '@/Core'
import toolbox from '@/toolbox'
import Toaster from './Toaster' // jsDoc type

const { makeConfigurableOption } = toolbox.services

/**
 * @name options
 * @description
 * Toast manager service options
 */
const options = {
  //
}

/**
 * toasters
 * @alias options.toasters
 * @type {Toaster[]}
 * @default []
 * @description
 * Array of Toaster instance to pass to ToastManager constructor
 *
 * > 🔧 Configurable option: `Core.config.services['toast-manager'].toasters`. See service {@link ./config configuration}
 *
 * Default: `[]`
 */
makeConfigurableOption(options, {
  propertyName: 'toasters',
  serviceIdentifier: 'toast-manager',
  configPath: 'toasters',
  formatter: (toasterDefs) => {
    const tmp = []
    const Toaster = Core.context.services['toast-manager'].Toaster
    const Collection = Core.context.support.collection.Collection
    for (const name in toasterDefs) {
      // Create a new Toaster
      const toaster = new Toaster({
        name: name,
        ...toasterDefs[name],
        toasts: new Collection([], {
          // Defines 'id' property as item key
          findBy: function (key, collection) {
            return collection.find(toast => toast.id === key)
          }
        })
      })
      // Add custom toaster
      tmp.push(toaster)
    }
    return tmp
  },
  defaultValue: []
})

export default options
