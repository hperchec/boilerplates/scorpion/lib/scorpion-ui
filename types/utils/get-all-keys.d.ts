export function getAllKeys(obj: any, options?: {
    excludeKeys?: Array<string | RegExp>;
    includeSelf?: boolean;
    includePrototypeChain?: boolean;
    includeTop?: boolean;
    includeEnumerables?: boolean;
    includeNonenumerables?: boolean;
    includeStrings?: boolean;
    includeSymbols?: boolean;
}): string[];
export default getAllKeys;
//# sourceMappingURL=get-all-keys.d.ts.map