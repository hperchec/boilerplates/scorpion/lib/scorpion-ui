---
title: Service (keyboard-event-manager)
headline: Service (keyboard-event-manager)
sidebarTitle: keyboard-event-manager
initialOpenGroupIndex: -1
prev: ../
next: ./options
---

# Service (keyboard-event-manager)

<a name="service"></a>

The service "keyboard-event-manager" is designed to make authentication managemnent easier.

## Create

Returns an instance of `KeyboardEventManager` created with options.

**Returns**: <code>module:services/keyboard-event-manager/KeyboardEventManager~KeyboardEventManager</code> - Once services are created:

```js
Core.service('keyboard-event-manager')
```

## Vue

### Plugin

On registering, service Vue plugin will be set in `Core.context.vue.plugins['keyboard-event-manager']`.

Plugin will be automatically used by Vue.

The following properties will be set to Vue prototype:```jsVue.prototype.$keyboardEventManager // => Core.service('keyboard-event-manager')```

### Root instance

<a name="app"></a>

#### Prototype $app property

On registering, the service will inject properties to \`Core.context.vue.root.prototype.app\`.

> Properties will be shared by all components under \`vm.$app\` where \`vm\` is the component instance.

<a name="app-usersettings"></a>

##### User settings

Service will define the following user settings keys:

##### Others

## Expose (service context)

The service will expose the following properties under `Core.context.services['keyboard-event-manager']`:
<a name="expose-keyboardeventmanager">
</a>

### .KeyboardEventManager

> Exposed as `Core.context.services['keyboard-event-manager'].KeyboardEventManager`

**See**: [KeyboardEventManager](./KeyboardEventManager)

