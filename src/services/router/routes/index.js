/**
 * @vuepress
 * ---
 * title: Router - routes
 * headline: Router - routes
 * sidebarTitle: .routes
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import privateRoutes from './private'
import publicRoutes from './public'

/**
 * @name routes
 * @static
 * @type {object}
 */
export default {
  /**
   * @alias routes.private
   * @type {object}
   * @see {@link ./private private}
   * @description Private routes
   */
  private: privateRoutes,
  /**
   * @alias routes.public
   * @type {object}
   * @see {@link ./public public}
   * @description Public routes
   */
  public: publicRoutes
}
