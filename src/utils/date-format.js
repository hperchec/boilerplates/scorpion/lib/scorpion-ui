/**
 * @vuepress
 * ---
 * title: dateFormat
 * headline: dateFormat
 * sidebarTitle: dateFormat
 * ---
 */

// dateformat
import dateformat from 'dateformat'

/**
 * dateFormat
 * @param {Date} date - The date to format
 * @param {string} format - The target format
 * @returns {string} Returns the formatted date as string
 * @description
 * Format a date *({@link ../context/vue/filters#filters-dateformat Defined as Vue filter})*.
 *
 * ::: tip See also
 * [dateformat](https://www.npmjs.com/package/dateformat) package documentation
 * :::
 * @example
 * dateFormat(new Date(), 'yyyy-mm-dd') // => '1900-01-01'
 */
export const dateFormat = function (date, format) {
  return dateformat(date, format)
}

export default dateFormat
