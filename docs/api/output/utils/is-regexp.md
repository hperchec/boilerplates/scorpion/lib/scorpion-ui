---
title: isRegExp
headline: isRegExp
sidebarTitle: isRegExp
---

# isRegExp

<a name="isregexp">
</a>

## isRegExp ⇒ <code>boolean</code>

Check if value is a RegExp

**Returns**: <code>boolean</code> - Returns true if value is RegExp

| Param | Type | Description |
| --- | --- | --- |
| value | <code>\*</code> | The value to check |

**Example**

```js
isRegExp(/^Hello$/) // => trueisRegExp('Hello') // => false
```

