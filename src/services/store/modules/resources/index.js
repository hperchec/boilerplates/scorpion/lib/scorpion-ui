/**
 * Store.state.Resources
 */
export default {
  /**
   * @private
   */
  namespaced: true,
  /**
   * modules
   * @alias module:store.modules
   * @type {object}
   * @protected
   * @description Vuex Store option: store modules
   */
  modules: {
    // ...
  },
  /**
   * state
   * @alias module:store.state
   * @type {object}
   * @readonly
   * @description Vuex Store option. See syntax for accessor.
   */
  state: {
    // ...
  },
  /**
   * getters
   * @alias module:store.getters
   * @type {object}
   * @readonly
   * @description Vuex Store option. See syntax for accessor.
   */
  getters: {
    // ...
  },
  /**
   * actions
   * @alias module:store.actions
   * @type {object}
   * @protected
   * @description Vuex Store option. See syntax for accessor.
   */
  actions: {
    /**
     * Set loading status
     * @param {import("vuex").ActionContext} context - The action context
     * @param {object} payload - The payload
     * @param {string} payload.namespace - The resource modeul namespace (ex: "Users")
     * @param {string} payload.loadingName - The loading name as defined in state.loadingStatus
     * @param {boolean} payload.status - The status to set
     */
    setLoading ({ commit }, { namespace, loadingName, status }) {
      const toto = this.getResourceModule(namespace)
      console.log('toto : ', toto)
      const loadingMutation = [ namespace, 'SET_LOADING_STATUS' ].join('/')
      commit(loadingMutation, { name: loadingName, status: status }, { root: true })
    }
  },
  /**
   * mutations
   * @alias module:store.mutations
   * @type {object}
   * @protected
   * @description Vuex Store option. See syntax for accessor.
   */
  mutations: {
    // ...
  }
}
