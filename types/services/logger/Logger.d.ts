export default Logger;
declare const Logger_base: any;
declare class Logger extends Logger_base {
    [x: string]: any;
    static rootOption: string;
    static aliases: any[];
    static defaultOptions: {
        log: boolean;
        types: object;
    };
    static install(_Vue: import("vue/types/umd"), options?: object): void;
    constructor(options?: {
        log?: boolean;
        types?: object;
    });
    readonly get log(): boolean;
    readonly get types(): any;
    setType(name: string, options?: {
        badgeContent?: string;
        badgeColor?: string;
        badgeBgColor?: string;
        timeColor?: string;
        messageColor?: string;
        prependMessage?: string;
    }): void;
    consoleLog(message: string, options?: {
        type?: string;
        dev?: boolean;
        prod?: boolean;
    }): void;
    consoleLogTest(message?: string): void;
    toJSON(): object;
    #private;
}
declare namespace Logger { }
//# sourceMappingURL=Logger.d.ts.map