/**
 * @vuepress
 * ---
 * title: "Service (modal-manager)"
 * headline: "Service (modal-manager)"
 * sidebarTitle: modal-manager
 * initialOpenGroupIndex: -1 # optional, defaults to 0, defines the index of initially opened subgroup
 * prev: ../
 * next: ./options
 * ---
 */

import Service from '@/Service'

import components from './components'
import options from './options'
import Modal from './Modal'
import ModalManager from './ModalManager'

/* eslint-disable jsdoc/require-param, jsdoc/require-returns */

/**
 * @scorpion_ui_service {ModalManager} modal-manager
 * @description
 * The service "modal-manager" is designed to make authentication managemnent easier.
 */
export default new Service({
  // Service
  service: {
    // Identifier
    identifier: 'modal-manager',
    /**
     * @alias register
     * @description
     * On registering, the service will set root instance options:
     * at created hook, `ModalContainer` component is automatically add to root component.
     *
     * It injects properties to Vue root instance prototype `$app`:
     *
     * - `$app.ui.showModal`
     * - `$app.ui.hideModal`
     *
     * It will also define Vue components:
     *
     * - `Core.context.vue.components.modals.BaseModal`
     * - `Core.context.vue.components.modals.ModalContainer`
     * - `Core.context.vue.components.modals.ModalOverlay`
     * - `Core.context.vue.components.modals.Modal`
     */
    register: function (_Core) {
      // Define vue root instance prototype in '$app' namespace
      const $app = _Core.context.vue.root.prototype.app
      /**
       * @alias $app.ui.showModal
       * @param {string} name - The modal name
       * @param {object} props - The props to pass to the modal component
       * @returns {void}
       * @description
       * Show a modal
       * @example
       * // In any component
       * this.$app.ui.showModal('MyModal', {
       *   foo: 'bar'
       * })
       */
      $app.ui.showModal = function (name, props) {
        const root = this // 'this' is the Vue root instance
        root.$modalManager.show(name, props)
      }
      /**
       * @alias $app.ui.hideModal
       * @param {string} name - The modal name
       * @returns {void}
       * @description
       * Hide a modal
       * @example
       * // In any component
       * this.$app.ui.hideModal('MyModal')
       */
      $app.ui.hideModal = function (name) {
        const root = this // 'this' is the Vue root instance
        root.$modalManager.hide(name)
      }
      // Expose vue components
      _Core.context.vue.components.modals = {
        BaseModal: components.modals.BaseModal,
        ModalContainer: components.modals.ModalContainer,
        ModalOverlay: components.modals.ModalOverlay,
        Modal: components.modals.Modal
      }
    },
    /**
     * @alias create
     * @description
     * Returns an instance of `ModalManager` created with options.
     */
    create: (serviceOptions, { ModalManager: _ModalManager }, _Core) => {
      // Return new ModalManager
      return new _ModalManager(serviceOptions)
    },
    /**
     * @alias plugin
     * @description
     * The following properties will be set to Vue prototype:
     *
     * ```js
     * Vue.prototype.$modalManager // => Core.service('modal-manager')
     * Vue.prototype.$modals // => Core.service('modal-manager')
     * ```
     */
    vuePlugin: ({ ModalManager: _ModalManager }, _Core) => _ModalManager,
    // Vue plugin options
    pluginOptions: {},
    // rootOption
    rootOption: ({ ModalManager: _ModalManager }, _Core) => _ModalManager.rootOption
  },
  // On initialized
  onInitialized: (Core) => {
    // ...
  },
  // Options
  options,
  /**
   * @alias expose
   */
  expose: {
    /**
     * @see {@link ./Modal Modal}
     */
    Modal,
    /**
     * @see {@link ./ModalManager ModalManager}
     */
    ModalManager
  }
})
