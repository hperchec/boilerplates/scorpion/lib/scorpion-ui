---
title: error-handler
headline: error-handler
---

# error-handler

<a name="errorhandler">
</a>

## errorHandler ⇒ <code>boolean</code>

The global runtime error handler (called by webpack-dev-server `client.overlay.runtimeErrors` defined method)

**Returns**: <code>boolean</code> - Must return boolean or undefined

| Param | Type | Description |
| --- | --- | --- |
| error | <code>Error</code> | The error instance |

