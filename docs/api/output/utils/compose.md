---
title: compose
headline: compose
sidebarTitle: compose
---

# compose

<a name="compose">
</a>

## compose ⇒ <code>\*</code>

Compose functions. If at least one returned value is a promise, so the entire result will be promisyfied

**Returns**: <code>\*</code> - The returned value of compose functions

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| funcs | <code>Array.&lt;function()&gt;</code> |  | The array of function to compose |
| [args] | <code>Array.&lt;\*&gt;</code> | <code>[]</code> | The array of arguments to pass |

**Example**

```js
compose([ (str) => [ str.toUpperCase() ], (str) => [ `_${str}` ] ], [ 'foo' ]) // => '_FOO'compose([ (str) => [ str.toUpperCase() ], (str) => new Promise((resolve) => resolve([ `_${str}` ])) ], [ 'foo' ]) // => Promise resolved with '_FOO'
```

