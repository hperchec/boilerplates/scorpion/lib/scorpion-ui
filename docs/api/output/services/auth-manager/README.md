---
title: Service (auth-manager)
headline: Service (auth-manager)
sidebarTitle: auth-manager
initialOpenGroupIndex: -1
prev: ../
next: ./options
---

# Service (auth-manager)

<a name="service"></a>

The service "auth-manager" is designed to make authentication managemnent easier.

## Register

On registering, the service will inject properties to Vue root instance prototype `$app.auth`:- `$app.auth.user`

## Create

Returns an instance of `Authmanager` created with options.

**Returns**: <code>module:services/auth-manager/AuthManager~AuthManager</code> - Once services are created:

```js
Core.service('auth-manager')
```

## Vue

### Plugin

On registering, service Vue plugin will be set in `Core.context.vue.plugins['auth-manager']`.

Plugin will be automatically used by Vue.

The following properties will be set to Vue prototype:```jsVue.prototype.$authManager // => Core.service('auth-manager')```

### Root instance

<a name="app"></a>

#### Prototype $app property

On registering, the service will inject properties to \`Core.context.vue.root.prototype.app\`.

> Properties will be shared by all components under \`vm.$app\` where \`vm\` is the component instance.

<a name="app-usersettings"></a>

##### User settings

Service will define the following user settings keys:

##### Others

## Expose (service context)

The service will expose the following properties under `Core.context.services['auth-manager']`:
<a name="expose-authmanager">
</a>

### .AuthManager

> Exposed as `Core.context.services['auth-manager'].AuthManager`

**See**: [AuthManager](./AuthManager)

