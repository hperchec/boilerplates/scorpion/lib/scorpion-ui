export default Modal;
declare class Modal {
    static props: object;
    static getDefaultProps(): object;
    constructor(name: string, component: import("vue").Component, props?: object);
    _manager: any;
    _name: any;
    _component: any;
    _props: {};
    _vm: any;
    set name(value: string);
    get name(): string;
    set component(value: any);
    get component(): any;
    readonly get props(): any;
    readonly get isShown(): boolean;
    readonly get isHidden(): boolean;
    readonly get vm(): any;
    show(props?: object): void;
    hide(): void;
    toJSON(): object;
    toString(): string;
    #private;
}
//# sourceMappingURL=Modal.d.ts.map