/**
 * @vuepress
 * ---
 * title: "Service (modal-manager) options"
 * headline: "Service (modal-manager) options"
 * sidebarTitle: Options
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import Modal from './Modal'
import toolbox from '@/toolbox'

const { makeConfigurableOption } = toolbox.services

/**
 * @name options
 * @typicalname Core.context.services['modal-manager'].options
 * @description
 * Modal manager service options
 *
 * Configurable options:
 *
 * - **baseZIndex**: `Core.config.services['modal-manager'].baseZIndex`
 */
const options = {
  /**
   * modals
   * @alias options.modals
   * @type {Modal[]}
   * @description
   * The modals to pass to ModalManager constructor
   */
  modals: [],
  /**
   * useTransition
   * @alias options.useTransition
   * @type {?object}
   * @description
   * If defined, a props object to pass to Vue built-in `<transition-group>` component
   * to wrap each ModalOverlay component.
   */
  useTransition: undefined
}

/**
 * baseZIndex
 * @name options.baseZIndex
 * @type {number}
 * @default 500
 * @description
 * Define the bbase zIndex for modal overlay
 *
 * Can be overwritten via Core global configuration: `Core.config.services['modal-manager'].baseZIndex`
 */
makeConfigurableOption(options, {
  propertyName: 'baseZIndex',
  serviceIdentifier: 'modal-manager',
  configPath: 'baseZIndex',
  defaultValue: 500
})

export default options
