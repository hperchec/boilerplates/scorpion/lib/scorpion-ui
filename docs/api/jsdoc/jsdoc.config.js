'use strict'

const docsConfig = require('../docs.config')

/**
 * See https://jsdoc.app/about-configuring-jsdoc.html
 * and plugin: https://gitlab.com/hperchec/jsdoc-plugin-define
 */
module.exports = {
  plugins: [
    'node_modules/@hperchec/jsdoc-plugin-define',
    './plugins/scorpion-ui-service',
    './plugins/sidebar-title',
    'jsdoc-plugin-typescript'
  ],
  pluginOptions: {
    define: {
      globals: {
        VUEPRESS_BASE_PATH: docsConfig.vuepress.basePath,
        VUEPRESS_TARGET_PATH: docsConfig.vuepress.targetPath
      }
    }
  },
  typescript: {
    moduleRoot: 'src'
  }
}
