export default {
  /**
   * Default mutation name
   * @type {string}
   * @default SET_LOADING_STATUS
   */
  mutationName: 'SET_LOADING_STATUS'
}
