---
title: safeAsync
headline: safeAsync
sidebarTitle: safeAsync
---

# safeAsync

<a name="safeasync">
</a>

## safeAsync ⇒ <code>Promise.&lt;\*&gt;</code>

Encapsulate in a try/catch block a async function to execute

**Returns**: <code>Promise.&lt;\*&gt;</code> - Returns the value returned by `func`, else Error

| Param | Type | Description |
| --- | --- | --- |
| func | <code>function</code> | The function to execute |

**Example**

```js
safeAsync(function () {  // Fetch API})
```

