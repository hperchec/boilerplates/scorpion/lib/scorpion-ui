export default AppError;
declare class AppError extends Error {
    static errorCode: string;
    constructor(original?: (Error | string) | null);
    private _errorCode;
    _errorMessage: string;
    set errorCode(value: string);
    get errorCode(): string;
    set errorMessage(value: string);
    get errorMessage(): string;
    init(): void;
    toJSON(): object;
}
//# sourceMappingURL=AppError.d.ts.map