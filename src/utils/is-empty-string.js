/**
 * @vuepress
 * ---
 * title: isEmptyString
 * headline: isEmptyString
 * sidebarTitle: isEmptyString
 * ---
 */

/**
 * isEmptyString
 * @param {string} str - The string
 * @returns {boolean} Returns true if string is empty
 * @description
 * Check if the string is empty
 * @example
 * isEmptyString('') // true
 * isEmptyString('foo bar') // false
 */
export const isEmptyString = function (str) {
  return str
    ? !(str.length > 0)
    : true
}

export default isEmptyString
