/**
 * @vuepress
 * ---
 * title: isBlankString
 * headline: isBlankString
 * sidebarTitle: isBlankString
 * ---
 */

/**
 * isBlankString
 * @param {string} str - The string
 * @returns {boolean} Returns true if the string is empty or contains only spaces
 * @description
 * Check if the string is empty or contains only spaces
 * @example
 * isBlankString('') // true
 * isBlankString('    ') // true
 * isBlankString('  foo bar  ') // false
 */
export const isBlankString = function (str) {
  return !(str.trim().length > 0)
}

export default isBlankString
