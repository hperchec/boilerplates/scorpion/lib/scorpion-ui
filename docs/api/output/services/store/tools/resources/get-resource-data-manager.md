---
title: get-resource-data-manager
headline: get-resource-data-manager
---

# get-resource-data-manager

<a name="getresourcedatamanager">
</a>

## getResourceDataManager ⇒ <code>module:services/store/plugins/resources-plugin/DataManager~DataManager</code>

Get a resource data manager

**Returns**: <code>module:services/store/plugins/resources-plugin/DataManager~DataManager</code> - The module data manager

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| store | <code>module:vuex~Store</code> |  | The store instance |
| moduleName | <code>string</code> |  | The resource module name (ex: "Users") |
| [rootModule] | <code>string</code> | <code>&quot;\&quot;Resources\&quot;&quot;</code> | The root resource module ( Default: "Resources") |

