---
title: Core class
headline: Core class
sidebarTitle: Core
sidebarDepth: 0
prev: ./utils/
next: ./Service
---

# Core class

<a name="core">
</a>

## Core

Scorpion UI Core class

**Schema**:

- [Core](#core)
  - [.#useVuePlugins()](#core-usevueplugins) ⇒ <code>Promise.&lt;void&gt;</code>
  - [.#createServices()](#core-createservices) ⇒ <code>Promise.&lt;void&gt;</code>
  - _static async methods_
    - [.setup()](#core-setup) ⇒ <code>Promise.&lt;void&gt;</code>
    - [.runApp()](#core-runapp) ⇒ <code>Promise.&lt;Vue&gt;</code>
  - _static methods_
    - [.init(_Vue)](#core-init) ⇒ <code>void</code>
    - [.registerService(service)](#core-registerservice) ⇒ <code>void</code>
    - [.registerVuePlugin(name, plugin, [options])](#core-registervueplugin) ⇒ <code>void</code>
    - [.configure(config)](#core-configure) ⇒ <code>void</code>
    - [.resolveConfig([config])](#core-resolveconfig) ⇒ <code>object</code>
    - [.getRunningApp()](#core-getrunningapp) ⇒ <code>Vue</code>
    - [.getRegisteredServices()](#core-getregisteredservices) ⇒ <code>object</code>
    - [.hasServiceRegistered(identifier)](#core-hasserviceregistered) ⇒ <code>boolean</code>
    - [.getPluginableServices()](#core-getpluginableservices) ⇒ <code>object</code>
    - [.getCreatedServices()](#core-getcreatedservices) ⇒ <code>object</code>
    - [.service(identifier)](#core-service)
    - [.onInitialized(func)](#core-oninitialized) ⇒ <code>void</code>
    - [.onBeforeUseVuePlugin(name, func)](#core-onbeforeusevueplugin) ⇒ <code>void</code>
    - [.onAfterUseVuePlugin(name, func)](#core-onafterusevueplugin) ⇒ <code>void</code>
    - [.onBeforeCreateService(identifier, func)](#core-onbeforecreateservice) ⇒ <code>void</code>
    - [.onServiceCreated(identifier, func)](#core-onservicecreated) ⇒ <code>void</code>
    - [.onBeforeAppBoot(func)](#core-onbeforeappboot) ⇒ <code>void</code>
    - [.onAppBoot(func)](#core-onappboot) ⇒ <code>void</code>
    - [.info()](#core-info) ⇒ <code>object</code>
  - _static properties_
    - [.version](#core-version) : <code>string</code>
    - [.isInitialized](#core-isinitialized) : <code>boolean</code>
    - [.isSetup](#core-issetup) : <code>boolean</code>
    - [.Vue](#core-vue) : <code>Class.&lt;Vue&gt;</code>
    - [.config](#core-config) : <code>Class.&lt;globalConfig&gt;</code>
    - [.context](#core-context) : <code>Class.&lt;module:@/context/index~context&gt;</code>

<a name="core-usevueplugins">
</a>

### Core.#useVuePlugins() ⇒ <code>Promise.&lt;void&gt;</code>

Call Vue.use(plugin, options) for each plugin defined in Core.context.vue.plugins

<a name="core-createservices">
</a>

### Core.#createServices() ⇒ <code>Promise.&lt;void&gt;</code>

Create services. Loop on `Core.context.services` object properties. If a service is "pluginable" (has `service.vuePlugin` property set),the corresponding option is automatically added to `Core.context.vue.root.options`.

<a name="core-setup">
</a>

### Core.setup() ⇒ <code>Promise.&lt;void&gt;</code>

Setup Core. It will:- create and use Vue plugins- create servicesCore must be _initialized_ before setup.

**Category**: static async methods  
<a name="core-runapp">
</a>

### Core.runApp() ⇒ <code>Promise.&lt;Vue&gt;</code>

Run the app. It will:- call "BEFORE_APP_BOOT" hook- call "APP_BOOT" hook- create new Vue instance with `Core.context.vue.root.options`All options in `Core.context.vue.root.options` are injected.Pluginable services that have `config.rootOption` defined are auto-injected too.(example: `router` service is passed as `{ router: Core.service('router') }`)New Vue instance will be assigned to `window.<propertyName>` where `propertyName` is the value of `Core.config.vue.root.name`.Core must be _initialized_ and _setup_ before running app.

**Returns**: <code>Promise.&lt;Vue&gt;</code> - The running app root Vue instance

**Category**: static async methods  
<a name="core-init">
</a>

### Core.init(_Vue) ⇒ <code>void</code>

Initialize Core. Takes `Vue` as first argument. It will:- set `Core.Vue`- register services- call "INITIALIZED" hook

**Category**: static methods  
| Param | Type | Description |
| --- | --- | --- |
| _Vue | <code>Class.&lt;Vue&gt;</code> | Vue |

<a name="core-registerservice">
</a>

### Core.registerService(service) ⇒ <code>void</code>

Register a service. Takes a service object as first argument. See services documentation.Core must be _initialized_ before registering service.

**Category**: static methods  
| Param | Type | Description |
| --- | --- | --- |
| service | <code>module:Service</code> | The service object |

<a name="core-registervueplugin">
</a>

### Core.registerVuePlugin(name, plugin, [options]) ⇒ <code>void</code>

Register a Vue plugin.Core must be _initialized_ before registering Vue plugin.

**Category**: static methods  
| Param | Type | Default | Description |
| --- | --- | --- | --- |
| name | <code>string</code> |  | The plugin name |
| plugin | <code>object</code> |  | The plugin |
| [options] | <code>object</code> | <code>{}</code> | The plugin options |

<a name="core-configure">
</a>

### Core.configure(config) ⇒ <code>void</code>

Configure Core. Takes an object that follows [global configuration](./config) schema as first argument.The given object will be [deep merged](./utils/deep-merge) with Core configuration. The result will be set in `Core.config`.Core must be _initialized_ before configuring.

**Category**: static methods  
| Param | Type | Description |
| --- | --- | --- |
| config | <code>object</code> | The config |

<a name="core-resolveconfig">
</a>

### Core.resolveConfig([config]) ⇒ <code>object</code>

Resolve configuration. Returns the result of `config` parameter object and the `Core.config` object deep merge.

**Returns**: <code>object</code> - Returns the resolved config

**Category**: static methods  
| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [config] | <code>object</code> | <code>{}</code> | (Optional) The config object |

<a name="core-getrunningapp">
</a>

### Core.getRunningApp() ⇒ <code>Vue</code>

Get the current running app (created after the `runApp()` method call)

**Returns**: <code>Vue</code> - Returns the app root Vue instance

**Category**: static methods  
<a name="core-getregisteredservices">
</a>

### Core.getRegisteredServices() ⇒ <code>object</code>

Returns an object like: `{ [identifier: String]: Service }`

**Returns**: <code>object</code> - Return an object of registered services

**Category**: static methods  
<a name="core-hasserviceregistered">
</a>

### Core.hasServiceRegistered(identifier) ⇒ <code>boolean</code>

Returns boolean based on passed service `identifier`

**Returns**: <code>boolean</code> - Returns true if service is defined

**Category**: static methods  
| Param | Type | Description |
| --- | --- | --- |
| identifier | <code>string</code> | The service unique identifier |

<a name="core-getpluginableservices">
</a>

### Core.getPluginableServices() ⇒ <code>object</code>

Returns registered services that must be treated as Vue plugins

**Returns**: <code>object</code> - Returns an object of "pluginable" services

**Category**: static methods  
<a name="core-getcreatedservices">
</a>

### Core.getCreatedServices() ⇒ <code>object</code>

Returns an object like: `{ [identifier: String]: any }`

**Returns**: <code>object</code> - Return an object of created services

**Category**: static methods  
<a name="core-service">
</a>

### Core.service(identifier)

Access created service

**Category**: static methods  
| Param | Type | Description |
| --- | --- | --- |
| identifier | <code>string</code> | The service identifier |

<a name="core-oninitialized">
</a>

### Core.onInitialized(func) ⇒ <code>void</code>

**INITIALIZED** hook append method.The function to append can be _async_:```tsfunction () => void | Promise<void>```

**Category**: static methods  
| Param | Type | Description |
| --- | --- | --- |
| func | <code>function</code> | The function to append to the hook queue |

**Example**

```js
Core.onInitialized(() => {  // Services are only registered but NOT created  console.log('Core is initialized!')})
```

<a name="core-onbeforeusevueplugin">
</a>

### Core.onBeforeUseVuePlugin(name, func) ⇒ <code>void</code>

**BEFORE_USE_VUE_PLUGIN** hook append method.The function to append, which can be _async_ takes two arguments:```tsfunction (plugin: Object, options: any) => void | Promise<void>```- **plugin**: `{object}` the Vue plugin object- **options**: `{any}` the plugin options

**Category**: static methods  
| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The plugin name |
| func | <code>function</code> | The function to append to the hook queue |

**Example**

```js
Core.onBeforeUseVuePlugin('<name>', (plugin, options) => {  // Do logic before use Vue plugin...})
```

<a name="core-onafterusevueplugin">
</a>

### Core.onAfterUseVuePlugin(name, func) ⇒ <code>void</code>

**AFTER_USE_VUE_PLUGIN** hook append method.The function to append, which can be _async_ takes two arguments:```tsfunction (plugin: Object, options: any) => void | Promise<void>```- **plugin**: `{object}` the Vue plugin object- **options**: `{any}` the plugin options

**Category**: static methods  
| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The plugin name |
| func | <code>function</code> | The function to append to the hook queue |

**Example**

```js
Core.onAfterUseVuePlugin('<name>', (plugin, options) => {  // Do logic after use Vue plugin...})
```

<a name="core-onbeforecreateservice">
</a>

### Core.onBeforeCreateService(identifier, func) ⇒ <code>void</code>

**BEFORE_CREATE_SERVICE** hook append method.The function to append takes three arguments:```tsfunction (options: Object, context: Object, Core: Function) => void | Promise<void>```- **options**: `{object}` the service options- **context**: `{object}` the service context (`expose`)- **Core**: `{Function}` the Core

**Category**: static methods  
| Param | Type | Description |
| --- | --- | --- |
| identifier | <code>string</code> | The service unique identifier |
| func | <code>function</code> | The function to append to the hook queue |

**Example**

```js
Core.onBeforeCreateService('<identifier>', (options, context) => {  // Do logic before create service...})
```

<a name="core-onservicecreated">
</a>

### Core.onServiceCreated(identifier, func) ⇒ <code>void</code>

**SERVICE_CREATED** hook append method.The function to append, that can be async, takes one argument:```tsfunction () => void | Promise<void>```- **service**: `{*}` the created service

**Category**: static methods  
| Param | Type | Description |
| --- | --- | --- |
| identifier | <code>string</code> | The service unique identifier |
| func | <code>function</code> | The function to append to the hook queue |

**Example**

```js
Core.onServiceCreated('<identifier>', (service) => {  // Service is created now. Do logic...})
```

<a name="core-onbeforeappboot">
</a>

### Core.onBeforeAppBoot(func) ⇒ <code>void</code>

**BEFORE_APP_BOOT** hook append method.The function to append can be _async_:```tsfunction () => void | Promise<void>```

**Category**: static methods  
| Param | Type | Description |
| --- | --- | --- |
| func | <code>function</code> | The function to append to the hook queue |

**Example**

```js
Core.onBeforeAppBoot(() => {  // Services are created and accessible in this hook  Core.service('logger').consoleLog('Before app boot!')})
```

<a name="core-onappboot">
</a>

### Core.onAppBoot(func) ⇒ <code>void</code>

**APP_BOOT** hook append method.The function to append can be _async_:```tsfunction () => void | Promise<void>```

**Category**: static methods  
| Param | Type | Description |
| --- | --- | --- |
| func | <code>function</code> | The function to append to the hook queue |

**Example**

```js
Core.onAppBoot(() => {  // Services are created and accessible in this hook  Core.service('logger').consoleLog('App boot!')})
```

<a name="core-info">
</a>

### Core.info() ⇒ <code>object</code>

Returns Core info

**Returns**: <code>object</code> - - The info object

**Category**: static methods  
**Example**

```js
Core.info()
```

<a name="core-version">
</a>

### Core.version : <code>string</code>

Core version

**Category**: static properties  
**Read only**: true  
<a name="core-isinitialized">
</a>

### Core.isInitialized : <code>boolean</code>

Returns true if Core is initialized

**Category**: static properties  
**Read only**: true  
<a name="core-issetup">
</a>

### Core.isSetup : <code>boolean</code>

Returns true if Core is setup

**Category**: static properties  
**Read only**: true  
<a name="core-vue">
</a>

### Core.Vue : <code>Class.&lt;Vue&gt;</code>

Vue class reference. Will be defined once `init()` is called

**Default**: <code>undefined</code>  
**Category**: static properties  
**Read only**: true  
<a name="core-config">
</a>

### Core.config : <code>Class.&lt;globalConfig&gt;</code>

Contains default configuration.Will be overwritten once configure() is called.

**Category**: static properties  
<a name="core-context">
</a>

### Core.context : <code>Class.&lt;module:@/context/index~context&gt;</code>

The app context. See also [context](./context) documentation.

**Category**: static properties  
