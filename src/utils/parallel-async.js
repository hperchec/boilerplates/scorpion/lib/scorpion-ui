/**
 * @vuepress
 * ---
 * title: parallelAsync
 * headline: parallelAsync
 * sidebarTitle: parallelAsync
 * ---
 */

/**
 * parallelAsync
 * @param {Array} callbackFns - The array of Promises (same as Promise.all() argument)
 * @returns {Promise<*>} The return value of Promise.all() call
 * @description
 * Resolve some promises in parallel with Promise.all() method
 *
 * ::: tip See also
 * [Promise.all()](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Promise/all) documentation
 * :::
 */
export const parallelAsync = async (callbackFns) => {
  return await Promise.all(callbackFns)
}

export default parallelAsync
