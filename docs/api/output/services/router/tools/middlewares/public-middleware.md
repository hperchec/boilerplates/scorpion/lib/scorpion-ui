---
title: public-middleware
headline: public-middleware
---

# public-middleware

<a name="publicmiddleware">
</a>

## publicMiddleware ⇒ <code>function</code>

Creates a public middleware: only public routes

**Returns**: <code>function</code> - - The encapsulated function

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| middlewareFunc | <code>function</code> |  | The middleware function |
| [metaKey] | <code>string</code> | <code>&quot;&#x27;requiresAuth&#x27;&quot;</code> | The meta key |

