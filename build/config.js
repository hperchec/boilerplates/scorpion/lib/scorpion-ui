/**
 * @file Build configuration
 * @author Hervé Perchec <herve.perchec@gmail.com>
 * @description @hperchec/scorpion-ui [/build/config.js]
 */

const path = require('path')
// Rollup plugins
const alias = require('@rollup/plugin-alias')
const eslint = require('@rollup/plugin-eslint')
const vue = require('rollup-plugin-vue')
const json = require('@rollup/plugin-json')
const cjs = require('@rollup/plugin-commonjs')
const nodePolyfills = require('rollup-plugin-polyfill-node')
const node = require('@rollup/plugin-node-resolve').nodeResolve
// const babel = require('@rollup/plugin-babel')
const replace = require('@rollup/plugin-replace')
// Custom rollup plugins
const extractCSS = require('../shared/rollup/plugins/extract-css')

const eslintOptions = {
  include: [
    'src/**/*.{js,vue}'
  ]
}

const vueOptions = {
  css: false,
  normalizer: '~#shared/rollup/plugins/vue/lib/runtime/component-normalizer.js'
}

// package.json
const packageJson = require('../package.json')

// Get version
const version = process.env.VERSION || packageJson.version

/**
 * The banners to prepend to bundle content
 */
const banner =
  '/*!\n' +
  ` * Scorpion UI v${version}\n` +
  ' * @author [Hervé Perchec](http://herve-perchec.fr)\n' +
  ' * Released under the GPLv3 License.\n' +
  ' */'

/**
 * Aliases
 */
const aliases = require('./alias')
// 'resolve' util
const resolve = p => {
  const base = p.split('/')[0]
  if (aliases[base]) {
    return path.resolve(aliases[base], p.slice(base.length + 1))
  } else {
    return path.resolve(__dirname, '../', p)
  }
}

/**
 * Globals
 */
const globals = {
  '@cospired/i18n-iso-languages': 'languages',
  '../../../../../lib/runtime/component-normalizer.js': 'defaultCustomNormalizer',
  axios: 'axios',
  'base64-arraybuffer-es6': 'base64-arraybuffer-es6',
  bcryptjs: 'bcryptjs',
  color: 'color',
  classnames: 'classnames',
  dateformat: 'dateformat',
  deepmerge: 'deepmerge',
  'deep-bind': 'deep-bind',
  dexie: 'dexie',
  'dexie-encrypted': 'dexie-encrypted',
  'hotkeys-js': 'hotkeys-js',
  'hex-encoding': 'hex-encoding',
  'is-plain-object': 'is-plain-object',
  isobject: 'isobject',
  'is-extendable': 'is-extendable',
  lodash: 'lodash',
  'mobile-device-detect': 'mobileDeviceDetect',
  'promise-queue': 'promise-queue',
  'pusher-js': 'pusherJs',
  'register-service-worker': 'register-service-worker',
  'split-lines': 'split-lines',
  'strip-indent': 'strip-indent',
  'uint8-encoding': 'uint8-encoding',
  vuex: 'vuex',
  'vue-i18n': 'vue-i18n',
  'vue-json-pretty': 'vue-json-pretty',
  'vue-reactive-refs': 'vue-reactive-refs',
  'vue-router': 'vue-router'
}

const builds = {
  // Non minified ESM Development build
  devESM: {
    entry: {
      'scorpion-ui': resolve('@/index.js')
    },
    dest: resolve('dist'),
    moduleName: 'scorpion-ui',
    format: 'esm',
    env: 'development',
    external: function (id) {
      for (const key in globals) {
        if (id.startsWith(key)) {
          return true
        }
      }
    },
    plugins: [
      eslint(eslintOptions),
      extractCSS(),
      vue(vueOptions),
      json(),
      cjs(),
      nodePolyfills(),
      node()
      // babel({ extensions: [ '.vue' ], babelHelpers: 'runtime' })
    ],
    banner: banner
  },
  // Minified ESM production build
  minESM: {
    entry: {
      'scorpion-ui.min': resolve('@/index.js')
    },
    dest: resolve('dist'),
    moduleName: 'scorpion-ui',
    format: 'esm',
    env: 'production',
    external: function (id) {
      for (const key in globals) {
        if (id.startsWith(key)) {
          return true
        }
      }
    },
    plugins: [
      eslint(eslintOptions),
      extractCSS(),
      vue(vueOptions),
      json(),
      cjs(),
      nodePolyfills(),
      node()
      // babel({ babelHelpers: 'runtime' })
    ],
    banner: banner
  },
  // Non minified CommonJS build
  commonJs: {
    entry: {
      'scorpion-ui.common': resolve('@/index.js')
    },
    dest: resolve('dist'),
    moduleName: 'scorpion-ui',
    format: 'cjs',
    env: 'production',
    external: function (id) {
      for (const key in globals) {
        if (id.startsWith(key)) {
          return true
        }
      }
    },
    plugins: [
      eslint(eslintOptions),
      extractCSS(),
      vue(vueOptions),
      json(),
      cjs(),
      nodePolyfills(),
      node()
      // babel({ babelHelpers: 'runtime' })
    ],
    banner: banner
  },
  // Minified CommonJS build
  minCommonJs: {
    entry: {
      'scorpion-ui.common.min': resolve('@/index.js')
    },
    dest: resolve('dist'),
    moduleName: 'scorpion-ui',
    format: 'cjs',
    exports: 'named',
    env: 'production',
    external: function (id) {
      for (const key in globals) {
        if (id.startsWith(key)) {
          return true
        }
      }
    },
    plugins: [
      eslint(eslintOptions),
      extractCSS(),
      vue(vueOptions),
      json(),
      cjs(),
      nodePolyfills(),
      node()
      // babel({ babelHelpers: 'runtime' })
    ],
    banner: banner
  }
}

/**
 * Generate rollup config
 * @param {string} name - The build name
 * @returns {object} - The generated config
 */
function genConfig (name) {
  const opts = builds[name]

  // console.log('__dir', __dirname)
  const config = {
    input: opts.entry,
    external: opts.external,
    plugins: [
      alias({
        entries: Object.assign({}, aliases, opts.alias)
      })
    ].concat(opts.plugins || []),
    output: {
      // file: opts.dest,
      dir: opts.dest,
      format: opts.format,
      banner: opts.banner,
      globals: opts.globals || {},
      name: opts.moduleName,
      exports: opts.exports || 'auto'
      // inlineDynamicImports: true,
    },
    onwarn: (msg, warn) => {
      if (!/Circular/.test(msg)) {
        warn(msg)
      }
    }
  }

  // // console.log('pluging', config.plugins)

  // // built-in vars
  // const vars = {
  //   __VERSION__: version,
  //   __DEV__: `process.env.NODE_ENV !== 'production'`,
  //   __TEST__: false,
  //   __GLOBAL__: opts.format === 'umd' || name.includes('browser')
  // }

  // // build-specific env
  // if (opts.env) {
  //   vars['process.env.NODE_ENV'] = JSON.stringify(opts.env)
  //   vars.__DEV__ = opts.env !== 'production'
  // }

  const vars = {
    __VERSION__: `'${version}'`,
    __DOC_BASE_URL__: `'${packageJson.homepage}'`,
    // __getDocUrl__: `((url) => '${packageJson.homepage}' + url)`,
    __getDocUrl__: '((url) => "<DOC_URL>" + url)',
    preventAssignment: true
  }
  config.plugins.push(replace(vars))

  Object.defineProperty(config, '_name', {
    enumerable: false,
    value: name
  })

  return config
}

if (process.env.TARGET) {
  module.exports = genConfig(process.env.TARGET)
} else {
  exports.globals = globals
  exports.getBuild = genConfig
  exports.getAllBuilds = () => Object.keys(builds).map(genConfig)
}
