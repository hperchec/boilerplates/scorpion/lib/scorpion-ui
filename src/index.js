/**
 * @vuepress
 * ---
 * title: ScorpionUI API
 * headline: ScorpionUI API
 * prev: false # Disable prev link
 * next: ./config/
 * ---
 */

/**
 * @module ScorpionUI
 * @author Hervé Perchec <contact@herve-perchec.com>
 * @description
 * Scorpion UI module
 *
 * ```js
 * import ScorpionUI from '@hperchec/scorpion-ui'
 * ```
 */

import _Core from './Core'
import _utils from './utils'
import _toolbox from './toolbox'
import _builtInServices from './services'
import _Service from './Service'
import _ServiceDefinition from './ServiceDefinition'

export const Core = _Core
export const utils = _utils
export const toolbox = _toolbox
export const builtInServices = _builtInServices
export const Service = _Service
export const ServiceDefinition = _ServiceDefinition

/**
 * @alias module:ScorpionUI
 * @typicalname ScorpionUI
 * @sidebar_title .exports (default)
 * @description scorpion-ui module
 * @type {object}
 */
const ScorpionUI = {
  /**
   * @alias module:ScorpionUI.Core
   * @description The Core class
   * @type {Function}
   * @see {@link ./Core}
   */
  Core,
  /**
   * @alias module:ScorpionUI.builtInServices
   * @description The built-in services
   * @type {object}
   * @see {@link ./services}
   */
  builtInServices,
  /**
   * @alias module:ScorpionUI.Service
   * @description The Service class
   * @type {Function}
   * @see {@link ./Service}
   */
  Service,
  /**
   * @alias module:ScorpionUI.ServiceDefinition
   * @description The ServiceDefinition class
   * @type {Function}
   * @see {@link ./ServiceDefinition}
   */
  ServiceDefinition,
  /**
   * @alias module:ScorpionUI.utils
   * @description The default Core utils
   * @type {object}
   * @see {@link ./utils}
   */
  utils,
  /**
   * @alias module:ScorpionUI.toolbox
   * @description The lib toolbox
   * @type {object}
   * @see {@link ./toolbox}
   */
  toolbox
}

// ⚠ IMPORTANT: expose the library as a global window property
// @ts-ignore
if (window.ScorpionUI) {
  // @ts-ignore
  throw new Error('@hperchec/scorpion-ui [conflict]: window.ScorpionUI is already defined: ', window.ScorpionUI)
} else {
  // @ts-ignore
  window.ScorpionUI = ScorpionUI
}

export default ScorpionUI
