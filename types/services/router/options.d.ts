export default options;
declare namespace options {
    let routes: any;
    function scrollBehavior(to: any, from: any, savedPosition: any): any;
    let defaultOnComplete: Function;
    let defaultOnAbort: Function;
    let beforeEach: Function[];
    let beforeResolve: Function[];
    let afterEach: Function[];
}
//# sourceMappingURL=options.d.ts.map