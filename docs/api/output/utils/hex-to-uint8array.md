---
title: hexToUint8Array
headline: hexToUint8Array
sidebarTitle: hexToUint8Array
---

# hexToUint8Array

<a name="hextouint8array">
</a>

## hexToUint8Array ⇒ <code>Uint8Array</code>

Convert a hexadecimal string to Uint8Array. (Equivalent to php hex2bin function)::: tip See also[hex-encoding](https://www.npmjs.com/package/hex-encoding) package documentation:::

**Returns**: <code>Uint8Array</code> - Returns the decoded string as Uint8Array

| Param | Type | Description |
| --- | --- | --- |
| str | <code>string</code> | The hexadecimal string |

**Example**

```js
hexToUint8Array('666f6f20626172') // => Uint8Array containing string: 'foo bar'
```

