/**
 * @vuepress
 * ---
 * title: isFalse
 * headline: isFalse
 * sidebarTitle: isFalse
 * ---
 */

/**
 * isFalse
 * @param {*} value - The variable to check
 * @returns {boolean} Returns true if value strictly equals false
 * @description
 * Returns true if variable is boolean and false (=== false)
 * @example
 * isFalse(false) // => true
 * isFalse('Hello') // => false
 */
export const isFalse = function (value) {
  return value === false
}

export default isFalse
