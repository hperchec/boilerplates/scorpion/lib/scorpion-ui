/**
 * @vuepress
 * ---
 * title: orderBy
 * headline: orderBy
 * sidebarTitle: orderBy
 * ---
 */

// Lodash.orderBy
import lodashOrderBy from 'lodash.orderby'

/**
 * orderBy
 * @param {...*} args - Same arguments as lodash.orderby
 * @description Sort array of objects (lodash.orderby).
 * @returns {Array} Returns the ordered array
 * @example
 * const array = [ { id: 1, name: 'John' }, { id: 2, name: 'Jane' } ]
 * orderBy(array, function (item) { return item.id }) // Return -> [ { id: 1, name: 'John' }, { id: 2, name: 'Jane' } ]
 * orderBy(array, function (item) { return item.name }) // Return -> [ { id: 2, name: 'Jane' }, { id: 1, name: 'John' } ]
 */
export const orderBy = function (...args) {
  return lodashOrderBy(...args)
}

export default orderBy
