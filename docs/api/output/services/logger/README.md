---
title: Service (logger)
headline: Service (logger)
sidebarTitle: logger
initialOpenGroupIndex: -1
prev: ../
next: ./options
---

# Service (logger)

<a name="service"></a>

The service "logger" is for logging messages (only console is supported until now).

## Register

On registering, the service will inject properties to Vue root instance prototype `$app`:- `$app.log`It will also define Vue components:- `Core.context.vue.components.commons.logger.JsonPretty`: see [commons components](./components/commons/)

## Create

Returns an instance of `Logger` created with options.

**Returns**: <code>module:services/logger/Logger~Logger</code> - Once services are created:

```js
Core.service('logger')
```

## Vue

### Plugin

On registering, service Vue plugin will be set in `Core.context.vue.plugins['logger']`.

Plugin will be automatically used by Vue.

The following properties will be set to Vue prototype:```jsVue.prototype.$logger // => Core.service('logger')```

### Root instance

<a name="app"></a>

#### Prototype $app property

On registering, the service will inject properties to \`Core.context.vue.root.prototype.app\`.

> Properties will be shared by all components under \`vm.$app\` where \`vm\` is the component instance.

**Schema**:

- [$app](#app) : <code>object</code>
  - [.userSettings](#app-usersettings) : <code>object</code>
  - [.log(...args)](#app-log) ⇒ <code>void</code>

<a name="app-usersettings"></a>

##### User settings

Service will define the following user settings keys:

##### Others

<a name="app-log">
</a>

###### vm.$app.log(...args) ⇒ <code>void</code>

log method: shortcut for Logger consoleLog() method

| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | Same parameters as Logger consoleLog() method |

**Example**

```js
// In any componentthis.$app.log('Hello world!')
```

## Expose (service context)

The service will expose the following properties under `Core.context.services['logger']`:
