/**
 * @vuepress
 * ---
 * title: "Service (toast-manager): Toast class"
 * headline: "Service (toast-manager): Toast class"
 * sidebarTitle: .Toast
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

import Core from '@/Core'
import { getAllKeys, isDef, random, typeCheck } from '@/utils'
import Toaster from './Toaster' // jsDoc type

/**
 * @classdesc
 * Service **toast-manager**: Toast class
 */
class Toast {
  /**
   * @private
   */
  _id
  _toaster
  _content
  _asyncHide
  _timeOut
  _timeOutID
  _keepAlive
  _noClose

  /**
   * Create new Toast
   * @param {object} options - The Toast options
   * @param {Toaster} options.toaster - The toast toaster
   * @param {string|object} options.content - The toast content (can be string or Vue component object)
   * @param {object} [options.contentProps = {}] - The props to pass to content component
   * @param {Function} [options.asyncHide = undefined] - A function to call to hide the toast asynchronously
   * @param {number} [options.timeOut = 5000] - The time (in ms) to wait before hide toast (Default: 5000ms). Useless if `asyncHide` property is defined
   * @param {boolean} [options.keepAlive = false] - If the toast must be closed manually
   * @param {boolean} [options.noClose = false] - If the toast cant be closed manually
   * @param {object} [options.meta = {}] - All other meta data to pass to the toast
   */
  constructor (options) {
    // First merge options
    const opts = Object.assign(Toast.defaultOptions, options)
    // Assign properties
    this.toaster = opts.toaster
    this.content = opts.content
    if (isDef(opts.contentProps)) this.contentProps = opts.contentProps
    if (isDef(opts.asyncHide)) this.asyncHide = opts.asyncHide
    if (isDef(opts.timeOut)) this.timeOut = opts.timeOut
    if (isDef(opts.keepAlive)) this.keepAlive = opts.keepAlive
    if (isDef(opts.noClose)) this.noClose = opts.noClose
    if (isDef(opts.meta)) this.meta = opts.meta
    // Set unique ID
    this.setId()
  }

  /**
   * Accessors & mutators
   */

  /**
   * Toast unique id
   * @category properties
   * @readonly
   * @type {string}
   */
  get id () {
    return this._id
  }

  /**
   * Toast toaster
   * @category properties
   * @type {Toaster}
   */
  get toaster () {
    return this._toaster
  }

  set toaster (value) {
    // Check type
    if (typeCheck(Core.context.services['toast-manager'].Toaster, value)) {
      // Assign
      this._toaster = value
    } else {
      // Else -> throw error
      throw new Error('Toast class: \'toaster\' property must be Toaster. ' + typeof value + ' received...')
    }
  }

  /**
   * Toast content
   * @category properties
   * @type {string|object}
   */
  get content () {
    return this._content
  }

  set content (value) {
    // Check type
    if (typeCheck([ String, Object ], value)) {
      // Assign
      this._content = value
    } else {
      // Else -> throw error
      throw new Error('Toast class: \'content\' property must be String or Vue component object. ' + typeof value + ' received...')
    }
  }

  /**
   * Toast contentProps
   * @category properties
   * @type {object}
   */
  get contentProps () {
    return this._contentProps
  }

  set contentProps (value) {
    // Check type
    if (typeCheck(Object, value)) {
      // Assign
      this._contentProps = value
    } else {
      // Else -> throw error
      throw new Error('Toast class: \'contentProps\' property must be object. ' + typeof value + ' received...')
    }
  }

  /**
   * Toast asyncHide
   * @category properties
   * @type {Function}
   */
  get asyncHide () {
    return this._asyncHide
  }

  set asyncHide (value) {
    if (isDef(value)) {
      // Check type
      if (typeCheck(Function, value)) {
        // Assign
        // asyncHide function must be of type: function (hide) {
        //   // Do logic here
        //   hide() // Don't forget to call hide function
        // }
        this._asyncHide = value
      } else {
        // Else -> throw error
        throw new Error('Toast class: \'asyncHide\' property must be Function. ' + typeof value + ' received...')
      }
    }
  }

  /**
   * Toast time out
   * @category properties
   * @type {number}
   */
  get timeOut () {
    return this._timeOut
  }

  set timeOut (value) {
    // Check type
    if (typeCheck(Number, value)) {
      // Assign
      this._timeOut = value
    } else {
      // Else -> throw error
      throw new Error('Toast class: \'timeOut\' property must be Number. ' + typeof value + ' received...')
    }
  }

  /**
   * Toast time out ID
   * @category properties
   * @readonly
   * @type {number}
   */
  get timeOutID () {
    return this._timeOutID
  }

  /**
   * Toast keep-alive
   * @category properties
   * @type {boolean}
   */
  get keepAlive () {
    return this._keepAlive
  }

  set keepAlive (value) {
    // Check type
    if (typeCheck(Boolean, value)) {
      // Assign
      this._keepAlive = value
    } else {
      // Else -> throw error
      throw new Error('Toast class: \'keepAlive\' property must be Boolean. ' + typeof value + ' received...')
    }
  }

  /**
   * Toast no-close
   * @category properties
   * @type {boolean}
   */
  get noClose () {
    return this._noClose
  }

  set noClose (value) {
    // Check type
    if (typeCheck(Boolean, value)) {
      // Assign
      this._noClose = value
    } else {
      // Else -> throw error
      throw new Error('Toast class: \'noClose\' property must be Boolean. ' + typeof value + ' received...')
    }
  }

  /**
   * Methods
   */

  /**
   * setId
   * @category methods
   * @returns {void}
   * @description
   * Set unique ID
   */
  setId () {
    const randomStr = random.alphaNumeric(8)
    this._id = `toast_${this.toaster.name}_${Date.now()}_${randomStr}`
  }

  /**
   * init
   * @category methods
   * @returns {Toast} Returns the Toast instance itself
   * @description
   * Init toast
   */
  init () {
    // Delete function to pass
    const delFunc = () => {
      this.toaster.delete(this.id)
    }
    // Check if asyncHide
    if (this.asyncHide) {
      this.asyncHide(delFunc)
    } else {
      if (!this.keepAlive) {
        this._timeOutID = setTimeout(delFunc, this.timeOut)
      }
    }
    return this
  }

  /**
   * clearTimeOut
   * @category methods
   * @returns {void}
   * @description
   * Clear time out
   */
  clearTimeOut () {
    clearTimeout(this.timeOutID)
  }

  /**
   * delete
   * @category methods
   * @returns {void}
   * @description
   * Delete toast
   */
  delete () {
    this.clearTimeOut()
    this.toaster.delete(this.id)
  }

  /**
   * Overrides the default toJSON object method for JSON.stringify() calls
   * @returns {object} - The instance data to be serialized
   */
  toJSON () {
    const keys = getAllKeys(this, {
      excludeKeys: [
        /^_/, // private properties that starts with '_'
        'toJSON',
        'toString',
        'constructor'
      ]
    })
    const jsonObj = keys.reduce((obj, key) => {
      obj[key] = this[key]
      return obj
    }, {})
    return jsonObj
  }

  /**
   * Static
   */

  /**
   * Default options
   * @type {object}
   * @property {Toaster} toaster - The toast toaster (default: `undefined`)
   * @property {string|object} content - The toast content (can be string or Vue component object) (default: `undefined`)
   * @property {object} contentProps - The props to pass to content component if defined (default: `{}`)
   * @property {Function} asyncHide - A function to call to hide the toast asynchronously (default: `undefined`)
   * @property {number} timeOut - The time (in ms) to wait before hide toast. Useless if `asyncHide` property is defined (default: `5000`)
   * @property {boolean} keepAlive - If the toast must be closed manually (default: `false`)
   * @property {boolean} noClose - If the toast cant be closed manually (default: `false`)
   * @property {object} meta - All other meta data to pass to the toast (default: `{}`)
   */
  static get defaultOptions () {
    return {
      toaster: undefined,
      content: undefined,
      contentProps: {},
      asyncHide: undefined,
      timeOut: 5000,
      keepAlive: false,
      noClose: false,
      meta: {}
    }
  }
}

export default Toast
